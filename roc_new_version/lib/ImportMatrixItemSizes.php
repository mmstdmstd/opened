<?php
class ImportMatrixItemSizes
{
	private $ItemsAdded = 0;
	private $ItemsDeleted = 0;

	private $OriginalExcelFileName;
	private $ExcelFileName;
	private $ColumnsArray;

	private function getColumnName($pColumnNumber)
	{
		foreach ($this->ColumnsArray as $ColumnNumber => $ColumnName) {
			if ((int)$pColumnNumber == (int)$ColumnNumber) return $ColumnName;
		}
	}

	public function setFileName($pExcelFileName)
	{
		$this->ExcelFileName = $pExcelFileName;
	}

	public function setOriginalFileName($pOriginalExcelFileName)
	{
		$this->OriginalExcelFileName = $pOriginalExcelFileName;
	}


	public function Run()
	{
		$this->ItemsAdded = 0;

		$ExcellArray = new sfExcelReader($this->ExcelFileName);
		$ExcellTabData = $ExcellArray->sheets[0]['cells'];
		// echo '<pre> $ExcellTabData ::'.print_r( $ExcellTabData, true ).'</pre><br>';
		//return ;
		$this->ColumnsArray = $ExcellTabData[1];
		//Util::deb( count($ExcellTabData), ' count($ExcellTabData)::');

		for ($I = 2; $I <= count($ExcellTabData) + 250; $I++) {
			if (empty($ExcellTabData[$I])) continue;
			$DataArray = $ExcellTabData[$I];
			unset($lSizeOrder);
			$Size = '';

			foreach ($DataArray as $ColumnNumber => $ColumnValue) { // all columns in dict
				$ColumnName = trim($this->getColumnName($ColumnNumber));
				$ColumnValue = trim($ColumnValue);
				if (strtolower($ColumnName) == strtolower('SIZE')) {
					$Size = $ColumnValue;
				}
			} // foreach( $datasArray as $ColumnNumber=>$ColumnValue ) { // all columns in dict
			 //Util::deb($Size, ' $Size::');

			if( strlen($Size) > 0 ) {
			  try {
				  $lSizeOrder= new SizeOrder();
				  $lSizeOrder->setSize($Size);
				  $lSizeOrder->save();
				  $this->ItemsAdded ++;
		  	} catch (Exception $lException) {
			  	continue;
			  }
			}
		}

	}

	public function DeleteAllRows()
	{
		$this->ItemsDeleted=0;
		$SizeOrderList= SizeOrderPeer::getSizeOrders('', false);
		foreach($SizeOrderList as $lSizeOrder) {
			$lSizeOrder->delete();
			$this->ItemsDeleted++;
		}
	}

	public function getInfoText()
	{
		$Res = '<hr>';
		$Res .= 'Import from : <b>' . $this->OriginalExcelFileName . '</b>. <br><br>&nbsp;&nbsp;' .
		'Size Orders Deleted: <b>' . $this->ItemsDeleted . '</b><br>&nbsp;&nbsp;' .
			$this->ItemsAdded . ' Size Orders Added.';
		//OptionsPeer::addOptionsValue( 'ImportMatrixItemSizes', $this->ItemsAdded . ' rows uploaded at ' .strftime('%Y-%m-%d %H:%M') );
		ImportRulesPeer::addImportRulesValue( 'ImportMatrixItemSizes', $this->ItemsAdded . ' rows uploaded at ' .strftime('%Y-%m-%d %H:%M') );
		return $Res;
	}

}