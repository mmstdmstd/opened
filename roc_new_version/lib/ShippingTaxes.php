<?php
class ShippingTaxes {


  /*
  * Function etShippingRates
  * @params $dst_zip
  * @return $rates
  */
  public static function getShippingRates( $dst_zip, $LowerArrayKey= false, $is_debug= false )
  {
    /*echo ' -21 getShippingRates  $dst_zip::<pre> '. print_r( $dst_zip, true ) .'</pre><br>';
    echo ' -22 getShippingRates  $LowerArrayKey  ::  <pre> '. print_r( $LowerArrayKey, true ) .'</pre><br>';
    echo ' -23 getShippingRates  $is_debug::<pre> '. print_r( $is_debug, true ) .'</pre><br>'; */
    $rates = array();
    /************** USPS ************/
    $postal_code = sfConfig::get('app_UPS_postal_code'); // 20770
    $TotalShipWeight= Cart::getTotalShipWeight();
    //   Util::deb( $TotalShipWeight, '-$TotalShipWeight::' );
    
    if ( empty($TotalShipWeight) ) $TotalShipWeight= 2;
    $RSSRM = "
      <RateV3Request USERID=\"397PETWO5514\">
          <Package ID=\"1ST\">
              <Service>ALL</Service>
              <ZipOrigination>$postal_code</ZipOrigination>
              <ZipDestination>$dst_zip</ZipDestination>
              <Pounds>" . $TotalShipWeight . "</Pounds>
              <Ounces>0</Ounces>
              <Container/>
              <Size>REGULAR</Size>
              <Machinable>true</Machinable>
          </Package>
      </RateV3Request>";
    //$is_debug= 1;
    //echo '-10:: $RSSRM:: <pre>' . print_r($RSSRM, true) . '</pre><br>';
    $ch = curl_init();
    $URL= "http://production.shippingapis.com/ShippingAPI.dll?API=RateV3&XML=" . urlencode($RSSRM);
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    $data   = strstr($result, '<?');
    if ( $is_debug ) {
      echo '$result::<pre>';
      print_r($result);
      echo '</pre>';
    }
    $parser = xml_parser_create();
    xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
    xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
    xml_parse_into_struct($parser, $data, $values, $tags);
    xml_parser_free($parser);

    if ( $is_debug ) {
      echo '$values::<pre>';
      print_r($values);
      echo '</pre>';
    }
    $counter = 0;
    $c       = 0;
    foreach ($values as $index) {
      foreach ($index as $key => $value) {
        /*echo '<pre>';
        print_r($key);
        echo '</pre>';*/
        if ($key == 'value' and preg_match("/Flat Rate Boxes$/", $value)) {
          $c = $counter + 1;
        }
      }
      $counter++;
    }
    if ($c != 0) {
      $rates['USPS'] = $values[$c]['value'];
    }
    curl_close($ch);

    /************** UPS ************/
    if ( !$LowerArrayKey ) {
      $rates['UPS-Ground']          = self::ups($dst_zip, '03', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['UPS-3Day']    = self::ups($dst_zip, '12', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['UPS-2Day']         = self::ups($dst_zip, '02', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['UPS-NextDay'] = self::ups($dst_zip, '01', $TotalShipWeight/*'1'*/, '1', '1', '1');
    } else {
      $rates['ups_ground']          = self::ups($dst_zip, '03', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['ups_3_day_selected']    = self::ups($dst_zip, '12', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['ups_2_day_air']         = self::ups($dst_zip, '02', $TotalShipWeight/*'1'*/, '1', '1', '1');
      $rates['ups_next_day_air_saver'] = self::ups($dst_zip, '01', $TotalShipWeight/*'1'*/, '1', '1', '1');
    } // {"postal_code":"55555","usps":"0","ups_ground":"9.49","ups_3_day_selected":"14.54","ups_2_day_air":"19.38","ups_next_day_air_saver":"50.56","est_tax":"1.99","est_shipping":"50.56","estimated_total":"342.55"};

/*    $out_tax_rates['tax']                = "$tax";
    $out_tax_rates['USPS']               = $shipping['USPS'];
    $out_tax_rates['UPS-Ground']          = $shipping['UPSGround'];

    $out_tax_rates['UPS-3Day']    = $shipping['UPS3DaySelected'];
    $out_tax_rates['UPS-2Day']         = $shipping['UPS2DayAir'];
    $out_tax_rates['UPS-NextDay'] = $shipping['UPSNextDayAirSaver'];
*/
    if ( $is_debug ) {
      echo '<pre>';
      print_r($rates);
      echo '</pre>';
    }
    return $rates;
  } // public static function getShippingRates( $dst_zip, $is_debug= false )


  private static function ups($dest_zip, $service, $weight, $length, $width, $height)
  {
    /*$access_license_number = 'EC4B8375440DAE00';
    $user_id               = 'petworldstore';
    $password              = 'pet37world';
    $postal_code           = '10001';
    $shipper_number        = '2937X9';  */
    $access_license_number = sfConfig::get('app_UPS_access_license_number');
    $user_id = sfConfig::get('app_UPS_user_id');
    $password = sfConfig::get('app_UPS_password');
    $postal_code = sfConfig::get('app_UPS_postal_code');
    $shipper_number = sfConfig::get('app_UPS_shipper_number');

    $data ="<?xml version=\"1.0\"?>
      <AccessRequest xml:lang=\"en-US\">
    		<AccessLicenseNumber>$access_license_number</AccessLicenseNumber>
    		<UserId>$user_id</UserId>
    		<Password>$password</Password>
    	</AccessRequest>
    	<?xml version=\"1.0\"?>
    	<RatingServiceSelectionRequest xml:lang=\"en-US\">
    		<Request>
    			<TransactionReference>
    				<CustomerContext>Bare Bones Rate Request</CustomerContext>
    				<XpciVersion>1.0001</XpciVersion>
    			</TransactionReference>
    			<RequestAction>Rate</RequestAction>
    			<RequestOption>Rate</RequestOption>
    		</Request>
    	<PickupType>
    		<Code>01</Code>
    	</PickupType>
    	<Shipment>
    		<Shipper>
    			<Address>
    				<PostalCode>$postal_code</PostalCode>
    				<CountryCode>US</CountryCode>
    			</Address>
			<ShipperNumber>$shipper_number</ShipperNumber>
    		</Shipper>
    		<ShipTo>
    			<Address>
    				<PostalCode>$dest_zip</PostalCode>
    				<CountryCode>US</CountryCode>
				<ResidentialAddressIndicator/>
    			</Address>
    		</ShipTo>
    		<ShipFrom>
    			<Address>
    				<PostalCode>$postal_code</PostalCode>
    				<CountryCode>US</CountryCode>
    			</Address>
    		</ShipFrom>
    		<Service>
    			<Code>$service</Code>
    		</Service>
    		<Package>
    			<PackagingType>
    				<Code>02</Code>
    			</PackagingType>
    			<Dimensions>
    				<UnitOfMeasurement>
    					<Code>IN</Code>
    				</UnitOfMeasurement>
    				<Length>$length</Length>
    				<Width>$width</Width>
    				<Height>$height</Height>
    			</Dimensions>
    			<PackageWeight>
    				<UnitOfMeasurement>
    					<Code>LBS</Code>
    				</UnitOfMeasurement>
    				<Weight>$weight</Weight>
    			</PackageWeight>
    		</Package>
    	</Shipment>
    	</RatingServiceSelectionRequest>";
    $ch = curl_init("https://www.ups.com/ups.app/xml/Rate");
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch,CURLOPT_POST,1);
    curl_setopt($ch,CURLOPT_TIMEOUT, 60);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    $result = curl_exec ($ch);
    $data = strstr($result, '<?');
    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $data, $vals, $index);
    xml_parser_free($xml_parser);
    $params = array();
    $level = array();
    foreach ($vals as $xml_elem) {
      if ($xml_elem['type'] == 'open') {
        if (array_key_exists('attributes',$xml_elem)) {
          list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
        }
        else {
          $level[$xml_elem['level']] = $xml_elem['tag'];
        }
      }
      if ($xml_elem['type'] == 'complete') {
        $start_level = 1;
        $php_stmt = '$params';
        while($start_level < $xml_elem['level']) {
          $php_stmt .= '[$level['.$start_level.']]';
          $start_level++;
        }
        $php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
        eval($php_stmt);
      }
    }
    curl_close($ch);
    if ( empty($params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE']) ) return 0;
    return $params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'];
  }  // private static function ups($dest_zip, $service, $weight, $length, $width, $height)



  public static function Calculate_tax( $dst_zip, $dst_state, $is_debug= false )
  {
    if (strlen($dst_state) != 2 )  {
      $dst_state= AppUtils::getStateNameByAbbr($dst_state);
    }
    $tax           = 0;
    $out_tax_rates = array();
    // This is test array of taxes
    $taxes         = array(
    'MI' => 6,
    'IN' => 7,
    'WI' => 5,
    'MN' => 6.875,
    'IL' =>  8.75
    );

    $state   = $dst_state;
    $dst_zip = $dst_zip;
    $shipping = self::getShippingRates($dst_zip);

    //print_r($CheckoutArray);  //debug

    foreach ($taxes as $key => $value) {
      if ($key == $state) {
        $tax = $value;
      }
    }
    return $tax;
/*    $out_tax_rates['tax']                = "$tax";
    $out_tax_rates['USPS']               = $shipping['USPS'];
    $out_tax_rates['UPSGround']          = $shipping['UPSGround'];
    $out_tax_rates['UPS3DaySelected']    = $shipping['UPS3DaySelected'];
    $out_tax_rates['UPS2DayAir']         = $shipping['UPS2DayAir'];
    $out_tax_rates['UPSNextDayAirSaver'] = $shipping['UPSNextDayAirSaver'];  */

    /*
    $out_tax_rates['tax']                = $tax;
    $out_tax_rates['USPS']               = $shipping['USPS'];
    $out_tax_rates['UPS-Ground']          = $shipping['UPS-Ground'];

    $out_tax_rates['UPS-3Day']    = $shipping['UPS-3Day'];
    $out_tax_rates['UPS-2Day']         = $shipping['UPS-2Day'];
    $out_tax_rates['UPS-NextDay'] = $shipping['UPS-NextDay'];
    $out_tax_rates = json_encode($out_tax_rates);
    return $out_tax_rates; */
  } // public static function executeCalculate_tax()


}
?>
