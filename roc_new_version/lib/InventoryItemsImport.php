<?php

class InventoryItemsImport {
	private $InventoryItemsImportCVSFile='';
	private $ColumnsArray= array();
	private $InventaryItemsSkuArray= array();

	private $ItemsDeletedAsDisabled=0;
	private $ItemsSkippedAsNonExisting=0;
	private $ItemsAdded=0;
	private $ItemsModified=0;
	private $ItemsSkippedAsExising=0;
	private $ItemsDeletedAsNoneExising=0;
	private $ImagesAdded=0;
	private $ImageThumbnailsAdded=0;
	private $InventoryCategoriesDeletedAsEmpty= 0;
  private $ItemsSkippedAsWithEmptyInventoryCategory= 0;

	private function getColumnName($pColumnNumber ) {
		foreach( $this->ColumnsArray as $ColumnNumber=>$ColumnName ) {
			if ( (int)$pColumnNumber== (int)$ColumnNumber ) return $ColumnName;
		}
	}

	public function setInventoryItemsImportCVSFile($pInventoryItemsImportCVSFile) {
		$this->InventoryItemsImportCVSFile= $pInventoryItemsImportCVSFile;
	}

	private function ReadCSVData() {
		$CsvFileName= $this->InventoryItemsImportCVSFile;
		$ResArray= array();
		$reader = new sfCsvReader( $CsvFileName, ',' );
		//	Util::deb( $CsvFileName, ' $CsvFileName::' );
		//  echo '<pre> $CsvFileName ::'.print_r( $CsvFileName, true ).'</pre><br>';
		$reader->open();

    $data_keys = $reader->read();

		$I= 0;
		while ( $data = $reader->read() ) {
			if ( count($data) == 1 ) {
				continue;
			}
			$L= count($data);
			//unset($data[$L-1]);
			//unset($data[0]);
			//$ResArray[]= $data;
			//Util::deb( count($data), 'count($data)::' );
      /* if ( $I <= 10 ) {
			  Util::deb( $data, '$data::' );
			  Util::deb( $I, '$I::' );
      } 
      if ( $I>10 ) die("JJJ");  */

      $data_mixed = array_combine($data_keys,$data);
			//  Util::deb( $data_mixed, '$data_mixed::' );

			try {
			  //if ( $I== 0 ) {
			    //$this->ColumnsArray= $data;
			  //} else {
			    $this->ImportCvsData($I, $data_mixed);
			  //}
			}
		  catch (Exception $x) {
			  echo '<pre> Error '.print_r( $x, true ).' On Row data::'.print_r( $data, true ).'</pre><br>';
			}
			$I++;
      // die("UUU");
			/*if( $I>= 4 ) { // FOR DEBUGGING
			  $reader->close();
		    echo '<pre> Rows added $I ::'.print_r( $I, true ).'</pre><br>';
		//Util::deb( $ResArray, ' $ResArray::' );
		//die("DIE");
			  return $ResArray;
			} */

		}

		$reader->close();
		//Util::deb( $ResArray, ' $ResArray::' );
		// die("DIE");
		return $ResArray;
	} // private function ReadCSVData( $ParentSKU, $ProductId, $isPageSetsListByDirs ) {

	private function DeleteNonExistingInventoryItems() {
	$InventoryItemsList= InventoryItemPeer::getInventoryItems( 1, false, false, '', '', '', '', false, '', '', '', '', '', '', '', '', 'SKU', '' );
	foreach( $InventoryItemsList as $InventoryItem ) {
	if ( !in_array( $InventoryItem->getSku(), $this->InventaryItemsSkuArray ) ) {
	$InventoryItem->delete();
	$this->ItemsDeletedAsNoneExising++;
	}
	}
	return true;
	}

	public function Run() {
		$CvsDataArray= $this->ReadCSVData();
		/*$this->ColumnsArray= $CvsDataArray[0];
		$RowNumber= 1;
		foreach( $CvsDataArray as $CvsKey=>$CvsDataRowValues) {
			if ( $RowNumber== 1 ) {
				$RowNumber++;
				continue; // Skip Titles row
			}
			$this->ImportCvsData($CvsKey, $CvsDataRowValues);
			$RowNumber++;
		} */
		// $this->DeleteNonExistingInventoryItems();
		// $this->DeleteNonExistingInventoryCategories();
		return true;
	} // public function Run() {


	private function ImportCvsData($CvsKey, $CvsDataRowValues) {
		$ItemCode= '';
		$Title= '';
/*UDF_IT_MATRIX
UDF_IT_ROOTITEM
UDF_IT_FEATURES
*/
		$Brand= '';
		$InventoryCategory= '';
		$DescriptionShort= '';
		$DescriptionLong= '';
		$Size= '';
		$Color= '';
		$Finish='';
		$Options= '';
		$Gender= '';
		$Paperwork= '';
		$Sizing= '';
		$Customizable= '';
		$Video= '';
		$Sizing= '';
		$Documents= '';
		$Hand= '';

		$Matrix= '';
		$RootItem= '';
		$Features= '';

		$UnitMeasure= '';
		$QtyOnHand= '';
		$StdUnitPrice= '';
		$Msrp= '';
		$Clearance= '';
		$SaleStartDate= '';
		$SaleEndDate= '';
		$SaleMethod= '';
		$SalePrice= '';
		$SalePromoDiscount= '';
		$TaxClass= '';
		$ShipWeight= '';
		$SetupCharge= '';
		$DateUpdated= '';
		$TimeUpdated= '';
		$Enabled= '';
		// UDF_ADDITIONAL_FREIGHT and UDF_FREE_SHIPPING
		$AdditionalFreight= '';  
		$FreeShipping= false;    
		$PhoneOrder= false;
		$UDF_IT_SWC_BEIGE= false;
    $UDF_IT_SWC_BLACK= false;
    $UDF_IT_SWC_BLUE= false;
    $UDF_IT_SWC_BROWN= false;
    $UDF_IT_SWC_CAMO= false;
    $UDF_IT_SWC_GOLD= false;
    $UDF_IT_SWC_GRAY= false;
    $UDF_IT_SWC_GREEN= false;
    $UDF_IT_SWC_ORANGE= false;
    $UDF_IT_SWC_PINK= false;
    $UDF_IT_SWC_PURPLE= false;
    $UDF_IT_SWC_RED= false;
    $UDF_IT_SWC_SILVER= false;
    $UDF_IT_SWC_WHITE= false;
    $UDF_IT_SWC_YELLOW= false;

		//$this->InventaryItemsSkuArray[]= trim( $CvsDataRowValues[1] );

    //$this->InventaryItemsSkuArray[]= trim( $CvsDataRowValues[0] );
    $this->InventaryItemsSkuArray[]= trim( $CvsDataRowValues['ItemCode'] );

		//foreach( $CvsDataRowValues as $ColumnValue ) { // all columns in Inventory Item
			//$ColumnName= trim( $this->getColumnName($ColumnNumber) );
			//$ColumnValue= trim($ColumnValue);

			//if ( strtolower($ColumnName) == strtolower('ItemCode') ) {

				//$ItemCode= $CvsDataRowValues[0];
				$ItemCode= $CvsDataRowValues['ItemCode'];

				/*if ( $ItemCode == 'PG2750702' ) {
				  Util::deb( $ItemCode, '$ItemCode::' );
				  Util::deb( $CvsDataRowValues, '$CvsDataRowValues::' );
				  Util::deb( $CvsDataRowValues[122], '$CvsDataRowValues[122]::' );
				  Util::deb( $CvsDataRowValues[123], '$CvsDataRowValues[123]::' );
				} */ 
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_DESCRIPTION') /*strtolower('ItemCodeDesc')*/ ) {
				//$Title = $CvsDataRowValues[105];
				$Title = $CvsDataRowValues['UDF_IT_DESCRIPTION'];
        $Title = mb_convert_encoding ($Title , 'UTF-8', 'Cp-1251');
        $Title = preg_replace("/®/", '(R)', $Title);
        $Title = preg_replace("/™/", '(TM)', $Title);
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_BRAND') ) {
				//$Brand= $CvsDataRowValues[103];
				$Brand= $CvsDataRowValues['UDF_BRAND'];
			//}
			//if ( strtolower($ColumnName) == strtolower('ProductLine') ) {
				//$InventoryCategory= $CvsDataRowValues[26];
				$InventoryCategory= $CvsDataRowValues['ProductLine'];
        // Util::deb( $InventoryCategory, '$InventoryCategory::' );
        // die(-1);
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_DESCRIPTION') ) {
				//$DescriptionShort = $CvsDataRowValues[105];
				$DescriptionShort = $CvsDataRowValues['UDF_IT_DESCRIPTION'];
        $DescriptionShort = mb_convert_encoding ($DescriptionShort , 'UTF-8', 'Cp-1251');
        $DescriptionShort = preg_replace("/®/", '(R)', $DescriptionShort);
        $DescriptionShort = preg_replace("/™/", '(TM)', $DescriptionShort);
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_LONG_DESCRI') ) {
				//$DescriptionLong = $CvsDataRowValues[108];
				$DescriptionLong = $CvsDataRowValues['UDF_IT_LONG_DESCRIPTION'];
        $DescriptionLong = mb_convert_encoding ($DescriptionLong , 'UTF-8', 'Cp-1251');
        $DescriptionLong = preg_replace("/®/", '(R)', $DescriptionLong);
        $DescriptionLong = preg_replace("/™/", '(TM)', $DescriptionLong);
			//}
			//if ( strtolower($ColumnName) == strtolower('Category1') ) {
				//$Size= $CvsDataRowValues[33];
    		$Size= $CvsDataRowValues['UDF_SIZE'];
		    if ( empty($Size) ) {
				  $Size= $CvsDataRowValues['Category1'];
				}
			//}
			//if ( strtolower($ColumnName) == strtolower('Category2')  ) {
				//$Color= $CvsDataRowValues[34];
    		$Color= $CvsDataRowValues['UDF_COLOR'];
		    if (empty($Color)) {
				  $Color= $CvsDataRowValues['Category2'];
				}
			//}
			//if ( strtolower($ColumnName) == strtolower('Category3')  ) { // !!! PLAIN, BASKET, NYLON LOOK, HI GLOSS
				//$Finish= $CvsDataRowValues[35];
    		$Finish= $CvsDataRowValues['UDF_FINISH'];
		    if ( empty($Finish) ) {
				  $Finish= $CvsDataRowValues['Category3'];
				}
			//}
			//if ( strtolower($ColumnName) == strtolower('Category4') ) { // 62025SH, 0 RIDGE
				//$Options= $CvsDataRowValues[36];
    		$Options= $CvsDataRowValues['UDF_OPTIONS'];
		    if (empty($Options)) {
				  $Options= $CvsDataRowValues['Category4'];
				}
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_GENDER')  ) {
				//$Gender= $CvsDataRowValues[107];
				$Gender= $CvsDataRowValues['UDF_IT_GENDER'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_PAPERWORK_R')  ) {
				//$Paperwork= $CvsDataRowValues[109];
				$Paperwork= $CvsDataRowValues['UDF_IT_PAPERWORK_REQUIRED'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_SIZING_CHAR')  ) {
				//$Sizing= $CvsDataRowValues[111];
				$Sizing= $CvsDataRowValues['UDF_IT_SIZING_CHART'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_CUSTOMIZABL')  ) {
				//$Customizable= $CvsDataRowValues[104];
				$Customizable= $CvsDataRowValues['UDF_IT_CUSTOMIZABLE'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_VIDEO')  ) {
				//$Video= $CvsDataRowValues[112];
				$Video= $CvsDataRowValues['UDF_IT_VIDEO'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_PRODUCT_DOC')  ) { // empty !!!
				//$Documents= $CvsDataRowValues[110];
				$Documents= $CvsDataRowValues['UDF_IT_PRODUCT_DOCS'];
			//}

				//$Hand= $CvsDataRowValues[118];
				$Hand= $CvsDataRowValues['UDF_IT_HAND'];
        /* if ( !empty($Hand) ) {
          Util::deb( $ItemCode, '$ItemCode::' );
          Util::deb( $Hand, '$Hand::' );
        }  */

			//if ( strtolower($ColumnName) == strtolower('UDF_IT_MATRIX')  ) {
				//$Matrix= $CvsDataRowValues[115];
				$Matrix= $CvsDataRowValues['UDF_IT_MATRIX'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_ROOTITEM')  ) {
				//$RootItem= $CvsDataRowValues[116];
				$RootItem= $CvsDataRowValues['UDF_IT_ROOTITEM'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_FEATURES')  ) {
				//$Features= $CvsDataRowValues[114];
				$Features= $CvsDataRowValues['UDF_IT_FEATURES'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SalesUnitOfMeasure')  ) { // EACH
				//$UnitMeasure= $CvsDataRowValues[15];
				$UnitMeasure= $CvsDataRowValues['SalesUnitOfMeasure'];
			//}
			//if ( strtolower($ColumnName) == strtolower('TotalQuantityOnHan')  ) {
				//$QtyOnHand= $CvsDataRowValues[99];
				$QtyOnHand= $CvsDataRowValues['TotalQuantityOnHand'];
			//}
			//if ( strtolower($ColumnName) == strtolower('StandardUnitPrice')  ) {
				//$StdUnitPrice= $CvsDataRowValues[42];
				$StdUnitPrice= $CvsDataRowValues['StandardUnitPrice'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SuggestedRetailPri')  ) { // !!! Float 2 decims
				//$Msrp= $CvsDataRowValues[97];
				$Msrp= $CvsDataRowValues['SuggestedRetailPrice'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SalesPromotionCode')  ) { // !!! Float 2 decims
				//$Clearance= $CvsDataRowValues[78];
				$Clearance= $CvsDataRowValues['SalesPromotionCode'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SaleStartingDate')  ) { // 10/31/2008
				//$SaleStartDate= $CvsDataRowValues[79];
				$SaleStartDate= $CvsDataRowValues['SaleStartingDate'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SaleEndingDate')  ) {
				//$SaleEndDate= $CvsDataRowValues[80];
				$SaleEndDate= $CvsDataRowValues['SaleEndingDate'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SaleMethod')  ) { // varcahr(1) ?
				//$SaleMethod= $CvsDataRowValues[81];
				$SaleMethod= $CvsDataRowValues['SaleMethod'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SalesPromotionPric')  ) {
				//$SalePrice= $CvsDataRowValues[96];
				$SalePrice= $CvsDataRowValues['SalesPromotionPrice'];
			//}

			//if ( strtolower($ColumnName) == strtolower('SalesPromotionDisc')  ) {
				//$SalePromoDiscount= $CvsDataRowValues[98]; // !!  new field sale_promo_discount ?
				$SalePromoDiscount= $CvsDataRowValues['SalesPromotionDiscountPercent'];
			//}
			//if ( strtolower($ColumnName) == strtolower('TaxClass')  ) {
				//$TaxClass= $CvsDataRowValues[24];
				$TaxClass= $CvsDataRowValues['TaxClass'];
			//}



			//if ( strtolower($ColumnName) == strtolower('ShipWeight')  ) {
				//$ShipWeight= $CvsDataRowValues[121]; // 38
				$ShipWeight= $CvsDataRowValues['ShipWeight'];
			//}
			//if ( strtolower($ColumnName) == strtolower('SetupCharge')  ) {
				//$SetupCharge= $CvsDataRowValues[90]; // varchar 10
				$SetupCharge= $CvsDataRowValues['SetupCharge'];
			//}

			//if ( strtolower($ColumnName) == strtolower('DateUpdated')  ) {
				//$DateUpdated= $CvsDataRowValues[53];
				$DateUpdated= $CvsDataRowValues['DateUpdated'];
			//}
			//if ( strtolower($ColumnName) == strtolower('TimeUpdated')  ) {
				//$TimeUpdated= $CvsDataRowValues[54];
				$TimeUpdated= $CvsDataRowValues['TimeUpdated'];
			//}
			//if ( strtolower($ColumnName) == strtolower('UDF_IT_ENABLED')  ) {
				//$Enabled= $CvsDataRowValues[106];
				$Enabled= $CvsDataRowValues['UDF_IT_ENABLED'];

				//$AdditionalFreight= $CvsDataRowValues[122];
				$AdditionalFreight= $CvsDataRowValues['UDF_ADDITIONAL_FREIGHT'];
				//$FreeShipping= $CvsDataRowValues[123];
				$FreeShipping= $CvsDataRowValues['UDF_FREE_SHIPPING'];
				if ( $FreeShipping=='Y' ) $FreeShipping= true;
			// Util::deb( $FreeShipping, '-00 $FreeShipping::' );

				//$PhoneOrder= $CvsDataRowValues[125];
				$PhoneOrder= $CvsDataRowValues['UDF_IT_PHONE_ONLY'];

				if ( $PhoneOrder=='Y') {
          $PhoneOrder= true;
        } else {
          $PhoneOrder= false;
        }

		    /*if ( (  strlen(trim($CvsDataRowValues['UDF_IT_SWC_BEIGE'])) > 0 and trim($CvsDataRowValues['UDF_IT_SWC_BEIGE']) !='N'  )	or
					( strlen(trim($CvsDataRowValues['UDF_IT_SWC_BLACK'])) > 0 and trim($CvsDataRowValues['UDF_IT_SWC_BLACK'] ) !='N' )  or
					( strlen(trim($CvsDataRowValues['UDF_IT_SWC_BLUE'])) > 0 and trim($CvsDataRowValues['UDF_IT_SWC_BLUE']) !='N' ) or
					( strlen(trim($CvsDataRowValues['UDF_IT_SWC_BROWN'])) > 0 and trim($CvsDataRowValues['UDF_IT_SWC_BROWN']) != 'N' ) ) {  */
					//Util::deb( $CvsDataRowValues, '$CvsDataRowValues::');
					//die("oii");
				//}
		    $UDF_IT_SWC_BEIGE= $CvsDataRowValues['UDF_IT_SWC_BEIGE'];
		    $UDF_IT_SWC_BLACK= $CvsDataRowValues['UDF_IT_SWC_BLACK'];
		    $UDF_IT_SWC_BLUE= $CvsDataRowValues['UDF_IT_SWC_BLUE'];
		    $UDF_IT_SWC_BROWN= $CvsDataRowValues['UDF_IT_SWC_BROWN'];
		    $UDF_IT_SWC_CAMO= $CvsDataRowValues['UDF_IT_SWC_CAMO'];
		    $UDF_IT_SWC_GOLD= $CvsDataRowValues['UDF_IT_SWC_GOLD'];
		    $UDF_IT_SWC_GRAY= $CvsDataRowValues['UDF_IT_SWC_GRAY'];
		    $UDF_IT_SWC_GREEN= $CvsDataRowValues['UDF_IT_SWC_GREEN'];
		    $UDF_IT_SWC_ORANGE= $CvsDataRowValues['UDF_IT_SWC_ORANGE'];
		    $UDF_IT_SWC_PINK= $CvsDataRowValues['UDF_IT_SWC_PINK'];
		    $UDF_IT_SWC_PURPLE= $CvsDataRowValues['UDF_IT_SWC_PURPLE'];
		    $UDF_IT_SWC_RED= $CvsDataRowValues['UDF_IT_SWC_RED'];
		    $UDF_IT_SWC_SILVER= $CvsDataRowValues['UDF_IT_SWC_SILVER'];
		    $UDF_IT_SWC_WHITE= $CvsDataRowValues['UDF_IT_SWC_WHITE'];
		    $UDF_IT_SWC_YELLOW= $CvsDataRowValues['UDF_IT_SWC_YELLOW'];


		//} // foreach( $DishesArray as $ColumnNumber=>$ColumnValue ) { // all columns in dish

//echo $TimeUpdated; exit;

		if ( $Enabled != 'Y' ) {
			$this->DeleteItemAsDisabled( $ItemCode );
			return false;
		}

		//Util::deb( $Enabled, ' $Enabled::' );
		if ( empty($ItemCode) ) return false;
		$lInventoryItem= InventoryItemPeer::getSimilarInventoryItem( $ItemCode, true );

		if ( !empty($lInventoryItem) ) { // there is Inventory Item with given sku($ItemCode)
			$WasModified= false;
			if ( strtotime($lInventoryItem->getDateUpdated()) < strtotime($DateUpdated) ) {
				$WasModified= true;
				//Util::deb( strtotime($lInventoryItem->getDateUpdated()), 'BIGGER  $lInventoryItem->getDateUpdated()::' );
				//Util::deb( strtotime($DateUpdated), 'BIGGER $DateUpdated::' );
			} else {
				if ( strtotime($lInventoryItem->getDateUpdated()) == strtotime($DateUpdated) ) {
					//Util::deb( $TimeUpdated, 'EQUAL  $TimeUpdated::' );
					if ( round($lInventoryItem->getTimeUpdated(),5) < round($TimeUpdated,5) ) {
						// Util::deb( $lInventoryItem->getTimeUpdated(), '+++  $lInventoryItem->getTimeUpdated()::' );
						$WasModified= true;
						// Util::deb( $WasModified, ' ++$WasModified::' );
						// Util::deb( $lInventoryItem->getSku(), '++lInventoryItem->getItemCode()::' );
					}
				}

			}

//$WasModified = true;  //debug

			if ( $WasModified ) { // this  Inventory Item was modified at servers datas
				//Util::deb( $ItemCode, '-1 $ItemCode::' );
				$lInventoryItem->setTitle($Title);

//echo "Modified SKU: $ItemCode<br />"; //debug

				//	$lInventoryItem->setBrand($Brand);
				//Util::deb( $Brand, ' --$Brand::' );
				if ( !empty($Brand) ) {
					$lBrand= BrandPeer::getSimilarBrand( trim($Brand), -1, false );
					// Util::deb( $lBrand, '-- $lBrand::' );
					if( empty($lBrand) ) {
						$lBrand= new Brand();
						$lBrand->setTitle( trim($Brand) );
						$lBrand->save();
					}
					// Util::deb( $lBrand->getId(),' -- $lBrand->getId()::' );
					$lInventoryItem->setBrandId( $lBrand->getId() );
					//if ( $lInventoryItem->getBrandId()== 0 ) die("NULL -1");
				}

				// Util::deb( $InventoryCategory, ' $InventoryCategory::' );
				//die("-1DIE");
				if ( empty($InventoryCategory) ) {
          //Util::deb( $InventoryCategory, '-1 $InventoryCategory::' );
  				$this->ItemsSkippedAsWithEmptyInventoryCategory++;
          return false;
        }
				$lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $InventoryCategory, /*'Category ' . */ /*$InventoryCategory*/'', /* 'Subcategory '.*//*$InventoryCategory*/'' );
        //Util::deb( $lInventoryCategory, ' -2 $lInventoryCategory::' );
				if( empty($lInventoryCategory) ) {
          //Util::deb( $lInventoryCategory, '-3 $lInventoryCategory::' );
    			$this->ItemsSkippedAsWithEmptyInventoryCategory++;
          return false;
				}

				// Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
				$lInventoryItem->setInventoryCategory( $lInventoryCategory->getProductLine() );
				//die("DIE");


				$lInventoryItem->setDescriptionShort($DescriptionShort);
				$lInventoryItem->setDescriptionLong($DescriptionLong);
				$lInventoryItem->setSize($Size);
				$lInventoryItem->setColor($Color);
				$lInventoryItem->setFinish($Finish);
				$lInventoryItem->setOptions($Options);
				$lInventoryItem->setGender($Gender);
				$lInventoryItem->setPaperwork($Paperwork);
				$lInventoryItem->setSizing($Sizing);
				$lInventoryItem->setCustomizable($Customizable);
				$lInventoryItem->setVideo($Video);
				$lInventoryItem->setDocuments($Documents);
				$lInventoryItem->setHand($Hand);

				$lInventoryItem->setMatrix( strtoupper($Matrix) == 'Y' );
				//Util::deb( $RootItem, ' $RootItem::' );
				if ( !empty($RootItem) ) {
				  $lInventoryItem->setRootItem($RootItem);
				}
				$lInventoryItem->setFeatures($Features);
				$lInventoryItem->setUnitMeasure($UnitMeasure);
				$lInventoryItem->setQtyOnHand($QtyOnHand);
				$lInventoryItem->setStdUnitPrice($StdUnitPrice);
				$lInventoryItem->setMsrp($Msrp);
				$lInventoryItem->setClearance($Clearance);
				if ( !empty($SaleStartDate) ) {
					$lInventoryItem->setSaleStartDate( strtotime($SaleStartDate) );
				} else {
					$lInventoryItem->setSaleStartDate( null );
				}
				if ( !empty($SaleEndDate) ) {
					$lInventoryItem->setSaleEndDate( strtotime($SaleEndDate) );
				} else {
					$lInventoryItem->setSaleEndDate( null );
				}
				$lInventoryItem->setSaleMethod($SaleMethod);
				$lInventoryItem->setSalePrice($SalePrice);
				$lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
				$lInventoryItem->setTaxClass($TaxClass);
				$lInventoryItem->setShipWeight($ShipWeight);
				$lInventoryItem->setSetupCharge( strtoupper($SetupCharge) == "Y");
				$lInventoryItem->setDateUpdated( strtotime($DateUpdated) );
				$lInventoryItem->setTimeUpdated( round($TimeUpdated,5) );
				
				$lInventoryItem->setAdditionalFreight($AdditionalFreight);
			// Util::deb( $FreeShipping, '-11 $FreeShipping::' );
				$lInventoryItem->setFreeShipping($FreeShipping);

			 //Util::deb( $PhoneOrder, '-11 $PhoneOrder::' );
        $lInventoryItem->setPhoneOrder($PhoneOrder);
				$lInventoryItem->setUdfItSwcBeige( strtolower($UDF_IT_SWC_BEIGE)=='y' );
				$lInventoryItem->setUdfItSwcBlack( strtolower($UDF_IT_SWC_BLACK)=='y' );
				$lInventoryItem->setUdfItSwcBlue( strtolower($UDF_IT_SWC_BLUE)=='y' );
				$lInventoryItem->setUdfItSwcBrown( strtolower($UDF_IT_SWC_BROWN)=='y' );
				$lInventoryItem->setUdfItSwcCamo( strtolower($UDF_IT_SWC_CAMO)=='y' );
				$lInventoryItem->setUdfItSwcGold( strtolower($UDF_IT_SWC_GOLD)=='y' );
				$lInventoryItem->setUdfItSwcGray( strtolower($UDF_IT_SWC_GRAY)=='y' );
				$lInventoryItem->setUdfItSwcGreen( strtolower($UDF_IT_SWC_GREEN)=='y' );
				$lInventoryItem->setUdfItSwcOrange( strtolower($UDF_IT_SWC_ORANGE)=='y' );
				$lInventoryItem->setUdfItSwcPink( strtolower($UDF_IT_SWC_PINK)=='y' );
				$lInventoryItem->setUdfItSwcPurple( strtolower($UDF_IT_SWC_PURPLE)=='y' );
				$lInventoryItem->setUdfItSwcRed( strtolower($UDF_IT_SWC_RED)=='y' );
				$lInventoryItem->setUdfItSwcSilver( strtolower($UDF_IT_SWC_SILVER)=='y' );
				$lInventoryItem->setUdfItSwcWhite( strtolower($UDF_IT_SWC_WHITE)=='y' );
				$lInventoryItem->setUdfItSwcYellow( strtolower($UDF_IT_SWC_YELLOW)=='y' );


				$lInventoryItem->save();
				$this->ItemsModified++;
			} else {  // if ( $WasModified ) { // this  Inventory Item was modified at servers datas
				// Util::deb( '-3  SkippedAsExising::');

        //echo "SKU: $ItemCode<br />"; //debug

				$this->ItemsSkippedAsExising++;
			}

		} else { // Add new Inventory Item
			// Util::deb('-2 Add new::');
			$lInventoryItem= new InventoryItem();
			$lInventoryItem->setSku($ItemCode);
			$lInventoryItem->setTitle($Title);

			//$lInventoryItem->setBrandId($BrandId);
			// Util::deb( $Brand, ' ++ $Brand::' );
			if ( !empty($Brand) ) {
				$lBrand= BrandPeer::getSimilarBrand( $Brand, -1, false );
				// Util::deb( $lBrand, ' ++$lBrand::' );
				if( empty($lBrand) ) {
					$lBrand= new Brand();
					$lBrand->setTitle( $Brand );
					//Util::deb( $lBrand, ' SAVING lBrand::' );
					$lBrand->save();
				}
				$lInventoryItem->setBrandId( $lBrand->getId() );
				//if ( $lInventoryItem->getBrandId()== 0 ) die("NULL -2");
			}


			// Util::deb( $InventoryCategory, ' $InventoryCategory::' );
      //Util::deb( $InventoryCategory, '-11 $InventoryCategory::' );
			if ( empty($InventoryCategory) ) {
        //Util::deb( $InventoryCategory, '-12 $InventoryCategory::' );
  			$this->ItemsSkippedAsWithEmptyInventoryCategory++;
        return false;
			}
			$lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $InventoryCategory, /*'Category '.*//*$InventoryCategory*/'', /*'Subcategory '.*//*$InventoryCategory*/ '' );
			//die("-1 DIE");
			if( empty( $lInventoryCategory ) ) {
        Util::deb( $lInventoryCategory, '-13 $lInventoryCategory::' );
  			$this->ItemsSkippedAsWithEmptyInventoryCategory++;
        return false;
			}
				// Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
			$lInventoryItem->setInventoryCategory( $lInventoryCategory->getProductLine() );

			//            $lInventoryItem->setInventoryCategory( $lInventoryCategory );
			$lInventoryItem->setDescriptionShort($DescriptionShort);
			$lInventoryItem->setDescriptionLong($DescriptionLong);
			$lInventoryItem->setSize($Size);
			$lInventoryItem->setColor($Color);
			$lInventoryItem->setFinish($Finish);
			$lInventoryItem->setOptions($Options);
			$lInventoryItem->setGender($Gender);
			$lInventoryItem->setPaperwork($Paperwork);
			$lInventoryItem->setSizing($Sizing);
			$lInventoryItem->setCustomizable($Customizable);
			$lInventoryItem->setVideo($Video);
			$lInventoryItem->setDocuments($Documents);
  		$lInventoryItem->setHand($Hand);

			$lInventoryItem->setMatrix( strtoupper($Matrix) == 'Y' );
			//Util::deb( $RootItem, ' -2 $RootItem::' );
			if ( !empty($RootItem) ) {
  			$lInventoryItem->setRootItem($RootItem);
			}
			$lInventoryItem->setFeatures($Features);
			$lInventoryItem->setUnitMeasure($UnitMeasure);
			$lInventoryItem->setQtyOnHand($QtyOnHand);
			$lInventoryItem->setStdUnitPrice($StdUnitPrice);
			$lInventoryItem->setMsrp($Msrp);
			$lInventoryItem->setClearance($Clearance);
			if ( !empty($SaleStartDate) ) {
				$lInventoryItem->setSaleStartDate( strtotime($SaleStartDate) );
			}
			if ( !empty($SaleEndDate) ) {
				$lInventoryItem->setSaleEndDate( strtotime($SaleEndDate) );
			}
			$lInventoryItem->setSaleMethod($SaleMethod);
			$lInventoryItem->setSalePrice($SalePrice);
			$lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
			$lInventoryItem->setTaxClass($TaxClass);
			$lInventoryItem->setShipWeight($ShipWeight);
			$lInventoryItem->setSetupCharge($SetupCharge);
			
			$lInventoryItem->setAdditionalFreight($AdditionalFreight);
			//Util::deb( $FreeShipping, '-2 $FreeShipping::' );
			$lInventoryItem->setFreeShipping($FreeShipping);

			// Util::deb( $PhoneOrder, '-2 $PhoneOrder::' );
			$lInventoryItem->setPhoneOrder($PhoneOrder);

			$lInventoryItem->setUdfItSwcBeige( strtolower($UDF_IT_SWC_BEIGE)=='y' );
			$lInventoryItem->setUdfItSwcBlack( strtolower($UDF_IT_SWC_BLACK)=='y' );
			$lInventoryItem->setUdfItSwcBlue( strtolower($UDF_IT_SWC_BLUE)=='y' );
			$lInventoryItem->setUdfItSwcBrown( strtolower($UDF_IT_SWC_BROWN)=='y' );
			$lInventoryItem->setUdfItSwcCamo( strtolower($UDF_IT_SWC_CAMO)=='y' );
			$lInventoryItem->setUdfItSwcGold( strtolower($UDF_IT_SWC_GOLD)=='y' );
			$lInventoryItem->setUdfItSwcGray( strtolower($UDF_IT_SWC_GRAY)=='y' );
			$lInventoryItem->setUdfItSwcGreen( strtolower($UDF_IT_SWC_GREEN)=='y' );
			$lInventoryItem->setUdfItSwcOrange( strtolower($UDF_IT_SWC_ORANGE)=='y' );
			$lInventoryItem->setUdfItSwcPink( strtolower($UDF_IT_SWC_PINK)=='y' );
			$lInventoryItem->setUdfItSwcPurple( strtolower($UDF_IT_SWC_PURPLE)=='y' );
			$lInventoryItem->setUdfItSwcRed( strtolower($UDF_IT_SWC_RED)=='y' );
			$lInventoryItem->setUdfItSwcSilver( strtolower($UDF_IT_SWC_SILVER)=='y' );
			$lInventoryItem->setUdfItSwcWhite( strtolower($UDF_IT_SWC_WHITE)=='y' );
			$lInventoryItem->setUdfItSwcYellow( strtolower($UDF_IT_SWC_YELLOW)=='y' );

      $lInventoryItem->setDateUpdated( strtotime($DateUpdated) );
			$lInventoryItem->setTimeUpdated( round($TimeUpdated,5) );

			$lInventoryItem->save();
			$this->ItemsAdded++;
		}

		$Debug= false;
		//Util::deb( $ItemCode, ' $ItemCode::' );
		if ( $ItemCode== '0-8001-065' ) {
			$Debug= true;
		}

		$sf_upload_dir= sfConfig::get('sf_upload_dir');
		$InventoryItemsThumbnails= sfConfig::get('app_application_InventoryItemsThumbnails');

		$ImportThumbnailImageFile=  AppUtils::getImportThumbnailImageBySku( $ItemCode, true );
		//if ( $Debug ) Util::deb( $ImportThumbnailImageFile, ' $ImportThumbnailImageFile::' );

		$DestFilename= $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . basename($ImportThumbnailImageFile);

		if ( !empty($ImportThumbnailImageFile) ) {
			$CopyRes= copy( $ImportThumbnailImageFile,  $DestFilename );
			if ( $CopyRes ) {
				$this->ImageThumbnailsAdded++;
			}
			chmod( $DestFilename, 0755 );
		}




		$InventoryItems= sfConfig::get('app_application_InventoryItems');
		$ImportImageFileArray=  AppUtils::getImportImagesBySku( $ItemCode, true );
		// if ( $Debug ) Util::deb( $ImportImageFileArray, ' $ImportImageFileArray::' );
		foreach( $ImportImageFileArray as $ImportImageFile ) {


			$DestFilename= $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . basename($ImportImageFile);
			// if ( $Debug ) Util::deb( $DestFilename, ' $DestFilename::' );

			if ( !empty($ImportImageFile) ) {
				$CopyRes= copy( $ImportImageFile,  $DestFilename );
				if ( $CopyRes ) {
					$this->ImagesAdded++;
				}
				chmod( $DestFilename, 0755 );
			}

		}

		return true;
	} //function ImportCvsData($CvsDataRow) {

	public function getInfoText() {
		$Res= '<hr>';
		$Res.= 'File loaded: <b>'. $this->InventoryItemsImportCVSFile.'</b>. <br><br>&nbsp;&nbsp;';
		$Res.= 'Inventory Items Added: <b>' . $this->ItemsAdded.'</b><br>&nbsp;&nbsp;'.
		'Inventory Items Modified: <b>' . $this->ItemsModified . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Skipped As Exising: <b>' . $this->ItemsSkippedAsExising . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Deleted As Disabled: <b>' . $this->ItemsDeletedAsDisabled . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Skipped As Non Existing: <b>' . $this->ItemsSkippedAsNonExisting . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Skipped As With Empty Inventory Category: <b>' . $this->ItemsSkippedAsWithEmptyInventoryCategory . '</b><br>&nbsp;&nbsp;'.


		'Inventory Categories Deleted As Empty: <b>' . $this->InventoryCategoriesDeletedAsEmpty . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Images Copied: <b>' . $this->ImagesAdded . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Image Thumbnails Copied: <b>' . $this->ImageThumbnailsAdded . '</b><br>&nbsp;&nbsp;';
		return $Res;
	}

	private function DeleteItemAsDisabled( $ItemCode ) {

		//Util::deb( $ItemCode, ' DeleteItemAsDisabled $ItemCode::' );
		$lInventoryItem= InventoryItemPeer::getSimilarInventoryItem( $ItemCode, true );
		//Util::deb( $lInventoryItem, ' $lInventoryItem::' );
		if ( !empty($lInventoryItem) ) {
		  $lInventoryItem->delete();
		  $this->ItemsDeletedAsDisabled++;
		  return true;
		} else {
		  $this->ItemsSkippedAsNonExisting++;
		  return false;
		}

	}


	private function DeleteNonExistingInventoryCategories() {
    return;
	  $InventoryCategoriesList= InventoryCategoryPeer::getInventoryCategories(1, false);
	  foreach( $InventoryCategoriesList as $lInventoryCategory ) {
	    $InventoryItemCount= InventoryItemPeer::getInventoryItemsByInventoryCategory( 1, false, true, $lInventoryCategory->getProductLine() );
	    if ( $InventoryItemCount == 0 ) {
  	    Util::deb( $lInventoryCategory->getProductLine(), '$lInventoryCategory->getProductLine()::' );
	      Util::deb( $InventoryItemCount, '$InventoryItemCount::' );
	      $lInventoryCategory->delete();
  		  $this->InventoryCategoriesDeletedAsEmpty++;
	    }

	  }
	  //die('DIE');
	}
}

?>
