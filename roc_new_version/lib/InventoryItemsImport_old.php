<?php

class InventoryItemsImport {
	private $InventoryItemsImportCVSFile='';
	private $ColumnsArray= array();
	private $InventaryItemsSkuArray= array();

	private $ItemsDeletedAsDisabled=0;
	private $ItemsSkippedAsNonExisting=0;
	private $ItemsAdded=0;
	private $ItemsModified=0;
	private $ItemsSkippedAsExising=0;
	private $ItemsDeletedAsNoneExising=0;
	private $ImagesAdded=0;
	private $ImageThumbnailsAdded=0;

	private function getColumnName($pColumnNumber ) {
		foreach( $this->ColumnsArray as $ColumnNumber=>$ColumnName ) {
			if ( (int)$pColumnNumber== (int)$ColumnNumber ) return $ColumnName;
		}
	}

	public function setInventoryItemsImportCVSFile($pInventoryItemsImportCVSFile) {
		$this->InventoryItemsImportCVSFile= $pInventoryItemsImportCVSFile;
	}

	private function ReadCSVData() {
		$CsvFileName= $this->InventoryItemsImportCVSFile;
		$ResArray= array();
		$reader = new sfCsvReader( $CsvFileName, '|' );
		//	Util::deb( $CsvFileName, ' $CsvFileName::' );
		//  echo '<pre> $CsvFileName ::'.print_r( $CsvFileName, true ).'</pre><br>';
		$reader->open();

		$I= 0;
		while ( $data = $reader->read() ) {
			if ( count($data) == 1 ) {
				continue;
			}
			$L= count($data);
			unset($data[$L-1]);
			unset($data[0]);
			$ResArray[]= $data;
			$I++;
			/*if( $I>= 50 ) { // FOR DEBUGGING
			$reader->close();
		//Util::deb( $ResArray, ' $ResArray::' );
		//die("DIE");
			return $ResArray;
			}  */
		}

		$reader->close();
		//Util::deb( $ResArray, ' $ResArray::' );
		//die("DIE");
		return $ResArray;
	} // private function ReadCSVData( $ParentSKU, $ProductId, $isPageSetsListByDirs ) {

	private function DeleteNonExistingInventoryItems() {
	$InventoryItemsList= InventoryItemPeer::getInventoryItems( 1, false, false, '', '', '', '', false, '', '', '', '', '', '', '', '', 'SKU', '' );
	foreach( $InventoryItemsList as $InventoryItem ) {
	if ( !in_array( $InventoryItem->getSku(), $this->InventaryItemsSkuArray ) ) {
	$InventoryItem->delete();
	$this->ItemsDeletedAsNoneExising++;
	}
	}
	return true;
	} 

	public function Run() {
		$CvsDataArray= $this->ReadCSVData( );
		$this->ColumnsArray= $CvsDataArray[0];
		$RowNumber= 1;
		foreach( $CvsDataArray as $CvsKey=>$CvsDataRowValues) {
			if ( $RowNumber== 1 ) {
				$RowNumber++;
				continue; // Skip Titles row
			}
			$this->ImportCvsData($CvsKey, $CvsDataRowValues);
			$RowNumber++;
		}
		// $this->DeleteNonExistingInventoryItems();
		return true;
	} // public function Run() {


	private function ImportCvsData($CvsKey, $CvsDataRowValues) {
		$ItemCode= '';
		$Title= '';
/*UDF_IT_MATRIX
UDF_IT_ROOTITEM
UDF_IT_FEATURES
*/		
		$Brand= '';
		$InventoryCategory= '';
		$DescriptionShort= '';
		$DescriptionLong= '';
		$Size= '';
		$Color= '';
		$Finish='';
		$Options= '';
		$Gender= '';
		$Paperwork= '';
		$Sizing= '';
		$Customizable= '';
		$Video= '';
		$Sizing= '';
		$Documents= '';

		$Matrix= '';
		$RootItem= '';		
		$Features= '';
		
		$UnitMeasure= '';
		$QtyOnHand= '';
		$StdUnitPrice= '';
		$Msrp= '';
		$Clearance= '';
		$SaleStartDate= '';
		$SaleEndDate= '';
		$SaleMethod= '';
		$SalePrice= '';
		$SalePromoDiscount= '';
		$TaxClass= '';
		$ShipWeight= '';
		$SetupCharge= '';
		$DateUpdated= '';
		$TimeUpdated= '';
		$Enabled= '';
		$this->InventaryItemsSkuArray[]= trim( $CvsDataRowValues[1] );
		foreach( $CvsDataRowValues as $ColumnNumber=>$ColumnValue ) { // all columns in Inventory Item
			$ColumnName= trim( $this->getColumnName($ColumnNumber) );
			$ColumnValue= trim($ColumnValue);

			if ( strtolower($ColumnName) == strtolower('ItemCode') ) {
				$ItemCode= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_DESCRIPTION') /*strtolower('ItemCodeDesc')*/ ) {
				$Title= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_BRAND') ) {
				$Brand= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('ProductLine') ) {
				$InventoryCategory= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_DESCRIPTION') ) {
				$DescriptionShort= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_LONG_DESCRI') ) {
				$DescriptionLong= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('Category1') ) {
				$Size= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('Category2')  ) {
				$Color= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('Category3')  ) { // !!! PLAIN, BASKET, NYLON LOOK, HI GLOSS
				$Finish= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('Category4') ) { // 62025SH, 0 RIDGE
				$Options= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_GENDER')  ) {
				$Gender= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_PAPERWORK_R')  ) {
				$Paperwork= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_SIZING_CHAR')  ) {
				$Sizing= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_CUSTOMIZABL')  ) {
				$Customizable= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_VIDEO')  ) {
				$Video= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_PRODUCT_DOC')  ) { // empty !!!
				$Documents= $ColumnValue;
			}

			if ( strtolower($ColumnName) == strtolower('UDF_IT_MATRIX')  ) { 
				$Matrix= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_ROOTITEM')  ) { 
				$RootItem= $ColumnValue;
			}			
			if ( strtolower($ColumnName) == strtolower('UDF_IT_FEATURES')  ) { 
				$Features= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SalesUnitOfMeasure')  ) { // EACH
				$UnitMeasure= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('TotalQuantityOnHan')  ) {
				$QtyOnHand= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('StandardUnitPrice')  ) {
				$StdUnitPrice= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SuggestedRetailPri')  ) { // !!! Float 2 decims
				$Msrp= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SalesPromotionCode')  ) { // !!! Float 2 decims
				$Clearance= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SaleStartingDate')  ) { // 10/31/2008
				$SaleStartDate= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SaleEndingDate')  ) {
				$SaleEndDate= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SaleMethod')  ) { // varcahr(1) ?
				$SaleMethod= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SalesPromotionPric')  ) {
				$SalePrice= $ColumnValue;
			}

			if ( strtolower($ColumnName) == strtolower('SalesPromotionDisc')  ) {
				$SalePromoDiscount= $ColumnValue; // !!  new field sale_promo_discount ?
			}
			if ( strtolower($ColumnName) == strtolower('TaxClass')  ) {
				$TaxClass= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('ShipWeight')  ) {
				$ShipWeight= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('SetupCharge')  ) {
				$SetupCharge= $ColumnValue; // varchar 10
			}

			if ( strtolower($ColumnName) == strtolower('DateUpdated')  ) {
				$DateUpdated= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('TimeUpdated')  ) {
				$TimeUpdated= $ColumnValue;
			}
			if ( strtolower($ColumnName) == strtolower('UDF_IT_ENABLED')  ) {
				$Enabled= $ColumnValue;
			}

		} // foreach( $DishesArray as $ColumnNumber=>$ColumnValue ) { // all columns in dish

		if ( $Enabled!= 'Y' ) {
			$this->DeleteItemAsDisabled( $ItemCode );
			return false;
		}

		//Util::deb( $Enabled, ' $Enabled::' );
		if ( empty($ItemCode) ) return false;
		$lInventoryItem= InventoryItemPeer::getSimilarInventoryItem( $ItemCode, true );
		if ( !empty($lInventoryItem) ) { // there is Inventory Item with given sku($ItemCode)
			$WasModified= false;
			if ( strtotime($lInventoryItem->getDateUpdated()) < strtotime($DateUpdated) ) {
				$WasModified= true;
				//Util::deb( strtotime($lInventoryItem->getDateUpdated()), 'BIGGER  $lInventoryItem->getDateUpdated()::' );
				//Util::deb( strtotime($DateUpdated), 'BIGGER $DateUpdated::' );
			} else {
				if ( strtotime($lInventoryItem->getDateUpdated()) == strtotime($DateUpdated) ) {
					//Util::deb( $TimeUpdated, 'EQUAL  $TimeUpdated::' );
					if ( round($lInventoryItem->getTimeUpdated(),5) < round($TimeUpdated,5) ) {
						// Util::deb( $lInventoryItem->getTimeUpdated(), '+++  $lInventoryItem->getTimeUpdated()::' );
						$WasModified= true;
						// Util::deb( $WasModified, ' ++$WasModified::' );
						// Util::deb( $lInventoryItem->getSku(), '++lInventoryItem->getItemCode()::' );
					}
				}

			}
			if ( $WasModified ) { // this  Inventory Item was modified at servers datas
				//Util::deb( $ItemCode, '-1 $ItemCode::' );
				$lInventoryItem->setTitle($Title);


				//	$lInventoryItem->setBrand($Brand);
				//Util::deb( $Brand, ' --$Brand::' );
				if ( !empty($Brand) ) {
					$lBrand= BrandPeer::getSimilarBrand( trim($Brand), -1, false );
					// Util::deb( $lBrand, '-- $lBrand::' );
					if( empty($lBrand) ) {
						$lBrand= new Brand();
						$lBrand->setTitle( trim($Brand) );
						$lBrand->save();
					}
					// Util::deb( $lBrand->getId(),' -- $lBrand->getId()::' );
					$lInventoryItem->setBrandId( $lBrand->getId() );
					//if ( $lInventoryItem->getBrandId()== 0 ) die("NULL -1");
				}

				// Util::deb( $InventoryCategory, ' $InventoryCategory::' );
				//die("-1DIE");
				if ( !empty($InventoryCategory) ) {
					$lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $InventoryCategory, /*'Category ' . */ /*$InventoryCategory*/'', /* 'Subcategory '.*//*$InventoryCategory*/'' );
					if( empty($lInventoryCategory) ) {
						$lInventoryCategory= new InventoryCategory();
						$lInventoryCategory->setProductLine( $InventoryCategory );
						$lInventoryCategory->setCategory( /*'Category '.*/$InventoryCategory );
						$lInventoryCategory->setSubcategory( /*'Subcategory '.*/$InventoryCategory );
						$lInventoryCategory->save();
					}
					// Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
					$lInventoryItem->setInventoryCategory( $lInventoryCategory->getProductLine() );
				}
				//die("DIE");


				$lInventoryItem->setDescriptionShort($DescriptionShort);
				$lInventoryItem->setDescriptionLong($DescriptionLong);
				$lInventoryItem->setSize($Size);
				$lInventoryItem->setColor($Color);
				$lInventoryItem->setFinish($Finish);
				$lInventoryItem->setOptions($Options);
				$lInventoryItem->setGender($Gender);
				$lInventoryItem->setPaperwork($Paperwork);
				$lInventoryItem->setSizing($Sizing);
				$lInventoryItem->setCustomizable($Customizable);
				$lInventoryItem->setVideo($Video);
				$lInventoryItem->setDocuments($Documents);

				$lInventoryItem->setMatrix( strtoupper($Matrix) == 'Y' );
				//Util::deb( $RootItem, ' $RootItem::' );
				if ( !empty($RootItem) ) {
				  $lInventoryItem->setRootItem($RootItem);				
				}
				$lInventoryItem->setFeatures($Features);
				$lInventoryItem->setUnitMeasure($UnitMeasure);
				$lInventoryItem->setQtyOnHand($QtyOnHand);
				$lInventoryItem->setStdUnitPrice($StdUnitPrice);
				$lInventoryItem->setMsrp($Msrp);
				$lInventoryItem->setClearance($Clearance);
				if ( !empty($SaleStartDate) ) {
					$lInventoryItem->setSaleStartDate( strtotime($SaleStartDate) );
				} else {
					$lInventoryItem->setSaleStartDate( null );
				}
				if ( !empty($SaleEndDate) ) {
					$lInventoryItem->setSaleEndDate( strtotime($SaleEndDate) );
				} else {
					$lInventoryItem->setSaleEndDate( null );
				}
				$lInventoryItem->setSaleMethod($SaleMethod);
				$lInventoryItem->setSalePrice($SalePrice);
				$lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
				$lInventoryItem->setTaxClass($TaxClass);
				$lInventoryItem->setShipWeight($ShipWeight);
				$lInventoryItem->setSetupCharge( strtoupper($SetupCharge) == "Y");
				$lInventoryItem->setDateUpdated( strtotime($DateUpdated) );
				$lInventoryItem->setTimeUpdated( round($TimeUpdated,5) );
				$lInventoryItem->save();
				$this->ItemsModified++;
			} else {  // if ( $WasModified ) { // this  Inventory Item was modified at servers datas
				// Util::deb( '-3  SkippedAsExising::');
				$this->ItemsSkippedAsExising++;
			}

		} else { // Add new Inventory Item
			// Util::deb('-2 Add new::');
			$lInventoryItem= new InventoryItem();
			$lInventoryItem->setSku($ItemCode);
			$lInventoryItem->setTitle($Title);

			//$lInventoryItem->setBrandId($BrandId);
			// Util::deb( $Brand, ' ++ $Brand::' );
			if ( !empty($Brand) ) {
				$lBrand= BrandPeer::getSimilarBrand( $Brand, -1, false );
				// Util::deb( $lBrand, ' ++$lBrand::' );
				if( empty($lBrand) ) {
					$lBrand= new Brand();
					$lBrand->setTitle( $Brand );
					//Util::deb( $lBrand, ' SAVING lBrand::' );
					$lBrand->save();
				}
				$lInventoryItem->setBrandId( $lBrand->getId() );
				//if ( $lInventoryItem->getBrandId()== 0 ) die("NULL -2");
			}


			// Util::deb( $InventoryCategory, ' $InventoryCategory::' );
			if ( !empty($InventoryCategory) ) {
				$lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $InventoryCategory, /*'Category '.*//*$InventoryCategory*/'', /*'Subcategory '.*//*$InventoryCategory*/ '' );
				//die("-1 DIE");
				if( empty($lInventoryCategory) ) {
					$lInventoryCategory= new InventoryCategory();
					$lInventoryCategory->setProductLine( $InventoryCategory );
					$lInventoryCategory->setCategory( /*'Category '.*/ $InventoryCategory );
					$lInventoryCategory->setSubcategory( /*'Subcategory '.*/ $InventoryCategory );
					$lInventoryCategory->save();
				}
				// Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
				$lInventoryItem->setInventoryCategory( $lInventoryCategory->getProductLine() );
			}
//die("-2 DIE");
				
			//            $lInventoryItem->setInventoryCategory( $lInventoryCategory );
			$lInventoryItem->setDescriptionShort($DescriptionShort);
			$lInventoryItem->setDescriptionLong($DescriptionLong);
			$lInventoryItem->setSize($Size);
			$lInventoryItem->setColor($Color);
			$lInventoryItem->setFinish($Finish);
			$lInventoryItem->setOptions($Options);
			$lInventoryItem->setGender($Gender);
			$lInventoryItem->setPaperwork($Paperwork);
			$lInventoryItem->setSizing($Sizing);
			$lInventoryItem->setCustomizable($Customizable);
			$lInventoryItem->setVideo($Video);
			$lInventoryItem->setDocuments($Documents);

			$lInventoryItem->setMatrix( strtoupper($Matrix) == 'Y' );
			//Util::deb( $RootItem, ' -2 $RootItem::' );
			if ( !empty($RootItem) ) {
  			$lInventoryItem->setRootItem($RootItem);			
			}
			$lInventoryItem->setFeatures($Features);
			$lInventoryItem->setUnitMeasure($UnitMeasure);
			$lInventoryItem->setQtyOnHand($QtyOnHand);
			$lInventoryItem->setStdUnitPrice($StdUnitPrice);
			$lInventoryItem->setMsrp($Msrp);
			$lInventoryItem->setClearance($Clearance);
			if ( !empty($SaleStartDate) ) {
				$lInventoryItem->setSaleStartDate( strtotime($SaleStartDate) );
			}
			if ( !empty($SaleEndDate) ) {
				$lInventoryItem->setSaleEndDate( strtotime($SaleEndDate) );
			}
			$lInventoryItem->setSaleMethod($SaleMethod);
			$lInventoryItem->setSalePrice($SalePrice);
			$lInventoryItem->setSalePromoDiscount($SalePromoDiscount);
			$lInventoryItem->setTaxClass($TaxClass);
			$lInventoryItem->setShipWeight($ShipWeight);
			$lInventoryItem->setSetupCharge($SetupCharge);
			$lInventoryItem->setDateUpdated( strtotime($DateUpdated) );
			$lInventoryItem->setTimeUpdated( round($TimeUpdated,5) );

			$lInventoryItem->save();
			$this->ItemsAdded++;
		}

		$Debug= false;
		//Util::deb( $ItemCode, ' $ItemCode::' );
		if ( $ItemCode== '0-8001-065' ) {
			$Debug= true;
		}

		$sf_upload_dir= sfConfig::get('sf_upload_dir');
		$InventoryItemsThumbnails= sfConfig::get('app_application_InventoryItemsThumbnails');

		$ImportThumbnailImageFile=  AppUtils::getImportThumbnailImageBySku( $ItemCode, true );
		//if ( $Debug ) Util::deb( $ImportThumbnailImageFile, ' $ImportThumbnailImageFile::' );

		$DestFilename= $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . basename($ImportThumbnailImageFile);

		if ( !empty($ImportThumbnailImageFile) ) {
			$CopyRes= copy( $ImportThumbnailImageFile,  $DestFilename );
			if ( $CopyRes ) {
				$this->ImageThumbnailsAdded++;
			}
			chmod( $DestFilename, 0755 );
		}




		$InventoryItems= sfConfig::get('app_application_InventoryItems');
		$ImportImageFileArray=  AppUtils::getImportImagesBySku( $ItemCode, true );
		// if ( $Debug ) Util::deb( $ImportImageFileArray, ' $ImportImageFileArray::' );
		foreach( $ImportImageFileArray as $ImportImageFile ) {


			$DestFilename= $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . basename($ImportImageFile);
			// if ( $Debug ) Util::deb( $DestFilename, ' $DestFilename::' );

			if ( !empty($ImportImageFile) ) {
				$CopyRes= copy( $ImportImageFile,  $DestFilename );
				if ( $CopyRes ) {
					$this->ImagesAdded++;
				}
				chmod( $DestFilename, 0755 );
			}

		}

		return true;
	} //function ImportCvsData($CvsDataRow) {

	public function getInfoText() {
		$Res= '<hr>';
		$Res.= 'File loaded: <b>'. $this->InventoryItemsImportCVSFile.'</b>. <br><br>&nbsp;&nbsp;';
		$Res.= 'Inventory Items Added: <b>' . $this->ItemsAdded.'</b><br>&nbsp;&nbsp;'.
		'Inventory Items Modified: <b>' . $this->ItemsModified . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Skipped As Exising: <b>' . $this->ItemsSkippedAsExising . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Deleted As Disabled: <b>' . $this->ItemsDeletedAsDisabled . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Skipped As Non Existing: <b>' . $this->ItemsSkippedAsNonExisting . '</b><br>&nbsp;&nbsp;'.
		//'Inventory Items Deleted As None Exising: <b>' . $this->ItemsDeletedAsNoneExising . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Images Copied: <b>' . $this->ImagesAdded . '</b><br>&nbsp;&nbsp;'.
		'Inventory Items Image Thumbnails Copied: <b>' . $this->ImageThumbnailsAdded . '</b><br>&nbsp;&nbsp;';
		return $Res;
	}

	private function DeleteItemAsDisabled( $ItemCode ) {
		
		//Util::deb( $ItemCode, ' DeleteItemAsDisabled $ItemCode::' );
		$lInventoryItem= InventoryItemPeer::getSimilarInventoryItem( $ItemCode, true );
		//Util::deb( $lInventoryItem, ' $lInventoryItem::' );
		if ( !empty($lInventoryItem) ) {
		  $lInventoryItem->delete();
		  $this->ItemsDeletedAsDisabled++;
		  return true;
		} else {
		  $this->ItemsSkippedAsNonExisting++;
		  return false;			
		}

	}
}

?>