<?php

//if (!defined('BASEPATH'))
//    exit('No direct script access allowed');


class PayPal {


  public function __construct($DeduggingPaypal=false)
  {
    $this->DeduggingPaypal= $DeduggingPaypal;
        //Util::deb($this->DeduggingPaypal, '$this->DeduggingPaypal::');
        // Параметры нашего запроса
        if ( $this->DeduggingPaypal ) {
          $this->_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';
        }
//die("INSIDE of _this(::");

  }
    /**
     * Последние сообщения об ошибках
     * @var array
     */
    protected $_errors = array();

    /**
     * Данные API
     * Обратите внимание на то, что для песочницы нужно использовать соответствующие данные
     * @var array
     */
    protected $_credentials = array(
    );
    /**
     * Указываем, куда будет отправляться запрос
     * Реальные условия - https://api-3t.paypal.com/nvp
     * Песочница - https://api-3t.sandbox.paypal.com/nvp  https://api.sandbox.paypal.com/nvp/
     * @var string
     */
    //protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';
    protected $_endPoint = 'https://api-3t.paypal.com/nvp';
    //protected $_endPoint = 'https://api.sandbox.paypal.com/nvp/';

    /**
     * Версия API
     * @var string
     */
    protected $_version = '74.0';
    protected $DeduggingPaypal=false;

    /**
     * Сформировываем запрос
     *
     * @param string $method Данные о вызываемом методе перевода
     * @param array $params Дополнительные параметры
     * @return array / boolean Response array / boolean false on failure
     */
    public function request($method,$params = array(), $is_debug= false ) {
			if ( $is_debug ) {
				$Paypal_USER = sfConfig::get('app_Paypal_Testing_USER');
				$Paypal_PWD = sfConfig::get('app_Paypal_Testing_PWD');
				$Paypal_SIGNATURE = sfConfig::get('app_Paypal_Testing_SIGNATURE');
 			} else {
        $Paypal_USER = sfConfig::get('app_Paypal_USER');
        $Paypal_PWD = sfConfig::get('app_Paypal_PWD');
        $Paypal_SIGNATURE = sfConfig::get('app_Paypal_SIGNATURE');
			}
      $this->_credentials = array(
        'USER' => $Paypal_USER,
        'PWD' => $Paypal_PWD,
        'SIGNATURE' => $Paypal_SIGNATURE
      );

      //if ( $is_debug ) echo '-41:: $params:: <pre>'. print_r( $params, true ) .'</pre><br>';
        $this->_errors = array();
        if( empty($method) ) {
            $this -> _errors = array('No Method Specified');
            return false;
        }

        // Параметры нашего запроса
        $requestParams = array(
            'METHOD' => $method,
            'VERSION' => $this -> _version
        ) + $this->_credentials;

        //echo "<pre>"; //debug
        //print_r($params); //debug
        //echo "</pre>"; //debug
        //exit; //debug

        // Сформировываем данные для NVP
        $request = http_build_query($requestParams + $params);

        // Настраиваем cURL
        $curlOptions = array (
            CURLOPT_URL => $this -> _endPoint,
            CURLOPT_VERBOSE => 1,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            //CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', // Файл сертификата
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $request
        );

        $ch = curl_init();
        curl_setopt_array($ch,$curlOptions);

        // Отправляем наш запрос, $response будет содержать ответ от API
        $response = curl_exec($ch);

        // Проверяем, нету ли ошибок в инициализации cURL
        if (curl_errno($ch)) {
            $this->_errors = curl_error($ch);
            curl_close($ch);
            return false;
        }
        else  {
            curl_close($ch);
            $responseArray = array();
            parse_str($response, $responseArray); // Разбиваем данные, полученные от NVP в массив
            return $responseArray;
        }
    }

    public function _this()
    {
        $url = $this->config->item('base_url');
//        Util::deb($this->DeduggingPaypal, '$this->DeduggingPaypal::');
        // Параметры нашего запроса
//die("INSIDE of _this(::");
        if ($this->uri->segment(2) == 'success') {
            $this->success();
        }
        else {
            $requestParams = array(
                'RETURNURL' => $url . 'testpaypal/success',
                'CANCELURL' => $url . 'testpaypal/cancelled'
            );

            $orderParams = array(
                'PAYMENTREQUEST_0_AMT' => '10.00',
                //'PAYMENTREQUEST_0_SHIPPINGAMT' => '1.00',
                'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
                'PAYMENTREQUEST_0_ITEMAMT' => '10.00'
            );

            $item = array(
                'L_PAYMENTREQUEST_0_NAME0' => 'subscription',
                'L_PAYMENTREQUEST_0_DESC0' => 'subscription on some plan',
                'L_PAYMENTREQUEST_0_AMT0' => '10.00',
                'L_PAYMENTREQUEST_0_QTY0' => '1'
            );

            $response = $this->request('SetExpressCheckout', $requestParams + $orderParams + $item);
            if (is_array($response) && $response['ACK'] == 'Success') { // Запрос был успешно принят
                $token = $response['TOKEN'];
                if ( $this->DeduggingPaypal ) {
                  header('Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token));
                } else {
                  header('Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token));
                }
                //header('Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token));
            }
        }
    }

    public function success()
    {
        if (isset($_GET['token']) && !empty($_GET['token'])) { // Токен присутствует
            // Получаем детали оплаты, включая информацию о покупателе.
            // Эти данные могут пригодиться в будущем для создания, к примеру, базы постоянных покупателей
            $checkoutDetails = $this->request('GetExpressCheckoutDetails', array('TOKEN' => $_GET['token']));
            // Завершаем транзакцию
            $requestParams = array(
                'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
                'PAYERID' => $_GET['PayerID'],
                'TOKEN'   => $_GET['token']
            );


            //----------------TEST---------------
            $orderParams = array(
                'PAYMENTREQUEST_0_AMT' => '10.00',
                //'PAYMENTREQUEST_0_SHIPPINGAMT' => '1.00',
                'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
                'PAYMENTREQUEST_0_ITEMAMT' => '10.00'
            );

            $item = array(
                'L_PAYMENTREQUEST_0_NAME0' => 'subscription',
                'L_PAYMENTREQUEST_0_DESC0' => 'subscription on some plan',
                'L_PAYMENTREQUEST_0_AMT0' => '10.00',
                'L_PAYMENTREQUEST_0_QTY0' => '1'
            );
            $response = $this->request('DoExpressCheckoutPayment', $requestParams + $orderParams + $item);
            if(is_array($response) && $response['ACK'] == 'Success') { // Оплата успешно проведена
                echo '<b>Here is the information that we can write into our database</b><br /><br />';
                // Выводим детали платежа
                echo '<b>Check out details</b> <br />';
                echo '<pre>';
                print_r($checkoutDetails);
                echo '</pre>';

                echo '<b>Payment data</b> <br />';
                // Выводим данные платежа
                echo '<pre>';
                print_r($response);
                echo '</pre>';

                exit;
            }
            else { // Иначе показываем ошибку
                echo 'Some error';exit;
            }
        }
    }

    public function cancelled()
    {
        echo 'Canceled'; exit;
    }
}