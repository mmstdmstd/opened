<?php

require 'lib/model/om/BaseOrderedItemPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'ordered_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class OrderedItemPeer extends BaseOrderedItemPeer {

  public static function getOrderedItems( $page=1, $ReturnPager=true, $OrderId= '', $Sorting='', $Limit= '' ) {
    //Util::deb($Sorting,'$Sorting::');
    $c = new Criteria();
    $c->add(OrderedItemPeer::ORDER_ID, $OrderId);
    $c->addDescendingOrderByColumn(OrderedItemPeer::CREATED_AT);

    if (!empty($Limit)) {
    	$c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return OrderedItemPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'OrderedItem', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

} // OrderedItemPeer
