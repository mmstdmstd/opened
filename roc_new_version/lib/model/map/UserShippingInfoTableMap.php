<?php


/**
 * This class defines the structure of the 'user_shipping_info' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class UserShippingInfoTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.UserShippingInfoTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('user_shipping_info');
		$this->setPhpName('UserShippingInfo');
		$this->setClassname('UserShippingInfo');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
		$this->addForeignKey('USER_ID', 'UserId', 'INTEGER', 'sf_guard_user', 'ID', true, null, null);
		$this->addColumn('B_NAME', 'BName', 'VARCHAR', true, 100, null);
		$this->addColumn('B_COUNTRY', 'BCountry', 'VARCHAR', true, 2, null);
		$this->addColumn('B_PHONE', 'BPhone', 'VARCHAR', true, 50, null);
		$this->addColumn('B_STREET', 'BStreet', 'VARCHAR', true, 50, null);
		$this->addColumn('B_STREET_2', 'BStreet2', 'VARCHAR', false, 50, null);
		$this->addColumn('B_CITY', 'BCity', 'VARCHAR', true, 50, null);
		$this->addColumn('B_STATE', 'BState', 'VARCHAR', true, 2, null);
		$this->addColumn('B_ZIP', 'BZip', 'VARCHAR', true, 10, null);
		$this->addColumn('S_NAME', 'SName', 'VARCHAR', true, 100, null);
		$this->addColumn('S_COUNTRY', 'SCountry', 'VARCHAR', true, 2, null);
		$this->addColumn('S_STREET', 'SStreet', 'VARCHAR', true, 50, null);
		$this->addColumn('S_STREET_2', 'SStreet2', 'VARCHAR', true, 50, null);
		$this->addColumn('S_CITY', 'SCity', 'VARCHAR', true, 50, null);
		$this->addColumn('S_STATE', 'SState', 'VARCHAR', true, 2, null);
		$this->addColumn('S_ZIP', 'SZip', 'VARCHAR', true, 10, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('sfGuardUser', 'sfGuardUser', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), 'RESTRICT', null);
	} // buildRelations()

	/**
	 * 
	 * Gets the list of behaviors registered for this table
	 * 
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
			'symfony' => array('form' => 'true', 'filter' => 'true', ),
		);
	} // getBehaviors()

} // UserShippingInfoTableMap
