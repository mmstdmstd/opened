<?php


/**
 * This class defines the structure of the 'inventory_item_paperwork' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class InventoryItemPaperworkTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.InventoryItemPaperworkTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('inventory_item_paperwork');
		$this->setPhpName('InventoryItemPaperwork');
		$this->setClassname('InventoryItemPaperwork');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
		$this->addForeignKey('INVENTORY_ITEM_ID', 'InventoryItemId', 'INTEGER', 'inventory_item', 'ID', true, null, null);
		$this->addForeignKey('PAPERWORK_ID', 'PaperworkId', 'INTEGER', 'paperwork', 'ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('InventoryItem', 'InventoryItem', RelationMap::MANY_TO_ONE, array('inventory_item_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('Paperwork', 'Paperwork', RelationMap::MANY_TO_ONE, array('paperwork_id' => 'id', ), 'RESTRICT', null);
	} // buildRelations()

	/**
	 * 
	 * Gets the list of behaviors registered for this table
	 * 
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
			'symfony' => array('form' => 'true', 'filter' => 'true', ),
		);
	} // getBehaviors()

} // InventoryItemPaperworkTableMap
