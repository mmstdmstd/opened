<?php


/**
 * This class defines the structure of the 'ordered_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class OrderedItemTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.OrderedItemTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('ordered_item');
		$this->setPhpName('OrderedItem');
		$this->setClassname('OrderedItem');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
		$this->addForeignKey('ORDER_ID', 'OrderId', 'INTEGER', 'ordered', 'ID', true, null, null);
		$this->addColumn('INVENTORY_ITEM_ID', 'InventoryItemId', 'INTEGER', true, null, null);
		$this->addColumn('SKU', 'Sku', 'VARCHAR', false, 30, null);
		$this->addColumn('QTY', 'Qty', 'INTEGER', true, 4, null);
		$this->addColumn('UNIT_PRICE', 'UnitPrice', 'FLOAT', true, 9,2, null);
		$this->addColumn('TITLE', 'Title', 'VARCHAR', true, 100, null);
		$this->addColumn('SIZE', 'Size', 'VARCHAR', false, 10, null);
		$this->addColumn('COLOR', 'Color', 'VARCHAR', false, 7, null);
		$this->addColumn('OPTIONS', 'Options', 'LONGVARCHAR', false, null, null);
		$this->addColumn('FINISH', 'Finish', 'VARCHAR', false, 20, null);
		$this->addColumn('HAND', 'Hand', 'VARCHAR', false, 50, null);
		$this->addColumn('GENDER', 'Gender', 'VARCHAR', false, 10, null);
		$this->addForeignKey('MEASURE_ID', 'MeasureId', 'INTEGER', 'measures', 'ID', false, null, null);
		$this->addColumn('SHIP_WEIGHT', 'ShipWeight', 'VARCHAR', false, 10, null);
		$this->addColumn('BACKORDER', 'Backorder', 'BOOLEAN', false, null, false);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', true, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Ordered', 'Ordered', RelationMap::MANY_TO_ONE, array('order_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('Measures', 'Measures', RelationMap::MANY_TO_ONE, array('measure_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('OrderedItemOptionsItems', 'OrderedItemOptionsItems', RelationMap::ONE_TO_MANY, array('id' => 'ordered_item_id', ), 'RESTRICT', null);
	} // buildRelations()

	/**
	 * 
	 * Gets the list of behaviors registered for this table
	 * 
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
			'symfony' => array('form' => 'true', 'filter' => 'true', ),
			'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // OrderedItemTableMap
