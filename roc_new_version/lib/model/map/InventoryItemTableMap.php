<?php


/**
 * This class defines the structure of the 'inventory_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class InventoryItemTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.InventoryItemTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('inventory_item');
		$this->setPhpName('InventoryItem');
		$this->setClassname('InventoryItem');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addColumn('ID', 'Id', 'INTEGER', true, null, null);
		$this->addPrimaryKey('SKU', 'Sku', 'VARCHAR', true, 30, null);
		$this->addColumn('TITLE', 'Title', 'VARCHAR', true, 100, null);
		$this->addForeignKey('BRAND_ID', 'BrandId', 'INTEGER', 'brand', 'ID', false, null, null);
		$this->addColumn('DESCRIPTION_SHORT', 'DescriptionShort', 'VARCHAR', true, 255, null);
		$this->addColumn('DESCRIPTION_LONG', 'DescriptionLong', 'LONGVARCHAR', false, null, null);
		$this->addColumn('SIZE', 'Size', 'VARCHAR', false, 10, null);
		$this->addColumn('COLOR', 'Color', 'VARCHAR', false, 7, null);
		$this->addColumn('GENDER', 'Gender', 'VARCHAR', false, 10, null);
		$this->addForeignKey('SIZING_ID', 'SizingId', 'INTEGER', 'sizings', 'ID', false, null, null);
		$this->addColumn('CUSTOMIZABLE', 'Customizable', 'BOOLEAN', false, null, false);
		$this->addColumn('VIDEO', 'Video', 'VARCHAR', false, 100, null);
		$this->addForeignKey('MEASURE_ID', 'MeasureId', 'INTEGER', 'measures', 'ID', false, null, null);
		$this->addColumn('QTY_ON_HAND', 'QtyOnHand', 'INTEGER', false, 4, null);
		$this->addColumn('STD_UNIT_PRICE', 'StdUnitPrice', 'FLOAT', false, 9,2, null);
		$this->addColumn('MSRP', 'Msrp', 'FLOAT', false, 9,2, null);
		$this->addColumn('CLEARANCE', 'Clearance', 'FLOAT', false, 9,2, null);
		$this->addColumn('SALE_START_DATE', 'SaleStartDate', 'DATE', false, null, null);
		$this->addColumn('SALE_END_DATE', 'SaleEndDate', 'DATE', false, null, null);
		$this->addColumn('SALE_METHOD', 'SaleMethod', 'VARCHAR', true, 30, null);
		$this->addColumn('SALE_PRICE', 'SalePrice', 'FLOAT', false, 9,2, null);
		$this->addColumn('SALE_PERCENT', 'SalePercent', 'FLOAT', false, 3,1, null);
		$this->addColumn('TAX_CLASS', 'TaxClass', 'VARCHAR', false, 10, null);
		$this->addColumn('SHIP_WEIGHT', 'ShipWeight', 'VARCHAR', false, 10, null);
		$this->addColumn('SALE_PROMO_DISCOUNT', 'SalePromoDiscount', 'FLOAT', false, 3,1, null);
		$this->addColumn('FINISH', 'Finish', 'VARCHAR', false, 20, null);
		$this->addColumn('DOCUMENTS', 'Documents', 'VARCHAR', false, 50, null);
		$this->addColumn('HAND', 'Hand', 'VARCHAR', false, 50, null);
		$this->addColumn('MATRIX', 'Matrix', 'BOOLEAN', false, null, false);
		$this->addColumn('ROOTITEM', 'Rootitem', 'VARCHAR', false, 30, null);
		$this->addColumn('FEATURES', 'Features', 'VARCHAR', false, 255, null);
		$this->addColumn('SETUP_CHARGE', 'SetupCharge', 'BOOLEAN', false, null, false);
		$this->addColumn('DATE_UPDATED', 'DateUpdated', 'TIMESTAMP', true, null, null);
		$this->addColumn('TIME_UPDATED', 'TimeUpdated', 'FLOAT', true, 9,5, 0);
		$this->addColumn('RECENTLY_VIEWED', 'RecentlyViewed', 'TIMESTAMP', false, null, null);
		$this->addColumn('ADDITIONAL_FREIGHT', 'AdditionalFreight', 'FLOAT', false, 9,2, null);
		$this->addColumn('FREE_SHIPPING', 'FreeShipping', 'BOOLEAN', false, null, false);
		$this->addColumn('PHONE_ORDER', 'PhoneOrder', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_BEIGE', 'UdfItSwcBeige', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_BLACK', 'UdfItSwcBlack', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_BLUE', 'UdfItSwcBlue', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_BROWN', 'UdfItSwcBrown', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_CAMO', 'UdfItSwcCamo', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_GOLD', 'UdfItSwcGold', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_GRAY', 'UdfItSwcGray', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_GREEN', 'UdfItSwcGreen', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_ORANGE', 'UdfItSwcOrange', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_PINK', 'UdfItSwcPink', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_PURPLE', 'UdfItSwcPurple', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_RED', 'UdfItSwcRed', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_SILVER', 'UdfItSwcSilver', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_WHITE', 'UdfItSwcWhite', 'BOOLEAN', false, null, false);
		$this->addColumn('UDF_IT_SWC_YELLOW', 'UdfItSwcYellow', 'BOOLEAN', false, null, false);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', true, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Brand', 'Brand', RelationMap::MANY_TO_ONE, array('brand_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('Sizings', 'Sizings', RelationMap::MANY_TO_ONE, array('sizing_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('Measures', 'Measures', RelationMap::MANY_TO_ONE, array('measure_id' => 'id', ), 'RESTRICT', null);
    $this->addRelation('OrderedItemOptionsItems', 'OrderedItemOptionsItems', RelationMap::ONE_TO_MANY, array('id' => 'inventory_item_id', ), 'RESTRICT', null);
    $this->addRelation('InventoryItemPaperwork', 'InventoryItemPaperwork', RelationMap::ONE_TO_MANY, array('id' => 'inventory_item_id', ), 'RESTRICT', null);
    $this->addRelation('InventoryItemInventoryCategory', 'InventoryItemInventoryCategory', RelationMap::ONE_TO_MANY, array('id' => 'inventory_item_id', ), 'RESTRICT', null);
    $this->addRelation('OptionsItems', 'OptionsItems', RelationMap::ONE_TO_MANY, array('id' => 'inventory_item_id', ), 'RESTRICT', null);
    $this->addRelation('OptionsValue', 'OptionsValue', RelationMap::ONE_TO_MANY, array('id' => 'inventory_item_id', ), 'RESTRICT', null);
	} // buildRelations()

	/**
	 * 
	 * Gets the list of behaviors registered for this table
	 * 
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
			'symfony' => array('form' => 'true', 'filter' => 'true', ),
			'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // InventoryItemTableMap
