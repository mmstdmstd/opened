<?php

require 'lib/model/om/BaseOptionsItemsPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'options_items' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class OptionsItemsPeer extends BaseOptionsItemsPeer {

	public static function getOptionsItems( $page=1, $ReturnPager=true, $inventory_item_id, $Sorting='CODE' ) {
		$c = new Criteria();

		//Util::deb($Sorting, '$Sorting::');
		$c->addJoin( OptionsItemsPeer::OPTION_ID, OptionsPeer::ID, Criteria::LEFT_JOIN);
		$c->add( OptionsItemsPeer::INVENTORY_ITEM_ID, $inventory_item_id);

//		$c->addAscendingOrderByColumn(OptionsItemsPeer::ORDERING );
		$c->addAscendingOrderByColumn(OptionsPeer::TITLE );

		if ( !$ReturnPager ) {
			return OptionsItemsPeer::doSelect( $c );
		}
		$pager = new sfPropelPager( 'OptionsItem', (int)sfConfig::get('app_application_rows_in_pager' ) );
		$pager->setPage( $page );
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

} // OptionsItemsPeer
