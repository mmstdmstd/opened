<?php

require 'lib/model/om/BaseInventoryCategoryPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'inventory_category' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryCategoryPeer extends BaseInventoryCategoryPeer {

    
    public static function getInventoryCategories( $page=1, $ReturnPager=true, $filter_product_line='', $Sorting='PRODUCT_LINE' ) {
    $c = new Criteria();
    if ( !empty($filter_product_line) and $filter_product_line!= '-' ) {
      $c->add( InventoryCategoryPeer::PRODUCT_LINE , '%'.$filter_product_line.'%', Criteria::LIKE );
    }
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(InventoryCategoryPeer::CREATED_AT);
    }
    if ( $Sorting=='PRODUCT_LINE' ) {
      $c->addAscendingOrderByColumn(InventoryCategoryPeer::PRODUCT_LINE );
    }
    if ( !$ReturnPager ) {
      return InventoryCategoryPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'InventoryCategory', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarInventoryCategory($InventoryCategoryProductLine, $Category='', $Subcategory='' , $con = null)
  {
    $c = new Criteria();
    $c->add( InventoryCategoryPeer::PRODUCT_LINE , $InventoryCategoryProductLine );
    if ( !empty($Category) ) {
      $c->add( InventoryCategoryPeer::CATEGORY, $Category );
    }
    if ( !empty($Subcategory) ) {
      $c->add( InventoryCategoryPeer::SUBCATEGORY , $Subcategory );
    }
    if($lResult = InventoryCategoryPeer::doSelectOne( $c, $con ) )
    {
      return $lResult;
    }
    return '';
  }

  public static function getSimilarInventoryCategoryBySubcategory($Subcategory, $con = null)
  {
    $c = new Criteria();
    $c->add( InventoryCategoryPeer::SUBCATEGORY , $Subcategory );
    if($lResult = InventoryCategoryPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';    
  }
  
  public static function getSelectionList( $Code, $ProductLine, $NewCode='', $NewCodeText='' ) {
    $List= InventoryCategoryPeer::getInventoryCategories( 1, false );
    $ResArray= array();
    if ( !empty($Code) or !empty($ProductLine) ) {
      $ResArray[$Code]= $ProductLine;
    }
    foreach( $List as $lInventoryCategory ) {
      $ResArray[ $lInventoryCategory->getId() ]= $lInventoryCategory->getProductLine();
    }
    if ( !empty($NewCode) and !empty($NewCodeText) ) {
      $ResArray[ $NewCode ]= $NewCodeText;      
    }
    return $ResArray;
  }
    
  public static function getCategoriesList( $HideEmptyCategories= false ) {
    $c = new Criteria();    
    $c->add( InventoryCategoryPeer::CATEGORY, '', Criteria::NOT_EQUAL  );
    $c->add( InventoryCategoryPeer::ID, 1370, Criteria::NOT_EQUAL  );    // Жопа	
    $c->addAscendingOrderByColumn(InventoryCategoryPeer::CATEGORY );
    $c->setDistinct();
    $c->addSelectColumn('category');
    $c->addSelectColumn('product_line');
    $ResArray= array();
    $stmt= InventoryCategoryPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    // Util::deb( $HideEmptyCategories, '$HideEmptyCategories' );
    while($result) {
      if ( !empty($result['category']) ) {
        
        // Util::deb( $result, '$result::' );
        /*$InventoryItemsCount= 1;
        if ( $HideEmptyCategories ) {
          $InventoryItemsCount= InventoryItemPeer::getInventoryItemsCountByInventoryCategory( $result['category'] );
           Util::deb( $InventoryItemsCount, '$InventoryItemsCount::' );
        }
        if ( $InventoryItemsCount > 0 ) { */
        $ResArray[ $result['category'] ]= $result['category'];
        //}
      }
      $result = $stmt->fetch();
    }
    return $ResArray;
  }
  
  
  public static function getSubcategoriesList( $category= '', $HideEmptySubcategories= false ) {
    $c = new Criteria();    
    $c->add( InventoryCategoryPeer::SUBCATEGORY, '', Criteria::NOT_EQUAL  );
    if ( !empty($category) ) {
      $c->add( InventoryCategoryPeer::CATEGORY, $category  );    	
    }
    $c->add( InventoryCategoryPeer::ID, 1370, Criteria::NOT_EQUAL  );    // Жопа 	
    $c->addAscendingOrderByColumn(InventoryCategoryPeer::CATEGORY );
    $c->setDistinct();
    $c->addSelectColumn('subcategory');
    $c->addSelectColumn('product_line');
    $ResArray= array();
    $stmt= InventoryCategoryPeer::doSelectStmt( $c );
    $result = $stmt->fetch();
    // Util::deb( $HideEmptySubcategories, '$HideEmptySubcategories' );
    while($result) {
      if ( !empty($result['subcategory']) ) {

        /*$InventoryItemsCount= 1;
        if ( $HideEmptySubcategories ) {
          $InventoryItemsCount= InventoryItemPeer::getInventoryItemsByInventorySubcategory( 1, false, true, $result['product_line'] );
          // Util::deb( $InventoryItemsCount, '$InventoryItemsCount::' );
        }
        if ( $InventoryItemsCount > 0 ) {        */
        $ResArray[ $result['subcategory'] ]= $result['subcategory'];
        //}
        
      }
      $result = $stmt->fetch();
    }
    return $ResArray;
  }
  
} // InventoryCategoryPeer
