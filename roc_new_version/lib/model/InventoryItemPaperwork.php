<?php

require 'lib/model/om/BaseInventoryItemPaperwork.php';


/**
 * Skeleton subclass for representing a row from the 'inventory_item_paperwork' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItemPaperwork extends BaseInventoryItemPaperwork {

} // InventoryItemPaperwork
