<?php

require 'lib/model/om/BasePaperworkPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'paperwork' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class PaperworkPeer extends BasePaperworkPeer {
  
  public static function getPaperworks( $page=1, $ReturnPager=true, $Limit= '', $Sorting='ORDERING' ) {
    $c = new Criteria();
    //$c->addAscendingOrderByColumn(PaperworkPeer::ORDERING );

		//Util::deb($Sorting, '$Sorting::');
		if ( $Sorting=='KEY' ) {
			$c->addAscendingOrderByColumn(PaperworkPeer::KEY );
		}
		if ( $Sorting=='TITLE' ) {
			$c->addAscendingOrderByColumn(PaperworkPeer::TITLE );
		}
		if ( $Sorting=='ORDERING' or empty($Sorting) ) {
			$c->addAscendingOrderByColumn(PaperworkPeer::ORDERING );
		}
		if ( $Sorting=='DESCRIPTION' ) {
			$c->addAscendingOrderByColumn(PaperworkPeer::DESCRIPTION );
		}

    if ( !empty($Limit) ) {
      $c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return PaperworkPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'Paperwork', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

	public static function getPaperworkByCode( $code ) {
		$c = new Criteria();
		$c->add( PaperworkPeer::CODE , $code );
		return PaperworkPeer::doSelectOne( $c );
	}

} // PaperworkPeer
