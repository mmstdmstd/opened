<?php

require 'lib/model/om/BaseEmailSubmissionPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'email_submission' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class EmailSubmissionPeer extends BaseEmailSubmissionPeer {

	public static function getEmailSubmissions( $page=1, $ReturnPager=true, $Sorting='EMAIL' ) {
    $c = new Criteria();
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(EmailSubmissionPeer::CREATED_AT);
    }
    if ( $Sorting=='EMAIL' ) {
      $c->addAscendingOrderByColumn(EmailSubmissionPeer::EMAIL );
    }
    if ( !$ReturnPager ) {
      return EmailSubmissionPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'EmailSubmission', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarEmailSubmission($Email, $con = null)
  {
    $c = new Criteria();
    $c->add( EmailSubmissionPeer::EMAIL , $Email );
    return EmailSubmissionPeer::doSelectOne( $c, $con );
  }

} // EmailSubmissionPeer
