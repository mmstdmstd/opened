<?php

require 'lib/model/om/BaseUniquesSkuListPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'uniques_sku_list' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class UniquesSkuListPeer extends BaseUniquesSkuListPeer {



	public static function getUniquesSkuList($page = 1, $ReturnPager = true, $ReturnCount= false, $unique_session_id='', $rows_in_pager = '', $Sorting = '' )
	{
		// Util::deb( $Sorting, ' getInventory_Items_Left_sidebar_test $Sorting::' );
		$c = new Criteria();
		$c->add( UniquesSkuListPeer::UNIQUE_SESSION_ID, $unique_session_id);


		if ($ReturnCount) {
			return UniquesSkuListPeer::doCount($c);
		}
		if (!$ReturnPager) {
			return UniquesSkuListPeer::doSelect($c);
		}
		if (empty($rows_in_pager)) $rows_in_pager = (int)sfConfig::get('app_application_rows_in_pager');
		$pager = new sfNPropelPager('UniquesSkuList', $rows_in_pager);
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	} // public static function getInventory_Items_Left_sidebar_ByUniquesSkuList($page = 1, $ReturnPager = true, $rows_in_pager = '', $Sorting = '' )


	public static function getSimilarUniquesSkuList( $unique_session_id, $sku )
	{
		$c = new Criteria();
		$c->add( UniquesSkuListPeer::UNIQUE_SESSION_ID , $unique_session_id );
		$c->add( UniquesSkuListPeer::SKU, $sku );
		if($lResult = UniquesSkuListPeer::doSelectOne( $c ) )
		{
			return $lResult;
		}
		return '';
	}

	public static function DeleteSimilarUniquesBySeconds( $Seconds= 60 )
	{
		$c = new Criteria();
		$c->add( UniquesSkuListPeer::CREATED_AT, time() - $Seconds, Criteria::LESS_THAN );
	  UniquesSkuListPeer::doDelete( $c );
	}

} // UniquesSkuListPeer
