<?php

require 'lib/model/om/BaseYoutubeVideoPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'youtube_video' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class YoutubeVideoPeer extends BaseYoutubeVideoPeer {

    public static function getYoutubeVideos( $page=1, $ReturnPager=true, $Sorting='PRODUCT_LINE', $Limit= '' ) {
    $c = new Criteria();
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(YoutubeVideoPeer::CREATED_AT);
    }
    if ( $Sorting=='KEY' ) {
      $c->addAscendingOrderByColumn(YoutubeVideoPeer::KEY );
    }
    if ( $Sorting=='TITLE' ) {
      $c->addAscendingOrderByColumn(YoutubeVideoPeer::TITLE );
    }
    if ( $Sorting=='ORDERING' ) {
      $c->addAscendingOrderByColumn(YoutubeVideoPeer::ORDERING );
    }
    if ( $Sorting=='DESCRIPTION' ) {
      $c->addAscendingOrderByColumn(YoutubeVideoPeer::DESCRIPTION );
    }
    if (!empty($Limit)) {
    	$c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return YoutubeVideoPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'YoutubeVideo', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarYoutubeVideo($YoutubeVideoKey, $con = null)
  {
    $c = new Criteria();
    $c->add( YoutubeVideoPeer::KEY , $YoutubeVideoKey );
    if($lResult = YoutubeVideoPeer::doSelectOne( $c, $con ) )
    {
      return $lResult;
    }
    return '';
  }

} // YoutubeVideoPeer