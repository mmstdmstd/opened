<?php

require 'lib/model/om/BaseImportRulesPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'import_rules' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ImportRulesPeer extends BaseImportRulesPeer {


	// 		ImportRulesPeer::addImportRulesValue( 'ImportMatrixItemSizes', $this->ItemsAdded . ' rows uploaded at ' .strftime('%Y-%m-%d %H:%M') );
	public static function addImportRulesValue( $KeyName, $KeyValue )
	{
		$c = new Criteria();
		$c->add( ImportRulesPeer::KEY_NAME , $KeyName );
		$lImportRules = ImportRulesPeer::doSelectOne( $c );
		if ( empty($lImportRules) ) {
			$lImportRules = new ImportRules();
			$lImportRules->setKeyName($KeyName);
		}
		$lImportRules->setKeyValue($KeyValue);
		$lImportRules->save();
	}

	public static function getImportRulesByKey( $KeyName )
	{
		$c = new Criteria();
		$c->add( ImportRulesPeer::KEY_NAME , $KeyName );
		return ImportRulesPeer::doSelectOne( $c );
	}


} // ImportRulesPeer
