<?php

require 'lib/model/om/BaseOptionsPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'options' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class OptionsPeer extends BaseOptionsPeer {


	public static function addOptionsValue( $KeyName, $KeyValue )
	{
		$c = new Criteria();
		$c->add( OptionsPeer::KEY_NAME , $KeyName );
		$lOptions = OptionsPeer::doSelectOne( $c );
		if ( empty($lOptions) ) {
			$lOptions = new Options();
			$lOptions->setKeyName($KeyName);
		}
		$lOptions->setKeyValue($KeyValue);
		$lOptions->save();
	}

	public static function getOptionByKey( $KeyName )
	{
		$c = new Criteria();
		$c->add( OptionsPeer::KEY_NAME , $KeyName );
		return OptionsPeer::doSelectOne( $c );
	}

} // OptionsPeer
