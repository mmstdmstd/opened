<?php

require 'lib/model/om/BaseInventoryItemPaperworkPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'inventory_item_paperwork' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItemPaperworkPeer extends BaseInventoryItemPaperworkPeer {


	public static function getInventoryItemPaperworks( $page=1, $ReturnPager=true, $inventory_item_id, $Sorting='CODE' ) {
		$c = new Criteria();
		//$c->addAscendingOrderByColumn(InventoryItemPaperworkPeer::ORDERING );

		//Util::deb($Sorting, '$Sorting::');
		$c->addJoin(InventoryItemPaperworkPeer::PAPERWORK_ID, PaperworkPeer::ID, Criteria::LEFT_JOIN);
		$c->add( InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $inventory_item_id);

	  $c->addAscendingOrderByColumn(PaperworkPeer::ORDERING );

		if ( !$ReturnPager ) {
			return InventoryItemPaperworkPeer::doSelect( $c );
		}
		$pager = new sfPropelPager( 'InventoryItemPaperwork', (int)sfConfig::get('app_application_rows_in_pager' ) );
		$pager->setPage( $page );
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

} // InventoryItemPaperworkPeer
