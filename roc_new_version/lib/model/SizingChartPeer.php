<?php

require 'lib/model/om/BaseSizingChartPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'sizing_chart' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class SizingChartPeer extends BaseSizingChartPeer {

  public static function getSizingCharts( $page=1, $ReturnPager=true, $Limit= '', $Sorting='PRODUCT_LINE' ) {
    $c = new Criteria();
		if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
			$c->addDescendingOrderByColumn(SizingChartPeer::CREATED_AT);
		}
		if ( $Sorting=='KEY' ) {
			$c->addAscendingOrderByColumn(SizingChartPeer::KEY );
		}
		if ( $Sorting=='TITLE' ) {
			$c->addAscendingOrderByColumn(SizingChartPeer::TITLE );
		}
		if ( $Sorting=='ORDERING' ) {
			$c->addAscendingOrderByColumn(SizingChartPeer::ORDERING );
		}
		if ( $Sorting=='DESCRIPTION' ) {
			$c->addAscendingOrderByColumn(SizingChartPeer::DESCRIPTION );
		}



    if ( !empty($Limit) ) {
      $c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return SizingChartPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'SizingChart', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

} // SizingChartPeer
