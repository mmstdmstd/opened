<?php

require 'lib/model/om/BaseDocumentPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'document' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class DocumentPeer extends BaseDocumentPeer {

	// $this->DocumentsPager = DocumentPeer::getDocuments($this->page, true, $this->sorting);
  public static function getDocuments( $page=1, $ReturnPager=true, $Limit= '', $Sorting='TITLE' ) {
    $c = new Criteria();

		if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
			$c->addDescendingOrderByColumn(DocumentPeer::CREATED_AT);
		}
		if ( $Sorting=='UPDATED_AT' ) {
			$c->addDescendingOrderByColumn(DocumentPeer::UPDATED_AT);
		}
		if ( $Sorting=='TITLE' ) {
			$c->addAscendingOrderByColumn(DocumentPeer::TITLE );
		}
		if ( $Sorting=='KEY' ) {
			$c->addAscendingOrderByColumn(DocumentPeer::KEY );
		}
		if ( $Sorting=='ORDERING' ) {
			$c->addAscendingOrderByColumn(DocumentPeer::ORDERING );
		}

    if ( !empty($Limit) ) {
      $c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return DocumentPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'Document', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarDocument($DocumentKey, $con = null)
  {
    $c = new Criteria();
    $c->add( DocumentPeer::KEY , $DocumentKey );
    if($lResult = DocumentPeer::doSelectOne( $c, $con ) )
    {
      return $lResult;
    }
    return '';
  }

} // DocumentPeer
