<?php

require 'lib/model/om/BaseOrderedPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'ordered' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class OrderedPeer extends BaseOrderedPeer {
  public static $StatusChoices= array( 'D'=>'Draft', 'N'=>'New', 'O'=>'Opened', 'P'=>'Paid' ); // D-Draft, Order was submitted on Review Order page, but not submitted for payment with payment method
//New - user created order and admin yet did not view it.
//Opened - user created order and admin seen this  order and maybe sent emeil to user.
//Payed - order is payed and closed.

  public static $PaymentMethodChoices= array( 'CC'=>'Credit Card', 'PP'=>'PayPal', 'DP'=>'Department Purchase', 'GC'=>'Google Checkout' );

	public static function getPaymentMethodAsText($PaymentMethod) {
		$payment_methods_array= sfConfig::get('app_application_payment_methods');

	  if ( !empty( $payment_methods_array[ $PaymentMethod ]) ) {
	  	return $payment_methods_array[ $PaymentMethod ];
	  }
	  return $PaymentMethod;
	}


	public static function getLastOrderOfUser( $sfGuardUserId ) {
    $c = new Criteria();
    $c->add( OrderedPeer::USER_ID , $sfGuardUserId );
    $c->addDescendingOrderByColumn( OrderedPeer::ID );
    return OrderedPeer::doSelectOne( $c );
	}

  // Ordereds
	// OrderedPeer::getOrdereds($this->page, true, $this->filter_payment_method, $this->filter_status, $this->filter_date_from, $this->filter_date_till, $this->filter_state, $this->sorting);
  public static function getOrdereds( $page=1, $ReturnPager=true, $filter_payment_method, $filter_status, $filter_date_from, $filter_date_till, $filter_state, $Sorting='PRODUCT_LINE', $Limit= '' ) {
    //Util::deb($Sorting,'$Sorting::');
    $c = new Criteria();


		if (!empty($filter_payment_method) and $filter_payment_method != '-') {
			$c->add(OrderedPeer::PAYMENT_METHOD, $filter_payment_method);
		}

		if (!empty($filter_status) and $filter_status != '-') {
			$c->add(OrderedPeer::STATUS, $filter_status);
		}

		if (!empty($filter_state) and $filter_state != '-') {
			$c->add(OrderedPeer::S_STATE, $filter_state);
		}


		if (   !empty($filter_date_from) and !empty($filter_date_till)   ) {
			$date_from_Cond= $c->getNewCriterion( OrderedPeer::CREATED_AT, $filter_date_from, Criteria::GREATER_EQUAL );
			$date_till_Cond= $c->getNewCriterion( OrderedPeer::CREATED_AT, $filter_date_till, Criteria::LESS_EQUAL );
			$date_from_Cond->addAnd( $date_till_Cond );
			$c->add( $date_from_Cond );
		} else {
			if ( !empty($filter_date_from) ) {
				$c->add( OrderedPeer::CREATED_AT, $filter_date_from, Criteria::GREATER_EQUAL  );
			}
			if ( !empty($filter_date_till) ) {
				$c->add( OrderedPeer::CREATED_AT, $filter_date_till, Criteria::LESS_EQUAL );
			}
		}



		if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(OrderedPeer::CREATED_AT);
    }
    if ( $Sorting=='ID' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::ID );
    }
    if ( $Sorting=='USER' ) {
      //$c->addAscendingOrderByColumn(OrderedPeer::TITLE );
      $c->addJoin( OrderedPeer::USER_ID, sfGuardUserPeer::ID, Criteria::JOIN );
      $c->addAscendingOrderByColumn(sfGuardUserPeer::USERNAME);
    }
    if ( $Sorting=='STATUS' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::STATUS );
    }
    if ( $Sorting=='TransactionId' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::TRANSACTION_ID );
    }

    if ( $Sorting=='CITY' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::S_CITY );
    }
    if ( $Sorting=='COUNTRY' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::S_COUNTRY );
    }
    if ( $Sorting=='ZIP' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::S_ZIP );
    }

    if ( $Sorting=='STATE' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::S_STATE );
    }


    if ( $Sorting=='SHIPPING_COST' ) {
			$c->addAscendingOrderByColumn(OrderedPeer::SHIPPING_COST );
		}

    if ( $Sorting=='TOTAL_PRICE' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::TOTAL_PRICE );
    }
    if ( $Sorting=='TAX' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::TAX );
    }
    if ( $Sorting=='PAYMENT_METHOD' ) {
      $c->addAscendingOrderByColumn(OrderedPeer::PAYMENT_METHOD );
    }

    if (!empty($Limit)) {
    	$c->setLimit($Limit);
    }
    if ( !$ReturnPager ) {
      return OrderedPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'Ordered', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarOrdered($OrderedId, $con = null)
  {
    $c = new Criteria();
    $c->add( OrderedPeer::KEY , $OrderedKey );
    if($lResult = OrderedPeer::doSelectOne( $c, $con ) )
    {
      return $lResult;
    }
    return '';
  }

} // OrderedPeer
