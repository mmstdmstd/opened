<?php

require 'lib/model/om/BaseSizeOrderPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'size_order' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class SizeOrderPeer extends BaseSizeOrderPeer {

	public static function getSizeOrders( $page=1, $ReturnPager=true, $Sorting='ID', $Limit= '' ) {
		$c = new Criteria();
		if ( $Sorting=='ID' or empty($Sorting) ) {
			$c->addAscendingOrderByColumn(SizeOrderPeer::ID);
		}
		if ( $Sorting=='SIZE' ) {
			$c->addAscendingOrderByColumn(SizeOrderPeer::SIZE );
		}
		if (!empty($Limit)) {
			$c->setLimit($Limit);
		}
		if ( !$ReturnPager ) {
			return SizeOrderPeer::doSelect( $c );
		}
		$pager = new sfPropelPager( 'SizeOrder', (int)sfConfig::get('app_application_rows_in_pager' ) );
		$pager->setPage( $page );
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

} // SizeOrderPeer
