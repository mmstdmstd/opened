<?php

require 'lib/model/om/BaseInventoryItem.php';


/**
 * Skeleton subclass for representing a row from the 'inventory_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItem extends BaseInventoryItem {

	/**
	 * Initializes internal state of InventoryItem object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}


	public function __toString()
	{
	  return $this->getTitle();
	}


	public function getClearance_Or_SalePrice()
	{ //If sale_method == "D" or "P" check both the clearance price and the sale_price and use the one that is not $0.00.
  // If both clearance and sale_price have values, use the lowest of the two. If sale_start_date or sale_end_date is not NULL then the sale
  // is only valid between those dates (including the start and end date). Also, always check to make sure the price displayed on the page
  // is not $0.00. If it's $0.00, display an mdash in its place.

    $SalePrice= $this->getSalePrice();
    $Clearance= $this->getClearance();

    //Util::deb( $SalePrice, ' $SalePrice::' );
    //Util::deb( $Clearance, '$Clearance::' );
    if ( $Clearance< $SalePrice ) {
      //Util::deb( '-2::' );
      if ( $Clearance!= 0 ) {
        $ProductPrice= $Clearance;
      } else {
        $ProductPrice= $SalePrice;
      }
    }

    if ( $SalePrice < $Clearance ) {
      // Util::deb( '-12::' );
      if ( $SalePrice!= 0 ) {
        $ProductPrice= $SalePrice;
      } else {
        $ProductPrice= $Clearance;
      }
    }

    if ( $SalePrice == $Clearance ) {
      $ProductPrice= $SalePrice;
    }

    return $ProductPrice;
  }


	public function getCurrentlyOnSale() {
    $CurrentDate = strtotime(date("m-d-Y",time()));
    $CurrentlyOnSale = FALSE;
    $SaleStartDate= $this->getSaleStartDate();
    $SaleEndDate= $this->getSaleEndDate();

    if ($SaleStartDate == '' && $SaleEndDate == '') {
      $CurrentlyOnSale = TRUE;
    } else {
      if (strtotime($SaleStartDate) <= $CurrentDate) {
        if (strtotime($SaleEndDate) >= $CurrentDate) {
          $CurrentlyOnSale = TRUE;
        }
      } elseif (strtotime($SaleEndDate) >= $CurrentDate) {
        if (strtotime($SaleStartDate) <= $CurrentDate) {
          $CurrentlyOnSale = TRUE;
        }
      }
    }
    return $CurrentlyOnSale;
  }

	public function getTitleWithRSymbol() { // (R) symbol in product titles needs to be converted to the HTML ® symbol when displayed on the listings
    $Title= $this->getTitle();
    $Title= str_replace( "(TM)", "&trade;", $Title );
    return  str_replace( "(R)", "&reg;", $Title );
  }


} // InventoryItem
