<?php

require 'lib/model/om/BaseInventoryItemPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'inventory_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItemPeer extends BaseInventoryItemPeer
{
	public static $PriorPricesArray= array(25=>0, 50=>25, 75=>50, 100=>75, 150=>100, 200=>150, 250=>200, 500=>250, 750=>500, 1000=>750, 2500=>1000, 5000=>2500, 10000=>5000, 20000=>10000);
	public static $MaxPeriodPrice= 20000;
	////////////// PRICES   if(CLEARANCE<>0 and CLEARANCE<STD_UNIT_PRICE,CLEARANCE,STD_UNIT_PRICE)

	public static function getInventoryItemsListByLimits($page, $Limit)
	{
		$c = new Criteria();
		//$c->add( InventoryItemPeer::COLOR , null, Criteria::NOT_EQUAL );
		//$c->add( InventoryItemPeer::COLOR , '', Criteria::NOT_EQUAL );

		//$c->setLimit($Limit);
		$pager = new sfPropelPager('InventoryItem', $Limit);
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

	// $this->InventoryItemsPager= InventoryItemPeer::getInventory_Items_Left_sidebar_ByUniquesSkuList($this->page, true, false, $unique_session_id, $this->rows_in_pager, $this->sorting );

	public static function getInventory_Items_Left_sidebar_ByUniquesSkuList($page = 1, $ReturnPager = true, $ReturnCount= false, $unique_session_id='', $rows_in_pager = '', $Sorting = '' )
	{
		// Util::deb( $Sorting, ' getInventory_Items_Left_sidebar_test $Sorting::' );
		$c = new Criteria();
		$c->addJoin(InventoryItemPeer::SKU, UniquesSkuListPeer::SKU, Criteria::LEFT_JOIN);
		$c->add( UniquesSkuListPeer::UNIQUE_SESSION_ID, $unique_session_id);
		$c->setDistinct();

		if ($Sorting == 'QTY_ON_HAND') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::QTY_ON_HAND);
		}
		if ($Sorting == 'STD_UNIT_PRICE_LOW_TO_HIGN') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
		}

		if ($Sorting == 'STD_UNIT_PRICE_HIGN_TO_LOW') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
		}

		if ($ReturnCount) {
			return InventoryItemPeer::doCount($c);
		}
		if (!$ReturnPager) {
			return InventoryItemPeer::doSelect($c);
		}
		if (empty($rows_in_pager)) $rows_in_pager = (int)sfConfig::get('app_application_rows_in_pager');
		$pager = new sfNPropelPager('InventoryItem', $rows_in_pager);
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	} // public static function getInventory_Items_Left_sidebar_ByUniquesSkuList($page = 1, $ReturnPager = true, $rows_in_pager = '', $Sorting = '' )


///////////////////////////////////////////////

	public static function getColorsCountListingAllGroups( $filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('colors_count_beige', 'count( udf_it_swc_beige )');
		$c->addAsColumn('colors_count_black', 'count( udf_it_swc_black )');
		$c->addAsColumn('colors_count_blue', 'count( udf_it_swc_blue )');
		$c->addAsColumn('colors_count_brown', 'count( udf_it_swc_brown )');
		$c->addAsColumn('colors_count_camo', 'count( udf_it_swc_camo )');
		$c->addAsColumn('colors_count_gold', 'count( udf_it_swc_gold )');
		$c->addAsColumn('colors_count_gray', 'count( udf_it_swc_gray )');
		$c->addAsColumn('colors_count_green', 'count( udf_it_swc_green )');
		$c->addAsColumn('colors_count_orange', 'count( udf_it_swc_orange )');
		$c->addAsColumn('colors_count_pink', 'count( udf_it_swc_pink )');
		$c->addAsColumn('colors_count_purple', 'count( udf_it_swc_purple )');
		$c->addAsColumn('colors_count_red', 'count( udf_it_swc_red )');
		$c->addAsColumn('colors_count_silver', 'count( udf_it_swc_silver )');
		$c->addAsColumn('colors_count_white', 'count( udf_it_swc_white )');
		$c->addAsColumn('colors_count_yellow', 'count( udf_it_swc_yellow )');

		/*  select sum(UDF_IT_SWC_BLACK=1) UDF_IT_SWC_BLACK, sum(UDF_IT_SWC_WHITE=1) UDF_IT_SWC_WHITE, ... FROM inventory_item  */
		// $c->addSelectColumn(InventoryItemPeer::COLOR);
		$c->setDistinct();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);


			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond);//TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/*$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		//$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		//$c->add( $RootItem_Cond );

		$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
		$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
		$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

		$c_BEIGE= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BEIGE, true);
		$c_BLACK= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BLACK, true);
		$c_BEIGE->addOr($c_BLACK);

		$c_BLUE= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BLUE, true);
		$c_BEIGE->addOr($c_BLUE);


		$c_BROWN= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BROWN, true);
		$c_BEIGE->addOr($c_BROWN);
		$c_CAMO= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_CAMO, true);
		$c_BEIGE->addOr($c_CAMO);
		$c_GOLD= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GOLD, true);
		$c_BEIGE->addOr($c_GOLD);
		$c_GRAY= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GRAY, true);
		$c_BEIGE->addOr($c_GRAY);
		$c_GREEN= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GREEN, true);
		$c_BEIGE->addOr($c_GREEN);
		$c_ORANGE= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_ORANGE, true);
		$c_BEIGE->addOr($c_ORANGE);
		$c_PINK= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_PINK, true);
		$c_BEIGE->addOr($c_PINK);
		$c_PURPLE= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_PURPLE, true);
		$c_BEIGE->addOr($c_PURPLE);
		$c_RED= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_RED, true);
		$c_BEIGE->addOr($c_RED);
		$c_SILVER= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_SILVER, true);
		$c_BEIGE->addOr($c_SILVER);
		$c_WHITE= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_WHITE, true);
		$c_BEIGE->addOr($c_WHITE);
		$c_YELLOW= $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_YELLOW, true);
		$c_BEIGE->addOr($c_YELLOW);

		$c->addOr($c_BEIGE);
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_BEIGE );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_BLACK );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_BLUE );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_BROWN );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_CAMO );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_GOLD );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_GRAY );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_GREEN );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_ORANGE );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_PINK );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_PURPLE );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_RED );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_SILVER );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_WHITE );
		$c->addGroupByColumn( InventoryItemPeer::UDF_IT_SWC_YELLOW );


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'colors ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		$I= 0; $colors_names= array( 'Beige', 'Black', 'Blue', 'Brown', 'Camo', 'Gold', 'Gray', 'Green','Orange', 'Pink', 'Purple', 'Red', 'Silver', 'White','Yellow' );
		while ($result = $stmt->fetch()) {
			Util::deb($result, '$result::');
			//if ($result['colors_count_beige'] <= 0) continue;
			$ResArray[] = array( 'color' => $colors_names[$I], 'colors_count' => $result['colors_count_beige']);
			$I++;
		}
		Util::deb( $ResArray, '$ResArray::');
		return $ResArray;

	}


	/*SELECT DISTINCT  count(udf_it_swc_beige) color_count, 'beige' AS color
		FROM `inventory_item`
		WHERE (inventory_item.MATRIX=0 OR inventory_item.SKU = inventory_item.ROOTITEM) AND inventory_item.udf_it_swc_beige=1
	*/
	public static function getColorsCountListing($field_name, $filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('colors_count', 'count(' . $field_name . ')');
		$c->addAsColumn('color', "'" . $field_name . "'");
		// $c->addSelectColumn(InventoryItemPeer::COLOR);
		// $c->setDistinct();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);


			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/*$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		//$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		//$c->add( $RootItem_Cond );

		if ($field_name == 'udf_it_swc_beige') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_BEIGE, true);
		}
		if ($field_name == 'udf_it_swc_black') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_BLACK, true);
		}
		if ($field_name == 'udf_it_swc_blue') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_BLUE, true);
		}
		if ($field_name == 'udf_it_swc_brown') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_BROWN, true);
		}
		if ($field_name == 'udf_it_swc_camo') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_CAMO, true);
		}
		if ($field_name == 'udf_it_swc_gold') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_GOLD, true);
		}
		if ($field_name == 'udf_it_swc_gray') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_GRAY, true);
		}
		if ($field_name == 'udf_it_swc_green') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_GREEN, true);
		}
		if ($field_name == 'udf_it_swc_orange') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_ORANGE, true);
		}
		if ($field_name == 'udf_it_swc_pink') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_PINK, true);
		}
		if ($field_name == 'udf_it_swc_purple') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_PURPLE, true);
		}
		if ($field_name == 'udf_it_swc_red') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_RED, true);
		}
		if ($field_name == 'udf_it_swc_silver') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_SILVER, true);
		}
		if ($field_name == 'udf_it_swc_white') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_WHITE, true);
		}
		if ($field_name == 'udf_it_swc_yellow') {
			$c->add(InventoryItemPeer::UDF_IT_SWC_YELLOW, true);
		}
		//$c->addDescendingOrderByColumn($c->getColumnForAs('colors_count'));
		//$c->addGroupByColumn('color');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'colors ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['colors_count'] <= 0) continue;
			$ResArray = array('color' => $result['color'], 'colors_count' => $result['colors_count']);
		}
		return $ResArray;

	}


	/*
	///////////////////////////////////////////////
		public static function getColorsCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters= array(), $checked_action= '')
		{
			$c = new Criteria();
			$c->addAsColumn('colors_count', 'count(color)');
			$c->addAsColumn('color', 'color');
			// $c->addSelectColumn(InventoryItemPeer::COLOR);
			$c->setDistinct();

			$inventory_category_joined = false;
			if (!empty($filter_category)) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
				$inventory_category_joined = true;
			}

			if (!empty($filter_subcategory)) {
				if (!$inventory_category_joined) {
					$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
					$inventory_category_joined = true;
				}
				$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
				$inventory_category_joined = true;
			}

			if (!empty($filter_title) and $filter_title != '-') {

				$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
				$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

				$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

				$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($SIZE_Cond);


				$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($COLOR_Cond);

				$OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($OPTIONS_Cond);

				$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($GENDER_Cond);

				$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
				$TITLE_Cond->addOr($SKU_Cond);

				$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

			}
			$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
			$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
			$Matrix_Cond->addOr($RootItem_Cond);
			$c->add($Matrix_Cond);
			$c->add(InventoryItemPeer::COLOR, '', Criteria::NOT_EQUAL);
			$c->add(InventoryItemPeer::COLOR, null, Criteria::NOT_EQUAL);

			$c->addDescendingOrderByColumn($c->getColumnForAs('colors_count'));
			$c->addGroupByColumn('color');


			// +++ extended_parameters START
			//  Util::deb($extended_parameters, 'colors ++$extended_parameters');
			if ( !empty($extended_parameters['selected_brands']) ) {
				$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
			}
			if ( !empty($extended_parameters['selected_sizes']) ) {
				$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
			}
			if ( !empty($extended_parameters['selected_genders']) ) {
				$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
			}

			if ( !empty($extended_parameters['selected_prices']) ) {
				$prices_conditions = ' ( ';
				$LLL = count($extended_parameters['selected_prices']);
				for ($III = 0; $III < $LLL; $III++) {
					$MinPrice=  round(floatval( $extended_parameters['selected_prices'][$III] - 25 ), 2);
					$MaxPrice=  round(floatval($extended_parameters['selected_prices'][$III]), 2);
					if ( $MaxPrice< InventoryItemPeer::$MaxPeriodPrice ) {
						$prices_conditions .=  ' ( round(inventory_item.STD_UNIT_PRICE,2) >= '. $MinPrice .
							' AND round(inventory_item.STD_UNIT_PRICE,2) < '. $MaxPrice . " " .
							($III == $LLL - 1 ? ' ) ' : " ) OR ");
					} else {
						$prices_conditions .=  ' ( round(inventory_item.STD_UNIT_PRICE,2) >= '. $MinPrice .
							($III == $LLL - 1 ? ' ) ' : " ) OR ");
					}
				}
				$prices_conditions.= ' ) ';
				$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
				$c->add($PricesAddCont_Cond);
			}

			if ( !empty($extended_parameters['selected_sale_prices']) ) {
				$c->add( InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN );
			}
			if ( !empty($extended_parameters['selected_finishs']) ) {
				$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
			}

			if ( !empty($extended_parameters['selected_hands']) ) {
				foreach( $extended_parameters['selected_hands'] as $HandName ) {
					if ( strtoupper($HandName) == 'RIGHT' ) {
						$extended_parameters['selected_hands'][]= 'R';
					}
					if ( strtoupper($HandName) == 'LEFT' ) {
						$extended_parameters['selected_hands'][]= 'L';
					}
				}
				$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
			}
			// +++ extended_parameters END


			$stmt = InventoryItemPeer::DoSelectStmt($c);
			$ResArray = Array();
			while ($result = $stmt->fetch()) {
				if ($result['colors_count'] <= 0) continue;
				$ResArray[] = array('color' => $result['color'], 'colors_count' => $result['colors_count']);
			}
			return $ResArray;

		}
	 */

	public static function getPricesCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('std_unit_prices_count', 'count(std_unit_price)');
		$c->addAsColumn('std_unit_price', 'std_unit_price');
//		$c->addSelectColumn(InventoryItemPeer::STD_UNIT_PRICE);


		$c->setDistinct();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);


			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, price.

		}
		/*$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		// $RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		// $c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::STD_UNIT_PRICE, '', Criteria::NOT_EQUAL);
		$c->add(InventoryItemPeer::STD_UNIT_PRICE, null, Criteria::NOT_EQUAL);

		$c->addDescendingOrderByColumn($c->getColumnForAs('std_unit_prices_count'));
		$c->addGroupByColumn('std_unit_price');


		// +++ extended_parameters START
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['std_unit_prices_count'] <= 0) continue;
			$ResArray[] = array('std_unit_price' => $result['std_unit_price'], 'std_unit_prices_count' => $result['std_unit_prices_count']);
		}
		return $ResArray;

	}

//////////////////////////////// Hand ///////////////
	public static function getHandsCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('hands_count', 'count(hand)');
		$c->addAsColumn('hand', 'hand');
		//$c->addSelectColumn(InventoryItemPeer::HAND);
		$c->setDistinct();
		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);

			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/*$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		// $RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		// $c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::HAND, '', Criteria::NOT_EQUAL);
		$c->add(InventoryItemPeer::HAND, null, Criteria::NOT_EQUAL);

		$c->addDescendingOrderByColumn($c->getColumnForAs('hands_count'));
		$c->addGroupByColumn('hand');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'hands  ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			// $c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				// $MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['hands_count'] <= 0) continue;
			$ResArray[] = array('hand' => $result['hand'], 'hands_count' => $result['hands_count']);
		}
		return $ResArray;

	}

//////////////////////////////// Finish ///////////////
	public static function getFinishsCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('finishs_count', 'count(finish)');
		$c->addAsColumn('finish', 'finish');
		//$c->addSelectColumn(InventoryItemPeer::FINISH);
		$c->setDistinct();
		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);

			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/* $Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		// $RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		// $c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::FINISH, '', Criteria::NOT_EQUAL);
		$c->add(InventoryItemPeer::FINISH, null, Criteria::NOT_EQUAL);

		$c->addAscendingOrderByColumn($c->getColumnForAs('finish'));
		$c->addGroupByColumn('finish');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'finishs ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}
		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['finishs_count'] <= 0) continue;
			$ResArray[] = array('finish' => $result['finish'], 'finishs_count' => $result['finishs_count']);
		}
		return $ResArray;

	}

	// Sale_Price
	public static function getSale_PricesCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('sale_prices_count', 'count(sale_price)');
		$c->addAsColumn('sale_price', 'sale_price');
		//$c->addSelectColumn(InventoryItemPeer::SALE_PRICE);
		$c->setDistinct();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);


			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, price.

		}
		/* $Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		//$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		//$c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);

		//$c->addDescendingOrderByColumn($c->getColumnForAs('sale_prices_count'));
		//$c->addGroupByColumn('sale_price');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'sale_prices++$extended_parameters');

		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
//			$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['sale_prices_count'] <= 0) continue;
			$ResArray[] = array('sale_price' => $result['sale_price'], 'sale_prices_count' => $result['sale_prices_count']);
		}
		return $ResArray;

	}


///////////////////////////////////////////////

	public static function getGendersCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('genders_count', 'count(gender)');
		$c->addAsColumn('gender', 'gender');
		//$c->addSelectColumn(InventoryItemPeer::GENDER);
		$c->setDistinct();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);

			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/* $Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond);  */
		// $RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		// $c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::GENDER, '', Criteria::NOT_EQUAL);
		$c->add(InventoryItemPeer::GENDER, null, Criteria::NOT_EQUAL);

		$c->addDescendingOrderByColumn($c->getColumnForAs('genders_count'));
		$c->addGroupByColumn('gender');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'genders ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['genders_count'] <= 0) continue;
			$ResArray[] = array('gender' => $result['gender'], 'genders_count' => $result['genders_count']);
		}
		return $ResArray;

	}

///////////////////////////////////////////////
	public static function getSizesCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('sizes_count', 'count(size)');
		$c->addAsColumn('size', 'size');
		//$c->addSelectColumn(InventoryItemPeer::SIZE);
		$c->setDistinct();
		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);

			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);

			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);

			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/* $Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond);  */
		//$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		//$c->add( $RootItem_Cond );

		$c->add(InventoryItemPeer::SIZE, '', Criteria::NOT_EQUAL);
		$c->add(InventoryItemPeer::SIZE, null, Criteria::NOT_EQUAL);

		$c->addDescendingOrderByColumn($c->getColumnForAs('sizes_count'));
		$c->addGroupByColumn('size');


		// +++ extended_parameters START
		//  Util::deb($extended_parameters, 'sizes ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END

		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['sizes_count'] <= 0) continue;
			$ResArray[] = array('size' => $result['size'], 'sizes_count' => $result['sizes_count']);
		}
		return $ResArray;

	}


	public static function getBrandsCountListing($filter_category = '', $filter_subcategory = '', $filter_title = '', $extended_parameters = array(), $checked_action = '')
	{
		$c = new Criteria();
		$c->addAsColumn('brands_count', 'count(brand_id)');
		$c->addAsColumn('brand_id', 'brand_id');
		$c->addAsColumn('brand_name', 'brand.title');
		$c->addSelectColumn(InventoryItemPeer::BRAND_ID);
		$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
		$c->setDistinct();
		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		if (!empty($filter_title) and $filter_title != '-') {

			$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);


			$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);


			$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($SIZE_Cond);


			$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($COLOR_Cond);

			// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
			// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

			$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
			$TITLE_Cond->addOr($GENDER_Cond);

			$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
			$TITLE_Cond->addOr($SKU_Cond);


			$BRAND_Cond = $c->getNewCriterion(BrandPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
			$TITLE_Cond->addOr($BRAND_Cond);

			$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

		}
		/* $Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
		$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		$Matrix_Cond->addOr($RootItem_Cond);
		$c->add($Matrix_Cond); */
		//$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
		//$c->add( $RootItem_Cond );

		$c->addAscendingOrderByColumn($c->getColumnForAs('brand_name'));
		$c->addGroupByColumn('brand_id');
		$c->addGroupByColumn('brand_name');

		// +++ extended_parameters START
		//Util::deb($extended_parameters, 'getBrandsCountListing  ++$extended_parameters');
		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}
		if (!empty($extended_parameters['selected_colors'])) {
			// Util::deb( $checked_action, ' ++INSIDE$checked_action::');
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}
		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::SALE_PRICE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}
		// +++ extended_parameters END


		$stmt = InventoryItemPeer::DoSelectStmt($c);
		$ResArray = Array();
		while ($result = $stmt->fetch()) {
			if ($result['brands_count'] <= 0) continue;
			$ResArray[] = array('brand_id' => $result['brand_id'], 'brand_name' => $result['brand_name'], 'brands_count' => $result['brands_count']);
		}
		return $ResArray;

	}


	public static function getInventoryItemsCountByInventoryCategory($filter_inventory_category = '')
	{
		$c = new Criteria();

		$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
		$c->add(InventoryCategoryPeer::CATEGORY, $filter_inventory_category);

		return InventoryItemPeer::doCount($c);
	}

	public static function getInventoryItemsByInventoryCategory($page = 1, $ReturnPager = true, $ReturnCount = false, $filter_inventory_category = '',
																															$filter_matrix = '', $rows_in_pager = '')
	{
		$c = new Criteria();

		$c->add(InventoryItemPeer::INVENTORY_CATEGORY, $filter_inventory_category);

		if (strlen($filter_matrix) > 0 and (int)$filter_matrix == 1) {
//      $c->add( InventoryItemPeer::MATRIX, $filter_matrix );
			$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
			$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
			$Matrix_Cond->addOr($RootItem_Cond);
			$c->add($Matrix_Cond);
		}

		if ($ReturnCount) {
			return InventoryItemPeer::doCount($c);
		}
		if (!$ReturnPager) {
			return InventoryItemPeer::doSelect($c);
		}
		if (empty($rows_in_pager)) $rows_in_pager = (int)sfConfig::get('app_application_rows_in_pager');
		if ($rows_in_pager > 0) {
			$pager = new sfNPropelPager('InventoryItem', $rows_in_pager);
		}
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

	/*
		public static function getInventoryItemsByInventorySubcategory( $page=1, $ReturnPager=true, $ReturnCount= false, $filter_inventory_subcategory= '',
		$filter_matrix= '', $rows_in_pager= '' ) {
			$c = new Criteria();

			 $c->add( InventoryItemPeer::INVENTORY_SUBCATEGORY, $filter_inventory_subcategory );

			// Util::deb( $filter_matrix, ' $filter_matrix::' );
	//     Util::deb( $filter_rootitem, ' $filter_rootitem::' );
			if ( strlen($filter_matrix)> 0 and (int)$filter_matrix == 1 ) {
	//      $c->add( InventoryItemPeer::MATRIX, $filter_matrix );
				$Matrix_Cond= $c->getNewCriterion( InventoryItemPeer::MATRIX, false );
				$RootItem_Cond= $c->getNewCriterion( InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM );
				$Matrix_Cond->addOr( $RootItem_Cond );
				$c->add( $Matrix_Cond );
			}

			if ( $ReturnCount ) {
				return InventoryItemPeer::doCount( $c );
			}
			if ( !$ReturnPager ) {
				return InventoryItemPeer::doSelect( $c );
			}
			if ( empty($rows_in_pager) ) $rows_in_pager= (int)sfConfig::get('app_application_rows_in_pager' );
			if ( $rows_in_pager >0 ) {
				$pager = new sfNPropelPager( 'InventoryItem', $rows_in_pager );
			}
			$pager->setPage( $page );
			$pager->setCriteria($c);
			$pager->init();
			return $pager;
		}*/


	public static function getInventory_Items_Left_sidebar_test($page = 1, $ReturnPager = true, $ReturnCount = false, $rows_in_pager = '', $filter_matrix = '', $filter_rootitem = '',
																															$filter_title = '', $ExtendedSearch = false, $filter_sku = '', $filter_category = '', $filter_subcategory = '', $filter_brand_id = '', $filter_qty_on_hand_min = '',
																															$filter_qty_on_hand_max = '', $filter_std_unit_price_min = '', $filter_std_unit_price_max = '', $Sorting = '', $filter_sale_method = '', $extended_parameters = array())
	{
		// Util::deb( $filter_category, ' $filter_category::' );
		// Util::deb( $filter_subcategory, ' $filter_subcategory::' );
		// Util::deb( $filter_brand_id, ' $filter_brand_id::' );
		//die("UI");
		$ModuleName = sfContext::getInstance()->getModuleName();
		$ActionName = sfContext::getInstance()->getActionName();

		$lInventoryItemBySku = InventoryItemPeer::getSimilarInventoryItem($filter_title);
		// Util::deb($lInventoryItemBySku, '$lInventoryItemBySku::');
		$isSeacrhByParentSku = false;
		if (!empty($lInventoryItemBySku)) {
			if ($lInventoryItemBySku->getMatrix() == 1 and $filter_title != $lInventoryItemBySku->getRootItem()) {
				$isSeacrhByParentSku = true;
			}
		}

		$c = new Criteria();
		// $c->addAsColumn('rootitem', 'rootitem');

		$inventory_category_joined = false;
		// Util::deb($filter_category, '$filter_category::');
		if (!empty($filter_category)) {
			//$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);

			$c->addJoin(InventoryItemInventoryCategoryPeer::INVENTORY_CATEGORY_ID, InventoryCategoryPeer::ID, Criteria::LEFT_JOIN);
			//$c->addJoin( InventoryItemInventoryCategoryPeer::SKU, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);

			// $c->add( InventoryCategoryPeer::PRODUCT_LINE, $filter_category ); ????? NOT IN ALL CAESES
			// Util::deb( $filter_category, ' Join 1 $filter_category::' );
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			// Util::deb( $filter_category, ' Join +2 $filter_category::' );
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

		// Util::deb( $filter_matrix, ' $filter_matrix::' );
		// Util::deb( $filter_rootitem, ' $filter_rootitem::' );
		// When I click "Blauer" as a brand, and then click "Brown" as a color, I should not see all 71 matrix items in the product listing. I should see only the root
		// items displayed.  In other words, any root item from a matrix that contains an item that matches the filter should be displayed, but only the
		// root item and not the individual matrix item
		//Util::deb( $filter_matrix, ' $filter_matrix::' );
		// Util::deb( $extended_parameters, ' $extended_parameters::' );
		if (strlen($filter_matrix) > 0 and (int)$filter_matrix == 1 ) { // and !$isSeacrhByParentSku
//      $c->add( InventoryItemPeer::MATRIX, $filter_matrix );
			//$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
			//if ( !empty($extended_parameters['select_only_rootitems']) and $extended_parameters['select_only_rootitems'] == 1 ) {

			// parent_items_rray
			$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, true);
			$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
			$Matrix_Cond->addAnd( $RootItem_Cond );

			$NonMatrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
			$Matrix_Cond->addOr($NonMatrix_Cond);
			if ( !empty( $extended_parameters['parent_items_array']) and is_array($extended_parameters['parent_items_array']) ) {
				$parent_items_array_Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, true);
				$parent_items_array_Cond = $c->getNewCriterion(InventoryItemPeer::ROOTITEM, $extended_parameters['parent_items_array'], Criteria::IN);
				$parent_items_array_Cond->addAnd($parent_items_array_Matrix_Cond);
				$Matrix_Cond->addOr( $parent_items_array_Cond );
			}


			$c->add( $Matrix_Cond );

			// VALID  ??   SELECT * FROM `inventory_item` WHERE inventory_item.BRAND_ID IN ('21') and ( ( matrix=1 and sku =rootitem) or matrix=0 )
			// BAD SELECT * FROM `inventory_item` WHERE (inventory_item.SKU = inventory_item.ROOTITEM OR inventory_item.MATRIX=1) AND inventory_item.BRAND_ID IN ('21')

			//$Matrix_Cond->addOr($RootItem_Cond);
			//$c->add($Matrix_Cond);
			//}
		}

		if (strlen($filter_rootitem) > 0) {
			$c->add(InventoryItemPeer::ROOTITEM, $filter_rootitem);
		}

		//Util::deb( $filter_title, ' $filter_title::' );
		if (!empty($filter_title) and $filter_title != '-') {
			if ($ExtendedSearch) {
				//$c->add( InventoryItemPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );

				$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
				$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);


				$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);
				//$c->addOr( $TITLE_Cond );


				$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($SIZE_Cond);


				$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($COLOR_Cond);

				// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
				// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

				$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($GENDER_Cond);

				//Util::deb($filter_title, '$filter_title::');
				if (!$isSeacrhByParentSku) {
					$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
					$TITLE_Cond->addOr($SKU_Cond);
				} else {
					$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
					$SKU_Cond_Parent = $c->getNewCriterion(InventoryItemPeer::SKU, $lInventoryItemBySku->getRootItem());
					$SKU_Cond->addOr($SKU_Cond_Parent);
					$TITLE_Cond->addOr($SKU_Cond);

				}


				/* $BRAND_Cond= $c->getNewCriterion( BrandPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );
				 $c->addJoin( InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::JOIN );
				 $c->addOr( $BRAND_Cond );  */

				$BRAND_Cond = $c->getNewCriterion(BrandPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
				$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
				$TITLE_Cond->addOr($BRAND_Cond);

				$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

			} else {
				$c->add(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			}
		}

		// Util::deb($filter_sku, '$filter_sku::');

		if (!empty($filter_sku) and $filter_sku != '-') {
			$c->add(InventoryItemPeer::SKU, '%' . $filter_sku . '%', Criteria::LIKE);
		}
		if (!empty($filter_brand_id) and $filter_brand_id != '-') {
			//Util::deb( 'INSIDE::');
			$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
			$c->add(BrandPeer::ID, $filter_brand_id);
		}
		if ((!empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min != '-') and (!empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max != '-')) {
			$QtyOnHandMin_Cond = $c->getNewCriterion(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL);
			$QtyOnHandMax_Cond = $c->getNewCriterion(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL);
			$QtyOnHandMin_Cond->addAnd($QtyOnHandMax_Cond);
			$c->add($QtyOnHandMin_Cond);
		} else {
			if (!empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min != '-') {
				$c->add(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL);
			}
			if (!empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max != '-') {
				$c->add(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL);
			}
		}

		if ((!empty($filter_std_unit_price_min) and $filter_std_unit_price_min != '-') and (!empty($filter_std_unit_price_max) and $filter_std_unit_price_max != '-')) {
			$StdUnitPriceMin_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL);
			$StdUnitPriceMax_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL);
			$StdUnitPriceMin_Cond->addAnd($StdUnitPriceMax_Cond);
			$c->add($StdUnitPriceMin_Cond);
		} else {
			if (!empty($filter_std_unit_price_min) and $filter_std_unit_price_min != '-') {
				$c->add(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL);
			}
			if (!empty($filter_std_unit_price_max) and $filter_std_unit_price_max != '-') {
				$c->add(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL);
			}
		}
		if (!empty($filter_sale_method)) {
			if (is_array($filter_sale_method)) {
				$c->add(InventoryItemPeer::SALE_METHOD, $filter_sale_method, Criteria::IN);
			} else {
				if ($filter_sale_method != '-') {
					$c->add(InventoryItemPeer::SALE_METHOD, $filter_sale_method);
				}
			}
		}


		if ($Sorting == 'CREATED_AT') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::CREATED_AT);
		}
		if ($Sorting == 'SKU') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::SKU);
		}
		if ($Sorting == 'TITLE') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::TITLE);
		}
		if ($Sorting == 'BRAND') {
			$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
			$c->addAscendingOrderByColumn(BrandPeer::TITLE);
		}
		if ($Sorting == 'COLOR') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::COLOR);
		}
		if ($Sorting == 'SIZE') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::SIZE);
		}

		// Test HAND
		//if ( $Sorting=='HAND' ) {
		//$c->addAscendingOrderByColumn( InventoryItemPeer::HAND  );
		//}

		if ($Sorting == 'QTY_ON_HAND') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::QTY_ON_HAND);
		}
		if ($Sorting == 'STD_UNIT_PRICE_LOW_TO_HIGN') {
			if ($ModuleName == 'main' and $ActionName == 'sale_prices') {
				$c->addAscendingOrderByColumn(InventoryItemPeer::SALE_PRICE);
				$c->addAscendingOrderByColumn(InventoryItemPeer::CLEARANCE);

			} else {
				$c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
			}
		}

		if ($Sorting == 'STD_UNIT_PRICE_HIGN_TO_LOW') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
		}


		if ($Sorting == 'CLEARANCE') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::CLEARANCE);
		}
		$c->setDistinct();

		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_colors'])) {
			//Util::deb($extended_parameters['selected_colors'], ' ++$extended_parameters');
			//$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
			$Color_Cond = InventoryItemPeer::getColorCondition($c,$extended_parameters);
			if ( !empty($Color_Cond) ) {
				$c->add($Color_Cond);
			}
		}

		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				//$MinPrice = round(floatval($extended_parameters['selected_prices'][$III] - 25), 2);
				//Util::deb($extended_parameters['selected_prices'][$III], '$extended_parameters[selected_prices][$III]::');
				if ( empty($extended_parameters['selected_prices'][$III]) or $extended_parameters['selected_prices'][$III]==';' ) continue;
				$MinPrice = InventoryItemPeer::$PriorPricesArray[$extended_parameters['selected_prices'][$III]];
				$MaxPrice = round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ($MaxPrice < InventoryItemPeer::$MaxPeriodPrice) {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < ' . $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .= ' ( round(inventory_item.STD_UNIT_PRICE,2) >= ' . $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");

				}
			}
			$prices_conditions .= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add(InventoryItemPeer::CLEARANCE, 0, Criteria::GREATER_THAN);
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach ($extended_parameters['selected_hands'] as $HandName) {
				if (strtoupper($HandName) == 'RIGHT') {
					$extended_parameters['selected_hands'][] = 'R';
				}
				if (strtoupper($HandName) == 'LEFT') {
					$extended_parameters['selected_hands'][] = 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}

		if ($ReturnCount) {
			return InventoryItemPeer::doCount($c);
		}
		if (!$ReturnPager) {
			return InventoryItemPeer::doSelect($c);
		}
		if (empty($rows_in_pager)) $rows_in_pager = (int)sfConfig::get('app_application_rows_in_pager');
		$pager = new sfNPropelPager('InventoryItem', $rows_in_pager);
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
//		die("END");
		return $pager;
	}



	public static function getInventoryItems($page = 1, $ReturnPager = true, $ReturnCount = false, $rows_in_pager = '', $filter_matrix = '', $filter_rootitem = '',
																					 $filter_title = '', $ExtendedSearch = false, $filter_sku = '', $filter_category = '', $filter_subcategory = '', $filter_brand_id = '', $filter_qty_on_hand_min = '',
																					 $filter_qty_on_hand_max = '', $filter_std_unit_price_min = '', $filter_std_unit_price_max = '', $Sorting = '', $filter_sale_method = '', $extended_parameters = array())
	{

		//Util::deb( $Sorting, ' $Sorting::' );

		$ModuleName = sfContext::getInstance()->getModuleName();
		$ActionName = sfContext::getInstance()->getActionName();

		$lInventoryItemBySku= InventoryItemPeer::getSimilarInventoryItem($filter_title);
		//Util::deb($lInventoryItemBySku, '$lInventoryItemBySku::');
		$isSeacrhByParentSku= false;
		if ( !empty($lInventoryItemBySku) ) {
			if ( $lInventoryItemBySku->getMatrix()==1 and $filter_title!= $lInventoryItemBySku->getRootItem() ) {
				$isSeacrhByParentSku= true;
			}
		}

		$c = new Criteria();

		$inventory_category_joined = false;
		if (!empty($filter_category)) {
			$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
			// $c->add( InventoryCategoryPeer::PRODUCT_LINE, $filter_category ); ????? NOT IN ALL CAESES
			$c->add(InventoryCategoryPeer::CATEGORY, $filter_category);
			$inventory_category_joined = true;
		}

		if (!empty($filter_subcategory)) {
			if (!$inventory_category_joined) {
				$c->addJoin(InventoryItemPeer::INVENTORY_CATEGORY, InventoryCategoryPeer::PRODUCT_LINE, Criteria::LEFT_JOIN);
				$inventory_category_joined = true;
			}
			$c->add(InventoryCategoryPeer::SUBCATEGORY, $filter_subcategory);
			$inventory_category_joined = true;
		}

//     Util::deb( $filter_matrix, ' $filter_matrix::' );
//     Util::deb( $filter_rootitem, ' $filter_rootitem::' );
		if (strlen($filter_matrix) > 0 and (int)$filter_matrix == 1/* and !$isSeacrhByParentSku*/ ) {
//      $c->add( InventoryItemPeer::MATRIX, $filter_matrix );
			$Matrix_Cond = $c->getNewCriterion(InventoryItemPeer::MATRIX, false);
			$RootItem_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, 'inventory_item.SKU = inventory_item.ROOTITEM', Criteria::CUSTOM);
			$Matrix_Cond->addOr($RootItem_Cond);
			$c->add($Matrix_Cond);
		}

		if (strlen($filter_rootitem) > 0) {
			$c->add(InventoryItemPeer::ROOTITEM, $filter_rootitem);
		}

		//Util::deb( $filter_title, ' $filter_title::' );
		// Util::deb( $ExtendedSearch, ' $ExtendedSearch::' );
		if (!empty($filter_title) and $filter_title != '-') {
			if ($ExtendedSearch) {
				//$c->add( InventoryItemPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );

				$TITLE_Cond = $c->getNewCriterion(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
				$DESCRIPTION_SHORT_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_SHORT_Cond);


				$DESCRIPTION_LONG_Cond = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_LONG, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($DESCRIPTION_LONG_Cond);
				//$c->addOr( $TITLE_Cond );


				$SIZE_Cond = $c->getNewCriterion(InventoryItemPeer::SIZE, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($SIZE_Cond);


				$COLOR_Cond = $c->getNewCriterion(InventoryItemPeer::COLOR, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($COLOR_Cond);

				// $OPTIONS_Cond = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '%' . $filter_title . '%', Criteria::LIKE);
				// $TITLE_Cond->addOr($OPTIONS_Cond); //TODO

				$GENDER_Cond = $c->getNewCriterion(InventoryItemPeer::GENDER, '%' . $filter_title . '%', Criteria::LIKE);
				$TITLE_Cond->addOr($GENDER_Cond);

				//Util::deb($filter_title, '$filter_title::');
				if ( !$isSeacrhByParentSku ) {
					$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
					$TITLE_Cond->addOr($SKU_Cond);
				} else {
					$SKU_Cond = $c->getNewCriterion(InventoryItemPeer::SKU, $filter_title);
					$SKU_Cond_Parent = $c->getNewCriterion(InventoryItemPeer::SKU, $lInventoryItemBySku->getRootItem());
					$SKU_Cond->addOr($SKU_Cond_Parent);
					$TITLE_Cond->addOr($SKU_Cond);

				}


				/* $BRAND_Cond= $c->getNewCriterion( BrandPeer::TITLE, '%'.$filter_title.'%', Criteria::LIKE );
				 $c->addJoin( InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::JOIN );
				 $c->addOr( $BRAND_Cond );  */

				$BRAND_Cond = $c->getNewCriterion(BrandPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
				$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
				$TITLE_Cond->addOr($BRAND_Cond);

				$c->addOr($TITLE_Cond); //3) Yes, search should include description_short, description_long, brand, size, color, options, gender.

			} else {
				$c->add(InventoryItemPeer::TITLE, '%' . $filter_title . '%', Criteria::LIKE);
			}
		}

		// Util::deb($filter_sku, '$filter_sku::');

		if (!empty($filter_sku) and $filter_sku != '-') {
			$c->add(InventoryItemPeer::SKU, '%' . $filter_sku . '%', Criteria::LIKE);
		}
		if (!empty($filter_brand_id) and $filter_brand_id != '-') {
			$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
			$c->add(BrandPeer::ID, $filter_brand_id);
		}

		if ((!empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min != '-') and (!empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max != '-')) {
			$QtyOnHandMin_Cond = $c->getNewCriterion(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL);
			$QtyOnHandMax_Cond = $c->getNewCriterion(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL);
			$QtyOnHandMin_Cond->addAnd($QtyOnHandMax_Cond);
			$c->add($QtyOnHandMin_Cond);
		} else {
			if (!empty($filter_qty_on_hand_min) and $filter_qty_on_hand_min != '-') {
				$c->add(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_min, Criteria::GREATER_EQUAL);
			}
			if (!empty($filter_qty_on_hand_max) and $filter_qty_on_hand_max != '-') {
				$c->add(InventoryItemPeer::QTY_ON_HAND, $filter_qty_on_hand_max, Criteria::LESS_EQUAL);
			}
		}

		if ((!empty($filter_std_unit_price_min) and $filter_std_unit_price_min != '-') and (!empty($filter_std_unit_price_max) and $filter_std_unit_price_max != '-')) {
			$StdUnitPriceMin_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL);
			$StdUnitPriceMax_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL);
			$StdUnitPriceMin_Cond->addAnd($StdUnitPriceMax_Cond);
			$c->add($StdUnitPriceMin_Cond);
		} else {
			if (!empty($filter_std_unit_price_min) and $filter_std_unit_price_min != '-') {
				$c->add(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_min, Criteria::GREATER_EQUAL);
			}
			if (!empty($filter_std_unit_price_max) and $filter_std_unit_price_max != '-') {
				$c->add(InventoryItemPeer::STD_UNIT_PRICE, $filter_std_unit_price_max, Criteria::LESS_EQUAL);
			}
		}
		if (!empty($filter_sale_method)) {
			if (is_array($filter_sale_method)) {
				$c->add(InventoryItemPeer::SALE_METHOD, $filter_sale_method, Criteria::IN);
			} else {
				if ($filter_sale_method != '-') {
					$c->add(InventoryItemPeer::SALE_METHOD, $filter_sale_method);
				}
			}
		}


		if ($Sorting == 'CREATED_AT') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::CREATED_AT);
		}
		if ($Sorting == 'SKU') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::SKU);
		}
		if ($Sorting == 'TITLE') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::TITLE);
		}
		if ($Sorting == 'BRAND') {
			$c->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, Criteria::LEFT_JOIN);
			$c->addAscendingOrderByColumn(BrandPeer::TITLE);
		}
		if ($Sorting == 'COLOR') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::COLOR);
		}
		if ($Sorting == 'SIZE') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::SIZE);
		}

		// Test HAND
		//if ( $Sorting=='HAND' ) {
		//$c->addAscendingOrderByColumn( InventoryItemPeer::HAND  );
		//}

		if ($Sorting == 'QTY_ON_HAND') {
			$c->addAscendingOrderByColumn(InventoryItemPeer::QTY_ON_HAND);
		}
		if ($Sorting == 'STD_UNIT_PRICE_LOW_TO_HIGN') {
			if ($ModuleName == 'main' and $ActionName == 'sale_prices') {
				$c->addAscendingOrderByColumn(InventoryItemPeer::SALE_PRICE);
				$c->addAscendingOrderByColumn(InventoryItemPeer::CLEARANCE);

			} else {
				$c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
			}
		}

		if ($Sorting == 'STD_UNIT_PRICE_HIGN_TO_LOW') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
		}


		if ($Sorting == 'CLEARANCE') {
			$c->addDescendingOrderByColumn(InventoryItemPeer::CLEARANCE);
		}
		$c->setDistinct();

		// Util::deb($extended_parameters, 'CLEARANCE ++$extended_parameters');
		if (!empty($extended_parameters['selected_brands'])) {
			$c->add(InventoryItemPeer::BRAND_ID, $extended_parameters['selected_brands'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_sizes'])) {
			$c->add(InventoryItemPeer::SIZE, $extended_parameters['selected_sizes'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_colors'])) {
			$c->add(InventoryItemPeer::COLOR, $extended_parameters['selected_colors'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_genders'])) {
			$c->add(InventoryItemPeer::GENDER, $extended_parameters['selected_genders'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_prices'])) {
			$prices_conditions = ' ( ';
			$LLL = count($extended_parameters['selected_prices']);
			for ($III = 0; $III < $LLL; $III++) {
				$MinPrice=  round(floatval( $extended_parameters['selected_prices'][$III] - 25 ), 2);
				$MaxPrice=  round(floatval($extended_parameters['selected_prices'][$III]), 2);
				if ( $MaxPrice< 500 ) {
					$prices_conditions .=  ' ( round(inventory_item.STD_UNIT_PRICE,2) >= '. $MinPrice .
						' AND round(inventory_item.STD_UNIT_PRICE,2) < '. $MaxPrice . " " .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");
				} else {
					$prices_conditions .=  ' ( round(inventory_item.STD_UNIT_PRICE,2) >= '. $MinPrice .
						($III == $LLL - 1 ? ' ) ' : " ) OR ");

				}
			}
			$prices_conditions.= ' ) ';
			$PricesAddCont_Cond = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, $prices_conditions, Criteria::CUSTOM);
			$c->add($PricesAddCont_Cond);
		}

		if (!empty($extended_parameters['selected_sale_prices'])) {
			$c->add( InventoryItemPeer::CLEARANCE, 0, Criteria::GREATER_THAN );
		}
		if (!empty($extended_parameters['selected_finishs'])) {
			$c->add(InventoryItemPeer::FINISH, $extended_parameters['selected_finishs'], Criteria::IN);
		}

		if (!empty($extended_parameters['selected_hands'])) {
			foreach( $extended_parameters['selected_hands'] as $HandName ) {
				if ( strtoupper($HandName) == 'RIGHT' ) {
					$extended_parameters['selected_hands'][]= 'R';
				}
				if ( strtoupper($HandName) == 'LEFT' ) {
					$extended_parameters['selected_hands'][]= 'L';
				}
			}
			$c->add(InventoryItemPeer::HAND, $extended_parameters['selected_hands'], Criteria::IN);
		}

		if ($ReturnCount) {
			return InventoryItemPeer::doCount($c);
		}
		if (!$ReturnPager) {
			return InventoryItemPeer::doSelect($c);
		}
		if (empty($rows_in_pager)) $rows_in_pager = (int)sfConfig::get('app_application_rows_in_pager');
		$pager = new sfNPropelPager('InventoryItem', $rows_in_pager);
		$pager->setPage($page);
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}


	public static function getSimilarInventoryItem($InventoryItemSku, $IsEqual = true, $con = null)
	{
		$c = new Criteria();
		if ($IsEqual) {
			$c->add(InventoryItemPeer::SKU, $InventoryItemSku);
		} else {
			$c->add(InventoryItemPeer::SKU, $InventoryItemSku, Criteria::NOT_EQUAL);
		}
		if ($lResult = InventoryItemPeer::doSelect($c, $con)) {
			return $lResult[0];
		}
		return '';
	}

	public static function getSelectionList($Code = '', $Name = '', $NewCode = '', $NewCodeText = '', $HideOpenedInventoryItems = false, $filter_status = '')
	{
		$List = InventoryItemPeer::getInventoryItems(1, false, false, '', '', '', '', '', '', '', '', '', '', '', $filter_status, '', '', '', $HideOpenedInventoryItems);
		$ResArray = array();
		if (!empty($Code) or !empty($Name)) {
			$ResArray[$Code] = $Name;
		}
		foreach ($List as $lInventoryItem) {
			$ResArray[$lInventoryItem->getId()] = $lInventoryItem;
		}
		if (!empty($NewCode) and !empty($NewCodeText)) {
			$ResArray[$NewCode] = $NewCodeText;
		}
		return $ResArray;
	}

	public static function getColorsListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Color_Cond_1 = $c->getNewCriterion(InventoryItemPeer::COLOR, null, Criteria::NOT_EQUAL);
		$Color_Cond_2 = $c->getNewCriterion(InventoryItemPeer::COLOR, '', Criteria::NOT_EQUAL);
		$Color_Cond_1->addAnd($Color_Cond_2);
		$c->add($Color_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::COLOR);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('color');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = array('color' => $result['color'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getSizesListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Size_Cond_1 = $c->getNewCriterion(InventoryItemPeer::SIZE, null, Criteria::NOT_EQUAL);
		$Size_Cond_2 = $c->getNewCriterion(InventoryItemPeer::SIZE, '', Criteria::NOT_EQUAL);
		$Size_Cond_1->addAnd($Size_Cond_2);
		$c->add($Size_Cond_1);

		//$c->addAscendingOrderByColumn( InventoryItemPeer::SIZE );
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('size');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = array('size' => $result['size'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getInventoryItemBySku( $sku ) {
		$c = new Criteria();
		$c->add(InventoryItemPeer::SKU, $sku );
		return InventoryItemPeer::doSelectOne($c);
	}


	//getOptionsListByRootItem( $RootItem );
	public static function getOptionsListByRootItem($RootItem)
	{
		// Util::deb($RootItem, '$RootItem::');
		$lInventoryItem= InventoryItemPeer::getInventoryItemBySku($RootItem);
		// Util::deb($lInventoryItem, '$lInventoryItem::');
		if( empty($lInventoryItem) ) return array();

		$c = new Criteria();
		//$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		// $Options_Cond_1 = $c->getNewCriterion(InventoryItemPeer::OPTIONS, null, Criteria::NOT_EQUAL);
		// $Options_Cond_2 = $c->getNewCriterion(InventoryItemPeer::OPTIONS, '', Criteria::NOT_EQUAL);
		//$Options_Cond_1->addAnd($Options_Cond_2);
		//$c->add($Options_Cond_1);

		//$c->addAscendingOrderByColumn(InventoryItemPeer::OPTIONS);
	//	$c->addAscendingOrderByColumn( OptionsPeer::TITLE );
		//$c->addJoin( OptionsItemsPeer::OPTION_ID, OptionsPeer::ID, Criteria::LEFT_JOIN  );

		$c->addJoin( OptionsItemsPeer::INVENTORY_ITEM_ID, InventoryItemPeer::ID, Criteria::JOIN);
		$c->add( OptionsItemsPeer::INVENTORY_ITEM_ID, $lInventoryItem->getId() );

//		$c->add( UniquesSkuListPeer::UNIQUE_SESSION_ID, $unique_session_id);

		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('options.title');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		//	die("die -1qq:");
		$result = $stmt->fetch();
		while ($result) {

			$ResArray[$result['sku']] = array('option' => $result['title'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}


	public static function getHandsListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Hand_Cond_1 = $c->getNewCriterion(InventoryItemPeer::HAND, null, Criteria::NOT_EQUAL);
		$Hand_Cond_2 = $c->getNewCriterion(InventoryItemPeer::HAND, '', Criteria::NOT_EQUAL);
		$Hand_Cond_1->addAnd($Hand_Cond_2);
		$c->add($Hand_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::HAND);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('hand');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = array('hand' => $result['hand'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}


	public static function getFinishesListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Finish_Cond_1 = $c->getNewCriterion(InventoryItemPeer::FINISH, null, Criteria::NOT_EQUAL);
		$Finish_Cond_2 = $c->getNewCriterion(InventoryItemPeer::FINISH, '', Criteria::NOT_EQUAL);
		$Finish_Cond_1->addAnd($Finish_Cond_2);
		$c->add($Finish_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::FINISH);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('finish');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = array('finish' => $result['finish'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getGendersListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Gender_Cond_1 = $c->getNewCriterion(InventoryItemPeer::GENDER, null, Criteria::NOT_EQUAL);
		$Gender_Cond_2 = $c->getNewCriterion(InventoryItemPeer::GENDER, '', Criteria::NOT_EQUAL);
		$Gender_Cond_1->addAnd($Gender_Cond_2);
		$c->add($Gender_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::GENDER);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('gender');
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = array('gender' => $result['gender'], 'std_unit_price' => $result['std_unit_price']);
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getDescriptionShortsListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$DescriptionShort_Cond_1 = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, null, Criteria::NOT_EQUAL);
		$DescriptionShort_Cond_2 = $c->getNewCriterion(InventoryItemPeer::DESCRIPTION_SHORT, '', Criteria::NOT_EQUAL);
		$DescriptionShort_Cond_1->addAnd($DescriptionShort_Cond_2);
		$c->add($DescriptionShort_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::DESCRIPTION_SHORT);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('description_short');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = $result['description_short'];
			$result = $stmt->fetch();
		}
		return $ResArray;
	}


	public static function getPaperworksListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$Paperwork_Cond_1 = $c->getNewCriterion(InventoryItemPeer::PAPERWORK, null, Criteria::NOT_EQUAL);
		$Paperwork_Cond_2 = $c->getNewCriterion(InventoryItemPeer::PAPERWORK, '', Criteria::NOT_EQUAL);
		$Paperwork_Cond_1->addAnd($Paperwork_Cond_2);
		$c->add($Paperwork_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::PAPERWORK);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('paperwork');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = $result['paperwork'];
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getUnitMeasuresListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$UnitMeasure_Cond_1 = $c->getNewCriterion(InventoryItemPeer::UNIT_MEASURE, null, Criteria::NOT_EQUAL);
		$UnitMeasure_Cond_2 = $c->getNewCriterion(InventoryItemPeer::UNIT_MEASURE, '', Criteria::NOT_EQUAL);
		$UnitMeasure_Cond_1->addAnd($UnitMeasure_Cond_2);
		$c->add($UnitMeasure_Cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::UNIT_MEASURE);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('unit_measure');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = $result['unit_measure'];
			$result = $stmt->fetch();
		}
		return $ResArray;
	}

	public static function getHandListByRootItem($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$hand_cond_1 = $c->getNewCriterion(InventoryItemPeer::HAND, null, Criteria::NOT_EQUAL);
		$hand_cond_2 = $c->getNewCriterion(InventoryItemPeer::HAND, '', Criteria::NOT_EQUAL);
		$hand_cond_1->addAnd($hand_cond_2);
		$c->add($hand_cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::HAND);
		$c->setDistinct();
		$c->addSelectColumn('sku');
		$c->addSelectColumn('hand');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		while ($result) {
			$ResArray[$result['sku']] = $result['hand'];
			$result = $stmt->fetch();
		}
		return $ResArray;
	}


	/*  public static function getRecentlyViewedList($CurrentSkuToSkip= '') {
			$c = new Criteria();
			$c->add( InventoryItemPeer::RECENTLY_VIEWED , null, Criteria::NOT_EQUAL );
			$Title_Cond_1= $c->getNewCriterion( InventoryItemPeer::TITLE, null, Criteria::NOT_EQUAL );
			$Title_Cond_2= $c->getNewCriterion( InventoryItemPeer::TITLE, '', Criteria::NOT_EQUAL );
			$Title_Cond_1->addAnd( $Title_Cond_2 );
			$c->add( $Title_Cond_1 );

			if ( !empty($CurrentSkuToSkip) ) {
				$c->add( InventoryItemPeer::SKU , $CurrentSkuToSkip, Criteria::NOT_EQUAL );
			}
			$c->addAscendingOrderByColumn(InventoryItemPeer::RECENTLY_VIEWED);
			$c->setLimit( (int)sfConfig::get('app_application_recently_viewed_in_list' ) );

			return InventoryItemPeer::doSelect( $c );
		} */

	public static function getHasDifferentPrices($RootItem)
	{
		$c = new Criteria();
		$c->add(InventoryItemPeer::ROOTITEM, $RootItem);
		$hand_cond_1 = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, null, Criteria::NOT_EQUAL);
		$hand_cond_2 = $c->getNewCriterion(InventoryItemPeer::STD_UNIT_PRICE, '', Criteria::NOT_EQUAL);
		$hand_cond_1->addAnd($hand_cond_2);
		$c->add($hand_cond_1);

		$c->addAscendingOrderByColumn(InventoryItemPeer::STD_UNIT_PRICE);
		$c->setDistinct();
		$c->addSelectColumn('std_unit_price');
		$ResArray = array();
		$stmt = InventoryItemPeer::doSelectStmt($c);
		$result = $stmt->fetch();
		$IsFirstRow = true;
		while ($result) {
			if ($IsFirstRow) {
				$std_unit_price = $result['std_unit_price'];
				$IsFirstRow = false;
			} else {
				if ($std_unit_price != $result['std_unit_price']) return true;
				$std_unit_price = $result['std_unit_price'];
			}
			$result = $stmt->fetch();
		}
		return false;
	}


	public static  function getColorCondition($c, $extended_parameters)
	{
		$Color_Cond = '';
		$hasCondition = false;
		if (in_array('Beige', $extended_parameters['selected_colors'])) {
			$BEIGE_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BEIGE, true);
			$Color_Cond = $BEIGE_Cond;
			$hasCondition = true;
		}
		if (in_array('Black', $extended_parameters['selected_colors'])) {
			$BLACK_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BLACK, true);
			if ($hasCondition) {
				$Color_Cond->addOr($BLACK_Cond);
			} else {
				$Color_Cond = $BLACK_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Blue', $extended_parameters['selected_colors'])) {
			$BLUE_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BLUE, true);
			if ($hasCondition) {
				$Color_Cond->addOr($BLUE_Cond);
			} else {
				$Color_Cond = $BLUE_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Brown', $extended_parameters['selected_colors'])) {
			$BROWN_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_BROWN, true);
			if ($hasCondition) {
				$Color_Cond->addOr($BROWN_Cond);
			} else {
				$Color_Cond = $BROWN_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Camo', $extended_parameters['selected_colors'])) {
			$CAMO_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_CAMO, true);
			if ($hasCondition) {
				$Color_Cond->addOr($CAMO_Cond);
			} else {
				$Color_Cond = $CAMO_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Gold', $extended_parameters['selected_colors'])) {
			$GOLD_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GOLD, true);
			if ($hasCondition) {
				$Color_Cond->addOr($GOLD_Cond);
			} else {
				$Color_Cond = $GOLD_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Gray', $extended_parameters['selected_colors'])) {
			$GRAY_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GRAY, true);
			if ($hasCondition) {
				$Color_Cond->addOr($GRAY_Cond);
			} else {
				$Color_Cond = $GRAY_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Green', $extended_parameters['selected_colors'])) {
			$GREEN_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_GREEN, true);
			if ($hasCondition) {
				$Color_Cond->addOr($GREEN_Cond);
			} else {
				$Color_Cond = $GREEN_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Orange', $extended_parameters['selected_colors'])) {
			$ORANGE_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_ORANGE, true);
			if ($hasCondition) {
				$Color_Cond->addOr($ORANGE_Cond);
			} else {
				$Color_Cond = $ORANGE_Cond;
			}
			$hasCondition = true;
		}

		if (in_array('Pink', $extended_parameters['selected_colors'])) {
			$PINK_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_PINK, true);
			if ($hasCondition) {
				$Color_Cond->addOr($PINK_Cond);
			} else {
				$Color_Cond = $PINK_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Purple', $extended_parameters['selected_colors'])) {
			$PURPLE_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_PURPLE, true);
			if ($hasCondition) {
				$Color_Cond->addOr($PURPLE_Cond);
			} else {
				$Color_Cond = $PURPLE_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Red', $extended_parameters['selected_colors'])) {
			$RED_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_RED, true);
			//Util::deb( $hasCondition, ' ++INSIDE $hasCondition::');
			if ($hasCondition) {
				$Color_Cond->addOr($RED_Cond);
			} else {
				$Color_Cond = $RED_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Silver', $extended_parameters['selected_colors'])) {
			$SILVER_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_SILVER, true);
			if ($hasCondition) {
				$Color_Cond->addOr($SILVER_Cond);
			} else {
				$Color_Cond = $SILVER_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('White', $extended_parameters['selected_colors'])) {
			$WHITE_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_WHITE, true);
			if ($hasCondition) {
				$Color_Cond->addOr($WHITE_Cond);
			} else {
				$Color_Cond = $WHITE_Cond;
			}
			$hasCondition = true;
		}
		if (in_array('Yellow', $extended_parameters['selected_colors'])) {
			$YELLOW_Cond = $c->getNewCriterion(InventoryItemPeer::UDF_IT_SWC_YELLOW, true);
			if ($hasCondition) {
				$Color_Cond->addOr($YELLOW_Cond);
			} else {
				$Color_Cond = $YELLOW_Cond;
			}
			$hasCondition = true;
		}
		if ( $hasCondition ) {
			return $Color_Cond;
		}
		return '';
	} //

} // InventoryItemPeer




