<?php

require 'lib/model/om/BaseColorMappingPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'color_mapping' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ColorMappingPeer extends BaseColorMappingPeer {

	public static function getColorMappings( $page=1, $ReturnPager=true, $Limit= '' ) {
		$c = new Criteria();
		$c->addAscendingOrderByColumn(ColorMappingPeer::COLOR );
		if ( !empty($Limit) ) {
			$c->setLimit($Limit);
		}
		if ( !$ReturnPager ) {
			return ColorMappingPeer::doSelect( $c );
		}
		$pager = new sfPropelPager( 'ColorMapping', (int)sfConfig::get('app_application_rows_in_pager' ) );
		$pager->setPage( $page );
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

	public static function getSimilarColorMapping($ColorMapping, $con = null)
	{
		$c = new Criteria();
		$c->add( ColorMappingPeer::COLOR , $ColorMapping );
		if($lResult = ColorMappingPeer::doSelectOne( $c, $con ) )
		{
			return $lResult;
		}
		return '';
	}

} // ColorMappingPeer
