<?php

require 'lib/model/om/BaseBrand.php';


/**
 * Skeleton subclass for representing a row from the 'brand' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class Brand extends BaseBrand {

	public function __toString()
	{
	  return $this->getTitle();
	}
    
} // Brand
