<?php

require 'lib/model/om/BaseBrandPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'brand' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class BrandPeer extends BaseBrandPeer {

    public static function getBrands( $page=1, $ReturnPager=true, $filter_title='', $Sorting='TITLE' ) {
    $c = new Criteria();
    if ( !empty($filter_title) and $filter_title!= '-' ) {
      $c->add( BrandPeer::TITLE , '%'.$filter_title.'%', Criteria::LIKE );
    }
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(BrandPeer::CREATED_AT);
      $c->addDescendingOrderByColumn(BrandPeer::ID);
    }
    if ( $Sorting=='TITLE' ) {
      $c->addAscendingOrderByColumn(BrandPeer::TITLE );
    }
    if ( !$ReturnPager ) {
      return BrandPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'Brand', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }

  public static function getSimilarBrand($BrandTitle, $pBrandId, $IsEqual=true, $con = null)
  {
    $c = new Criteria();
    $c->add( BrandPeer::TITLE , $BrandTitle );
    if ( $IsEqual ) {
      $c->add( BrandPeer::ID, $pBrandId );
    } else {
      $c->add( BrandPeer::ID, $pBrandId, Criteria::NOT_EQUAL );
    }
    if($lResult = BrandPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getSelectionList( $Code, $Title, $NewCode='', $NewCodeText='' ) {
    $List= BrandPeer::getBrands( 1, false );
    $ResArray= array();
    if ( !empty($Code) or !empty($Title) ) {
      $ResArray[$Code]= $Title;
    }
    foreach( $List as $lBrand ) {
      $ResArray[ $lBrand->getId() ]= $lBrand->getTitle();
    }
    if ( !empty($NewCode) and !empty($NewCodeText) ) {
      $ResArray[ $NewCode ]= $NewCodeText;      
    }
    return $ResArray;
  }

} // BrandPeer
