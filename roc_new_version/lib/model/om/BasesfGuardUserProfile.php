<?php

/**
 * Base class that represents a row from the 'sf_guard_user_profile' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BasesfGuardUserProfile extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        sfGuardUserProfilePeer
	 */
	protected static $peer;

	/**
	 * The value for the user_id field.
	 * @var        int
	 */
	protected $user_id;

	/**
	 * The value for the first_name field.
	 * @var        string
	 */
	protected $first_name;

	/**
	 * The value for the last_name field.
	 * @var        string
	 */
	protected $last_name;

	/**
	 * The value for the b_country field.
	 * @var        string
	 */
	protected $b_country;

	/**
	 * The value for the b_street field.
	 * @var        string
	 */
	protected $b_street;

	/**
	 * The value for the b_street_2 field.
	 * @var        string
	 */
	protected $b_street_2;

	/**
	 * The value for the b_city field.
	 * @var        string
	 */
	protected $b_city;

	/**
	 * The value for the b_state field.
	 * @var        string
	 */
	protected $b_state;

	/**
	 * The value for the b_zip field.
	 * @var        string
	 */
	protected $b_zip;

	/**
	 * The value for the phone field.
	 * @var        string
	 */
	protected $phone;

	/**
	 * The value for the fax field.
	 * @var        string
	 */
	protected $fax;

	/**
	 * The value for the url field.
	 * @var        string
	 */
	protected $url;

	/**
	 * The value for the s_country field.
	 * @var        string
	 */
	protected $s_country;

	/**
	 * The value for the s_street field.
	 * @var        string
	 */
	protected $s_street;

	/**
	 * The value for the s_street_2 field.
	 * @var        string
	 */
	protected $s_street_2;

	/**
	 * The value for the s_city field.
	 * @var        string
	 */
	protected $s_city;

	/**
	 * The value for the s_state field.
	 * @var        string
	 */
	protected $s_state;

	/**
	 * The value for the s_zip field.
	 * @var        string
	 */
	protected $s_zip;

	/**
	 * The value for the updated_at field.
	 * @var        string
	 */
	protected $updated_at;

	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;

	/**
	 * @var        sfGuardUser
	 */
	protected $asfGuardUser;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	// symfony behavior
	
	const PEER = 'sfGuardUserProfilePeer';

	/**
	 * Get the [user_id] column value.
	 * 
	 * @return     int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * Get the [first_name] column value.
	 * 
	 * @return     string
	 */
	public function getFirstName()
	{
		return $this->first_name;
	}

	/**
	 * Get the [last_name] column value.
	 * 
	 * @return     string
	 */
	public function getLastName()
	{
		return $this->last_name;
	}

	/**
	 * Get the [b_country] column value.
	 * 
	 * @return     string
	 */
	public function getBCountry()
	{
		return $this->b_country;
	}

	/**
	 * Get the [b_street] column value.
	 * 
	 * @return     string
	 */
	public function getBStreet()
	{
		return $this->b_street;
	}

	/**
	 * Get the [b_street_2] column value.
	 * 
	 * @return     string
	 */
	public function getBStreet2()
	{
		return $this->b_street_2;
	}

	/**
	 * Get the [b_city] column value.
	 * 
	 * @return     string
	 */
	public function getBCity()
	{
		return $this->b_city;
	}

	/**
	 * Get the [b_state] column value.
	 * 
	 * @return     string
	 */
	public function getBState()
	{
		return $this->b_state;
	}

	/**
	 * Get the [b_zip] column value.
	 * 
	 * @return     string
	 */
	public function getBZip()
	{
		return $this->b_zip;
	}

	/**
	 * Get the [phone] column value.
	 * 
	 * @return     string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * Get the [fax] column value.
	 * 
	 * @return     string
	 */
	public function getFax()
	{
		return $this->fax;
	}

	/**
	 * Get the [url] column value.
	 * 
	 * @return     string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * Get the [s_country] column value.
	 * 
	 * @return     string
	 */
	public function getSCountry()
	{
		return $this->s_country;
	}

	/**
	 * Get the [s_street] column value.
	 * 
	 * @return     string
	 */
	public function getSStreet()
	{
		return $this->s_street;
	}

	/**
	 * Get the [s_street_2] column value.
	 * 
	 * @return     string
	 */
	public function getSStreet2()
	{
		return $this->s_street_2;
	}

	/**
	 * Get the [s_city] column value.
	 * 
	 * @return     string
	 */
	public function getSCity()
	{
		return $this->s_city;
	}

	/**
	 * Get the [s_state] column value.
	 * 
	 * @return     string
	 */
	public function getSState()
	{
		return $this->s_state;
	}

	/**
	 * Get the [s_zip] column value.
	 * 
	 * @return     string
	 */
	public function getSZip()
	{
		return $this->s_zip;
	}

	/**
	 * Get the [optionally formatted] temporal [updated_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->updated_at === null) {
			return null;
		}


		if ($this->updated_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->updated_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set the value of [user_id] column.
	 * 
	 * @param      int $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setUserId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::USER_ID;
		}

		if ($this->asfGuardUser !== null && $this->asfGuardUser->getId() !== $v) {
			$this->asfGuardUser = null;
		}

		return $this;
	} // setUserId()

	/**
	 * Set the value of [first_name] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setFirstName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->first_name !== $v) {
			$this->first_name = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::FIRST_NAME;
		}

		return $this;
	} // setFirstName()

	/**
	 * Set the value of [last_name] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setLastName($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->last_name !== $v) {
			$this->last_name = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::LAST_NAME;
		}

		return $this;
	} // setLastName()

	/**
	 * Set the value of [b_country] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBCountry($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_country !== $v) {
			$this->b_country = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_COUNTRY;
		}

		return $this;
	} // setBCountry()

	/**
	 * Set the value of [b_street] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBStreet($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_street !== $v) {
			$this->b_street = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_STREET;
		}

		return $this;
	} // setBStreet()

	/**
	 * Set the value of [b_street_2] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBStreet2($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_street_2 !== $v) {
			$this->b_street_2 = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_STREET_2;
		}

		return $this;
	} // setBStreet2()

	/**
	 * Set the value of [b_city] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBCity($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_city !== $v) {
			$this->b_city = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_CITY;
		}

		return $this;
	} // setBCity()

	/**
	 * Set the value of [b_state] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBState($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_state !== $v) {
			$this->b_state = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_STATE;
		}

		return $this;
	} // setBState()

	/**
	 * Set the value of [b_zip] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setBZip($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_zip !== $v) {
			$this->b_zip = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::B_ZIP;
		}

		return $this;
	} // setBZip()

	/**
	 * Set the value of [phone] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setPhone($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->phone !== $v) {
			$this->phone = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::PHONE;
		}

		return $this;
	} // setPhone()

	/**
	 * Set the value of [fax] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setFax($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->fax !== $v) {
			$this->fax = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::FAX;
		}

		return $this;
	} // setFax()

	/**
	 * Set the value of [url] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setUrl($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->url !== $v) {
			$this->url = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::URL;
		}

		return $this;
	} // setUrl()

	/**
	 * Set the value of [s_country] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSCountry($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_country !== $v) {
			$this->s_country = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_COUNTRY;
		}

		return $this;
	} // setSCountry()

	/**
	 * Set the value of [s_street] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSStreet($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_street !== $v) {
			$this->s_street = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_STREET;
		}

		return $this;
	} // setSStreet()

	/**
	 * Set the value of [s_street_2] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSStreet2($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_street_2 !== $v) {
			$this->s_street_2 = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_STREET_2;
		}

		return $this;
	} // setSStreet2()

	/**
	 * Set the value of [s_city] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSCity($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_city !== $v) {
			$this->s_city = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_CITY;
		}

		return $this;
	} // setSCity()

	/**
	 * Set the value of [s_state] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSState($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_state !== $v) {
			$this->s_state = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_STATE;
		}

		return $this;
	} // setSState()

	/**
	 * Set the value of [s_zip] column.
	 * 
	 * @param      string $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setSZip($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_zip !== $v) {
			$this->s_zip = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::S_ZIP;
		}

		return $this;
	} // setSZip()

	/**
	 * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setUpdatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->updated_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->updated_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = sfGuardUserProfilePeer::UPDATED_AT;
			}
		} // if either are not null

		return $this;
	} // setUpdatedAt()

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 */
	public function setId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = sfGuardUserProfilePeer::ID;
		}

		return $this;
	} // setId()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->user_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->first_name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->last_name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->b_country = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->b_street = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->b_street_2 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->b_city = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->b_state = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->b_zip = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->phone = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->fax = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
			$this->url = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
			$this->s_country = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
			$this->s_street = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
			$this->s_street_2 = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
			$this->s_city = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
			$this->s_state = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
			$this->s_zip = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
			$this->updated_at = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
			$this->id = ($row[$startcol + 19] !== null) ? (int) $row[$startcol + 19] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 20; // 20 = sfGuardUserProfilePeer::NUM_COLUMNS - sfGuardUserProfilePeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating sfGuardUserProfile object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->asfGuardUser !== null && $this->user_id !== $this->asfGuardUser->getId()) {
			$this->asfGuardUser = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(sfGuardUserProfilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = sfGuardUserProfilePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->asfGuardUser = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(sfGuardUserProfilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				sfGuardUserProfilePeer::doDelete($this, $con);
				$this->postDelete($con);
				$this->setDeleted(true);
				$con->commit();
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(sfGuardUserProfilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			// symfony_timestampable behavior
			if ($this->isModified() && !$this->isColumnModified(sfGuardUserProfilePeer::UPDATED_AT))
			{
			  $this->setUpdatedAt(time());
			}

			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
				// symfony_timestampable behavior
				
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				sfGuardUserProfilePeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if ($this->asfGuardUser->isModified() || $this->asfGuardUser->isNew()) {
					$affectedRows += $this->asfGuardUser->save($con);
				}
				$this->setsfGuardUser($this->asfGuardUser);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = sfGuardUserProfilePeer::ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = sfGuardUserProfilePeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += sfGuardUserProfilePeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if (!$this->asfGuardUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->asfGuardUser->getValidationFailures());
				}
			}


			if (($retval = sfGuardUserProfilePeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = sfGuardUserProfilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getUserId();
				break;
			case 1:
				return $this->getFirstName();
				break;
			case 2:
				return $this->getLastName();
				break;
			case 3:
				return $this->getBCountry();
				break;
			case 4:
				return $this->getBStreet();
				break;
			case 5:
				return $this->getBStreet2();
				break;
			case 6:
				return $this->getBCity();
				break;
			case 7:
				return $this->getBState();
				break;
			case 8:
				return $this->getBZip();
				break;
			case 9:
				return $this->getPhone();
				break;
			case 10:
				return $this->getFax();
				break;
			case 11:
				return $this->getUrl();
				break;
			case 12:
				return $this->getSCountry();
				break;
			case 13:
				return $this->getSStreet();
				break;
			case 14:
				return $this->getSStreet2();
				break;
			case 15:
				return $this->getSCity();
				break;
			case 16:
				return $this->getSState();
				break;
			case 17:
				return $this->getSZip();
				break;
			case 18:
				return $this->getUpdatedAt();
				break;
			case 19:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                        BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. Defaults to BasePeer::TYPE_PHPNAME.
	 * @param      boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns.  Defaults to TRUE.
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true)
	{
		$keys = sfGuardUserProfilePeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getUserId(),
			$keys[1] => $this->getFirstName(),
			$keys[2] => $this->getLastName(),
			$keys[3] => $this->getBCountry(),
			$keys[4] => $this->getBStreet(),
			$keys[5] => $this->getBStreet2(),
			$keys[6] => $this->getBCity(),
			$keys[7] => $this->getBState(),
			$keys[8] => $this->getBZip(),
			$keys[9] => $this->getPhone(),
			$keys[10] => $this->getFax(),
			$keys[11] => $this->getUrl(),
			$keys[12] => $this->getSCountry(),
			$keys[13] => $this->getSStreet(),
			$keys[14] => $this->getSStreet2(),
			$keys[15] => $this->getSCity(),
			$keys[16] => $this->getSState(),
			$keys[17] => $this->getSZip(),
			$keys[18] => $this->getUpdatedAt(),
			$keys[19] => $this->getId(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = sfGuardUserProfilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setUserId($value);
				break;
			case 1:
				$this->setFirstName($value);
				break;
			case 2:
				$this->setLastName($value);
				break;
			case 3:
				$this->setBCountry($value);
				break;
			case 4:
				$this->setBStreet($value);
				break;
			case 5:
				$this->setBStreet2($value);
				break;
			case 6:
				$this->setBCity($value);
				break;
			case 7:
				$this->setBState($value);
				break;
			case 8:
				$this->setBZip($value);
				break;
			case 9:
				$this->setPhone($value);
				break;
			case 10:
				$this->setFax($value);
				break;
			case 11:
				$this->setUrl($value);
				break;
			case 12:
				$this->setSCountry($value);
				break;
			case 13:
				$this->setSStreet($value);
				break;
			case 14:
				$this->setSStreet2($value);
				break;
			case 15:
				$this->setSCity($value);
				break;
			case 16:
				$this->setSState($value);
				break;
			case 17:
				$this->setSZip($value);
				break;
			case 18:
				$this->setUpdatedAt($value);
				break;
			case 19:
				$this->setId($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = sfGuardUserProfilePeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setUserId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setFirstName($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setLastName($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBCountry($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setBStreet($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setBStreet2($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setBCity($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setBState($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setBZip($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setPhone($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setFax($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setUrl($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSCountry($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSStreet($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSStreet2($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSCity($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setSState($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setSZip($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setUpdatedAt($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setId($arr[$keys[19]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(sfGuardUserProfilePeer::DATABASE_NAME);

		if ($this->isColumnModified(sfGuardUserProfilePeer::USER_ID)) $criteria->add(sfGuardUserProfilePeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(sfGuardUserProfilePeer::FIRST_NAME)) $criteria->add(sfGuardUserProfilePeer::FIRST_NAME, $this->first_name);
		if ($this->isColumnModified(sfGuardUserProfilePeer::LAST_NAME)) $criteria->add(sfGuardUserProfilePeer::LAST_NAME, $this->last_name);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_COUNTRY)) $criteria->add(sfGuardUserProfilePeer::B_COUNTRY, $this->b_country);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_STREET)) $criteria->add(sfGuardUserProfilePeer::B_STREET, $this->b_street);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_STREET_2)) $criteria->add(sfGuardUserProfilePeer::B_STREET_2, $this->b_street_2);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_CITY)) $criteria->add(sfGuardUserProfilePeer::B_CITY, $this->b_city);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_STATE)) $criteria->add(sfGuardUserProfilePeer::B_STATE, $this->b_state);
		if ($this->isColumnModified(sfGuardUserProfilePeer::B_ZIP)) $criteria->add(sfGuardUserProfilePeer::B_ZIP, $this->b_zip);
		if ($this->isColumnModified(sfGuardUserProfilePeer::PHONE)) $criteria->add(sfGuardUserProfilePeer::PHONE, $this->phone);
		if ($this->isColumnModified(sfGuardUserProfilePeer::FAX)) $criteria->add(sfGuardUserProfilePeer::FAX, $this->fax);
		if ($this->isColumnModified(sfGuardUserProfilePeer::URL)) $criteria->add(sfGuardUserProfilePeer::URL, $this->url);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_COUNTRY)) $criteria->add(sfGuardUserProfilePeer::S_COUNTRY, $this->s_country);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_STREET)) $criteria->add(sfGuardUserProfilePeer::S_STREET, $this->s_street);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_STREET_2)) $criteria->add(sfGuardUserProfilePeer::S_STREET_2, $this->s_street_2);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_CITY)) $criteria->add(sfGuardUserProfilePeer::S_CITY, $this->s_city);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_STATE)) $criteria->add(sfGuardUserProfilePeer::S_STATE, $this->s_state);
		if ($this->isColumnModified(sfGuardUserProfilePeer::S_ZIP)) $criteria->add(sfGuardUserProfilePeer::S_ZIP, $this->s_zip);
		if ($this->isColumnModified(sfGuardUserProfilePeer::UPDATED_AT)) $criteria->add(sfGuardUserProfilePeer::UPDATED_AT, $this->updated_at);
		if ($this->isColumnModified(sfGuardUserProfilePeer::ID)) $criteria->add(sfGuardUserProfilePeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(sfGuardUserProfilePeer::DATABASE_NAME);

		$criteria->add(sfGuardUserProfilePeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of sfGuardUserProfile (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserId($this->user_id);

		$copyObj->setFirstName($this->first_name);

		$copyObj->setLastName($this->last_name);

		$copyObj->setBCountry($this->b_country);

		$copyObj->setBStreet($this->b_street);

		$copyObj->setBStreet2($this->b_street_2);

		$copyObj->setBCity($this->b_city);

		$copyObj->setBState($this->b_state);

		$copyObj->setBZip($this->b_zip);

		$copyObj->setPhone($this->phone);

		$copyObj->setFax($this->fax);

		$copyObj->setUrl($this->url);

		$copyObj->setSCountry($this->s_country);

		$copyObj->setSStreet($this->s_street);

		$copyObj->setSStreet2($this->s_street_2);

		$copyObj->setSCity($this->s_city);

		$copyObj->setSState($this->s_state);

		$copyObj->setSZip($this->s_zip);

		$copyObj->setUpdatedAt($this->updated_at);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     sfGuardUserProfile Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     sfGuardUserProfilePeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new sfGuardUserProfilePeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a sfGuardUser object.
	 *
	 * @param      sfGuardUser $v
	 * @return     sfGuardUserProfile The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setsfGuardUser(sfGuardUser $v = null)
	{
		if ($v === null) {
			$this->setUserId(NULL);
		} else {
			$this->setUserId($v->getId());
		}

		$this->asfGuardUser = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the sfGuardUser object, it will not be re-added.
		if ($v !== null) {
			$v->addsfGuardUserProfile($this);
		}

		return $this;
	}


	/**
	 * Get the associated sfGuardUser object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     sfGuardUser The associated sfGuardUser object.
	 * @throws     PropelException
	 */
	public function getsfGuardUser(PropelPDO $con = null)
	{
		if ($this->asfGuardUser === null && ($this->user_id !== null)) {
			$this->asfGuardUser = sfGuardUserPeer::retrieveByPk($this->user_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->asfGuardUser->addsfGuardUserProfiles($this);
			 */
		}
		return $this->asfGuardUser;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

			$this->asfGuardUser = null;
	}

} // BasesfGuardUserProfile
