<?php

/**
 * Base class that represents a row from the 'inventory_item' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseInventoryItem extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        InventoryItemPeer
	 */
	protected static $peer;

	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;

	/**
	 * The value for the sku field.
	 * @var        string
	 */
	protected $sku;

	/**
	 * The value for the title field.
	 * @var        string
	 */
	protected $title;

	/**
	 * The value for the brand_id field.
	 * @var        int
	 */
	protected $brand_id;

	/**
	 * The value for the description_short field.
	 * @var        string
	 */
	protected $description_short;

	/**
	 * The value for the description_long field.
	 * @var        string
	 */
	protected $description_long;

	/**
	 * The value for the size field.
	 * @var        string
	 */
	protected $size;

	/**
	 * The value for the color field.
	 * @var        string
	 */
	protected $color;

	/**
	 * The value for the gender field.
	 * @var        string
	 */
	protected $gender;

	/**
	 * The value for the sizing_id field.
	 * @var        int
	 */
	protected $sizing_id;

	/**
	 * The value for the customizable field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $customizable;

	/**
	 * The value for the video field.
	 * @var        string
	 */
	protected $video;

	/**
	 * The value for the measure_id field.
	 * @var        int
	 */
	protected $measure_id;

	/**
	 * The value for the qty_on_hand field.
	 * @var        int
	 */
	protected $qty_on_hand;

	/**
	 * The value for the std_unit_price field.
	 * @var        double
	 */
	protected $std_unit_price;

	/**
	 * The value for the msrp field.
	 * @var        double
	 */
	protected $msrp;

	/**
	 * The value for the clearance field.
	 * @var        double
	 */
	protected $clearance;

	/**
	 * The value for the sale_start_date field.
	 * @var        string
	 */
	protected $sale_start_date;

	/**
	 * The value for the sale_end_date field.
	 * @var        string
	 */
	protected $sale_end_date;

	/**
	 * The value for the sale_method field.
	 * @var        string
	 */
	protected $sale_method;

	/**
	 * The value for the sale_price field.
	 * @var        double
	 */
	protected $sale_price;

	/**
	 * The value for the sale_percent field.
	 * @var        double
	 */
	protected $sale_percent;

	/**
	 * The value for the tax_class field.
	 * @var        string
	 */
	protected $tax_class;

	/**
	 * The value for the ship_weight field.
	 * @var        string
	 */
	protected $ship_weight;

	/**
	 * The value for the sale_promo_discount field.
	 * @var        double
	 */
	protected $sale_promo_discount;

	/**
	 * The value for the finish field.
	 * @var        string
	 */
	protected $finish;

	/**
	 * The value for the documents field.
	 * @var        string
	 */
	protected $documents;

	/**
	 * The value for the hand field.
	 * @var        string
	 */
	protected $hand;

	/**
	 * The value for the matrix field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $matrix;

	/**
	 * The value for the rootitem field.
	 * @var        string
	 */
	protected $rootitem;

	/**
	 * The value for the features field.
	 * @var        string
	 */
	protected $features;

	/**
	 * The value for the setup_charge field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $setup_charge;

	/**
	 * The value for the date_updated field.
	 * @var        string
	 */
	protected $date_updated;

	/**
	 * The value for the time_updated field.
	 * Note: this column has a database default value of: 0
	 * @var        double
	 */
	protected $time_updated;

	/**
	 * The value for the recently_viewed field.
	 * @var        string
	 */
	protected $recently_viewed;

	/**
	 * The value for the additional_freight field.
	 * @var        double
	 */
	protected $additional_freight;

	/**
	 * The value for the free_shipping field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $free_shipping;

	/**
	 * The value for the phone_order field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $phone_order;

	/**
	 * The value for the udf_it_swc_beige field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_beige;

	/**
	 * The value for the udf_it_swc_black field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_black;

	/**
	 * The value for the udf_it_swc_blue field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_blue;

	/**
	 * The value for the udf_it_swc_brown field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_brown;

	/**
	 * The value for the udf_it_swc_camo field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_camo;

	/**
	 * The value for the udf_it_swc_gold field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_gold;

	/**
	 * The value for the udf_it_swc_gray field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_gray;

	/**
	 * The value for the udf_it_swc_green field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_green;

	/**
	 * The value for the udf_it_swc_orange field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_orange;

	/**
	 * The value for the udf_it_swc_pink field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_pink;

	/**
	 * The value for the udf_it_swc_purple field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_purple;

	/**
	 * The value for the udf_it_swc_red field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_red;

	/**
	 * The value for the udf_it_swc_silver field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_silver;

	/**
	 * The value for the udf_it_swc_white field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_white;

	/**
	 * The value for the udf_it_swc_yellow field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $udf_it_swc_yellow;

	/**
	 * The value for the created_at field.
	 * @var        string
	 */
	protected $created_at;

	/**
	 * The value for the updated_at field.
	 * @var        string
	 */
	protected $updated_at;

	/**
	 * @var        Brand
	 */
	protected $aBrand;

	/**
	 * @var        Sizings
	 */
	protected $aSizings;

	/**
	 * @var        Measures
	 */
	protected $aMeasures;

	/**
	 * @var        array OrderedItemOptionsItems[] Collection to store aggregation of OrderedItemOptionsItems objects.
	 */
	protected $collOrderedItemOptionsItemss;

	/**
	 * @var        Criteria The criteria used to select the current contents of collOrderedItemOptionsItemss.
	 */
	private $lastOrderedItemOptionsItemsCriteria = null;

	/**
	 * @var        array InventoryItemPaperwork[] Collection to store aggregation of InventoryItemPaperwork objects.
	 */
	protected $collInventoryItemPaperworks;

	/**
	 * @var        Criteria The criteria used to select the current contents of collInventoryItemPaperworks.
	 */
	private $lastInventoryItemPaperworkCriteria = null;

	/**
	 * @var        array InventoryItemInventoryCategory[] Collection to store aggregation of InventoryItemInventoryCategory objects.
	 */
	protected $collInventoryItemInventoryCategorys;

	/**
	 * @var        Criteria The criteria used to select the current contents of collInventoryItemInventoryCategorys.
	 */
	private $lastInventoryItemInventoryCategoryCriteria = null;

	/**
	 * @var        array OptionsItems[] Collection to store aggregation of OptionsItems objects.
	 */
	protected $collOptionsItemss;

	/**
	 * @var        Criteria The criteria used to select the current contents of collOptionsItemss.
	 */
	private $lastOptionsItemsCriteria = null;

	/**
	 * @var        array OptionsValue[] Collection to store aggregation of OptionsValue objects.
	 */
	protected $collOptionsValues;

	/**
	 * @var        Criteria The criteria used to select the current contents of collOptionsValues.
	 */
	private $lastOptionsValueCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	// symfony behavior
	
	const PEER = 'InventoryItemPeer';

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->customizable = false;
		$this->matrix = false;
		$this->setup_charge = false;
		$this->time_updated = 0;
		$this->free_shipping = false;
		$this->phone_order = false;
		$this->udf_it_swc_beige = false;
		$this->udf_it_swc_black = false;
		$this->udf_it_swc_blue = false;
		$this->udf_it_swc_brown = false;
		$this->udf_it_swc_camo = false;
		$this->udf_it_swc_gold = false;
		$this->udf_it_swc_gray = false;
		$this->udf_it_swc_green = false;
		$this->udf_it_swc_orange = false;
		$this->udf_it_swc_pink = false;
		$this->udf_it_swc_purple = false;
		$this->udf_it_swc_red = false;
		$this->udf_it_swc_silver = false;
		$this->udf_it_swc_white = false;
		$this->udf_it_swc_yellow = false;
	}

	/**
	 * Initializes internal state of BaseInventoryItem object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get the [sku] column value.
	 * 
	 * @return     string
	 */
	public function getSku()
	{
		return $this->sku;
	}

	/**
	 * Get the [title] column value.
	 * 
	 * @return     string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Get the [brand_id] column value.
	 * 
	 * @return     int
	 */
	public function getBrandId()
	{
		return $this->brand_id;
	}

	/**
	 * Get the [description_short] column value.
	 * 
	 * @return     string
	 */
	public function getDescriptionShort()
	{
		return $this->description_short;
	}

	/**
	 * Get the [description_long] column value.
	 * 
	 * @return     string
	 */
	public function getDescriptionLong()
	{
		return $this->description_long;
	}

	/**
	 * Get the [size] column value.
	 * 
	 * @return     string
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 * Get the [color] column value.
	 * 
	 * @return     string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * Get the [gender] column value.
	 * 
	 * @return     string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Get the [sizing_id] column value.
	 * 
	 * @return     int
	 */
	public function getSizingId()
	{
		return $this->sizing_id;
	}

	/**
	 * Get the [customizable] column value.
	 * 
	 * @return     boolean
	 */
	public function getCustomizable()
	{
		return $this->customizable;
	}

	/**
	 * Get the [video] column value.
	 * 
	 * @return     string
	 */
	public function getVideo()
	{
		return $this->video;
	}

	/**
	 * Get the [measure_id] column value.
	 * 
	 * @return     int
	 */
	public function getMeasureId()
	{
		return $this->measure_id;
	}

	/**
	 * Get the [qty_on_hand] column value.
	 * 
	 * @return     int
	 */
	public function getQtyOnHand()
	{
		return $this->qty_on_hand;
	}

	/**
	 * Get the [std_unit_price] column value.
	 * 
	 * @return     double
	 */
	public function getStdUnitPrice()
	{
		return $this->std_unit_price;
	}

	/**
	 * Get the [msrp] column value.
	 * 
	 * @return     double
	 */
	public function getMsrp()
	{
		return $this->msrp;
	}

	/**
	 * Get the [clearance] column value.
	 * 
	 * @return     double
	 */
	public function getClearance()
	{
		return $this->clearance;
	}

	/**
	 * Get the [optionally formatted] temporal [sale_start_date] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getSaleStartDate($format = 'Y-m-d')
	{
		if ($this->sale_start_date === null) {
			return null;
		}


		if ($this->sale_start_date === '0000-00-00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->sale_start_date);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->sale_start_date, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [sale_end_date] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getSaleEndDate($format = 'Y-m-d')
	{
		if ($this->sale_end_date === null) {
			return null;
		}


		if ($this->sale_end_date === '0000-00-00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->sale_end_date);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->sale_end_date, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [sale_method] column value.
	 * 
	 * @return     string
	 */
	public function getSaleMethod()
	{
		return $this->sale_method;
	}

	/**
	 * Get the [sale_price] column value.
	 * 
	 * @return     double
	 */
	public function getSalePrice()
	{
		return $this->sale_price;
	}

	/**
	 * Get the [sale_percent] column value.
	 * 
	 * @return     double
	 */
	public function getSalePercent()
	{
		return $this->sale_percent;
	}

	/**
	 * Get the [tax_class] column value.
	 * 
	 * @return     string
	 */
	public function getTaxClass()
	{
		return $this->tax_class;
	}

	/**
	 * Get the [ship_weight] column value.
	 * 
	 * @return     string
	 */
	public function getShipWeight()
	{
		return $this->ship_weight;
	}

	/**
	 * Get the [sale_promo_discount] column value.
	 * 
	 * @return     double
	 */
	public function getSalePromoDiscount()
	{
		return $this->sale_promo_discount;
	}

	/**
	 * Get the [finish] column value.
	 * 
	 * @return     string
	 */
	public function getFinish()
	{
		return $this->finish;
	}

	/**
	 * Get the [documents] column value.
	 * 
	 * @return     string
	 */
	public function getDocuments()
	{
		return $this->documents;
	}

	/**
	 * Get the [hand] column value.
	 * 
	 * @return     string
	 */
	public function getHand()
	{
		return $this->hand;
	}

	/**
	 * Get the [matrix] column value.
	 * 
	 * @return     boolean
	 */
	public function getMatrix()
	{
		return $this->matrix;
	}

	/**
	 * Get the [rootitem] column value.
	 * 
	 * @return     string
	 */
	public function getRootitem()
	{
		return $this->rootitem;
	}

	/**
	 * Get the [features] column value.
	 * 
	 * @return     string
	 */
	public function getFeatures()
	{
		return $this->features;
	}

	/**
	 * Get the [setup_charge] column value.
	 * 
	 * @return     boolean
	 */
	public function getSetupCharge()
	{
		return $this->setup_charge;
	}

	/**
	 * Get the [optionally formatted] temporal [date_updated] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getDateUpdated($format = 'Y-m-d H:i:s')
	{
		if ($this->date_updated === null) {
			return null;
		}


		if ($this->date_updated === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->date_updated);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_updated, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [time_updated] column value.
	 * 
	 * @return     double
	 */
	public function getTimeUpdated()
	{
		return $this->time_updated;
	}

	/**
	 * Get the [optionally formatted] temporal [recently_viewed] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getRecentlyViewed($format = 'Y-m-d H:i:s')
	{
		if ($this->recently_viewed === null) {
			return null;
		}


		if ($this->recently_viewed === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->recently_viewed);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->recently_viewed, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [additional_freight] column value.
	 * 
	 * @return     double
	 */
	public function getAdditionalFreight()
	{
		return $this->additional_freight;
	}

	/**
	 * Get the [free_shipping] column value.
	 * 
	 * @return     boolean
	 */
	public function getFreeShipping()
	{
		return $this->free_shipping;
	}

	/**
	 * Get the [phone_order] column value.
	 * 
	 * @return     boolean
	 */
	public function getPhoneOrder()
	{
		return $this->phone_order;
	}

	/**
	 * Get the [udf_it_swc_beige] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcBeige()
	{
		return $this->udf_it_swc_beige;
	}

	/**
	 * Get the [udf_it_swc_black] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcBlack()
	{
		return $this->udf_it_swc_black;
	}

	/**
	 * Get the [udf_it_swc_blue] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcBlue()
	{
		return $this->udf_it_swc_blue;
	}

	/**
	 * Get the [udf_it_swc_brown] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcBrown()
	{
		return $this->udf_it_swc_brown;
	}

	/**
	 * Get the [udf_it_swc_camo] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcCamo()
	{
		return $this->udf_it_swc_camo;
	}

	/**
	 * Get the [udf_it_swc_gold] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcGold()
	{
		return $this->udf_it_swc_gold;
	}

	/**
	 * Get the [udf_it_swc_gray] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcGray()
	{
		return $this->udf_it_swc_gray;
	}

	/**
	 * Get the [udf_it_swc_green] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcGreen()
	{
		return $this->udf_it_swc_green;
	}

	/**
	 * Get the [udf_it_swc_orange] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcOrange()
	{
		return $this->udf_it_swc_orange;
	}

	/**
	 * Get the [udf_it_swc_pink] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcPink()
	{
		return $this->udf_it_swc_pink;
	}

	/**
	 * Get the [udf_it_swc_purple] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcPurple()
	{
		return $this->udf_it_swc_purple;
	}

	/**
	 * Get the [udf_it_swc_red] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcRed()
	{
		return $this->udf_it_swc_red;
	}

	/**
	 * Get the [udf_it_swc_silver] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcSilver()
	{
		return $this->udf_it_swc_silver;
	}

	/**
	 * Get the [udf_it_swc_white] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcWhite()
	{
		return $this->udf_it_swc_white;
	}

	/**
	 * Get the [udf_it_swc_yellow] column value.
	 * 
	 * @return     boolean
	 */
	public function getUdfItSwcYellow()
	{
		return $this->udf_it_swc_yellow;
	}

	/**
	 * Get the [optionally formatted] temporal [created_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->created_at === null) {
			return null;
		}


		if ($this->created_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->created_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [updated_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->updated_at === null) {
			return null;
		}


		if ($this->updated_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->updated_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = InventoryItemPeer::ID;
		}

		return $this;
	} // setId()

	/**
	 * Set the value of [sku] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSku($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->sku !== $v) {
			$this->sku = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SKU;
		}

		return $this;
	} // setSku()

	/**
	 * Set the value of [title] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setTitle($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = InventoryItemPeer::TITLE;
		}

		return $this;
	} // setTitle()

	/**
	 * Set the value of [brand_id] column.
	 * 
	 * @param      int $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setBrandId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->brand_id !== $v) {
			$this->brand_id = $v;
			$this->modifiedColumns[] = InventoryItemPeer::BRAND_ID;
		}

		if ($this->aBrand !== null && $this->aBrand->getId() !== $v) {
			$this->aBrand = null;
		}

		return $this;
	} // setBrandId()

	/**
	 * Set the value of [description_short] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setDescriptionShort($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->description_short !== $v) {
			$this->description_short = $v;
			$this->modifiedColumns[] = InventoryItemPeer::DESCRIPTION_SHORT;
		}

		return $this;
	} // setDescriptionShort()

	/**
	 * Set the value of [description_long] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setDescriptionLong($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->description_long !== $v) {
			$this->description_long = $v;
			$this->modifiedColumns[] = InventoryItemPeer::DESCRIPTION_LONG;
		}

		return $this;
	} // setDescriptionLong()

	/**
	 * Set the value of [size] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSize($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->size !== $v) {
			$this->size = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SIZE;
		}

		return $this;
	} // setSize()

	/**
	 * Set the value of [color] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setColor($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->color !== $v) {
			$this->color = $v;
			$this->modifiedColumns[] = InventoryItemPeer::COLOR;
		}

		return $this;
	} // setColor()

	/**
	 * Set the value of [gender] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setGender($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->gender !== $v) {
			$this->gender = $v;
			$this->modifiedColumns[] = InventoryItemPeer::GENDER;
		}

		return $this;
	} // setGender()

	/**
	 * Set the value of [sizing_id] column.
	 * 
	 * @param      int $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSizingId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->sizing_id !== $v) {
			$this->sizing_id = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SIZING_ID;
		}

		if ($this->aSizings !== null && $this->aSizings->getId() !== $v) {
			$this->aSizings = null;
		}

		return $this;
	} // setSizingId()

	/**
	 * Set the value of [customizable] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setCustomizable($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->customizable !== $v || $this->isNew()) {
			$this->customizable = $v;
			$this->modifiedColumns[] = InventoryItemPeer::CUSTOMIZABLE;
		}

		return $this;
	} // setCustomizable()

	/**
	 * Set the value of [video] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setVideo($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->video !== $v) {
			$this->video = $v;
			$this->modifiedColumns[] = InventoryItemPeer::VIDEO;
		}

		return $this;
	} // setVideo()

	/**
	 * Set the value of [measure_id] column.
	 * 
	 * @param      int $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setMeasureId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->measure_id !== $v) {
			$this->measure_id = $v;
			$this->modifiedColumns[] = InventoryItemPeer::MEASURE_ID;
		}

		if ($this->aMeasures !== null && $this->aMeasures->getId() !== $v) {
			$this->aMeasures = null;
		}

		return $this;
	} // setMeasureId()

	/**
	 * Set the value of [qty_on_hand] column.
	 * 
	 * @param      int $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setQtyOnHand($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->qty_on_hand !== $v) {
			$this->qty_on_hand = $v;
			$this->modifiedColumns[] = InventoryItemPeer::QTY_ON_HAND;
		}

		return $this;
	} // setQtyOnHand()

	/**
	 * Set the value of [std_unit_price] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setStdUnitPrice($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->std_unit_price !== $v) {
			$this->std_unit_price = $v;
			$this->modifiedColumns[] = InventoryItemPeer::STD_UNIT_PRICE;
		}

		return $this;
	} // setStdUnitPrice()

	/**
	 * Set the value of [msrp] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setMsrp($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->msrp !== $v) {
			$this->msrp = $v;
			$this->modifiedColumns[] = InventoryItemPeer::MSRP;
		}

		return $this;
	} // setMsrp()

	/**
	 * Set the value of [clearance] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setClearance($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->clearance !== $v) {
			$this->clearance = $v;
			$this->modifiedColumns[] = InventoryItemPeer::CLEARANCE;
		}

		return $this;
	} // setClearance()

	/**
	 * Sets the value of [sale_start_date] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSaleStartDate($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->sale_start_date !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->sale_start_date !== null && $tmpDt = new DateTime($this->sale_start_date)) ? $tmpDt->format('Y-m-d') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->sale_start_date = ($dt ? $dt->format('Y-m-d') : null);
				$this->modifiedColumns[] = InventoryItemPeer::SALE_START_DATE;
			}
		} // if either are not null

		return $this;
	} // setSaleStartDate()

	/**
	 * Sets the value of [sale_end_date] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSaleEndDate($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->sale_end_date !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->sale_end_date !== null && $tmpDt = new DateTime($this->sale_end_date)) ? $tmpDt->format('Y-m-d') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->sale_end_date = ($dt ? $dt->format('Y-m-d') : null);
				$this->modifiedColumns[] = InventoryItemPeer::SALE_END_DATE;
			}
		} // if either are not null

		return $this;
	} // setSaleEndDate()

	/**
	 * Set the value of [sale_method] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSaleMethod($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->sale_method !== $v) {
			$this->sale_method = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SALE_METHOD;
		}

		return $this;
	} // setSaleMethod()

	/**
	 * Set the value of [sale_price] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSalePrice($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->sale_price !== $v) {
			$this->sale_price = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SALE_PRICE;
		}

		return $this;
	} // setSalePrice()

	/**
	 * Set the value of [sale_percent] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSalePercent($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->sale_percent !== $v) {
			$this->sale_percent = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SALE_PERCENT;
		}

		return $this;
	} // setSalePercent()

	/**
	 * Set the value of [tax_class] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setTaxClass($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tax_class !== $v) {
			$this->tax_class = $v;
			$this->modifiedColumns[] = InventoryItemPeer::TAX_CLASS;
		}

		return $this;
	} // setTaxClass()

	/**
	 * Set the value of [ship_weight] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setShipWeight($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->ship_weight !== $v) {
			$this->ship_weight = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SHIP_WEIGHT;
		}

		return $this;
	} // setShipWeight()

	/**
	 * Set the value of [sale_promo_discount] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSalePromoDiscount($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->sale_promo_discount !== $v) {
			$this->sale_promo_discount = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SALE_PROMO_DISCOUNT;
		}

		return $this;
	} // setSalePromoDiscount()

	/**
	 * Set the value of [finish] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setFinish($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->finish !== $v) {
			$this->finish = $v;
			$this->modifiedColumns[] = InventoryItemPeer::FINISH;
		}

		return $this;
	} // setFinish()

	/**
	 * Set the value of [documents] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setDocuments($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->documents !== $v) {
			$this->documents = $v;
			$this->modifiedColumns[] = InventoryItemPeer::DOCUMENTS;
		}

		return $this;
	} // setDocuments()

	/**
	 * Set the value of [hand] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setHand($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->hand !== $v) {
			$this->hand = $v;
			$this->modifiedColumns[] = InventoryItemPeer::HAND;
		}

		return $this;
	} // setHand()

	/**
	 * Set the value of [matrix] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setMatrix($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->matrix !== $v || $this->isNew()) {
			$this->matrix = $v;
			$this->modifiedColumns[] = InventoryItemPeer::MATRIX;
		}

		return $this;
	} // setMatrix()

	/**
	 * Set the value of [rootitem] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setRootitem($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->rootitem !== $v) {
			$this->rootitem = $v;
			$this->modifiedColumns[] = InventoryItemPeer::ROOTITEM;
		}

		return $this;
	} // setRootitem()

	/**
	 * Set the value of [features] column.
	 * 
	 * @param      string $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setFeatures($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->features !== $v) {
			$this->features = $v;
			$this->modifiedColumns[] = InventoryItemPeer::FEATURES;
		}

		return $this;
	} // setFeatures()

	/**
	 * Set the value of [setup_charge] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setSetupCharge($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->setup_charge !== $v || $this->isNew()) {
			$this->setup_charge = $v;
			$this->modifiedColumns[] = InventoryItemPeer::SETUP_CHARGE;
		}

		return $this;
	} // setSetupCharge()

	/**
	 * Sets the value of [date_updated] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setDateUpdated($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->date_updated !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->date_updated !== null && $tmpDt = new DateTime($this->date_updated)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->date_updated = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = InventoryItemPeer::DATE_UPDATED;
			}
		} // if either are not null

		return $this;
	} // setDateUpdated()

	/**
	 * Set the value of [time_updated] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setTimeUpdated($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->time_updated !== $v || $this->isNew()) {
			$this->time_updated = $v;
			$this->modifiedColumns[] = InventoryItemPeer::TIME_UPDATED;
		}

		return $this;
	} // setTimeUpdated()

	/**
	 * Sets the value of [recently_viewed] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setRecentlyViewed($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->recently_viewed !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->recently_viewed !== null && $tmpDt = new DateTime($this->recently_viewed)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->recently_viewed = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = InventoryItemPeer::RECENTLY_VIEWED;
			}
		} // if either are not null

		return $this;
	} // setRecentlyViewed()

	/**
	 * Set the value of [additional_freight] column.
	 * 
	 * @param      double $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setAdditionalFreight($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->additional_freight !== $v) {
			$this->additional_freight = $v;
			$this->modifiedColumns[] = InventoryItemPeer::ADDITIONAL_FREIGHT;
		}

		return $this;
	} // setAdditionalFreight()

	/**
	 * Set the value of [free_shipping] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setFreeShipping($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->free_shipping !== $v || $this->isNew()) {
			$this->free_shipping = $v;
			$this->modifiedColumns[] = InventoryItemPeer::FREE_SHIPPING;
		}

		return $this;
	} // setFreeShipping()

	/**
	 * Set the value of [phone_order] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setPhoneOrder($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->phone_order !== $v || $this->isNew()) {
			$this->phone_order = $v;
			$this->modifiedColumns[] = InventoryItemPeer::PHONE_ORDER;
		}

		return $this;
	} // setPhoneOrder()

	/**
	 * Set the value of [udf_it_swc_beige] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcBeige($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_beige !== $v || $this->isNew()) {
			$this->udf_it_swc_beige = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_BEIGE;
		}

		return $this;
	} // setUdfItSwcBeige()

	/**
	 * Set the value of [udf_it_swc_black] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcBlack($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_black !== $v || $this->isNew()) {
			$this->udf_it_swc_black = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_BLACK;
		}

		return $this;
	} // setUdfItSwcBlack()

	/**
	 * Set the value of [udf_it_swc_blue] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcBlue($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_blue !== $v || $this->isNew()) {
			$this->udf_it_swc_blue = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_BLUE;
		}

		return $this;
	} // setUdfItSwcBlue()

	/**
	 * Set the value of [udf_it_swc_brown] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcBrown($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_brown !== $v || $this->isNew()) {
			$this->udf_it_swc_brown = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_BROWN;
		}

		return $this;
	} // setUdfItSwcBrown()

	/**
	 * Set the value of [udf_it_swc_camo] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcCamo($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_camo !== $v || $this->isNew()) {
			$this->udf_it_swc_camo = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_CAMO;
		}

		return $this;
	} // setUdfItSwcCamo()

	/**
	 * Set the value of [udf_it_swc_gold] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcGold($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_gold !== $v || $this->isNew()) {
			$this->udf_it_swc_gold = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_GOLD;
		}

		return $this;
	} // setUdfItSwcGold()

	/**
	 * Set the value of [udf_it_swc_gray] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcGray($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_gray !== $v || $this->isNew()) {
			$this->udf_it_swc_gray = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_GRAY;
		}

		return $this;
	} // setUdfItSwcGray()

	/**
	 * Set the value of [udf_it_swc_green] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcGreen($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_green !== $v || $this->isNew()) {
			$this->udf_it_swc_green = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_GREEN;
		}

		return $this;
	} // setUdfItSwcGreen()

	/**
	 * Set the value of [udf_it_swc_orange] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcOrange($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_orange !== $v || $this->isNew()) {
			$this->udf_it_swc_orange = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_ORANGE;
		}

		return $this;
	} // setUdfItSwcOrange()

	/**
	 * Set the value of [udf_it_swc_pink] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcPink($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_pink !== $v || $this->isNew()) {
			$this->udf_it_swc_pink = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_PINK;
		}

		return $this;
	} // setUdfItSwcPink()

	/**
	 * Set the value of [udf_it_swc_purple] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcPurple($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_purple !== $v || $this->isNew()) {
			$this->udf_it_swc_purple = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_PURPLE;
		}

		return $this;
	} // setUdfItSwcPurple()

	/**
	 * Set the value of [udf_it_swc_red] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcRed($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_red !== $v || $this->isNew()) {
			$this->udf_it_swc_red = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_RED;
		}

		return $this;
	} // setUdfItSwcRed()

	/**
	 * Set the value of [udf_it_swc_silver] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcSilver($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_silver !== $v || $this->isNew()) {
			$this->udf_it_swc_silver = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_SILVER;
		}

		return $this;
	} // setUdfItSwcSilver()

	/**
	 * Set the value of [udf_it_swc_white] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcWhite($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_white !== $v || $this->isNew()) {
			$this->udf_it_swc_white = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_WHITE;
		}

		return $this;
	} // setUdfItSwcWhite()

	/**
	 * Set the value of [udf_it_swc_yellow] column.
	 * 
	 * @param      boolean $v new value
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUdfItSwcYellow($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->udf_it_swc_yellow !== $v || $this->isNew()) {
			$this->udf_it_swc_yellow = $v;
			$this->modifiedColumns[] = InventoryItemPeer::UDF_IT_SWC_YELLOW;
		}

		return $this;
	} // setUdfItSwcYellow()

	/**
	 * Sets the value of [created_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setCreatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->created_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->created_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = InventoryItemPeer::CREATED_AT;
			}
		} // if either are not null

		return $this;
	} // setCreatedAt()

	/**
	 * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     InventoryItem The current object (for fluent API support)
	 */
	public function setUpdatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->updated_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->updated_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = InventoryItemPeer::UPDATED_AT;
			}
		} // if either are not null

		return $this;
	} // setUpdatedAt()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->customizable !== false) {
				return false;
			}

			if ($this->matrix !== false) {
				return false;
			}

			if ($this->setup_charge !== false) {
				return false;
			}

			if ($this->time_updated !== 0) {
				return false;
			}

			if ($this->free_shipping !== false) {
				return false;
			}

			if ($this->phone_order !== false) {
				return false;
			}

			if ($this->udf_it_swc_beige !== false) {
				return false;
			}

			if ($this->udf_it_swc_black !== false) {
				return false;
			}

			if ($this->udf_it_swc_blue !== false) {
				return false;
			}

			if ($this->udf_it_swc_brown !== false) {
				return false;
			}

			if ($this->udf_it_swc_camo !== false) {
				return false;
			}

			if ($this->udf_it_swc_gold !== false) {
				return false;
			}

			if ($this->udf_it_swc_gray !== false) {
				return false;
			}

			if ($this->udf_it_swc_green !== false) {
				return false;
			}

			if ($this->udf_it_swc_orange !== false) {
				return false;
			}

			if ($this->udf_it_swc_pink !== false) {
				return false;
			}

			if ($this->udf_it_swc_purple !== false) {
				return false;
			}

			if ($this->udf_it_swc_red !== false) {
				return false;
			}

			if ($this->udf_it_swc_silver !== false) {
				return false;
			}

			if ($this->udf_it_swc_white !== false) {
				return false;
			}

			if ($this->udf_it_swc_yellow !== false) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->sku = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->title = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->brand_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
			$this->description_short = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->description_long = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->size = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->color = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->gender = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->sizing_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
			$this->customizable = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
			$this->video = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
			$this->measure_id = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
			$this->qty_on_hand = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
			$this->std_unit_price = ($row[$startcol + 14] !== null) ? (double) $row[$startcol + 14] : null;
			$this->msrp = ($row[$startcol + 15] !== null) ? (double) $row[$startcol + 15] : null;
			$this->clearance = ($row[$startcol + 16] !== null) ? (double) $row[$startcol + 16] : null;
			$this->sale_start_date = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
			$this->sale_end_date = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
			$this->sale_method = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
			$this->sale_price = ($row[$startcol + 20] !== null) ? (double) $row[$startcol + 20] : null;
			$this->sale_percent = ($row[$startcol + 21] !== null) ? (double) $row[$startcol + 21] : null;
			$this->tax_class = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
			$this->ship_weight = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
			$this->sale_promo_discount = ($row[$startcol + 24] !== null) ? (double) $row[$startcol + 24] : null;
			$this->finish = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
			$this->documents = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
			$this->hand = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
			$this->matrix = ($row[$startcol + 28] !== null) ? (boolean) $row[$startcol + 28] : null;
			$this->rootitem = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
			$this->features = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
			$this->setup_charge = ($row[$startcol + 31] !== null) ? (boolean) $row[$startcol + 31] : null;
			$this->date_updated = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
			$this->time_updated = ($row[$startcol + 33] !== null) ? (double) $row[$startcol + 33] : null;
			$this->recently_viewed = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
			$this->additional_freight = ($row[$startcol + 35] !== null) ? (double) $row[$startcol + 35] : null;
			$this->free_shipping = ($row[$startcol + 36] !== null) ? (boolean) $row[$startcol + 36] : null;
			$this->phone_order = ($row[$startcol + 37] !== null) ? (boolean) $row[$startcol + 37] : null;
			$this->udf_it_swc_beige = ($row[$startcol + 38] !== null) ? (boolean) $row[$startcol + 38] : null;
			$this->udf_it_swc_black = ($row[$startcol + 39] !== null) ? (boolean) $row[$startcol + 39] : null;
			$this->udf_it_swc_blue = ($row[$startcol + 40] !== null) ? (boolean) $row[$startcol + 40] : null;
			$this->udf_it_swc_brown = ($row[$startcol + 41] !== null) ? (boolean) $row[$startcol + 41] : null;
			$this->udf_it_swc_camo = ($row[$startcol + 42] !== null) ? (boolean) $row[$startcol + 42] : null;
			$this->udf_it_swc_gold = ($row[$startcol + 43] !== null) ? (boolean) $row[$startcol + 43] : null;
			$this->udf_it_swc_gray = ($row[$startcol + 44] !== null) ? (boolean) $row[$startcol + 44] : null;
			$this->udf_it_swc_green = ($row[$startcol + 45] !== null) ? (boolean) $row[$startcol + 45] : null;
			$this->udf_it_swc_orange = ($row[$startcol + 46] !== null) ? (boolean) $row[$startcol + 46] : null;
			$this->udf_it_swc_pink = ($row[$startcol + 47] !== null) ? (boolean) $row[$startcol + 47] : null;
			$this->udf_it_swc_purple = ($row[$startcol + 48] !== null) ? (boolean) $row[$startcol + 48] : null;
			$this->udf_it_swc_red = ($row[$startcol + 49] !== null) ? (boolean) $row[$startcol + 49] : null;
			$this->udf_it_swc_silver = ($row[$startcol + 50] !== null) ? (boolean) $row[$startcol + 50] : null;
			$this->udf_it_swc_white = ($row[$startcol + 51] !== null) ? (boolean) $row[$startcol + 51] : null;
			$this->udf_it_swc_yellow = ($row[$startcol + 52] !== null) ? (boolean) $row[$startcol + 52] : null;
			$this->created_at = ($row[$startcol + 53] !== null) ? (string) $row[$startcol + 53] : null;
			$this->updated_at = ($row[$startcol + 54] !== null) ? (string) $row[$startcol + 54] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 55; // 55 = InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating InventoryItem object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aBrand !== null && $this->brand_id !== $this->aBrand->getId()) {
			$this->aBrand = null;
		}
		if ($this->aSizings !== null && $this->sizing_id !== $this->aSizings->getId()) {
			$this->aSizings = null;
		}
		if ($this->aMeasures !== null && $this->measure_id !== $this->aMeasures->getId()) {
			$this->aMeasures = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = InventoryItemPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aBrand = null;
			$this->aSizings = null;
			$this->aMeasures = null;
			$this->collOrderedItemOptionsItemss = null;
			$this->lastOrderedItemOptionsItemsCriteria = null;

			$this->collInventoryItemPaperworks = null;
			$this->lastInventoryItemPaperworkCriteria = null;

			$this->collInventoryItemInventoryCategorys = null;
			$this->lastInventoryItemInventoryCategoryCriteria = null;

			$this->collOptionsItemss = null;
			$this->lastOptionsItemsCriteria = null;

			$this->collOptionsValues = null;
			$this->lastOptionsValueCriteria = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				InventoryItemPeer::doDelete($this, $con);
				$this->postDelete($con);
				$this->setDeleted(true);
				$con->commit();
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			// symfony_timestampable behavior
			if ($this->isModified() && !$this->isColumnModified(InventoryItemPeer::UPDATED_AT))
			{
			  $this->setUpdatedAt(time());
			}

			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
				// symfony_timestampable behavior
				if (!$this->isColumnModified(InventoryItemPeer::CREATED_AT))
				{
				  $this->setCreatedAt(time());
				}

			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				InventoryItemPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aBrand !== null) {
				if ($this->aBrand->isModified() || $this->aBrand->isNew()) {
					$affectedRows += $this->aBrand->save($con);
				}
				$this->setBrand($this->aBrand);
			}

			if ($this->aSizings !== null) {
				if ($this->aSizings->isModified() || $this->aSizings->isNew()) {
					$affectedRows += $this->aSizings->save($con);
				}
				$this->setSizings($this->aSizings);
			}

			if ($this->aMeasures !== null) {
				if ($this->aMeasures->isModified() || $this->aMeasures->isNew()) {
					$affectedRows += $this->aMeasures->save($con);
				}
				$this->setMeasures($this->aMeasures);
			}


			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = InventoryItemPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setNew(false);
				} else {
					$affectedRows += InventoryItemPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collOrderedItemOptionsItemss !== null) {
				foreach ($this->collOrderedItemOptionsItemss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collInventoryItemPaperworks !== null) {
				foreach ($this->collInventoryItemPaperworks as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collInventoryItemInventoryCategorys !== null) {
				foreach ($this->collInventoryItemInventoryCategorys as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collOptionsItemss !== null) {
				foreach ($this->collOptionsItemss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			if ($this->collOptionsValues !== null) {
				foreach ($this->collOptionsValues as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aBrand !== null) {
				if (!$this->aBrand->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aBrand->getValidationFailures());
				}
			}

			if ($this->aSizings !== null) {
				if (!$this->aSizings->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSizings->getValidationFailures());
				}
			}

			if ($this->aMeasures !== null) {
				if (!$this->aMeasures->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMeasures->getValidationFailures());
				}
			}


			if (($retval = InventoryItemPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collOrderedItemOptionsItemss !== null) {
					foreach ($this->collOrderedItemOptionsItemss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collInventoryItemPaperworks !== null) {
					foreach ($this->collInventoryItemPaperworks as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collInventoryItemInventoryCategorys !== null) {
					foreach ($this->collInventoryItemInventoryCategorys as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collOptionsItemss !== null) {
					foreach ($this->collOptionsItemss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}

				if ($this->collOptionsValues !== null) {
					foreach ($this->collOptionsValues as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = InventoryItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getSku();
				break;
			case 2:
				return $this->getTitle();
				break;
			case 3:
				return $this->getBrandId();
				break;
			case 4:
				return $this->getDescriptionShort();
				break;
			case 5:
				return $this->getDescriptionLong();
				break;
			case 6:
				return $this->getSize();
				break;
			case 7:
				return $this->getColor();
				break;
			case 8:
				return $this->getGender();
				break;
			case 9:
				return $this->getSizingId();
				break;
			case 10:
				return $this->getCustomizable();
				break;
			case 11:
				return $this->getVideo();
				break;
			case 12:
				return $this->getMeasureId();
				break;
			case 13:
				return $this->getQtyOnHand();
				break;
			case 14:
				return $this->getStdUnitPrice();
				break;
			case 15:
				return $this->getMsrp();
				break;
			case 16:
				return $this->getClearance();
				break;
			case 17:
				return $this->getSaleStartDate();
				break;
			case 18:
				return $this->getSaleEndDate();
				break;
			case 19:
				return $this->getSaleMethod();
				break;
			case 20:
				return $this->getSalePrice();
				break;
			case 21:
				return $this->getSalePercent();
				break;
			case 22:
				return $this->getTaxClass();
				break;
			case 23:
				return $this->getShipWeight();
				break;
			case 24:
				return $this->getSalePromoDiscount();
				break;
			case 25:
				return $this->getFinish();
				break;
			case 26:
				return $this->getDocuments();
				break;
			case 27:
				return $this->getHand();
				break;
			case 28:
				return $this->getMatrix();
				break;
			case 29:
				return $this->getRootitem();
				break;
			case 30:
				return $this->getFeatures();
				break;
			case 31:
				return $this->getSetupCharge();
				break;
			case 32:
				return $this->getDateUpdated();
				break;
			case 33:
				return $this->getTimeUpdated();
				break;
			case 34:
				return $this->getRecentlyViewed();
				break;
			case 35:
				return $this->getAdditionalFreight();
				break;
			case 36:
				return $this->getFreeShipping();
				break;
			case 37:
				return $this->getPhoneOrder();
				break;
			case 38:
				return $this->getUdfItSwcBeige();
				break;
			case 39:
				return $this->getUdfItSwcBlack();
				break;
			case 40:
				return $this->getUdfItSwcBlue();
				break;
			case 41:
				return $this->getUdfItSwcBrown();
				break;
			case 42:
				return $this->getUdfItSwcCamo();
				break;
			case 43:
				return $this->getUdfItSwcGold();
				break;
			case 44:
				return $this->getUdfItSwcGray();
				break;
			case 45:
				return $this->getUdfItSwcGreen();
				break;
			case 46:
				return $this->getUdfItSwcOrange();
				break;
			case 47:
				return $this->getUdfItSwcPink();
				break;
			case 48:
				return $this->getUdfItSwcPurple();
				break;
			case 49:
				return $this->getUdfItSwcRed();
				break;
			case 50:
				return $this->getUdfItSwcSilver();
				break;
			case 51:
				return $this->getUdfItSwcWhite();
				break;
			case 52:
				return $this->getUdfItSwcYellow();
				break;
			case 53:
				return $this->getCreatedAt();
				break;
			case 54:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                        BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. Defaults to BasePeer::TYPE_PHPNAME.
	 * @param      boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns.  Defaults to TRUE.
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true)
	{
		$keys = InventoryItemPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getSku(),
			$keys[2] => $this->getTitle(),
			$keys[3] => $this->getBrandId(),
			$keys[4] => $this->getDescriptionShort(),
			$keys[5] => $this->getDescriptionLong(),
			$keys[6] => $this->getSize(),
			$keys[7] => $this->getColor(),
			$keys[8] => $this->getGender(),
			$keys[9] => $this->getSizingId(),
			$keys[10] => $this->getCustomizable(),
			$keys[11] => $this->getVideo(),
			$keys[12] => $this->getMeasureId(),
			$keys[13] => $this->getQtyOnHand(),
			$keys[14] => $this->getStdUnitPrice(),
			$keys[15] => $this->getMsrp(),
			$keys[16] => $this->getClearance(),
			$keys[17] => $this->getSaleStartDate(),
			$keys[18] => $this->getSaleEndDate(),
			$keys[19] => $this->getSaleMethod(),
			$keys[20] => $this->getSalePrice(),
			$keys[21] => $this->getSalePercent(),
			$keys[22] => $this->getTaxClass(),
			$keys[23] => $this->getShipWeight(),
			$keys[24] => $this->getSalePromoDiscount(),
			$keys[25] => $this->getFinish(),
			$keys[26] => $this->getDocuments(),
			$keys[27] => $this->getHand(),
			$keys[28] => $this->getMatrix(),
			$keys[29] => $this->getRootitem(),
			$keys[30] => $this->getFeatures(),
			$keys[31] => $this->getSetupCharge(),
			$keys[32] => $this->getDateUpdated(),
			$keys[33] => $this->getTimeUpdated(),
			$keys[34] => $this->getRecentlyViewed(),
			$keys[35] => $this->getAdditionalFreight(),
			$keys[36] => $this->getFreeShipping(),
			$keys[37] => $this->getPhoneOrder(),
			$keys[38] => $this->getUdfItSwcBeige(),
			$keys[39] => $this->getUdfItSwcBlack(),
			$keys[40] => $this->getUdfItSwcBlue(),
			$keys[41] => $this->getUdfItSwcBrown(),
			$keys[42] => $this->getUdfItSwcCamo(),
			$keys[43] => $this->getUdfItSwcGold(),
			$keys[44] => $this->getUdfItSwcGray(),
			$keys[45] => $this->getUdfItSwcGreen(),
			$keys[46] => $this->getUdfItSwcOrange(),
			$keys[47] => $this->getUdfItSwcPink(),
			$keys[48] => $this->getUdfItSwcPurple(),
			$keys[49] => $this->getUdfItSwcRed(),
			$keys[50] => $this->getUdfItSwcSilver(),
			$keys[51] => $this->getUdfItSwcWhite(),
			$keys[52] => $this->getUdfItSwcYellow(),
			$keys[53] => $this->getCreatedAt(),
			$keys[54] => $this->getUpdatedAt(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = InventoryItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setSku($value);
				break;
			case 2:
				$this->setTitle($value);
				break;
			case 3:
				$this->setBrandId($value);
				break;
			case 4:
				$this->setDescriptionShort($value);
				break;
			case 5:
				$this->setDescriptionLong($value);
				break;
			case 6:
				$this->setSize($value);
				break;
			case 7:
				$this->setColor($value);
				break;
			case 8:
				$this->setGender($value);
				break;
			case 9:
				$this->setSizingId($value);
				break;
			case 10:
				$this->setCustomizable($value);
				break;
			case 11:
				$this->setVideo($value);
				break;
			case 12:
				$this->setMeasureId($value);
				break;
			case 13:
				$this->setQtyOnHand($value);
				break;
			case 14:
				$this->setStdUnitPrice($value);
				break;
			case 15:
				$this->setMsrp($value);
				break;
			case 16:
				$this->setClearance($value);
				break;
			case 17:
				$this->setSaleStartDate($value);
				break;
			case 18:
				$this->setSaleEndDate($value);
				break;
			case 19:
				$this->setSaleMethod($value);
				break;
			case 20:
				$this->setSalePrice($value);
				break;
			case 21:
				$this->setSalePercent($value);
				break;
			case 22:
				$this->setTaxClass($value);
				break;
			case 23:
				$this->setShipWeight($value);
				break;
			case 24:
				$this->setSalePromoDiscount($value);
				break;
			case 25:
				$this->setFinish($value);
				break;
			case 26:
				$this->setDocuments($value);
				break;
			case 27:
				$this->setHand($value);
				break;
			case 28:
				$this->setMatrix($value);
				break;
			case 29:
				$this->setRootitem($value);
				break;
			case 30:
				$this->setFeatures($value);
				break;
			case 31:
				$this->setSetupCharge($value);
				break;
			case 32:
				$this->setDateUpdated($value);
				break;
			case 33:
				$this->setTimeUpdated($value);
				break;
			case 34:
				$this->setRecentlyViewed($value);
				break;
			case 35:
				$this->setAdditionalFreight($value);
				break;
			case 36:
				$this->setFreeShipping($value);
				break;
			case 37:
				$this->setPhoneOrder($value);
				break;
			case 38:
				$this->setUdfItSwcBeige($value);
				break;
			case 39:
				$this->setUdfItSwcBlack($value);
				break;
			case 40:
				$this->setUdfItSwcBlue($value);
				break;
			case 41:
				$this->setUdfItSwcBrown($value);
				break;
			case 42:
				$this->setUdfItSwcCamo($value);
				break;
			case 43:
				$this->setUdfItSwcGold($value);
				break;
			case 44:
				$this->setUdfItSwcGray($value);
				break;
			case 45:
				$this->setUdfItSwcGreen($value);
				break;
			case 46:
				$this->setUdfItSwcOrange($value);
				break;
			case 47:
				$this->setUdfItSwcPink($value);
				break;
			case 48:
				$this->setUdfItSwcPurple($value);
				break;
			case 49:
				$this->setUdfItSwcRed($value);
				break;
			case 50:
				$this->setUdfItSwcSilver($value);
				break;
			case 51:
				$this->setUdfItSwcWhite($value);
				break;
			case 52:
				$this->setUdfItSwcYellow($value);
				break;
			case 53:
				$this->setCreatedAt($value);
				break;
			case 54:
				$this->setUpdatedAt($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = InventoryItemPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setSku($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setTitle($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBrandId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setDescriptionShort($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDescriptionLong($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setSize($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setColor($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setGender($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setSizingId($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setCustomizable($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setVideo($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setMeasureId($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setQtyOnHand($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setStdUnitPrice($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setMsrp($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setClearance($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setSaleStartDate($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setSaleEndDate($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setSaleMethod($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setSalePrice($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setSalePercent($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setTaxClass($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setShipWeight($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setSalePromoDiscount($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setFinish($arr[$keys[25]]);
		if (array_key_exists($keys[26], $arr)) $this->setDocuments($arr[$keys[26]]);
		if (array_key_exists($keys[27], $arr)) $this->setHand($arr[$keys[27]]);
		if (array_key_exists($keys[28], $arr)) $this->setMatrix($arr[$keys[28]]);
		if (array_key_exists($keys[29], $arr)) $this->setRootitem($arr[$keys[29]]);
		if (array_key_exists($keys[30], $arr)) $this->setFeatures($arr[$keys[30]]);
		if (array_key_exists($keys[31], $arr)) $this->setSetupCharge($arr[$keys[31]]);
		if (array_key_exists($keys[32], $arr)) $this->setDateUpdated($arr[$keys[32]]);
		if (array_key_exists($keys[33], $arr)) $this->setTimeUpdated($arr[$keys[33]]);
		if (array_key_exists($keys[34], $arr)) $this->setRecentlyViewed($arr[$keys[34]]);
		if (array_key_exists($keys[35], $arr)) $this->setAdditionalFreight($arr[$keys[35]]);
		if (array_key_exists($keys[36], $arr)) $this->setFreeShipping($arr[$keys[36]]);
		if (array_key_exists($keys[37], $arr)) $this->setPhoneOrder($arr[$keys[37]]);
		if (array_key_exists($keys[38], $arr)) $this->setUdfItSwcBeige($arr[$keys[38]]);
		if (array_key_exists($keys[39], $arr)) $this->setUdfItSwcBlack($arr[$keys[39]]);
		if (array_key_exists($keys[40], $arr)) $this->setUdfItSwcBlue($arr[$keys[40]]);
		if (array_key_exists($keys[41], $arr)) $this->setUdfItSwcBrown($arr[$keys[41]]);
		if (array_key_exists($keys[42], $arr)) $this->setUdfItSwcCamo($arr[$keys[42]]);
		if (array_key_exists($keys[43], $arr)) $this->setUdfItSwcGold($arr[$keys[43]]);
		if (array_key_exists($keys[44], $arr)) $this->setUdfItSwcGray($arr[$keys[44]]);
		if (array_key_exists($keys[45], $arr)) $this->setUdfItSwcGreen($arr[$keys[45]]);
		if (array_key_exists($keys[46], $arr)) $this->setUdfItSwcOrange($arr[$keys[46]]);
		if (array_key_exists($keys[47], $arr)) $this->setUdfItSwcPink($arr[$keys[47]]);
		if (array_key_exists($keys[48], $arr)) $this->setUdfItSwcPurple($arr[$keys[48]]);
		if (array_key_exists($keys[49], $arr)) $this->setUdfItSwcRed($arr[$keys[49]]);
		if (array_key_exists($keys[50], $arr)) $this->setUdfItSwcSilver($arr[$keys[50]]);
		if (array_key_exists($keys[51], $arr)) $this->setUdfItSwcWhite($arr[$keys[51]]);
		if (array_key_exists($keys[52], $arr)) $this->setUdfItSwcYellow($arr[$keys[52]]);
		if (array_key_exists($keys[53], $arr)) $this->setCreatedAt($arr[$keys[53]]);
		if (array_key_exists($keys[54], $arr)) $this->setUpdatedAt($arr[$keys[54]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);

		if ($this->isColumnModified(InventoryItemPeer::ID)) $criteria->add(InventoryItemPeer::ID, $this->id);
		if ($this->isColumnModified(InventoryItemPeer::SKU)) $criteria->add(InventoryItemPeer::SKU, $this->sku);
		if ($this->isColumnModified(InventoryItemPeer::TITLE)) $criteria->add(InventoryItemPeer::TITLE, $this->title);
		if ($this->isColumnModified(InventoryItemPeer::BRAND_ID)) $criteria->add(InventoryItemPeer::BRAND_ID, $this->brand_id);
		if ($this->isColumnModified(InventoryItemPeer::DESCRIPTION_SHORT)) $criteria->add(InventoryItemPeer::DESCRIPTION_SHORT, $this->description_short);
		if ($this->isColumnModified(InventoryItemPeer::DESCRIPTION_LONG)) $criteria->add(InventoryItemPeer::DESCRIPTION_LONG, $this->description_long);
		if ($this->isColumnModified(InventoryItemPeer::SIZE)) $criteria->add(InventoryItemPeer::SIZE, $this->size);
		if ($this->isColumnModified(InventoryItemPeer::COLOR)) $criteria->add(InventoryItemPeer::COLOR, $this->color);
		if ($this->isColumnModified(InventoryItemPeer::GENDER)) $criteria->add(InventoryItemPeer::GENDER, $this->gender);
		if ($this->isColumnModified(InventoryItemPeer::SIZING_ID)) $criteria->add(InventoryItemPeer::SIZING_ID, $this->sizing_id);
		if ($this->isColumnModified(InventoryItemPeer::CUSTOMIZABLE)) $criteria->add(InventoryItemPeer::CUSTOMIZABLE, $this->customizable);
		if ($this->isColumnModified(InventoryItemPeer::VIDEO)) $criteria->add(InventoryItemPeer::VIDEO, $this->video);
		if ($this->isColumnModified(InventoryItemPeer::MEASURE_ID)) $criteria->add(InventoryItemPeer::MEASURE_ID, $this->measure_id);
		if ($this->isColumnModified(InventoryItemPeer::QTY_ON_HAND)) $criteria->add(InventoryItemPeer::QTY_ON_HAND, $this->qty_on_hand);
		if ($this->isColumnModified(InventoryItemPeer::STD_UNIT_PRICE)) $criteria->add(InventoryItemPeer::STD_UNIT_PRICE, $this->std_unit_price);
		if ($this->isColumnModified(InventoryItemPeer::MSRP)) $criteria->add(InventoryItemPeer::MSRP, $this->msrp);
		if ($this->isColumnModified(InventoryItemPeer::CLEARANCE)) $criteria->add(InventoryItemPeer::CLEARANCE, $this->clearance);
		if ($this->isColumnModified(InventoryItemPeer::SALE_START_DATE)) $criteria->add(InventoryItemPeer::SALE_START_DATE, $this->sale_start_date);
		if ($this->isColumnModified(InventoryItemPeer::SALE_END_DATE)) $criteria->add(InventoryItemPeer::SALE_END_DATE, $this->sale_end_date);
		if ($this->isColumnModified(InventoryItemPeer::SALE_METHOD)) $criteria->add(InventoryItemPeer::SALE_METHOD, $this->sale_method);
		if ($this->isColumnModified(InventoryItemPeer::SALE_PRICE)) $criteria->add(InventoryItemPeer::SALE_PRICE, $this->sale_price);
		if ($this->isColumnModified(InventoryItemPeer::SALE_PERCENT)) $criteria->add(InventoryItemPeer::SALE_PERCENT, $this->sale_percent);
		if ($this->isColumnModified(InventoryItemPeer::TAX_CLASS)) $criteria->add(InventoryItemPeer::TAX_CLASS, $this->tax_class);
		if ($this->isColumnModified(InventoryItemPeer::SHIP_WEIGHT)) $criteria->add(InventoryItemPeer::SHIP_WEIGHT, $this->ship_weight);
		if ($this->isColumnModified(InventoryItemPeer::SALE_PROMO_DISCOUNT)) $criteria->add(InventoryItemPeer::SALE_PROMO_DISCOUNT, $this->sale_promo_discount);
		if ($this->isColumnModified(InventoryItemPeer::FINISH)) $criteria->add(InventoryItemPeer::FINISH, $this->finish);
		if ($this->isColumnModified(InventoryItemPeer::DOCUMENTS)) $criteria->add(InventoryItemPeer::DOCUMENTS, $this->documents);
		if ($this->isColumnModified(InventoryItemPeer::HAND)) $criteria->add(InventoryItemPeer::HAND, $this->hand);
		if ($this->isColumnModified(InventoryItemPeer::MATRIX)) $criteria->add(InventoryItemPeer::MATRIX, $this->matrix);
		if ($this->isColumnModified(InventoryItemPeer::ROOTITEM)) $criteria->add(InventoryItemPeer::ROOTITEM, $this->rootitem);
		if ($this->isColumnModified(InventoryItemPeer::FEATURES)) $criteria->add(InventoryItemPeer::FEATURES, $this->features);
		if ($this->isColumnModified(InventoryItemPeer::SETUP_CHARGE)) $criteria->add(InventoryItemPeer::SETUP_CHARGE, $this->setup_charge);
		if ($this->isColumnModified(InventoryItemPeer::DATE_UPDATED)) $criteria->add(InventoryItemPeer::DATE_UPDATED, $this->date_updated);
		if ($this->isColumnModified(InventoryItemPeer::TIME_UPDATED)) $criteria->add(InventoryItemPeer::TIME_UPDATED, $this->time_updated);
		if ($this->isColumnModified(InventoryItemPeer::RECENTLY_VIEWED)) $criteria->add(InventoryItemPeer::RECENTLY_VIEWED, $this->recently_viewed);
		if ($this->isColumnModified(InventoryItemPeer::ADDITIONAL_FREIGHT)) $criteria->add(InventoryItemPeer::ADDITIONAL_FREIGHT, $this->additional_freight);
		if ($this->isColumnModified(InventoryItemPeer::FREE_SHIPPING)) $criteria->add(InventoryItemPeer::FREE_SHIPPING, $this->free_shipping);
		if ($this->isColumnModified(InventoryItemPeer::PHONE_ORDER)) $criteria->add(InventoryItemPeer::PHONE_ORDER, $this->phone_order);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_BEIGE)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_BEIGE, $this->udf_it_swc_beige);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_BLACK)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_BLACK, $this->udf_it_swc_black);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_BLUE)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_BLUE, $this->udf_it_swc_blue);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_BROWN)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_BROWN, $this->udf_it_swc_brown);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_CAMO)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_CAMO, $this->udf_it_swc_camo);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_GOLD)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_GOLD, $this->udf_it_swc_gold);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_GRAY)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_GRAY, $this->udf_it_swc_gray);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_GREEN)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_GREEN, $this->udf_it_swc_green);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_ORANGE)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_ORANGE, $this->udf_it_swc_orange);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_PINK)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_PINK, $this->udf_it_swc_pink);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_PURPLE)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_PURPLE, $this->udf_it_swc_purple);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_RED)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_RED, $this->udf_it_swc_red);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_SILVER)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_SILVER, $this->udf_it_swc_silver);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_WHITE)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_WHITE, $this->udf_it_swc_white);
		if ($this->isColumnModified(InventoryItemPeer::UDF_IT_SWC_YELLOW)) $criteria->add(InventoryItemPeer::UDF_IT_SWC_YELLOW, $this->udf_it_swc_yellow);
		if ($this->isColumnModified(InventoryItemPeer::CREATED_AT)) $criteria->add(InventoryItemPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(InventoryItemPeer::UPDATED_AT)) $criteria->add(InventoryItemPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);

		$criteria->add(InventoryItemPeer::SKU, $this->sku);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     string
	 */
	public function getPrimaryKey()
	{
		return $this->getSku();
	}

	/**
	 * Generic method to set the primary key (sku column).
	 *
	 * @param      string $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setSku($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of InventoryItem (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setId($this->id);

		$copyObj->setSku($this->sku);

		$copyObj->setTitle($this->title);

		$copyObj->setBrandId($this->brand_id);

		$copyObj->setDescriptionShort($this->description_short);

		$copyObj->setDescriptionLong($this->description_long);

		$copyObj->setSize($this->size);

		$copyObj->setColor($this->color);

		$copyObj->setGender($this->gender);

		$copyObj->setSizingId($this->sizing_id);

		$copyObj->setCustomizable($this->customizable);

		$copyObj->setVideo($this->video);

		$copyObj->setMeasureId($this->measure_id);

		$copyObj->setQtyOnHand($this->qty_on_hand);

		$copyObj->setStdUnitPrice($this->std_unit_price);

		$copyObj->setMsrp($this->msrp);

		$copyObj->setClearance($this->clearance);

		$copyObj->setSaleStartDate($this->sale_start_date);

		$copyObj->setSaleEndDate($this->sale_end_date);

		$copyObj->setSaleMethod($this->sale_method);

		$copyObj->setSalePrice($this->sale_price);

		$copyObj->setSalePercent($this->sale_percent);

		$copyObj->setTaxClass($this->tax_class);

		$copyObj->setShipWeight($this->ship_weight);

		$copyObj->setSalePromoDiscount($this->sale_promo_discount);

		$copyObj->setFinish($this->finish);

		$copyObj->setDocuments($this->documents);

		$copyObj->setHand($this->hand);

		$copyObj->setMatrix($this->matrix);

		$copyObj->setRootitem($this->rootitem);

		$copyObj->setFeatures($this->features);

		$copyObj->setSetupCharge($this->setup_charge);

		$copyObj->setDateUpdated($this->date_updated);

		$copyObj->setTimeUpdated($this->time_updated);

		$copyObj->setRecentlyViewed($this->recently_viewed);

		$copyObj->setAdditionalFreight($this->additional_freight);

		$copyObj->setFreeShipping($this->free_shipping);

		$copyObj->setPhoneOrder($this->phone_order);

		$copyObj->setUdfItSwcBeige($this->udf_it_swc_beige);

		$copyObj->setUdfItSwcBlack($this->udf_it_swc_black);

		$copyObj->setUdfItSwcBlue($this->udf_it_swc_blue);

		$copyObj->setUdfItSwcBrown($this->udf_it_swc_brown);

		$copyObj->setUdfItSwcCamo($this->udf_it_swc_camo);

		$copyObj->setUdfItSwcGold($this->udf_it_swc_gold);

		$copyObj->setUdfItSwcGray($this->udf_it_swc_gray);

		$copyObj->setUdfItSwcGreen($this->udf_it_swc_green);

		$copyObj->setUdfItSwcOrange($this->udf_it_swc_orange);

		$copyObj->setUdfItSwcPink($this->udf_it_swc_pink);

		$copyObj->setUdfItSwcPurple($this->udf_it_swc_purple);

		$copyObj->setUdfItSwcRed($this->udf_it_swc_red);

		$copyObj->setUdfItSwcSilver($this->udf_it_swc_silver);

		$copyObj->setUdfItSwcWhite($this->udf_it_swc_white);

		$copyObj->setUdfItSwcYellow($this->udf_it_swc_yellow);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getOrderedItemOptionsItemss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addOrderedItemOptionsItems($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getInventoryItemPaperworks() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addInventoryItemPaperwork($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getInventoryItemInventoryCategorys() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addInventoryItemInventoryCategory($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getOptionsItemss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addOptionsItems($relObj->copy($deepCopy));
				}
			}

			foreach ($this->getOptionsValues() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addOptionsValue($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     InventoryItem Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     InventoryItemPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new InventoryItemPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Brand object.
	 *
	 * @param      Brand $v
	 * @return     InventoryItem The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setBrand(Brand $v = null)
	{
		if ($v === null) {
			$this->setBrandId(NULL);
		} else {
			$this->setBrandId($v->getId());
		}

		$this->aBrand = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Brand object, it will not be re-added.
		if ($v !== null) {
			$v->addInventoryItem($this);
		}

		return $this;
	}


	/**
	 * Get the associated Brand object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Brand The associated Brand object.
	 * @throws     PropelException
	 */
	public function getBrand(PropelPDO $con = null)
	{
		if ($this->aBrand === null && ($this->brand_id !== null)) {
			$this->aBrand = BrandPeer::retrieveByPk($this->brand_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aBrand->addInventoryItems($this);
			 */
		}
		return $this->aBrand;
	}

	/**
	 * Declares an association between this object and a Sizings object.
	 *
	 * @param      Sizings $v
	 * @return     InventoryItem The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setSizings(Sizings $v = null)
	{
		if ($v === null) {
			$this->setSizingId(NULL);
		} else {
			$this->setSizingId($v->getId());
		}

		$this->aSizings = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Sizings object, it will not be re-added.
		if ($v !== null) {
			$v->addInventoryItem($this);
		}

		return $this;
	}


	/**
	 * Get the associated Sizings object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Sizings The associated Sizings object.
	 * @throws     PropelException
	 */
	public function getSizings(PropelPDO $con = null)
	{
		if ($this->aSizings === null && ($this->sizing_id !== null)) {
			$this->aSizings = SizingsPeer::retrieveByPk($this->sizing_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aSizings->addInventoryItems($this);
			 */
		}
		return $this->aSizings;
	}

	/**
	 * Declares an association between this object and a Measures object.
	 *
	 * @param      Measures $v
	 * @return     InventoryItem The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setMeasures(Measures $v = null)
	{
		if ($v === null) {
			$this->setMeasureId(NULL);
		} else {
			$this->setMeasureId($v->getId());
		}

		$this->aMeasures = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Measures object, it will not be re-added.
		if ($v !== null) {
			$v->addInventoryItem($this);
		}

		return $this;
	}


	/**
	 * Get the associated Measures object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Measures The associated Measures object.
	 * @throws     PropelException
	 */
	public function getMeasures(PropelPDO $con = null)
	{
		if ($this->aMeasures === null && ($this->measure_id !== null)) {
			$this->aMeasures = MeasuresPeer::retrieveByPk($this->measure_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aMeasures->addInventoryItems($this);
			 */
		}
		return $this->aMeasures;
	}

	/**
	 * Clears out the collOrderedItemOptionsItemss collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addOrderedItemOptionsItemss()
	 */
	public function clearOrderedItemOptionsItemss()
	{
		$this->collOrderedItemOptionsItemss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collOrderedItemOptionsItemss collection (array).
	 *
	 * By default this just sets the collOrderedItemOptionsItemss collection to an empty array (like clearcollOrderedItemOptionsItemss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initOrderedItemOptionsItemss()
	{
		$this->collOrderedItemOptionsItemss = array();
	}

	/**
	 * Gets an array of OrderedItemOptionsItems objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this InventoryItem has previously been saved, it will retrieve
	 * related OrderedItemOptionsItemss from storage. If this InventoryItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array OrderedItemOptionsItems[]
	 * @throws     PropelException
	 */
	public function getOrderedItemOptionsItemss($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
			   $this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				OrderedItemOptionsItemsPeer::addSelectColumns($criteria);
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				OrderedItemOptionsItemsPeer::addSelectColumns($criteria);
				if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
					$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;
		return $this->collOrderedItemOptionsItemss;
	}

	/**
	 * Returns the number of related OrderedItemOptionsItems objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related OrderedItemOptionsItems objects.
	 * @throws     PropelException
	 */
	public function countOrderedItemOptionsItemss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				$count = OrderedItemOptionsItemsPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
					$count = OrderedItemOptionsItemsPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collOrderedItemOptionsItemss);
				}
			} else {
				$count = count($this->collOrderedItemOptionsItemss);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a OrderedItemOptionsItems object to this object
	 * through the OrderedItemOptionsItems foreign key attribute.
	 *
	 * @param      OrderedItemOptionsItems $l OrderedItemOptionsItems
	 * @return     void
	 * @throws     PropelException
	 */
	public function addOrderedItemOptionsItems(OrderedItemOptionsItems $l)
	{
		if ($this->collOrderedItemOptionsItemss === null) {
			$this->initOrderedItemOptionsItemss();
		}
		if (!in_array($l, $this->collOrderedItemOptionsItemss, true)) { // only add it if the **same** object is not already associated
			array_push($this->collOrderedItemOptionsItemss, $l);
			$l->setInventoryItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related OrderedItemOptionsItemss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getOrderedItemOptionsItemssJoinOptions($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;

		return $this->collOrderedItemOptionsItemss;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related OrderedItemOptionsItemss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getOrderedItemOptionsItemssJoinOrderedItem($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOrderedItem($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OrderedItemOptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOrderedItem($criteria, $con, $join_behavior);
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;

		return $this->collOrderedItemOptionsItemss;
	}

	/**
	 * Clears out the collInventoryItemPaperworks collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addInventoryItemPaperworks()
	 */
	public function clearInventoryItemPaperworks()
	{
		$this->collInventoryItemPaperworks = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collInventoryItemPaperworks collection (array).
	 *
	 * By default this just sets the collInventoryItemPaperworks collection to an empty array (like clearcollInventoryItemPaperworks());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initInventoryItemPaperworks()
	{
		$this->collInventoryItemPaperworks = array();
	}

	/**
	 * Gets an array of InventoryItemPaperwork objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this InventoryItem has previously been saved, it will retrieve
	 * related InventoryItemPaperworks from storage. If this InventoryItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array InventoryItemPaperwork[]
	 * @throws     PropelException
	 */
	public function getInventoryItemPaperworks($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInventoryItemPaperworks === null) {
			if ($this->isNew()) {
			   $this->collInventoryItemPaperworks = array();
			} else {

				$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

				InventoryItemPaperworkPeer::addSelectColumns($criteria);
				$this->collInventoryItemPaperworks = InventoryItemPaperworkPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

				InventoryItemPaperworkPeer::addSelectColumns($criteria);
				if (!isset($this->lastInventoryItemPaperworkCriteria) || !$this->lastInventoryItemPaperworkCriteria->equals($criteria)) {
					$this->collInventoryItemPaperworks = InventoryItemPaperworkPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastInventoryItemPaperworkCriteria = $criteria;
		return $this->collInventoryItemPaperworks;
	}

	/**
	 * Returns the number of related InventoryItemPaperwork objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related InventoryItemPaperwork objects.
	 * @throws     PropelException
	 */
	public function countInventoryItemPaperworks(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collInventoryItemPaperworks === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

				$count = InventoryItemPaperworkPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

				if (!isset($this->lastInventoryItemPaperworkCriteria) || !$this->lastInventoryItemPaperworkCriteria->equals($criteria)) {
					$count = InventoryItemPaperworkPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collInventoryItemPaperworks);
				}
			} else {
				$count = count($this->collInventoryItemPaperworks);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a InventoryItemPaperwork object to this object
	 * through the InventoryItemPaperwork foreign key attribute.
	 *
	 * @param      InventoryItemPaperwork $l InventoryItemPaperwork
	 * @return     void
	 * @throws     PropelException
	 */
	public function addInventoryItemPaperwork(InventoryItemPaperwork $l)
	{
		if ($this->collInventoryItemPaperworks === null) {
			$this->initInventoryItemPaperworks();
		}
		if (!in_array($l, $this->collInventoryItemPaperworks, true)) { // only add it if the **same** object is not already associated
			array_push($this->collInventoryItemPaperworks, $l);
			$l->setInventoryItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related InventoryItemPaperworks from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getInventoryItemPaperworksJoinPaperwork($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInventoryItemPaperworks === null) {
			if ($this->isNew()) {
				$this->collInventoryItemPaperworks = array();
			} else {

				$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

				$this->collInventoryItemPaperworks = InventoryItemPaperworkPeer::doSelectJoinPaperwork($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(InventoryItemPaperworkPeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastInventoryItemPaperworkCriteria) || !$this->lastInventoryItemPaperworkCriteria->equals($criteria)) {
				$this->collInventoryItemPaperworks = InventoryItemPaperworkPeer::doSelectJoinPaperwork($criteria, $con, $join_behavior);
			}
		}
		$this->lastInventoryItemPaperworkCriteria = $criteria;

		return $this->collInventoryItemPaperworks;
	}

	/**
	 * Clears out the collInventoryItemInventoryCategorys collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addInventoryItemInventoryCategorys()
	 */
	public function clearInventoryItemInventoryCategorys()
	{
		$this->collInventoryItemInventoryCategorys = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collInventoryItemInventoryCategorys collection (array).
	 *
	 * By default this just sets the collInventoryItemInventoryCategorys collection to an empty array (like clearcollInventoryItemInventoryCategorys());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initInventoryItemInventoryCategorys()
	{
		$this->collInventoryItemInventoryCategorys = array();
	}

	/**
	 * Gets an array of InventoryItemInventoryCategory objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this InventoryItem has previously been saved, it will retrieve
	 * related InventoryItemInventoryCategorys from storage. If this InventoryItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array InventoryItemInventoryCategory[]
	 * @throws     PropelException
	 */
	public function getInventoryItemInventoryCategorys($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInventoryItemInventoryCategorys === null) {
			if ($this->isNew()) {
			   $this->collInventoryItemInventoryCategorys = array();
			} else {

				$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

				InventoryItemInventoryCategoryPeer::addSelectColumns($criteria);
				$this->collInventoryItemInventoryCategorys = InventoryItemInventoryCategoryPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

				InventoryItemInventoryCategoryPeer::addSelectColumns($criteria);
				if (!isset($this->lastInventoryItemInventoryCategoryCriteria) || !$this->lastInventoryItemInventoryCategoryCriteria->equals($criteria)) {
					$this->collInventoryItemInventoryCategorys = InventoryItemInventoryCategoryPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastInventoryItemInventoryCategoryCriteria = $criteria;
		return $this->collInventoryItemInventoryCategorys;
	}

	/**
	 * Returns the number of related InventoryItemInventoryCategory objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related InventoryItemInventoryCategory objects.
	 * @throws     PropelException
	 */
	public function countInventoryItemInventoryCategorys(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collInventoryItemInventoryCategorys === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

				$count = InventoryItemInventoryCategoryPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

				if (!isset($this->lastInventoryItemInventoryCategoryCriteria) || !$this->lastInventoryItemInventoryCategoryCriteria->equals($criteria)) {
					$count = InventoryItemInventoryCategoryPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collInventoryItemInventoryCategorys);
				}
			} else {
				$count = count($this->collInventoryItemInventoryCategorys);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a InventoryItemInventoryCategory object to this object
	 * through the InventoryItemInventoryCategory foreign key attribute.
	 *
	 * @param      InventoryItemInventoryCategory $l InventoryItemInventoryCategory
	 * @return     void
	 * @throws     PropelException
	 */
	public function addInventoryItemInventoryCategory(InventoryItemInventoryCategory $l)
	{
		if ($this->collInventoryItemInventoryCategorys === null) {
			$this->initInventoryItemInventoryCategorys();
		}
		if (!in_array($l, $this->collInventoryItemInventoryCategorys, true)) { // only add it if the **same** object is not already associated
			array_push($this->collInventoryItemInventoryCategorys, $l);
			$l->setInventoryItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related InventoryItemInventoryCategorys from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getInventoryItemInventoryCategorysJoinInventoryCategory($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInventoryItemInventoryCategorys === null) {
			if ($this->isNew()) {
				$this->collInventoryItemInventoryCategorys = array();
			} else {

				$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

				$this->collInventoryItemInventoryCategorys = InventoryItemInventoryCategoryPeer::doSelectJoinInventoryCategory($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastInventoryItemInventoryCategoryCriteria) || !$this->lastInventoryItemInventoryCategoryCriteria->equals($criteria)) {
				$this->collInventoryItemInventoryCategorys = InventoryItemInventoryCategoryPeer::doSelectJoinInventoryCategory($criteria, $con, $join_behavior);
			}
		}
		$this->lastInventoryItemInventoryCategoryCriteria = $criteria;

		return $this->collInventoryItemInventoryCategorys;
	}

	/**
	 * Clears out the collOptionsItemss collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addOptionsItemss()
	 */
	public function clearOptionsItemss()
	{
		$this->collOptionsItemss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collOptionsItemss collection (array).
	 *
	 * By default this just sets the collOptionsItemss collection to an empty array (like clearcollOptionsItemss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initOptionsItemss()
	{
		$this->collOptionsItemss = array();
	}

	/**
	 * Gets an array of OptionsItems objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this InventoryItem has previously been saved, it will retrieve
	 * related OptionsItemss from storage. If this InventoryItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array OptionsItems[]
	 * @throws     PropelException
	 */
	public function getOptionsItemss($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOptionsItemss === null) {
			if ($this->isNew()) {
			   $this->collOptionsItemss = array();
			} else {

				$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				OptionsItemsPeer::addSelectColumns($criteria);
				$this->collOptionsItemss = OptionsItemsPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				OptionsItemsPeer::addSelectColumns($criteria);
				if (!isset($this->lastOptionsItemsCriteria) || !$this->lastOptionsItemsCriteria->equals($criteria)) {
					$this->collOptionsItemss = OptionsItemsPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOptionsItemsCriteria = $criteria;
		return $this->collOptionsItemss;
	}

	/**
	 * Returns the number of related OptionsItems objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related OptionsItems objects.
	 * @throws     PropelException
	 */
	public function countOptionsItemss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collOptionsItemss === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				$count = OptionsItemsPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				if (!isset($this->lastOptionsItemsCriteria) || !$this->lastOptionsItemsCriteria->equals($criteria)) {
					$count = OptionsItemsPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collOptionsItemss);
				}
			} else {
				$count = count($this->collOptionsItemss);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a OptionsItems object to this object
	 * through the OptionsItems foreign key attribute.
	 *
	 * @param      OptionsItems $l OptionsItems
	 * @return     void
	 * @throws     PropelException
	 */
	public function addOptionsItems(OptionsItems $l)
	{
		if ($this->collOptionsItemss === null) {
			$this->initOptionsItemss();
		}
		if (!in_array($l, $this->collOptionsItemss, true)) { // only add it if the **same** object is not already associated
			array_push($this->collOptionsItemss, $l);
			$l->setInventoryItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related OptionsItemss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getOptionsItemssJoinOptions($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOptionsItemss === null) {
			if ($this->isNew()) {
				$this->collOptionsItemss = array();
			} else {

				$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

				$this->collOptionsItemss = OptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OptionsItemsPeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastOptionsItemsCriteria) || !$this->lastOptionsItemsCriteria->equals($criteria)) {
				$this->collOptionsItemss = OptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		}
		$this->lastOptionsItemsCriteria = $criteria;

		return $this->collOptionsItemss;
	}

	/**
	 * Clears out the collOptionsValues collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addOptionsValues()
	 */
	public function clearOptionsValues()
	{
		$this->collOptionsValues = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collOptionsValues collection (array).
	 *
	 * By default this just sets the collOptionsValues collection to an empty array (like clearcollOptionsValues());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initOptionsValues()
	{
		$this->collOptionsValues = array();
	}

	/**
	 * Gets an array of OptionsValue objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this InventoryItem has previously been saved, it will retrieve
	 * related OptionsValues from storage. If this InventoryItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array OptionsValue[]
	 * @throws     PropelException
	 */
	public function getOptionsValues($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOptionsValues === null) {
			if ($this->isNew()) {
			   $this->collOptionsValues = array();
			} else {

				$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

				OptionsValuePeer::addSelectColumns($criteria);
				$this->collOptionsValues = OptionsValuePeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

				OptionsValuePeer::addSelectColumns($criteria);
				if (!isset($this->lastOptionsValueCriteria) || !$this->lastOptionsValueCriteria->equals($criteria)) {
					$this->collOptionsValues = OptionsValuePeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOptionsValueCriteria = $criteria;
		return $this->collOptionsValues;
	}

	/**
	 * Returns the number of related OptionsValue objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related OptionsValue objects.
	 * @throws     PropelException
	 */
	public function countOptionsValues(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collOptionsValues === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

				$count = OptionsValuePeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

				if (!isset($this->lastOptionsValueCriteria) || !$this->lastOptionsValueCriteria->equals($criteria)) {
					$count = OptionsValuePeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collOptionsValues);
				}
			} else {
				$count = count($this->collOptionsValues);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a OptionsValue object to this object
	 * through the OptionsValue foreign key attribute.
	 *
	 * @param      OptionsValue $l OptionsValue
	 * @return     void
	 * @throws     PropelException
	 */
	public function addOptionsValue(OptionsValue $l)
	{
		if ($this->collOptionsValues === null) {
			$this->initOptionsValues();
		}
		if (!in_array($l, $this->collOptionsValues, true)) { // only add it if the **same** object is not already associated
			array_push($this->collOptionsValues, $l);
			$l->setInventoryItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this InventoryItem is new, it will return
	 * an empty collection; or if this InventoryItem has previously
	 * been saved, it will retrieve related OptionsValues from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in InventoryItem.
	 */
	public function getOptionsValuesJoinOptions($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOptionsValues === null) {
			if ($this->isNew()) {
				$this->collOptionsValues = array();
			} else {

				$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

				$this->collOptionsValues = OptionsValuePeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OptionsValuePeer::INVENTORY_ITEM_ID, $this->id);

			if (!isset($this->lastOptionsValueCriteria) || !$this->lastOptionsValueCriteria->equals($criteria)) {
				$this->collOptionsValues = OptionsValuePeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		}
		$this->lastOptionsValueCriteria = $criteria;

		return $this->collOptionsValues;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collOrderedItemOptionsItemss) {
				foreach ((array) $this->collOrderedItemOptionsItemss as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collInventoryItemPaperworks) {
				foreach ((array) $this->collInventoryItemPaperworks as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collInventoryItemInventoryCategorys) {
				foreach ((array) $this->collInventoryItemInventoryCategorys as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collOptionsItemss) {
				foreach ((array) $this->collOptionsItemss as $o) {
					$o->clearAllReferences($deep);
				}
			}
			if ($this->collOptionsValues) {
				foreach ((array) $this->collOptionsValues as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collOrderedItemOptionsItemss = null;
		$this->collInventoryItemPaperworks = null;
		$this->collInventoryItemInventoryCategorys = null;
		$this->collOptionsItemss = null;
		$this->collOptionsValues = null;
			$this->aBrand = null;
			$this->aSizings = null;
			$this->aMeasures = null;
	}

} // BaseInventoryItem
