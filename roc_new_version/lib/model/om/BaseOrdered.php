<?php

/**
 * Base class that represents a row from the 'ordered' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseOrdered extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        OrderedPeer
	 */
	protected static $peer;

	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;

	/**
	 * The value for the user_id field.
	 * @var        int
	 */
	protected $user_id;

	/**
	 * The value for the status field.
	 * Note: this column has a database default value of: 'O'
	 * @var        string
	 */
	protected $status;

	/**
	 * The value for the b_country field.
	 * @var        string
	 */
	protected $b_country;

	/**
	 * The value for the b_street field.
	 * @var        string
	 */
	protected $b_street;

	/**
	 * The value for the b_street_2 field.
	 * @var        string
	 */
	protected $b_street_2;

	/**
	 * The value for the b_city field.
	 * @var        string
	 */
	protected $b_city;

	/**
	 * The value for the b_state field.
	 * @var        string
	 */
	protected $b_state;

	/**
	 * The value for the b_zip field.
	 * @var        string
	 */
	protected $b_zip;

	/**
	 * The value for the b_phone field.
	 * @var        string
	 */
	protected $b_phone;

	/**
	 * The value for the s_country field.
	 * @var        string
	 */
	protected $s_country;

	/**
	 * The value for the s_street field.
	 * @var        string
	 */
	protected $s_street;

	/**
	 * The value for the s_street_2 field.
	 * @var        string
	 */
	protected $s_street_2;

	/**
	 * The value for the s_city field.
	 * @var        string
	 */
	protected $s_city;

	/**
	 * The value for the s_state field.
	 * @var        string
	 */
	protected $s_state;

	/**
	 * The value for the s_zip field.
	 * @var        string
	 */
	protected $s_zip;

	/**
	 * The value for the total_price field.
	 * @var        double
	 */
	protected $total_price;

	/**
	 * The value for the tax field.
	 * @var        double
	 */
	protected $tax;

	/**
	 * The value for the shipping_cost field.
	 * @var        double
	 */
	protected $shipping_cost;

	/**
	 * The value for the discount field.
	 * @var        double
	 */
	protected $discount;

	/**
	 * The value for the transaction_id field.
	 * @var        string
	 */
	protected $transaction_id;

	/**
	 * The value for the notes field.
	 * @var        string
	 */
	protected $notes;

	/**
	 * The value for the payment_method field.
	 * @var        string
	 */
	protected $payment_method;

	/**
	 * The value for the shipping field.
	 * @var        string
	 */
	protected $shipping;

	/**
	 * The value for the created_at field.
	 * @var        string
	 */
	protected $created_at;

	/**
	 * The value for the updated_at field.
	 * @var        string
	 */
	protected $updated_at;

	/**
	 * @var        sfGuardUser
	 */
	protected $asfGuardUser;

	/**
	 * @var        array OrderedItem[] Collection to store aggregation of OrderedItem objects.
	 */
	protected $collOrderedItems;

	/**
	 * @var        Criteria The criteria used to select the current contents of collOrderedItems.
	 */
	private $lastOrderedItemCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	// symfony behavior
	
	const PEER = 'OrderedPeer';

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->status = 'O';
	}

	/**
	 * Initializes internal state of BaseOrdered object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get the [user_id] column value.
	 * 
	 * @return     int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * Get the [status] column value.
	 * 
	 * @return     string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * Get the [b_country] column value.
	 * 
	 * @return     string
	 */
	public function getBCountry()
	{
		return $this->b_country;
	}

	/**
	 * Get the [b_street] column value.
	 * 
	 * @return     string
	 */
	public function getBStreet()
	{
		return $this->b_street;
	}

	/**
	 * Get the [b_street_2] column value.
	 * 
	 * @return     string
	 */
	public function getBStreet2()
	{
		return $this->b_street_2;
	}

	/**
	 * Get the [b_city] column value.
	 * 
	 * @return     string
	 */
	public function getBCity()
	{
		return $this->b_city;
	}

	/**
	 * Get the [b_state] column value.
	 * 
	 * @return     string
	 */
	public function getBState()
	{
		return $this->b_state;
	}

	/**
	 * Get the [b_zip] column value.
	 * 
	 * @return     string
	 */
	public function getBZip()
	{
		return $this->b_zip;
	}

	/**
	 * Get the [b_phone] column value.
	 * 
	 * @return     string
	 */
	public function getBPhone()
	{
		return $this->b_phone;
	}

	/**
	 * Get the [s_country] column value.
	 * 
	 * @return     string
	 */
	public function getSCountry()
	{
		return $this->s_country;
	}

	/**
	 * Get the [s_street] column value.
	 * 
	 * @return     string
	 */
	public function getSStreet()
	{
		return $this->s_street;
	}

	/**
	 * Get the [s_street_2] column value.
	 * 
	 * @return     string
	 */
	public function getSStreet2()
	{
		return $this->s_street_2;
	}

	/**
	 * Get the [s_city] column value.
	 * 
	 * @return     string
	 */
	public function getSCity()
	{
		return $this->s_city;
	}

	/**
	 * Get the [s_state] column value.
	 * 
	 * @return     string
	 */
	public function getSState()
	{
		return $this->s_state;
	}

	/**
	 * Get the [s_zip] column value.
	 * 
	 * @return     string
	 */
	public function getSZip()
	{
		return $this->s_zip;
	}

	/**
	 * Get the [total_price] column value.
	 * 
	 * @return     double
	 */
	public function getTotalPrice()
	{
		return $this->total_price;
	}

	/**
	 * Get the [tax] column value.
	 * 
	 * @return     double
	 */
	public function getTax()
	{
		return $this->tax;
	}

	/**
	 * Get the [shipping_cost] column value.
	 * 
	 * @return     double
	 */
	public function getShippingCost()
	{
		return $this->shipping_cost;
	}

	/**
	 * Get the [discount] column value.
	 * 
	 * @return     double
	 */
	public function getDiscount()
	{
		return $this->discount;
	}

	/**
	 * Get the [transaction_id] column value.
	 * 
	 * @return     string
	 */
	public function getTransactionId()
	{
		return $this->transaction_id;
	}

	/**
	 * Get the [notes] column value.
	 * 
	 * @return     string
	 */
	public function getNotes()
	{
		return $this->notes;
	}

	/**
	 * Get the [payment_method] column value.
	 * 
	 * @return     string
	 */
	public function getPaymentMethod()
	{
		return $this->payment_method;
	}

	/**
	 * Get the [shipping] column value.
	 * 
	 * @return     string
	 */
	public function getShipping()
	{
		return $this->shipping;
	}

	/**
	 * Get the [optionally formatted] temporal [created_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->created_at === null) {
			return null;
		}


		if ($this->created_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->created_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [updated_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->updated_at === null) {
			return null;
		}


		if ($this->updated_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->updated_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = OrderedPeer::ID;
		}

		return $this;
	} // setId()

	/**
	 * Set the value of [user_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setUserId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->user_id !== $v) {
			$this->user_id = $v;
			$this->modifiedColumns[] = OrderedPeer::USER_ID;
		}

		if ($this->asfGuardUser !== null && $this->asfGuardUser->getId() !== $v) {
			$this->asfGuardUser = null;
		}

		return $this;
	} // setUserId()

	/**
	 * Set the value of [status] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setStatus($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->status !== $v || $this->isNew()) {
			$this->status = $v;
			$this->modifiedColumns[] = OrderedPeer::STATUS;
		}

		return $this;
	} // setStatus()

	/**
	 * Set the value of [b_country] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBCountry($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_country !== $v) {
			$this->b_country = $v;
			$this->modifiedColumns[] = OrderedPeer::B_COUNTRY;
		}

		return $this;
	} // setBCountry()

	/**
	 * Set the value of [b_street] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBStreet($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_street !== $v) {
			$this->b_street = $v;
			$this->modifiedColumns[] = OrderedPeer::B_STREET;
		}

		return $this;
	} // setBStreet()

	/**
	 * Set the value of [b_street_2] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBStreet2($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_street_2 !== $v) {
			$this->b_street_2 = $v;
			$this->modifiedColumns[] = OrderedPeer::B_STREET_2;
		}

		return $this;
	} // setBStreet2()

	/**
	 * Set the value of [b_city] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBCity($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_city !== $v) {
			$this->b_city = $v;
			$this->modifiedColumns[] = OrderedPeer::B_CITY;
		}

		return $this;
	} // setBCity()

	/**
	 * Set the value of [b_state] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBState($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_state !== $v) {
			$this->b_state = $v;
			$this->modifiedColumns[] = OrderedPeer::B_STATE;
		}

		return $this;
	} // setBState()

	/**
	 * Set the value of [b_zip] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBZip($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_zip !== $v) {
			$this->b_zip = $v;
			$this->modifiedColumns[] = OrderedPeer::B_ZIP;
		}

		return $this;
	} // setBZip()

	/**
	 * Set the value of [b_phone] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setBPhone($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->b_phone !== $v) {
			$this->b_phone = $v;
			$this->modifiedColumns[] = OrderedPeer::B_PHONE;
		}

		return $this;
	} // setBPhone()

	/**
	 * Set the value of [s_country] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSCountry($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_country !== $v) {
			$this->s_country = $v;
			$this->modifiedColumns[] = OrderedPeer::S_COUNTRY;
		}

		return $this;
	} // setSCountry()

	/**
	 * Set the value of [s_street] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSStreet($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_street !== $v) {
			$this->s_street = $v;
			$this->modifiedColumns[] = OrderedPeer::S_STREET;
		}

		return $this;
	} // setSStreet()

	/**
	 * Set the value of [s_street_2] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSStreet2($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_street_2 !== $v) {
			$this->s_street_2 = $v;
			$this->modifiedColumns[] = OrderedPeer::S_STREET_2;
		}

		return $this;
	} // setSStreet2()

	/**
	 * Set the value of [s_city] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSCity($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_city !== $v) {
			$this->s_city = $v;
			$this->modifiedColumns[] = OrderedPeer::S_CITY;
		}

		return $this;
	} // setSCity()

	/**
	 * Set the value of [s_state] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSState($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_state !== $v) {
			$this->s_state = $v;
			$this->modifiedColumns[] = OrderedPeer::S_STATE;
		}

		return $this;
	} // setSState()

	/**
	 * Set the value of [s_zip] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setSZip($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->s_zip !== $v) {
			$this->s_zip = $v;
			$this->modifiedColumns[] = OrderedPeer::S_ZIP;
		}

		return $this;
	} // setSZip()

	/**
	 * Set the value of [total_price] column.
	 * 
	 * @param      double $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setTotalPrice($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->total_price !== $v) {
			$this->total_price = $v;
			$this->modifiedColumns[] = OrderedPeer::TOTAL_PRICE;
		}

		return $this;
	} // setTotalPrice()

	/**
	 * Set the value of [tax] column.
	 * 
	 * @param      double $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setTax($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->tax !== $v) {
			$this->tax = $v;
			$this->modifiedColumns[] = OrderedPeer::TAX;
		}

		return $this;
	} // setTax()

	/**
	 * Set the value of [shipping_cost] column.
	 * 
	 * @param      double $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setShippingCost($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->shipping_cost !== $v) {
			$this->shipping_cost = $v;
			$this->modifiedColumns[] = OrderedPeer::SHIPPING_COST;
		}

		return $this;
	} // setShippingCost()

	/**
	 * Set the value of [discount] column.
	 * 
	 * @param      double $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setDiscount($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->discount !== $v) {
			$this->discount = $v;
			$this->modifiedColumns[] = OrderedPeer::DISCOUNT;
		}

		return $this;
	} // setDiscount()

	/**
	 * Set the value of [transaction_id] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setTransactionId($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->transaction_id !== $v) {
			$this->transaction_id = $v;
			$this->modifiedColumns[] = OrderedPeer::TRANSACTION_ID;
		}

		return $this;
	} // setTransactionId()

	/**
	 * Set the value of [notes] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setNotes($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->notes !== $v) {
			$this->notes = $v;
			$this->modifiedColumns[] = OrderedPeer::NOTES;
		}

		return $this;
	} // setNotes()

	/**
	 * Set the value of [payment_method] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setPaymentMethod($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->payment_method !== $v) {
			$this->payment_method = $v;
			$this->modifiedColumns[] = OrderedPeer::PAYMENT_METHOD;
		}

		return $this;
	} // setPaymentMethod()

	/**
	 * Set the value of [shipping] column.
	 * 
	 * @param      string $v new value
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setShipping($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->shipping !== $v) {
			$this->shipping = $v;
			$this->modifiedColumns[] = OrderedPeer::SHIPPING;
		}

		return $this;
	} // setShipping()

	/**
	 * Sets the value of [created_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setCreatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->created_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->created_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = OrderedPeer::CREATED_AT;
			}
		} // if either are not null

		return $this;
	} // setCreatedAt()

	/**
	 * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     Ordered The current object (for fluent API support)
	 */
	public function setUpdatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->updated_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->updated_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = OrderedPeer::UPDATED_AT;
			}
		} // if either are not null

		return $this;
	} // setUpdatedAt()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->status !== 'O') {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->user_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->status = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->b_country = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->b_street = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->b_street_2 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->b_city = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->b_state = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->b_zip = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->b_phone = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->s_country = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
			$this->s_street = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
			$this->s_street_2 = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
			$this->s_city = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
			$this->s_state = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
			$this->s_zip = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
			$this->total_price = ($row[$startcol + 16] !== null) ? (double) $row[$startcol + 16] : null;
			$this->tax = ($row[$startcol + 17] !== null) ? (double) $row[$startcol + 17] : null;
			$this->shipping_cost = ($row[$startcol + 18] !== null) ? (double) $row[$startcol + 18] : null;
			$this->discount = ($row[$startcol + 19] !== null) ? (double) $row[$startcol + 19] : null;
			$this->transaction_id = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
			$this->notes = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
			$this->payment_method = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
			$this->shipping = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
			$this->created_at = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
			$this->updated_at = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 26; // 26 = OrderedPeer::NUM_COLUMNS - OrderedPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Ordered object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->asfGuardUser !== null && $this->user_id !== $this->asfGuardUser->getId()) {
			$this->asfGuardUser = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = OrderedPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->asfGuardUser = null;
			$this->collOrderedItems = null;
			$this->lastOrderedItemCriteria = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				OrderedPeer::doDelete($this, $con);
				$this->postDelete($con);
				$this->setDeleted(true);
				$con->commit();
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			// symfony_timestampable behavior
			if ($this->isModified() && !$this->isColumnModified(OrderedPeer::UPDATED_AT))
			{
			  $this->setUpdatedAt(time());
			}

			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
				// symfony_timestampable behavior
				if (!$this->isColumnModified(OrderedPeer::CREATED_AT))
				{
				  $this->setCreatedAt(time());
				}

			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				OrderedPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if ($this->asfGuardUser->isModified() || $this->asfGuardUser->isNew()) {
					$affectedRows += $this->asfGuardUser->save($con);
				}
				$this->setsfGuardUser($this->asfGuardUser);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = OrderedPeer::ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = OrderedPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += OrderedPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collOrderedItems !== null) {
				foreach ($this->collOrderedItems as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->asfGuardUser !== null) {
				if (!$this->asfGuardUser->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->asfGuardUser->getValidationFailures());
				}
			}


			if (($retval = OrderedPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collOrderedItems !== null) {
					foreach ($this->collOrderedItems as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrderedPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getUserId();
				break;
			case 2:
				return $this->getStatus();
				break;
			case 3:
				return $this->getBCountry();
				break;
			case 4:
				return $this->getBStreet();
				break;
			case 5:
				return $this->getBStreet2();
				break;
			case 6:
				return $this->getBCity();
				break;
			case 7:
				return $this->getBState();
				break;
			case 8:
				return $this->getBZip();
				break;
			case 9:
				return $this->getBPhone();
				break;
			case 10:
				return $this->getSCountry();
				break;
			case 11:
				return $this->getSStreet();
				break;
			case 12:
				return $this->getSStreet2();
				break;
			case 13:
				return $this->getSCity();
				break;
			case 14:
				return $this->getSState();
				break;
			case 15:
				return $this->getSZip();
				break;
			case 16:
				return $this->getTotalPrice();
				break;
			case 17:
				return $this->getTax();
				break;
			case 18:
				return $this->getShippingCost();
				break;
			case 19:
				return $this->getDiscount();
				break;
			case 20:
				return $this->getTransactionId();
				break;
			case 21:
				return $this->getNotes();
				break;
			case 22:
				return $this->getPaymentMethod();
				break;
			case 23:
				return $this->getShipping();
				break;
			case 24:
				return $this->getCreatedAt();
				break;
			case 25:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                        BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. Defaults to BasePeer::TYPE_PHPNAME.
	 * @param      boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns.  Defaults to TRUE.
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true)
	{
		$keys = OrderedPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getUserId(),
			$keys[2] => $this->getStatus(),
			$keys[3] => $this->getBCountry(),
			$keys[4] => $this->getBStreet(),
			$keys[5] => $this->getBStreet2(),
			$keys[6] => $this->getBCity(),
			$keys[7] => $this->getBState(),
			$keys[8] => $this->getBZip(),
			$keys[9] => $this->getBPhone(),
			$keys[10] => $this->getSCountry(),
			$keys[11] => $this->getSStreet(),
			$keys[12] => $this->getSStreet2(),
			$keys[13] => $this->getSCity(),
			$keys[14] => $this->getSState(),
			$keys[15] => $this->getSZip(),
			$keys[16] => $this->getTotalPrice(),
			$keys[17] => $this->getTax(),
			$keys[18] => $this->getShippingCost(),
			$keys[19] => $this->getDiscount(),
			$keys[20] => $this->getTransactionId(),
			$keys[21] => $this->getNotes(),
			$keys[22] => $this->getPaymentMethod(),
			$keys[23] => $this->getShipping(),
			$keys[24] => $this->getCreatedAt(),
			$keys[25] => $this->getUpdatedAt(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrderedPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setUserId($value);
				break;
			case 2:
				$this->setStatus($value);
				break;
			case 3:
				$this->setBCountry($value);
				break;
			case 4:
				$this->setBStreet($value);
				break;
			case 5:
				$this->setBStreet2($value);
				break;
			case 6:
				$this->setBCity($value);
				break;
			case 7:
				$this->setBState($value);
				break;
			case 8:
				$this->setBZip($value);
				break;
			case 9:
				$this->setBPhone($value);
				break;
			case 10:
				$this->setSCountry($value);
				break;
			case 11:
				$this->setSStreet($value);
				break;
			case 12:
				$this->setSStreet2($value);
				break;
			case 13:
				$this->setSCity($value);
				break;
			case 14:
				$this->setSState($value);
				break;
			case 15:
				$this->setSZip($value);
				break;
			case 16:
				$this->setTotalPrice($value);
				break;
			case 17:
				$this->setTax($value);
				break;
			case 18:
				$this->setShippingCost($value);
				break;
			case 19:
				$this->setDiscount($value);
				break;
			case 20:
				$this->setTransactionId($value);
				break;
			case 21:
				$this->setNotes($value);
				break;
			case 22:
				$this->setPaymentMethod($value);
				break;
			case 23:
				$this->setShipping($value);
				break;
			case 24:
				$this->setCreatedAt($value);
				break;
			case 25:
				$this->setUpdatedAt($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OrderedPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUserId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setStatus($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setBCountry($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setBStreet($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setBStreet2($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setBCity($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setBState($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setBZip($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setBPhone($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setSCountry($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setSStreet($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setSStreet2($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setSCity($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setSState($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setSZip($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setTotalPrice($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setTax($arr[$keys[17]]);
		if (array_key_exists($keys[18], $arr)) $this->setShippingCost($arr[$keys[18]]);
		if (array_key_exists($keys[19], $arr)) $this->setDiscount($arr[$keys[19]]);
		if (array_key_exists($keys[20], $arr)) $this->setTransactionId($arr[$keys[20]]);
		if (array_key_exists($keys[21], $arr)) $this->setNotes($arr[$keys[21]]);
		if (array_key_exists($keys[22], $arr)) $this->setPaymentMethod($arr[$keys[22]]);
		if (array_key_exists($keys[23], $arr)) $this->setShipping($arr[$keys[23]]);
		if (array_key_exists($keys[24], $arr)) $this->setCreatedAt($arr[$keys[24]]);
		if (array_key_exists($keys[25], $arr)) $this->setUpdatedAt($arr[$keys[25]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(OrderedPeer::DATABASE_NAME);

		if ($this->isColumnModified(OrderedPeer::ID)) $criteria->add(OrderedPeer::ID, $this->id);
		if ($this->isColumnModified(OrderedPeer::USER_ID)) $criteria->add(OrderedPeer::USER_ID, $this->user_id);
		if ($this->isColumnModified(OrderedPeer::STATUS)) $criteria->add(OrderedPeer::STATUS, $this->status);
		if ($this->isColumnModified(OrderedPeer::B_COUNTRY)) $criteria->add(OrderedPeer::B_COUNTRY, $this->b_country);
		if ($this->isColumnModified(OrderedPeer::B_STREET)) $criteria->add(OrderedPeer::B_STREET, $this->b_street);
		if ($this->isColumnModified(OrderedPeer::B_STREET_2)) $criteria->add(OrderedPeer::B_STREET_2, $this->b_street_2);
		if ($this->isColumnModified(OrderedPeer::B_CITY)) $criteria->add(OrderedPeer::B_CITY, $this->b_city);
		if ($this->isColumnModified(OrderedPeer::B_STATE)) $criteria->add(OrderedPeer::B_STATE, $this->b_state);
		if ($this->isColumnModified(OrderedPeer::B_ZIP)) $criteria->add(OrderedPeer::B_ZIP, $this->b_zip);
		if ($this->isColumnModified(OrderedPeer::B_PHONE)) $criteria->add(OrderedPeer::B_PHONE, $this->b_phone);
		if ($this->isColumnModified(OrderedPeer::S_COUNTRY)) $criteria->add(OrderedPeer::S_COUNTRY, $this->s_country);
		if ($this->isColumnModified(OrderedPeer::S_STREET)) $criteria->add(OrderedPeer::S_STREET, $this->s_street);
		if ($this->isColumnModified(OrderedPeer::S_STREET_2)) $criteria->add(OrderedPeer::S_STREET_2, $this->s_street_2);
		if ($this->isColumnModified(OrderedPeer::S_CITY)) $criteria->add(OrderedPeer::S_CITY, $this->s_city);
		if ($this->isColumnModified(OrderedPeer::S_STATE)) $criteria->add(OrderedPeer::S_STATE, $this->s_state);
		if ($this->isColumnModified(OrderedPeer::S_ZIP)) $criteria->add(OrderedPeer::S_ZIP, $this->s_zip);
		if ($this->isColumnModified(OrderedPeer::TOTAL_PRICE)) $criteria->add(OrderedPeer::TOTAL_PRICE, $this->total_price);
		if ($this->isColumnModified(OrderedPeer::TAX)) $criteria->add(OrderedPeer::TAX, $this->tax);
		if ($this->isColumnModified(OrderedPeer::SHIPPING_COST)) $criteria->add(OrderedPeer::SHIPPING_COST, $this->shipping_cost);
		if ($this->isColumnModified(OrderedPeer::DISCOUNT)) $criteria->add(OrderedPeer::DISCOUNT, $this->discount);
		if ($this->isColumnModified(OrderedPeer::TRANSACTION_ID)) $criteria->add(OrderedPeer::TRANSACTION_ID, $this->transaction_id);
		if ($this->isColumnModified(OrderedPeer::NOTES)) $criteria->add(OrderedPeer::NOTES, $this->notes);
		if ($this->isColumnModified(OrderedPeer::PAYMENT_METHOD)) $criteria->add(OrderedPeer::PAYMENT_METHOD, $this->payment_method);
		if ($this->isColumnModified(OrderedPeer::SHIPPING)) $criteria->add(OrderedPeer::SHIPPING, $this->shipping);
		if ($this->isColumnModified(OrderedPeer::CREATED_AT)) $criteria->add(OrderedPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(OrderedPeer::UPDATED_AT)) $criteria->add(OrderedPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(OrderedPeer::DATABASE_NAME);

		$criteria->add(OrderedPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Ordered (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setUserId($this->user_id);

		$copyObj->setStatus($this->status);

		$copyObj->setBCountry($this->b_country);

		$copyObj->setBStreet($this->b_street);

		$copyObj->setBStreet2($this->b_street_2);

		$copyObj->setBCity($this->b_city);

		$copyObj->setBState($this->b_state);

		$copyObj->setBZip($this->b_zip);

		$copyObj->setBPhone($this->b_phone);

		$copyObj->setSCountry($this->s_country);

		$copyObj->setSStreet($this->s_street);

		$copyObj->setSStreet2($this->s_street_2);

		$copyObj->setSCity($this->s_city);

		$copyObj->setSState($this->s_state);

		$copyObj->setSZip($this->s_zip);

		$copyObj->setTotalPrice($this->total_price);

		$copyObj->setTax($this->tax);

		$copyObj->setShippingCost($this->shipping_cost);

		$copyObj->setDiscount($this->discount);

		$copyObj->setTransactionId($this->transaction_id);

		$copyObj->setNotes($this->notes);

		$copyObj->setPaymentMethod($this->payment_method);

		$copyObj->setShipping($this->shipping);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getOrderedItems() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addOrderedItem($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Ordered Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     OrderedPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new OrderedPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a sfGuardUser object.
	 *
	 * @param      sfGuardUser $v
	 * @return     Ordered The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setsfGuardUser(sfGuardUser $v = null)
	{
		if ($v === null) {
			$this->setUserId(NULL);
		} else {
			$this->setUserId($v->getId());
		}

		$this->asfGuardUser = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the sfGuardUser object, it will not be re-added.
		if ($v !== null) {
			$v->addOrdered($this);
		}

		return $this;
	}


	/**
	 * Get the associated sfGuardUser object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     sfGuardUser The associated sfGuardUser object.
	 * @throws     PropelException
	 */
	public function getsfGuardUser(PropelPDO $con = null)
	{
		if ($this->asfGuardUser === null && ($this->user_id !== null)) {
			$this->asfGuardUser = sfGuardUserPeer::retrieveByPk($this->user_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->asfGuardUser->addOrdereds($this);
			 */
		}
		return $this->asfGuardUser;
	}

	/**
	 * Clears out the collOrderedItems collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addOrderedItems()
	 */
	public function clearOrderedItems()
	{
		$this->collOrderedItems = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collOrderedItems collection (array).
	 *
	 * By default this just sets the collOrderedItems collection to an empty array (like clearcollOrderedItems());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initOrderedItems()
	{
		$this->collOrderedItems = array();
	}

	/**
	 * Gets an array of OrderedItem objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this Ordered has previously been saved, it will retrieve
	 * related OrderedItems from storage. If this Ordered is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array OrderedItem[]
	 * @throws     PropelException
	 */
	public function getOrderedItems($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItems === null) {
			if ($this->isNew()) {
			   $this->collOrderedItems = array();
			} else {

				$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

				OrderedItemPeer::addSelectColumns($criteria);
				$this->collOrderedItems = OrderedItemPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

				OrderedItemPeer::addSelectColumns($criteria);
				if (!isset($this->lastOrderedItemCriteria) || !$this->lastOrderedItemCriteria->equals($criteria)) {
					$this->collOrderedItems = OrderedItemPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOrderedItemCriteria = $criteria;
		return $this->collOrderedItems;
	}

	/**
	 * Returns the number of related OrderedItem objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related OrderedItem objects.
	 * @throws     PropelException
	 */
	public function countOrderedItems(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collOrderedItems === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

				$count = OrderedItemPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

				if (!isset($this->lastOrderedItemCriteria) || !$this->lastOrderedItemCriteria->equals($criteria)) {
					$count = OrderedItemPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collOrderedItems);
				}
			} else {
				$count = count($this->collOrderedItems);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a OrderedItem object to this object
	 * through the OrderedItem foreign key attribute.
	 *
	 * @param      OrderedItem $l OrderedItem
	 * @return     void
	 * @throws     PropelException
	 */
	public function addOrderedItem(OrderedItem $l)
	{
		if ($this->collOrderedItems === null) {
			$this->initOrderedItems();
		}
		if (!in_array($l, $this->collOrderedItems, true)) { // only add it if the **same** object is not already associated
			array_push($this->collOrderedItems, $l);
			$l->setOrdered($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Ordered is new, it will return
	 * an empty collection; or if this Ordered has previously
	 * been saved, it will retrieve related OrderedItems from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Ordered.
	 */
	public function getOrderedItemsJoinMeasures($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItems === null) {
			if ($this->isNew()) {
				$this->collOrderedItems = array();
			} else {

				$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

				$this->collOrderedItems = OrderedItemPeer::doSelectJoinMeasures($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OrderedItemPeer::ORDER_ID, $this->id);

			if (!isset($this->lastOrderedItemCriteria) || !$this->lastOrderedItemCriteria->equals($criteria)) {
				$this->collOrderedItems = OrderedItemPeer::doSelectJoinMeasures($criteria, $con, $join_behavior);
			}
		}
		$this->lastOrderedItemCriteria = $criteria;

		return $this->collOrderedItems;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collOrderedItems) {
				foreach ((array) $this->collOrderedItems as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collOrderedItems = null;
			$this->asfGuardUser = null;
	}

} // BaseOrdered
