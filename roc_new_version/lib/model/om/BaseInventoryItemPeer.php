<?php

/**
 * Base static class for performing query and update operations on the 'inventory_item' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseInventoryItemPeer {

	/** the default database name for this class */
	const DATABASE_NAME = 'propel';

	/** the table name for this class */
	const TABLE_NAME = 'inventory_item';

	/** the related Propel class for this table */
	const OM_CLASS = 'InventoryItem';

	/** A class that can be returned by this peer. */
	const CLASS_DEFAULT = 'lib.model.InventoryItem';

	/** the related TableMap class for this table */
	const TM_CLASS = 'InventoryItemTableMap';
	
	/** The total number of columns. */
	const NUM_COLUMNS = 55;

	/** The number of lazy-loaded columns. */
	const NUM_LAZY_LOAD_COLUMNS = 0;

	/** the column name for the ID field */
	const ID = 'inventory_item.ID';

	/** the column name for the SKU field */
	const SKU = 'inventory_item.SKU';

	/** the column name for the TITLE field */
	const TITLE = 'inventory_item.TITLE';

	/** the column name for the BRAND_ID field */
	const BRAND_ID = 'inventory_item.BRAND_ID';

	/** the column name for the DESCRIPTION_SHORT field */
	const DESCRIPTION_SHORT = 'inventory_item.DESCRIPTION_SHORT';

	/** the column name for the DESCRIPTION_LONG field */
	const DESCRIPTION_LONG = 'inventory_item.DESCRIPTION_LONG';

	/** the column name for the SIZE field */
	const SIZE = 'inventory_item.SIZE';

	/** the column name for the COLOR field */
	const COLOR = 'inventory_item.COLOR';

	/** the column name for the GENDER field */
	const GENDER = 'inventory_item.GENDER';

	/** the column name for the SIZING_ID field */
	const SIZING_ID = 'inventory_item.SIZING_ID';

	/** the column name for the CUSTOMIZABLE field */
	const CUSTOMIZABLE = 'inventory_item.CUSTOMIZABLE';

	/** the column name for the VIDEO field */
	const VIDEO = 'inventory_item.VIDEO';

	/** the column name for the MEASURE_ID field */
	const MEASURE_ID = 'inventory_item.MEASURE_ID';

	/** the column name for the QTY_ON_HAND field */
	const QTY_ON_HAND = 'inventory_item.QTY_ON_HAND';

	/** the column name for the STD_UNIT_PRICE field */
	const STD_UNIT_PRICE = 'inventory_item.STD_UNIT_PRICE';

	/** the column name for the MSRP field */
	const MSRP = 'inventory_item.MSRP';

	/** the column name for the CLEARANCE field */
	const CLEARANCE = 'inventory_item.CLEARANCE';

	/** the column name for the SALE_START_DATE field */
	const SALE_START_DATE = 'inventory_item.SALE_START_DATE';

	/** the column name for the SALE_END_DATE field */
	const SALE_END_DATE = 'inventory_item.SALE_END_DATE';

	/** the column name for the SALE_METHOD field */
	const SALE_METHOD = 'inventory_item.SALE_METHOD';

	/** the column name for the SALE_PRICE field */
	const SALE_PRICE = 'inventory_item.SALE_PRICE';

	/** the column name for the SALE_PERCENT field */
	const SALE_PERCENT = 'inventory_item.SALE_PERCENT';

	/** the column name for the TAX_CLASS field */
	const TAX_CLASS = 'inventory_item.TAX_CLASS';

	/** the column name for the SHIP_WEIGHT field */
	const SHIP_WEIGHT = 'inventory_item.SHIP_WEIGHT';

	/** the column name for the SALE_PROMO_DISCOUNT field */
	const SALE_PROMO_DISCOUNT = 'inventory_item.SALE_PROMO_DISCOUNT';

	/** the column name for the FINISH field */
	const FINISH = 'inventory_item.FINISH';

	/** the column name for the DOCUMENTS field */
	const DOCUMENTS = 'inventory_item.DOCUMENTS';

	/** the column name for the HAND field */
	const HAND = 'inventory_item.HAND';

	/** the column name for the MATRIX field */
	const MATRIX = 'inventory_item.MATRIX';

	/** the column name for the ROOTITEM field */
	const ROOTITEM = 'inventory_item.ROOTITEM';

	/** the column name for the FEATURES field */
	const FEATURES = 'inventory_item.FEATURES';

	/** the column name for the SETUP_CHARGE field */
	const SETUP_CHARGE = 'inventory_item.SETUP_CHARGE';

	/** the column name for the DATE_UPDATED field */
	const DATE_UPDATED = 'inventory_item.DATE_UPDATED';

	/** the column name for the TIME_UPDATED field */
	const TIME_UPDATED = 'inventory_item.TIME_UPDATED';

	/** the column name for the RECENTLY_VIEWED field */
	const RECENTLY_VIEWED = 'inventory_item.RECENTLY_VIEWED';

	/** the column name for the ADDITIONAL_FREIGHT field */
	const ADDITIONAL_FREIGHT = 'inventory_item.ADDITIONAL_FREIGHT';

	/** the column name for the FREE_SHIPPING field */
	const FREE_SHIPPING = 'inventory_item.FREE_SHIPPING';

	/** the column name for the PHONE_ORDER field */
	const PHONE_ORDER = 'inventory_item.PHONE_ORDER';

	/** the column name for the UDF_IT_SWC_BEIGE field */
	const UDF_IT_SWC_BEIGE = 'inventory_item.UDF_IT_SWC_BEIGE';

	/** the column name for the UDF_IT_SWC_BLACK field */
	const UDF_IT_SWC_BLACK = 'inventory_item.UDF_IT_SWC_BLACK';

	/** the column name for the UDF_IT_SWC_BLUE field */
	const UDF_IT_SWC_BLUE = 'inventory_item.UDF_IT_SWC_BLUE';

	/** the column name for the UDF_IT_SWC_BROWN field */
	const UDF_IT_SWC_BROWN = 'inventory_item.UDF_IT_SWC_BROWN';

	/** the column name for the UDF_IT_SWC_CAMO field */
	const UDF_IT_SWC_CAMO = 'inventory_item.UDF_IT_SWC_CAMO';

	/** the column name for the UDF_IT_SWC_GOLD field */
	const UDF_IT_SWC_GOLD = 'inventory_item.UDF_IT_SWC_GOLD';

	/** the column name for the UDF_IT_SWC_GRAY field */
	const UDF_IT_SWC_GRAY = 'inventory_item.UDF_IT_SWC_GRAY';

	/** the column name for the UDF_IT_SWC_GREEN field */
	const UDF_IT_SWC_GREEN = 'inventory_item.UDF_IT_SWC_GREEN';

	/** the column name for the UDF_IT_SWC_ORANGE field */
	const UDF_IT_SWC_ORANGE = 'inventory_item.UDF_IT_SWC_ORANGE';

	/** the column name for the UDF_IT_SWC_PINK field */
	const UDF_IT_SWC_PINK = 'inventory_item.UDF_IT_SWC_PINK';

	/** the column name for the UDF_IT_SWC_PURPLE field */
	const UDF_IT_SWC_PURPLE = 'inventory_item.UDF_IT_SWC_PURPLE';

	/** the column name for the UDF_IT_SWC_RED field */
	const UDF_IT_SWC_RED = 'inventory_item.UDF_IT_SWC_RED';

	/** the column name for the UDF_IT_SWC_SILVER field */
	const UDF_IT_SWC_SILVER = 'inventory_item.UDF_IT_SWC_SILVER';

	/** the column name for the UDF_IT_SWC_WHITE field */
	const UDF_IT_SWC_WHITE = 'inventory_item.UDF_IT_SWC_WHITE';

	/** the column name for the UDF_IT_SWC_YELLOW field */
	const UDF_IT_SWC_YELLOW = 'inventory_item.UDF_IT_SWC_YELLOW';

	/** the column name for the CREATED_AT field */
	const CREATED_AT = 'inventory_item.CREATED_AT';

	/** the column name for the UPDATED_AT field */
	const UPDATED_AT = 'inventory_item.UPDATED_AT';

	/**
	 * An identiy map to hold any loaded instances of InventoryItem objects.
	 * This must be public so that other peer classes can access this when hydrating from JOIN
	 * queries.
	 * @var        array InventoryItem[]
	 */
	public static $instances = array();


	// symfony behavior
	
	/**
	 * Indicates whether the current model includes I18N.
	 */
	const IS_I18N = false;

	/**
	 * holds an array of fieldnames
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
	 */
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('Id', 'Sku', 'Title', 'BrandId', 'DescriptionShort', 'DescriptionLong', 'Size', 'Color', 'Gender', 'SizingId', 'Customizable', 'Video', 'MeasureId', 'QtyOnHand', 'StdUnitPrice', 'Msrp', 'Clearance', 'SaleStartDate', 'SaleEndDate', 'SaleMethod', 'SalePrice', 'SalePercent', 'TaxClass', 'ShipWeight', 'SalePromoDiscount', 'Finish', 'Documents', 'Hand', 'Matrix', 'Rootitem', 'Features', 'SetupCharge', 'DateUpdated', 'TimeUpdated', 'RecentlyViewed', 'AdditionalFreight', 'FreeShipping', 'PhoneOrder', 'UdfItSwcBeige', 'UdfItSwcBlack', 'UdfItSwcBlue', 'UdfItSwcBrown', 'UdfItSwcCamo', 'UdfItSwcGold', 'UdfItSwcGray', 'UdfItSwcGreen', 'UdfItSwcOrange', 'UdfItSwcPink', 'UdfItSwcPurple', 'UdfItSwcRed', 'UdfItSwcSilver', 'UdfItSwcWhite', 'UdfItSwcYellow', 'CreatedAt', 'UpdatedAt', ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'sku', 'title', 'brandId', 'descriptionShort', 'descriptionLong', 'size', 'color', 'gender', 'sizingId', 'customizable', 'video', 'measureId', 'qtyOnHand', 'stdUnitPrice', 'msrp', 'clearance', 'saleStartDate', 'saleEndDate', 'saleMethod', 'salePrice', 'salePercent', 'taxClass', 'shipWeight', 'salePromoDiscount', 'finish', 'documents', 'hand', 'matrix', 'rootitem', 'features', 'setupCharge', 'dateUpdated', 'timeUpdated', 'recentlyViewed', 'additionalFreight', 'freeShipping', 'phoneOrder', 'udfItSwcBeige', 'udfItSwcBlack', 'udfItSwcBlue', 'udfItSwcBrown', 'udfItSwcCamo', 'udfItSwcGold', 'udfItSwcGray', 'udfItSwcGreen', 'udfItSwcOrange', 'udfItSwcPink', 'udfItSwcPurple', 'udfItSwcRed', 'udfItSwcSilver', 'udfItSwcWhite', 'udfItSwcYellow', 'createdAt', 'updatedAt', ),
		BasePeer::TYPE_COLNAME => array (self::ID, self::SKU, self::TITLE, self::BRAND_ID, self::DESCRIPTION_SHORT, self::DESCRIPTION_LONG, self::SIZE, self::COLOR, self::GENDER, self::SIZING_ID, self::CUSTOMIZABLE, self::VIDEO, self::MEASURE_ID, self::QTY_ON_HAND, self::STD_UNIT_PRICE, self::MSRP, self::CLEARANCE, self::SALE_START_DATE, self::SALE_END_DATE, self::SALE_METHOD, self::SALE_PRICE, self::SALE_PERCENT, self::TAX_CLASS, self::SHIP_WEIGHT, self::SALE_PROMO_DISCOUNT, self::FINISH, self::DOCUMENTS, self::HAND, self::MATRIX, self::ROOTITEM, self::FEATURES, self::SETUP_CHARGE, self::DATE_UPDATED, self::TIME_UPDATED, self::RECENTLY_VIEWED, self::ADDITIONAL_FREIGHT, self::FREE_SHIPPING, self::PHONE_ORDER, self::UDF_IT_SWC_BEIGE, self::UDF_IT_SWC_BLACK, self::UDF_IT_SWC_BLUE, self::UDF_IT_SWC_BROWN, self::UDF_IT_SWC_CAMO, self::UDF_IT_SWC_GOLD, self::UDF_IT_SWC_GRAY, self::UDF_IT_SWC_GREEN, self::UDF_IT_SWC_ORANGE, self::UDF_IT_SWC_PINK, self::UDF_IT_SWC_PURPLE, self::UDF_IT_SWC_RED, self::UDF_IT_SWC_SILVER, self::UDF_IT_SWC_WHITE, self::UDF_IT_SWC_YELLOW, self::CREATED_AT, self::UPDATED_AT, ),
		BasePeer::TYPE_FIELDNAME => array ('id', 'sku', 'title', 'brand_id', 'description_short', 'description_long', 'size', 'color', 'gender', 'sizing_id', 'customizable', 'video', 'measure_id', 'qty_on_hand', 'std_unit_price', 'msrp', 'clearance', 'sale_start_date', 'sale_end_date', 'sale_method', 'sale_price', 'sale_percent', 'tax_class', 'ship_weight', 'sale_promo_discount', 'finish', 'documents', 'hand', 'matrix', 'rootitem', 'features', 'setup_charge', 'date_updated', 'time_updated', 'recently_viewed', 'additional_freight', 'free_shipping', 'phone_order', 'udf_it_swc_beige', 'udf_it_swc_black', 'udf_it_swc_blue', 'udf_it_swc_brown', 'udf_it_swc_camo', 'udf_it_swc_gold', 'udf_it_swc_gray', 'udf_it_swc_green', 'udf_it_swc_orange', 'udf_it_swc_pink', 'udf_it_swc_purple', 'udf_it_swc_red', 'udf_it_swc_silver', 'udf_it_swc_white', 'udf_it_swc_yellow', 'created_at', 'updated_at', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, )
	);

	/**
	 * holds an array of keys for quick access to the fieldnames array
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
	 */
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Sku' => 1, 'Title' => 2, 'BrandId' => 3, 'DescriptionShort' => 4, 'DescriptionLong' => 5, 'Size' => 6, 'Color' => 7, 'Gender' => 8, 'SizingId' => 9, 'Customizable' => 10, 'Video' => 11, 'MeasureId' => 12, 'QtyOnHand' => 13, 'StdUnitPrice' => 14, 'Msrp' => 15, 'Clearance' => 16, 'SaleStartDate' => 17, 'SaleEndDate' => 18, 'SaleMethod' => 19, 'SalePrice' => 20, 'SalePercent' => 21, 'TaxClass' => 22, 'ShipWeight' => 23, 'SalePromoDiscount' => 24, 'Finish' => 25, 'Documents' => 26, 'Hand' => 27, 'Matrix' => 28, 'Rootitem' => 29, 'Features' => 30, 'SetupCharge' => 31, 'DateUpdated' => 32, 'TimeUpdated' => 33, 'RecentlyViewed' => 34, 'AdditionalFreight' => 35, 'FreeShipping' => 36, 'PhoneOrder' => 37, 'UdfItSwcBeige' => 38, 'UdfItSwcBlack' => 39, 'UdfItSwcBlue' => 40, 'UdfItSwcBrown' => 41, 'UdfItSwcCamo' => 42, 'UdfItSwcGold' => 43, 'UdfItSwcGray' => 44, 'UdfItSwcGreen' => 45, 'UdfItSwcOrange' => 46, 'UdfItSwcPink' => 47, 'UdfItSwcPurple' => 48, 'UdfItSwcRed' => 49, 'UdfItSwcSilver' => 50, 'UdfItSwcWhite' => 51, 'UdfItSwcYellow' => 52, 'CreatedAt' => 53, 'UpdatedAt' => 54, ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'sku' => 1, 'title' => 2, 'brandId' => 3, 'descriptionShort' => 4, 'descriptionLong' => 5, 'size' => 6, 'color' => 7, 'gender' => 8, 'sizingId' => 9, 'customizable' => 10, 'video' => 11, 'measureId' => 12, 'qtyOnHand' => 13, 'stdUnitPrice' => 14, 'msrp' => 15, 'clearance' => 16, 'saleStartDate' => 17, 'saleEndDate' => 18, 'saleMethod' => 19, 'salePrice' => 20, 'salePercent' => 21, 'taxClass' => 22, 'shipWeight' => 23, 'salePromoDiscount' => 24, 'finish' => 25, 'documents' => 26, 'hand' => 27, 'matrix' => 28, 'rootitem' => 29, 'features' => 30, 'setupCharge' => 31, 'dateUpdated' => 32, 'timeUpdated' => 33, 'recentlyViewed' => 34, 'additionalFreight' => 35, 'freeShipping' => 36, 'phoneOrder' => 37, 'udfItSwcBeige' => 38, 'udfItSwcBlack' => 39, 'udfItSwcBlue' => 40, 'udfItSwcBrown' => 41, 'udfItSwcCamo' => 42, 'udfItSwcGold' => 43, 'udfItSwcGray' => 44, 'udfItSwcGreen' => 45, 'udfItSwcOrange' => 46, 'udfItSwcPink' => 47, 'udfItSwcPurple' => 48, 'udfItSwcRed' => 49, 'udfItSwcSilver' => 50, 'udfItSwcWhite' => 51, 'udfItSwcYellow' => 52, 'createdAt' => 53, 'updatedAt' => 54, ),
		BasePeer::TYPE_COLNAME => array (self::ID => 0, self::SKU => 1, self::TITLE => 2, self::BRAND_ID => 3, self::DESCRIPTION_SHORT => 4, self::DESCRIPTION_LONG => 5, self::SIZE => 6, self::COLOR => 7, self::GENDER => 8, self::SIZING_ID => 9, self::CUSTOMIZABLE => 10, self::VIDEO => 11, self::MEASURE_ID => 12, self::QTY_ON_HAND => 13, self::STD_UNIT_PRICE => 14, self::MSRP => 15, self::CLEARANCE => 16, self::SALE_START_DATE => 17, self::SALE_END_DATE => 18, self::SALE_METHOD => 19, self::SALE_PRICE => 20, self::SALE_PERCENT => 21, self::TAX_CLASS => 22, self::SHIP_WEIGHT => 23, self::SALE_PROMO_DISCOUNT => 24, self::FINISH => 25, self::DOCUMENTS => 26, self::HAND => 27, self::MATRIX => 28, self::ROOTITEM => 29, self::FEATURES => 30, self::SETUP_CHARGE => 31, self::DATE_UPDATED => 32, self::TIME_UPDATED => 33, self::RECENTLY_VIEWED => 34, self::ADDITIONAL_FREIGHT => 35, self::FREE_SHIPPING => 36, self::PHONE_ORDER => 37, self::UDF_IT_SWC_BEIGE => 38, self::UDF_IT_SWC_BLACK => 39, self::UDF_IT_SWC_BLUE => 40, self::UDF_IT_SWC_BROWN => 41, self::UDF_IT_SWC_CAMO => 42, self::UDF_IT_SWC_GOLD => 43, self::UDF_IT_SWC_GRAY => 44, self::UDF_IT_SWC_GREEN => 45, self::UDF_IT_SWC_ORANGE => 46, self::UDF_IT_SWC_PINK => 47, self::UDF_IT_SWC_PURPLE => 48, self::UDF_IT_SWC_RED => 49, self::UDF_IT_SWC_SILVER => 50, self::UDF_IT_SWC_WHITE => 51, self::UDF_IT_SWC_YELLOW => 52, self::CREATED_AT => 53, self::UPDATED_AT => 54, ),
		BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'sku' => 1, 'title' => 2, 'brand_id' => 3, 'description_short' => 4, 'description_long' => 5, 'size' => 6, 'color' => 7, 'gender' => 8, 'sizing_id' => 9, 'customizable' => 10, 'video' => 11, 'measure_id' => 12, 'qty_on_hand' => 13, 'std_unit_price' => 14, 'msrp' => 15, 'clearance' => 16, 'sale_start_date' => 17, 'sale_end_date' => 18, 'sale_method' => 19, 'sale_price' => 20, 'sale_percent' => 21, 'tax_class' => 22, 'ship_weight' => 23, 'sale_promo_discount' => 24, 'finish' => 25, 'documents' => 26, 'hand' => 27, 'matrix' => 28, 'rootitem' => 29, 'features' => 30, 'setup_charge' => 31, 'date_updated' => 32, 'time_updated' => 33, 'recently_viewed' => 34, 'additional_freight' => 35, 'free_shipping' => 36, 'phone_order' => 37, 'udf_it_swc_beige' => 38, 'udf_it_swc_black' => 39, 'udf_it_swc_blue' => 40, 'udf_it_swc_brown' => 41, 'udf_it_swc_camo' => 42, 'udf_it_swc_gold' => 43, 'udf_it_swc_gray' => 44, 'udf_it_swc_green' => 45, 'udf_it_swc_orange' => 46, 'udf_it_swc_pink' => 47, 'udf_it_swc_purple' => 48, 'udf_it_swc_red' => 49, 'udf_it_swc_silver' => 50, 'udf_it_swc_white' => 51, 'udf_it_swc_yellow' => 52, 'created_at' => 53, 'updated_at' => 54, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, )
	);

	/**
	 * Translates a fieldname to another type
	 *
	 * @param      string $name field name
	 * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @param      string $toType   One of the class type constants
	 * @return     string translated name of the field.
	 * @throws     PropelException - if the specified name could not be found in the fieldname mappings.
	 */
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	/**
	 * Returns an array of field names.
	 *
	 * @param      string $type The type of fieldnames to return:
	 *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     array A list of field names
	 */

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	/**
	 * Convenience method which changes table.column to alias.column.
	 *
	 * Using this method you can maintain SQL abstraction while using column aliases.
	 * <code>
	 *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
	 *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
	 * </code>
	 * @param      string $alias The alias for the current table.
	 * @param      string $column The column name for current table. (i.e. InventoryItemPeer::COLUMN_NAME).
	 * @return     string
	 */
	public static function alias($alias, $column)
	{
		return str_replace(InventoryItemPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	/**
	 * Add all the columns needed to create a new object.
	 *
	 * Note: any columns that were marked with lazyLoad="true" in the
	 * XML schema will not be added to the select list and only loaded
	 * on demand.
	 *
	 * @param      criteria object containing the columns to add.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function addSelectColumns(Criteria $criteria)
	{
		$criteria->addSelectColumn(InventoryItemPeer::ID);
		$criteria->addSelectColumn(InventoryItemPeer::SKU);
		$criteria->addSelectColumn(InventoryItemPeer::TITLE);
		$criteria->addSelectColumn(InventoryItemPeer::BRAND_ID);
		$criteria->addSelectColumn(InventoryItemPeer::DESCRIPTION_SHORT);
		$criteria->addSelectColumn(InventoryItemPeer::DESCRIPTION_LONG);
		$criteria->addSelectColumn(InventoryItemPeer::SIZE);
		$criteria->addSelectColumn(InventoryItemPeer::COLOR);
		$criteria->addSelectColumn(InventoryItemPeer::GENDER);
		$criteria->addSelectColumn(InventoryItemPeer::SIZING_ID);
		$criteria->addSelectColumn(InventoryItemPeer::CUSTOMIZABLE);
		$criteria->addSelectColumn(InventoryItemPeer::VIDEO);
		$criteria->addSelectColumn(InventoryItemPeer::MEASURE_ID);
		$criteria->addSelectColumn(InventoryItemPeer::QTY_ON_HAND);
		$criteria->addSelectColumn(InventoryItemPeer::STD_UNIT_PRICE);
		$criteria->addSelectColumn(InventoryItemPeer::MSRP);
		$criteria->addSelectColumn(InventoryItemPeer::CLEARANCE);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_START_DATE);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_END_DATE);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_METHOD);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_PRICE);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_PERCENT);
		$criteria->addSelectColumn(InventoryItemPeer::TAX_CLASS);
		$criteria->addSelectColumn(InventoryItemPeer::SHIP_WEIGHT);
		$criteria->addSelectColumn(InventoryItemPeer::SALE_PROMO_DISCOUNT);
		$criteria->addSelectColumn(InventoryItemPeer::FINISH);
		$criteria->addSelectColumn(InventoryItemPeer::DOCUMENTS);
		$criteria->addSelectColumn(InventoryItemPeer::HAND);
		$criteria->addSelectColumn(InventoryItemPeer::MATRIX);
		$criteria->addSelectColumn(InventoryItemPeer::ROOTITEM);
		$criteria->addSelectColumn(InventoryItemPeer::FEATURES);
		$criteria->addSelectColumn(InventoryItemPeer::SETUP_CHARGE);
		$criteria->addSelectColumn(InventoryItemPeer::DATE_UPDATED);
		$criteria->addSelectColumn(InventoryItemPeer::TIME_UPDATED);
		$criteria->addSelectColumn(InventoryItemPeer::RECENTLY_VIEWED);
		$criteria->addSelectColumn(InventoryItemPeer::ADDITIONAL_FREIGHT);
		$criteria->addSelectColumn(InventoryItemPeer::FREE_SHIPPING);
		$criteria->addSelectColumn(InventoryItemPeer::PHONE_ORDER);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_BEIGE);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_BLACK);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_BLUE);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_BROWN);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_CAMO);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_GOLD);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_GRAY);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_GREEN);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_ORANGE);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_PINK);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_PURPLE);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_RED);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_SILVER);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_WHITE);
		$criteria->addSelectColumn(InventoryItemPeer::UDF_IT_SWC_YELLOW);
		$criteria->addSelectColumn(InventoryItemPeer::CREATED_AT);
		$criteria->addSelectColumn(InventoryItemPeer::UPDATED_AT);
	}

	/**
	 * Returns the number of rows matching criteria.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @return     int Number of matching rows.
	 */
	public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
	{
		// we may modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}

		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		$criteria->setDbName(self::DATABASE_NAME); // Set the correct dbName

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		// BasePeer returns a PDOStatement
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}
	/**
	 * Method to select one object from the DB.
	 *
	 * @param      Criteria $criteria object used to create the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     InventoryItem
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = InventoryItemPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	/**
	 * Method to do selects.
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     array Array of selected Objects
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelect(Criteria $criteria, PropelPDO $con = null)
	{
		return InventoryItemPeer::populateObjects(InventoryItemPeer::doSelectStmt($criteria, $con));
	}
	/**
	 * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
	 *
	 * Use this method directly if you want to work with an executed statement durirectly (for example
	 * to perform your own object hydration).
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con The connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @return     PDOStatement The executed PDOStatement object.
	 * @see        BasePeer::doSelect()
	 */
	public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		if (!$criteria->hasSelectClause()) {
			$criteria = clone $criteria;
			InventoryItemPeer::addSelectColumns($criteria);
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		// BasePeer returns a PDOStatement
		return BasePeer::doSelect($criteria, $con);
	}
	/**
	 * Adds an object to the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doSelect*()
	 * methods in your stub classes -- you may need to explicitly add objects
	 * to the cache in order to ensure that the same objects are always returned by doSelect*()
	 * and retrieveByPK*() calls.
	 *
	 * @param      InventoryItem $value A InventoryItem object.
	 * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
	 */
	public static function addInstanceToPool(InventoryItem $obj, $key = null)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if ($key === null) {
				$key = (string) $obj->getSku();
			} // if key === null
			self::$instances[$key] = $obj;
		}
	}

	/**
	 * Removes an object from the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doDelete
	 * methods in your stub classes -- you may need to explicitly remove objects
	 * from the cache in order to prevent returning objects that no longer exist.
	 *
	 * @param      mixed $value A InventoryItem object or a primary key value.
	 */
	public static function removeInstanceFromPool($value)
	{
		if (Propel::isInstancePoolingEnabled() && $value !== null) {
			if (is_object($value) && $value instanceof InventoryItem) {
				$key = (string) $value->getSku();
			} elseif (is_scalar($value)) {
				// assume we've been passed a primary key
				$key = (string) $value;
			} else {
				$e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or InventoryItem object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
				throw $e;
			}

			unset(self::$instances[$key]);
		}
	} // removeInstanceFromPool()

	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
	 * @return     InventoryItem Found object or NULL if 1) no instance exists for specified key or 2) instance pooling has been disabled.
	 * @see        getPrimaryKeyHash()
	 */
	public static function getInstanceFromPool($key)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if (isset(self::$instances[$key])) {
				return self::$instances[$key];
			}
		}
		return null; // just to be explicit
	}
	
	/**
	 * Clear the instance pool.
	 *
	 * @return     void
	 */
	public static function clearInstancePool()
	{
		self::$instances = array();
	}
	
	/**
	 * Method to invalidate the instance pool of all tables related to inventory_item
	 * by a foreign key with ON DELETE CASCADE
	 */
	public static function clearRelatedInstancePool()
	{
	}

	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      array $row PropelPDO resultset row.
	 * @param      int $startcol The 0-based offset for reading from the resultset row.
	 * @return     string A string version of PK or NULL if the components of primary key in result array are all null.
	 */
	public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
	{
		// If the PK cannot be derived from the row, return NULL.
		if ($row[$startcol + 1] === null) {
			return null;
		}
		return (string) $row[$startcol + 1];
	}

	/**
	 * The returned array will contain objects of the default type or
	 * objects that inherit from the default.
	 *
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function populateObjects(PDOStatement $stmt)
	{
		$results = array();
	
		// set the class once to avoid overhead in the loop
		$cls = InventoryItemPeer::getOMClass(false);
		// populate the object(s)
		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj = InventoryItemPeer::getInstanceFromPool($key))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj->hydrate($row, 0, true); // rehydrate
				$results[] = $obj;
			} else {
				$obj = new $cls();
				$obj->hydrate($row);
				$results[] = $obj;
				InventoryItemPeer::addInstanceToPool($obj, $key);
			} // if key exists
		}
		$stmt->closeCursor();
		return $results;
	}

	/**
	 * Returns the number of rows matching criteria, joining the related Brand table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Sizings table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinSizings(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Measures table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinMeasures(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with their Brand objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);
		BrandPeer::addSelectColumns($criteria);

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = BrandPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = BrandPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = BrandPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					BrandPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded
				
				// Add the $obj1 (InventoryItem) to $obj2 (Brand)
				$obj2->addInventoryItem($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with their Sizings objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinSizings(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);
		SizingsPeer::addSelectColumns($criteria);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = SizingsPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = SizingsPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = SizingsPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					SizingsPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded
				
				// Add the $obj1 (InventoryItem) to $obj2 (Sizings)
				$obj2->addInventoryItem($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with their Measures objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinMeasures(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);
		MeasuresPeer::addSelectColumns($criteria);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = MeasuresPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = MeasuresPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = MeasuresPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					MeasuresPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded
				
				// Add the $obj1 (InventoryItem) to $obj2 (Measures)
				$obj2->addInventoryItem($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining all related tables
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}

	/**
	 * Selects a collection of InventoryItem objects pre-filled with all related objects.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol2 = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);

		BrandPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (BrandPeer::NUM_COLUMNS - BrandPeer::NUM_LAZY_LOAD_COLUMNS);

		SizingsPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (SizingsPeer::NUM_COLUMNS - SizingsPeer::NUM_LAZY_LOAD_COLUMNS);

		MeasuresPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (MeasuresPeer::NUM_COLUMNS - MeasuresPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

			// Add objects for joined Brand rows

			$key2 = BrandPeer::getPrimaryKeyHashFromRow($row, $startcol2);
			if ($key2 !== null) {
				$obj2 = BrandPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = BrandPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					BrandPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj2 (Brand)
				$obj2->addInventoryItem($obj1);
			} // if joined row not null

			// Add objects for joined Sizings rows

			$key3 = SizingsPeer::getPrimaryKeyHashFromRow($row, $startcol3);
			if ($key3 !== null) {
				$obj3 = SizingsPeer::getInstanceFromPool($key3);
				if (!$obj3) {

					$cls = SizingsPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					SizingsPeer::addInstanceToPool($obj3, $key3);
				} // if obj3 loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj3 (Sizings)
				$obj3->addInventoryItem($obj1);
			} // if joined row not null

			// Add objects for joined Measures rows

			$key4 = MeasuresPeer::getPrimaryKeyHashFromRow($row, $startcol4);
			if ($key4 !== null) {
				$obj4 = MeasuresPeer::getInstanceFromPool($key4);
				if (!$obj4) {

					$cls = MeasuresPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					MeasuresPeer::addInstanceToPool($obj4, $key4);
				} // if obj4 loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj4 (Measures)
				$obj4->addInventoryItem($obj1);
			} // if joined row not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Brand table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptBrand(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Sizings table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptSizings(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Measures table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptMeasures(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(InventoryItemPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			InventoryItemPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with all related objects except Brand.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptBrand(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol2 = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);

		SizingsPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (SizingsPeer::NUM_COLUMNS - SizingsPeer::NUM_LAZY_LOAD_COLUMNS);

		MeasuresPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (MeasuresPeer::NUM_COLUMNS - MeasuresPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined Sizings rows

				$key2 = SizingsPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = SizingsPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = SizingsPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					SizingsPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj2 (Sizings)
				$obj2->addInventoryItem($obj1);

			} // if joined row is not null

				// Add objects for joined Measures rows

				$key3 = MeasuresPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = MeasuresPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = MeasuresPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					MeasuresPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj3 (Measures)
				$obj3->addInventoryItem($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with all related objects except Sizings.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptSizings(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol2 = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);

		BrandPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (BrandPeer::NUM_COLUMNS - BrandPeer::NUM_LAZY_LOAD_COLUMNS);

		MeasuresPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (MeasuresPeer::NUM_COLUMNS - MeasuresPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::MEASURE_ID, MeasuresPeer::ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined Brand rows

				$key2 = BrandPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = BrandPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = BrandPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					BrandPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj2 (Brand)
				$obj2->addInventoryItem($obj1);

			} // if joined row is not null

				// Add objects for joined Measures rows

				$key3 = MeasuresPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = MeasuresPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = MeasuresPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					MeasuresPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj3 (Measures)
				$obj3->addInventoryItem($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of InventoryItem objects pre-filled with all related objects except Measures.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of InventoryItem objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptMeasures(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		InventoryItemPeer::addSelectColumns($criteria);
		$startcol2 = (InventoryItemPeer::NUM_COLUMNS - InventoryItemPeer::NUM_LAZY_LOAD_COLUMNS);

		BrandPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (BrandPeer::NUM_COLUMNS - BrandPeer::NUM_LAZY_LOAD_COLUMNS);

		SizingsPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (SizingsPeer::NUM_COLUMNS - SizingsPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(InventoryItemPeer::BRAND_ID, BrandPeer::ID, $join_behavior);

		$criteria->addJoin(InventoryItemPeer::SIZING_ID, SizingsPeer::ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = InventoryItemPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = InventoryItemPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://propel.phpdb.org/trac/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = InventoryItemPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				InventoryItemPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined Brand rows

				$key2 = BrandPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = BrandPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = BrandPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					BrandPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj2 (Brand)
				$obj2->addInventoryItem($obj1);

			} // if joined row is not null

				// Add objects for joined Sizings rows

				$key3 = SizingsPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = SizingsPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = SizingsPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					SizingsPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (InventoryItem) to the collection in $obj3 (Sizings)
				$obj3->addInventoryItem($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}

	/**
	 * Returns the TableMap related to this peer.
	 * This method is not needed for general use but a specific application could have a need.
	 * @return     TableMap
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	/**
	 * Add a TableMap instance to the database for this peer class.
	 */
	public static function buildTableMap()
	{
	  $dbMap = Propel::getDatabaseMap(BaseInventoryItemPeer::DATABASE_NAME);
	  if (!$dbMap->hasTable(BaseInventoryItemPeer::TABLE_NAME))
	  {
	    $dbMap->addTableObject(new InventoryItemTableMap());
	  }
	}

	/**
	 * The class that the Peer will make instances of.
	 *
	 * If $withPrefix is true, the returned path
	 * uses a dot-path notation which is tranalted into a path
	 * relative to a location on the PHP include_path.
	 * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
	 *
	 * @param      boolean  Whether or not to return the path wit hthe class name 
	 * @return     string path.to.ClassName
	 */
	public static function getOMClass($withPrefix = true)
	{
		return $withPrefix ? InventoryItemPeer::CLASS_DEFAULT : InventoryItemPeer::OM_CLASS;
	}

	/**
	 * Method perform an INSERT on the database, given a InventoryItem or Criteria object.
	 *
	 * @param      mixed $values Criteria or InventoryItem object containing data that is used to create the INSERT statement.
	 * @param      PropelPDO $con the PropelPDO connection to use
	 * @return     mixed The new primary key.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doInsert($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity
		} else {
			$criteria = $values->buildCriteria(); // build Criteria from InventoryItem object
		}


		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		try {
			// use transaction because $criteria could contain info
			// for more than one table (I guess, conceivably)
			$con->beginTransaction();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollBack();
			throw $e;
		}

		return $pk;
	}

	/**
	 * Method perform an UPDATE on the database, given a InventoryItem or Criteria object.
	 *
	 * @param      mixed $values Criteria or InventoryItem object containing data that is used to create the UPDATE statement.
	 * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doUpdate($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity

			$comparison = $criteria->getComparison(InventoryItemPeer::SKU);
			$selectCriteria->add(InventoryItemPeer::SKU, $criteria->remove(InventoryItemPeer::SKU), $comparison);

		} else { // $values is InventoryItem object
			$criteria = $values->buildCriteria(); // gets full criteria
			$selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
		}

		// set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	/**
	 * Method to DELETE all rows from the inventory_item table.
	 *
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 */
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		$affectedRows = 0; // initialize var to track total num of affected rows
		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			$affectedRows += BasePeer::doDeleteAll(InventoryItemPeer::TABLE_NAME, $con);
			// Because this db requires some delete cascade/set null emulation, we have to
			// clear the cached instance *after* the emulation has happened (since
			// instances get re-added by the select statement contained therein).
			InventoryItemPeer::clearInstancePool();
			InventoryItemPeer::clearRelatedInstancePool();
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Method perform a DELETE on the database, given a InventoryItem or Criteria object OR a primary key value.
	 *
	 * @param      mixed $values Criteria or InventoryItem object or primary key or array of primary keys
	 *              which is used to create the DELETE statement
	 * @param      PropelPDO $con the connection to use
	 * @return     int 	The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
	 *				if supported by native driver or if emulated using Propel.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	 public static function doDelete($values, PropelPDO $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			// invalidate the cache for all objects of this type, since we have no
			// way of knowing (without running a query) what objects should be invalidated
			// from the cache based on this Criteria.
			InventoryItemPeer::clearInstancePool();
			// rename for clarity
			$criteria = clone $values;
		} elseif ($values instanceof InventoryItem) { // it's a model object
			// invalidate the cache for this single object
			InventoryItemPeer::removeInstanceFromPool($values);
			// create criteria based on pk values
			$criteria = $values->buildPkeyCriteria();
		} else { // it's a primary key, or an array of pks
			$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(InventoryItemPeer::SKU, (array) $values, Criteria::IN);
			// invalidate the cache for this object(s)
			foreach ((array) $values as $singleval) {
				InventoryItemPeer::removeInstanceFromPool($singleval);
			}
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; // initialize var to track total num of affected rows

		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			InventoryItemPeer::clearRelatedInstancePool();
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Validates all modified columns of given InventoryItem object.
	 * If parameter $columns is either a single column name or an array of column names
	 * than only those columns are validated.
	 *
	 * NOTICE: This does not apply to primary or foreign keys for now.
	 *
	 * @param      InventoryItem $obj The object to validate.
	 * @param      mixed $cols Column name or array of column names.
	 *
	 * @return     mixed TRUE if all columns are valid or the error message of the first invalid column.
	 */
	public static function doValidate(InventoryItem $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(InventoryItemPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(InventoryItemPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach ($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(InventoryItemPeer::DATABASE_NAME, InventoryItemPeer::TABLE_NAME, $columns);
	}

	/**
	 * Retrieve a single object by pkey.
	 *
	 * @param      string $pk the primary key.
	 * @param      PropelPDO $con the connection to use
	 * @return     InventoryItem
	 */
	public static function retrieveByPK($pk, PropelPDO $con = null)
	{

		if (null !== ($obj = InventoryItemPeer::getInstanceFromPool((string) $pk))) {
			return $obj;
		}

		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
		$criteria->add(InventoryItemPeer::SKU, $pk);

		$v = InventoryItemPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	/**
	 * Retrieve multiple objects by pkey.
	 *
	 * @param      array $pks List of primary keys
	 * @param      PropelPDO $con the connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function retrieveByPKs($pks, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria(InventoryItemPeer::DATABASE_NAME);
			$criteria->add(InventoryItemPeer::SKU, $pks, Criteria::IN);
			$objs = InventoryItemPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

	// symfony behavior
	
	/**
	 * Returns an array of arrays that contain columns in each unique index.
	 *
	 * @return array
	 */
	static public function getUniqueColumnNames()
	{
	  return array(array('sku'));
	}

} // BaseInventoryItemPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseInventoryItemPeer::buildTableMap();

