<?php

/**
 * Base class that represents a row from the 'ordered_item' table.
 *
 * 
 *
 * @package    lib.model.om
 */
abstract class BaseOrderedItem extends BaseObject  implements Persistent {


	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        OrderedItemPeer
	 */
	protected static $peer;

	/**
	 * The value for the id field.
	 * @var        int
	 */
	protected $id;

	/**
	 * The value for the order_id field.
	 * @var        int
	 */
	protected $order_id;

	/**
	 * The value for the inventory_item_id field.
	 * @var        int
	 */
	protected $inventory_item_id;

	/**
	 * The value for the sku field.
	 * @var        string
	 */
	protected $sku;

	/**
	 * The value for the qty field.
	 * @var        int
	 */
	protected $qty;

	/**
	 * The value for the unit_price field.
	 * @var        double
	 */
	protected $unit_price;

	/**
	 * The value for the title field.
	 * @var        string
	 */
	protected $title;

	/**
	 * The value for the size field.
	 * @var        string
	 */
	protected $size;

	/**
	 * The value for the color field.
	 * @var        string
	 */
	protected $color;

	/**
	 * The value for the options field.
	 * @var        string
	 */
	protected $options;

	/**
	 * The value for the finish field.
	 * @var        string
	 */
	protected $finish;

	/**
	 * The value for the hand field.
	 * @var        string
	 */
	protected $hand;

	/**
	 * The value for the gender field.
	 * @var        string
	 */
	protected $gender;

	/**
	 * The value for the measure_id field.
	 * @var        int
	 */
	protected $measure_id;

	/**
	 * The value for the ship_weight field.
	 * @var        string
	 */
	protected $ship_weight;

	/**
	 * The value for the backorder field.
	 * Note: this column has a database default value of: false
	 * @var        boolean
	 */
	protected $backorder;

	/**
	 * The value for the created_at field.
	 * @var        string
	 */
	protected $created_at;

	/**
	 * The value for the updated_at field.
	 * @var        string
	 */
	protected $updated_at;

	/**
	 * @var        Ordered
	 */
	protected $aOrdered;

	/**
	 * @var        Measures
	 */
	protected $aMeasures;

	/**
	 * @var        array OrderedItemOptionsItems[] Collection to store aggregation of OrderedItemOptionsItems objects.
	 */
	protected $collOrderedItemOptionsItemss;

	/**
	 * @var        Criteria The criteria used to select the current contents of collOrderedItemOptionsItemss.
	 */
	private $lastOrderedItemOptionsItemsCriteria = null;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	// symfony behavior
	
	const PEER = 'OrderedItemPeer';

	/**
	 * Applies default values to this object.
	 * This method should be called from the object's constructor (or
	 * equivalent initialization method).
	 * @see        __construct()
	 */
	public function applyDefaultValues()
	{
		$this->backorder = false;
	}

	/**
	 * Initializes internal state of BaseOrderedItem object.
	 * @see        applyDefaults()
	 */
	public function __construct()
	{
		parent::__construct();
		$this->applyDefaultValues();
	}

	/**
	 * Get the [id] column value.
	 * 
	 * @return     int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get the [order_id] column value.
	 * 
	 * @return     int
	 */
	public function getOrderId()
	{
		return $this->order_id;
	}

	/**
	 * Get the [inventory_item_id] column value.
	 * 
	 * @return     int
	 */
	public function getInventoryItemId()
	{
		return $this->inventory_item_id;
	}

	/**
	 * Get the [sku] column value.
	 * 
	 * @return     string
	 */
	public function getSku()
	{
		return $this->sku;
	}

	/**
	 * Get the [qty] column value.
	 * 
	 * @return     int
	 */
	public function getQty()
	{
		return $this->qty;
	}

	/**
	 * Get the [unit_price] column value.
	 * 
	 * @return     double
	 */
	public function getUnitPrice()
	{
		return $this->unit_price;
	}

	/**
	 * Get the [title] column value.
	 * 
	 * @return     string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * Get the [size] column value.
	 * 
	 * @return     string
	 */
	public function getSize()
	{
		return $this->size;
	}

	/**
	 * Get the [color] column value.
	 * 
	 * @return     string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * Get the [options] column value.
	 * 
	 * @return     string
	 */
	public function getOptions()
	{
		return $this->options;
	}

	/**
	 * Get the [finish] column value.
	 * 
	 * @return     string
	 */
	public function getFinish()
	{
		return $this->finish;
	}

	/**
	 * Get the [hand] column value.
	 * 
	 * @return     string
	 */
	public function getHand()
	{
		return $this->hand;
	}

	/**
	 * Get the [gender] column value.
	 * 
	 * @return     string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Get the [measure_id] column value.
	 * 
	 * @return     int
	 */
	public function getMeasureId()
	{
		return $this->measure_id;
	}

	/**
	 * Get the [ship_weight] column value.
	 * 
	 * @return     string
	 */
	public function getShipWeight()
	{
		return $this->ship_weight;
	}

	/**
	 * Get the [backorder] column value.
	 * 
	 * @return     boolean
	 */
	public function getBackorder()
	{
		return $this->backorder;
	}

	/**
	 * Get the [optionally formatted] temporal [created_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getCreatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->created_at === null) {
			return null;
		}


		if ($this->created_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->created_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Get the [optionally formatted] temporal [updated_at] column value.
	 * 
	 *
	 * @param      string $format The date/time format string (either date()-style or strftime()-style).
	 *							If format is NULL, then the raw DateTime object will be returned.
	 * @return     mixed Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
	 * @throws     PropelException - if unable to parse/validate the date/time value.
	 */
	public function getUpdatedAt($format = 'Y-m-d H:i:s')
	{
		if ($this->updated_at === null) {
			return null;
		}


		if ($this->updated_at === '0000-00-00 00:00:00') {
			// while technically this is not a default value of NULL,
			// this seems to be closest in meaning.
			return null;
		} else {
			try {
				$dt = new DateTime($this->updated_at);
			} catch (Exception $x) {
				throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
			}
		}

		if ($format === null) {
			// Because propel.useDateTimeClass is TRUE, we return a DateTime object.
			return $dt;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $dt->format('U'));
		} else {
			return $dt->format($format);
		}
	}

	/**
	 * Set the value of [id] column.
	 * 
	 * @param      int $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = OrderedItemPeer::ID;
		}

		return $this;
	} // setId()

	/**
	 * Set the value of [order_id] column.
	 * 
	 * @param      int $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setOrderId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->order_id !== $v) {
			$this->order_id = $v;
			$this->modifiedColumns[] = OrderedItemPeer::ORDER_ID;
		}

		if ($this->aOrdered !== null && $this->aOrdered->getId() !== $v) {
			$this->aOrdered = null;
		}

		return $this;
	} // setOrderId()

	/**
	 * Set the value of [inventory_item_id] column.
	 * 
	 * @param      int $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setInventoryItemId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->inventory_item_id !== $v) {
			$this->inventory_item_id = $v;
			$this->modifiedColumns[] = OrderedItemPeer::INVENTORY_ITEM_ID;
		}

		return $this;
	} // setInventoryItemId()

	/**
	 * Set the value of [sku] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setSku($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->sku !== $v) {
			$this->sku = $v;
			$this->modifiedColumns[] = OrderedItemPeer::SKU;
		}

		return $this;
	} // setSku()

	/**
	 * Set the value of [qty] column.
	 * 
	 * @param      int $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setQty($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->qty !== $v) {
			$this->qty = $v;
			$this->modifiedColumns[] = OrderedItemPeer::QTY;
		}

		return $this;
	} // setQty()

	/**
	 * Set the value of [unit_price] column.
	 * 
	 * @param      double $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setUnitPrice($v)
	{
		if ($v !== null) {
			$v = (double) $v;
		}

		if ($this->unit_price !== $v) {
			$this->unit_price = $v;
			$this->modifiedColumns[] = OrderedItemPeer::UNIT_PRICE;
		}

		return $this;
	} // setUnitPrice()

	/**
	 * Set the value of [title] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setTitle($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->title !== $v) {
			$this->title = $v;
			$this->modifiedColumns[] = OrderedItemPeer::TITLE;
		}

		return $this;
	} // setTitle()

	/**
	 * Set the value of [size] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setSize($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->size !== $v) {
			$this->size = $v;
			$this->modifiedColumns[] = OrderedItemPeer::SIZE;
		}

		return $this;
	} // setSize()

	/**
	 * Set the value of [color] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setColor($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->color !== $v) {
			$this->color = $v;
			$this->modifiedColumns[] = OrderedItemPeer::COLOR;
		}

		return $this;
	} // setColor()

	/**
	 * Set the value of [options] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setOptions($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->options !== $v) {
			$this->options = $v;
			$this->modifiedColumns[] = OrderedItemPeer::OPTIONS;
		}

		return $this;
	} // setOptions()

	/**
	 * Set the value of [finish] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setFinish($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->finish !== $v) {
			$this->finish = $v;
			$this->modifiedColumns[] = OrderedItemPeer::FINISH;
		}

		return $this;
	} // setFinish()

	/**
	 * Set the value of [hand] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setHand($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->hand !== $v) {
			$this->hand = $v;
			$this->modifiedColumns[] = OrderedItemPeer::HAND;
		}

		return $this;
	} // setHand()

	/**
	 * Set the value of [gender] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setGender($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->gender !== $v) {
			$this->gender = $v;
			$this->modifiedColumns[] = OrderedItemPeer::GENDER;
		}

		return $this;
	} // setGender()

	/**
	 * Set the value of [measure_id] column.
	 * 
	 * @param      int $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setMeasureId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->measure_id !== $v) {
			$this->measure_id = $v;
			$this->modifiedColumns[] = OrderedItemPeer::MEASURE_ID;
		}

		if ($this->aMeasures !== null && $this->aMeasures->getId() !== $v) {
			$this->aMeasures = null;
		}

		return $this;
	} // setMeasureId()

	/**
	 * Set the value of [ship_weight] column.
	 * 
	 * @param      string $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setShipWeight($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->ship_weight !== $v) {
			$this->ship_weight = $v;
			$this->modifiedColumns[] = OrderedItemPeer::SHIP_WEIGHT;
		}

		return $this;
	} // setShipWeight()

	/**
	 * Set the value of [backorder] column.
	 * 
	 * @param      boolean $v new value
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setBackorder($v)
	{
		if ($v !== null) {
			$v = (boolean) $v;
		}

		if ($this->backorder !== $v || $this->isNew()) {
			$this->backorder = $v;
			$this->modifiedColumns[] = OrderedItemPeer::BACKORDER;
		}

		return $this;
	} // setBackorder()

	/**
	 * Sets the value of [created_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setCreatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->created_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->created_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = OrderedItemPeer::CREATED_AT;
			}
		} // if either are not null

		return $this;
	} // setCreatedAt()

	/**
	 * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
	 * 
	 * @param      mixed $v string, integer (timestamp), or DateTime value.  Empty string will
	 *						be treated as NULL for temporal objects.
	 * @return     OrderedItem The current object (for fluent API support)
	 */
	public function setUpdatedAt($v)
	{
		// we treat '' as NULL for temporal objects because DateTime('') == DateTime('now')
		// -- which is unexpected, to say the least.
		if ($v === null || $v === '') {
			$dt = null;
		} elseif ($v instanceof DateTime) {
			$dt = $v;
		} else {
			// some string/numeric value passed; we normalize that so that we can
			// validate it.
			try {
				if (is_numeric($v)) { // if it's a unix timestamp
					$dt = new DateTime('@'.$v, new DateTimeZone('UTC'));
					// We have to explicitly specify and then change the time zone because of a
					// DateTime bug: http://bugs.php.net/bug.php?id=43003
					$dt->setTimeZone(new DateTimeZone(date_default_timezone_get()));
				} else {
					$dt = new DateTime($v);
				}
			} catch (Exception $x) {
				throw new PropelException('Error parsing date/time value: ' . var_export($v, true), $x);
			}
		}

		if ( $this->updated_at !== null || $dt !== null ) {
			// (nested ifs are a little easier to read in this case)

			$currNorm = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
			$newNorm = ($dt !== null) ? $dt->format('Y-m-d H:i:s') : null;

			if ( ($currNorm !== $newNorm) // normalized values don't match 
					)
			{
				$this->updated_at = ($dt ? $dt->format('Y-m-d H:i:s') : null);
				$this->modifiedColumns[] = OrderedItemPeer::UPDATED_AT;
			}
		} // if either are not null

		return $this;
	} // setUpdatedAt()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
			if ($this->backorder !== false) {
				return false;
			}

		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->order_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->inventory_item_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->sku = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->qty = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
			$this->unit_price = ($row[$startcol + 5] !== null) ? (double) $row[$startcol + 5] : null;
			$this->title = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->size = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->color = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->options = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
			$this->finish = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
			$this->hand = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
			$this->gender = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
			$this->measure_id = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
			$this->ship_weight = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
			$this->backorder = ($row[$startcol + 15] !== null) ? (boolean) $row[$startcol + 15] : null;
			$this->created_at = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
			$this->updated_at = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			// FIXME - using NUM_COLUMNS may be clearer.
			return $startcol + 18; // 18 = OrderedItemPeer::NUM_COLUMNS - OrderedItemPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating OrderedItem object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aOrdered !== null && $this->order_id !== $this->aOrdered->getId()) {
			$this->aOrdered = null;
		}
		if ($this->aMeasures !== null && $this->measure_id !== $this->aMeasures->getId()) {
			$this->aMeasures = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedItemPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = OrderedItemPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aOrdered = null;
			$this->aMeasures = null;
			$this->collOrderedItemOptionsItemss = null;
			$this->lastOrderedItemOptionsItemsCriteria = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				OrderedItemPeer::doDelete($this, $con);
				$this->postDelete($con);
				$this->setDeleted(true);
				$con->commit();
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(OrderedItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		
		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			// symfony_timestampable behavior
			if ($this->isModified() && !$this->isColumnModified(OrderedItemPeer::UPDATED_AT))
			{
			  $this->setUpdatedAt(time());
			}

			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
				// symfony_timestampable behavior
				if (!$this->isColumnModified(OrderedItemPeer::CREATED_AT))
				{
				  $this->setCreatedAt(time());
				}

			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				OrderedItemPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aOrdered !== null) {
				if ($this->aOrdered->isModified() || $this->aOrdered->isNew()) {
					$affectedRows += $this->aOrdered->save($con);
				}
				$this->setOrdered($this->aOrdered);
			}

			if ($this->aMeasures !== null) {
				if ($this->aMeasures->isModified() || $this->aMeasures->isNew()) {
					$affectedRows += $this->aMeasures->save($con);
				}
				$this->setMeasures($this->aMeasures);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = OrderedItemPeer::ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = OrderedItemPeer::doInsert($this, $con);
					$affectedRows += 1; // we are assuming that there is only 1 row per doInsert() which
										 // should always be true here (even though technically
										 // BasePeer::doInsert() can insert multiple rows).

					$this->setId($pk);  //[IMV] update autoincrement primary key

					$this->setNew(false);
				} else {
					$affectedRows += OrderedItemPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collOrderedItemOptionsItemss !== null) {
				foreach ($this->collOrderedItemOptionsItemss as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aOrdered !== null) {
				if (!$this->aOrdered->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aOrdered->getValidationFailures());
				}
			}

			if ($this->aMeasures !== null) {
				if (!$this->aMeasures->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMeasures->getValidationFailures());
				}
			}


			if (($retval = OrderedItemPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collOrderedItemOptionsItemss !== null) {
					foreach ($this->collOrderedItemOptionsItemss as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrderedItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getId();
				break;
			case 1:
				return $this->getOrderId();
				break;
			case 2:
				return $this->getInventoryItemId();
				break;
			case 3:
				return $this->getSku();
				break;
			case 4:
				return $this->getQty();
				break;
			case 5:
				return $this->getUnitPrice();
				break;
			case 6:
				return $this->getTitle();
				break;
			case 7:
				return $this->getSize();
				break;
			case 8:
				return $this->getColor();
				break;
			case 9:
				return $this->getOptions();
				break;
			case 10:
				return $this->getFinish();
				break;
			case 11:
				return $this->getHand();
				break;
			case 12:
				return $this->getGender();
				break;
			case 13:
				return $this->getMeasureId();
				break;
			case 14:
				return $this->getShipWeight();
				break;
			case 15:
				return $this->getBackorder();
				break;
			case 16:
				return $this->getCreatedAt();
				break;
			case 17:
				return $this->getUpdatedAt();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param      string $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                        BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. Defaults to BasePeer::TYPE_PHPNAME.
	 * @param      boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns.  Defaults to TRUE.
	 * @return     an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true)
	{
		$keys = OrderedItemPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getId(),
			$keys[1] => $this->getOrderId(),
			$keys[2] => $this->getInventoryItemId(),
			$keys[3] => $this->getSku(),
			$keys[4] => $this->getQty(),
			$keys[5] => $this->getUnitPrice(),
			$keys[6] => $this->getTitle(),
			$keys[7] => $this->getSize(),
			$keys[8] => $this->getColor(),
			$keys[9] => $this->getOptions(),
			$keys[10] => $this->getFinish(),
			$keys[11] => $this->getHand(),
			$keys[12] => $this->getGender(),
			$keys[13] => $this->getMeasureId(),
			$keys[14] => $this->getShipWeight(),
			$keys[15] => $this->getBackorder(),
			$keys[16] => $this->getCreatedAt(),
			$keys[17] => $this->getUpdatedAt(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = OrderedItemPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setId($value);
				break;
			case 1:
				$this->setOrderId($value);
				break;
			case 2:
				$this->setInventoryItemId($value);
				break;
			case 3:
				$this->setSku($value);
				break;
			case 4:
				$this->setQty($value);
				break;
			case 5:
				$this->setUnitPrice($value);
				break;
			case 6:
				$this->setTitle($value);
				break;
			case 7:
				$this->setSize($value);
				break;
			case 8:
				$this->setColor($value);
				break;
			case 9:
				$this->setOptions($value);
				break;
			case 10:
				$this->setFinish($value);
				break;
			case 11:
				$this->setHand($value);
				break;
			case 12:
				$this->setGender($value);
				break;
			case 13:
				$this->setMeasureId($value);
				break;
			case 14:
				$this->setShipWeight($value);
				break;
			case 15:
				$this->setBackorder($value);
				break;
			case 16:
				$this->setCreatedAt($value);
				break;
			case 17:
				$this->setUpdatedAt($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = OrderedItemPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setOrderId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setInventoryItemId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setSku($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setQty($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setUnitPrice($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTitle($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setSize($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setColor($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setOptions($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setFinish($arr[$keys[10]]);
		if (array_key_exists($keys[11], $arr)) $this->setHand($arr[$keys[11]]);
		if (array_key_exists($keys[12], $arr)) $this->setGender($arr[$keys[12]]);
		if (array_key_exists($keys[13], $arr)) $this->setMeasureId($arr[$keys[13]]);
		if (array_key_exists($keys[14], $arr)) $this->setShipWeight($arr[$keys[14]]);
		if (array_key_exists($keys[15], $arr)) $this->setBackorder($arr[$keys[15]]);
		if (array_key_exists($keys[16], $arr)) $this->setCreatedAt($arr[$keys[16]]);
		if (array_key_exists($keys[17], $arr)) $this->setUpdatedAt($arr[$keys[17]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);

		if ($this->isColumnModified(OrderedItemPeer::ID)) $criteria->add(OrderedItemPeer::ID, $this->id);
		if ($this->isColumnModified(OrderedItemPeer::ORDER_ID)) $criteria->add(OrderedItemPeer::ORDER_ID, $this->order_id);
		if ($this->isColumnModified(OrderedItemPeer::INVENTORY_ITEM_ID)) $criteria->add(OrderedItemPeer::INVENTORY_ITEM_ID, $this->inventory_item_id);
		if ($this->isColumnModified(OrderedItemPeer::SKU)) $criteria->add(OrderedItemPeer::SKU, $this->sku);
		if ($this->isColumnModified(OrderedItemPeer::QTY)) $criteria->add(OrderedItemPeer::QTY, $this->qty);
		if ($this->isColumnModified(OrderedItemPeer::UNIT_PRICE)) $criteria->add(OrderedItemPeer::UNIT_PRICE, $this->unit_price);
		if ($this->isColumnModified(OrderedItemPeer::TITLE)) $criteria->add(OrderedItemPeer::TITLE, $this->title);
		if ($this->isColumnModified(OrderedItemPeer::SIZE)) $criteria->add(OrderedItemPeer::SIZE, $this->size);
		if ($this->isColumnModified(OrderedItemPeer::COLOR)) $criteria->add(OrderedItemPeer::COLOR, $this->color);
		if ($this->isColumnModified(OrderedItemPeer::OPTIONS)) $criteria->add(OrderedItemPeer::OPTIONS, $this->options);
		if ($this->isColumnModified(OrderedItemPeer::FINISH)) $criteria->add(OrderedItemPeer::FINISH, $this->finish);
		if ($this->isColumnModified(OrderedItemPeer::HAND)) $criteria->add(OrderedItemPeer::HAND, $this->hand);
		if ($this->isColumnModified(OrderedItemPeer::GENDER)) $criteria->add(OrderedItemPeer::GENDER, $this->gender);
		if ($this->isColumnModified(OrderedItemPeer::MEASURE_ID)) $criteria->add(OrderedItemPeer::MEASURE_ID, $this->measure_id);
		if ($this->isColumnModified(OrderedItemPeer::SHIP_WEIGHT)) $criteria->add(OrderedItemPeer::SHIP_WEIGHT, $this->ship_weight);
		if ($this->isColumnModified(OrderedItemPeer::BACKORDER)) $criteria->add(OrderedItemPeer::BACKORDER, $this->backorder);
		if ($this->isColumnModified(OrderedItemPeer::CREATED_AT)) $criteria->add(OrderedItemPeer::CREATED_AT, $this->created_at);
		if ($this->isColumnModified(OrderedItemPeer::UPDATED_AT)) $criteria->add(OrderedItemPeer::UPDATED_AT, $this->updated_at);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);

		$criteria->add(OrderedItemPeer::ID, $this->id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	/**
	 * Generic method to set the primary key (id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of OrderedItem (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setOrderId($this->order_id);

		$copyObj->setInventoryItemId($this->inventory_item_id);

		$copyObj->setSku($this->sku);

		$copyObj->setQty($this->qty);

		$copyObj->setUnitPrice($this->unit_price);

		$copyObj->setTitle($this->title);

		$copyObj->setSize($this->size);

		$copyObj->setColor($this->color);

		$copyObj->setOptions($this->options);

		$copyObj->setFinish($this->finish);

		$copyObj->setHand($this->hand);

		$copyObj->setGender($this->gender);

		$copyObj->setMeasureId($this->measure_id);

		$copyObj->setShipWeight($this->ship_weight);

		$copyObj->setBackorder($this->backorder);

		$copyObj->setCreatedAt($this->created_at);

		$copyObj->setUpdatedAt($this->updated_at);


		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getOrderedItemOptionsItemss() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addOrderedItemOptionsItems($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);

		$copyObj->setId(NULL); // this is a auto-increment column, so set to default value

	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     OrderedItem Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     OrderedItemPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new OrderedItemPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Ordered object.
	 *
	 * @param      Ordered $v
	 * @return     OrderedItem The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setOrdered(Ordered $v = null)
	{
		if ($v === null) {
			$this->setOrderId(NULL);
		} else {
			$this->setOrderId($v->getId());
		}

		$this->aOrdered = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Ordered object, it will not be re-added.
		if ($v !== null) {
			$v->addOrderedItem($this);
		}

		return $this;
	}


	/**
	 * Get the associated Ordered object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Ordered The associated Ordered object.
	 * @throws     PropelException
	 */
	public function getOrdered(PropelPDO $con = null)
	{
		if ($this->aOrdered === null && ($this->order_id !== null)) {
			$this->aOrdered = OrderedPeer::retrieveByPk($this->order_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aOrdered->addOrderedItems($this);
			 */
		}
		return $this->aOrdered;
	}

	/**
	 * Declares an association between this object and a Measures object.
	 *
	 * @param      Measures $v
	 * @return     OrderedItem The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setMeasures(Measures $v = null)
	{
		if ($v === null) {
			$this->setMeasureId(NULL);
		} else {
			$this->setMeasureId($v->getId());
		}

		$this->aMeasures = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Measures object, it will not be re-added.
		if ($v !== null) {
			$v->addOrderedItem($this);
		}

		return $this;
	}


	/**
	 * Get the associated Measures object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Measures The associated Measures object.
	 * @throws     PropelException
	 */
	public function getMeasures(PropelPDO $con = null)
	{
		if ($this->aMeasures === null && ($this->measure_id !== null)) {
			$this->aMeasures = MeasuresPeer::retrieveByPk($this->measure_id);
			/* The following can be used additionally to
			   guarantee the related object contains a reference
			   to this object.  This level of coupling may, however, be
			   undesirable since it could result in an only partially populated collection
			   in the referenced object.
			   $this->aMeasures->addOrderedItems($this);
			 */
		}
		return $this->aMeasures;
	}

	/**
	 * Clears out the collOrderedItemOptionsItemss collection (array).
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addOrderedItemOptionsItemss()
	 */
	public function clearOrderedItemOptionsItemss()
	{
		$this->collOrderedItemOptionsItemss = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collOrderedItemOptionsItemss collection (array).
	 *
	 * By default this just sets the collOrderedItemOptionsItemss collection to an empty array (like clearcollOrderedItemOptionsItemss());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initOrderedItemOptionsItemss()
	{
		$this->collOrderedItemOptionsItemss = array();
	}

	/**
	 * Gets an array of OrderedItemOptionsItems objects which contain a foreign key that references this object.
	 *
	 * If this collection has already been initialized with an identical Criteria, it returns the collection.
	 * Otherwise if this OrderedItem has previously been saved, it will retrieve
	 * related OrderedItemOptionsItemss from storage. If this OrderedItem is new, it will return
	 * an empty collection or the current collection, the criteria is ignored on a new object.
	 *
	 * @param      PropelPDO $con
	 * @param      Criteria $criteria
	 * @return     array OrderedItemOptionsItems[]
	 * @throws     PropelException
	 */
	public function getOrderedItemOptionsItemss($criteria = null, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
			   $this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				OrderedItemOptionsItemsPeer::addSelectColumns($criteria);
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelect($criteria, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return the collection.


				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				OrderedItemOptionsItemsPeer::addSelectColumns($criteria);
				if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
					$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;
		return $this->collOrderedItemOptionsItemss;
	}

	/**
	 * Returns the number of related OrderedItemOptionsItems objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related OrderedItemOptionsItems objects.
	 * @throws     PropelException
	 */
	public function countOrderedItemOptionsItemss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);
		} else {
			$criteria = clone $criteria;
		}

		if ($distinct) {
			$criteria->setDistinct();
		}

		$count = null;

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$count = 0;
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				$count = OrderedItemOptionsItemsPeer::doCount($criteria, false, $con);
			}
		} else {
			// criteria has no effect for a new object
			if (!$this->isNew()) {
				// the following code is to determine if a new query is
				// called for.  If the criteria is the same as the last
				// one, just return count of the collection.


				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
					$count = OrderedItemOptionsItemsPeer::doCount($criteria, false, $con);
				} else {
					$count = count($this->collOrderedItemOptionsItemss);
				}
			} else {
				$count = count($this->collOrderedItemOptionsItemss);
			}
		}
		return $count;
	}

	/**
	 * Method called to associate a OrderedItemOptionsItems object to this object
	 * through the OrderedItemOptionsItems foreign key attribute.
	 *
	 * @param      OrderedItemOptionsItems $l OrderedItemOptionsItems
	 * @return     void
	 * @throws     PropelException
	 */
	public function addOrderedItemOptionsItems(OrderedItemOptionsItems $l)
	{
		if ($this->collOrderedItemOptionsItemss === null) {
			$this->initOrderedItemOptionsItemss();
		}
		if (!in_array($l, $this->collOrderedItemOptionsItemss, true)) { // only add it if the **same** object is not already associated
			array_push($this->collOrderedItemOptionsItemss, $l);
			$l->setOrderedItem($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this OrderedItem is new, it will return
	 * an empty collection; or if this OrderedItem has previously
	 * been saved, it will retrieve related OrderedItemOptionsItemss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in OrderedItem.
	 */
	public function getOrderedItemOptionsItemssJoinInventoryItem($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinInventoryItem($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

			if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinInventoryItem($criteria, $con, $join_behavior);
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;

		return $this->collOrderedItemOptionsItemss;
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this OrderedItem is new, it will return
	 * an empty collection; or if this OrderedItem has previously
	 * been saved, it will retrieve related OrderedItemOptionsItemss from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in OrderedItem.
	 */
	public function getOrderedItemOptionsItemssJoinOptions($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		if ($criteria === null) {
			$criteria = new Criteria(OrderedItemPeer::DATABASE_NAME);
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collOrderedItemOptionsItemss === null) {
			if ($this->isNew()) {
				$this->collOrderedItemOptionsItemss = array();
			} else {

				$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		} else {
			// the following code is to determine if a new query is
			// called for.  If the criteria is the same as the last
			// one, just return the collection.

			$criteria->add(OrderedItemOptionsItemsPeer::ORDERED_ITEM_ID, $this->id);

			if (!isset($this->lastOrderedItemOptionsItemsCriteria) || !$this->lastOrderedItemOptionsItemsCriteria->equals($criteria)) {
				$this->collOrderedItemOptionsItemss = OrderedItemOptionsItemsPeer::doSelectJoinOptions($criteria, $con, $join_behavior);
			}
		}
		$this->lastOrderedItemOptionsItemsCriteria = $criteria;

		return $this->collOrderedItemOptionsItemss;
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collOrderedItemOptionsItemss) {
				foreach ((array) $this->collOrderedItemOptionsItemss as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collOrderedItemOptionsItemss = null;
			$this->aOrdered = null;
			$this->aMeasures = null;
	}

} // BaseOrderedItem
