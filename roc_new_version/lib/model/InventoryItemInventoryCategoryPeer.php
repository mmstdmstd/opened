<?php

require 'lib/model/om/BaseInventoryItemInventoryCategoryPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'inventory_item_inventory_category' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class InventoryItemInventoryCategoryPeer extends BaseInventoryItemInventoryCategoryPeer {

	public static function getInventoryItemInventoryCategorysByItemInventoryId( $page=1, $ReturnPager=true, $inventory_id, $Sorting='PRODUCT_LINE', $select_first_element= false ) {
		$c = new Criteria();

		//Util::deb($Sorting, '$Sorting::');
		$c->addJoin(InventoryItemInventoryCategoryPeer::INVENTORY_CATEGORY_ID, InventoryCategoryPeer::ID, Criteria::LEFT_JOIN);
		$c->add( InventoryItemInventoryCategoryPeer::INVENTORY_ITEM_ID, $inventory_id);

		$c->addAscendingOrderByColumn( InventoryCategoryPeer::PRODUCT_LINE );

		//$c->addAscendingOrderByColumn( InventoryCategoryPeer::ORDERING );

		if ( !$ReturnPager ) {
			if( $select_first_element ) {
			  return InventoryItemInventoryCategoryPeer::doSelectOne( $c );
				//Util::deb($A, '$A::');
				//if ( !empty($A[0]) ) return $A[0];
			} else {
				return InventoryItemInventoryCategoryPeer::doSelect( $c );
			}
		}
		$pager = new sfPropelPager( 'InventoryItemPaperwork', (int)sfConfig::get('app_application_rows_in_pager' ) );
		$pager->setPage( $page );
		$pager->setCriteria($c);
		$pager->init();
		return $pager;
	}

} // InventoryItemInventoryCategoryPeer
