<?php

require 'lib/model/om/BaseOrdered.php';


/**
 * Skeleton subclass for representing a row from the 'ordered' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class Ordered extends BaseOrdered {

	/**
	 * Initializes internal state of Ordered object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}


	public function getStatusAsText() {
	  if ( !empty(OrderedPeer::$StatusChoices  [ $this->getStatus() ]) ) {
	  	return OrderedPeer::$StatusChoices[ $this->getStatus() ];
	  }
	  return $this->getStatus();
	}

 	public function getPaymentMethodAsText() {
    if ( !empty(OrderedPeer::$PaymentMethodChoices  [ $this->getPaymentMethod() ]) ) {
	  	return OrderedPeer::$PaymentMethodChoices[ $this->getPaymentMethod() ];
	  }
	  return $this->getPaymentMethod();
	}

} // Ordered
