<?php

require 'lib/model/om/BaseUserShippingInfo.php';


/**
 * Skeleton subclass for representing a row from the 'user_shipping_info' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class UserShippingInfo extends BaseUserShippingInfo {

} // UserShippingInfo
