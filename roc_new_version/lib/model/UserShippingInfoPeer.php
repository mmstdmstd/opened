<?php

require 'lib/model/om/BaseUserShippingInfoPeer.php';


/**
 * Skeleton subclass for performing query and update operations on the 'user_shipping_info' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class UserShippingInfoPeer extends BaseUserShippingInfoPeer {

  public static function getUserShippingInfoByUserId( $UserId= '' ) {
    $c = new Criteria();
   	$c->add( UserShippingInfoPeer::USER_ID, $UserId );
    return UserShippingInfoPeer::doSelectOne( $c );
  }

} // UserShippingInfoPeer
