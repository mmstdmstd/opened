<?php 
class ImportProductLines {
	private $ItemsAdded=0;
	private $ExisingItemsUpdated=0;	
	private $ItemsDeletedAsNoneExising=0;
	private $RelatedInvetoryItemsDeleted=0;
  private $SkippedAsNotModified= 0;	
	
	private $InventoryCategoriesProductLineArray= array();

	
	private $OriginalExcelFileName;
	private $ExcelFileName;
	private $ColumnsArray;

	private function getColumnName($pColumnNumber) {
		foreach( $this->ColumnsArray as $ColumnNumber=>$ColumnName ) {
			if ( (int)$pColumnNumber== (int)$ColumnNumber ) return $ColumnName;
		}
	}

	public function setFileName($pExcelFileName) {
		$this->ExcelFileName= $pExcelFileName;
	}

	public function setOriginalFileName($pOriginalExcelFileName) {
		$this->OriginalExcelFileName= $pOriginalExcelFileName;
	}

	/*
  private function FillSizeOrder( $ExcellTabData ) {

		$AddedCount= 0;
		for($I= 3; $I<= count($ExcellTabData); $I++) {
			if( empty($ExcellTabData[$I] )) continue;
			$SizeValue= $ExcellTabData[$I];
			if( empty( $SizeValue[1] ) ) continue;
			$lNewSizeOrder= new SizeOrder();
			$lNewSizeOrder->setSize( $SizeValue[1] );
			$lNewSizeOrder->save();
			$AddedCount++;
		}
		Util::deb($AddedCount, '$AddedCount::');
  } */

	public function Run() {
		$this->ExisingItemsUpdated= 0;
		$this->ItemsAdded= 0;
  	$this->ItemsDeletedAsNoneExising= 0;
	  $this->RelatedInvetoryItemsDeleted= 0;
	  $this->SkippedAsNotModified= 0;
		
		$ExcellArray = new sfExcelReader($this->ExcelFileName);
		// Util::deb($this->ExcelTabNumber,'$this->ExcelTabNumber::');
		// Util::deb($ExcellArray->sheets,'$ExcellArray->sheets::');
		$ExcellTabData= $ExcellArray->sheets[0]['cells'];
		$this->ColumnsArray= $ExcellTabData[2];

		// Util::deb($this->ColumnsArray,'$this->ColumnsArray::');
		/* Util::deb($ExcellTabData,'$ExcellTabData::');
		$this->FillSizeOrder( $ExcellTabData );

		die("Run sizes-1.xls ");  */
		for($I= 3; $I<= count($ExcellTabData)+250; $I++) {
			if( empty($ExcellTabData[$I] )) continue;
			$DataArray= $ExcellTabData[$I];
			 //Util::deb($DataArray,'$DataArray::');
			unset($lInventoryCategory);
			$ProductLine= '';
			$Description= '';
			$Category= '';
			$Subcategory= '';

  		$this->InventoryCategoriesProductLineArray[]= trim( $DataArray[1] );
			foreach( $DataArray as $ColumnNumber=>$ColumnValue ) { // all columns in dict				
				$ColumnName= trim( $this->getColumnName($ColumnNumber) );
				$ColumnValue= trim($ColumnValue);
				if ( strtolower($ColumnName) == strtolower('Product Line') ) {
					$ProductLine= $ColumnValue;
				}
				if ( strtolower($ColumnName) == strtolower('Description') ) {
					$Description= $ColumnValue;
				}
				if ( strtolower($ColumnName) == strtolower('Category') ) {
					$Category= $ColumnValue;
				}
				if ( strtolower($ColumnName) == strtolower('Subcategory') ) {
					$Subcategory= $ColumnValue;
				}
			} // foreach( $datasArray as $ColumnNumber=>$ColumnValue ) { // all columns in dict

			if ( empty($ProductLine) or empty($Category) or empty($Subcategory) ) continue;
			// Util::deb( $ProductLine, ' $ProductLine::' );
			// Util::deb( $Category, ' $Category::' );
			// Util::deb( $Subcategory, ' $Subcategory::' );
			$lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $ProductLine, $Category, $Subcategory );
			 /*Util::deb($lInventoryCategory,'$lInventoryCategory::');
			 die("DIE"); */
			 
			$HasToModify= false;
			if( empty($lInventoryCategory) ) {
  			unset($lInventoryCategory);
  			$lInventoryCategory= new InventoryCategory();			
  			$this->ItemsAdded++;
  			$HasToModify= true;
			}
			
			if ( !$HasToModify ) {
  			if( $lInventoryCategory->getProductLine()!= $ProductLine   or   
	  		$lInventoryCategory->getDescription() != $Description   or	
		  	$lInventoryCategory->getCategory() != $Category   or 
			  $lInventoryCategory->getSubcategory() != $Subcategory ) {
				  $HasToModify= true;
				  $this->ExisingItemsUpdated++;
				  //Util::deb( $lInventoryCategory->getProductLine(), ' UPDATING $lInventoryCategory->getProductLine()::' );
			  }
			}

			if ( !$HasToModify ) {
				$this->SkippedAsNotModified++;				
			}
			if ( $HasToModify ) {
			  try{
    			$lInventoryCategory->setProductLine($ProductLine);
		    	$lInventoryCategory->setDescription( $Description );
			    $lInventoryCategory->setCategory( $Category );
			    $lInventoryCategory->setSubcategory( $Subcategory );
			    $lInventoryCategory->save();
			  }
			  catch (Exception $lException) {
			    continue;
			  }
			} 

		}
		$this->DeleteNonExistingProductLines();

	}

	private function DeleteNonExistingProductLines() {
	  
		// Util::deb( ' DeleteNonExistingProductLines::' );
	  // Util::deb( $this->InventoryCategoriesProductLineArray, ' $this->InventoryCategoriesProductLineArray::' );
	  
		$InventoryCategoriesList= InventoryCategoryPeer::getInventoryCategories( 1, false );
		
		foreach( $InventoryCategoriesList as $InventoryCategory ) {
			if ( !in_array( $InventoryCategory->getProductLine(), $this->InventoryCategoriesProductLineArray ) ) {
				//Util::deb( $InventoryCategory->getProductLine(), ' $InventoryCategory->getProductLine()::' );
				$RelatedInvetoryItems= $InventoryCategory->getInventoryItems();
				//Util::deb( $RelatedInvetoryItems, ' $RelatedInvetoryItems::' );
				foreach( $RelatedInvetoryItems as $lInvetoryItem ) {
					$lInvetoryItem->delete();
  				$this->RelatedInvetoryItemsDeleted++;
				}
				
				$InventoryCategory->delete();
				$this->ItemsDeletedAsNoneExising++;
			}
		}
		return true;
	}
	
	public function getInfoText() {
		$Res= '<hr>';
		$Res.= 'Import from : <b>'.$this->OriginalExcelFileName.'</b>. <br><br>&nbsp;&nbsp;';
		$Res.= 'Items Added: <b>' . $this->ItemsAdded.'</b><br>&nbsp;&nbsp;'.
		'Exising Items Updated: <b>' . $this->ExisingItemsUpdated . '</b><br>&nbsp;&nbsp;'.		
		'Inventory Categories Deleted As None Exising: <b>' . $this->ItemsDeletedAsNoneExising . '</b><br>&nbsp;&nbsp;'.
		'Inventory Categories Skipped As Not Modified: <b>' . $this->SkippedAsNotModified . '</b><br>&nbsp;&nbsp;'.			 
		'Related Invetory Items Deleted: <b>' . $this->RelatedInvetoryItemsDeleted . '</b><br><br>&nbsp;&nbsp;';
		return $Res;
	}


}