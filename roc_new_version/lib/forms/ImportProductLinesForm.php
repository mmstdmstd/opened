<?php
class ImportProductLinesForm extends sfForm
{
  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :
    
    
    $this->setWidgets(array(
    'xls_file' => new sfWidgetFormInputFile( array(), array( 'size'=>30,'maxlength'=>100, 'class'=>'text' ) ),
    ));

    // form labels
    $this->widgetSchema->setLabel( 'xls_file', 'Select xls file for Product Lines importing' );


    $this->widgetSchema->setNameFormat('import_product_lines[%s]');
    // validators :
    $this->setValidators(  array(
    'xls_file' => new sfValidatorFile(  array('required' => true ), array('required' => "Select xls file." ) ),
    )  );

        // post validators :
    $this->validatorSchema->setPostValidator(
     new sfValidatorCallback(array('callback' => array($this, 'checkFileType')))  ); 

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

  
  
  /**
         * @param unknown_type $pValidator
         * @param unknown_type $pValues
         * @return unknown
         */
  public function checkFileType($pValidator, $pValues)
  {
    $lError = array();
    $lHelpers = array();
    if ( empty( $pValues['xls_file'] ) ) return $pValues;

    $xls_file= $pValues['xls_file'];
    
    $Ext= Util::GetFileNameExt($xls_file->getOriginalName()); 
    if ( strtolower($Ext) == "xlsx" ) { 
      $lHelpers['xls_file'] = 'Please, do a "Save As" to get it into the xls format before uploading it.<br />';
      $lError [] = 'Please, do a "Save As" to get it into the xls format before uploading it.';        
    }  
    
    if ( strtolower($Ext) == "xlsx" ) { 
      $lHelpers['xls_file'] = 'Please, select xls format for uploading.<br />';
      $lError [] = 'Please, select xls format for uploading.';        
    }
    if(!empty($lError))
    {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr)
      {
        $lErrorMessage .= $lErr.'<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }

  

}