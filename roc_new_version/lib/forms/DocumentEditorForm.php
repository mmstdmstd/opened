<?php
class DocumentEditorForm extends sfForm
{
  public static $Document;

  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :


    $this->setWidgets(array(
    DocumentPeer::KEY => new sfWidgetFormInput( array(),array('size'=>3,'maxlength'=>3, 'class'=>( self::$Document instanceof Document?'readonly_field':'text' ), ( self::$Document instanceof Document?'readonly':'' ) => ( self::$Document instanceof Document?'readonly':'' ),
    ) ),
    DocumentPeer::TITLE => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>50, 'class'=>'text' ) ),
    DocumentPeer::FILENAME => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
    DocumentPeer::THUMBNAIL => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
    DocumentPeer::ORDERING => new sfWidgetFormInput( array(),array('size'=>4,'maxlength'=>4, 'class'=>'text' ) ),	
    DocumentPeer::CREATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
    DocumentPeer::UPDATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) )
    ));


    // form labels
    $this->widgetSchema->setLabel( DocumentPeer::KEY, 'Key' );
    $this->widgetSchema->setLabel( DocumentPeer::TITLE, 'Title' );
    $this->widgetSchema->setLabel( DocumentPeer::FILENAME, 'Pdf Filename' );
    $this->widgetSchema->setLabel( DocumentPeer::THUMBNAIL, 'Thumbnail' );
    $this->widgetSchema->setLabel( DocumentPeer::ORDERING, 'Order' );
    $this->widgetSchema->setLabel( DocumentPeer::CREATED_AT, 'Created At' );
    $this->widgetSchema->setLabel( DocumentPeer::UPDATED_AT, 'Updated At' );
    $this->widgetSchema->setNameFormat('document[%s]');

    if(self::$Document instanceof Document )
    {
      $this->setDefault( DocumentPeer::KEY, self::$Document->getKey() );
      $this->setDefault( DocumentPeer::TITLE, self::$Document->getTitle() );
      $this->setDefault( DocumentPeer::FILENAME, self::$Document->getFilename() );
      $this->setDefault( DocumentPeer::THUMBNAIL, self::$Document->getThumbnail() );
      $this->setDefault( DocumentPeer::ORDERING, self::$Document->getOrdering() );
      $this->setDefault( DocumentPeer::CREATED_AT, self::$Document->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) );
      $this->setDefault( DocumentPeer::UPDATED_AT, self::$Document->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) );
    }
    // validators :
    $this->setValidators(array(

    DocumentPeer::KEY => new sfValidatorAnd(array(
    new sfValidatorString(array(), array( ) ),
    new sfValidatorRegex(
    array('pattern' => "/\//", 'must_match' => false),
    array ('invalid' => "Key can not have slash(\"/\") character.")
    ),
    ),
    array('required'   => true),
    array('required'   => 'Key can not be empty.')
    ),
    DocumentPeer::TITLE => new sfValidatorString( array('required' => true), array('required' => 'Title can not be empty.') ),
    DocumentPeer::FILENAME => new sfValidatorString( array('required' => true), array('required' => 'Filename can not be empty.') ),
    DocumentPeer::THUMBNAIL => new sfValidatorString( array('required' => true), array('required' => 'Thumbnail can not be empty.') ),
    DocumentPeer::ORDERING => new sfValidatorInteger( array('required' => true), array('required' => 'Ordering can not be empty.') ),
    DocumentPeer::CREATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
    DocumentPeer::UPDATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
    ) );

    // post validators :
    $this->validatorSchema->setPostValidator(
    new sfValidatorCallback(array('callback' => array($this, 'checkStatusFilled')))  );

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }



  /**
         * Validate Unique Title
         *
         * @param unknown_type $pValidator
         * @param unknown_type $pValues
         * @return unknown
         */
  public function checkStatusFilled($pValidator, $pValues)
  {
    $lError = array();
    $lHelpers = array();
    $Key= $pValues[DocumentPeer::KEY];
    $CreatedAt= $pValues[DocumentPeer::CREATED_AT];

    if ( empty($CreatedAt) ) {
      $lSimilarDocument= DocumentPeer::getSimilarDocument( $Key );
      if( !empty( $lSimilarDocument ) ) {
        $lHelpers[DocumentPeer::KEY] = 'There is already Youtube Video with this key.<br />';
        $lError [] = 'There is already Youtube Video with this key.';
      }
    }
    if(!empty($lError))
    {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr)
      {
        $lErrorMessage .= $lErr.'<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }


}

?>