<?php
class ReviewOrderForm extends sfForm
{
	/**
	 * Configure form fields
	 */
	public function configure()
	{
    $AdditionalFreightSum= Cart::getAdditionalFreightSum();
    //Util::deb( $AdditionalFreightSum, '$AdditionalFreightSum::' );
	  
    $ShipServicesArray= sfConfig::get('app_application_UPSServices');
    
    $mode= sfContext::getInstance()->getRequest()->getParameter('mode','');
    // Util::deb( $mode, '-1ZZZ $mode::' );
     
    $current_shipping= sfContext::getInstance()->getRequest()->getParameter('current_shipping','');
    // Util::deb( $current_shipping, '-2ZZZ $current_shipping::' );
     
     
    if ( $mode== 'update' or $mode== 'delete' ) {
      $shipping_tax_data=null;
    } else {    
      $shipping_tax_data = Cart::getTaxShipData();  //  
    }
    // $shipping_tax_data['ups_next_day_air_saver']= 80.1; // DELETE ROW
    
    
    $shipping_tax_data_labeled= array();
    foreach( $shipping_tax_data as $shipping_tax_data_key=>$shipping_tax_data_value ) {
      if ( $shipping_tax_data_key!= 'postal_code' and $shipping_tax_data_key!= 'est_tax' ) {
        $shipping_tax_data_labeled[$shipping_tax_data_key]= $shipping_tax_data_value + $AdditionalFreightSum;
      }
    }
    
    $CheckoutArray= Cart::getShippingInfo();
    // Util::deb( $shipping_tax_data, '-1 $shipping_tax_data::' );
     
         
    if ( empty($shipping_tax_data) ) {
      // Util::deb( $ShipServicesArray, '-2 $ShipServicesArray::' );
      $shipping_tax_data = ShippingTaxes::getShippingRates($CheckoutArray['b_zip'], false, false );
      // Util::deb( $shipping_tax_data, '-2ZZZZZ$shipping_tax_data::' );
       
      $tax_percent= ShippingTaxes::Calculate_tax( $CheckoutArray['b_zip'], $CheckoutArray['b_state'], false );
      // Util::deb( $tax_percent, '-2ZZZZZ$tax_percent::' );
      Cart::setTaxShipDataAsArray($shipping_tax_data, $current_shipping, $tax_percent );
      $shipping_tax_data_labeled= array();
      foreach( $shipping_tax_data as $shipping_tax_data_key=>$shipping_tax_data_value ) {
        if ( $shipping_tax_data_key!= 'postal_code' and $shipping_tax_data_key!= 'est_tax' ) {
          $shipping_tax_data_labeled[$shipping_tax_data_key]= $shipping_tax_data_value + $AdditionalFreightSum;
        }
      }

    //echo ' -3 $shipping_tax_data::<pre> '. print_r( $shipping_tax_data, true ) .'</pre><br>';
     // Util::deb( $shipping_tax_data, '-3 $shipping_tax_data::' );
     // Util::deb( $shipping_tax_data_labeled, '3__11__$shipping_tax_data_labeled::' );
      foreach($ShipServicesArray as $key=>$value) {
        $ShipServicesArray[$key]= $value.' - '. ( !empty($shipping_tax_data_labeled[$key]) ? Util::getDigitMoney( $shipping_tax_data_labeled[$key], 'Money' ) : '');
      }

    } else {
      // Util::deb( $shipping_tax_data_labeled, '3__22__$shipping_tax_data_labeled::' );
      foreach($ShipServicesArray as $key=>$value) {
        // Util::deb( AppUtils::getShippingTaxDataValue($key,$shipping_tax_data_labeled), 'AppUtils::getShippingTaxDataValue($key,$shipping_tax_data_labeled)::' );
        $ShipServicesArray[$key]= $value.' ' . AppUtils::getShippingTaxDataValue( $key,$shipping_tax_data_labeled );
      }
      
    }
    // echo ' -4 $ShipServicesArray::<pre> '. print_r( $ShipServicesArray, true ) .'</pre><br>';
    // Util::deb( $ShipServicesArray, '-4 $ShipServicesArray::' );

		$WidgetsArray= array(
		  'notes' => new sfWidgetFormTextarea( array(), array('cols'=>80,'rows'=>10 ) ),
		  'shipping_service' => new sfWidgetFormSelectRadio( array('choices'=>$ShipServicesArray), array( 'class' => 'styled_radio', 'onclick'=> 'javascript: return ShippingServiceClicked(this); ' ) ),
		);
		$this->setWidgets($WidgetsArray);

		// form labels
		$this->widgetSchema->setLabel( 'notes', 'Notes' );
		$this->widgetSchema->setLabel( 'shipping_service', 'Shipping' );
    $this->widgetSchema->setNameFormat('review_order[%s]');

 	  $ReviewOrderArray= Cart::getReviewOrder();
 	  
    // Util::deb( $ReviewOrderArray, '-44 $ReviewOrderArray::' );
 	  
		if( !empty($ReviewOrderArray['notes']) ) {
			$this->setDefault( 'notes', $ReviewOrderArray['notes'] );
			if ( !empty($ReviewOrderArray['shipping_service']) ) {
			  $current_shipping_service= $ReviewOrderArray['shipping_service'];
			}
			
      //Util::deb( $current_shipping_service, '-44 $current_shipping_service::' );
			$this->setDefault( 'shipping_service', $current_shipping_service );
			
		} else {
       //$shipping_tax_data_array = Cart::getTaxShipData();
       $ShippingTaxDataCurrentValue= AppUtils::getShippingTaxDataCurrentValue( $shipping_tax_data );
       if ( !empty( $ShippingTaxDataCurrentValue ) ) {
         $this->setDefault( 'shipping_service', $ShippingTaxDataCurrentValue );
       }
    }
    
    if ( !empty($current_shipping) ) {      
			$this->setDefault( 'shipping_service', AppUtils::getShippingTaxDataHighlabel($current_shipping) );
    }

		// validators :
		$ValidatorArray= array(
  		'notes' => new sfValidatorString( array( 'required' => false), array('required' => 'Notes can not be empty.') ),
  		'shipping_service' => new sfValidatorChoice( array('choices' => array_keys($ShipServicesArray),'required' => true), array( 'required' => 'Please select a shipping option.' ) ),
		);
		$this->setValidators( $ValidatorArray );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}

}