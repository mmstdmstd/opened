PaperworkEditorForm

<?php
class PaperworkEditorForm extends sfForm
{
	public static $Paperwork;

	/**
	 * Configure form fields
	 */
	public function configure()
	{
		// form fields :


		$this->setWidgets(array(
			PaperworkPeer::CODE => new sfWidgetFormInput( array(),array('size'=>3,'maxlength'=>3, 'class'=>( self::$Paperwork instanceof Paperwork?'readonly_field':'text' ), ( self::$Paperwork instanceof Paperwork?'readonly':'' ) => ( self::$Paperwork instanceof Paperwork?'readonly':'' ),
			) ),
			PaperworkPeer::TITLE => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>50, 'class'=>'text' ) ),
			PaperworkPeer::FILENAME => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
			PaperworkPeer::THUMBNAIL => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
			PaperworkPeer::ORDERING => new sfWidgetFormInput( array(),array('size'=>4,'maxlength'=>4, 'class'=>'text' ) ),
		));


		// form labels
		$this->widgetSchema->setLabel( PaperworkPeer::CODE, 'Code' );
		$this->widgetSchema->setLabel( PaperworkPeer::TITLE, 'Title' );
		$this->widgetSchema->setLabel( PaperworkPeer::FILENAME, 'Filename' );
		$this->widgetSchema->setLabel( PaperworkPeer::THUMBNAIL, 'Thumbnail' );
		$this->widgetSchema->setLabel( PaperworkPeer::ORDERING, 'Order' );
		$this->widgetSchema->setNameFormat('paperwork[%s]');

		if(self::$Paperwork instanceof Paperwork )
		{
			$this->setDefault( PaperworkPeer::CODE, self::$Paperwork->getCode() );
			$this->setDefault( PaperworkPeer::TITLE, self::$Paperwork->getTitle() );
			$this->setDefault( PaperworkPeer::FILENAME, self::$Paperwork->getFilename() );
			$this->setDefault( PaperworkPeer::THUMBNAIL, self::$Paperwork->getThumbnail() );
			$this->setDefault( PaperworkPeer::ORDERING, self::$Paperwork->getOrdering() );
		}
		// validators :
		$this->setValidators(array(

			PaperworkPeer::CODE => new sfValidatorAnd(array(
					new sfValidatorString(array(), array( ) ),
					new sfValidatorRegex(
						array('pattern' => "/\//", 'must_match' => false),
						array ('invalid' => "Code can not have slash(\"/\") character.")
					),
				),
				array('required'   => true),
				array('required'   => 'Code can not be empty.')
			),
			PaperworkPeer::TITLE => new sfValidatorString( array('required' => true), array('required' => 'Title can not be empty.') ),
			PaperworkPeer::FILENAME => new sfValidatorString( array('required' => true), array('required' => 'Filename can not be empty.') ),
			PaperworkPeer::THUMBNAIL => new sfValidatorString( array('required' => true), array('required' => 'Thumbnail can not be empty.') ),
			PaperworkPeer::ORDERING => new sfValidatorInteger( array('required' => true), array('required' => 'Ordering can not be empty.') ),
		) );

		// post validators :
		//$this->validatorSchema->setPostValidator(
		//	new sfValidatorCallback(array('callback' => array($this, 'checkStatusFilled')))  );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}



	/**
	 * Validate Unique Title
	 *
	 * @param unknown_type $pValidator
	 * @param unknown_type $pValues
	 * @return unknown
	 */
	public function checkStatusFilled($pValidator, $pValues)
	{
		$lError = array();
		$lHelpers = array();
		$Code= $pValues[PaperworkPeer::CODE];

			$lSimilarPaperwork= PaperworkPeer::getSimilarPaperwork( $Code );
			if( !empty( $lSimilarPaperwork ) ) {
				$lHelpers[PaperworkPeer::CODE] = 'There is already Paperwork with this code.<br />';
				$lError [] = 'There is already Paperwork with this code.';
			}
		if(!empty($lError))
		{
			$this->getWidgetSchema()->setHelps($lHelpers);
			$lErrorMessage = '';
			foreach ($lError as $lErr)
			{
				$lErrorMessage .= $lErr.'<br />';
			}
			throw new sfValidatorError($pValidator, $lErrorMessage);
		}
		return $pValues;
	}


}

?>