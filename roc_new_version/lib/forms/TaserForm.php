<?php

class TaserForm extends sfForm {

  /**
   * Configure form fields
   */
  public function configure() {

    $StateList = Util::SetArrayHeader(array('' => '   '), AppUtils::getListOfStatesOfUSA(true));
    $CurrentEcwsList = array('' => ' -Select- ', '1-2 years' => '1-2 years', '2-3 years' => '2-3 years', '3-4 years' => '3-4 years', '4 and older' => '4 and older', 'Not applicable' => 'Not applicable');
    $PlanToReplaceEcwsList = array('' => ' -Select- ', '1-3 months' => '1-3 months', '3-6 months' => '3-6 months', '6 or more months' => '6 or more months', 'Not yet determined' => 'Not yet determined', 'Not applicable' => 'Not applicable');
    $HaveBudgetEcwsList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No');
    $IsYourAgencyList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No');
    $PlanToAcquireCameraList = array('' => ' -Select- ', '1-3 months' => '1-3 months', '3-6 months' => '3-6 months', '6 or more months' => '6 or more months', 'Not yet determined' => 'Not yet determined', 'Not applicable' => 'Not applicable');
    $HaveBudgetAllocatedForCameraList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No');
    $WidgetsArray = array(
        'first_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>1)),
        'last_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>2)),
        'department_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>3)),
        'department_person' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>4)),
        'email' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>5)),
        'phone' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>6)),
        'address' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>7)),
        'city' => new sfWidgetFormInput(array(), array('size' => 20, 'maxlength' => 30, 'class'=>"te-form-input", 'tabindex'=>8)),
        'state' => new sfWidgetFormSelect(array('choices' => $StateList), array('class'=>"te-form-select", 'tabindex'=>9)),
        'zip' => new sfWidgetFormInput(array(), array('size' => 6, 'maxlength' => 6, 'tabindex'=>10)),
        'ecws_model' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 100, 'tabindex'=>11)),
        'current_ecws' => new sfWidgetFormSelect(array('choices' => $CurrentEcwsList), array('class'=>"te-form-select2", 'tabindex'=>12)),
        'plan_to_replace_ecws' => new sfWidgetFormSelect(array('choices' => $PlanToReplaceEcwsList), array('class'=>"te-form-select2", 'tabindex'=>13)),
        'have_budget_ecws' => new sfWidgetFormSelect(array('choices' => $HaveBudgetEcwsList), array('class'=>"te-form-select2", 'tabindex'=>14)),
        'additional_notes' => new sfWidgetFormTextarea(array(), array('cols' => 50, 'rows' => 8, 'tabindex'=>15 )),
        'is_your_agency' => new sfWidgetFormSelect(array('choices' => $IsYourAgencyList), array('class'=>"te-form-select2", 'tabindex'=>16)),
        'plan_to_acquire_camera' => new sfWidgetFormSelect(array('choices' => $PlanToAcquireCameraList), array( 'class'=>"te-form-select2", 'tabindex'=>17)),
        'have_budget_allocated_for_camera' => new sfWidgetFormSelect(array('choices' => $HaveBudgetAllocatedForCameraList), array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No', 'tabindex'=>18)),
    );
    $this->setWidgets($WidgetsArray);

    // form labels
    $this->widgetSchema->setLabel('first_name', 'First Name');
    $this->widgetSchema->setLabel('last_name', 'Last Name');
    $this->widgetSchema->setLabel('department_name', 'Department Name');
    $this->widgetSchema->setLabel('department_person', 'Contact Person for Tasers in Department');
    $this->widgetSchema->setLabel('email', 'Email');
    $this->widgetSchema->setLabel('phone', 'Phone');
    $this->widgetSchema->setLabel('address', 'Address');
    $this->widgetSchema->setLabel('city', 'City');
    $this->widgetSchema->setLabel('state', 'State');
    $this->widgetSchema->setLabel('zip', 'Zip Code');
    $this->widgetSchema->setLabel('ecws_model', 'What make/model of ECWs are you interested in ?');
    $this->widgetSchema->setLabel('current_ecws', 'How old are your current ECWs?');
    $this->widgetSchema->setLabel('plan_to_replace_ecws', 'When do you plan to acquire or replace your ECWs?');
    $this->widgetSchema->setLabel('have_budget_ecws', 'Do you have budget allocated in your planned<br> acquire/replace timeframe for new ECWs?');
    $this->widgetSchema->setLabel('additional_notes', 'Additional notes');
    $this->widgetSchema->setLabel('is_your_agency', 'Is your agency considering video camera systems?');
    $this->widgetSchema->setLabel('plan_to_acquire_camera', 'When do you plan to acquire your camera system?');
    $this->widgetSchema->setLabel('have_budget_allocated_for_camera', 'Do you currently have budget allocated for camera systems?');

    $this->widgetSchema->setNameFormat('taser[%s]');

    // validators :
    $ValidatorArray = array(
        'first_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'last_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'department_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'department_person' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'email' => new sfValidatorEmail(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.', 'invalid' => 'E-mail&nbsp;is&nbsp;invalid.')),
        'phone' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'address' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'city' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'state' => new sfValidatorChoice(array('choices' => array_keys($StateList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'zip' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'ecws_model' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'current_ecws' => new sfValidatorChoice(array('choices' => array_keys($CurrentEcwsList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'plan_to_replace_ecws' => new sfValidatorChoice(array('choices' => array_keys($PlanToReplaceEcwsList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'have_budget_ecws' => new sfValidatorChoice(array('choices' => array_keys($HaveBudgetEcwsList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'additional_notes' => new sfValidatorString(array('required' => false), array()),
        'is_your_agency' => new sfValidatorChoice(array('choices' => array_keys($IsYourAgencyList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'plan_to_acquire_camera' => new sfValidatorChoice(array('choices' => array_keys($PlanToAcquireCameraList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'have_budget_allocated_for_camera' => new sfValidatorChoice(array('choices' => array_keys($HaveBudgetAllocatedForCameraList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
    );
    $this->setValidators($ValidatorArray);

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

}