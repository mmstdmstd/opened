<?php

class NewCustomerForm extends sfForm {
  //public static $InventoryItem;

  /**
   * Configure form fields
   */
  public function configure() {
    $WidgetsArray = array(
	'email' => new sfWidgetFormInput(array(), array()),
	'returning_email' => new sfWidgetFormInput(array(), array()),
	'password' => new sfWidgetFormInputPassword(array(), array()),
	'account_type' => new sfWidgetFormInputHidden(array(), array()),
    );
    $this->setWidgets($WidgetsArray);


    // form labels
    $this->widgetSchema->setLabel('email', 'email address');
    $this->widgetSchema->setLabel('returning_email', 'email');
    $this->widgetSchema->setLabel('password', 'password');
    $this->widgetSchema->setNameFormat('new_customer[%s]');

    if (Cart::hasLoggedUser()) {
      $this->setDefault('email', Cart::getLoggedUserEmail());
      $this->setDefault('returning_email', '');
    }

    // validators :
    $ValidatorArray = array(
	'email' => new sfValidatorEmail(array('required' => false), array('required' => 'Your e-mail  can not be empty.', 'invalid' => 'The email address is invalid.')),
	'returning_email' => new sfValidatorEmail(array('required' => false), array('required' => 'Your e-mail  can not be empty.', 'invalid' => 'Invalid email or password.')),
	'password' => new sfValidatorString(array('required' => false), array('required' => 'Password can not be empty.')),
	'account_type' => new sfValidatorString(array('required' => false), array()),
    );
    $this->setValidators($ValidatorArray);

    // post validators :
    $this->validatorSchema->setPostValidator(
	    new sfValidatorCallback(array('callback' => array($this, 'checkPasswordFilled'))));

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

  /**
   * Validate Unique Title
   *
   * @param unknown_type $pValidator
   * @param unknown_type $pValues
   * @return unknown
   */
  public function checkPasswordFilled($pValidator, $pValues) {
    $lError = array();
    $lHelpers = array();
    // Util::deb( $pValues, ' $pValues::' );
    $password = trim($pValues['password']);
    $email = trim($pValues['email']);


    $account_type = $pValues['account_type'];
    if ($account_type == 'Existing User' and !empty($pValues['returning_email'])) {
      $lsfGuardUser = sfGuardUserPeer::getByUsername($pValues['returning_email']);
      if (empty($lsfGuardUser)) {
	$lHelpers['password'] = 'There is no user with this email.<br />';
	$lError [] = 'There is no user with this email.';
      } else {
        if ( !empty($password) ) {
	  $checkPassword= $lsfGuardUser->checkPassword( $password );
	  if (!$checkPassword  ) {
	    $lHelpers['password'] = 'Invalid email or password.<br />';
	    $lError [] = 'Invalid email or password.';
	  }
	}
      }
    }

    if (!empty($lError)) {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr) {
	$lErrorMessage .= $lErr . '<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }

}