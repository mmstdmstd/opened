<?php
class SizingChartEditorForm extends sfForm
{
	public static $SizingChart;

	/**
	 * Configure form fields
	 */
	public function configure()
	{
		// form fields :


		$this->setWidgets(array(
			SizingChartPeer::KEY => new sfWidgetFormInput( array(),array('size'=>3,'maxlength'=>3, 'class'=>( self::$SizingChart instanceof SizingChart?'readonly_field':'text' ), ( self::$SizingChart instanceof SizingChart?'readonly':'' ) => ( self::$SizingChart instanceof SizingChart?'readonly':'' ),
			) ),
			SizingChartPeer::TITLE => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>50, 'class'=>'text' ) ),
			SizingChartPeer::FILENAME => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
			SizingChartPeer::THUMBNAIL => new sfWidgetFormInputFile( array(), array( 'size'=>40,'maxlength'=>100, 'class'=>'text' ) ),
			SizingChartPeer::ORDERING => new sfWidgetFormInput( array(),array('size'=>4,'maxlength'=>4, 'class'=>'text' ) ),
			SizingChartPeer::CREATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
			SizingChartPeer::UPDATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) )
		));


		// form labels
		$this->widgetSchema->setLabel( SizingChartPeer::KEY, 'Key' );
		$this->widgetSchema->setLabel( SizingChartPeer::TITLE, 'Title' );
		$this->widgetSchema->setLabel( SizingChartPeer::FILENAME, 'Filename' );
		$this->widgetSchema->setLabel( SizingChartPeer::THUMBNAIL, 'Thumbnail' );
		$this->widgetSchema->setLabel( SizingChartPeer::ORDERING, 'Order' );
		$this->widgetSchema->setLabel( SizingChartPeer::CREATED_AT, 'Created At' );
		$this->widgetSchema->setLabel( SizingChartPeer::UPDATED_AT, 'Updated At' );
		$this->widgetSchema->setNameFormat('sizing_chart[%s]');

		if(self::$SizingChart instanceof SizingChart )
		{
			$this->setDefault( SizingChartPeer::KEY, self::$SizingChart->getKey() );
			$this->setDefault( SizingChartPeer::TITLE, self::$SizingChart->getTitle() );
			$this->setDefault( SizingChartPeer::FILENAME, self::$SizingChart->getFilename() );
			$this->setDefault( SizingChartPeer::THUMBNAIL, self::$SizingChart->getThumbnail() );
			$this->setDefault( SizingChartPeer::ORDERING, self::$SizingChart->getOrdering() );
			$this->setDefault( SizingChartPeer::CREATED_AT, self::$SizingChart->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) );
			$this->setDefault( SizingChartPeer::UPDATED_AT, self::$SizingChart->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) );
		}
		// validators :
		$this->setValidators(array(

			SizingChartPeer::KEY => new sfValidatorAnd(array(
					new sfValidatorString(array(), array( ) ),
					new sfValidatorRegex(
						array('pattern' => "/\//", 'must_match' => false),
						array ('invalid' => "Key can not have slash(\"/\") character.")
					),
				),
				array('required'   => true),
				array('required'   => 'Key can not be empty.')
			),
			SizingChartPeer::TITLE => new sfValidatorString( array('required' => true), array('required' => 'Title can not be empty.') ),
			SizingChartPeer::FILENAME => new sfValidatorString( array('required' => true), array('required' => 'Filename can not be empty.') ),
			SizingChartPeer::THUMBNAIL => new sfValidatorString( array('required' => true), array('required' => 'Thumbnail can not be empty.') ),
			SizingChartPeer::ORDERING => new sfValidatorInteger( array('required' => true), array('required' => 'Ordering can not be empty.') ),
			SizingChartPeer::CREATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
			SizingChartPeer::UPDATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
		) );

		// post validators :
		//$this->validatorSchema->setPostValidator(
		//	new sfValidatorCallback(array('callback' => array($this, 'checkStatusFilled')))  );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}



	/**
	 * Validate Unique Title
	 *
	 * @param unknown_type $pValidator
	 * @param unknown_type $pValues
	 * @return unknown
	 */
	public function checkStatusFilled($pValidator, $pValues)
	{
		$lError = array();
		$lHelpers = array();
		$Key= $pValues[SizingChartPeer::KEY];
		$CreatedAt= $pValues[SizingChartPeer::CREATED_AT];

		if ( empty($CreatedAt) ) {
			$lSimilarSizingChart= SizingChartPeer::getSimilarSizingChart( $Key );
			if( !empty( $lSimilarSizingChart ) ) {
				$lHelpers[SizingChartPeer::KEY] = 'There is already sizing chart with this key.<br />';
				$lError [] = 'There is already sizing chart with this key.';
			}
		}
		if(!empty($lError))
		{
			$this->getWidgetSchema()->setHelps($lHelpers);
			$lErrorMessage = '';
			foreach ($lError as $lErr)
			{
				$lErrorMessage .= $lErr.'<br />';
			}
			throw new sfValidatorError($pValidator, $lErrorMessage);
		}
		return $pValues;
	}


}

?>