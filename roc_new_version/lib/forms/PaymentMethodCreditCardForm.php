<?php
class PaymentMethodCreditCardForm extends sfForm
{
	/**
	 * Configure form fields
	 */
	public function configure()
	{
    $payment_methods= sfConfig::get('app_application_payment_methods');
  //  Util::deb( $payment_methods, ' $payment_methods::' );

/*		$expiration_date_year_start= (int)sfConfig::get('app_application_expiration_date_year_start');
		$expiration_date_year_end= (int)sfConfig::get('app_application_expiration_date_year_end');

		$YearsList= array(''=>' -Select- ');
		for( $Year= $expiration_date_year_start; $Year<= $expiration_date_year_end; $Year++ ) {
		  $YearsList[$Year]= $Year;
	  }
	  //Util::deb( $YearsList, ' -1 $YearsList::' );

	  $MonthesList= array( ''=>' -Select- ', 1=>'01',  2=>'02', 3=>'03', 4=>'04', 5=>'05', 6=>'06', 7=>'07', 8=>'08', 9=>'09', 10=>'10', 11=>'11', 12=>'12' );
*/


    $WidgetsArray= array(
		//'notes' => new sfWidgetFormTextarea( array(), array('cols'=>80,'rows'=>10 ) ),
		'payment_method' => new sfWidgetFormSelectRadio( array('choices'=>$payment_methods), array( 'class'=>"styled_radio", 'onclick'=>'javascript:onChangePaymentMethod(this.value);' ) ),
	//	'card_number' => new sfWidgetFormInput( array(), array('size'=>20,'maxlength'=>50 ) ),
//		'expiration_date_year' => new sfWidgetFormSelect( array('choices'=>$YearsList), array() ),
  //  $WidgetsArray= array(

		//'expiration_date_month' => new sfWidgetFormSelect( array('choices'=>$MonthesList), array() ),
		//'card_security_code' => new sfWidgetFormInput( array(), array('size'=>20,'maxlength'=>50 ) ),

		);
		$this->setWidgets($WidgetsArray);
		// form labels
		//$this->widgetSchema->setLabel( 'notes', 'Notes' );
		$this->widgetSchema->setLabel( 'payment_method', 'Payment&nbsp;type');
		/* $this->widgetSchema->setLabel( 'card_number', 'Card&nbsp;Number' );
		$this->widgetSchema->setLabel( 'expiration_date_year', 'Expiration&nbsp;Date' );
		$this->widgetSchema->setLabel( 'expiration_date_month', '' );
		$this->widgetSchema->setLabel( 'card_security_code', 'Card&nbsp;Security&nbsp;Code' ); */

    $this->widgetSchema->setNameFormat('payment_type[%s]');

 	 /* $ReviewOrderArray= Cart::getReviewOrder();
		if( !empty($ReviewOrderArray['notes']) ) {
			//$this->setDefault( 'notes', $ReviewOrderArray['notes'] );
		}  */
		$CheckoutArray= Cart::getShippingInfo();
		if( !empty($CheckoutArray['card_number']) ) {
      // Util::deb( 'INSIDE::' );
			$this->setDefault( 'payment_method', $CheckoutArray['payment_method'] );
			/*$this->setDefault( 'card_number', $CheckoutArray['card_number'] );
			$this->setDefault( 'expiration_date_year', $CheckoutArray['expiration_date_year'] );
			$this->setDefault( 'expiration_date_month', $CheckoutArray['expiration_date_month'] );
			$this->setDefault( 'card_security_code', $CheckoutArray['card_security_code'] );*/
    } else {
      // Util::deb( 'OUT::' );
			$this->setDefault( 'payment_method', 'CC' );
    }
		// validators :
		$ValidatorArray= array(
		//'notes' => new sfValidatorString( array( 'required' => true), array('required' => 'Notes can not be empty.') ),
		'payment_method' => new sfValidatorChoice(array('choices' => array_keys($payment_methods),'required' => true), array('required'   => 'Payment type can not be empty.')),
		/*'card_number' => new sfValidatorString( array( 'required' => true), array('required' => 'Card Number can not be empty.') ),
		'expiration_date_year' => new sfValidatorChoice(array('choices' => array_keys($YearsList),'required' => true), array('required'   => 'Expiration Date Year can not be empty.')),
		'expiration_date_month' => new sfValidatorChoice(array('choices' => array_keys($MonthesList),'required' => true), array('required'   => ' Expiration Date Month can not be empty.')),
		'card_security_code' => new sfValidatorString( array('required' => true), array('required' => 'Card Security Code can not be empty.') ), */

		);
		$this->setValidators( $ValidatorArray );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}

}