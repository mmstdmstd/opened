<?php
class YoutubeVideoEditorForm extends sfForm
{
  public static $YoutubeVideo;

  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :


    $this->setWidgets(array(
    YoutubeVideoPeer::KEY => new sfWidgetFormInput( array(),array('size'=>3,'maxlength'=>3, 'class'=>( self::$YoutubeVideo instanceof YoutubeVideo?'readonly_field':'text' ), ( self::$YoutubeVideo instanceof YoutubeVideo?'readonly':'' ) => ( self::$YoutubeVideo instanceof YoutubeVideo?'readonly':'' ),
    ) ),
    YoutubeVideoPeer::URL => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>100, 'class'=>'text' ) ),
    YoutubeVideoPeer::TITLE => new sfWidgetFormInput( array(),array('size'=>50,'maxlength'=>50, 'class'=>'text' ) ),
    YoutubeVideoPeer::ORDERING => new sfWidgetFormInput( array(),array('size'=>4,'maxlength'=>4, 'class'=>'text' ) ),
    YoutubeVideoPeer::DESCRIPTION => new sfWidgetFormTextarea( array(), array('cols'=>80,'rows'=>10 ) ),
    YoutubeVideoPeer::CREATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) )
    ));


    // form labels
    $this->widgetSchema->setLabel( YoutubeVideoPeer::KEY, 'Key' );
    $this->widgetSchema->setLabel( YoutubeVideoPeer::URL, 'Url' );
    $this->widgetSchema->setLabel( YoutubeVideoPeer::TITLE, 'Title' );
    $this->widgetSchema->setLabel( YoutubeVideoPeer::ORDERING, 'Order' );
    $this->widgetSchema->setLabel( YoutubeVideoPeer::DESCRIPTION, 'Description' );
    $this->widgetSchema->setLabel( YoutubeVideoPeer::CREATED_AT, 'Created At' );
    $this->widgetSchema->setNameFormat('youtube_video[%s]');

    if(self::$YoutubeVideo instanceof YoutubeVideo )
    {
      $this->setDefault( YoutubeVideoPeer::KEY, self::$YoutubeVideo->getKey() );
      $this->setDefault( YoutubeVideoPeer::URL, self::$YoutubeVideo->getUrl() );
      $this->setDefault( YoutubeVideoPeer::TITLE, self::$YoutubeVideo->getTitle() );
      $this->setDefault( YoutubeVideoPeer::ORDERING, self::$YoutubeVideo->getOrdering() );
      $this->setDefault( YoutubeVideoPeer::DESCRIPTION, self::$YoutubeVideo->getDescription() );
      $this->setDefault( YoutubeVideoPeer::CREATED_AT, self::$YoutubeVideo->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) );
    }
    // validators :
    $this->setValidators(array(

    YoutubeVideoPeer::KEY => new sfValidatorAnd(array(
    new sfValidatorString(array(), array( ) ),
    new sfValidatorRegex(
    array('pattern' => "/\//", 'must_match' => false),
    array ('invalid' => "Key can not have slash(\"/\") character.")
    ),
    ),
    array('required'   => true),
    array('required'   => 'Key can not be empty.')
    ),
    YoutubeVideoPeer::URL => new sfValidatorUrl( array('protocols' => array('http', 'https')),array('invalid'=>'Must be valid usl entered with https/http.', 'required' => 'Url can not be empty.')  )  ,
    YoutubeVideoPeer::TITLE => new sfValidatorString( array('required' => true), array('required' => 'Title can not be empty.') ),

    YoutubeVideoPeer::ORDERING => new sfValidatorInteger( array('required' => true), array('required' => 'Ordering can not be empty.') ),
    YoutubeVideoPeer::DESCRIPTION => new sfValidatorString( array('required' => false), array('required' => 'Description can not be empty.') ),
    YoutubeVideoPeer::CREATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
    ) );

    // post validators :
    //$this->validatorSchema->setPostValidator(
    //new sfValidatorCallback(array('callback' => array($this, 'checkStatusFilled')))  );

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }



  /**
         * Validate Unique Title
         *
         * @param unknown_type $pValidator
         * @param unknown_type $pValues
         * @return unknown
         */
  public function checkStatusFilled($pValidator, $pValues)
  {
    $lError = array();
    $lHelpers = array();
    $Key= $pValues[YoutubeVideoPeer::KEY];
    $CreatedAt= $pValues[YoutubeVideoPeer::CREATED_AT];

    if ( empty($CreatedAt) ) {
      $lSimilarYoutubeVideo= YoutubeVideoPeer::getSimilarYoutubeVideo( $Key );
      if( !empty( $lSimilarYoutubeVideo ) ) {
        $lHelpers[YoutubeVideoPeer::KEY] = 'There is already Youtube Video with this key.<br />';
        $lError [] = 'There is already Youtube Video with this key.';
      }
    }
    if(!empty($lError))
    {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr)
      {
        $lErrorMessage .= $lErr.'<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }


}

?>