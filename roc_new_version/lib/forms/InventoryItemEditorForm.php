<?php
class InventoryItemEditorForm extends sfForm
{
  public static $InventoryItem;
  public static $IsInsert;
  
  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :

    // $BrandsList= Util::SetArrayHeader( array( ''=>'  -Select Brand-  '), BrandPeer::getSelectionList() ) ;
    /*$StatusList= Util::SetArrayHeader( array(''=>'  -Select Status-  '), InventoryItemPeer::$StatusArray );
    $PositionsList= PositionPeer::getSelectionList('','  -Select Position-  ');    
    $RankingList= Util::SetArrayHeader( array(''=>'  -Select Ranking-  '), AppUtils::GetRankingList(true) );
    $StateList= Util::SetArrayHeader( array(''=>'  -Select State-  '), AppUtils::getListOfStatesOfUSA() );
    $CollegesList= CollegePeer::getSelectionList('','');
    $GraduationYearList= Util::SetArrayHeader( array( ''=>'  -Select Year-  '), RecruitPeer::getGraduationYearList() ) ;
  inventory_item:
    _attributes:               { idMethod: native }
    id:                        { type: INTEGER, required: true, autoIncrement: true, primaryKey: true }
    title:                     { type: VARCHAR, size: '100', required: true }
    sku:                       { type: VARCHAR, size: '30', required: true }
    brand_id:                  { type: INTEGER, required: true, foreignTable: brand, foreignReference: id, onDelete: RESTRICT }        
    description_short:         { type: VARCHAR, size: '255', required: true }
    description_long:          { type: LONGVARCHAR, required: false }
    size:                      { type: VARCHAR, size: '10', required: false }
    color:                     { type: VARCHAR, size: '7', required: false }
    options:                   { type: LONGVARCHAR, required: false }
    gender:                    { type: VARCHAR, size: '1', required: false }    
    paperwork:                 { type: VARCHAR, size: '2', required: false }
    sizing:                    { type: VARCHAR, size: '10', required: false }
    customizable:              { type: boolean, default: false }
    video:                     { type: VARCHAR, size: '100', required: false }
    unit_measure:              { type: VARCHAR, size: '10', required: false }
    qty_on_hand:               { type: float, size: '9,2', required: false }
    std_unit_price:            { type: float, size: '9,2', required: false }
    sale_start_date:           { type: date, required: false }
    sale_end_date:             { type: date, required: false }
    sale_method_id:            { type: INTEGER, required: true, foreignTable: sale_method, foreignReference: id, onDelete: RESTRICT }            
    sale_price:                { type: float, size: '9,2', required: false }
    sale_percent:              { type: float, size: '3,1', required: false }
    tax_class:                 { type: VARCHAR, size: '10', required: false }
    ship_weight:               { type: VARCHAR, size: '10', required: false }    
    created_at:                { type: TIMESTAMP, required: true }
    updated_at:                { type: TIMESTAMP, required: true }
    _uniques:                  { ind_inventory_item: [brand_id, title], ind_inventory_item_sku: [sku] }
    
    */
    $WidgetsArray= array(
    InventoryItemPeer::ID => new sfWidgetFormInputHidden( array(), array('readonly'=>'readonly', 'class'=>'readonly_field', 'onfocus'=>'javascript:document.getElementById("inventory_item_inventory_item.TITLE").focus()' ) ),
    InventoryItemPeer::TITLE => new sfWidgetFormInput( array(),array('size'=>30,'maxlength'=>100, 'class'=>'text' ) ),    
    InventoryItemPeer::SKU => new sfWidgetFormInput( array(),array('size'=>30,'maxlength'=>30, 'class'=>'text' ) ),    
    InventoryItemPeer::BRAND_ID => new sfWidgetFormSelect( array('choices'=>$BrandsList), array('class'=>'text') ),
    InventoryItemPeer::DESCRIPTION_SHORT => new sfWidgetFormInput( array(),array('size'=>100,'maxlength'=>255, 'class'=>'text' ) ),    
/*    InventoryItemPeer::POSITION_ID => new sfWidgetFormSelect( array('choices'=>$PositionsList), array('class'=>'text') ),
    
    InventoryItemPeer::SCHOOL => new sfWidgetFormInput( array(),array('size'=>30,'maxlength'=>100, 'class'=>'text' ) ),    
    InventoryItemPeer::GRADUATION_YEAR => new sfWidgetFormSelect( array('choices'=>$GraduationYearList), array('class'=>'text') ),

    InventoryItemPeer::STATE => new sfWidgetFormSelect( array('choices'=>$StateList), array('class'=>'text') ),
    InventoryItemPeer::CITY => new sfWidgetFormInput( array(),array('size'=>30,'maxlength'=>50, 'class'=>'text' ) ),
    
    InventoryItemPeer::HEIGHT => new sfWidgetFormInput( array(),array('size'=>6,'maxlength'=>6, 'class'=>'text' ) ),
    InventoryItemPeer::WEIGHT => new sfWidgetFormInput( array(),array('size'=>6,'maxlength'=>6, 'class'=>'text' ) ),    
    'recruit_colleges_interest' => new sfWidgetFormChoice( array('choices'=>$CollegesList, 'multiple' => true,  
), array('size'=>6) ) , */

    InventoryItemPeer::CREATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
    InventoryItemPeer::UPDATED_AT => new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) ),
    );
/*    if ( !self::$IsProposedRecruit ) {
      $WidgetsArray[InventoryItemPeer::STATUS]= new sfWidgetFormSelect( array('choices'=>$StatusList), array('class'=>'text', 'onchange' => 'javascript:StatusonChange()') );
      $WidgetsArray[InventoryItemPeer::COMMITTED_COLLEGE_ID] = new sfWidgetFormSelect( array('choices'=>$CollegesList), array('class'=>'text') );
      $WidgetsArray[InventoryItemPeer::COMMITTED_DATE] = new sfWidgetFormInput( array(), array( 'readonly'=>'readonly', 'class'=>'readonly_field' ) );
    } */
    $this->setWidgets($WidgetsArray);
    

    // form labels
    $this->widgetSchema->setLabel( InventoryItemPeer::ID, 'Inventory Item ID' );
    $this->widgetSchema->setLabel( InventoryItemPeer::TITLE, 'Title' );    
    $this->widgetSchema->setLabel( InventoryItemPeer::SKU, 'SKU' );    
    $this->widgetSchema->setLabel( InventoryItemPeer::BRAND_ID, 'Brand' );
    /*$this->widgetSchema->setLabel( InventoryItemPeer::RANKING, 'Ranking' );
    $this->widgetSchema->setLabel( InventoryItemPeer::SCHOOL, 'School' );
    $this->widgetSchema->setLabel( InventoryItemPeer::GRADUATION_YEAR, 'Graduation Year' );

    $this->widgetSchema->setLabel( InventoryItemPeer::STATE, 'State' );
    $this->widgetSchema->setLabel( InventoryItemPeer::CITY, 'City' );
    $this->widgetSchema->setLabel( InventoryItemPeer::HEIGHT, 'Height' );
    $this->widgetSchema->setLabel( InventoryItemPeer::WEIGHT, 'Weight' );  */
    $this->widgetSchema->setLabel( InventoryItemPeer::CREATED_AT, 'Created At' );
    $this->widgetSchema->setLabel( InventoryItemPeer::UPDATED_AT, 'Updated At' );
    /*if ( !self::$IsProposedInventoryItem ) {
      $this->widgetSchema->setLabel( InventoryItemPeer::STATUS, 'Status' );      
      $this->widgetSchema->setLabel( InventoryItemPeer::COMMITTED_COLLEGE_ID, 'Contract Committed College' );
      $this->widgetSchema->setLabel( InventoryItemPeer::COMMITTED_DATE, 'Committed Date' );
    } */
    $this->widgetSchema->setNameFormat('inventory_item[%s]');

    if(self::$InventoryItem instanceof InventoryItem )
    {
      $this->setDefault( InventoryItemPeer::ID, self::$InventoryItem->getId() );
      $this->setDefault( InventoryItemPeer::TITLE, self::$InventoryItem->getTitle() );
      $this->setDefault( InventoryItemPeer::SKU, self::$InventoryItem->getSku() );
      $this->setDefault( InventoryItemPeer::BRAND_ID, self::$InventoryItem->getBrandId() );
      /*$this->setDefault( InventoryItemPeer::RANKING, self::$InventoryItem->getRanking() );
      $this->setDefault( InventoryItemPeer::SCHOOL, self::$InventoryItem->getSchool() );
      $this->setDefault( InventoryItemPeer::GRADUATION_YEAR, self::$InventoryItem->getGraduationYear() );

      $this->setDefault( InventoryItemPeer::STATE, self::$InventoryItem->getState() );
      $this->setDefault( InventoryItemPeer::CITY, self::$InventoryItem->getCity() );            
      $this->setDefault( InventoryItemPeer::HEIGHT, self::$InventoryItem->getHeight() );
      $this->setDefault( InventoryItemPeer::WEIGHT, self::$InventoryItem->getWeight() );
                 
      //$this->setDefault( 'recruit_colleges_interest', $RecruitCollegesInterestsArray ); */
      $this->setDefault( InventoryItemPeer::CREATED_AT, self::$InventoryItem->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) );
      $this->setDefault( InventoryItemPeer::UPDATED_AT, self::$InventoryItem->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) );
/*      if ( !self::$IsProposedInventoryItem ) {
        $this->setDefault( InventoryItemPeer::STATUS, self::$InventoryItem->getStatus() );        
        $this->setDefault( InventoryItemPeer::COMMITTED_COLLEGE_ID, self::$InventoryItem->getCommittedCollegeId() );
        $this->setDefault( InventoryItemPeer::COMMITTED_DATE, self::$InventoryItem->getCommittedDate( sfConfig::get('app_application_date_format' ) ) );
      } */

    }
    // validators :
    $ValidatorArray= array(
    InventoryItemPeer::ID => new sfValidatorString ( array( 'required' => false), array( ) ),

    InventoryItemPeer::TITLE => new sfValidatorString( array('required' => true), array('required' => 'Title can not be empty.') ),
    InventoryItemPeer::SKU => new sfValidatorString( array('required' => true), array('required' => 'Last name can not be empty.') ),
    
    InventoryItemPeer::BRAND_ID => new sfValidatorChoice(array('choices' => array_keys($BrandsList),'required' => true), array('required'   => 'Brand can not be empty.')),    
    /*InventoryItemPeer::RANKING => new sfValidatorChoice(array('choices' => array_keys($RankingList),'required' => true), array('required'   => 'Ranking can not be empty.')),    
    InventoryItemPeer::SCHOOL => new sfValidatorString( array('required' => true), array('required' => 'School can not be empty.') ),
    InventoryItemPeer::GRADUATION_YEAR => new sfValidatorChoice(array('choices' => array_keys($GraduationYearList),'required' => true), array('required'   => 'Graduation Year can not be empty.')),    
    
    InventoryItemPeer::STATE => new sfValidatorChoice(array('choices' => array_keys($StateList),'required' => true), array('required'   => 'State can not be empty.')),    
    InventoryItemPeer::CITY => new sfValidatorString( array('required' => true), array('required' => 'City can not be empty.') ),

    InventoryItemPeer::HEIGHT => new sfValidatorString( array('required' => true), array('required' => 'Height can not be empty.') ),
    InventoryItemPeer::WEIGHT => new sfValidatorString( array('required' => true), array('required' => 'Weight can not be empty.') ), */
    
    InventoryItemPeer::CREATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),
    InventoryItemPeer::UPDATED_AT  => new sfValidatorString ( array( 'required' => false), array( ) ),

    // 'recruit_colleges_interest' => new sfValidatorString( array('required' => false), array('required' => "Colleges of interest must be selected." ) )
    );
    /*if ( !self::$IsProposedInventoryItem ) {
      $ValidatorArray[InventoryItemPeer::STATUS]= new sfValidatorChoice(array('choices' => array_keys($StatusList),'required' => true), array('required'   => 'Status can not be empty.'));
      $ValidatorArray[InventoryItemPeer::COMMITTED_COLLEGE_ID]= new sfValidatorChoice(array('choices' => array_keys($CollegesList),'required' => false), array('required'   => 'Committed College can not be empty.'));
      $ValidatorArray[InventoryItemPeer::COMMITTED_DATE]= new sfValidatorString ( array( 'required' => false), array( ) );
    } */
    $this->setValidators( $ValidatorArray );

    // post validators :
    //$this->validatorSchema->setPostValidator(
    // new sfValidatorCallback(array('callback' => array($this, 'checkStatusFilled')))  ); 

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }



  /**
         * Validate Unique Title
         *
         * @param unknown_type $pValidator
         * @param unknown_type $pValues
         * @return unknown
         */
  public function checkStatusFilled($pValidator, $pValues)
  {
    $lError = array();
    $lHelpers = array();
    if ( empty( $pValues[InventoryItemPeer::STATUS] ) ) return $pValues;
    $Status= $pValues[InventoryItemPeer::STATUS];
    //$CollegesInterest= $pValues[InventoryItemPeer::COLLEGES_INTEREST];
    $CommittedCollegeId= $pValues[InventoryItemPeer::COMMITTED_COLLEGE_ID];

    //  public static $StatusArray = array( "NC"=>"No Commitment","VC"=>"Verbal Commitment", "CT"=>"Commited" );
    /*if ( $Status == "VC" and empty($CollegesInterest) ) { 
      $lHelpers[InventoryItemPeer::COLLEGES_INTEREST] = 'InventoryItem with Verbal Commitment status must have Colleges Interest filled.<br />';
      $lError [] = 'InventoryItem with Verbal Commitment status must have Colleges Interest filled.';        
    }  */
    
    if ( $Status == "CT" and empty($CommittedCollegeId) ) { 
      $lHelpers[InventoryItemPeer::FIRST_NAME] = 'Inventory Item with Commitment status must have Committed CollegeId selected.<br />';
      $lError [] = 'Inventory Item with Verbal Commitment status must have Committed CollegeId selected.';        
    }

    if(!empty($lError))
    {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr)
      {
        $lErrorMessage .= $lErr.'<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }


}