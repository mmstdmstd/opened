<?php

class ShippingForm extends sfForm {
  //public static $InventoryItem;

  /**
   * Configure form fields
   */
  public static $lUserShippingInfo;

  public function configure() {
    $StateList = Util::SetArrayHeader(array('' => '  -Select State-  '), AppUtils::getListOfStatesOfUSA(false, true));
    $CList = Util::SetArrayHeader(array('' => '  -Select Country-  '), Util::getCountriesList(sfContext::getInstance() /* ,$AddEmptyCountry=false,$type=false */));
    $CountriesList = array();
    ;
    foreach ($CList as $key => $value) {
      if ($key != 'US')
	continue;
      $CountriesList[$key] = $value;
    }

    $WidgetsArray = array(
	'name' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'address' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'address_2' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'city' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 30)),
	'state' => new sfWidgetFormSelect(array('choices' => $StateList), array()),
	'zip' => new sfWidgetFormInput(array(), array('size' => 6, 'maxlength' => 6)),
	'country' => new sfWidgetFormSelect(array('choices' => $CountriesList), array()),
	'is_billing_address' => new sfWidgetFormInputCheckbox(array(), array('onclick' => 'javascript:isBillingAddressChanged()', 'value' => 1)),
	'b_name' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'b_address' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'b_address_2' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 50)),
	'b_city' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 30)),
	'b_state' => new sfWidgetFormSelect(array('choices' => $StateList), array()),
	'b_zip' => new sfWidgetFormInput(array(), array('size' => 6, 'maxlength' => 6)),
	'b_country' => new sfWidgetFormSelect(array('choices' => $CountriesList), array()),
	'b_phone' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50)),
	'b_is_department_purchase' => new sfWidgetFormInputCheckbox(array(), array('value' => 1)),
	'password' => new sfWidgetFormInputPassword(array(), array()),
	'password_2' => new sfWidgetFormInputPassword(array(), array()),
    );
    $this->setWidgets($WidgetsArray);

    // form labels
    $this->widgetSchema->setLabel('name', 'Name');
    $this->widgetSchema->setLabel('address', 'Address');
    $this->widgetSchema->setLabel('address_2', '');
    $this->widgetSchema->setLabel('city', 'City');
    $this->widgetSchema->setLabel('state', 'State');
    $this->widgetSchema->setLabel('zip', 'Zip Code');
    $this->widgetSchema->setLabel('country', 'Country');

    $this->widgetSchema->setLabel('is_billing_address', 'Same as shipping address');
    $this->widgetSchema->setLabel('b_name', 'Name');
    $this->widgetSchema->setLabel('b_address', 'Address');
    $this->widgetSchema->setLabel('b_address_2', '');
    $this->widgetSchema->setLabel('b_city', 'City');
    $this->widgetSchema->setLabel('b_state', 'State');
    $this->widgetSchema->setLabel('b_zip', 'Zip Code');
    $this->widgetSchema->setLabel('b_country', 'Country');
    $this->widgetSchema->setLabel('b_phone', 'Phone');
    $this->widgetSchema->setLabel('b_is_department_purchase', 'This is a department purchase (tax-exempt)');

    $this->widgetSchema->setLabel('password', 'create a password');
    $this->widgetSchema->setLabel('password_2', 're-type password');

    $this->widgetSchema->setNameFormat('shipping[%s]');

    $CheckoutArray = Cart::getShippingInfo();
    // Util::deb( $CheckoutArray, '$CheckoutArray::' );
    if (!empty($CheckoutArray['name'])) {
  // Util::deb( AppUtils::getStateNameByAbbr($CheckoutArray['state']), 'AppUtils::getStateNameByAbbr($CheckoutArray[state])::' );
      $this->setDefault('name', $CheckoutArray['name']);
      $this->setDefault('address', $CheckoutArray['address']);
      $this->setDefault('address_2', $CheckoutArray['address_2']);
      $this->setDefault('city', $CheckoutArray['city']);
      $this->setDefault('state', /*AppUtils::getStateNameByAbbr( */$CheckoutArray['state'] /*)*/ );
      $this->setDefault('zip', $CheckoutArray['zip']);
      $this->setDefault('country', $CheckoutArray['country']);
      $this->setDefault('is_billing_address', $CheckoutArray['is_billing_address']);
      $this->setDefault('b_name', $CheckoutArray['b_name']);
      $this->setDefault('b_address', $CheckoutArray['b_address']);
      $this->setDefault('b_address_2', $CheckoutArray['b_address_2']);
      $this->setDefault('b_city', $CheckoutArray['b_city']);
      $this->setDefault('b_state', /*AppUtils::getStateNameByAbbr(*/ $CheckoutArray['b_state'] /*)*/ );
      $this->setDefault('b_zip', $CheckoutArray['b_zip']);
      $this->setDefault('b_country', $CheckoutArray['b_country']);
      if (!empty($CheckoutArray['b_phone']))
	$this->setDefault('b_phone', $CheckoutArray['b_phone']);
      if (!empty($CheckoutArray['b_is_department_purchase'])) {
	$this->setDefault('b_is_department_purchase', $CheckoutArray['b_is_department_purchase']);
      }
    }  //if( !empty($CheckoutArray['name']) ) {
    else {
      if (self::$lUserShippingInfo instanceof UserShippingInfo) {
	//$lLoggedsfGuardUser= sfGuardUserPeer::retrieveByUsername($CheckoutArray['LoggedUserEmail']);
	//if ( !empty($lLoggedsfGuardUser) ) {
//    			$this->setDefault( 'name', $lLoggedsfGuardUser->getName() );
//  			  $this->setDefault( 'b_name', $lLoggedsfGuardUser->getName() );

	$this->setDefault('name', self::$lUserShippingInfo->getSName());
	$this->setDefault('b_name', self::$lUserShippingInfo->getBName());

     Util::deb( self::$lUserShippingInfo->getSState(), 'self::$lUserShippingInfo->getSState()::' );
     Util::deb( AppUtils::getStateNameByAbbr( self::$lUserShippingInfo->getSState()), 'AppUtils::getStateNameByAbbr( self::$lUserShippingInfo->getSState()::' );

	//}
	$this->setDefault('address', self::$lUserShippingInfo->getSStreet());
	$this->setDefault('address_2', self::$lUserShippingInfo->getSStreet2());
	$this->setDefault('city', self::$lUserShippingInfo->getSCity());
	$this->setDefault('state', AppUtils::getStateAbbrByName( self::$lUserShippingInfo->getSState()) );
	$this->setDefault('zip', self::$lUserShippingInfo->getSZip());
	$this->setDefault('country', self::$lUserShippingInfo->getSCountry());
	$this->setDefault('b_address', self::$lUserShippingInfo->getBStreet());
	$this->setDefault('b_address_2', self::$lUserShippingInfo->getBStreet2());
	$this->setDefault('b_city', self::$lUserShippingInfo->getBCity());
	$this->setDefault('b_state', AppUtils::getStateAbbrByName( self::$lUserShippingInfo->getBState()) );
	$this->setDefault('b_zip', self::$lUserShippingInfo->getBZip());
	$this->setDefault('b_country', self::$lUserShippingInfo->getBCountry());
	$this->setDefault('b_phone', self::$lUserShippingInfo->getBPhone());
      }
    }

    // validators :
    $ValidatorArray = array(
	'name' => new sfValidatorString(array('required' => true), array('required' => 'Name can not be empty.')),
	'address' => new sfValidatorString(array('required' => true), array('required' => 'Address can not be empty.')),
	'address_2' => new sfValidatorString(array('required' => false), array('required' => 'Address can not be empty.')),
	'city' => new sfValidatorString(array('required' => true), array('required' => 'City can not be empty.')),
	'state' => new sfValidatorString(array('required' => true), array('required' => 'State can not be empty.')),
	'zip' => new sfValidatorString(array('required' => true), array('required' => 'Zip can not be empty.')),
	'country' => new sfValidatorChoice(array('choices' => array_keys($CountriesList), 'required' => true), array('required' => 'Country can not be empty.')),
	'is_billing_address' => new sfValidatorString(array('required' => false), array('required' => '')),
	'b_name' => new sfValidatorString(array('required' => true), array('required' => 'Billing Name can not be empty.')),
	'b_address' => new sfValidatorString(array('required' => true), array('required' => 'Billing Address can not be empty.')),
	'b_address_2' => new sfValidatorString(array('required' => false), array('required' => 'Billing Address can not be empty.')),
	'b_city' => new sfValidatorString(array('required' => true), array('required' => 'City can not be empty.')),
	'b_state' => new sfValidatorString(array('required' => true), array('required' => 'State can not be empty.')),
	'b_zip' => new sfValidatorString(array('required' => true), array('required' => 'Zip can not be empty.')),
	'b_country' => new sfValidatorChoice(array('choices' => array_keys($CountriesList), 'required' => true), array('required' => 'Country can not be empty.')),
	'b_phone' => new sfValidatorString(array('required' => true), array('required' => 'Phone can not be empty.')),
	'b_is_department_purchase' => new sfValidatorString(array('required' => false), array('required' => '')),
	'password' => new sfValidatorString(array('required' => false), array('required' => 'Password can not be empty.')),
	'password_2' => new sfValidatorString(array('required' => false), array('required' => 'Confirm Password can not be empty.')),
    );
    $this->setValidators($ValidatorArray);

    // post validators :
    $this->validatorSchema->setPostValidator(
	    new sfValidatorCallback(array('callback' => array($this, 'checkShipping'))));

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

  /**
   * Validate Unique Title
   *
   * @param unknown_type $pValidator
   * @param unknown_type $pValues
   * @return unknown
   */
  public function checkShipping($pValidator, $pValues) {
    $lError = array();
    $lHelpers = array();
    $is_billing_address = $pValues['is_billing_address'];
    $password = trim($pValues['password']);
    $password_2 = trim($pValues['password_2']);
    /* if ( $is_billing_address ) {
      if ( empty($pValues['b_name']) ) {
      $lHelpers['b_name'] = 'Billing name can not be empty.<br />';
      $lError [] = 'Billing name can not be empty.';
      }
      if ( empty($pValues['b_address']) ) {
      $lHelpers['b_address'] = 'Billing address can not be empty.<br />';
      $lError [] = 'Billing address can not be empty.';
      }
      if ( empty($pValues['b_city']) ) {
      $lHelpers['b_city'] = 'Billing city can not be empty.<br />';
      $lError [] = 'Billing city can not be empty.';
      }
      if ( empty($pValues['b_state']) ) {
      $lHelpers['b_state'] = 'Billing state can not be empty.<br />';
      $lError [] = 'Billing state can not be empty.';
      }
      if ( empty($pValues['b_zip']) ) {
      $lHelpers['b_zip'] = 'Billing zip can not be empty.<br />';
      $lError [] = 'Billing zip can not be empty.';
      }
      if ( empty($pValues['b_country']) ) {
      $lHelpers['b_country'] = 'Billing country can not be empty.<br />';
      $lError [] = 'Billing country can not be empty.';
      }
      if ( empty($pValues['b_phone']) ) {
      $lHelpers['b_phone'] = 'Billing phone can not be empty.<br />';
      $lError [] = 'Billing phone can not be empty.';
      }
      } */

    if (!empty($password) or !empty($password_2)) {
      if ($password != $password_2) {
	$lHelpers['password'] = 'Password must match confirm password.<br />';
	$lError [] = 'Password must match confirm password.';
      }
      $GuestUserArray = Cart::getGuestUser();
      if (!empty($GuestUserArray['LoggedUserEmail'])) {
	/*$lsfGuardUser = sfGuardUserPeer::getByUsername($GuestUserArray['LoggedUserEmail']);
	if (!empty($lsfGuardUser)) {
	  $lHelpers['b_country'] = 'There is already user with this email.<br />';
	  $lError [] = 'There is already user with this email.';
	} */
      }
    }


    if (!empty($lError)) {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr) {
	$lErrorMessage .= $lErr . '<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }

}