<?php

class FirearmForm extends sfForm {

  /**
   * Configure form fields
   */
  public function configure() {

    $StateList = Util::SetArrayHeader(array('' => ''), AppUtils::getListOfStatesOfUSA(true));
    /*$CurrentEcwsList = array('' => ' -Select- ', '1-2 years' => '1-2 years', '2-3 years' => '2-3 years', '3-4 years' => '3-4 years', '4 and older' => '4 and older', 'Not applicable' => 'Not applicable');
    $PlanToReplaceEcwsList = array('' => ' -Select- ', '1-3 months' => '1-3 months', '3-6 months' => '3-6 months', '6 or more months' => '6 or more months', 'Not yet determined' => 'Not yet determined', 'Not applicable' => 'Not applicable');
    $HaveBudgetEcwsList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No');
    $IsYourAgencyList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No');
    $PlanToAcquireCameraList = array('' => ' -Select- ', '1-3 months' => '1-3 months', '3-6 months' => '3-6 months', '6 or more months' => '6 or more months', 'Not yet determined' => 'Not yet determined', 'Not applicable' => 'Not applicable');
    $HaveBudgetAllocatedForCameraList = array('' => ' -Select- ', 'Yes' => 'Yes', 'No' => 'No'); */
    $WidgetsArray = array(
        'first_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>1 )),
        'last_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>2 )),
        'department_name' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>3 )),
        'department_person' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>4 )),
        'email' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>5 )),
        'phone' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>6 )),
        'address' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'class'=>"te-form-input", 'tabindex'=>7 )),
        'city' => new sfWidgetFormInput(array(), array('size' => 20, 'maxlength' => 30, 'class'=>"te-form-input", 'tabindex'=>8 )),
        'state' => new sfWidgetFormSelect(array('choices' => $StateList), array('class'=>"te-form-select", 'tabindex'=>9 )),
        'zip' => new sfWidgetFormInput(array(), array('size' => 6, 'maxlength' => 6, 'style'=>"display:block;margin-top:0px;width:75px;position:relative;top:-22px;left:165px; ", 'tabindex'=>10 )),
        'firearms_model' => new sfWidgetFormInput(array(), array('size' => 50, 'maxlength' => 100, 'style'=>"position:relative; left:66px; top:11px; display:block;", 'tabindex'=>11 )),
        'additional_comments' => new sfWidgetFormTextarea(array(), array('cols' => 50, 'rows' => 8, 'tabindex'=>12, 'style'=>'font-family:arial, sans-serif; font-size:12px; font-weight:normal; text-decoration:none;' )),
    );
    $this->setWidgets($WidgetsArray);

    // form labels
    $this->widgetSchema->setLabel('first_name', 'First Name');
    $this->widgetSchema->setLabel('last_name', 'Last Name');
    $this->widgetSchema->setLabel('department_name', 'Department Name');
    $this->widgetSchema->setLabel('department_person', 'Contact Person for Firearms in Department');
    $this->widgetSchema->setLabel('email', 'Email');
    $this->widgetSchema->setLabel('phone', 'Phone');
    $this->widgetSchema->setLabel('address', 'Address');
    $this->widgetSchema->setLabel('city', 'City');
    $this->widgetSchema->setLabel('state', 'State');
    $this->widgetSchema->setLabel('zip', 'Zip Code');

    $this->widgetSchema->setLabel('firearms_model', 'What make/model of of firearm(s) are you interested in ?');
    $this->widgetSchema->setLabel('additional_comments', 'Additional Comments');

    $this->widgetSchema->setNameFormat('firearm[%s]');

    // validators :
    $ValidatorArray = array(
        'first_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'last_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'department_name' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'department_person' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'email' => new sfValidatorEmail(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.', 'invalid' => 'E-mail&nbsp;is&nbsp;invalid.')),
        'phone' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'address' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'city' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'state' => new sfValidatorChoice(array('choices' => array_keys($StateList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'zip' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'firearms_model' => new sfValidatorString(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
        'additional_comments' => new sfValidatorString(array('required' => false), array()),
    );
    $this->setValidators($ValidatorArray);

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

}