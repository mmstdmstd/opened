<?php
class InventoryItemFilterForm extends sfForm
{
  /**
	 * Configure form fields
	 */
  public function configure()
  {
    // form fields :
    $BrandsList= Util::SetArrayHeader( array( ''=>'  -All Brands-  '), BrandPeer::getSelectionList() ) ;
Util::deb( $BrandsList, ' $BrandsList::' );
    $this->setWidgets(array(
    'filter_title' => new sfWidgetFormInput( array(),array('size'=>20,'maxlength'=>100, 'class'=>'text') ),
    'filter_sku' => new sfWidgetFormInput( array(),array('size'=>20,'maxlength'=>30, 'class'=>'text') ),
    //'filter_brand_id' => new sfWidgetFormChoice( array('choices'=>$BrandsList, 'multiple' => false ), array('size'=>1) ) ,
    'filter_qty_on_hand_min' => new sfWidgetFormInput( array(),array('size'=>10,'maxlength'=>10, 'class'=>'text') ),
    'filter_qty_on_hand_max' => new sfWidgetFormInput( array(),array('size'=>10,'maxlength'=>10, 'class'=>'text') ),    
    'filter_std_unit_price_min' => new sfWidgetFormInput( array(),array('size'=>10,'maxlength'=>10, 'class'=>'text') ),
    'filter_std_unit_price_max' => new sfWidgetFormInput( array(),array('size'=>10,'maxlength'=>10, 'class'=>'text') ),
    ));

    // form labels      
    $this->widgetSchema->setLabel( 'filter_title', 'Title' );
    $this->widgetSchema->setLabel( 'filter_sku', 'Sku' );
    $this->widgetSchema->setLabel( 'filter_brand_id', 'Brand' );    
    $this->widgetSchema->setLabel( 'filter_qty_on_hand_min', 'Qty On Hand From' );
    $this->widgetSchema->setLabel( 'filter_qty_on_hand_max', 'Qty On Hand Till' );
    $this->widgetSchema->setLabel( 'filter_std_unit_price_min', 'Std Unit Price From' );
    $this->widgetSchema->setLabel( 'filter_std_unit_price_max', 'Std Unit Price Till' );
    
    $this->widgetSchema->setNameFormat('inventory_item[%s]');

    // validators :
    $this->setValidators(array(
    'filter_title' => new sfValidatorString (   array( 'required' => false), array( )   ),
    'filter_sku' => new sfValidatorString (   array( 'required' => false), array( )   ),
    'filter_brand_id' => new sfValidatorChoice(array('choices' => array_keys($BrandsList),'required' => false), array()),
    
    'filter_qty_on_hand_min' => new sfValidatorNumber(  array( 'required' => false), array( )   ),
    'filter_qty_on_hand_max' => new sfValidatorNumber(  array( 'required' => false), array( )   ),
    'filter_std_unit_price_min' => new sfValidatorNumber(  array( 'required' => false), array( )   ),
    'filter_std_unit_price_max' => new sfValidatorNumber(  array( 'required' => false), array( )   ),
    ) );

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }

}

?>
