<?php

class ResetPasswordForm extends sfForm {

  /**
   * Configure form fields
   */
  public function configure() {

    $WidgetsArray = array(
      'email' => new sfWidgetFormInput(array(), array('size' => 30, 'maxlength' => 50, 'style'=>"position:relative; float:left; width:200px;  display:inline-block;" )),
    );
    $this->setWidgets($WidgetsArray);

    // form labels
    $this->widgetSchema->setLabel('email', 'Email');

    $this->widgetSchema->setNameFormat('reset_password[%s]');

    // validators :
    $ValidatorArray = array(
        'email' => new sfValidatorEmail(array('required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.', 'invalid' => 'E-mail&nbsp;is&nbsp;invalid.')),
    );
    $this->setValidators($ValidatorArray);
    // post validators :
    $this->validatorSchema->setPostValidator(
	    new sfValidatorCallback(array('callback' => array($this, 'ValidateUser'))));

    // set render form schemas :
    $this->widgetSchema->setFormFormatterName('table');
  }


  public function ValidateUser($pValidator, $pValues) {
    $lError = array();
    $lHelpers = array();
    $email = trim($pValues['email']);
     $lsfGuardUser= sfGuardUserPeer::getByUsername( $email );

    if (empty($lsfGuardUser)) {
    	$lHelpers['password'] = 'There is no user with this email.<br />';
	    $lError [] = 'There is no user with this email.';
    }

    if (!empty($lError)) {
      $this->getWidgetSchema()->setHelps($lHelpers);
      $lErrorMessage = '';
      foreach ($lError as $lErr) {
	      $lErrorMessage .= $lErr . '<br />';
      }
      throw new sfValidatorError($pValidator, $lErrorMessage);
    }
    return $pValues;
  }

}