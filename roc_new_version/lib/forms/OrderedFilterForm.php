<?php
class OrderedFilterForm extends sfForm
{
	/**
	 * Configure form fields
	 */
	public function configure()
	{  // 4.That is a good idea! Let's add a basic filter. I will detail it better in the task description. Filter the following fields "Status", "Payment Method",  
	//	"State", and "Date Range". I will put together a quick mock-up, not sure if you need it, though basic programming will.

		// form fields :
		$StatusList= Util::SetArrayHeader( array( ''=>'  -All-  '), OrderedPeer::$StatusChoices );
		$PaymentMethodList= Util::SetArrayHeader( array( ''=>'  -All-  '), sfConfig::get('app_application_payment_methods') );
		$StateList= Util::SetArrayHeader( array( ''=>'  -All-  '), AppUtils::getListOfStatesOfUSA(false, false) );
		$HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );

		$this->setWidgets(array(
			'filter_payment_method' => new sfWidgetFormSelect( array('choices'=>$PaymentMethodList), array('class'=>'text') ),
			'filter_status' => new sfWidgetFormSelect( array('choices'=>$StatusList), array('class'=>'text') ),
			'filter_date_from' => new sfWidgetFormDateJQueryUI( array( "change_month" => true, "change_year" => true, 'culture'=>sfContext::getInstance()->getUser()->getCulture() ), array('size'=>10, 'maxlength'=>10 ) ),
			'filter_date_till' => new sfWidgetFormDateJQueryUI( array( "change_month" => true, "change_year" => true, 'culture'=>sfContext::getInstance()->getUser()->getCulture() ), array('size'=>10, 'maxlength'=>10 ) ),
			'filter_state' => new sfWidgetFormSelect( array('choices'=>$StateList), array('class'=>'text') ),
		));

		// form labels
		$this->widgetSchema->setLabel( 'filter_payment_method', 'Payment Method' );
		$this->widgetSchema->setLabel( 'filter_status', 'Status' );
		$this->widgetSchema->setLabel( 'filter_date_from', 'Date' );
		$this->widgetSchema->setLabel( 'filter_date_till', 'Date' );
		$this->widgetSchema->setLabel( 'filter_state', 'State' );
		$this->widgetSchema->setNameFormat('ordered[%s]');





		// validators :
		$this->setValidators(array(
			'filter_payment_method' => new sfValidatorChoice(array('choices' => array_keys($PaymentMethodList),'required' => false), array()),
			'filter_status' => new sfValidatorChoice(array('choices' => array_keys($StatusList),'required' => false), array()),
			'filter_date_from' => new sfValidatorString( array('required' => false), array('required' => 'Date From can not be empty.') ),
			'filter_date_till' => new sfValidatorString( array('required' => false), array('required' => 'Date Till can not be empty.') ),
			'filter_state' => new sfValidatorChoice(array('choices' => array_keys($StateList),'required' => false), array())
		) );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}

}