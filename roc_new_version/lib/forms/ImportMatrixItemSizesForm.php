<?php
class ImportMatrixItemSizesForm extends sfForm
{
	/**
	 * Configure form fields
	 */
	public function configure()
	{
		// form fields :

		$DeleteExistingRowList= array( 'D'=> 'Delete Existing Rows', 'L'=> 'Leave Existing Rows' );
		$this->setWidgets(array(
			'xls_file' => new sfWidgetFormInputFile( array(), array( 'size'=>30,'maxlength'=>100, 'class'=>'text' ) ),
			'delete_existing_row' => new sfWidgetFormSelect(array('choices' => $DeleteExistingRowList), array('class'=>"te-form-select", 'tabindex'=>9 )),
		));

		// form labels
		$this->widgetSchema->setLabel( 'xls_file', 'Select xls file for Matrix Item Sizes importing' );
		$this->widgetSchema->setLabel('delete_existing_row', 'Delete Existing Row');


		$this->widgetSchema->setNameFormat('import_matrix_item_sizes[%s]');
		// validators :
		$this->setValidators(  array(
			'xls_file' => new sfValidatorFile(  array('required' => true ), array('required' => "Select xls file." ) ),
			'delete_existing_row' => new sfValidatorChoice(array('choices' => array_keys($DeleteExistingRowList), 'required' => true), array('required' => '&lt;&nbsp;required&nbsp;field.')),
		)  );

		// post validators :
		$this->validatorSchema->setPostValidator(
			new sfValidatorCallback(array('callback' => array($this, 'checkFileType')))  );

		// set render form schemas :
		$this->widgetSchema->setFormFormatterName('table');
	}



	/**
	 * @param unknown_type $pValidator
	 * @param unknown_type $pValues
	 * @return unknown
	 */
	public function checkFileType($pValidator, $pValues)
	{
		$lError = array();
		$lHelpers = array();
		if ( empty( $pValues['xls_file'] ) ) return $pValues;

		$xls_file= $pValues['xls_file'];

		$Ext= Util::GetFileNameExt($xls_file->getOriginalName());
		if ( strtolower($Ext) == "xlsx" ) {
			$lHelpers['xls_file'] = 'Please, do a "Save As" to get it into the xls format before uploading it.<br />';
			$lError [] = 'Please, do a "Save As" to get it into the xls format before uploading it.';
		}

		if ( strtolower($Ext) == "xlsx" ) {
			$lHelpers['xls_file'] = 'Please, select xls format for uploading.<br />';
			$lError [] = 'Please, select xls format for uploading.';
		}
		if(!empty($lError))
		{
			$this->getWidgetSchema()->setHelps($lHelpers);
			$lErrorMessage = '';
			foreach ($lError as $lErr)
			{
				$lErrorMessage .= $lErr.'<br />';
			}
			throw new sfValidatorError($pValidator, $lErrorMessage);
		}
		return $pValues;
	}



}