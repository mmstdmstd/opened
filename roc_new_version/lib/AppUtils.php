<?php

class AppUtils {

  public static function getYuotubeKey($Url) {
    $A= preg_split( '/\/embed\//' , $Url );
    if ( empty($A[1]) ) return '';
    $Key= $A[1];
    $A= preg_split( '/\?/' , $Key );
    if ( empty($A[1]) ) return $Key;
    return $A[0];

  }

  public static function getShippingTaxDataCurrentValue( $shipping_tax_data ) {
    if ( empty( $shipping_tax_data['est_shipping'] ) ) return '';
    if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_ground'] ) return 'UPS-Ground';
    if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_3_day_selected'] ) return 'UPS-3Day';
    if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_2_day_air'] ) return 'UPS-2Day';
    if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_next_day_air_saver'] ) return 'UPS-NextDay';
    return '';
  }

  public static function getShippingTaxDataLowlabel($key,$shipping_tax_data) {
    $Keys= array( 'UPS-Ground' => 'ups_ground',
    'UPS-3Day' =>'ups_3_day_selected',
    'UPS-2Day' => 'ups_2_day_air',
    'UPS-NextDay' => 'ups_next_day_air_saver' );
    if ( empty($Keys[$key]) ) return '';
    return $Keys[$key];
  }

  public static function getShippingTaxDataHighlabel($key,$shipping_tax_data) {
    $Keys= array( 'ups_ground' => 'UPS-Ground',
    'ups_3_day_selected' =>'UPS-3Day',
    'ups_2_day_air' => 'UPS-2Day',
    'ups_next_day_air_saver' => 'UPS-NextDay' );
    if ( empty($Keys[$key]) ) return '';
    return $Keys[$key];
  }
  
  public static function getShippingTaxDataValue($key,$shipping_tax_data) {
    $Keys= array( 'UPS-Ground' => 'ups_ground',
    'UPS-3Day' =>'ups_3_day_selected',
    'UPS-2Day' => 'ups_2_day_air',
    'UPS-NextDay' => 'ups_next_day_air_saver' );
    if ( empty($Keys[$key]) ) return '';
    $DataKey= $Keys[$key];
    if ( empty($shipping_tax_data[$DataKey]) ) return '';
    return '- '.Util::getDigitMoney( $shipping_tax_data[$DataKey], 'Money' );
  }

  public static function getThumbnailFromServer($Sku, $InventoryItemsImage_MaxImages = '', $InventoryItemsThumbnails = '', $sf_upload_dir = '', $HostForImage = '') {
    //Util::deb( $Sku, '$sku::' );
    if (empty($InventoryItemsImage_MaxImages)) {
      $InventoryItemsImage_MaxImages = (int) sfConfig::get('app_application_InventoryItemsImage_MaxImages');
    }
    if (empty($InventoryItemsThumbnails)) {
      $InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');
    }
    if (empty($HostForImage)) {
      $HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());
    }
    $Url = $HostForImage . 'uploads/InventoryItems/thumb/' . urlencode($Sku) . '.jpg';
    $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . $Sku . '.jpg';
    if (file_exists($ImageFile))
      return $Url;

    for ($I = 1; $I <= $InventoryItemsImage_MaxImages; $I++) {  // 10003-02.jpg
      $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . $Sku . '-' . $I . '.jpg';
      if (file_exists($ImageFile)) {
        return $HostForImage . 'uploads/InventoryItems/thumb/' . urlencode($Sku) . '-' . $I . '.jpg';
      }

      $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . $Sku . '-0' . $I . '.jpg';
      if (file_exists($ImageFile)) {
        return $HostForImage . 'uploads/InventoryItems/thumb/' . urlencode($Sku) . '-0' . $I . '.jpg';
      }
    } //for( $I= 1; $I<= $InventoryItemsImage_MaxImages; $I++ ) {
    return  AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage150.png';
  }

  public static function getProductRecentViewsList($sku) {
    $recently_viewed_in_list = sfConfig::get('app_application_recently_viewed_in_list');
    $ResArray = array();
    for ($I = 1; $I <= $recently_viewed_in_list + 4; $I++) {
      $Value = sfContext::getInstance()->getRequest()->getCookie('recent_product_' . $I);
      if (!empty($Value) and trim($Value) != $sku)
        $ResArray[] = $Value;
      if (count($ResArray) == $recently_viewed_in_list)
        return $ResArray;
    }
    return $ResArray;
  }

  public static function PushProductRecentView($sku, $Minutes = 60) {
    $recently_viewed_in_list = sfConfig::get('app_application_recently_viewed_in_list');
    //Util::deb( $recently_viewed_in_list, ' $recently_viewed_in_list::' );
    // Util::deb( $sku, ' $sku::' );
    $Hours = $Minutes * 60 * 24 * 30;
    $ValuesArray = array();
    for ($I = 1; $I <= $recently_viewed_in_list + 4; $I++) {
      $NextValue = sfContext::getInstance()->getRequest()->getCookie('recent_product_' . $I);
      //Util::deb( $NextValue, ' $NextValue::' );
      if (trim($sku) != trim($NextValue)) {
        $ValuesArray[] = $NextValue;
        //sfContext::getInstance()->getResponse()->setCookie( 'recent_product_'.($I+1), $NextValue, time()+$Hours, '/' );
      }
    }
    //Util::deb( $ValuesArray, ' $ValuesArray::' );
    sfContext::getInstance()->getResponse()->setCookie('recent_product_1', $sku, time() + $Hours, '/');
    for ($I = 0; $I < count($ValuesArray); $I++) {
      sfContext::getInstance()->getResponse()->setCookie('recent_product_' . ($I + 2), $ValuesArray[$I], time() + $Hours, '/');
    }

    for ($I = count($ValuesArray); $I <= $recently_viewed_in_list + 4; $I++) {
      sfContext::getInstance()->getResponse()->setCookie('recent_product_' . ($I), "", time() + $Hours, '/');
    }
  }

  public static function getInventoryCategoryByProductLine($ProductLine, $ReturnFullPath = true) {
    $sf_web_dir = sfConfig::get('sf_web_dir');
    $ResArray = array();

    $InventoryCategoriesImages = sfConfig::get('app_application_InventoryCategoriesImages');
    $ValidUploadingImages = sfConfig::get('app_application_ValidUploadingImages');
    foreach ($ValidUploadingImages as $Ext) {

      $ImageFile = $sf_web_dir . $InventoryCategoriesImages . DIRECTORY_SEPARATOR . $ProductLine . '.' . $Ext;
      //Util::deb( $ImageFile, ' $ImageFile::' );
      if (file_exists($ImageFile)) {
        if ($ReturnFullPath) {
          //Util::deb(  ' EXISTS::' );
          return $ImageFile;
        } else {
          //Util::deb(  ' ++EXISTS::' );
          return $InventoryCategoriesImages . DIRECTORY_SEPARATOR . $ProductLine . '.' . $Ext;
        }
      }
    }
    return $ResArray;
  }

  public static function getHostForImage($currentConfiguration) {
    if (Util::isDeveloperComp()) {
      return Util::getServerHost($currentConfiguration, false, false);
    }
    //return 'http://www.adenium.net/oherron.com/web/'; // /oherron.com/web/images/slideshow/slide-5.jpg
    return 'http://www.oherron.com/';
  }

  public static function getHostForJQuery($currentConfiguration) {
    if (Util::isDeveloperComp()) {
      return '';
    }
    //return 'http://www.adenium.net'; // /oherron.com/web/images/slideshow/slide-5.jpg
    return 'http://www.oherron.com';
  }

  public static function getImportImagesBySku($Sku, $ReturnFullPath = true) {
    $sf_root_dir = sfConfig::get('sf_root_dir');
    //    Util::deb($sf_root_dir,'$sf_root_dir::');
    $ResArray = array();


    $InventoryItemsImage_MaxImages = (int) sfConfig::get('app_application_InventoryItemsImage_MaxImages');
    // Util::deb( $InventoryItemsImage_MaxImages, 'InventoryItemsImage_MaxImages::' );

    $InventoryItemsImportImages = sfConfig::get('app_application_InventoryItemsImportImages');
    // Util::deb($InventoryItemsImportImages,'$InventoryItemsImportImages::');
    $ValidUploadingImages = sfConfig::get('app_application_ValidUploadingImages');
    // Util::deb( $ValidUploadingImages, '$ValidUploadingImages::' );
    foreach ($ValidUploadingImages as $Ext) {

      for ($I = 1; $I <= $InventoryItemsImage_MaxImages; $I++) {
        $ImageFile = $sf_root_dir . $InventoryItemsImportImages . DIRECTORY_SEPARATOR . $Sku . '-' . $I . '.' . $Ext;
        //Util::deb( $ImageFile, ' $ImageFile::' );
        if (file_exists($ImageFile)) {
          if ($ReturnFullPath) {
            //Util::deb(  ' EXISTS::' );
            $ResArray[] = $ImageFile;
          } else {
            //Util::deb(  ' ++EXISTS::' );
            $ResArray[] = $InventoryItemsImportImages . DIRECTORY_SEPARATOR . $Sku . '-t.' . $Ext;
          }
        }
      } // for( $I= 1; $I<= $InventoryItemsImage_MaxImages; $I++ ) {
    }
    return $ResArray;
  }

  public static function getImportThumbnailImageBySku($Sku, $ReturnFullPath = true) {
    $sf_root_dir = sfConfig::get('sf_root_dir');
    //    Util::deb($sf_root_dir,'$sf_root_dir::');

    $InventoryItemsImportThumbnails = sfConfig::get('app_application_InventoryItemsImportThumbnails');
    //  Util::deb($InventoryItemsImportThumbnails,'$InventoryItemsImportThumbnails::');
    $ValidUploadingImages = sfConfig::get('app_application_ValidUploadingImages');
    // Util::deb( $ValidUploadingImages, '$ValidUploadingImages::' );
    foreach ($ValidUploadingImages as $Ext) {
      $ThumbnailImageFile = $sf_root_dir . $InventoryItemsImportThumbnails . DIRECTORY_SEPARATOR . $Sku . '-t.' . $Ext;
      // Util::deb( $ThumbnailImageFile, ' $ThumbnailImageFile::' );
      if (file_exists($ThumbnailImageFile)) {
        if ($ReturnFullPath) {
          // Util::deb(  ' EXISTS::' );
          return $ThumbnailImageFile;
        } else {
          // Util::deb(  ' ++EXISTS::' );
          return /* DIRECTORY_SEPARATOR. */$InventoryItemsImportThumbnails . DIRECTORY_SEPARATOR . $Sku . '-t.' . $Ext;
        }
      }
    }
    return false;
  }

  public static function getImagesBySku($Sku, $ReturnFullPath = true, $ReturnLowestIndex = false, $get_all_matrix = false, $show_video = false) {
    $sf_upload_dir = sfConfig::get('sf_upload_dir');
    $ResArray = array();

    $InventoryItemsImage_MaxImages = (int) sfConfig::get('app_application_InventoryItemsImage_MaxImages');
    // Util::deb( $InventoryItemsImage_MaxImages, 'InventoryItemsImage_MaxImages::' );
    $InventoryItems = sfConfig::get('app_application_InventoryItems');
    // Util::deb($InventoryItems,'$InventoryItems::');
    $ValidUploadingImages = sfConfig::get('app_application_ValidUploadingImages');
    // Util::deb( $ValidUploadingImages, '$ValidUploadingImages::' );

    $LowestIndex = -1;
    $LowestFullPath = '';
    $SkuArray = array($Sku);

    $VideoArray = array();
    $VideoForRow = '';
    $Product = InventoryItemPeer::getSimilarInventoryItem($Sku);
    if (!empty($Product)) {
      $NextVideo = trim($Product->getVideo());
      $VideoForRow = '';
      if (!empty($NextVideo) and empty($VideoForRow)) {
        $A = preg_split('/,/', $NextVideo);
        if (!empty($A[0])) {
          $VideoForRow = $A[0];
          $VideoArray[$Sku] = $VideoForRow;
        }
      }
    }

//		Util::deb( $get_all_matrix, ' $get_all_matrix::' );

    if ($get_all_matrix) {
      $RelatedProductsList = InventoryItemPeer::getInventoryItems(1, false, false, '', '', $Sku, '', false, '', '', '', '', '', '', '', '', 'SKU', '');
      //		Util::deb( $RelatedProductsList, ' $RelatedProductsList::' );
      //20201
      foreach ($RelatedProductsList as $RelatedProduct) {
        $SkuArray[] = $RelatedProduct->getSku();
        //			Util::deb( $RelatedProduct->getSku(), ' $RelatedProduct->getSku()::' );
        if ($show_video) {
          $NextVideo = trim($RelatedProduct->getVideo());
          $VideoForRow = '';
          if (!empty($NextVideo) and empty($VideoForRow)) {
            $A = preg_split('/,/', $NextVideo);
            //				Util::deb( $A, ' $A::' );
            if (!empty($A[0])) {
              $VideoForRow = $A[0];
            }
          }
          //Util::deb( $VideoForRow, ' -1 $VideoForRow::' );
        } // if ( $show_video ) {
        $VideoArray[$RelatedProduct->getSku()] = $VideoForRow;
      }
      // Util::deb( $SkuArray, ' $SkuArray::' );
      // die("DIE");
    } /* else {
      $SkuArray[]= $Sku;
      } */

    // Util::deb( $VideoArray, ' $VideoArray::' );
    foreach ($SkuArray as $Sku) {
      foreach ($ValidUploadingImages as $Ext) {
        $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '.' . $Ext;
        // Util::deb( $ImageFile, ' $ImageFile::' );
        if (file_exists($ImageFile)) {
          if ($ReturnFullPath) {
            $ResArray[] = $ImageFile;
            if ($ReturnLowestIndex) {
              return $ImageFile;
            }
          } else {
            $ResArray[] = $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '.' . $Ext;
            if ($ReturnLowestIndex) {
              return $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '.' . $Ext;
            }
          }
        }

        for ($I = 1; $I <= $InventoryItemsImage_MaxImages; $I++) {
          $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-' . $I . '.' . $Ext;
          // Util::deb( $ImageFile, ' $ImageFile::' );
          $IsFileExists = false;
          if (file_exists($ImageFile)) {
            $IsFileExists = true;
            if ($ReturnFullPath) {
              // Util::deb(  ' EXISTS::' );
              $ResArray[] = $ImageFile;
              //Util::deb( $LowestIndex, ' $LowestIndex::' );
              //Util::deb( $I, ' $I::' );
              if ($LowestIndex >= $I or $LowestIndex < 0) {
                $LowestIndex = $I;
                $LowestFullPath = $ImageFile;
                //Util::deb( $LowestFullPath, ' INSIDE $LowestFullPath::' );
              }
            } else {
              // Util::deb(  ' ++EXISTS::' );
              $ResArray[] = $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-' . $I . '.' . $Ext;
              if ($LowestIndex >= $I or $LowestIndex < 0) {
                $LowestIndex = $I;
                $LowestFullPath = $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-' . $I . '.' . $Ext;
              }
            }
          }
          if (!$IsFileExists) {

            $ImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-0' . $I . '.' . $Ext;
            //Util::deb( $ImageFile, ' $ImageFile::' );
            $IsFileExists = false;
            if (file_exists($ImageFile)) {
              $IsFileExists = true;
              if ($ReturnFullPath) {
                // Util::deb(  ' EXISTS::' );
                $ResArray[] = $ImageFile;
                //Util::deb( $LowestIndex, ' $LowestIndex::' );
                //Util::deb( $I, ' $I::' );
                if ($LowestIndex >= $I or $LowestIndex < 0) {
                  $LowestIndex = $I;
                  $LowestFullPath = $ImageFile;
                  //Util::deb( $LowestFullPath, ' INSIDE $LowestFullPath::' );
                }
              } else {
                // Util::deb(  ' ++EXISTS::' );
                $ResArray[] = $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-0' . $I . '.' . $Ext;
                if ($LowestIndex >= $I or $LowestIndex < 0) {
                  $LowestIndex = $I;
                  $LowestFullPath = $InventoryItems . DIRECTORY_SEPARATOR . $Sku . '-0' . $I . '.' . $Ext;
                }
              }
            }
          }
        } // for( $I= 1; $I<= $InventoryItemsImage_MaxImages; $I++ ) {
      } // foreach( $ValidUploadingImages as $Ext ) {

      if (!empty($VideoArray[$Sku])) {
        $ResArray[] = 'video-' . $VideoArray[$Sku];
      }
    } // foreach( $SkuArray as $Sku ) {


    if ($ReturnLowestIndex) {
      return $LowestFullPath;
    }
    return $ResArray;
  }

  public static function getThumbnailImageBySku($Sku, $ReturnFullPath = true) {
    $sf_upload_dir = sfConfig::get('sf_upload_dir');
    //Util::deb($sf_upload_dir,'$sf_upload_dir::');

    $InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');
    //Util::deb($InventoryItemsThumbnails,'$InventoryItemsThumbnails::');
    $ValidUploadingImages = sfConfig::get('app_application_ValidUploadingImages');
    //Util::deb( $ValidUploadingImages, '$ValidUploadingImages::' );
    foreach ($ValidUploadingImages as $Ext) {
      $ThumbnailImageFile = $sf_upload_dir . DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . $Sku . '-t.' . $Ext;
      //Util::deb( $ThumbnailImageFile, ' $ThumbnailImageFile::' );
      if (file_exists($ThumbnailImageFile)) {
        if ($ReturnFullPath) {
          Util::deb(  ' EXISTS::' );
          return $ThumbnailImageFile;
        } else {
          return DIRECTORY_SEPARATOR . $InventoryItemsThumbnails . DIRECTORY_SEPARATOR . $Sku . '-t.' . $Ext;
        }
      }
    }
    return false;
  }

  //	$ThumbnailImageFile=  AppUtils::getResizedImageBySku($RecentlyViewedSku, false, false, false);
  public static function getResizedImageBySku($Sku, $ReturnFullPath = true, $getThumbnailDirectory = false, $ToThumbnailSize = false) { // AppUtils::getResizedImageBySku($Sku, false, true, true);
    $sf_upload_dir = sfConfig::get('sf_upload_dir');
    //Util::deb( $Sku, 'getResizedImageBySku $Sku::');
    //Util::deb( $getThumbnailDirectory, ' $getThumbnailDirectory::' );
    if ($getThumbnailDirectory) {
      $ImagesDirectory = sfConfig::get('app_application_InventoryItemsThumbnails');
    } else {
      $ImagesDirectory = sfConfig::get('app_application_InventoryItems');
      $ImageFileName = AppUtils::getImagesBySku($Sku, false, true);
    }
    if ($ToThumbnailSize) {
      $Width = sfConfig::get('app_application_InventoryItemsThumbnailWidth');
      $Height = sfConfig::get('app_application_InventoryItemsThumbnailHeight');
    } else {
      $Width = sfConfig::get('app_application_InventoryItemsThumbnailMoreViewsWidth');
      $Height = sfConfig::get('app_application_InventoryItemsThumbnailMoreViewsHeight');
    }

    $NewSize = Util::GetImageShowSize(DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $ImageFileName, $Width, $Height);
    if (!$ReturnFullPath) {
      $NewSize['ImageFileName'] = $ImageFileName;
      $A= preg_split(  '/\\'.DIRECTORY_SEPARATOR.'/'   , $ImageFileName);
      $L= count($A);
      $EncodedImageFileName='';
      for( $I=0; $I<$L; $I++ ) {
        if ( $I==$L-1 ) {
          $EncodedImageFileName.= urlencode($A[$I]);
        } else {
          $EncodedImageFileName.= $A[$I].DIRECTORY_SEPARATOR;
        }
      }
      $NewSize['ImageFileName_encoded'] = $EncodedImageFileName;
    }
    return $NewSize;
  }

  public static function runRawSql($sql) {
    $connection = Propel::getConnection();
    $statement = $connection->prepare($sql);
    $resultset = $statement->execute();
    return $resultset;
    ;
  }

  public static function getRawSql($sql) {
    $connection = Propel::getConnection();
    $statement = $connection->prepare($sql);
    $statement->execute();
    $resultset = $statement->fetch(PDO::FETCH_OBJ);
    //Util::deb($resultset,'$resultset::');
    return $resultset;
    ;
  }

  public static function GetWeekStartDateLimit() {
    $Year = (int) strftime('%Y');
    $Month = (int) strftime('%m');
    $Day = (int) strftime('%d');
    //Util::deb($Day,'$Day::');
    $WeekOfDay = (int) strftime('%w');
    //Util::deb($WeekOfDay,'$WeekOfDay::');
    if ($WeekOfDay > 0) {
      return Util::UnixDateTimeAddDay(mktime(0, 0, 0, $Month, $Day, $Year), -($WeekOfDay - 1));
    } else {
      return Util::UnixDateTimeAddDay(mktime(0, 0, 0, $Month, $Day, $Year), -6);
    }
  }

  public static function GetWeekEndDateLimit() {
    $Year = (int) strftime('%Y');
    $Month = (int) strftime('%m');
    $Day = (int) strftime('%d');
    //Util::deb($Day,'$Day::');
    $WeekOfDay = (int) strftime('%w');
    //Util::deb($WeekOfDay,'$WeekOfDay::');
    if ($WeekOfDay > 0) {
      return Util::UnixDateTimeAddDay(mktime(0, 0, 0, $Month, $Day, $Year), (7 - $WeekOfDay));
    } else {
      return Util::UnixDateTimeAddDay(mktime(0, 0, 0, $Month, $Day, $Year), 0);
    }
  }

  public static function getStateNameByAbbr( $state_short ) {
    $ListOfStates= AppUtils::getListOfStatesOfUSA( false, false );
    foreach( $ListOfStates as $key=>$value ) {
      if ( strtoupper($state_short) == strtoupper($value) ) return $key;
    }
    return strtoupper($state_short);
  }

	public static function getStateShortByFullName( $state_name ) {
		$ListOfStates= AppUtils::getListOfStatesOfUSA( false, false );
		foreach( $ListOfStates as $key=>$value ) {
			if ( strtoupper($state_name) == strtoupper($value) ) {
				return $key;
			}
		}
		return strtoupper($state_name);
	}

  public static function getStateAbbrByName( $state_name ) {
    $ListOfStates= AppUtils::getListOfStatesOfUSA( false, false );
    foreach( $ListOfStates as $key=>$value ) {
      if ( strtoupper($state_name) == strtoupper($key) ) return $value;
    }
    return strtoupper($state_name);
  }

  public static function getListOfStatesOfUSA( $ReturnAbbr= false, $KeysAsName= false ) {
    $StatesList= array(
        'AK' => 'Alaska',
        'AL' => 'Alabama',
        'AZ' => 'Arizona',
        'AR' => 'Arkansas',
        'CA' => 'California',
        'CO' => 'Colorado',
        'CT' => 'Connecticut',
        'DE' => 'Delaware',
        'DC' => 'District
          of Columbia',
        'FL' => 'Florida',
        'GA' => 'Georgia',
        'HI' => 'Hawaii',
        'ID' => 'Idaho',
        'IL' => 'Illinois',
        'IN' => 'Indiana',
        'IA' => 'Iowa',
        'KS' => 'Kansas',
        'KY' => 'Kentucky',
        'LA' => 'Louisiana',
        'ME' => 'Maine',
        'MD' => 'Maryland',
        'MA' => 'Massachusetts',
        'MI' => 'Michigan',
        'MN' => 'Minnesota',
        'MS' => 'Mississippi',
        'MO' => 'Missouri',
        'MT' => 'Montana',
        'NE' => 'Nebraska',
        'NV' => 'Nevada',
        'NH' => 'New Hampshire',
        'NJ' => 'New Jersey',
        'NM' => 'New Mexico',
        'NY' => 'New York',
        'NC' => 'North Carolina',
        'ND' => 'North Dacota',
        'OH' => 'Ohio',
        'OK' => 'Oklahoma',
        'OR' => 'Oregon',
        'PA' => 'Pennsylvania',
        'RI' => 'Rhode Island',
        'SC' => 'South Carolina',
        'SD' => 'South Dacota',
        'TN' => 'Tennessee',
        'TX' => 'Texas',
        'UT' => 'Utah',
        'VT' => 'Vermont',
        'VA' => 'Virginia',
        'WA' => 'Washington',
        'WV' => 'West Virginia',
        'WI' => 'Wisconsin',
        'WY' => 'Wyoming'
    );
    if ( $ReturnAbbr ) {
      $A= array();
      foreach( $StatesList as $key=>$value ) {
        $A[$key]= $key;
      }
      return $A;
    } else{
      if ( !$KeysAsName ) {
        return $StatesList;
      } else {
        $A= array();
        foreach( $StatesList as $key=>$value ) {
          $A[$value]= $value;
        }
        return $A;
      }
    }
  }

  public static function getSizeOrderedArray($DataArray) {
    $ResArray = array();
   /* $OrderedKeys = array('0 ', ' 1 ', ' 1.5 ', ' 2 ', ' 3 ', ' 3.1 ', ' 3.8 ', ' 4 ', ' 4.02 ', ' 4.02 ', ' 4.49 ', ' 4.5 ', ' 4.6 ', ' 5 ', ' 6 ', ' 6 1/4 ', ' 6.5 ', ' 6 1/2 ', ' 6 5/8 ', ' 6 3/4 ', ' 6 7/8 ', ' 7 ', ' 7 1/8 ', ' 7 1/4 ', ' 7 3/8 ', ' 7 1/2 ', ' 7.5 ', ' 7 5/8 ', ' 7 3/4 ', ' 7 7/8 ', ' 8 ', ' 8.5 ', ' 11 ', ' 11.5 ', ' 12 ', ' 12.5 ', ' 13 ', ' 13.5 ', ' 14 ', ' 14.5 ', ' 15 ', ' 15.5 ', ' 16 ', ' 16.5 ', ' 17 ', ' 17.5 ', ' 18 ', ' 18.5 ', ' 19 ', ' 19.5 ', ' 20 ', ' 20.5 ', ' 21 ', ' 21.16 ', ' 21.5 ', ' 22 ', ' 22.5 ', ' 23 ', ' 24 ', ' 24.5 ', ' 25 ', ' 26 ', ' 28 ', ' 29 ', ' 30 ', ' 31 ', ' 32 ', ' 33 ', ' 34 ', ' 35 ', ' 36 ', ' 37 ', ' 38 ', ' 40 ', ' 42 ', ' 44 ', ' 45 ', ' 46 ', ' 48 ', ' 49 ', ' 50 ', ' 51 ', ' 52 ', ' 54 ', ' 56 ', ' 57 ', ' 58 ', ' 60 ', ' 62 ', ' 66 ', ' 67 ', ' 71 ', ' 72 ', ' 73 ', ' 74 ', ' 80 ', ' 115 ', ' 223 ', ' 243 ', ' 300 ', ' 308 ', ' 357 ', ' 380 ', ' 454 ', ' 1911 ', ' 3630 ', ' 7060 ', ' 02L ', ' 02R ', ' 04R ', ' 04X30 ', ' 06R ', ' 075D ', ' 08EE ', ' 08R ', ' 08W ', ' 090D ', ' 095M ', ' 0X32 ', ' \"1 1/4\"\"\" ', ' 1 X 4 ', ' \"1\"\"\" ', ' \"1.25\"\"\" ', ' \"1.375X4.5\"\"\" ', ' \"1.375X5\"\"\" ', ' 1.5 X 108\' ', ' 1.5 X 54\' ', ' \"1.5\"\"\" ', ' \"1.5\"\" X 108\" ', ' \"1.5\"\" X 54\'\" ', ' 1.5X2 ', ' \"1.75\"\"\" ', ' \"1.75\"\"X7.5\"\"\" ', ' \"1/2\"\"\" ', ' \"1/2\"\"X2\"\"\" ', ' 10 OZ. ', ' \"10\"\"\" ', ' 10.5B ', ' 10.5C ', ' 10.5D ', ' 10.5E ', ' 10.5EE ', ' 10.5EEE ', ' 10.5EW ', ' 10.5M ', ' 10.5N ', ' 10.5R ', ' 10.5W ', ' 10.5XW ', ' 100\' ', ' 1000 FT ', ' 100EE ', ' 105EEE ', ' 105M ', ' 105W ', ' 105XW ', ' 10B ', ' 10C ', ' 10D ', ' 10E ', ' 10EE ', ' 10EEE ', ' 10EW ', ' 10GA ', ' 10L ', ' 10M ', ' 10MM ', ' 10N ', ' 10R ', ' 10W ', ' 10X12 ', ' 10XW ', ' 11.5 X 22 ', ' 11.5B ', ' 11.5C ', ' 11.5D ', ' 11.5E ', ' 11.5EE ', ' 11.5EEE ', ' 11.5EW ', ' 11.5M ', ' 11.5R ', ' 11.5W ', ' 11.5XW ', ' 110XW ', ' 115D ', ' 115M ', ' 115W ', ' 115XW ', ' 11B ', ' 11D ', ' 11E ', ' 11EE ', ' 11EEE ', ' 11EW ', ' 11M ', ' 11R ', ' 11W ', ' 11XW ', ' 12 X 20 ', ' \"12\"\"\" ', ' 12.5E ', ' 12.5X18.5 ', ' 120EEE ', ' 120M ', ' 120XW ', ' 12B ', ' 12C ', ' 12D ', ' 12E ', ' 12EE ', ' 12EW ', ' 12GA ', ' 12L ', ' 12M ', ' 12MW ', ' 12R ', ' 12W ', ' 12XW ', ' \"13.5\"\"\" ', ' 13.5X9 ', ' 130XW ', ' 13-13.5 ', ' 13-15 ', ' 13-16 ', ' 13B ', ' 13D ', ' 13E ', ' 13EE ', ' 13EEE ', ' 13EW ', ' 13M ', ' 13R ', ' 13W ', ' 13XW ', ' 14 X 21 ', ' 14 X 24 ', ' 14.5X31 ', ' 14.5X32 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X33 ', ' 14.5X34 ', ' 14.5X34 ', ' 14.5X34 ', ' 14.5X34 ', ' 14.5X34 ', ' 14.5X35 ', ' 140D ', ' 140XW ', ' 14-14.5 ', ' 14B ', ' 14C ', ' 14D ', ' 14E ', ' 14EE ', ' 14EEE ', ' 14EW ', ' 14L ', ' 14M ', ' 14R ', ' 14W ', ' 14X21X3.75 ', ' 14X32 ', ' 14X33 ', ' 14X34 ', ' 14XW ', ' 15.5 TALL ', ' 15.5T ', ' 15.5X29 ', ' 15.5X30 ', ' 15.5X31 ', ' 15.5X32 ', ' 15.5X33 ', ' 15.5X34 ', ' 15.5X35 ', ' 15.5X36 ', ' 15.5X36/37 ', ' 15.5X37 ', ' 150XW ', ' 15-15.5 ', ' 15D ', ' 15EE ', ' 15EEE ', ' 15M ', ' 15W ', ' 15X31 ', ' 15X32 ', ' 15X33 ', ' 15X34 ', ' 15X35 ', ' 15X36 ', ' 15X37 ', ' 16 OZ. ', ' 16 TALL ', ' \"16\"\"\" ', ' \"16, 21\"\"\" ', ' 16.0X32 ', ' 16.0X33 ', ' 16.0X34 ', ' 16.0X35 ', ' 16.5 TALL ', ' 16.5LB ', ' 16.5X30 ', ' 16.5X31 ', ' 16.5X32 ', ' 16.5X33 ', ' 16.5X34 ', ' 16.5X34/35 ', ' 16.5X35 ', ' 16.5X35LB ', ' 16.5X36 ', ' 16.5X36/37 ', ' 16.5X37 ', ' \"160,000 CP\" ', ' 16-16.5 ', ' 165X37 ', ' 16D ', ' 16EE ', ' 16M ', ' 16M ', ' 16R ', ' 16R ', ' 16T ', ' 16X30 ', ' 16X31 ', ' 16X32 ', ' 16X33 ', ' 16X34 ', ' 16X35 ', ' 16X36 ', ' 16X37 ', ' 17.0X32 ', ' 17.0X35 ', ' 17.5 TALL ', ' 17.5LB ', ' 17.5T ', ' 17.5X31 ', ' 17.5X32 ', ' 17.5X32 ', ' 17.5X33 ', ' 17.5X34 ', ' 17.5X35 ', ' 17.5X35LB ', ' 17.5X36 ', ' 17.5X37 ', ' 17.5X37 TL ', ' 17.5X37TAL ', ' 17.5X38 ', ' 17.5X39 ', ' 17.7 OZ ', ' 17-17.5 ', ' 175X32 ', ' 175X33 ', ' 175X34 ', ' 175X35 ', ' 17T ', ' 17X1517X16 ', ' 17X31 ', ' 17X32 ', ' 17X33 ', ' 17X34 ', ' 17X35 ', ' 17X36 ', ' 17X37 ', ' 17X39 ', ' 17X7X10 ', ' \"18\"\"\" ', ' 18.0X32 ', ' 18.0X33 ', ' 18.5 LONG ', ' 18.5 OZ ', ' 18.5 TALL ', ' 18.5 TALL ', ' 18.5LB ', ' 18.5T ', ' 18.5TALL ', ' 18.5X32 ', ' 18.5X33 ', ' 18.5X34 ', ' 18.5X35 ', ' 18.5X35T ', ' 18.5X36 ', ' 18.5X37 ', ' 18.5X37 TL ', ' 18.5X37TAL ', ' 18.5X38 ', ' 18.5X39 ', ' 18-18.5 ', ' \"18-20\"\" BBL\" ', ' 185X32 ', ' 185X33 ', ' 185X34 ', ' 185X35 ', ' 18R ', ' 18X28X10.5 ', ' 18X32 ', ' 18X33 ', ' 18X34 ', ' 18X35 ', ' 18X36 ', ' 18X36/37 ', ' 18X37 ', ' 18X38 ', ' 18X39 ', ' \"19\"\"\" ', ' 19.5 TALL ', ' 19.5LB ', ' 19.5TALL ', ' 19.5X32 ', ' 19.5X33 ', ' 19.5X34 ', ' 19.5X35 ', ' 19.5X37 ', ' 19.5X38 ', ' 19.5X39 ', ' 19.5X40 ', ' 19.5X42 ', ' 1911 10 RD ', ' \"1911 3\"\"\" ', ' \"1911 4\"\"\" ', ' \"1911 5\"\"\" ', ' 1911 CMDR ', ' 1911/M3 ', ' 1911/RAILS ', ' 1911/X300 ', ' 19-19.5 ', ' 19-20 ', ' 19T ', ' 19X32 ', ' 19X33 ', ' 19X34 ', ' 19X35 ', ' 19X36 ', ' 19X37 ', ' 19X38 ', ' 19X38LB ', ' 19X39 ', ' \"2\"\"\" ', ' \"2\"\" REV\" ', ' \"2\"\" X 4\"\"\" ', ' \"2\"\"DIA\" ', ' \"2\"\"WIDE\" ', ' \"2.25\"\"\" ', ' 20 16 8IN ', ' 20 RD MAGS ', ' 20 X 32 ', ' \"20\"\"\" ', ' 20.5 LONG ', ' 20.5 TALL ', ' 20.5X33 ', ' 20.5X37 ', ' 20.5X39 ', ' 200 FT ', ' 200B ', ' 20GA ', ' 20X32 ', ' 20X33 ', ' 20X34 ', ' 20X35 ', ' 20X36 ', ' 20X37 ', ' 20X38 ', ' 20X39 ', ' 21 TALL ', ' 21 X 24 ', ' \"21\"\"\" ', ' 21.5 TALL ', ' 21.5X35 ', ' 21.5X37 ', ' 21.5X39 ', ' 21-22 ', ' 21X34 ', ' 21X35 ', ' 21X36 ', ' 21X37 ', ' 21X39 ', ' 22.5 X 35 ', ' 22.5X37 ', ' 22.5X40 ', ' 226RDAK/M3 ', ' 22X17X8 ', ' 22X33 ', ' 22X34 ', ' 22X35 ', ' 22X36 ', ' 22X37 ', ' 22X37LB ', ' 22X38 ', ' 23.5 TALL ', ' 23X35 ', ' 23X38 ', ' 23X38 ', ' 24 X 42 ', ' 24 X 45 ', ' \"24.5\"\"X13\"\"\" ', ' 24-26 ', ' \"24-28\"\"\" ', ' 24X38 ', ' 25X12X10 ', ' \"26\"\"\" ', ' 26-28 ', ' 26-30 ', ' 26X39 ', ' 27X17 ', ' \"28\"\"\" ', ' 28-30 ', ' 28-30 ', ' 28-32 ', ' \"28-32\"\"\" ', ' 28-34 ', ' 28S ', ' 28X30 ', ' 28X32 ', ' 28X34 ', ' 28X36 ', ' 29S ', ' 2IN ', ' 2L ', ' 2X ', ' 2X/3X ', ' 2X2 ', ' 2X3 1/2 ', ' 2X4 ', ' 2XL ', ' 2XL LONG ', ' 2XL XLONG ', ' 2XL/3XL ', ' 2XL/3XL ', ' 2XLARGE ', ' 2XLL ', ' 2XLL ', ' 2XLR ', ' 2XLS ', ' 2XLT ', ' 2XLXL ', ' 3 1/2 DIA ', ' \"3 3/4\"\"DIA\" ', ' 3 IN ', ' 3 OZ ', ' \"3\"\"\" ', ' \"3\"\" REG.\" ', ' \"3\"\" X 9\"\"\" ', ' \"3\"\" XL\" ', ' \"3\"\" XLONG\" ', ' \"3\"\" XXL\" ', ' \"3\"\"DIA\" ', ' \"3\"\"X2\"\"X1\"\"\" ', ' \"3.5\"\"\" ', ' \"3.5\"\" REG.\" ', ' \"3.5\"\" XL\" ', ' \"3.5\"\"X2\"\"X1\"\"\" ', ' 3.5D ', ' 3.75X8X12D ', ' \"3/4\"\"\" ', ' \"3/8 X 1.5\"\"\" ', ' 3/8 X 2 ', ' \"3/8\"\"\" ', ' 30-06 ', ' 30-32 ', ' 30-34 ', ' 30L ', ' 30S ', ' 30X28 ', ' 30X30 ', ' 30X31 ', ' 30X32 ', ' 30X34 ', ' 30X36 ', ' 31-33 ', ' 31X30 ', ' 31X48 ', ' 32 OZ ', ' 32-34 ', ' 32-36 ', ' \"32-36\"\"\" ', ' 32L ', ' 32S ', ' 32X30 ', ' 32X32 ', ' 32X34 ', ' 32X36 ', ' \"33\"\" X 10\"\"\" ', ' 33-34 ', ' 33X30 ', ' 33X34 ', ' 34 LONG ', ' \"34\"\"\" ', ' 34-36 ', ' 34-40 ', ' 34L ', ' 34R ', ' 34S ', ' 34X29 ', ' 34X30 ', ' 34X31 ', ' 34X32 ', ' 34X32 ', ' 34X33 ', ' 34X34 ', ' 34X36 ', ' 35 X 45 ', ' 35-36 ', ' 357SIG ', ' 35X32 ', ' 35X34 ', ' 36 LONG ', ' 36 UNHEM ', ' \"36\"\"\" ', ' 36-38 ', ' 36-38R ', ' 36-40 ', ' 36L ', ' 36R ', ' 36S ', ' 36W ', ' 36X13X5 ', ' 36X30 ', ' 36X31 ', ' 36X32 ', ' 36X34 ', ' 36X36 ', ' 37-39 ', ' 37S ', ' \"38\"\"\" ', ' 380 AUTOS ', ' 38-40 ', ' 38-42 ', ' \"38-42\"\"\" ', ' 38L ', ' 38R ', ' 38S ', ' 38X30 ', ' 38X31 ', ' 38X32 ', ' 38X33 ', ' 38X34 ', ' 38X36 ', ' 3D ', ' 3X ', ' 3X/4X ', ' 3X4 ', ' 3XL ', ' 3XL ', ' 3XL LONG ', ' 3XL TALL ', ' 3XLL ', ' 3XLR ', ' 3XLR ', ' 3XLS ', ' 3XLT ', ' 4 IN ', ' 4 OZ ', ' \"4\"\"\" ', ' \"4\"\" X 11\"\"\" ', ' 4.4 OZ ', ' 4.5-8 ', ' 4.5M ', ' 4.5R ', ' 4.5W ', ' 40 S&W ', ' 40 WATT ', ' \"40\"\"\" ', ' 40-42 ', ' 40-42R ', ' 40-42S ', ' 40-46 ', ' 40L ', ' 40R ', ' 40S ', ' 40X30 ', ' 40X32 ', ' 40X33 ', ' 40X34 ', ' 40X36 ', ' 40XL ', ' 410GA ', ' 41-45R ', ' \"42\"\"\" ', ' \"42\"\"\" ', ' 42-44 ', ' 42-44 ', ' 42L ', ' 42R ', ' 42S ', ' 42X13.5X5. ', ' 42X30 ', ' 42X31 ', ' 42X32 ', ' 42X33 ', ' 42X34 ', ' 42X36 ', ' 42XL ', ' 42XXL ', ' 43-45 ', ' 44 X 1.5 ', ' 44-46 ', ' 44-46L ', ' 44-46R ', ' 44-48 ', ' \"44-48\"\"\" ', ' 44L ', ' 44R ', ' 44S ', ' 44X30 ', ' 44X32 ', ' 44X33 ', ' 44X34 ', ' 44X36 ', ' 44XL ', ' 45 ACP ', ' 45 SGL STK ', ' \"45\"\"\" ', ' 45-49R ', ' 45GAP ', ' 46 X 1.5 ', ' 46-48 ', ' 46-52 ', ' 46B ', ' 46G ', ' 46L ', ' 46R ', ' 46S ', ' 46X33 ', ' 46X34 ', ' 46X36 ', ' 46XL ', ' 46XXL ', ' \"47\"\"\" ', ' \"48\"\"X1\"\"\" ', ' \"48\"\"X1.25\"\"\" ', ' \"48\"\"X24X36\"\"\" ', ' 48-50 ', ' 48-50L ', ' 48-50R ', ' 48-52 ', ' 48G ', ' 48L ', ' 48R ', ' 48S ', ' 48X32 ', ' 48X33 ', ' 48X36 ', ' 49-53R ', ' 4D ', ' 4M ', ' 4R ', ' 4W ', ' 4X ', ' 4X/5X ', ' 4X4 ', ' 4X6X.004 ', ' 4X9 ', ' 4XL ', ' 4XL LONG ', ' 4XL TALL ', ' 4XLL ', ' 4XLR ', ' 4XLS ', ' 4XLT ', ' 5.45X39MM ', ' 5.56MM ', ' 5.5D ', ' 5.5M ', ' 5.5R ', ' 5.5W ', ' \"5.5X15.5\"\"\" ', ' 5.5XW ', ' \"5/16\"\"\" ', ' \"5/8\"\"\" ', ' 50-54 ', ' \"50-54\"\"\" ', ' 50L ', ' 50R ', ' 50S ', ' 50X34 ', ' 50XL ', ' 51X28X23 ', ' 52-54R ', ' 52-56 ', ' 52L ', ' 52R ', ' 52S ', ' 52X17.5X6 ', ' 52XL ', ' 53-57R ', ' \"54\"\"\" ', ' 54-58 ', ' 54L ', ' 54R ', ' \"56\"\"\" ', ' 56-58L ', ' 56-58R ', ' 56L ', ' 56R ', ' 58L ', ' 58R ', ' 5D ', ' 5M ', ' 5R ', ' 5W ', ' 5X8 ', ' 5XL ', ' 5XL LONG ', ' 5XL TALL ', ' 5XLL ', ' 5XLR ', ' 5XLT ', ' 5XLXL ', ' 5XW ', ' 6 1/2LO ', ' 6 1/2WO ', ' 6 1/4LO ', ' 6 1/4WO ', ' 6 3/4LO ', ' 6 3/4WO ', ' 6 5/8LO ', ' 6 5/8WO ', ' 6 7/8LO ', ' 6 7/8WO ', ' 6 OZ. ', ' \"6\"\"\" ', ' \"6\"\"X9\"\"\" ', ' 6.0M ', ' 6.5D ', ' 6.5E ', ' 6.5EE ', ' 6.5M ', ' 6.5R ', ' 6.5W ', ' 6.5XW ', ' \"60\"\"\" ', ' 60-62R ', ' 60L ', ' 60R ', ' 6-1/2LO ', ' 6-1/2WO ', ' 6-1/2XLO ', ' 6-1/2XXLO ', ' 6-1/4 WO ', ' 6-1/4LO ', ' 6-1/4WO ', ' 6-1/4XLO ', ' 62L ', ' 63/4LO ', ' 6-3/4LO ', ' 6-3/4WO ', ' 6-3/4XLO ', ' 64-66R ', ' 6-5/8LO ', ' 6-5/8WO ', ' 6-5/8XLO ', ' 6-7/8LO ', ' 6-7/8WO ', ' 6-7/8XLO ', ' 6-7/8XXLO ', ' 68L ', ' 6D ', ' 6E ', ' 6EE ', ' 6EEE ', ' 6LO ', ' 6M ', ' 6OZ ', ' 6P ', ' 6P ', ' 6P ', ' 6P/SCORP ', ' 6P/SCRPION ', ' 6R ', ' 6R/SCRPION ', ' 6W ', ' 6WO ', ' 6X ', ' 6X2.3 ', ' 6XL ', ' 6XL LONG ', ' 6XL XLONG ', ' 6XLL ', ' 6XLR ', ' 6XW ', ' 7 1/2LO ', ' 7 1/2WO ', ' 7 1/4 LO ', ' 7 1/4 WO ', ' 7 1/4LO ', ' 7 1/4WO ', ' 7 1/8LO ', ' 7 1/8WO ', ' 7 3/4LO ', ' 7 3/4WO ', ' 7 3/8LO ', ' 7 3/8WO ', ' 7 3-4 ', ' 7 5/8LO ', ' 7 5/8WO ', ' 7 7/8LO ', ' 7 7/8WO ', ' 7 LO ', ' 7.5D ', ' 7.5E ', ' 7.5EE ', ' 7.5EEE ', ' 7.5EW ', ' 7.5M ', ' 7.5N ', ' 7.5R ', ' 7.5W ', ' 7.5XW ', ' 7.62X39MM ', ' 7.62X54MM ', ' 7-1/2LO ', ' 7-1/2WO ', ' 7-1/2XLO ', ' 7-1/2XXLO ', ' 7-1/4LO ', ' 7-1/4WO ', ' 7-1/4XLO ', ' 7-1/4XXLO ', ' 7-1/4XXXLO ', ' 7-1/8LO ', ' 7-1/8WO ', ' 7-1/8XLO ', ' 7-1/8XXLO ', ' \"72\"\"\" ', ' 7-3/4LO ', ' 7-3/4WO ', ' 7-3/4XLO ', ' 7-3/8LO ', ' 7-3/8WO ', ' 7-3/8XLO ', ' 7-5/8LO ', ' 7-5/8WO ', ' 7-5/8XLO ', ' 75X30 ', ' 7-7/8 LO ', ' 7-7/8 XLO ', ' 7-7/8LO ', ' 7-7/8WO ', ' 7D ', ' 7E ', ' 7EE ', ' 7EEE ', ' 7EW ', ' 7LO ', ' 7M ', ' 7W ', ' 7WO ', ' 7X9 ', ' 7XLO ', ' 7XW ', ' 7XXLO ', ' 7XXXLO ', ' 8.0D ', ' 8.5-12 ', ' 8.5B ', ' 8.5C ', ' 8.5D ', ' 8.5E ', ' 8.5EE ', ' 8.5EEE ', ' 8.5EW ', ' 8.5M ', ' 8.5N ', ' 8.5R ', ' 8.5W ', ' 8.5XW ', ' 85EE ', ' 8B ', ' 8C ', ' 8D ', ' 8E ', ' 8EE ', ' 8EEE ', ' 8EW ', ' 8LO ', ' 8M ', ' 8N ', ' 8R ', ' 8W ', ' 8X10 ', ' 8XLO ', ' 8XW ', ' 9.0M ', ' 9.5B ', ' 9.5C ', ' 9.5D ', ' 9.5E ', ' 9.5EE ', ' 9.5EEE ', ' 9.5EW ', ' 9.5M ', ' 9.5M/W ', ' 9.5R ', ' 9.5W ', ' 9.5XW ', ' 9/40 STACK ', ' 95M ', ' 9B ', ' 9C ', ' 9D ', ' 9E ', ' 9EE ', ' 9EEE ', ' 9EW ', ' 9M ', ' 9MM ', ' 9MM/.40 ', ' 9MM/40 ', ' 9N ', ' 9R ', ' 9W ', ' 9XW ', ' ADJ ', ' ADJUSTABLE ', ' ASP ', ' AUTO ', ' \"AUTO 3.5\"\"\" ', ' \"AUTO 4\"\"\" ', ' \"AUTO 4.5\"\"\" ', ' AUTO 4/LT ', ' \"AUTO 5\"\"\" ', ' AUTO 9MM ', ' BDYGRD 2OZ ', ' BEAR TRAKR ', ' BER ', ' BER 8040D ', ' BER 84 ', ' BER 85F ', ' BER 92 ', ' BER 92 ', ' BER 92/96 ', ' BER 92F CL ', ' BER 92FC ', ' BER 92FS ', ' BER 96 ', ' BER CGR ', ' BER COUGAR ', ' BER COUGR ', ' BER PX4 ', ' BER PX4/LT ', ' BER380 ', ' BERETTA ', ' BG 2OZ ', ' BLACK ', ' BODYGD 3OZ ', ' C ', ' C CELL ', ' C/D CELL ', ' CAPRICE ', ' CASE ', ' CHARGER ', ' CIRCLE ', ' COLT 1911 ', ' COLT 45 ', ' COLT CMDR ', ' COLT COMM ', ' COLT DET S ', ' COLT GOVT ', ' COLT GOVTL ', ' COLT PYTHN ', ' COLT X200 ', ' COMP 2 ', ' CRN VIC ', ' CROWN VIC ', ' D CELL ', ' DBACK 380 ', ' DBL ', ' DBL STACK ', ' DET SPCL ', ' DIVIDED ', ' DOUBLE ', ' DSLED ', ' EX CERT ', ' EXPLORER ', ' F16 ', ' F21 ', ' F26 ', ' FEMALE ', ' FN 5-7 ', ' FN P45 ', ' FOR MKIII ', ' FOR MKIV ', ' FOX ', ' FPIU ', ' G22/TLR1 ', ' GALLON ', ' GK 17/22 ', ' GK 22/TLR1 ', ' GK21/USP ', ' GK22/TLR1 ', ' GLK ', ' GLK 17 ', ' GLK 17/19 ', ' GLK 17/22 ', ' GLK 17/22L ', ' GLK 17/23 ', ' GLK 17/26 ', ' GLK 17/LT ', ' GLK 17/M3X ', ' GLK 17/M6 ', ' GLK 17/TLR ', ' GLK 19/23 ', ' GLK 19/23L ', ' GLK 19/36 ', ' GLK 20/21 ', ' GLK 20/21L ', ' GLK 20/21X ', ' GLK 21 ', ' GLK 21/29 ', ' GLK 21/LT ', ' GLK 21/M3 ', ' GLK 21/XPH ', ' GLK 21TLR1 ', ' GLK 21X200 ', ' GLK 22 ', ' GLK 22/23 ', ' GLK 22/LT ', ' GLK 22/M3 ', ' GLK 22/M6 ', ' GLK 22M3X ', ' GLK 22M6X ', ' GLK 22X200 ', ' GLK 23 ', ' GLK 23/26 ', ' GLK 23/27 ', ' GLK 25 ', ' GLK 26/27 ', ' GLK 27 ', ' GLK 27/30 ', ' GLK 30 ', ' GLK 34/35 ', ' GLK 34/35 ', ' GLK 34/35L ', ' GLK 35 ', ' GLK 35/M6 ', ' GLK 36 ', ' GLK 37 ', ' GLK 37TLR1 ', ' GLK 38 ', ' GLK17/X400 ', ' GLK22/M3X ', ' GLK22/X200 ', ' GLK22X200 ', ' GLK23/LT ', ' GLOCK ', ' GLOCK 17 ', ' GLOCK 17/2 ', ' GOVT 45 ', ' GOVT 45 ', ' GSR/TLR ', ' H&K ', ' H&K 30C ', ' H&K 40C/M3 ', ' H&K GLO 23 ', ' H&K P7M13 ', ' H&K USP ', ' H&K USP 45 ', ' H&K USP40 ', ' H&K USP40C ', ' H&K USP45 ', ' H&K USP45C ', ' H&K USP9 ', ' HK USP ', ' HK USP45 ', ' HK USP45C ', ' IMPALA ', ' J FRAME ', ' \"J-FR 2\"\"\" ', ' J-FRAME ', ' \"J-FRAME 2\"\"\" ', ' \"K/L 4\"\"\" ', ' KAHR K9 ', ' KELTEC ', ' KEL-TEC ', ' KELTEC 32 ', ' KELTEC P11 ', ' \"K-FR 6\"\"\" ', ' K-FRAME ', ' KIM/TLE ', ' KIMB UC II ', ' KIMBER ', ' \"KIMBER 3\"\"\" ', ' \"KIMBER 4\"\"\" ', ' KIMBER 45 ', ' KIMBERX200 ', ' L ', ' L (10) ', ' \"L BY 1.5\"\"\" ', ' \"L BY 1.75\"\"\" ', ' L LONG ', ' L SHORT ', ' L TALL ', ' L WMNS ', ' L XLONG ', ' L/XL ', ' L/XLR ', ' L/XLS ', ' L/XLT ', ' L-2XL ', ' LARGE ', ' LCP ', ' LCP 380 ', ' LG ', ' \"LG 38-42\"\"\" ', ' LG AUTO ', ' LG/#6 PAD ', ' LG/#8 PAD ', ' LL ', ' LR ', ' LRG ', ' LS ', ' LT ', ' LXL ', ' M ', ' M (9) ', ' M 40-42R ', ' \"M BY 1.5\"\"\" ', ' \"M BY 1.75\"\"\" ', ' M LONG ', ' M SHORT ', ' M TALL ', ' M&P 9/40 ', ' M&P/LT ', ' M/L ', ' M/LR ', ' M/LS ', ' M/LT ', ' M26 ', ' M3 ', ' MD ', ' MD AUTO ', ' MD/#6 PAD ', ' MD/#8 PAD ', ' MD/LG ', ' MED ', ' \"MED 34-36\"\"\" ', ' \"MED 34-38\"\"\" ', ' MED AUTO ', ' MED AUTOS ', ' MEDIUM ', ' MEDIUM (9) ', ' METAL 9/40 ', ' MINI MAG ', ' MK 3 ', ' MK 4 ', ' MK 6 ', ' MK IV ', ' MK3 ', ' MK4 ', ' MK-4 ', ' MK6 ', ' ML ', ' M-LB ', ' MR ', ' MS ', ' MT ', ' MXL ', ' \"N-FRM 6\"\"\" ', ' ONE SIZE ', ' ONE SIZE ', ' OVER SIZE ', ' P2022/M6 ', ' P220 ', ' P220/P226 ', ' P220/X300 ', ' P220R/X300 ', ' P226 ', ' P226/M3 ', ' P226/TLR1 ', ' P226/X300 ', ' P226/X300 ', ' P228/P229 ', ' P229 ', ' P229/M3 ', ' P3AT ', ' P85/P89 ', ' P85/P90 ', ' PARA ', ' PEL 7060 ', ' PINT ', ' POLY/XT ', ' POLYDSLED ', ' POLYSTGR ', ' POLYSTGR ', ' POLYSTINGE ', ' POLYSTINGR ', ' PPK ', ' PPKS ', ' PPS ', ' PUNCH ', ' PUNCH2-MK3 ', ' PX4 STORM ', ' PX4RP ', ' QUART ', ' REM 1187 ', ' RUG LCP ', ' RUG SGL 6 ', ' RUGER ', ' RUGER GP10 ', ' RUGER KP94 ', ' RUGER LCR ', ' RUGER P85 ', ' RUGER P89 ', ' RUGER P90 ', ' RUGER P94 ', ' RUGER P97 ', ' RUGER SR9 ', ' S ', ' S SHORT ', ' S (8) ', ' S 28-32 ', ' S SHORT ', ' S&W #1 ', ' S&W 380 ', ' S&W 4000 ', ' S&W 5900 ', ' S&W 5906 ', ' S&W 642-1 ', ' S&W 669 ', ' S&W 6906 ', ' S&W 99/M5 ', ' S&W BG380 ', ' S&W J ', ' \"S&W K/L 4\"\"\" ', ' \"S&W K/L 6\"\"\" ', ' S&W K-FR 2 ', ' S&W M&P ', ' S&W M&P 40 ', ' S&W M&P 45 ', ' S&W M&P/L ', ' S&W M&P40 ', ' S&W MP ', ' S&W MP/LT ', ' S&W MP45 ', ' S&W MP9 ', ' S&W MPP/M5 ', ' S&W SIGMA ', ' S/M ', ' SCORPION ', ' SEE MEMO ', ' SGL RW MAG ', ' SGL STACK ', ' SIG ', ' SIG 220/6 ', ' SIG 226DAK ', ' SIG 229 ', ' SIG 229R ', ' SIG 2340 ', ' SIG 9/40 ', ' SIG P2 ', ' SIG P220 ', ' SIG P220R ', ' SIG P226 ', ' SIG P226R ', ' SIG P226X3 ', ' SIG P228 ', ' SIG P229 ', ' SIG P229M3 ', ' SIG P229R ', ' SIG P229RM ', ' SIG P230 ', ' SIG P238 ', ' SIG P239 ', ' SIG P250 ', ' SIG PRO ', ' SIG SP2340 ', ' SIG/LIGHT ', ' SIG/X300 ', ' SIG220M3 ', ' SIG226M3 ', ' SIG226RDAK ', ' SIG229 ', ' SIGMA ', ' SIGMA 9MM ', ' SIGMA9/40 ', ' SIGPRO2340 ', ' SINGLE ', ' SL ', ' SM ', ' \"SM 24-26\"\"\" ', ' \"SM 28-32\"\"\" ', ' SM AUTO ', ' SM REVOLVR ', ' SMALL ', ' SPFD ', ' \"SPFD 3\"\"\" ', ' SPFD XD ', ' \"SPFD XD 4\"\"\" ', ' \"SPFD XD 5\"\"\" ', ' SPFD XD/LT ', ' \"SPFD XD4\"\"\" ', ' SPFD XD40C ', ' SPFD/LT ', ' SPFLD ', ' SPFLD 1911 ', ' SPFLD TRP ', ' SPFLD X40D ', ' SPFLD XD ', ' SPFLD XD40 ', ' SPFLD XD45 ', ' SPFLD XD9 ', ' SPFLD/LT ', ' SPFLD/TLR ', ' SPR ', ' SPR OP ', ' SPR XD ', ' SPR/M3 ', ' SPRINGFIEL ', ' SR ', ' SS ', ' STD ', ' STGR S-200 ', ' STI/M3 ', ' STINGER ', ' STINGER DS ', ' STINGER/8X ', ' STINGERXT ', ' STRION ', ' STRION LED ', ' STRION/6P ', ' SW ', ' SW 3914 ', ' SW 3953 ', ' SW 40 ', ' SW 4006 ', ' SW 4013 ', ' SW 4503 ', ' SW 4506 ', ' SW 4516 ', ' SW 4586 ', ' SW 4586 ', ' SW 5904 ', ' SW 5906 ', ' SW 637 ', ' \"SW 686 4\"\"\" ', ' \"SW 686 4\"\"\" ', ' SW 6904 ', ' SW 6906 ', ' SW 6906 ', ' SW 6946 ', ' SW 99 ', ' SW 9F/40F ', ' SW BG 380 ', ' SW BGD 380 ', ' SW J ', ' SW J-FRAME ', ' SW K ', ' \"SW K/L 4\"\"\" ', ' SW K-FRAME ', ' SW M&P ', ' SW M&P 45 ', ' SW MP CMP ', ' SW MP/ ', ' SW MP/M3 ', ' SW N 4 ', ' \"SW N 4\"\"\" ', ' SW N 6 ', ' SW SIGMA ', ' SW3913 ', ' SW4003TSW ', ' SXL ', ' TAHOE ', ' TASER ', ' TAUR 24/7 ', ' TAURUS ', ' TAURUS 85 ', ' TAURUS 9 ', ' TAURUS85 ', ' THESE ARE ', ' TRI-HINGE ', ' UNIV ', ' UNIVERSAL ', ' \"UP TO 44\"\"\" ', ' USP ', ' USP 40C ', ' USP 45 ', ' USP 9/40 ', ' USP COMP ', ' WALTHER ', ' WLTHR ', ' WLTHR P22 ', ' WLTHR PPKS ', ' X LARGE ', ' X2 ', ' X26 ', ' XD 9/40 ', ' XD/TLR1 ', ' XD40/TLR1 ', ' XD45 ', ' XD45/M6 ', ' XD45/TLR1 ', ' XD45/TLR1 ', ' XD9/40 ', ' XDM ', ' XDM 40 ', ' XDM40/TLR1 ', ' XL ', ' XL (11) ', ' \"XL 44-46\"\"\" ', ' XL LONG ', ' XL TALL ', ' XL WMNS ', ' XL XLONG ', ' XL/2X ', ' XL/XXL ', ' XL35 ', ' XLARGE ', ' X-LARGE ', ' XLL ', ' XLR ', ' XLS ', ' XLT ', ' XLXL ', ' XLXXL ', ' XS ', ' XS (7) ', ' XS SHORT ', ' XS/SM ', ' XSL ', ' XSR ', ' XSS ', ' XXL ', ' XXL (12) ', ' XXL LONG ', ' XXL TALL ', ' XXL XLONG ', ' XXL XXLONG ', ' XXL/XXXL ', ' XXLARGE ', ' XXLL ', ' XXLR ', ' XXLS ', ' XXLT ', ' XXLXL ', ' XXLXXL ', ' XXXL ', ' XXXL LONG ', ' XXXL TALL ', ' XXXLARGE ', ' XXXLL ', ' XXXLR ', ' XXXLS ', ' XXXLT ', ' XXXLXL ', ' XXXLXXL ', ' XXXXL ', ' XXXXL TALL ', ' XXXXLL ', ' XXXXLR ', ' XXXXLT ', ' XXXXLXL ', ' XXXXLXXL ', ' XXXXXL ', ' XXXXXLL ', ' XXXXXLR ', ' XXXXXLT ', ' XXXXXLXXL ', ' XXXXXXL ', ' XXXXXXLL ', ' XXXXXXLS ', ' YOUTH L ', ' YOUTH XL '
    );
*/
/*
    foreach ($OrderedKeys as $OrderedValue) {
      reset($DataArray);
      foreach ($DataArray as $DataKey => $DataItem) {
        $DataItem['size']= str_replace( '"', "", $DataItem['size'] );
        if (trim($OrderedValue) == AddSlashes(trim($DataItem['size']))) {
          $ResArray[$DataKey] = $DataItem;
        }
      }
    }

    return $ResArray;

	$SizeOrderList= SizeOrderPeer::getSizeOrders('', false);
	foreach( $SizeOrderList as $lSizeOrder ) {
	  foreach( $SizesCountListing as $SizeKey=> $SizesCount) {
      if ( strtolower($lSizeOrder->getSize()) == strtolower($SizesCount['size']) ) {
				$ResArray[]= $SizesCount;
				$SizesCountListing[ $SizeKey ]['is_used']= 1 ;
				break;
			}
		}
	}


*/

		//  $OrderedKeys
		//  Util::deb( count($OrderedKeys), 'count($OrderedKeys)::' );
		$SizeOrderList= SizeOrderPeer::getSizeOrders('', false);
		foreach ( $SizeOrderList as $lSizeOrder ) {
			reset($DataArray);
			foreach ($DataArray as $DataKey => $DataItem) {
				//$DataItem['size']= str_replace( '"', "", $DataItem['size'] );
				$IsFound= false;
				if (trim($lSizeOrder->getSize()) == AddSlashes(trim($DataItem['size']))) {
					$ResArray[$DataKey] = $DataItem;
					$IsFound= true;
				}
			}
			if ( !$IsFound ) {
				$DataArray[$DataKey]['missed']= 1;
			}
		}
		reset( $DataArray );
		foreach ($DataArray as $DataKey => $DataItem) {
			if ( empty($DataItem['missed']) ) {
				$ResArray[$DataKey] = $DataItem;
			}
		}
		return $ResArray;

  }

}

