<?php
class sfNPropelPager extends sfPropelPager {
  
  /*
  public function init( $Data= array() )
  {
    parent::init();
    $this->m_Data = $Data;
    //Util::deb( $this->m_Data, 'init  $this->m_Data::' );
    $this->m_DataRowsCount= 0;
    foreach ($this->m_Data as $Key=>$DataYarray) {
      // Util::deb($Key,'$Key::');
      // Util::deb($DataYarray,'$DataYarray::');
      if ( !array($DataYarray) ) continue;
      $this->m_DataRowsCount+= count($DataYarray);  
    }
    // Util::deb($this->m_DataRowsCount,'??$this->m_DataRowsCount::');
    // Util::deb( $this->getMaxPerPage(), '??$this->getMaxPerPage()::' );
    // $this->setLastPage();
    $this->setLastPage( ceil(   $this->m_DataRowsCount / $this->getMaxPerPage()   ) );
  }*/
  
/*  
  public function getLinks($nb_links = 5)
  {
    
    // Util::deb($this->page,'$this->page::');
    $links = array();
    $url_page= sfContext::getInstance()->getRequest()->getParameter('page','');
    if ( !empty($url_page) ) {
      $this->page= $url_page;
    }
    
    $tmp   = $this->page - floor($nb_links / 2);
    //Util::deb($this->lastPage,'$this->lastPage::');
    
    $check = $this->lastPage - $nb_links + 1;
     //Util::deb($check,'$check::');
    
    $limit = $check > 0 ? $check : 1;
    
    $begin = $tmp > 0 ? ($tmp > $limit ? $limit : $tmp) : 1;

    $i = (int) $begin;
    while ($i < $begin + $nb_links && $i <= $this->lastPage)
    {
      $links[] = $i++;
    }

    $this->currentMaxLink = count($links) ? $links[count($links) - 1] : 1;

    //Util::deb( $links, '$links::' );
    return $links;
  }
*/
  public function getLinks($nb_links = 5, $ShowLastPages= 0)
  {
    $links = array();
    $tmp   = $this->page - floor($nb_links / 2);
    $check = $this->lastPage - $nb_links + 1;
    $limit = $check > 0 ? $check : 1;
    $begin = $tmp > 0 ? ($tmp > $limit ? $limit : $tmp) : 1;

    //Util::deb( $this->lastPage, ' $this->lastPage::' );
    $i = (int) $begin;
    while ($i < $begin + $nb_links && $i <= $this->lastPage)
    {
      $links[ $i ] = $i;
      $i++;
    }
    $X= $i-1;
    $this->currentMaxLink = $this->lastPage;  //count($links) ? $links[ count($links) - 1 ] : 1;
    //Util::deb( $this->currentMaxLink, ' $this->currentMaxLink::' );
    

    if ( $ShowLastPages> 0 ) {
    	$Y= $this->lastPage - $ShowLastPages + 1;
      if ( $Y - $X > 1 ) {
  		  $links[ $X+intval(($Y-$X)/2)  ]= '...';
      }
    	for( $I= $this->lastPage - $ShowLastPages + 1; $I<= $this->lastPage; $I++ ) {
    		$links[ $I ]= $I;
    	}
    }
    //Util::deb( $links, ' $links::' );
    //die("DIE");
    return $links;
  }


  
}