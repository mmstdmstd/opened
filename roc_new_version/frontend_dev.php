<?php

// this check prevents access to debug front controllers that are deployed by accident to production servers.
// feel free to remove this, extend it or make something more sophisticated.
/*if (!in_array(@$_SERVER['REMOTE_ADDR'], array('91.218.31.33', '::1', '98.228.61.182', '98.227.150.127')))
{
  die('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
} */
require_once(dirname(__FILE__).'/config/ProjectConfiguration.class.php');
ini_set( 'output_buffering', 1 );

$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'dev', true);
sfContext::createInstance($configuration)->dispatch();
