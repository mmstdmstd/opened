<?php
$app = sfConfig::get('sf_app');
?>
  <script type="text/javascript" language="JavaScript">
  <!--

  <?php if ( $app== 'frontend' ) : ?>
  function AskForActive(inactive_email) {
    var Url= '<?php echo url_for('@ask_for_active?inactive_email=') ?>'+inactive_email;
    document.location= Url;
  }
  
  function AskForUnbanned(banned_email) {
    var Url= '<?php echo url_for('@ask_for_unbanned?banned_email=') ?>'+banned_email;
    document.location= Url;
  }
  <?php endif; ?>

  //-->
  </script>
<style>
body {background-image:url('/images/login-background.png');} 
</style>


<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" />
<?php echo $form['_csrf_token']->render()?>
<div class="homePageContent">
	<div class="regPageContentBox">
		<div class="regPageContentCol1">
<!--			<h1>App Name</h1>   -->
			<?php // $lCmsItem= CmsItemPeer::getContentByAlias('UserRegister');
  			//if ( !empty($lCmsItem) and $app== 'frontend' ) { echo htmlspecialchars_decode( $lCmsItem->getContent() ); }
	  	?> 
			
		</div><!-- /.homePageContentCol1 -->

		<div class="regPageContentCol2">
			<div class="regPageContentColTop"></div>
			<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="loginBox" />
				<?php echo $form['_csrf_token']->render()?>
<!--				<h1><?php echo "Login to ".sfConfig::get('app_application_site_name') ?></h1>  -->
            <?php
            if (trim($sf_context->getUser()->getFlash('user_message') != '')) {
            ?>            
		       	<div class="ErrorLine margin"><?php echo $form->renderGlobalErrors() ?>
		       	<?php echo $sf_context->getUser()->getFlash('user_message');
        		  $login_status = $sf_context->getUser()->getAttribute('login_status', '');
		          $inactive_email = $sf_context->getUser()->getAttribute('inactive_email', '');          
		          $banned_email = $sf_context->getUser()->getAttribute('banned_email', '');
                    
		          $banned_reason = $sf_context->getUser()->getAttribute('banned_reason', '');
		          if( !empty($banned_reason) ) echo '<br>'.$banned_reason;
		          $InvalidLoginsCount = (int)$sf_context->getUser()->getAttribute('InvalidLoginsCount',0);
                    
		          $sf_context->getUser()->getAttributeHolder()->remove('login_status');
		          $sf_context->getUser()->getAttributeHolder()->remove('inactive_email');
		          $sf_context->getUser()->getAttributeHolder()->remove('banned_reason');
		          $sf_context->getUser()->getAttributeHolder()->remove('banned_email');
		       	?> </div>            
            <?php
            }
            ?>
          
				  <?php if ( $login_status== 'inactive' and $app== 'frontend' ) : ?>
    			      <input type="button" class="action_submit" onclick="javascript:AskForActive('<?php echo $inactive_email ?>')" value="Request To Activate User"/><br>
				  <?php endif; ?>
				  <?php if ( $login_status== 'banned' and $app== 'frontend' ) : ?>
        			  <input type="button" class="action_submit" onclick="javascript:AskForUnbanned('<?php echo $banned_email ?>')" value="Request To Unban User"/><br>
				  <?php endif; ?>
       	    		   	
				
				<div class="registerBox">

<!--			      	<?php echo "Email" ?> -->
            <label for="signin_username">Email:</label>
			      	<?php echo $form['username']->render() ?><?php echo strip_tags($form['username']->renderError()) ?>
<div style="clear:both"></div>
			       	<!--<?php echo $form['password']->renderLabel() ?>: -->
            <label for="signin_password">Password:</label>
			      	<?php echo $form['password']->render() ?><?php echo strip_tags($form['password']->renderError()) ?>
    
				    <?php if ( $InvalidLoginsCount > 3 and $app== 'frontend' ) : ?>
				    	Enter capture:
				        <input type="text" size="5" maxlength="5" name="signin[capture]" id="signin_capture" >
						<?php 
					        include_once("../plugins/sfCryptographpPlugin/lib/helper/CryptographpHelper.php");        
					        echo cryptographp_picture();
					        echo cryptographp_reload();
						?><small>Capture is not case sensitive</small>
					<?php endif ?>
					<?php if ($app== 'frontend') : ?>
				        <div class="forgetPsr"><?php echo link_to('Forgot Password ?', '@forget_password')?></div>
					<?php endif ?>
          <div style="clear:both"></div>
			        <div class="regSubmit">
			          <span class="buttonInputBg"><input id="login-btn" type="submit" class="action_submit" value="Login" /></span>
<!--
			          <span class="buttonInputBg"><input type="button" value="Home" onclick='javascript:document.location="<?php echo url_for('@homepage') ?>"' /></span>
-->			          
			          <input type="hidden" id="hid_LoginReferer" name="hid_LoginReferer" value="<?php echo !empty($LoginReferer)?$LoginReferer:'' ?>" >
			        </div>

					<div class="clr"></div>
				</div><!-- /.registerBox -->
			</form>
			<div class="regPageContentColBtm"></div>
		</div><!-- /.regPageContentCol2 -->
		<div class="clr"></div>
		<div class="hrLine"></div>
		<!-- <h2>Friends Playing</h2>
		<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:facepile href="http://www.facebook.com/pages/Recruit-Wars/205413242830458" width="200" max_rows="2"></fb:facepile> -->


	</div><!-- /.homePageContentBox -->
</div><!-- /.homePageContent -->
 </form>
