<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUser.php 7634 2008-02-27 18:01:40Z fabien $
 */
class sfGuardUser extends PluginsfGuardUser
{
	public function __toString()
	{
	  return $this->getName();
	}
	
  public function getPredictsCount()  {
    $c = new Criteria();    
    $c->add( PredictPeer::SF_GUARD_USER_ID, $this->getId() );
    return PredictPeer::doCount( $c );    
  }
	
	public function getIsActiveAsText() {
	  if ( $this->getIsActive() ) return sfGuardUserPeer::$IsActiveArray[1];
	  return sfGuardUserPeer::$IsActiveArray[0];
	}

	public function getUserBanned() {
    $c = new Criteria();
    $c->add( UserBannedPeer::SF_GUARD_USER_ID, $this->getId() );
    return UserBannedPeer::doSelectOne($c);	  
	}
	
	public function getIsBanned() {
	  $lUserBanned= $this->getUserBanned();
	  if ( empty($lUserBanned) ) return false;
	  return true;
	}
	
	public function getIsBannedText($NotBannedText='') {
	  $lUserBanned= $this->getUserBanned();
	  if ( empty($lUserBanned) ) return $NotBannedText;
	  return 'Banned on '.$lUserBanned->getCreatedAt( sfConfig::get('app_application_date_format' ) ).':'.$lUserBanned->getReason();
	}
	
}
