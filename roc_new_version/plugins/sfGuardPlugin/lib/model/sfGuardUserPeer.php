<?php

/*
* This file is part of the symfony package.
* (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardUserPeer.php 7634 2008-02-27 18:01:40Z fabien $
 */
class sfGuardUserPeer extends PluginsfGuardUserPeer
{
  public static $IsActiveArray= array( 0=>'Inactive', 1=>'Active' );

  public static function retrieveByUsername($username, $isActive = true)
  {
    $c = new Criteria();
    $c->add(self::USERNAME, $username);
    $c->add(self::IS_ACTIVE, $isActive);

    return self::doSelectOne($c);
  }

  public static function getByUsername($username)
  {
    $c = new Criteria();
    $c->add(self::USERNAME, $username);
    return self::doSelectOne($c);
  }

  public static function getUserByEmailHash($key)
  {
    $c = new Criteria();
    $c->add(self::EMAIL_HASH, $key);
    return self::doSelectOne($c);
  }

//      $this->UsersPager= sfGuardUserPeer::getUsers( $this->page, true, $this->filter_username, $this->filter_is_active, $this->filter_user_group_id, $this->sorting, true  );  
  public static function getUsers( $page=1, $ReturnPager=true, $filter_username='', $filter_is_active='', 
  $filter_user_group_id= '', $Sorting='USERNAME', $ShowAdmin= false, $ReturnCount= false ) {
    $c = new Criteria();
    if( !$ShowAdmin ) {
      $c->add( sfGuardUserPeer::IS_SUPER_ADMIN, false );
    }
    if ( !empty($filter_username) and $filter_username!= '-' ) {
      $c->add( sfGuardUserPeer::NAME , '%'.$filter_username.'%', Criteria::LIKE );
    }
    if ( strlen($filter_is_active)>0 and $filter_is_active!= '-' ) {
      $c->add( sfGuardUserPeer::IS_ACTIVE, $filter_is_active );
    }
    
    if ( !empty($filter_user_group_id) and $filter_user_group_id!= '-' ) {
      $c->addJoin( sfGuardUserPeer::ID, sfGuardUserGroupPeer::USER_ID, Criteria::JOIN );      
      $c->add( sfGuardUserGroupPeer::GROUP_ID, $filter_user_group_id );      
    }    
    
    if ( $Sorting=='CREATED_AT' or empty($Sorting) ) {
      $c->addDescendingOrderByColumn(sfGuardUserPeer::CREATED_AT);
      $c->addDescendingOrderByColumn(sfGuardUserPeer::ID);
    }
    if ( $Sorting=='USERNAME' ) {
      $c->addAscendingOrderByColumn(sfGuardUserPeer::USERNAME);
    }
    if ( $Sorting=='NAME' ) {
      $c->addAscendingOrderByColumn(sfGuardUserPeer::NAME);
    }
    if ( $Sorting=='IS_ACTIVE' ) {
      $c->addAscendingOrderByColumn( sfGuardUserPeer::IS_ACTIVE );
      $c->addDescendingOrderByColumn( sfGuardUserPeer::ID );
    }    
    
    if ( $Sorting=='USER_GROUP' ) {
      $c->addJoin( sfGuardUserPeer::ID, sfGuardUserGroupPeer::USER_ID, Criteria::JOIN );
      $c->addJoin( sfGuardUserGroupPeer::GROUP_ID, sfGuardGroupPeer::ID, Criteria::JOIN );
      $c->addAscendingOrderByColumn( sfGuardGroupPeer::NAME );
      $c->addAscendingOrderByColumn( sfGuardUserPeer::NAME );
    }
    
    if ( $ReturnCount ) {
      return sfGuardUserPeer::doCount( $c );      
    }
        
    if ( !$ReturnPager ) {
      return sfGuardUserPeer::doSelect( $c );
    }
    $pager = new sfPropelPager( 'sfGuardUser', (int)sfConfig::get('app_application_rows_in_pager' ) );
    $pager->setPage( $page );
    $pager->setCriteria($c);
    $pager->init();
    return $pager;
  }
  
  
  public static function getSimilarsfGuardUserByEmail($sfGuardUserEmail, $psfGuardUserId, $IsEqual=true, $con = null)
  {
    $c = new Criteria();
    $c->add( sfGuardUserPeer::USERNAME, $sfGuardUserEmail );
    if ( $IsEqual ) {
      $c->add( sfGuardUserPeer::ID, $psfGuardUserId );
    } else {
      $c->add( sfGuardUserPeer::ID, $psfGuardUserId, Criteria::NOT_EQUAL );
    }
    if($lResult = sfGuardUserPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getSimilarsfGuardUser($sfGuardUserName, $psfGuardUserId, $IsEqual=true, $con = null)
  {
    $c = new Criteria();
    $c->add( sfGuardUserPeer::NAME, $sfGuardUserName );
    if ( $IsEqual ) {
      $c->add( sfGuardUserPeer::ID, $psfGuardUserId );
    } else {
      $c->add( sfGuardUserPeer::ID, $psfGuardUserId, Criteria::NOT_EQUAL );
    }
    if($lResult = sfGuardUserPeer::doSelect( $c, $con ) )
    {
      return $lResult[0];
    }
    return '';
  }

  public static function getsfGuardUsersSelectionList( $Code, $Name, $NewCode='', $NewCodeText='', $ShowAdmin= false ) {
    $List= sfGuardUserPeer::getUsers( 1, false, '', '', '', '', $ShowAdmin );
    $ResArray= array();
    if ( !empty($Code) or !empty($Name) ) {
      $ResArray[$Code]= $Name;
    }
    foreach( $List as $lsfGuardUser ) {
      $ResArray[ $lsfGuardUser->getId() ]= $lsfGuardUser->getName();
    }
    if ( !empty($NewCode) and !empty($NewCodeText) ) {
      $ResArray[ $NewCode ]= $NewCodeText;
    }
    return $ResArray;
  }



  /**
  * generate Email Hash with given length($Length) and verifies in database it is unique($UniqueInsfGuardUser)
  *
  */
  public static function GenerateEmailHash($Length=32,$UniqueInsfGuardUser=true) {
    while (true) {
      $c = new Criteria();
      $EmailHash= self::PreparePassword($Length,$UniqueInsfGuardUser);
      $c->add(sfGuardUserPeer::EMAIL_HASH, $EmailHash); // verifies if such exists in database
      $sfGuardUsers= sfGuardUserPeer::doSelect($c);
      if ( empty($sfGuardUsers) ) return $EmailHash;
    }
    return '';
  }
  
  /**
  * generate Password with given length($Length) and verifies in database it is unique($UniqueInsfGuardUser)
  *
  */
  public static function GeneratePassword($Length=8,$UniqueInsfGuardUser=true) {
    while (true) {
      $c = new Criteria();
      $Password= self::PreparePassword($Length,$UniqueInsfGuardUser);
      $c->add(sfGuardUserPeer::PASSWORD, $Password); // verifies if such exists in database
      $sfGuardUsers= sfGuardUserPeer::doSelect($c);
      if ( empty($sfGuardUsers) ) return $Password;
    }
    return '';
  }

  /**
  * Prepare Password with given length($Length)
  *
  */
  public static function PreparePassword($Length) {
    $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $Res='';
    for ($I=0; $I< $Length; $I++ ) {
      $Index= rand(0,strlen($alphabet)-1);
      $Res.= substr($alphabet,$Index,1);
    }
    return $Res;
  }

    
}
