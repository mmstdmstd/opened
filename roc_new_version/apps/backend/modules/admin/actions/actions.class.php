<?php

/**
 * admin actions.
 *
 * @package    sf_sandbox
 * @subpackage admin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class adminActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{
	}

	public function executeIndex_new(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeYoutube_table(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeYoutube_add(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeYoutube_edit(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeOrder(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeOrder_view(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeDocument_table(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeDocument_add(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeDocument_edit2(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeSizechart_table(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeSizechart_add(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	public function executeSizechart_edit(sfWebRequest $request)
	{
		if ($this->getUser()->isAuthenticated()) {
			//$this->setLayout('layout2');
		}
	}

	////////////// INVENTORY_ITEMS BEGIN ////////////////////////
	/**
	 * Show list of InventoryItems in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeInventory_items(sfWebRequest $request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		$IsEditor = $this->getUser()->hasCredential('Editor');
		if (!$IsAdmin and !$IsEditor) $this->redirect('@homepage');
		try {
			$this->form = new InventoryItemFilterForm();
			$data = $request->getParameter('inventory_item');
			$this->filter_title = '';
			$this->filter_sku = '';
			$this->filter_brand_id = '';
			$this->filter_qty_on_hand_min = '';
			$this->filter_qty_on_hand_max = '';
			$this->filter_std_unit_price_min = '';
			$this->filter_std_unit_price_max = '';
			$response = $this->context->getResponse();
			$response->addJavascript('/js/overlib/overlib.js', 'last');
			$response->addJavascript('/js/tiny_mce/tiny_mce.js');
			$response->addJavascript('/js/funcs.js');
			$response->addJavascript('/js/tiny_mce/tiny_mce_popup.js');
			//Util::deb( $data, '$data::' );
			if ($request->isMethod('post')) {
				if (strlen($data['filter_title']) > 0) {
					$this->filter_title = $data['filter_title'];
					if ($this->filter_title == '-') $this->filter_title = '';
				}
				if (strlen($data['filter_sku']) > 0) {
					$this->filter_sku = $data['filter_sku'];
					if ($this->filter_sku == '-') $this->filter_sku = '';
				}
				if (strlen($data['filter_brand_id']) > 0) {
					$this->filter_brand_id = $data['filter_brand_id'];
					if ($this->filter_brand_id == '-') $this->filter_brand_id = '';
				}
				if (strlen($data['filter_qty_on_hand_min']) > 0) {
					$this->filter_qty_on_hand_min = $data['filter_qty_on_hand_min'];
					if ($this->filter_qty_on_hand_min == '-') $this->filter_qty_on_hand_min = '';
				}

				if (count($data['filter_qty_on_hand_max']) > 0) {
					$this->filter_qty_on_hand_max = $data['filter_qty_on_hand_max'];
					if ($this->filter_qty_on_hand_max == '-') $this->filter_qty_on_hand_max = '';
				}
				if (count($data['filter_std_unit_price_min']) > 0) {
					$this->filter_std_unit_price_min = $data['filter_std_unit_price_min'];
					if ($this->filter_std_unit_price_min == '-') $this->filter_std_unit_price_min = '';
				}
				if (count($data['filter_std_unit_price_max']) > 0) {
					$this->filter_std_unit_price_max = $data['filter_std_unit_price_max'];
					if ($this->filter_std_unit_price_max == '-') $this->filter_std_unit_price_max = '';
				}
			} else {
				$this->filter_title = $request->getParameter('filter_title');
				if ($this->filter_title == '-') $this->filter_title = '';
				$this->filter_sku = $request->getParameter('filter_sku');
				if ($this->filter_sku == '-') $this->filter_sku = '';
				$this->filter_brand_id = $request->getParameter('filter_brand_id');
				if ($this->filter_brand_id == '-') $this->filter_brand_id = '';
				$this->filter_qty_on_hand_min = $request->getParameter('filter_qty_on_hand_min');
				if ($this->filter_qty_on_hand_min == '-') $this->filter_qty_on_hand_min = '';
				$this->filter_qty_on_hand_max = $request->getParameter('filter_qty_on_hand_max');
				if ($this->filter_qty_on_hand_max == '-') $this->filter_qty_on_hand_max = '';

				$this->filter_std_unit_price_min = $request->getParameter('filter_std_unit_price_min');
				if ($this->filter_std_unit_price_min == '-') $this->filter_std_unit_price_min = '';
				$this->filter_std_unit_price_max = $request->getParameter('filter_std_unit_price_max');
				if ($this->filter_std_unit_price_max == '-') $this->filter_std_unit_price_max = '';
				$data = array('filter_title' => $this->filter_title, 'filter_sku' => $this->filter_sku, 'filter_brand_id' => $this->filter_brand_id, 'filter_qty_on_hand_min' => $this->filter_qty_on_hand_min, 'filter_qty_on_hand_max' => $this->filter_qty_on_hand_max, 'filter_std_unit_price_min' => $this->filter_std_unit_price_min, 'filter_std_unit_price_max' => $this->filter_std_unit_price_max);
			}
			if ($this->is_selection) {
				$this->setLayout('layout_empty');
			}
			$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->InventoryItemsPager = InventoryItemPeer::getInventoryItems($this->page, true, false, '', '', '', $this->filter_title, false, $this->filter_sku, '', '', $this->filter_brand_id, $this->filter_qty_on_hand_min, $this->filter_qty_on_hand_max, $this->filter_std_unit_price_min, $this->filter_std_unit_price_max, $this->sorting, '');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}

	}


	/**
	 * Show editor of InventoryItem by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeInventory_item_edit($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		$IsEditor = $this->getUser()->hasCredential('Editor');
		if (!$IsAdmin and !$IsEditor) $this->redirect('@homepage');

		$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		$response->addJavascript('/js/jquery/ui.js', 'last');
		$response->addStylesheet('/css/calendar.css', 'last');
		try {
			$this->recruit_id = $request->getParameter('id');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->filter_title = $request->getParameter('filter_title');
			$this->filter_sku = $request->getParameter('filter_sku');
			$this->filter_brand_id = $request->getParameter('filter_brand_id');
			$this->filter_qty_on_hand_min = $request->getParameter('filter_qty_on_hand_min');
			$this->filter_qty_on_hand_max = $request->getParameter('filter_qty_on_hand_max');
			$this->filter_std_unit_price_min = $request->getParameter('filter_std_unit_price_min');
			$this->filter_std_unit_price_max = $request->getParameter('filter_std_unit_price_max');
			$this->IsInsert = false;
			if (!empty($this->recruit_id)) {
				$this->InventoryItem = InventoryItemPeer::retrieveByPK($this->recruit_id);
				if (empty($this->InventoryItem)) $this->redirect('@homepage');
				if ($this->InventoryItem->getSport() != AppUtils::getCurrentSport()) {
					$this->redirect('@homepage'); // attempt to edit Player of other Sport...
				}
			} else {
				$this->IsInsert = true;
			}
			if (!empty($this->recruit_id)) {
				InventoryItemEditorForm::$InventoryItem = $this->InventoryItem;
			}
			InventoryItemEditorForm::$IsInsert = $this->IsInsert;
			$this->form = new InventoryItemEditorForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('recruit');
				$this->form->bind($data, $request->getFiles('recruit'));
				if ($this->form->isValid()) {
					$OriginalStatus = '';
					$CurrentStatus = '';
					if (empty($this->InventoryItem)) {
						$this->InventoryItem = new InventoryItem();
					} else {
						$OriginalStatus = $this->InventoryItem->getStatus();
						$CurrentStatus = $data[InventoryItemPeer::STATUS];
						$ProposerUserId = $this->InventoryItem->getProposerUserId();
					}
					$this->InventoryItem->setFirstName($data[InventoryItemPeer::FIRST_NAME]);
					$this->InventoryItem->setLastName($data[InventoryItemPeer::LAST_NAME]);
					$this->InventoryItem->setSport(AppUtils::getCurrentSport());
					$this->InventoryItem->setStatus($data[InventoryItemPeer::STATUS]);
					$this->InventoryItem->setPositionId($data[InventoryItemPeer::POSITION_ID]);
					$this->InventoryItem->setRanking($data[InventoryItemPeer::RANKING]);
					$this->InventoryItem->setSchool($data[InventoryItemPeer::SCHOOL]);
					$this->InventoryItem->setGraduationYear($data[InventoryItemPeer::GRADUATION_YEAR]);
					$this->InventoryItem->setState($data[InventoryItemPeer::STATE]);
					$this->InventoryItem->setCity($data[InventoryItemPeer::CITY]);
					$this->InventoryItem->setHeight($data[InventoryItemPeer::HEIGHT]);
					$this->InventoryItem->setWeight($data[InventoryItemPeer::WEIGHT]);
					if (!empty($data[InventoryItemPeer::COMMITTED_COLLEGE_ID])) {
						$this->InventoryItem->setCommittedCollegeId($data[InventoryItemPeer::COMMITTED_COLLEGE_ID]);
					}
					if ($data[InventoryItemPeer::STATUS] == 'CT') { // CT - Commited
						$this->InventoryItem->setCommittedDate(time());
					} else { // NC - "No Commitment", VC - "verbal commitment"
						$this->InventoryItem->setCommittedDate(null);
					}
					$recruit_colleges_interest_Array = $data['recruit_colleges_interest'];
					$con->beginTransaction();
					$this->InventoryItem->setUpdatedAt(time());
					$this->InventoryItem->save();
					$this->InventoryItem->setInventoryItemCollegesInterest($recruit_colleges_interest_Array);
					$con->commit();

					if ($OriginalStatus == 'PR' and $CurrentStatus != 'PR') {
						$lProposerUser = sfGuardUserPeer::retrieveByPK($ProposerUserId);
						$site_name = sfConfig::get('app_application_site_name');
						$notification_email = sfConfig::get('app_notification_email');
						$notification_email_title = sfConfig::get('app_notification_email_title');
						$site_url = '<a href="' . Util::getServerHost(sfContext::getInstance()->getConfiguration(), false, false) . '">' . $site_name . '</a>';

						$Msg = CmsItemPeer::getBodyContentByAlias('InventoryItemWasConfirmed',
							array('user_name' => $lProposerUser->getName(),
								'site_name' => $site_name,
								'recruit_name' => $this->InventoryItem->getFirstName() . ' ' . $this->InventoryItem->getLastName(),
								'site_url' => $site_url
							));
						Util::SendEmail($lProposerUser->getUsername(), $notification_email, "Your InventoryItem Was Confirmed at " . $site_name, $Msg);
					}

					$this->getUser()->setFlash('user_message', "InventoryItem'" . $this->InventoryItem->getLastName() . "' was " . (empty($this->recruit_id) ? "added." : "edited."));
					if ($this->IsInsert) {
						$this->redirect('@admin_recruit_edit?id=' . $this->InventoryItem->getId() . '&page=' . $this->page . '&filter_title=' . $this->filter_title . '&filter_sku=' . $this->filter_sku . '&filter_brand_id=' . $this->filter_brand_id . '&filter_qty_on_hand_min=' . $this->filter_qty_on_hand_min . '&filter_qty_on_hand_max=' . $this->filter_qty_on_hand_max . '&filter_std_unit_price_min=' . $this->filter_std_unit_price_min . '&filter_std_unit_price_max=' . $this->filter_std_unit_price_max . '&sorting=' . $this->sorting);
					} else {
						$this->redirect('@admin_recruits?page=' . $this->page . '&filter_title=' . $this->filter_title . '&filter_sku=' . $this->filter_sku . '&filter_brand_id=' . $this->filter_brand_id . '&filter_qty_on_hand_min=' . $this->filter_qty_on_hand_min . '&filter_qty_on_hand_max=' . $this->filter_qty_on_hand_max . '&filter_std_unit_price_min=' . $this->filter_std_unit_price_min . '&filter_std_unit_price_max=' . $this->filter_std_unit_price_max . '&sorting=' . $this->sorting);
					}
				}
			}
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeInventory_item_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		$IsEditor = $this->getUser()->hasCredential('Editor');
		if (!$IsAdmin and !$IsEditor) $this->redirect('@homepage');

		$con = Propel::getConnection(InventoryItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->recruit_id = $request->getParameter('id');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->filter_title = $request->getParameter('filter_title');
			$this->filter_sku = $request->getParameter('filter_sku');
			$this->filter_brand_id = $request->getParameter('filter_brand_id');
			$this->filter_qty_on_hand_min = $request->getParameter('filter_qty_on_hand_min');
			$this->filter_qty_on_hand_max = $request->getParameter('filter_qty_on_hand_max');
			$this->filter_std_unit_price_min = $request->getParameter('filter_std_unit_price_min');
			$this->filter_std_unit_price_max = $request->getParameter('filter_std_unit_price_max');
			$lInventoryItem = InventoryItemPeer::retrieveByPK($this->recruit_id);
			if (empty($lInventoryItem)) $this->redirect('@homepage');

			$con->beginTransaction();
			foreach ($lInventoryItem->getInventoryItemExternalRankings() as $lInventoryItemExternalRanking) {
				$lInventoryItemExternalRanking->delete();
			}

			$OriginalStatus = $lInventoryItem->getStatus();
			$ProposerUserId = $lInventoryItem->getProposerUserId();
			$recruit_name = $lInventoryItem->getFirstName() . ' ' . $lInventoryItem->getLastName();
			$lInventoryItem->delete();
			if ($OriginalStatus == 'PR' and !empty($ProposerUserId)) {
				$lProposerUser = sfGuardUserPeer::retrieveByPK($ProposerUserId);
				if (!empty($lProposerUser)) {
					$site_name = sfConfig::get('app_application_site_name');
					$notification_email = sfConfig::get('app_notification_email');
					$notification_email_title = sfConfig::get('app_notification_email_title');
					$site_url = '<a href="' . Util::getServerHost(sfContext::getInstance()->getConfiguration(), false, false) . '">' . $site_name . '</a>';

					$Msg = CmsItemPeer::getBodyContentByAlias('ProposedInventoryItemWasDeleted',
						array('user_name' => $lProposerUser->getName(),
							'site_name' => $site_name,
							'recruit_name' => $recruit_name,
							'site_url' => $site_url
						));
					Util::SendEmail($lProposerUser->getUsername(), $notification_email, "Your InventoryItem Was Confirmed at " . $site_name, $Msg);
				}
			}
			$con->commit();
			$this->redirect('@admin_recruits?page=' . $this->page . '&filter_title=' . $this->filter_title . '&filter_sku=' . $this->filter_sku . '&filter_brand_id=' . $this->filter_brand_id . '&filter_qty_on_hand_min=' . $this->filter_qty_on_hand_min . '&filter_qty_on_hand_max=' . $this->filter_qty_on_hand_max . '&filter_std_unit_price_min=' . $this->filter_std_unit_price_min . '&filter_std_unit_price_max=' . $this->filter_std_unit_price_max . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// INVENTORY_ITEMS END ////////////////////////


	////////////// YOUTUBE_VIDEOS BEGIN ////////////////////////
	/**
	 * Show editor of YoutubeVideo by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeYoutube_videos(sfWebRequest $request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		try {
			$data = $request->getParameter('youtube_video');
			$response = $this->context->getResponse();
			//$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->YoutubeVideosPager = YoutubeVideoPeer::getYoutubeVideos($this->page, true, $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/**
	 * Show editor of YoutubeVideo by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeYoutube_video_edit($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(YoutubeVideoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->youtube_video_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->IsInsert = false;
			if (!empty($this->youtube_video_key) and $this->youtube_video_key != '-') {
				$this->YoutubeVideo = YoutubeVideoPeer::retrieveByPK($this->youtube_video_key);
				if (empty($this->YoutubeVideo)) $this->redirect('@homepage');
			} else {
			}
			if (!empty($this->youtube_video_key)  and $this->youtube_video_key != '-') {
				YoutubeVideoEditorForm::$YoutubeVideo = $this->YoutubeVideo;
			}
			$this->form = new YoutubeVideoEditorForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('youtube_video');
				$this->form->bind($data, $request->getFiles('youtube_video'));
				if ($this->form->isValid()) {
					if (empty($this->YoutubeVideo)) {
						$this->YoutubeVideo = new YoutubeVideo();
					}
					if ($this->IsInsert) {
					}
					$this->YoutubeVideo->setKey($data[YoutubeVideoPeer::KEY]);
					$this->YoutubeVideo->setOrdering($data[YoutubeVideoPeer::ORDERING]);
					$this->YoutubeVideo->setUrl($data[YoutubeVideoPeer::URL]);
					$this->YoutubeVideo->setTitle($data[YoutubeVideoPeer::TITLE]);
					$this->YoutubeVideo->setDescription($data[YoutubeVideoPeer::DESCRIPTION]);
					$con->beginTransaction();
					$this->YoutubeVideo->save();
					$con->commit();
					$this->getUser()->setFlash('user_message', "YoutubeVideo'" . $this->YoutubeVideo->getKey() . "' was " . (empty($this->youtube_video_key) ? "added." : "edited."));
					$this->redirect('@admin_youtube_videos?page=' . $this->page . '&sorting=' . $this->sorting);
				}
			}
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeYoutube_video_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(YoutubeVideoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->youtube_video_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$lYoutubeVideo = YoutubeVideoPeer::retrieveByPK($this->youtube_video_key);
			if (empty($lYoutubeVideo)) $this->redirect('@homepage');
			$con->beginTransaction();
			$lYoutubeVideo->delete();
			$con->commit();
			$this->redirect('@admin_youtube_videos?page=' . $this->page . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// YOUTUBE_VIDEOS END ////////////////////////


	// import_product_lines
	////////////// IMPORTING BEGIN ////////////////////////
	public function executeImport_product_lines($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$response = $this->context->getResponse();
		try {
			$this->form = new ImportProductLinesForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('import_product_lines');
				$this->form->bind($data, $request->getFiles('import_product_lines'));
				if ($this->form->isValid()) {
					$data = $request->getParameter('import_product_lines');
					$ExcelFileName = $_FILES['import_product_lines']['tmp_name']['xls_file'];
					$OriginalExcelFileName = $_FILES['import_product_lines']['name']['xls_file'];
					if (file_exists($ExcelFileName)) {
						include_once('lib/ImportProductLines.php');
						$ImportProductLines = new ImportProductLines();
						$ImportProductLines->setOriginalFileName($OriginalExcelFileName);
						$ImportProductLines->setFileName($ExcelFileName);
						$ImportProductLines->Run();
						$this->InfoText = $ImportProductLines->getInfoText();
					} // if (file_exists($ExcelFileName)) {
				}
			}
		} catch (Exception $lException) {
			unset($_SESSION["excel_source_encode"]);
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// IMPORTING END ////////////////////////


	////////////// ORDERED BEGIN ////////////////////////
	/*payment_methods*
	 * Show editor of Ordered by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeOrdereds(sfWebRequest $request)
	{
		//$this->setLayout('layout2');
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		try {
			//$data = $request->getParameter('ordered');
			$this->form = new OrderedFilterForm();
			$data= Util::TrimArrayItems($request->getParameter('ordered'));
			$this->filter_payment_method= '';
			$this->filter_status= '';
			$this->filter_date_from= '';
			$this->filter_date_till= '';
			$this->filter_state= '';
			$this->saved= $request->getParameter('saved');
			$response = $this->context->getResponse();

			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/ui.js', 'last');
			$response->addJavascript('/js/jquery/ui.datepicker.js', 'last');

			$response->addStylesheet('jquery-ui.css');

			if ($request->isMethod('post'))
			{
				if ( strlen($data['filter_payment_method'])>0 ) {
					$this->filter_payment_method= $data['filter_payment_method'];
					if ( $this->filter_payment_method=='-' ) $this->filter_payment_method='';
				}
				if ( strlen($data['filter_status'])>0 ) {
					$this->filter_status= $data['filter_status'];
					if ( $this->filter_status=='-' ) $this->filter_status='';
				}
				if ( strlen($data['filter_date_from'])>0 ) {
					$this->filter_date_from= $data['filter_date_from'];
					if ( $this->filter_date_from=='-' ) $this->filter_date_from='';
				}
				if ( strlen($data['filter_date_till'])>0 ) {
					$this->filter_date_till= $data['filter_date_till'];
					if ( $this->filter_date_till=='-' ) $this->filter_date_till='';
				}
				if ( strlen($data['filter_state'])>0 ) {
					$this->filter_state= $data['filter_state'];
					if ( $this->filter_state=='-' ) $this->filter_state='';
				}
			} else {
				$this->filter_payment_method= $request->getParameter('filter_payment_method');
				if ( $this->filter_payment_method=='-' ) $this->filter_payment_method='';
				$this->filter_status= $request->getParameter('filter_status');
				if ( $this->filter_status=='-' ) $this->filter_status='';
				$this->filter_date_from= $request->getParameter('filter_date_from');
				if ( $this->filter_date_from=='-' ) $this->filter_date_from='';
				$this->filter_date_till= $request->getParameter('filter_date_till');
				if ( $this->filter_date_till=='-' ) $this->filter_date_till='';
				$this->filter_state= $request->getParameter('filter_state');
				if ( $this->filter_state=='-' ) $this->filter_state='';
				$data= Util::TrimArrayItems( array(  'filter_payment_method' => $this->filter_payment_method,  'filter_status' => $this->filter_status, 'filter_date_from' => $this->filter_date_from, 'filter_date_till' => $this->filter_date_till, 'filter_state' => $this->filter_state ) );
			}
			$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->sort = $request->getParameter('sort');
			$this->OrderedsPager = OrderedPeer::getOrdereds($this->page, true, $this->filter_payment_method, $this->filter_status, $this->filter_date_from, $this->filter_date_till, $this->filter_state, $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/**
	 * Show editor of Ordered by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeOrdered_edit($request)
	{
		//$this->setLayout('layout2');
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(OrderedItemPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {

			$this->id = $request->getParameter('id');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);

			$this->filter_payment_method= $request->getParameter('filter_payment_method');
			if ( $this->filter_payment_method=='-' ) $this->filter_payment_method='';
			$this->filter_status= $request->getParameter('filter_status');
			if ( $this->filter_status=='-' ) $this->filter_status='';
			$this->filter_date_from= $request->getParameter('filter_date_from');
			if ( $this->filter_date_from=='-' ) $this->filter_date_from='';
			$this->filter_date_till= $request->getParameter('filter_date_till');
			if ( $this->filter_date_till=='-' ) $this->filter_date_till='';
			$this->filter_state= $request->getParameter('filter_state');
			if ( $this->filter_state=='-' ) $this->filter_state='';

			if (!empty($this->id) and $this->id != '-') {
				$this->Ordered = OrderedPeer::retrieveByPK($this->id);
				$this->OrderedItemList = OrderedItemPeer::getOrderedItems(1, false, $this->id);
				if (empty($this->Ordered)) $this->redirect('@homepage');
			} else {
			}
			 if ($request->isMethod('post'))
			{
				$checked_backorders= $_POST['checked_backorders'];
				$checked_backorders_array= Util::SplitStringAsKeyValues($checked_backorders);
				$con->beginTransaction();
				foreach($checked_backorders_array as $checked_backorder_id=>$checked_backorder_value) {
					$lOrderedItem= OrderedItemPeer::retrieveByPK( $checked_backorder_id );
					if ( !empty($lOrderedItem) ) {
						$lOrderedItem->setBackorder($checked_backorder_value);
						$lOrderedItem->save();
					}
				}
				$con->commit();
  			$this->redirect( '@admin_ordereds?page=' . $this->page . '&sorting='.$this->sorting . '&saved=1' . '&filter_payment_method=' . $this->filter_payment_method .
				'&filter_status=' . $this->filter_status . '&filter_date_from=' . $this->filter_date_from . '&filter_date_till=' . $this->filter_date_till . '&filter_state=' . $this->filter_state);
			}

		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeOrdered_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->id = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$lOrdered = OrderedPeer::retrieveByPK($this->id);
			if (empty($lOrdered)) $this->redirect('@homepage');
			$con->beginTransaction();
			$lOrdered->delete();
			$con->commit();
			$this->redirect('@admin_ordereds?page=' . $this->page . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// ORDERED END ////////////////////////


	////////////// DOCUMENTS BEGIN ////////////////////////
	/**
	 * Show editor of Document by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDocuments(sfWebRequest $request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		try {
			$data = $request->getParameter('document');
			$response = $this->context->getResponse();
			//$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->DocumentsPager = DocumentPeer::getDocuments($this->page, true, '', $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/**
	 * Show editor of Document by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDocument_edit($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$response = $this->context->getResponse();
		try {
			$this->document_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->IsInsert = false;
			if (!empty($this->document_key) and $this->document_key != '-') {
				$this->Document = DocumentPeer::retrieveByPK($this->document_key);
				if (empty($this->Document)) $this->redirect('@homepage');
			} else {
			}
			if (!empty($this->document_key)  and $this->document_key != '-') {
				DocumentEditorForm::$Document = $this->Document;
			}
			$this->form = new DocumentEditorForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('document');
				$FileData = $request->getFiles('document');
				//Util::deb($data, '$data::');
				//Util::deb($FileData, '$FileData::');

				$this->form->bind($data, $request->getFiles('document'));
				if ($this->form->isValid()) {
					if (empty($this->Document)) {
						$this->Document = new Document();
					}
					if ($this->IsInsert) {
					}
					$this->Document->setKey($data[DocumentPeer::KEY]);
					$this->Document->setOrdering($data[DocumentPeer::ORDERING]);
					$this->Document->setTitle($data[DocumentPeer::TITLE]);
					$this->Document->setFilename($FileData[DocumentPeer::FILENAME]['name']);
					$this->Document->setThumbnail($FileData[DocumentPeer::THUMBNAIL]['name']);
					$this->Document->save();
//die("HTR");
					$sf_web_dir = sfConfig::get('sf_web_dir');
					$DocumentsDirectory = sfConfig::get('app_application_DocumentsDirectory');
					if (!empty($FileData[DocumentPeer::FILENAME]['tmp_name'])) {
						$TempFileName = $FileData[DocumentPeer::FILENAME]['tmp_name'];
						$OrigFileName = $FileData[DocumentPeer::FILENAME]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $DocumentsDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
					}

					$DocumentsThumbnailsDirectory = sfConfig::get('app_application_DocumentsThumbnailsDirectory');
					if (!empty($FileData[DocumentPeer::THUMBNAIL]['tmp_name'])) {
						$TempFileName = $FileData[DocumentPeer::THUMBNAIL]['tmp_name'];
						$OrigFileName = $FileData[DocumentPeer::THUMBNAIL]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $DocumentsThumbnailsDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
					}
					$this->getUser()->setFlash('user_message', "Document'" . $this->Document->getKey() . "' was " . (empty($this->document_key) ? "added." : "edited."));
					$this->redirect('@admin_documents?page=' . $this->page . '&sorting=' . $this->sorting);
				}
			}
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeDocument_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$response = $this->context->getResponse();
		try {
			$this->document_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$lDocument = DocumentPeer::retrieveByPK($this->document_key);
			if (empty($lDocument)) $this->redirect('@homepage');
			$sf_web_dir = sfConfig::get('sf_web_dir');
			$DocumentsDirectory = sfConfig::get('app_application_DocumentsDirectory');
			$DocumentsThumbnailsDirectory = sfConfig::get('app_application_DocumentsThumbnailsDirectory');

			$FileName = $sf_web_dir . DIRECTORY_SEPARATOR . $DocumentsDirectory . DIRECTORY_SEPARATOR . $lDocument->getFilename();
			$ThumbnailFileName = $sf_web_dir . DIRECTORY_SEPARATOR . $DocumentsThumbnailsDirectory . DIRECTORY_SEPARATOR . $lDocument->getThumbnail();

			$lDocument->delete();

			if (file_exists($FileName) and strlen($lDocument->getFilename()) > 0) {
				unlink($FileName);
			}
			if (file_exists($ThumbnailFileName) and strlen($lDocument->getThumbnail()) > 0) {
				unlink($ThumbnailFileName);
			}


			$this->redirect('@admin_documents?page=' . $this->page . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	////////////// DOCUMENTS END ////////////////////////




	// import_color_mapping
	////////////// IMPORTING BEGIN ////////////////////////
	public function executeImport_color_mapping($request)
	{
		//$this->setLayout('layout2');
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$response = $this->context->getResponse();
		try {
			$this->form = new ImportColorMappingsForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('import_color_mapping');
				$this->form->bind($data, $request->getFiles('import_color_mapping'));
				if ($this->form->isValid()) {
					$data = $request->getParameter('import_color_mapping');
					// Util::deb($data, '$data::');
					// Util::deb($_FILES, '$_FILES::');
					$ExcelFileName = $_FILES['import_color_mapping']['tmp_name']['xls_file'];
					$OriginalExcelFileName = $_FILES['import_color_mapping']['name']['xls_file'];
					if (file_exists($ExcelFileName)) {
						include_once('lib/ImportColorMappings.php');
						$ImportColorMappings = new ImportColorMappings();
						$ImportColorMappings->setOriginalFileName($OriginalExcelFileName);
						$ImportColorMappings->setFileName($ExcelFileName);
						$ImportColorMappings->getDeleteRows();
						$ImportColorMappings->Run();
						$this->InfoText = $ImportColorMappings->getInfoText();
					} // if (file_exists($ExcelFileName)) {
				}
			}
		} catch (Exception $lException) {
			//unset($_SESSION["excel_source_encode"]);
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	// import_matrix_item_sizes
	////////////// IMPORTING BEGIN ////////////////////////
	public function executeImport_matrix_item_sizes($request)
	{
		//$this->setLayout('layout2');

		/* Circle=100; $Page=1;
		$I= 0;
		for( $Page=1; $Page <= 200; $Page++ ) {
		  $lInventoryItemsList= InventoryItemPeer::getInventoryItemsListByLimits( $Page, $Circle );//getInventoryItems('');
		  foreach( $lInventoryItemsList as $lInventoryItem ) {
			  $I++;
			  $lInventoryItem->setId($I);
			  $lInventoryItem->save();
			  //Util::deb($I, '$I::');
		  }
			Util::deb($Page, '$Page::');
		}
		Util::deb($I, '$I::');
		//return; */

		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$response = $this->context->getResponse();
		try {
			$this->form = new ImportMatrixItemSizesForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('import_matrix_item_sizes');
				$this->form->bind($data, $request->getFiles('import_matrix_item_sizes'));
				if ($this->form->isValid()) {
					$data = $request->getParameter('import_matrix_item_sizes');
					$delete_existing_row= $data['delete_existing_row'];
					// Util::deb($data, '$data::');
					// Util::deb($_POST, '$_POST::');
					// die("UU");
					$ExcelFileName = $_FILES['import_matrix_item_sizes']['tmp_name']['xls_file'];
					$OriginalExcelFileName = $_FILES['import_matrix_item_sizes']['name']['xls_file'];
					if (file_exists($ExcelFileName)) {
						include_once('lib/ImportMatrixItemSizes.php');
						$ImportMatrixItemSizes = new ImportMatrixItemSizes();
						$ImportMatrixItemSizes->setOriginalFileName($OriginalExcelFileName);
						$ImportMatrixItemSizes->setFileName($ExcelFileName);
						if ( $delete_existing_row == 'D' ) {
						  $ImportMatrixItemSizes->DeleteAllRows();
						}
						$ImportMatrixItemSizes->Run();
						$this->InfoText = $ImportMatrixItemSizes->getInfoText();
					} // if (file_exists($ExcelFileName)) {
				}
			}
		} catch (Exception $lException) {
			//unset($_SESSION["excel_source_encode"]);
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// IMPORTING END ////////////////////////

	////////////// SIZING CHARTS BEGIN ////////////////////////
	/**
	 * Show editor of SizingChart by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeSizing_charts(sfWebRequest $request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		try {
			$data = $request->getParameter('sizing_chart');
			$response = $this->context->getResponse();
			//$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->SizingChartsPager = SizingChartPeer::getSizingCharts($this->page, true, '', $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/**
	 * Show editor of SizingChart by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeSizing_chart_edit($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(SizingChartPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->sizing_chart_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->IsInsert = false;
			if (!empty($this->sizing_chart_key) and $this->sizing_chart_key != '-') {
				$this->SizingChart = SizingChartPeer::retrieveByPK($this->sizing_chart_key);
				if (empty($this->SizingChart)) $this->redirect('@homepage');
			} else {
			}
			if (!empty($this->sizing_chart_key)  and $this->sizing_chart_key != '-') {
				SizingChartEditorForm::$SizingChart = $this->SizingChart;
			}
			$this->form = new SizingChartEditorForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('sizing_chart');
				$FileData = $request->getFiles('sizing_chart');
				$this->form->bind($data, $request->getFiles('sizing_chart'));
				if ($this->form->isValid()) {
					if (empty($this->SizingChart)) {
						$this->SizingChart = new SizingChart();
					}
					if ($this->IsInsert) {
					}
					$this->SizingChart->setKey($data[SizingChartPeer::KEY]);
					$this->SizingChart->setOrdering($data[SizingChartPeer::ORDERING]);
					$this->SizingChart->setTitle($data[SizingChartPeer::TITLE]);
					$con->beginTransaction();
					$this->SizingChart->save();

					$sf_web_dir = sfConfig::get('sf_web_dir');
					$SizingChartsDirectory = sfConfig::get('app_application_SizingChartsDirectory');
					// Util::deb($FileData, '$FileData::');
					if (!empty($FileData[SizingChartPeer::FILENAME]['tmp_name'])) {
						$TempFileName = $FileData[SizingChartPeer::FILENAME]['tmp_name'];
						// Util::deb($TempFileName, '--1 $TempFileName::');
						$OrigFileName = $FileData[SizingChartPeer::FILENAME]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $SizingChartsDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
						$this->SizingChart->setFilename( $FileData[SizingChartPeer::FILENAME]['name'] );
						$this->SizingChart->save();
					}

					$SizingChartsThumbnailsDirectory = sfConfig::get('app_application_SizingChartsThumbnailsDirectory');
					if (!empty($FileData[SizingChartPeer::THUMBNAIL]['tmp_name'])) {
						$TempFileName = $FileData[SizingChartPeer::THUMBNAIL]['tmp_name'];
						$OrigFileName = $FileData[SizingChartPeer::THUMBNAIL]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $SizingChartsThumbnailsDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
						$this->SizingChart->setThumbnail( $FileData[SizingChartPeer::THUMBNAIL]['name'] );
						$this->SizingChart->save();
					}
					$con->commit();
					$this->getUser()->setFlash('user_message', "SizingChart'" . $this->SizingChart->getKey() . "' was " . (empty($this->sizing_chart_key) ? "added." : "edited."));
					$this->redirect('@admin_sizing_charts?page=' . $this->page . '&sorting=' . $this->sorting);
				}
			}
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeSizing_chart_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(SizingChartPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->sizing_chart_key = $request->getParameter('key');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$lSizingChart = SizingChartPeer::retrieveByPK($this->sizing_chart_key);
			if (empty($lSizingChart)) $this->redirect('@homepage');
			$con->beginTransaction();
			$lSizingChart->delete();
			$con->commit();
			$this->redirect('@admin_sizing_charts?page=' . $this->page . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// SIZING CHARTS END ////////////////////////




	////////////// PAPERWORKS BEGIN ////////////////////////
	/**
	 * Show editor of Paperwork by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executePaperworks(sfWebRequest $request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		try {
			$data = $request->getParameter('paperwork');
			$response = $this->context->getResponse();
			//$this->form->bind($data);
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			$this->PaperworksPager = PaperworkPeer::getPaperworks($this->page, true, '', $this->sorting);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/**
	 * Show editor of Paperwork by its ID in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executePaperwork_edit($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(PaperworkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->paperwork_code = $request->getParameter('code');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$this->IsInsert = false;
			if (!empty($this->paperwork_code) and $this->paperwork_code != '-') {
				$this->Paperwork = PaperworkPeer::retrieveByPK($this->paperwork_code);
				if (empty($this->Paperwork)) $this->redirect('@homepage');
			} else {
			}
			if (!empty($this->paperwork_code)  and $this->paperwork_code != '-') {
				PaperworkEditorForm::$Paperwork = $this->Paperwork;
			}
			$this->form = new PaperworkEditorForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('paperwork');
				$FileData = $request->getFiles('paperwork');
				$this->form->bind($data, $request->getFiles('paperwork'));
				if ($this->form->isValid()) {
					if (empty($this->Paperwork)) {
						$this->Paperwork = new Paperwork();
					}
					if ($this->IsInsert) {
					}
					$this->Paperwork->setCode($data[PaperworkPeer::CODE]);
					$this->Paperwork->setOrdering($data[PaperworkPeer::ORDERING]);
					$this->Paperwork->setTitle($data[PaperworkPeer::TITLE]);
					$con->beginTransaction();
					$this->Paperwork->save();

					$sf_web_dir = sfConfig::get('sf_web_dir');
					$PaperworksDirectory = sfConfig::get('app_application_PaperworksDirectory');
					// Util::deb($FileData, '$FileData::');
					if (!empty($FileData[PaperworkPeer::FILENAME]['tmp_name'])) {
						$TempFileName = $FileData[PaperworkPeer::FILENAME]['tmp_name'];
						// Util::deb($TempFileName, '--1 $TempFileName::');
						$OrigFileName = $FileData[PaperworkPeer::FILENAME]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $PaperworksDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
						$this->Paperwork->setFilename( $FileData[PaperworkPeer::FILENAME]['name'] );
						$this->Paperwork->save();
					}

					$PaperworksThumbnailsDirectory = sfConfig::get('app_application_PaperworksThumbnailsDirectory');
					if (!empty($FileData[PaperworkPeer::THUMBNAIL]['tmp_name'])) {
						$TempFileName = $FileData[PaperworkPeer::THUMBNAIL]['tmp_name'];
						$OrigFileName = $FileData[PaperworkPeer::THUMBNAIL]['name'];
						$DestFilename = $sf_web_dir . DIRECTORY_SEPARATOR . $PaperworksThumbnailsDirectory . DIRECTORY_SEPARATOR . $OrigFileName;
						rename($TempFileName, $DestFilename);
						chmod($DestFilename, 0766);
						$this->Paperwork->setThumbnail( $FileData[PaperworkPeer::THUMBNAIL]['name'] );
						$this->Paperwork->save();
					}
					$con->commit();
					$this->getUser()->setFlash('user_message', "Paperwork'" . $this->Paperwork->getCode() . "' was " . (empty($this->paperwork_Code) ? "added." : "edited."));
					$this->redirect('@admin_paperworks?page=' . $this->page . '&sorting=' . $this->sorting);
				}
			}
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executePaperwork_delete($request)
	{
		$IsAdmin = $this->getUser()->hasCredential('Admin');
		if (!$IsAdmin) $this->redirect('@homepage');
		$con = Propel::getConnection(PaperworkPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		$response = $this->context->getResponse();
		try {
			$this->paperwork_code = $request->getParameter('code');
			$this->sorting = $request->getParameter('sorting');
			$this->page = $request->getParameter('page', 1);
			$lPaperwork = PaperworkPeer::retrieveByPK($this->paperwork_code);
			if (empty($lPaperwork)) $this->redirect('@homepage');
			$con->beginTransaction();

			$sf_web_dir = sfConfig::get('sf_web_dir');
			$PaperworksDirectory= sfConfig::get('app_application_PaperworksDirectory' );
			$FileName = $sf_web_dir . DIRECTORY_SEPARATOR . $PaperworksDirectory . DIRECTORY_SEPARATOR . $lPaperwork->getFilename();
			$PaperworksThumbnailsDirectory= sfConfig::get('app_application_PaperworksThumbnailsDirectory' );
			$ThumbnailFileName = $sf_web_dir . DIRECTORY_SEPARATOR . $PaperworksThumbnailsDirectory . DIRECTORY_SEPARATOR . $lPaperwork->getThumbnail();
			$lPaperwork->delete();
			$con->commit();


			if (file_exists($FileName) and strlen($lPaperwork->getFilename()) > 0) {
				unlink($FileName);
			}
			if (file_exists($ThumbnailFileName) and strlen($lPaperwork->getThumbnail()) > 0) {
				unlink($ThumbnailFileName);
			}
			$this->redirect('@admin_paperworks?page=' . $this->page . '&sorting=' . $this->sorting);
		} catch (Exception $lException) {
			$con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	////////////// PAPERWORKS END ////////////////////////

}