<script type="text/javascript" language="JavaScript">
	<!--
	function onSubmit() {
		var theForm = document.getElementById("form_paperwork_edit");
		theForm.submit();
	}

	//-->
</script>

<form action="<?php echo url_for('@admin_paperwork_edit?code='.$paperwork_code.'&page='.$page.'&sorting='.$sorting ) ?>" id="form_paperwork_edit"
			name="form_paperwork_edit"	method="POST" enctype="multipart/form-data" >
	<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">
	<?php echo $form['_csrf_token']->render()?>
	<div class="Div_Editor_Conteiner">
		<br><h2 class="EditorTitle"><?php echo __( ( !empty($paperwork_code) and $paperwork_code!='-')?'Edit':'Add').' '.__('Size Charts') ?></h2>
		<span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

		<table class="Table_Editor_Conteiner">

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[PaperworkPeer::CODE]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ PaperworkPeer::CODE ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ PaperworkPeer::CODE ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[PaperworkPeer::TITLE]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ PaperworkPeer::TITLE ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ PaperworkPeer::TITLE ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>




			<tr>
				<td class="left">
					<?php echo strip_tags( $form[PaperworkPeer::FILENAME]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ PaperworkPeer::FILENAME ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ PaperworkPeer::FILENAME ]->renderError() )  :"" ) ?>
  	      </span><br>
					<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
					$PaperworksDirectory= sfConfig::get('app_application_PaperworksDirectory' );
					if ( !empty($Paperwork) ) {
						$Image= DIRECTORY_SEPARATOR . $PaperworksDirectory . DIRECTORY_SEPARATOR . $Paperwork->getFilename(false);
						if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
							$FileName= DIRECTORY_SEPARATOR.$PaperworksDirectory . DIRECTORY_SEPARATOR . $Paperwork->getFilename();
							echo '<table border="0">';
							echo '</td><td>';
							echo  '<a href="'.$HostForImage . $Image.'" >'.$Image.'</a>' . '</td></tr></table><br><br>';
						}
					}
					?>
				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[PaperworkPeer::THUMBNAIL]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ PaperworkPeer::THUMBNAIL ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ PaperworkPeer::THUMBNAIL ]->renderError() )  :"" ) ?>
  	      </span><br>
					<?php

					$PaperworksThumbnailsDirectory= sfConfig::get('app_application_PaperworksThumbnailsDirectory' );
					// Util::deb($PaperworksThumbnailsDirectory, '$PaperworksDirectory::');
					if ( !empty($Paperwork) ) {
						//				Util::deb($Image, '$Image::');
						$Image= DIRECTORY_SEPARATOR . $PaperworksThumbnailsDirectory . DIRECTORY_SEPARATOR . $Paperwork->getThumbnail(false);
						if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
							$listthb_width= (int)sfConfig::get('app_application_listthb_width' );
							$listthb_height= (int)sfConfig::get('app_application_listthb_height' );
							$FileName= DIRECTORY_SEPARATOR.$PaperworksThumbnailsDirectory . DIRECTORY_SEPARATOR . $Paperwork->getThumbnail();
							// Util::deb($FileName, '$FileName::');
							echo '<table border="0">';
							$NewSize= Util::GetImageShowSize( $FileName, $listthb_width, $listthb_height );
							// Util::deb( $NewSize, '$NewSize::');
							$ImageTag= image_tag(  $HostForImage . $Image.'?p='.time(), array( 'border'=>'none' , 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'] )  );
							$Src= '<a href="'.$HostForImage.$FileName.'" >'.$ImageTag.'</a>'; /* '</td><td align="left" valign="top" >'.'<a class="thickbox" href="'.url_for( "@admin_showimage?type=Beverage&image=" . $FileName ) . '&alt='.$Paperwork->getThumbnail().
                '&height='.( ($listthb_width>$NewSize['OriginalHeight']?$listthb_width:$NewSize['OriginalHeight']) + 10 ).
                '&width='.( ($listthb_height>$NewSize['OriginalWidth']?$listthb_height:$NewSize['OriginalWidth']) + 10 ).
                '" title="'.$Paperwork->getThumbnail().'">'.$ImageTag.'</a> ';*/
							//Util::deb( $Src, '$Src::');
							echo $Src;
							echo '</td><td>';
							echo /* '<a href="'.$HostForImage . $Image.'" >'.$Image.'('.$NewSize['OriginalWidth'].'x'.$NewSize['OriginalHeight'].')'.'</a>' . .
  	          '<br><br>&nbsp;&nbsp;Delete Image&nbsp;:&nbsp;<input type="checkbox" value="1" id="beverage_image_delete" name="beverage_image_delete" >'.*/
							'</td></tr></table>';
						}
					}
					?>

				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[PaperworkPeer::ORDERING]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ PaperworkPeer::ORDERING]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ PaperworkPeer::ORDERING ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td></td>
				<td  style="padding-left:150px;">

					<div id="backend-edit-btn3-div" />
					<?php if( empty($paperwork_code) or $paperwork_code == '-' ) : ?>
						<img id="backend-edit-btn2" src= <?php echo image_path('add-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php else: ?>
						<img id="backend-edit-btn2" src= <?php echo image_path('save-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php endif; ?>
					<img id="backend-edit-btn3" src= <?php echo image_path('cancel-btn.png'); ?>  onclick='javascript:document.location="<?php echo url_for('@admin_paperworks?page='.$page.'&sorting='.$sorting ) ?>"' style="cursor:pointer;" />
	</div>

	</td>
	</tr>
	</table>
	</div>

</form>





