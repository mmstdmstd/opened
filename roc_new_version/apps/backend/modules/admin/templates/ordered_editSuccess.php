<script type="text/javascript" language="JavaScript">
	<!--

	function SaveOrder() {
		var theForm = document.getElementById("form_ordered_edit");
		var checked_backorders = '';
		for (i = 0; i < theForm.elements.length; i++) {
			var FieldName = theForm.elements[i].name
			K = FieldName.indexOf('cbx_backorder_');
			if (K >= 0) {
				//FieldName = VerifyArr[j].substring(0, K);
				var ItemId = FieldName.substring(K + 'cbx_backorder_'.length, FieldName.length);
				checked_backorders = checked_backorders + ItemId + "=" + ( document.getElementById(FieldName).checked ? "1" : "0")+";"
			}

		}
  	document.getElementById("checked_backorders").value= checked_backorders
	  theForm.submit();
	}

	function CancelOrder() {
		document.location = "<?php echo url_for('@admin_ordereds?page=' . $page . '&sorting=' . $sorting .
	'&filter_payment_method=' . (strlen($filter_payment_method) > 0 ? $filter_payment_method : '-') .
	'&filter_status=' . (strlen($filter_status) > 0 ? $filter_status : '-') .
	'&filter_date_from=' . (strlen($filter_date_from) > 0 ? $filter_date_from : '-') .
	'&filter_date_till=' . (strlen($filter_date_till) > 0 ? $filter_date_till : '-') .
	'&filter_state=' . (strlen($filter_state) > 0 ? $filter_state : '-')
) ?>"
	}


	//-->
</script>


<form action="<?php echo url_for('@admin_ordered_edit?id=' . $id . '&page=' . $page . '&sorting=' . $sorting .
	'&filter_payment_method=' . (strlen($filter_payment_method) > 0 ? $filter_payment_method : '-') .
	'&filter_status=' . (strlen($filter_status) > 0 ? $filter_status : '-') .
	'&filter_date_from=' . (strlen($filter_date_from) > 0 ? $filter_date_from : '-') .
	'&filter_date_till=' . (strlen($filter_date_till) > 0 ? $filter_date_till : '-') .
	'&filter_state=' . (strlen($filter_state) > 0 ? $filter_state : '-')
) ?>" id="form_ordered_edit" method="POST">
	<input type="hidden" value="" id="checked_backorders" name="checked_backorders" >
	<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">

	<div style=background-color:#ffffff;>
		<p class="backend-heading">View Order:&nbsp;#<?php echo $Ordered->getId() ?></p>

		<table id="order_view_table">
			<tr>
				<td class="order_view1">User:</td>
				<td class="order_view2"><?php $lSfGuardUser = $Ordered->getSfGuardUser();
					if (!empty($lSfGuardUser)) echo $Ordered->getSfGuardUser() ?></td>
				<td class="order_view3">Status:</td>
				<td class="order_view4"><b><?php echo $Ordered->getStatusAsText() ?></b></td>
			</tr>
			<tr>
				<td class="order_view1">Email:</td>
				<td class="order_view2"><?php if (!empty($lSfGuardUser)) echo $lSfGuardUser->getUsername() ?></td>
				<td class="order_view3">Created:</td>
				<td class="order_view4"><?php echo $Ordered->getCreatedAt(sfConfig::get('app_application_date_time_format')) ?></td>
			</tr>
			<tr>
				<td class="order_view1"></td>
				<td class="order_view2"></td>
				<td class="order_view3"></td>
				<td class="order_view4"></td>
			</tr>
		</table>

		<table id="order_view_table2">
			<tr>
				<td colspan="6" class="order_view5"></td>
			</tr>
			<tr>
				<td colspan="2" class="order_view16"><b>Payment Info</b></td>
				<td colspan="2" class="order_view16"><b>Billing Address</b></td>
				<td colspan="2" class="order_view16"><b>Shipping Address</b></td>
			</tr>
			<tr>
				<td class="order_view6">Transaction Id:</td>
				<td class="order_view7"><b><?php echo $Ordered->getTransactionId() ?></b></td>
				<td class="order_view3">Address:</td>
				<td class="order_view8"><?php echo $Ordered->getBStreet() ?></td>
				<td class="order_view3">Address:</td>
				<td class="order_view9"><?php echo $Ordered->getSStreet() ?></td>
			</tr>
			<tr>
				<td class="order_view6">Subtotal:</td>
				<td class="order_view7"><b><?php echo Util::getDigitMoney($Ordered->getTotalPrice(), 'Money') ?></b></td>
				<td class="order_view3">City:</td>
				<td class="order_view8"><?php echo $Ordered->getBCity() ?></td>
				<td class="order_view3">City:</td>
				<td class="order_view9"><?php echo $Ordered->getSCity() ?></td>
			</tr>
			<tr>
				<td class="order_view6">Shipping</td>
				<td class="order_view7"><b>$<?php echo $Ordered->getShippingCost() ?></b></td>
				<td class="order_view3">State:</td>
				<td class="order_view8"><?php echo $Ordered->getBState() ?></td>
				<td class="order_view3">State:</td>
				<td class="order_view9"><?php echo $Ordered->getSState() ?></td>
			</tr>
			<tr>
				<td class="order_view6">Tax</td>
				<td class="order_view7"><b><?php echo Util::getDigitMoney($Ordered->getTax(), 'Money') ?></b></td>
				<td class="order_view3">Zip:</td>
				<td class="order_view8"><?php echo $Ordered->getBZip() ?></td>
				<td class="order_view3">Zip:</td>
				<td class="order_view9"><?php echo $Ordered->getSZip() ?></td>
			</tr>
			<tr>
				<td class="order_view6">Total</td>
				<td class="order_view7"><b><?php echo Util::getDigitMoney($Ordered->getTotalPrice() + $Ordered->getShippingCost() + $Ordered->getTax(), 'Money') ?></b>
				</td>
				<td class="order_view3">Country:</td>
				<td class="order_view8"><?php echo $Ordered->getBCountry() ?></td>
				<td class="order_view3">Country:</td>
				<td class="order_view9"><?php echo $Ordered->getSCountry() ?></td>
			</tr>
			<tr>
				<td class="order_view6">Payment Method</td>
				<td class="order_view7"><b><?php echo $Ordered->getPaymentMethodAsText() ?></b></td>
				<td class="order_view3"></td>
				<td class="order_view8"></td>
				<td class="order_view3"></td>
				<td class="order_view9"></td>
			</tr>
			<tr>
				<td colspan="6" id="order_view10"></td>
			</tr>

			<tr>
				<td colspan="6" class="order_view5"></td>
			</tr>
		</table>

		<table id="order_view_table3">
			<tr>
				<td class="order_view11 order_view16"><b>Order Details</b></td>
				<td class="order_view12"></td>
				<td class="order_view13"></td>
				<td class="order_view14"></td>
				<td class="order_view15"></td>
			</tr>
			<tr>
				<td class="order_view11"><b>Items:&nbsp;<?php echo count($OrderedItemList) ?></b></td>
				<td class="order_view12"></td>
				<td class="order_view13"></td>
				<td class="order_view14"></td>
				<td class="order_view15"></td>
			</tr>
			<tr>
				<td class="order_view11"></td>
				<td class="order_view12"></td>
				<td class="order_view13"></td>
				<td class="order_view14"></td>
				<td class="order_view15"></td>
			</tr>
			<tr>
				<td class="order_view11"></td>
				<td class="order_view12"></td>
				<td class="order_view13"></td>
				<td class="order_view14"></td>
				<td class="order_view15"></td>
			</tr>
			<tr>
				<td class="order_view11">Product (sku)</td>
				<td class="order_view12">Unit</td>
				<td class="order_view13">Qty</td>
				<td class="order_view14">Price</td>
				<td class="order_view15">Backorder</td>
			</tr>
			<?php $SubtotalSum = 0;
			foreach ($OrderedItemList as $OrderedItem) : ?>
			<tr>
				<td class="order_view11"><b><?php $lProduct = InventoryItemPeer::retrieveByPK($OrderedItem->getSku());
						echo (!empty($lProduct) ? $lProduct : '') . ' (' . $OrderedItem->getSku() . ')' ?></b></td>
				<td class="order_view12 order_view17"><b><?php echo Util::getDigitMoney($OrderedItem->getUnitPrice(), 'Money') ?></b></td>
				<td class="order_view13"><b><?php echo $OrderedItem->getQty() ?></b></td>
				<td class="order_view14"><b><?php echo Util::getDigitMoney($OrderedItem->getQty() * $OrderedItem->getUnitPrice(), 'Money') ?></b></td>
				<td class="order_view15"><input type="checkbox" name="cbx_backorder_<?php echo $OrderedItem->getId() ?>" id="cbx_backorder_<?php echo $OrderedItem->getId() ?>"
																				value="" <?php echo($OrderedItem->getBackorder() ? 'checked="checked"' : "") ?> ></td>
				<?php $SubtotalSum += $OrderedItem->getQty() * $OrderedItem->getUnitPrice();
				endforeach ?>
			</tr>
			<tr>
				<td colspan="5" class="order_view18"></td>
			</tr>
			<tr>
				<td class="order_view11"></td>
				<td class="order_view12 order_view17"></td>
				<td class="order_view13"><b>Subtotal:</b></td>
				<td class="order_view14"><b><?php echo Util::getDigitMoney($SubtotalSum, 'Money') ?></php></b></td>
				<td class="order_view15"></td>
			</tr>

      <tr>
        <td>
      		<div id="backend-edit-btn7-div" />
		        <img id="backend-edit-btn2" src= "<?php echo image_path('save-btn.png'); ?>" onclick="javascript:SaveOrder()" />
		        <img id="backend-edit-btn3" src= "<?php echo image_path('cancel-btn.png'); ?>"  onclick="javascript:CancelOrder()" />
	         </div>
	         <a id="back_to_link" onclick="javascript:CancelOrder()" >&lt; Back to Orders</a>
        </td>
      </tr>
		</table>
	 </div>
</form>