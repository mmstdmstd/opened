  <script type="text/javascript" language="JavaScript">
  <!--
  function onSubmit() {
    var theForm = document.getElementById("form_youtube_video_edit");
    theForm.submit();
  }

  //-->
  </script>

<form action="<?php echo url_for('@admin_youtube_video_edit?key='.$youtube_video_key.'&page='.$page.'&sorting='.$sorting ) ?>" id="form_youtube_video_edit" method="POST" enctype="multipart/form-data" >
<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">
<?php echo $form['_csrf_token']->render()?>
<div class="Div_Editor_Conteiner">
  <br><h2 class="EditorTitle"><?php echo __( ( !empty($youtube_video_key) and $youtube_video_key!='-')?'Edit':'Add').' '.__('YouTube Video') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner">

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[YoutubeVideoPeer::KEY]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ YoutubeVideoPeer::KEY ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ YoutubeVideoPeer::KEY ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[YoutubeVideoPeer::TITLE]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ YoutubeVideoPeer::TITLE ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ YoutubeVideoPeer::TITLE ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[YoutubeVideoPeer::URL]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left">
         	<?php echo $form[ YoutubeVideoPeer::URL ]->render(); ?><br>

          <?php if( !empty($youtube_video_key) and $youtube_video_key != '-' ) :
          $Url= $YoutubeVideo->getUrl();
            if ( !empty($Url) ) :
            ?>
            <iframe width="276" height="217" src="<?php echo $Url ?>" frameborder="0" allowfullscreen>
            </iframe>
            <?php endif; ?>
          <?php endif; ?>

  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ YoutubeVideoPeer::URL ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[YoutubeVideoPeer::ORDERING]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ YoutubeVideoPeer::ORDERING]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ YoutubeVideoPeer::ORDERING ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[YoutubeVideoPeer::DESCRIPTION]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ YoutubeVideoPeer::DESCRIPTION]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ YoutubeVideoPeer::DESCRIPTION ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>



    <tr>
    <td></td>
      <td  style="padding-left:150px;">
				<div id="backend-edit-btn3-div" />
        <?php if( empty($youtube_video_key) or $youtube_video_key == '-' ) : ?>
        <!--  <input type="button" id="action_submit" value="Add" onclick='javascript:onSubmit()' /> -->
					<img id="backend-edit-btn2" src= <?php echo image_path('add-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php else: ?>
        <!-- <input type="button" id="action_submit" value="Update" onclick='javascript:onSubmit()' />  -->
				<img id="backend-edit-btn2" src= <?php echo image_path('save-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
      <?php endif; ?>
      <!-- <input type="button" id="action_submit" value="Cancel" onclick='javascript:document.location="<?php echo url_for('@admin_youtube_videos?page='.$page.'&sorting='.$sorting ) ?>"' />  -->
				<img id="backend-edit-btn3" src= <?php echo image_path('cancel-btn.png'); ?>  onclick='javascript:document.location="<?php echo url_for('@admin_youtube_videos?page='.$page.'&sorting='.$sorting ) ?>"' style="cursor:pointer;" />
        </div>
      </td>
    </tr>
  </table>
</div>

</form>





