<script type="text/javascript" language="JavaScript">
	<!--

	function onSubmit() {
		var theForm = document.getElementById("form_paperworks");
		theForm.submit();
	}

	function onDelete(Code) {
		if ( !confirm("Do you want to delete link to this Paperwork ?") ) return;
		var HRef= '<?php echo url_for('@admin_paperwork_delete?page='.$page.'&code=') ?>' + Code
		document.location= HRef;
	}

	//-->
</script>


<div id="content-div">
	<p class="backend-heading3">Paperworks</p>
	<div class="backend-addnew-btn-div" onclick='javascript:document.location="<?php echo url_for('@admin_paperwork_edit?code=-&page='.$page.'&sorting='.$sorting )?>"'/>
	<?php echo image_tag("addnew-btn.png", array( 'class'=>'backend-addnew-btn' )); ?>
</div>


<form action="<?php echo url_for('@admin_paperworks?page='.$page . '&sorting='.$sorting  ) ?>" id="form_paperworks" method="POST">
	<?php if( $PaperworksPager->getNbResults()> 0 ) : ?>




		<input type="hidden" id="page" name="page" value="<?php echo $page ?>" >
		<p class="page_number">Page&nbsp;<?php echo $page ?></p>
		<table class="table13" cellspacing="0" cellpadding="0" id="table12">

			<tr id="table12-heading">
				<td id="table12-h1">
				</td>
				<td class="table12-h2a"><?php echo link_to(__('Key'), '@admin_paperworks?page='.$page.'&sorting=CODE' ) ?></td>
				<td class="table12-h2"><?php echo link_to(__('Title'), '@admin_paperworks?page='.$page.'&sorting=TITLE' ) ?></td>
				<td id="table12-h3">Filename</td>
				<td id="table12-h3">Thumbnail</td>
				<td id="table12-h4"><?php echo link_to(__('Order'), '@admin_paperworks?page='.$page.'&sorting=ORDERING' ) ?></td>
				<td id="table12-h14"></td>
			</tr>

			<?php $RowNumber=0; foreach( $PaperworksPager->getResults() as $Paperwork ) : ?>
				<tr id="admin_row_<?php echo ($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber?>"
						class="admin_row_<?php echo $RowNumber%2==0 ?'even':'odd'?>"
						onmouseover="AdminTROver( '<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber ?>' );"
						onmouseout="AdminTROut('<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber  ?>' );" >

					<td valign="top" class="col_1 alignment">
						<div class="backend-edit-btn-div" onclick='javascript:document.location="<?php echo url_for('@admin_paperwork_edit?code='. $Paperwork->getCode() .'&page='.$page.'&sorting='.$sorting , array(  )  )?>"' />
						<?php echo image_tag("edit-btn.png", array( 'class'=>'backend-edit-btn' )); ?>
						</div>
					</td>


					<td align="center" valign="top" class="col_pad">
						<?php echo $Paperwork->getCode() ?>
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $Paperwork->getTitle()?>&nbsp;
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $Paperwork->getFilename()?>&nbsp;
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $Paperwork->getThumbnail()?>&nbsp;
					</td>

					<td align="center" valign="top" class="col_pad_center">
						<?php echo $Paperwork->getOrdering()?>&nbsp;
					</td>

					<!--
  	  <td align="center">
        <?php
          echo link_to( __("Edit"), '@admin_paperwork_edit?code='. $Paperwork->getCode().'&page='.$page.'&sorting='.$sorting, array() )
        ?>
      </td>
-->

					<td align="center" valign="top" class="col_14 col_pad" >
						<!--
        <a style="cursor:pointer;" onclick="javascript:onDelete('<?php echo $Paperwork->getCode() ?>')" ><?php echo __("Delete") ?></a>
-->
						<div class="backend-del-btn-div" />
						<input class="backend-del-btn" id="" onclick="javascript:onDelete('<?php echo $Paperwork->getCode() ?>'); return false; "  src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/delete-btn.png" type="image" value="" />
						</div>
					</td>

				</tr>

				<?php $RowNumber++;  endforeach; ?>

			<tr>
				<td colspan="8" style="border-bottom:1px solid #888687;">
				</td>
			</tr>
		</table>
		<p class="page_number">Page&nbsp;<?php echo $page ?></p>

		<!--
<div class="add-new-item"><?php echo link_to(__('Add new'),'@admin_paperwork_edit?code=-&page='.$page.'&sorting='.$sorting) ?></div>
-->

	<?php endif ?>

	<?php if ($PaperworksPager->haveToPaginate()): ?>
		<?php echo link_to('&laquo;', '@admin_paperworks?page=1&sorting='.$sorting ) ?> &nbsp;
		<?php $links = $PaperworksPager->getLinks();
		foreach ($links as $page): ?>
			<?php echo ($page == $PaperworksPager->getPage()) ? $page : link_to($page, '@admin_paperworks?page='.$page.'&sorting='.$sorting ) ?>
			<?php if ($page != $PaperworksPager->getCurrentMaxLink()): ?>
				&nbsp;
			<?php endif ?>
		<?php endforeach ?>
		&nbsp; <?php echo link_to('&raquo;', '@admin_paperworks?page='.$page.'&sorting='.$sorting ) ?>
	<?php endif ?>


	<?php if( $PaperworksPager->getNbResults()== 0 ) : ?>
		<div class="nothing-found">
			<?php echo __("No Paperworks") ?>
		</div>
		<div class="add-new-item">
			<?php echo link_to(__('Add new'),'@admin_paperwork_edit?code=-&page='.$page.'&sorting='.$sorting ) ?>
		</div>
	<?php endif ?>




</form>

</div><!-- close content div -->