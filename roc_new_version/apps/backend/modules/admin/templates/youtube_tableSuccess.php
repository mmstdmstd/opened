<style>
/* youtube table */
.backend-heading {
  font-size:20px;
  font-weight:bold;
  padding:16px;
  margin:0;
}
#table13 {
  margin:0 auto 8px;
  width:97%;
}
#table12 {
/*  margin:50px auto 8px;*/
  width:97%;
  font-family:"arial",sans-serif;
  font-size:12px;
  margin:50px 0 8px 0px;
}
#table12-h1{
  width:50px;
  border-top:solid 1px #888687;
  border-left:solid 1px #888687;
  border-bottom:solid 1px #888687;
  text-align:center;
}
.table12-h2a{
/*  width:173px; */
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  width:40px;
  text-align:center;
}
.table12-h2a a {
  color:#ffffff;
}
.table12-h2{
/*  width:173px; */
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  width:142px;
}
.table12-h2 a {
  color:#ffffff;
}
#table12-h3{
/*  width:235px;*/
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  width:175px;
}
.table12-h4 a {
  color:#ffffff;
}
#table12-h4{
/*  width:111px;*/
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  width:41px;
  text-align:center;
}
#table12-h4 a {
  color:#ffffff;
}
#table12-h5{
  width:235px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h5 a {
  color:#ffffff;
}
#table12-h6{
  width:52px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h7{
/*  width:220px;*/
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h8{
  width:104px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h9{
  width:103px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h10{
  width:64px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h11{
/*  width:159px; */
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h12{
/*  width:151px;  */
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
}
#table12-h13{
/*  width:151px;  */
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  width:90px;
}
#table12-h13 a {
  color:#ffffff;
}
#table12-h14{
  width:46px;
  border-top:solid 1px #888687;
  border-bottom:solid 1px #888687;
  border-right:solid 1px #888687;
  text-align:center;
}
#table12-heading{
  background-color:#58595b;
  height:22px;
  color:#ffffff;
}
.odd-row{
  height:23px;
  background-color:#ffffff;
}
.even-row{
  height:23px;
  background-color:#f9f5e9;
}
.col_1{
  border-left:solid 1px #888687;
}
.col_14{
  text-align:center;
  border-right:solid 1px #888687;
}
#field10a_div {
  width:98%;
/*  height:180px;    */
  background-color:#edecea;
  margin:20px auto 0;
  position:relative;
  padding-top:14px;
  padding-bottom:18px;
}
.sub-heading0 {
  width:98%;
  margin:0 auto;
  padding-top:20px;
  padding-bottom:15px;
  line-height:18px;
}
.sub-heading4 {
  margin-left:0px;
  color:#000;
}
.sub-heading4a {
  position:absolute;
  left:175px;
}
.sub-heading5 {
  width:16px;
  height:16px;
  border:solid 1px #888687;
  float:right;
  text-align:center;
  background-color:#fff;
  line-height:16px;
  margin-right:15px;
  position:relative;
  top:2px;
}
.sub-heading6 {
  float:right;
  color:#000;
  margin-top:2px;
  margin-right:15px;
}
.sub-heading7 {
  float:right;
  color:#7b7b79;
  margin-top:2px;
  margin-right:15px;
}
.row_border{
  height:0px;
  border-top:solid 1px #888687;
}
.list_title_link {
  color:#ffffff;
}
.admin_row_even {
/*  background-color: #fbfbfa;*/
  background-color: #ffffff;
  line-height: 1.2em;
}
.admin_row_odd {
/*  background-color: #fbfbfa;*/
  background-color: #e6e7e9;
  line-height: 1.2em;
}
.admin_row_hover {
  background-color: #fffaa2;
/*  line-height: 1.2em; */
  height:23px;
}
.admin_row2_even {
  height:23px;
/*  background-color:#f9f5e9;*/
  background-color:#e6e7e9;
}
.admin_row2_odd {
  height:23px;
  background-color:#ffffff;
}
.admin_row2_hover {
  background-color: #fffaa2;
  height:23px;
}
.alignment{
  text-align:center;  
  padding-top:8px;
}
.alignment2{
  padding-top:8px;
  padding-bottom:11px;
}
.backend-edit-btn-div {
  position:relative;
  width:50px;
  height:12px;
  margin:0 auto;
  overflow:hidden;
  top:2px;
}
.backend-edit-btn {
  position: absolute;
  left:0px;
  top:0px;
}
.backend-edit-btn:hover {
  top:-12px;
}
#backend-edit-btn2-div {
  position:relative;
  width:90px;
  height:20px;
  overflow:hidden;
  margin:0;
}
#backend-edit-btn2 {
  position:absolute;
  left:0px;
  top:0px;
}
#backend-edit-btn2:hover {
  top:-20px;
}
</style>
<div id="content-div">
		<p class="backend-heading3">Videos</p>
    <div id="backend-edit-btn2-div" onclick='alert("Add New Button Pressed")'; />
      <img id="backend-edit-btn2" src="addnew-btn.png" />       
     <input class="print_btn" id="backend-edit-btn2" onclick="javascript:MakePrint(); return false;"  src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/addnew-btn.png" type="image" value="Print" />
    </div>









      <!-- sz table starts here -->
        <table class="table13" cellspacing="0" cellpadding="0" id="table12">
          <tr id="table12-heading">
            <td id="table12-h1">
            </td>
            <td class="table12-h2a"><?php echo link_to(__('Key'), '@admin_youtube_videos?page='.$page.'&sorting=KEY' ) ?></td>
            <td class="table12-h2"><?php echo link_to(__('Title'), '@admin_youtube_videos?page='.$page.'&sorting=TITLE' ) ?></td>
            <td id="table12-h3"><a class="list_title_link" href="">URL</a></td>
            <td id="table12-h4"><?php echo link_to(__('Order'), '@admin_youtube_videos?page='.$page.'&sorting=ORDERING' ) ?></td>
            <td id="table12-h5"><?php echo link_to(__('Description'), '@admin_youtube_videos?page='.$page.'&sorting=DESCRIPTION' ) ?></td>
            <td id="table12-h13"><?php echo link_to(__('Created at'), '@admin_youtube_videos?page='.$page.'&sorting=CREATED_AT' ) ?></td>
            <td id="table12-h14"></td>
          </tr>


  <?php $RowNumber=0; foreach( $YoutubeVideosPager->getResults() as $YoutubeVideo ) : ?>
    <tr id="admin_row_<?php echo ($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber?>"
   class="admin_row_<?php echo $RowNumber%2==0 ?'even':'odd'?>"
   onmouseover="AdminTROver( '<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber ?>' );"
   onmouseout="AdminTROut('<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber  ?>' );" >

            <td valign="top" class="col_1 alignment">
              <div class="backend-edit-btn-div" />
                <input class="backend-edit-btn" id="backend-edit-btn" onclick="location.href='http://www.oherron.com/backend.php/admin/youtube_video_edit/key/tsg/page?sorting=';" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/edit-btn.png" type="image" value=""  />      
              </div>
            </td>

      <td align="left" style="padding-left:10px;">
        <?php echo $YoutubeVideo->getKey() ?>
      </td>

      <td align="left">
        <?php echo $YoutubeVideo->getTitle()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $YoutubeVideo->getUrl()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $YoutubeVideo->getOrdering()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $YoutubeVideo->getDescription()?>&nbsp;
      </td>


      <td align="center">
        <?php echo $YoutubeVideo->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) ?>
      </td>
<!--
  	  <td align="center">
        <?php
          echo link_to( __("Edit"), '@admin_youtube_video_edit?key='. $YoutubeVideo->getKey().'&page='.$page.'&sorting='.$sorting, array() )
        ?>
      </td>
-->

      <td align="center" >
              <div class="backend-edit-btn-div" />
                <input class="backend-edit-btn" id="backend-edit-btn" onclick="javascript:onDelete('<?php echo $YoutubeVideo->getKey() ?>')" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/delete-btn.png" type="image" value=""  />      
              </div>

<!--
        <a style="cursor:pointer;" onclick="javascript:onDelete('<?php echo $YoutubeVideo->getKey() ?>')" ><?php echo __("Delete") ?></a>
-->
      </td>

    </tr>

  <?php $RowNumber++;  endforeach; ?>






<!-- begin odd row sz -->
          <tr id="admin_row2_even_0"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_0' );"
            onmouseout="AdminTROut('admin_row2_even_0' );" >

            <td valign="top" class="col_1 alignment">
              <div class="backend-edit-btn-div" />
                <input class="backend-edit-btn" id="backend-edit-btn" onclick="location.href='http://example';" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/edit-btn.png" type="image" value=""  />      
              </div>
            </td>
            <td valign="top" class="alignment">cew</td>
            <td valign="top" class="alignment2">Taser CEW Technology</td>
            <td valign="top" class="alignment2">http://www.youtube.com/embed/DRq2evgzrsY</td>
            <td valign="top" class="alignment">1</td>
            <td valign="top" class="alignment2">See the evolution of Taser's CEWs to today's smart taser technology.</td>
            <td valign="top" class="alignment2">2013-02-18 22:2</td>
            <td valign="top" class="col_14 alignment">
              <div class="backend-edit-btn-div" onclick='alert("Delete Button Pressed")'; />
                <img class="backend-edit-btn" src="delete-btn.png" />       
              </div>
            </td>
          </tr>
          <tr id="admin_row2_odd_1"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_1' );"
            onmouseout="AdminTROut('admin_row2_odd_1' );" >

            <td class="col_1 alignment" valign="top">
              <div class="backend-edit-btn-div" onclick='alert("Edit Button Pressed")'; />
                <img class="backend-edit-btn" src="edit-btn.png" />       
              </div>
            </td>
            <td valign="top" class="alignment">erg</td>
            <td valign="top" class="alignment2">The CTAV by CORTAC</td>
            <td valign="top" class="alignment2">http://www.youtube.com/embed/DRq2evgzrsY</td>
            <td valign="top" class="alignment">2</td>
            <td valign="top" class="alignment2">See the evolution of Taser's CEWs to today's smart taser technology.</td>
            <td valign="top" class="alignment2">2013-02-18 22:2</td>
            <td valign="top" class="col_14 alignment">
              <div class="backend-edit-btn-div" onclick='alert("Delete Button Pressed")'; />
                <img class="backend-edit-btn" src="delete-btn.png" />       
              </div>
            </td>
          </tr>
          <tr id="admin_row2_even_2"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_2' );"
            onmouseout="AdminTROut('admin_row2_even_2' );" >


            <td class="col_1 alignment" valign="top">
              <div class="backend-edit-btn-div" onclick='alert("Edit Button Pressed")'; />
                <img class="backend-edit-btn" src="edit-btn.png" />       
              </div>
            </td>
            <td valign="top" class="alignment">cew</td>
            <td valign="top" class="alignment2">Garrett's PD6500i Walk-Through Metal Detector</td>
            <td valign="top" class="alignment2">http://www.youtube.com/embed/DRq2evgzrsY</td>
            <td valign="top" class="alignment">3</td>
            <td valign="top" class="alignment2">See the evolution of Taser's CEWs to today's smart taser technology.</td>
            <td valign="top" class="alignment2" >2013-02-18 22:2</td>
            <td valign="top" class="col_14 alignment">
              <div class="backend-edit-btn-div" onclick='alert("Delete Button Pressed")'; />
                <img class="backend-edit-btn" src="delete-btn.png" />       
              </div>
            </td>
          </tr>
          
<!-- add border to last row-->
          <tr>
            <td colspan="15" class="row_border"></td>
          </tr>
        </table>
<!-- sz table ends here -->



</div>

<!-->
