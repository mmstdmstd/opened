  <script type="text/javascript" language="JavaScript">
  <!--
  function onDelete(inventory_item_id) {
    if ( !confirm("Do you want to delete this Inventory Item with all related data ?") ) return;
    var Url= '<?php echo url_for('@admin_inventory_item_delete?id=') ?>' + inventory_item_id+'<?php echo 
    (  strlen($filter_title)> 0 ? '/filter_title/'.$filter_title : '' ).
    (  strlen($filter_sku)> 0 ? '/filter_sku/'. $filter_sku : '' ) .     
    (  strlen($filter_brand_id)> 0 ? '/filter_brand_id/'. $filter_brand_id : '' ) . 
    (  strlen($filter_qty_on_hand_min)> 0 ? '/filter_qty_on_hand_min/'. $filter_qty_on_hand_min : '' ) .     
    (  strlen($filter_qty_on_hand_max)> 0 ? '/filter_qty_on_hand_max/'. $filter_qty_on_hand_max : '' ) .     
    (  strlen($filter_std_unit_price_min)> 0 ? '/filter_std_unit_price_min/'. $filter_std_unit_price_min : '' ) . 
    (  strlen($filter_std_unit_price_max)> 0 ? '/filter_std_unit_price_max/'. $filter_std_unit_price_max : '' ) .       
    '/sorting/'.$sorting ?>'
    document.location= Url
  }
   
  function onSubmit() {
    var theForm = document.getElementById("form_inventory_items");
    theForm.submit();
  }
  //-->
  </script>
    
<div class="Div_ListConteiner">
<br><h2 class="ListTitle"><?php echo __("List of Inventory Items" ).' <small>( '.$InventoryItemsPager->getNbResults().'/'.
InventoryItemPeer::getInventoryItems(1,false, true, '','', '', '','',false, '','','', '', '', '', '', '' ).' )</small>'; ?></h2>


<form action="<?php echo url_for('@admin_inventory_items?page='.$page.
( strlen($filter_title) > 0 ? '&filter_title=' . $filter_title : '' ).
( strlen($filter_sku) > 0? '&filter_sku=' . $filter_sku : '' ) . 
( strlen($filter_brand_id) > 0 ? '&filter_brand_id=' . $filter_brand_id : '' ).
( strlen($filter_qty_on_hand_min) > 0? '&filter_qty_on_hand_min=' . $filter_qty_on_hand_min : '' ) . 
( strlen($filter_qty_on_hand_max) > 0? '&filter_qty_on_hand_max=' . $filter_qty_on_hand_max : '' ) . 
( strlen($filter_std_unit_price_min) > 0? '&filter_std_unit_price_min=' . $filter_std_unit_price_min : '' ) . 
( strlen($filter_std_unit_price_max) > 0? '&filter_std_unit_price_max=' . $filter_std_unit_price_max : '' ) . 
'&sorting='.$sorting  ) ?>" id="form_inventory_items" method="POST">
  
  <table class="Table_Filter" border="0" >
    <tr valign="top">    
      <td class="left" >      
        <?php  echo strip_tags( $form['filter_title']->renderLabel() ) ?>&nbsp;:
       	<?php  echo $form[ 'filter_title' ]->render(); ?><br>
      </td>
      <td class="left" >      
        <?php  echo strip_tags( $form['filter_sku']->renderLabel() ) ?>&nbsp;:
       	<?php  echo $form[ 'filter_sku' ]->render(); ?>
      </td>
      <td class="left" >      
        <?php  echo strip_tags( $form['filter_brand_id']->renderLabel() ) ?>&nbsp;:
       	<?php  echo $form[ 'filter_brand_id' ]->render(); ?><br>
      </td>
    </tr>    
    
    <tr valign="top">    
      <td class="left" >
        <?php echo strip_tags( $form['filter_qty_on_hand_min']->renderLabel() ) ?>&nbsp;:
       	<?php echo $form[ 'filter_qty_on_hand_min' ]->render(); ?>
      </td>      
      <td class="left" >      
        <?php echo strip_tags( $form['filter_qty_on_hand_max']->renderLabel() ) ?>&nbsp;:
       	<?php echo $form[ 'filter_qty_on_hand_max' ]->render(); ?><br>
      </td>
      <td class="right">
        &nbsp;
      </td>      
    </tr>    
    
    <tr valign="top">    
      <td class="left" >
        <?php echo strip_tags( $form['filter_std_unit_price_min']->renderLabel() ) ?>&nbsp;:
       	<?php echo $form[ 'filter_std_unit_price_min' ]->render(); ?>
      </td>      
      <td class="left" >      
        <?php echo strip_tags( $form['filter_std_unit_price_max']->renderLabel() ) ?>&nbsp;:
       	<?php echo $form[ 'filter_std_unit_price_max' ]->render(); ?><br>
      </td>
      <td class="right" colspan="5">
        <input type="button" id="action_submit" value="Filter" onclick='javascript:onSubmit()' />
      </td>      
    </tr>    

    
    
  </table>  
<?php if( $InventoryItemsPager->getNbResults()> 0 ) : ?>

  
  <input type="hidden" id="page" name="page" value="<?php echo $page ?>" >
  <div>
  <table class="Table_List" width="1150px" style="margin-left:5px;">
    
    <tr >
      <th>        
        <?php 
        echo link_to(__('Title'), '@admin_inventory_items?page='.$page.'&sorting=TITLE&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
      </th>
      
      <th>
        <?php echo link_to(__('SKU'), '@admin_inventory_items?page='.$page.'&sorting=SKU&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
      </th>
      
      <th>
        <?php echo link_to(__('Brand'), '@admin_inventory_items?page='.$page.'&sorting=BRAND&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max ) ?>
      </th>
      
      
      <th>
        <?php echo link_to(__('Qty On Hand'), '@admin_inventory_items?page='.$page.'&sorting=QTY_ON_HAND&filter_title='.$filter_title . '&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id . '&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max ) ?>
      </th>
      
      <th>
        <?php echo link_to(__('Std Unit Price'), '@admin_inventory_items?page='.$page.'&sorting=STD_UNIT_PRICE&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max ) ?>
      </th>
        

      <th>
        <?php echo link_to(__('Updated at'), '@admin_inventory_items?page='.$page.'&sorting=UPDATED_AT&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
      </th>
      <th>&nbsp;
        
      </th>
      <th>&nbsp;        
      </th>
    </tr>
  <?php $RowNumber=0; foreach( $InventoryItemsPager->getResults() as $InventoryItem ) : ?>
    <tr id="admin_row_<?php echo ($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber?>" 
   class="admin_row_<?php echo $RowNumber%2==0 ?'even':'odd'?>"
   onmouseover="AdminTROver( '<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber ?>' );" 
   onmouseout="AdminTROut('<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber  ?>' );" >      
      <td align="left" style="padding-left:10px;"> 
        <?php echo $InventoryItem?>&nbsp;        
      </td>

      <td align="left" > 
        <?php echo $InventoryItem->getSku()?>&nbsp;        
      </td>

      <td align="left" > 
        <?php 
        $lBrand= $InventoryItem->getBrand(); 
        if ( !empty($lBrand) ) {
        	echo $lBrand;
        }
        ?>&nbsp;        
      </td>

      <td align="right" > 
        <?php echo $InventoryItem->getQtyOnHand()?>&nbsp;        
      </td>

      <td align="right" > 
        <?php echo $InventoryItem->getStdUnitPrice()?>&nbsp;
      </td>
                  
      <td align="center"> 
        <?php echo $InventoryItem->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) ?>               
      </td>
      
  	  <td align="center">
        <?php 
          echo link_to( __("Edit"), '@admin_inventory_item_edit?id='. $InventoryItem->getSku().'&page='.$page.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.'&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max.
'&sorting='.$sorting, array() )
        ?>
      </td>   
      
      <td align="center" >
        <a style="cursor:pointer;" onclick="javascript:onDelete(<?php echo $InventoryItem->getSku() ?>)" ><?php echo __("Delete") ?></a>
      </td>   
         
    </tr>        

  <?php $RowNumber++;  endforeach; ?>
  </table>  
  </div>
  
<div class="add-new-item"><?php echo link_to(__('Add new'),'@admin_inventory_item_edit?id=0&page='.$page.'&sorting='.$sorting.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min. '&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?></div>
 
<?php endif ?>      

<?php if ($InventoryItemsPager->haveToPaginate()): ?>
  <?php echo link_to('&laquo;', '@admin_inventory_items?page=1&sorting='.$sorting.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min. '&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
 '&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max.
 '&sorting='.$sorting ) ?> &nbsp;
  <?php $links = $InventoryItemsPager->getLinks(); 
  foreach ($links as $page): ?>
    <?php echo ($page == $InventoryItemsPager->getPage()) ? $page : link_to($page, '@admin_inventory_items?page='.$page.'&sorting='.$sorting.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min. '&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
    <?php if ($page != $InventoryItemsPager->getCurrentMaxLink()): ?>
      &nbsp;
    <?php endif ?>
  <?php endforeach ?>
  &nbsp; <?php echo link_to('&raquo;', '@admin_inventory_items?page='.$page.'&sorting='.$sorting.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min.  '&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
<?php endif ?>


<?php if( $InventoryItemsPager->getNbResults()== 0 ) : ?>
  <div class="nothing-found">
  <?php echo __("No Inventory Items") ?>
  </div>
  <div class="add-new-item">
  <?php echo link_to(__('Add new'),'@admin_inventory_item_edit?id=0&page='.$page.'&sorting='.$sorting.'&filter_title='.$filter_title.
'&filter_sku='.$filter_sku.'&filter_brand_id='.$filter_brand_id.
'&filter_qty_on_hand_min='.$filter_qty_on_hand_min. '&filter_qty_on_hand_max='.$filter_qty_on_hand_max.
'&filter_std_unit_price_min='.$filter_std_unit_price_min.'&filter_std_unit_price_max='.$filter_std_unit_price_max ) ?>
  </div>
<?php endif ?>

</div>


</form>