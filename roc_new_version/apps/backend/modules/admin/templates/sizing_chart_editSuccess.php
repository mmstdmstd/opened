<script type="text/javascript" language="JavaScript">
	<!--
	function onSubmit() {
		var theForm = document.getElementById("form_sizing_chart_edit");
		theForm.submit();
	}

	//-->
</script>

<form action="<?php echo url_for('@admin_sizing_chart_edit?key='.$sizing_chart_key.'&page='.$page.'&sorting='.$sorting ) ?>" id="form_sizing_chart_edit"
	name="form_sizing_chart_edit"	method="POST" enctype="multipart/form-data" >
	<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">
	<?php echo $form['_csrf_token']->render()?>
	<div class="Div_Editor_Conteiner">
		<br><h2 class="EditorTitle"><?php echo __( ( !empty($sizing_chart_key) and $sizing_chart_key!='-')?'Edit':'Add').' '.__('Size Charts') ?></h2>
		<span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

		<table class="Table_Editor_Conteiner">

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[SizingChartPeer::KEY]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ SizingChartPeer::KEY ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::KEY ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[SizingChartPeer::TITLE]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ SizingChartPeer::TITLE ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::TITLE ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>




			<tr>
				<td class="left">
					<?php echo strip_tags( $form[SizingChartPeer::FILENAME]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ SizingChartPeer::FILENAME ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::FILENAME ]->renderError() )  :"" ) ?>
  	      </span><br>
					<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
					$SizingChartsDirectory= sfConfig::get('app_application_SizingChartsDirectory' );
					if ( !empty($SizingChart) ) {
						$Image= DIRECTORY_SEPARATOR . $SizingChartsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getFilename(false);
						if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
							$FileName= DIRECTORY_SEPARATOR.$SizingChartsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getFilename();
							echo '<table border="0">';
							echo '</td><td>';
							echo  '<a href="'.$HostForImage . $Image.'" >'.$Image.'</a>' . '</td></tr></table><br><br>';
						}
					}
					?>
				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[SizingChartPeer::THUMBNAIL]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ SizingChartPeer::THUMBNAIL ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::THUMBNAIL ]->renderError() )  :"" ) ?>
  	      </span><br>
					<?php

					$SizingChartsThumbnailsDirectory= sfConfig::get('app_application_SizingChartsThumbnailsDirectory' );
					// Util::deb($SizingChartsThumbnailsDirectory, '$SizingChartsDirectory::');
					if ( !empty($SizingChart) ) {
						//				Util::deb($Image, '$Image::');
						$Image= DIRECTORY_SEPARATOR . $SizingChartsThumbnailsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getThumbnail(false);
						if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
							$listthb_width= (int)sfConfig::get('app_application_listthb_width' );
							$listthb_height= (int)sfConfig::get('app_application_listthb_height' );
							$FileName= DIRECTORY_SEPARATOR.$SizingChartsThumbnailsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getThumbnail();
							// Util::deb($FileName, '$FileName::');
							echo '<table border="0">';
							$NewSize= Util::GetImageShowSize( $FileName, $listthb_width, $listthb_height );
							// Util::deb( $NewSize, '$NewSize::');
							$ImageTag= image_tag(  $HostForImage . $Image.'?p='.time(), array( 'border'=>'none' , 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'] )  );
							$Src= '<a href="'.$HostForImage.$FileName.'" >'.$ImageTag.'</a>'; /* '</td><td align="left" valign="top" >'.'<a class="thickbox" href="'.url_for( "@admin_showimage?type=Beverage&image=" . $FileName ) . '&alt='.$SizingChart->getThumbnail().
                '&height='.( ($listthb_width>$NewSize['OriginalHeight']?$listthb_width:$NewSize['OriginalHeight']) + 10 ).
                '&width='.( ($listthb_height>$NewSize['OriginalWidth']?$listthb_height:$NewSize['OriginalWidth']) + 10 ).
                '" title="'.$SizingChart->getThumbnail().'">'.$ImageTag.'</a> ';*/
							//Util::deb( $Src, '$Src::');
							echo $Src;
							echo '</td><td>';
							echo /* '<a href="'.$HostForImage . $Image.'" >'.$Image.'('.$NewSize['OriginalWidth'].'x'.$NewSize['OriginalHeight'].')'.'</a>' . .
  	          '<br><br>&nbsp;&nbsp;Delete Image&nbsp;:&nbsp;<input type="checkbox" value="1" id="beverage_image_delete" name="beverage_image_delete" >'.*/
							'</td></tr></table>';
						}
					}
					?>

				</td>
			</tr>

			<tr>
				<td class="left">
					<?php echo strip_tags( $form[SizingChartPeer::ORDERING]->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ SizingChartPeer::ORDERING]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::ORDERING ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<?php if ( !empty($SizingChart) ) { ?>
				<tr>
					<td class="left">
						<?php echo strip_tags( $form[SizingChartPeer::CREATED_AT]->renderLabel() ) ?>&nbsp;:
					</td>
					<td class="right">
						<?php echo $form[ SizingChartPeer::CREATED_AT ]->render(); ?>
						<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::CREATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
					</td>
				</tr>
				<tr>
					<td class="left">
						<?php echo strip_tags( $form[SizingChartPeer::UPDATED_AT]->renderLabel() ) ?>&nbsp;:
					</td>
					<td class="right">
						<?php echo $form[ SizingChartPeer::UPDATED_AT ]->render(); ?>
						<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ SizingChartPeer::UPDATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
					</td>
				</tr>
			<?php } ?>

			<tr>
				<td></td>
				<td  style="padding-left:150px;">

					<div id="backend-edit-btn3-div" />
					<?php if( empty($sizing_chart_key) or $sizing_chart_key == '-' ) : ?>
						<img id="backend-edit-btn2" src= <?php echo image_path('add-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php else: ?>
						<img id="backend-edit-btn2" src= <?php echo image_path('save-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php endif; ?>
					<img id="backend-edit-btn3" src= <?php echo image_path('cancel-btn.png'); ?>  onclick='javascript:document.location="<?php echo url_for('@admin_sizing_charts?page='.$page.'&sorting='.$sorting ) ?>"' style="cursor:pointer;" />
	</div>

	</td>
	</tr>
	</table>
	</div>

</form>





