<script type="text/javascript" language="JavaScript">
  <!--

  function onSubmit() {
    var theForm = document.getElementById("form_import_product_lines");
    theForm.submit();
  }
  
  function onCancel(){
    var HRef= '<?php echo url_for('@homepage') ?>';
    document.location= HRef
  }
  
    
  //-->
  </script>
  
  


<form action="<?php echo url_for('@admin_import_product_lines') ?>" id="form_import_product_lines" method="POST" enctype="multipart/form-data" >
<?php echo $form['_csrf_token']->render()?>
<div class="Div_Editor_Conteiner">
  <br><h2 class="EditorTitle"><?php echo __('Product lines import') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner" border="0">
    

      <?php 
      if ( !empty($InfoText) ) : ?>
      <tr>
        <td class="center" colspan="2">
          <h3><?php echo htmlspecialchars_decode($InfoText) ?></h3>
        </td>
      </tr>
      <?php endif;  ?>

    
      <tr>
        <td class="center" colspan="2">
          <h4><?php echo __("To import Product lines, browse to select your xls file(not xlsx format) containing the Product lines.") ?></h4>
        </td>
      </tr>
     

      <tr>
        <td class="left">
          <?php echo strip_tags( $form['xls_file']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ 'xls_file' ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ 'xls_file' ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
    <tr>
      <td align="center">
        <input type="button" id="action_submit" value="Load" onclick='javascript:onSubmit()' />
        <input type="button" id="action_submit" value="Cancel" onclick='javascript:onCancel()' />
      </td>
    </tr>
    

    <tr>
      <td align="left" colspan="2">&nbsp;
      </td>
    </tr>
    
    
  </table>
</div>


</form>
<?php 
/* 

Подскажите, пожалуйста, у меня проблема при загрузке excel-евских файлов file.xlsx
через класс Spreadsheet_Excel_Reader(A class for reading Microsoft Excel (97/2003) Spreadsheets. Version 2.21)
я получаю ошибку:

    The filename /tmp/phpBhd0uE is not readable.

В свойcтвах файла написано "Microsoft Excel Worksheet". 

Я копирую нужную страницу в буфер обмена и вставляю ее в свежесозданный в Open Office документ "Excel spreadsheet".
И этот новый документ аплодиться нормально. 

Я полез в код OLERead класса и нашел определения метода read который разбирает заголовки файла. 

Похоже что формат "Microsoft Excel Worksheet" более поздний по отношению к формату "Excel spreadsheet" и
класс Spreadsheet_Excel_Reader просто нге умеет с ним работать ?

Нет ли чего возможно более современного Spreadsheet_Excel_Reader ?


  */    
      ?>