


<p class="backend-heading">View Order:&nbsp;#527</p>

<table id="order_view_table">
  <tr>
    <td class="order_view1">User:</td>
    <td class="order_view2">Eduardo Saldo</td>
    <td class="order_view3">Status:</td>
    <td class="order_view4"><b>NEW</b></td>
  </tr>
  <tr>
    <td class="order_view1">Email:</td>
    <td class="order_view2">edusaldo@hotmail.com</td>
    <td class="order_view3">Created:</td>
    <td class="order_view4">January 22, 2013&nbsp;2:23 p.m.</td>
  </tr>
  <tr>
    <td class="order_view1"></td>
    <td class="order_view2"></td>
    <td class="order_view3"></td>
    <td class="order_view4"></td>
  </tr>
</table>

<table id="order_view_table2">
  <tr>
    <td colspan="6" class="order_view5"></td>
  </tr>
  <tr>
    <td colspan="2" class="order_view16"><b>Payment Info</b></td>
    <td colspan="2" class="order_view16"><b>Billing Address</b></td>
    <td colspan="2" class="order_view16"><b>Shipping Address</b></td>
  </tr>
  <tr>
    <td class="order_view6">Transaction Id:</td>
    <td class="order_view7"><b>WO_4953867048</b></td>
    <td class="order_view3">Address:</td>
    <td class="order_view8">12335 Superior Main St. Apt 109</td>
    <td class="order_view3">Address:</td>
    <td class="order_view9">1456 Palmetto Dr.</td>
  </tr>
  <tr>
    <td class="order_view6">Subtotal:</td>
    <td class="order_view7"><b>$3821.55</b></td>
    <td class="order_view3">City:</td>
    <td class="order_view8">Laurel</td>
    <td class="order_view3">City:</td>
    <td class="order_view9">Greenville</td>
  </tr>
  <tr>
    <td class="order_view6">Shipping</td>
    <td class="order_view7"><b>$50.00</b></td>
    <td class="order_view3">State:</td>
    <td class="order_view8">MD</td>
    <td class="order_view3">State:</td>
    <td class="order_view9">SC</td>
  </tr>
  <tr>
    <td class="order_view6">Tax</td>
    <td class="order_view7"><b>$0.00</b></td>
    <td class="order_view3">Zip:</td>
    <td class="order_view8">20724</td>
    <td class="order_view3">Zip:</td>
    <td class="order_view9">41732</td>
  </tr>
  <tr>
    <td class="order_view6">Total</td>
    <td class="order_view7"><b>$3781.55</b></td>
    <td class="order_view3">Country:</td>
    <td class="order_view8">US</td>
    <td class="order_view3">Country:</td>
    <td class="order_view9">US</td>
  </tr>
  <tr>
    <td class="order_view6">Payment Method</td>
    <td class="order_view7"><b>Credit Card</b></td>
    <td class="order_view3"></td>
    <td class="order_view8"></td>
    <td class="order_view3"></td>
    <td class="order_view9"></td>
  </tr>
  <tr>
    <td colspan="6" id="order_view10"></td>
  </tr>

  <tr>
    <td colspan="6" class="order_view5"></td>
  </tr>
</table>

<table id="order_view_table3">
  <tr>
    <td class="order_view11 order_view16"><b>Order Details</b></td> 
    <td class="order_view12"></td>
    <td class="order_view13"></td>
    <td class="order_view14"></td>
    <td class="order_view15"></td>
  </tr>
  <tr>
    <td class="order_view11"><b>Items:&nbsp;2</b></td> 
    <td class="order_view12"></td>
    <td class="order_view13"></td>
    <td class="order_view14"></td>
    <td class="order_view15"></td>
  </tr>
  <tr>
    <td class="order_view11"></td> 
    <td class="order_view12"></td>
    <td class="order_view13"></td>
    <td class="order_view14"></td>
    <td class="order_view15"></td>
  </tr>
  <tr>
    <td class="order_view11"></td> 
    <td class="order_view12"></td>
    <td class="order_view13"></td>
    <td class="order_view14"></td>
    <td class="order_view15"></td>
  </tr>
  <tr>
    <td class="order_view11">Product (sku)</td> 
    <td class="order_view12">Unit</td>
    <td class="order_view13">Qty</td>
    <td class="order_view14">Price</td>
    <td class="order_view15">Backorder</td>
  </tr>
  <tr>
    <td class="order_view11"><b>S&W M&P 9mm Magazine 17 Rd (39490)</b></td> 
    <td class="order_view12 order_view17"><b>$1,227.95</b></td>
    <td class="order_view13"><b>3</b></td>
    <td class="order_view14"><b>$3683.85</b></td>
    <td class="order_view15"><input type="checkbox" name="" value=""></td>
  </tr>
  <tr>
    <td class="order_view11"><b>Magpul PMAG LR 308Win 20RD Black&nbsp;(MGMP1243BLK)</b></td> 
    <td class="order_view12 order_view17"><b>$22.95</b></td>
    <td class="order_view13"><b>6</b></td>
    <td class="order_view14"><b>$137.70</b></td>
    <td class="order_view15"><input type="checkbox" name="" value="" checked="checked"></td>
  </tr>
  <tr>
    <td colspan="5" class="order_view18"></td>
  </tr>
  <tr>
    <td class="order_view11"></td> 
    <td class="order_view12 order_view17"></td>
    <td class="order_view13"><b>Subtotal:</b></td>
    <td class="order_view14"><b>$3821.55</b></td>
    <td class="order_view15"></td>
  </tr>  
</table>
<div id="backend-edit-btn3-div" onclick='alert("Button Pressed")'; />
  <img id="backend-edit-btn2" src= <?php echo image_path('save-btn.png'); ?> />       
  <img id="backend-edit-btn3" src= <?php echo image_path('cancel-btn.png'); ?> />
</div>
<a id="youtube_add_link" href="order">&lt; Back to Orders</a>











        

