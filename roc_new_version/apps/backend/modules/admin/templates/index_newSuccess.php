<!-- sz added for the content partial -->

<!-- end sz added for the content partial -->
  <script type="text/javascript" language="JavaScript">
  <!--

   function SiteInventoryItemsImport() {
    if ( !confirm("Do you want to run Cron for Inventory Items Import ?") ) return;
    var Url= '<?php echo Util::cross_app_link_to("frontend","@cron_inventory_items_import",array( 'manually'=>1 ) ) ?>'
    document.location= Url
  }

   function SiteInventoryItemsImportSmallTest() {
    if ( !confirm("Do you want to run Cron for Inventory Items Import (Small Test) ?") ) return;
    var Url= '<?php echo Util::cross_app_link_to("frontend","@cron_inventory_items_import",array( 'manually'=>1, 'small_test'=>1 ) ) ?>'
    document.location= Url
  }
  //-->
  </script>

<?php
$IsAdmin= $sf_user->hasCredential('Admin');
$IsEditor= $sf_user->hasCredential('Editor');
?>



<div class="Div_ListConteiner">
 <br />
 <center><h2 class="Title"><?php echo 'Administration Zone' ?></h2></center>
 <div class="mainpage-menu">
  <?php echo link_to( "Admin Home", "admin/index") ?> <br>

<?php if ( $IsAdmin ) : ?>
  <?php //echo link_to( "Inventory Items", "@admin_inventory_items") ?> <br>

  <a style="cursor:pointer;" onclick="javascript:SiteInventoryItemsImport()" >Inventory Items Import</a><br>
  <a style="cursor:pointer;" onclick="javascript:SiteInventoryItemsImportSmallTest()" >Inventory Items Import(Small Test)</a><br>
  <?php echo link_to( "YouTube Videos", "@admin_youtube_videos") ?> <br>

  <?php echo link_to( "Importing Product Lines", "@admin_import_product_lines") ?> <br><br><br>

  <?php echo link_to( "ORDERS", "@admin_ordereds") ?> <br><br><br>

  <?php echo link_to( "Documents", "@admin_documents") ?> <br>
  <?php echo link_to( "Paperworks", "@admin_paperworks") ?> <br>
  <?php echo link_to( "Size Charts", "@admin_sizing_charts") ?> <br>
  <!-- Please develop a way for the client to view, add, and edit files for each of the documents, paperwork, and size chart sections on the website.   -->

  <hr>
  <!-- <a style="cursor:pointer;" onclick="javascript:SiteBackup()" >Site Backup</a><br>    -->
<?php endif; ?>


<?php if ( !$IsAdmin and $IsEditor ) : ?>
  <?php //echo link_to( "News", "@admin_cms_items") ?> <br>
<?php endif; ?>
  </div>
</div>
