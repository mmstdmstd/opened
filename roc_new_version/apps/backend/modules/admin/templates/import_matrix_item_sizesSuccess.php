
<script type="text/javascript" language="JavaScript">
	<!--

	function onSubmit() {
		var theForm = document.getElementById("form_import_matrix_item_size");
		theForm.submit();
	}

	function onCancel(){
		var HRef= '<?php echo url_for('@homepage') ?>';
		document.location= HRef
	}

	//-->
</script>


<form action="<?php echo url_for('@admin_import_matrix_item_sizes') ?>" id="form_import_matrix_item_size" method="POST" enctype="multipart/form-data" >
	<?php echo $form['_csrf_token']->render()?>
	<div class="Div_Editor_Conteiner">
		<br><h2 class="EditorTitle"><?php echo __('Matrix item size import') ?></h2>
		<span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

		<table class="Table_Editor_Conteiner" border="0" style="width: 800px;">


			<?php
			if ( !empty($InfoText) ) : ?>
				<tr>
					<td class="center" colspan="2">
						<h3><?php echo htmlspecialchars_decode($InfoText) ?></h3>
					</td>
				</tr>
			<?php endif;  ?>


			<tr>
				<td class="center" colspan="2">
					<h4><?php echo __("To import Matrix item size, browse to select your xls file(not xlsx/ods format) containing the Matrix item size mapping. <br>Importing file must be with valid format : size column must have header SIZE in first row and data from second row including.") ?></h4>
				</td>
			</tr>


			<tr>
				<td class="left" style="width: 300px;">
					<?php echo strip_tags( $form['xls_file']->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ 'xls_file' ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ 'xls_file' ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td class="left" style="width: 300px;">
					<?php echo strip_tags( $form['delete_existing_row']->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ 'delete_existing_row' ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ 'delete_existing_row' ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td align="center" colspan="2" >&nbsp;
				</td>
			</tr>

			<tr>
				<td align="center" colspan="2" >
					<input type="button" id="action_submit" value="Load" onclick='javascript:onSubmit()' />
					<input type="button" id="action_submit" value="Cancel" onclick='javascript:onCancel()' />
				</td>
			</tr>

      <?php $lImportRules= ImportRulesPeer::getImportRulesByKey( 'ImportMatrixItemSizes' );
			if ( !empty($lImportRules) ) : ?>
			<tr>
				<td align="left" colspan="2">Last Uploaded&nbsp;:&nbsp;
					<?php echo $lImportRules->getKeyValue() ?>
				</td>
			</tr>
			<?php endif; ?>



		</table>
	</div>


</form>
<?php

?>