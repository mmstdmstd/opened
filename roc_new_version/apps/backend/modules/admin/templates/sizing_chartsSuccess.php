<script type="text/javascript" language="JavaScript">
	<!--

	function onSubmit() {
		var theForm = document.getElementById("form_sizing_charts");
		theForm.submit();
	}

	function onDelete(Key) {
		if ( !confirm("Do you want to delete link to this Sizing Chart ?") ) return;
		var HRef= '<?php echo url_for('@admin_sizing_chart_delete?page='.$page.'&key=') ?>' + Key
		document.location= HRef;
	}

	//-->
</script>


<div id="content-div">
	<p class="backend-heading3">Size Charts</p>
	<div class="backend-addnew-btn-div" onclick='javascript:document.location="<?php echo url_for('@admin_sizing_chart_edit?key=-&page='.$page.'&sorting='.$sorting )?>"'/>
	<?php echo image_tag("addnew-btn.png", array( 'class'=>'backend-addnew-btn' )); ?>
</div>


<form action="<?php echo url_for('@admin_sizing_charts?page='.$page . '&sorting='.$sorting  ) ?>" id="form_sizing_charts" method="POST">
	<?php if( $SizingChartsPager->getNbResults()> 0 ) : ?>




		<input type="hidden" id="page" name="page" value="<?php echo $page ?>" >
		<p class="page_number">Page&nbsp;<?php echo $page ?></p>
		<table class="table13" cellspacing="0" cellpadding="0" id="table12">

			<tr id="table12-heading">
				<td id="table12-h1">
				</td>
				<td class="table12-h2a"><?php echo link_to(__('Key'), '@admin_sizing_charts?page='.$page.'&sorting=KEY' ) ?></td>
				<td class="table12-h2"><?php echo link_to(__('Title'), '@admin_sizing_charts?page='.$page.'&sorting=TITLE' ) ?></td>
				<td id="table12-h3">Filename</td>
				<td id="table12-h3">Thumbnail</td>
				<td id="table12-h4"><?php echo link_to(__('Order'), '@admin_sizing_charts?page='.$page.'&sorting=ORDERING' ) ?></td>
				<td id="table12-h13"><?php echo link_to(__('Created at'), '@admin_sizing_charts?page='.$page.'&sorting=CREATED_AT' ) ?></td>
				<td id="table12-h14"></td>
			</tr>

			<?php $RowNumber=0; foreach( $SizingChartsPager->getResults() as $SizingChart ) : ?>
				<tr id="admin_row_<?php echo ($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber?>"
						class="admin_row_<?php echo $RowNumber%2==0 ?'even':'odd'?>"
						onmouseover="AdminTROver( '<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber ?>' );"
						onmouseout="AdminTROut('<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber  ?>' );" >

					<td valign="top" class="col_1 alignment">
						<div class="backend-edit-btn-div" onclick='javascript:document.location="<?php echo url_for('@admin_sizing_chart_edit?key='. $SizingChart->getKey() .'&page='.$page.'&sorting='.$sorting , array(  )  )?>"' />
						<?php echo image_tag("edit-btn.png", array( 'class'=>'backend-edit-btn' )); ?>
						</div>
					</td>


					<td align="center" valign="top" class="col_pad">
						<?php echo $SizingChart->getKey() ?>
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $SizingChart->getTitle()?>&nbsp;
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $SizingChart->getFilename()?>&nbsp;
					</td>

					<td align="left" valign="top" class="col_pad">
						<?php echo $SizingChart->getThumbnail()?>&nbsp;
					</td>

					<td align="center" valign="top" class="col_pad_center">
						<?php echo $SizingChart->getOrdering()?>&nbsp;
					</td>


					<td align="left" valign="top" class="col_pad">
						<?php echo $SizingChart->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) ?>
					</td>
					<!--
  	  <td align="center">
        <?php
          echo link_to( __("Edit"), '@admin_sizing_chart_edit?key='. $SizingChart->getKey().'&page='.$page.'&sorting='.$sorting, array() )
        ?>
      </td>
-->

					<td align="center" valign="top" class="col_14 col_pad" >
						<!--
        <a style="cursor:pointer;" onclick="javascript:onDelete('<?php echo $SizingChart->getKey() ?>')" ><?php echo __("Delete") ?></a>
-->
						<div class="backend-del-btn-div" />
						<input class="backend-del-btn" id="" onclick="javascript:onDelete('<?php echo $SizingChart->getKey() ?>'); return false; "  src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/delete-btn.png" type="image" value="" />
						</div>
					</td>

				</tr>

				<?php $RowNumber++;  endforeach; ?>

			<tr>
				<td colspan="8" style="border-bottom:1px solid #888687;">
				</td>
			</tr>
		</table>
		<p class="page_number">Page&nbsp;<?php echo $page ?></p>

		<!--
<div class="add-new-item"><?php echo link_to(__('Add new'),'@admin_sizing_chart_edit?key=-&page='.$page.'&sorting='.$sorting) ?></div>
-->

	<?php endif ?>

	<?php if ($SizingChartsPager->haveToPaginate()): ?>
		<?php echo link_to('&laquo;', '@admin_sizing_charts?page=1&sorting='.$sorting ) ?> &nbsp;
		<?php $links = $SizingChartsPager->getLinks();
		foreach ($links as $page): ?>
			<?php echo ($page == $SizingChartsPager->getPage()) ? $page : link_to($page, '@admin_sizing_charts?page='.$page.'&sorting='.$sorting ) ?>
			<?php if ($page != $SizingChartsPager->getCurrentMaxLink()): ?>
				&nbsp;
			<?php endif ?>
		<?php endforeach ?>
		&nbsp; <?php echo link_to('&raquo;', '@admin_sizing_charts?page='.$page.'&sorting='.$sorting ) ?>
	<?php endif ?>


	<?php if( $SizingChartsPager->getNbResults()== 0 ) : ?>
		<div class="nothing-found">
			<?php echo __("No Sizing Charts") ?>
		</div>
		<div class="add-new-item">
			<?php echo link_to(__('Add new'),'@admin_sizing_chart_edit?key=-&page='.$page.'&sorting='.$sorting ) ?>
		</div>
	<?php endif ?>




</form>

</div><!-- close content div -->