

  <script type="text/javascript" language="JavaScript">
  <!--

  function onSubmit() {
    var theForm = document.getElementById("form_documents");
    theForm.submit();
  }

  function onDelete(Key) {
  	if ( !confirm("Do you want to delete link to this document ?") ) return;
    var HRef= '<?php echo url_for('@admin_document_delete?page='.$page.'&key=') ?>' + Key
    document.location= HRef;
  }

  //-->
  </script>

<div class="Div_ListConteiner">
<br><h2 class="ListTitle"><?php echo __("List of Documents" ); ?></h2>

<form action="<?php echo url_for('@admin_documents?page='.$page . '&sorting='.$sorting  ) ?>" id="form_documents" method="POST">
<?php if( $DocumentsPager->getNbResults()> 0 ) : ?>



	<div class="add-new-item">
		<img style="width:89px; height:20px;  overflow:hidden;  margin:0px 16px 0;cursor:pointer; " src="<?php echo image_path('addnew-btn.png'); ?>" onclick="javascript:document.location=' <?php echo url_for( '@admin_document_edit?key=-&page='.$page.'&sorting='.$sorting )?>'" />
	</div>

  <input type="hidden" id="page" name="page" value="<?php echo $page ?>" >
  <div>
  <table class="Table_List"  style="margin-left:5px;">

    <tr >
			<th>&nbsp;</th>
      <th>
        <?php echo link_to(__('Key'), '@admin_documents?page='.$page.'&sorting=KEY' ) ?>
      </th>

      <th>
        <?php echo link_to(__('Title'), '@admin_documents?page='.$page.'&sorting=TITLE' ) ?>
      </th>

      <th>
        Filename
      </th>

      <th>
        Thumbnail
      </th>


      <th>
        <?php echo link_to(__('Order'), '@admin_documents?page='.$page.'&sorting=ORDERING' ) ?>
      </th>

      <th>
        <?php echo link_to(__('Created at'), '@admin_documents?page='.$page.'&sorting=CREATED_AT' ) ?>
      </th>

      <th>
        <?php echo link_to(__('Updated at'), '@admin_documents?page='.$page.'&sorting=UPDATED_AT' ) ?>
      </th>

      <th>&nbsp;</th>
    </tr>
  <?php $RowNumber=0; foreach( $DocumentsPager->getResults() as $Document ) : ?>
    <tr id="admin_row_<?php echo ($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber?>"
   class="admin_row_<?php echo $RowNumber%2==0 ?'even':'odd'?>"
   onmouseover="AdminTROver( '<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber ?>' );"
   onmouseout="AdminTROut('<?php echo "admin_row_".($RowNumber%2==0 ?'even':'odd').'_'.$RowNumber  ?>' );" >

  		<td align="left" style="padding-left:10px;">
    		<div class="backend-edit-btn-div" />
	      	<img class="backend-edit-btn" style="cursor:pointer;" src="<?php echo image_path('edit-btn.png'); ?>" onclick="javascript:document.location='<?php
					echo url_for( '@admin_document_edit?key='. $Document->getKey().'&page='.$page.'&sorting='.$sorting ) ?>' "  />
	      </div>
	    </td>

    	<td align="left">
        <?php echo $Document->getKey() ?>
      </td>

      <td align="left">
        <?php echo $Document->getTitle()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $Document->getFilename()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $Document->getThumbnail()?>&nbsp;
      </td>

      <td align="left">
        <?php echo $Document->getOrdering()?>&nbsp;
      </td>


      <td align="center">
        <?php echo $Document->getCreatedAt( sfConfig::get('app_application_date_time_format' ) ) ?>
      </td>

      <td align="center">
        <?php echo $Document->getUpdatedAt( sfConfig::get('app_application_date_time_format' ) ) ?>
      </td>



      <td align="center" >
				<div class="backend-edit-btn-div" />
  				<img class="backend-edit-btn" src="<?php echo image_path('delete-btn.png'); ?>"  style="cursor:pointer;" onclick="javascript:onDelete('<?php echo $Document->getKey() ?>')" > />
        </div>
      </td>

    </tr>

  <?php $RowNumber++;  endforeach; ?>
  </table>
  </div>


<?php endif ?>

<?php if ($DocumentsPager->haveToPaginate()): ?>
  <?php echo link_to('&laquo;', '@admin_documents?page=1&sorting='.$sorting ) ?> &nbsp;
  <?php $links = $DocumentsPager->getLinks();
  foreach ($links as $page): ?>
    <?php echo ($page == $DocumentsPager->getPage()) ? $page : link_to($page, '@admin_documents?page='.$page.'&sorting='.$sorting ) ?>
    <?php if ($page != $DocumentsPager->getCurrentMaxLink()): ?>
      &nbsp;
    <?php endif ?>
  <?php endforeach ?>
  &nbsp; <?php echo link_to('&raquo;', '@admin_documents?page='.$page.'&sorting='.$sorting ) ?>
<?php endif ?>


<?php if( $DocumentsPager->getNbResults()== 0 ) : ?>
  <div class="nothing-found">
  <?php echo __("No Documents") ?>
  </div>
	<div class="add-new-item">
		<img style="width:89px; height:20px;  overflow:hidden;  margin:0px 16px 0;cursor:pointer; " src="<?php echo image_path('addnew-btn.png'); ?>" onclick="javascript:document.location=' <?php echo url_for( '@admin_document_edit?key=-&page='.$page.'&sorting='.$sorting )?>'" />
	</div>
<?php endif ?>

</div>


</form>