<?php 
$IsAdmin= $sf_user->hasCredential('Admin');
$IsEditor= $sf_user->hasCredential('Editor');

if ( !( $sf_user->isAuthenticated() ) ) return;
$ModuleName= $sf_context->getModuleName();
$ActionName= $sf_context->getActionName();
?>

<div id="menu">
  <ul>    

    <?php if ( $IsAdmin ) :?>    
    <li class="first-item">
	    <?php echo link_to( "Admin Home", "@homepage",      ( ( ( $ActionName=='index' )  )?array("class"=>"current_admin_menu"):array("class"=>"admin_menu" ) )     ) ?>
    </li>

    <li>
	    <?php echo link_to(   "You Tube Videos", "@admin_youtube_videos",   ( ( ( $ActionName=='youtube_videos' or $ActionName=='youtube_videos_edit' )  )?array("class"=>"current_admin_menu"):array("class"=>"admin_menu" ) )    ) ?>
    </li>
    <!-- <li>
	    <?php //echo link_to(   "Inventory Items", "@admin_inventory_items",   ( ( ( $ActionName=='inventory_items' or $ActionName=='inventory_item_edit' )  )?array("class"=>"current_admin_menu"):array("class"=>"admin_menu" ) )    ) ?>
    </li> -->
    
    <?php endif; ?>    
    
    
    
    <?php if ( !$IsAdmin and $IsEditor ) : ?>
    <li>
	    <?php  ?>
    </li>
    <?php endif; ?>    
   
  </ul>
</div>
</div>