<script type="text/javascript" language="JavaScript">
	<!--

	function onSubmit() {
		var theForm = document.getElementById("form_import_color_mapping");
		theForm.submit();
	}

	function onCancel(){
		var HRef= '<?php echo url_for('@homepage') ?>';
		document.location= HRef
	}

	function Fill_UDF_SWC_ColorsFields() {
		if ( !confirm("Do you want to go through the database row by row and fill in the 15 UDF_SWC_[color] fields based on what is in the original Color field ?") ) return;
		var HRef= '<?php echo Util::cross_app_link_to("frontend","@cron_fill_udf_swc_colors_fields",array( 'manually'=>1 ) ) ?>'

		document.location= HRef

	}

	//-->
</script>




<form action="<?php echo url_for('@admin_import_color_mapping') ?>" id="form_import_color_mapping" method="POST" enctype="multipart/form-data" >
	<?php echo $form['_csrf_token']->render()?>
	<div class="Div_Editor_Conteiner">
		<br><h2 class="EditorTitle"><?php echo __('Color mappings import') ?></h2>
		<span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

		<table class="Table_Editor_Conteiner" border="0" style="width: 800px;">


			<?php
			if ( !empty($InfoText) ) : ?>
				<tr>
					<td class="center" colspan="2">
						<h3><?php echo htmlspecialchars_decode($InfoText) ?></h3>
					</td>
				</tr>
			<?php endif;  ?>


			<tr>
				<td class="center" colspan="2">
					<h4><?php echo __("To import Color mappings, browse to select your xls file(not xlsx/ods format) containing the Color mapping.<br> All existing rows will be deleted . ") ?></h4>
				</td>
			</tr>


			<tr>
				<td class="left" style="width: 300px;">
					<?php echo strip_tags( $form['xls_file']->renderLabel() ) ?>&nbsp;:
				</td>
				<td class="right">
					<?php echo $form[ 'xls_file' ]->render(); ?>
					<span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ 'xls_file' ]->renderError() )  :"" ) ?>
  	      </span>
				</td>
			</tr>

			<tr>
				<td align="center" colspan="2" >&nbsp;
				</td>
			</tr>

			<tr>
				<td align="center" colspan="2" >
					<input type="button" id="action_submit" value="Load" onclick='javascript:onSubmit()' />
					<input type="button" id="action_submit" value="Cancel" onclick='javascript:onCancel()' />
				</td>
			</tr>


			<tr>
				<td align="left" colspan="2">&nbsp;
				</td>
			</tr>

			<tr>
				<td align="left" colspan="2">&nbsp;
					<input type="button" id="action_submit" value="Fill UDF_SWC_[color] fields " onclick='javascript:Fill_UDF_SWC_ColorsFields()' />
				</td>
			</tr>


		</table>
	</div>


</form>
<?php

?>