  <script type="text/javascript" language="JavaScript">
  <!--
  function onSubmit() {
    var theForm = document.getElementById("form_document_edit");
    theForm.submit();
  }

  //-->
  </script>

<form action="<?php echo url_for('@admin_document_edit?key='.$document_key.'&page='.$page.'&sorting='.$sorting ) ?>" id="form_document_edit" method="POST" enctype="multipart/form-data" >
<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">
<?php echo $form['_csrf_token']->render()?>
<div class="Div_Editor_Conteiner">
  <br><h2 class="EditorTitle"><?php echo __( ( !empty($document_key) and $document_key!='-')?'Edit':'Add').' '.__('Document') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner">

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::KEY]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::KEY ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::KEY ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::TITLE]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::TITLE ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::TITLE ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>




      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::FILENAME]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::FILENAME ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::FILENAME ]->renderError() )  :"" ) ?>
  	      </span><br>
  	      <?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
            $DocumentsDirectory= sfConfig::get('app_application_DocumentsDirectory' );
            if ( !empty($Document) ) {
              $Image= DIRECTORY_SEPARATOR . $DocumentsDirectory . DIRECTORY_SEPARATOR . $Document->getFilename(false);
              if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
                $FileName= DIRECTORY_SEPARATOR.$DocumentsDirectory . DIRECTORY_SEPARATOR . $Document->getFilename();
                echo '<table border="0">';
                echo '</td><td>';
                echo  '<a href="'.$HostForImage . $Image.'" >'.$Image.'</a>' . '</td></tr></table><br><br>';
              }
            }
  	    ?>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::THUMBNAIL]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::THUMBNAIL ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::THUMBNAIL ]->renderError() )  :"" ) ?>
  	      </span><br>
  	      <?php 

            $DocumentsThumbnailsDirectory= sfConfig::get('app_application_DocumentsThumbnailsDirectory' );
            if ( !empty($Document) ) {
			//				Util::deb($Image, '$Image::');
              $Image= DIRECTORY_SEPARATOR . $DocumentsThumbnailsDirectory . DIRECTORY_SEPARATOR . $Document->getThumbnail(false);
              if ( file_exists( sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image ) and is_file(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR .$Image) ) {
                $listthb_width= (int)sfConfig::get('app_application_listthb_width' );
                $listthb_height= (int)sfConfig::get('app_application_listthb_height' );
                $FileName= DIRECTORY_SEPARATOR.$DocumentsThumbnailsDirectory . DIRECTORY_SEPARATOR . $Document->getThumbnail();
		//Util::deb($FileName, '$FileName::');
                echo '<table border="0">';
                $NewSize= Util::GetImageShowSize( $FileName, $listthb_width, $listthb_height );
		//Util::deb( $NewSize, '$NewSize::');
                $ImageTag= image_tag(  $HostForImage . $Image.'?p='.time(), array( 'border'=>'none' , 'width'=>$NewSize['Width'], 'height'=>$NewSize['Height'] )  );
                $Src= '<a href="'.$HostForImage.$FileName.'" >'.$ImageTag.'</a>'; /* '</td><td align="left" valign="top" >'.'<a class="thickbox" href="'.url_for( "@admin_showimage?type=Beverage&image=" . $FileName ) . '&alt='.$Document->getThumbnail().
                '&height='.( ($listthb_width>$NewSize['OriginalHeight']?$listthb_width:$NewSize['OriginalHeight']) + 10 ).
                '&width='.( ($listthb_height>$NewSize['OriginalWidth']?$listthb_height:$NewSize['OriginalWidth']) + 10 ).
                '" title="'.$Document->getThumbnail().'">'.$ImageTag.'</a> ';*/
		//Util::deb( $Src, '$Src::');
                echo $Src;
                echo '</td><td>';
                echo /* '<a href="'.$HostForImage . $Image.'" >'.$Image.'('.$NewSize['OriginalWidth'].'x'.$NewSize['OriginalHeight'].')'.'</a>' . .
  	          '<br><br>&nbsp;&nbsp;Delete Image&nbsp;:&nbsp;<input type="checkbox" value="1" id="beverage_image_delete" name="beverage_image_delete" >'.*/
  	          '</td></tr></table>';
              }
            }
  	      ?>

        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::ORDERING]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::ORDERING]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::ORDERING ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <?php if ( !empty($Document) ) { ?>
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::CREATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::CREATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::CREATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[DocumentPeer::UPDATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ DocumentPeer::UPDATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ DocumentPeer::UPDATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      <?php } ?>

    <tr>
    <td></td>
      <td  style="padding-left:150px;">

				<div id="backend-edit-btn3-div" />
        <?php if( empty($document_key) or $document_key == '-' ) : ?>
					<img id="backend-edit-btn2" src= <?php echo image_path('add-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php else: ?>
					<img id="backend-edit-btn2" src= <?php echo image_path('save-btn.png'); ?>  onclick='javascript:onSubmit()' style="cursor:pointer;" />
        <?php endif; ?>
				  <img id="backend-edit-btn3" src= <?php echo image_path('cancel-btn.png'); ?>  onclick='javascript:document.location="<?php echo url_for('@admin_documents?page='.$page.'&sorting='.$sorting ) ?>"' style="cursor:pointer;" />
				</div>

      </td>
    </tr>
  </table>
</div>

</form>





