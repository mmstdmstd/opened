<p style="color:black; border:solid 1px #b2b2b0; background-color:#fcfac1; position:absolute; left:584px;top:120px; padding:8px 27px;">Item has been saved.</p>

        <p class="backend-heading">Orders</p>

        <ul class="order_page">
          <li style="font-weight:bold;color:#000000;">Page</li>
          <li><a href="">First</a></li>
          <li><a href="">&lt;Prev</a></li>
          <li><a href="">1</a></li>
          <li><a href="">2</a></li>
          <li><a href="">3</a></li>
          <li><a href="">4</a></li>
          <li><a href="">5</a></li>
          <li><a href="">Next&gt;</a></li>
          <li><a href="">Last</a></li>
        </ul>

<!-- table starts here -->
        <table class="table13" cellspacing="0" cellpadding="0" id="table12d">
          <tr id="table12b-heading">
            <td id="table12-h1"></td>
            <td class="table12a-h2a"><a class="list_title_link" href="">Id</a></td>
            <td class="table12a-h2a"><a class="list_title_link" href="">Status</a></td>
            <td class="table12-h2"><a class="list_title_link" href="">User</a></td>
            <td id="table12a-h3"><a class="list_title_link" href="">City</a></td>
            <td id="table12b-h3"><a class="list_title_link" href="">State</a></td>
            <td id="table12-h4"><a class="list_title_link" href="">Zip</a></td>
            <td id="table12a-h5"><a class="list_title_link" href="">Country</a></td>
            <td class="table12-h2b"><a class="list_title_link" href="">Transaction Id</a></td>
            <td class="table12c-h2b"><a class="list_title_link" href="">Shipping</a></td>
            <td class="table12c-h2b"><a class="list_title_link" href="">Tax</a></td>
            <td class="table12c-h2b"><a class="list_title_link" href="">Total<br />Price</a></td>
            <td class="table12-h2a"><a class="list_title_link" href="">Payment<br />Method</a></td>
            <td id="table12-h13b"><a class="col_14a list_title_link" href="">Created at</a></td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_even_0"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_0' );"
            onmouseout="AdminTROut('admin_row2_even_0' );" >

            <td valign="top" class="col_1 alignment10">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src=<?php echo image_path('view-btn.png'); ?> />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_4957167502</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">Department Purchase</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_odd_1"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_1' );"
            onmouseout="AdminTROut('admin_row2_odd_1' );" >

            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b"">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Overland Park</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">PayPal</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_even_2"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_2' );"
            onmouseout="AdminTROut('admin_row2_even_2' );" >


            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$349.59</td>
            <td valign="top" class="alignment12a">Credit Card</td>
            <td valign="top" class="col_14a alignment12" >2013-02-18<br />22:22</td>
          </tr>
          
<!-- begin row -->
          <tr id="admin_row2_odd_3"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_3' );"
            onmouseout="AdminTROut('admin_row2_odd_3' );" >

            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b"">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Overland Park</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">PayPal</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr>

          <!-- begin row -->
          <tr id="admin_row2_even_4"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_4' );"
            onmouseout="AdminTROut('admin_row2_even_4' );" >


            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$349.59</td>
            <td valign="top" class="alignment12a">Credit Card</td>
            <td valign="top" class="col_14a alignment12" >2013-02-18<br />22:22</td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_odd_5"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_5' );"
            onmouseout="AdminTROut('admin_row2_odd_5' );" >

            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b"">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Overland Park</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">PayPal</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr>


<!-- begin row -->
          <tr id="admin_row2_even_6"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_6' );"
            onmouseout="AdminTROut('admin_row2_even_6' );" >


            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$349.59</td>
            <td valign="top" class="alignment12a">Credit Card</td>
            <td valign="top" class="col_14a alignment12" >2013-02-18<br />22:22</td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_odd_7"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_7' );"
            onmouseout="AdminTROut('admin_row2_odd_7' );" >

            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b"">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Overland Park</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">PayPal</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr>

<!-- begin row -->
          <tr id="admin_row2_even_8"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_8' );"
            onmouseout="AdminTROut('admin_row2_even_8' );" >


            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$349.59</td>
            <td valign="top" class="alignment12a">Credit Card</td>
            <td valign="top" class="col_14a alignment12" >2013-02-18<br />22:22</td>
          </tr>
          
<!-- begin row -->
          <tr id="admin_row2_odd_9"
            class="admin_row2_even"
            class="admin_row2_odd"
            onmouseover="AdminTROver( 'admin_row2_odd_9' );"
            onmouseout="AdminTROut('admin_row2_odd_9' );" >

            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b"">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Overland Park</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment12a">PayPal</td>
            <td valign="top" class="col_14a alignment12">2013-02-18<br />22:22</td>
          </tr> 
          
<!-- begin row -->
          <tr id="admin_row2_even_10"
            class="admin_row2_odd"
            class="admin_row2_even"
            onmouseover="AdminTROver( 'admin_row2_even_10' );"
            onmouseout="AdminTROut('admin_row2_even_10' );" >


            <td class="col_1 alignment10" valign="top">
              <div class="backend-edit-btn-div" onclick="(window.location='order_view')"; />
                <img class="backend-edit-btn" src="<?php echo image_path('view-btn.png'); ?>" />       
              </div>
            </td>
            <td valign="top" class="alignment10">540</td>
            <td valign="top" class="alignment10">New</td>
            <td valign="top" class="alignment12b">Mark Domeyer (mdomeyer@iname.com)</td>
            <td valign="top" class="alignment12">Irwin</td>
            <td valign="top" class="alignment12">PA</td>
            <td valign="top" class="alignment10">15642</td>
            <td valign="top" class="alignment12">US</td>
            <td valign="top" class="alignment12">WO_495</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$12.59</td>
            <td valign="top" class="alignment13">$349.59</td>
            <td valign="top" class="alignment12a">Credit Card</td>
            <td valign="top" class="col_14a alignment12" >2013-02-18<br />22:22</td>
          </tr>         
<!-- add border to last row-->
          <tr>
            <td colspan="14" class="row_border"></td>
          </tr>
        </table>

        <ul class="order_page">
          <li style="font-weight:bold;color:#000000;">Page</li>
          <li><a href="">First</a></li>
          <li><a href="">&lt;Prev</a></li>
          <li><a href="">1</a></li>
          <li><a href="">2</a></li>
          <li><a href="">3</a></li>
          <li><a href="">4</a></li>
          <li><a href="">5</a></li>
          <li><a href="">Next&gt;</a></li>
          <li><a href="">Last</a></li>
        </ul>











        

