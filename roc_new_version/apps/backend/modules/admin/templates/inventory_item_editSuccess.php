  <script type="text/javascript" language="JavaScript">
  <!--
  function onSubmit() {
    var theForm = document.getElementById("form_inventory_item_edit");
    theForm.submit();
  }

  //-->
  </script>

<form action="<?php echo url_for('@admin_inventory_item_edit?id='.$inventory_item_id.'&page='.$page.'&filter_name='.$filter_name.
'&filter_state='.$filter_state.
'&filter_conference_id='.$filter_conference_id.
'&sorting='.$sorting ) ?>" id="form_inventory_item_edit" method="POST" enctype="multipart/form-data" >
<input type="hidden" value="<?php echo $sf_request->getParameter("id") ?>" id="id" name="id">
<?php echo $form['_csrf_token']->render()?>
<div class="Div_Editor_Conteiner">
  <br><h2 class="EditorTitle"><?php echo __(!empty($inventory_item_id)?'Edit':'Add').' '.__('Inventory Item') ?></h2>
    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner">
      <?php echo $form[ InventoryItemPeer::ID ]->render(); ?>
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[InventoryItemPeer::TITLE]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ InventoryItemPeer::TITLE]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ InventoryItemPeer::TITLE]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[InventoryItemPeer::SKU]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ InventoryItemPeer::SKU]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ InventoryItemPeer::SKU]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[InventoryItemPeer::BRAND_ID]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ InventoryItemPeer::BRAND_ID]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ InventoryItemPeer::BRAND_ID]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
      <tr>
        <td class="left">
          <?php echo strip_tags( $form[InventoryItemPeer::CREATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ InventoryItemPeer::CREATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ InventoryItemPeer::CREATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form[InventoryItemPeer::UPDATED_AT]->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right">
         	<?php echo $form[ InventoryItemPeer::UPDATED_AT ]->render(); ?>
  	      <span class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form[ InventoryItemPeer::UPDATED_AT ]->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>
      
    <tr>
    <td></td>
      <td  style="padding-left:150px;">
        <?php if( empty($inventory_item_id) ) : ?>
          <input type="button" id="action_submit" value="Add" onclick='javascript:onSubmit()' />
        <?php else: ?>
        <input type="button" id="action_submit" value="Update" onclick='javascript:onSubmit()' />
      <?php endif; ?>
      <input type="button" id="action_submit" value="Cancel" onclick='javascript:document.location="<?php echo url_for('@admin_inventory_items?page='.$page.'&sorting='.$sorting.'&filter_name='.$filter_name.'&filter_state='.$filter_state.
'&filter_conference_id='.$filter_conference_id
) ?>"' />
      </td>
    </tr>
  </table>
</div>

</form>





