<?php

class myUser extends sfGuardSecurityUser//sfBasicSecurityUser
{  
  public function getAccessAsText() {
    $ResStr= '';
/*    $IsSuperAdmin= $this->hasCredential('super admin');
    if ($IsSuperAdmin) {
      $ResStr= 'Super Admin';
      return $ResStr;
    } */
    $IsAdmin= $this->hasCredential('admin');
    if ($IsAdmin) {
      $ResStr.= (empty($ResStr)?'':',').' Admin';
      return $ResStr;
    }
    
    $IsEditor= $this->hasCredential('Editor');
    if ($IsEditor) {
      $ResStr.= (empty($ResStr)?'':',').'Editor';
      return $ResStr;
    }


    return $ResStr;
  }
  
  public function signIn($user, $remember = false, $con = null)
  {
    if ( !empty($_POST['remember_me']) ) {
      $remember_me= $_POST['remember_me'];
      if($remember_me == 'yes') $this->rememberMe($user);
    }
    return parent::signIn($user, $remember, $con );
  } 
  
  public function setReferer($referer)
  {
    $this->setAttribute('referer', $referer);
  }

  public function rememberMe(sfGuardUser $pUser)
  {
  }
  
}