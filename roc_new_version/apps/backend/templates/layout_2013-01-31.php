<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include_http_metas() ?>
	<?php include_metas() ?>
	<?php include_title() ?>
	<link rel="shortcut icon" href="/favicon.ico" />
	<?php include_stylesheets() ?>
	<?php include_javascripts() ?>
</head>
<body>
<?php 
$CurrentCulture = $sf_context->getUser()->getCulture();
?>
<div id="wrap">
 <div id="header">
  <div id="logo">
    <?php echo image_tag('logo.png',array('alt'=>"Logo") ) ?>
    App Name – Dev 1.0 
  </div>    
  </div>
	<div id="main_nav">
		<?php include_partial( 'admin/admin_menu', array( ) ) ?>
	</div>

	<div id="main">
		<div id="content">
			<?php echo $sf_content ?>
		</div>
		<div id="footer">
			<?php if ( !( $sf_context->getUser()->isAuthenticated() ) ) : ?>
			<small> <?php echo link_to("Login",'@sf_guard_signin' ); ?>&nbsp; </small> 
			<?php  else:  ?>
			<small> <?php  echo link_to("Logout",'@sf_guard_signout' );  ?>&nbsp; 
			<?php //Util::deb($sf_context->getUser()->isAuthenticated());
			echo 'Logged as <b>' . $sf_context->getUser()->getGuardUser()->getUsername().'</b>&nbsp;('.
			$sf_context->getUser()->getAccessAsText(). ' )' ?>
			
			</small>
			<?php  endif; ?>
			<small>
            <p>
			<b><?php  echo sfConfig::get('app_application_name') .'</b> '.sfConfig::get('app_application_version')?>
            </p>
            <p>	(c) 2012 App Name. </p>
			</small>
		</div>
	</div>
</div>
</body>
</html>
