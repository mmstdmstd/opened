<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include_http_metas() ?>
	<?php include_metas() ?>
	<?php include_title() ?>
	<link rel="shortcut icon" href="/favicon.ico" />
	<?php include_stylesheets() ?>
	<?php include_javascripts() ?>

	<script type="text/javascript" language="JavaScript">
		<!--

		function SiteInventoryItemsImport() {
			if ( !confirm("Do you want to run Cron for Inventory Items Import ?") ) return;
			var Url= '<?php echo Util::cross_app_link_to("frontend","@cron_inventory_items_import",array( 'manually'=>1 ) ) ?>'
			document.location= Url
		}

		function SiteInventoryItemsImportSmallTest() {
			if ( !confirm("Do you want to run Cron for Inventory Items Import (Small Test) ?") ) return;
			var Url= '<?php echo Util::cross_app_link_to("frontend","@cron_inventory_items_import",array( 'manually'=>1, 'small_test'=>1 ) ) ?>'
			document.location= Url
		}
		//-->
	</script>

</head>
<body style="background-color:#eef0f1;">
<!-- sz added -->
<div id="wrapper-div">

	<div id="header-div">
		<div id="logo_div">
			<a style="outline:none;" href="<?php echo url_for("@homepage"); ?>">
				<?php echo image_tag('login-logo3.png',array('alt'=>"Ray O'Herron Admin Management") ) ?>
			</a>
			<span id="header-note">User: admin&nbsp;&nbsp;<a href="">Logout</a></span>
		</div>
	</div><!-- close header -->

	<div id="sidebar_div"><!-- sidebar with menu -->
		<div id="backend-menu-div">
			<div id="category-buttons">
<!-- sz added -->
        <?php $page=$_SERVER['REQUEST_URI']; ?>
<!-- end sz added see also below list items php code -->
				<ul>

					<li <?php  if ($page=='/backend.php/') echo 'id=currentpage1'; ?>>
						<a id="menu-item1" href="<?php echo url_for("@homepage"); ?>"></a>
					</li>

					<li <?php  if ($page=='/backend.php/admin/ordereds') echo 'id=currentpage2'; ?> >
						<a id="menu-item2" href="<?php echo url_for("@admin_ordereds"); ?>"></a>
					</li>

					<li <?php  if ($page=='/backend.php/admin/youtube_videos') echo 'id=currentpage3'; ?> >
						<a id="menu-item3" href="<?php echo url_for("@admin_youtube_videos"); ?>"></a>
					</li>

					<li>
						<a id="menu-item4" href="<?php echo url_for("@admin_paperworks"); ?>"></a>
					</li>

					<li>
						<a id="menu-item5" href="<?php echo url_for("@admin_documents"); ?>"></a>
					</li>

					<li>
						<a id="menu-item6" href="<?php echo url_for("@admin_size_charts"); ?>"></a>
					</li>

					<li>
					  <a id="menu-item7" href="<?php echo url_for("@admin_import_product_lines"); ?>"></a>
					</li>

					<li>

					</li>
				</ul>
			</div>

		</div>


		<p id="side-div"><b>Webmaster Tools</b></p>
		<a class="side-div-link" href="" onclick="javascript:SiteInventoryItemsImport();">Inventory Items Import</a>
		<a class="side-div-link" href="" onclick="javascript:SiteInventoryItemsImportSmallTest();">Inventory Items Small Test</a>
		<a class="side-div-link" href="<?php echo url_for("@admin_import_color_mapping"); ?>">Color Mapping</a>
		<a class="side-div-link" href="<?php echo url_for("@admin_import_matrix_item_sizes"); ?>">Import Matrix Sizes</a>
   
	</div><!-- close sidebar div-->


	<?php echo $sf_content ?>


</div><!-- close content -->




<div style="clear:both;"></div>
<div id="footer_div">
	<p id="footer_text">&copy;&nbsp;2011-2013&nbsp;Ray O'Herron Co., Inc. All Rights Reserved.
	</p>
	<a id="wgc" href="http://www.magimpact.com" title="who are we?">
		<img alt="Powered by Magnetic Impact" border="0px" src="/images/Powered-by-Magimpact.jpg" />
 	</a>
</div>
</div><!-- close wrapper-->

<!-- end sz added -->

	<?php return; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include_http_metas() ?>
	<?php include_metas() ?>
	<?php include_title() ?>
	<link rel="shortcut icon" href="/favicon.ico" />
	<?php include_stylesheets() ?>
	<?php include_javascripts() ?>
</head>
<body>
<?php
$CurrentCulture = $sf_context->getUser()->getCulture();
?>
<div id="wrap-login">
	<div id="header">
		<div id="logo-login">
			<?php echo image_tag('login-logo.png',array('alt'=>"Logo") ) ?>
			<!--       App Name – Dev 1.0  -->
		</div>
	</div>
	<div id="main_nav">
		<?php include_partial( 'admin/admin_menu', array( ) ) ?>
	</div>

	<div id="main">
		<div id="content">
			<?php echo $sf_content ?>
		</div>
		<!--
		<div id="footer">
			<?php if ( !( $sf_context->getUser()->isAuthenticated() ) ) : ?>
			<small> <?php echo link_to("Login",'@sf_guard_signin' ); ?>&nbsp; </small>
			<?php  else:  ?>
			<small> <?php  echo link_to("Logout",'@sf_guard_signout' );  ?>&nbsp;
			<?php //Util::deb($sf_context->getUser()->isAuthenticated());
			echo 'Logged as <b>' . $sf_context->getUser()->getGuardUser()->getUsername().'</b>&nbsp;('.
			$sf_context->getUser()->getAccessAsText(). ' )' ?>

			</small>
			<?php  endif; ?>
			<small>
            <p>

			<b><?php  echo sfConfig::get('app_application_name') .'</b> '.sfConfig::get('app_application_version')?>

            </p>
            <p>	(c) 2012 App Name. </p>
			</small>
		</div>
-->
	</div>
</div>
</body>
</html>
