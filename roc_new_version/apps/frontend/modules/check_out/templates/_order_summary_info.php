<?php $HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()); ?>

<style type="text/css">
  #order-summary {width:254px; overflow:hidden; float:left; margin:0 0 0 28px; background:#bcbdc1; padding:3px 3px 0 3px;}
  #order-summary h3 {width:auto; font:bold 120% arial,sans-serif; padding:.4em 0 .4em 10px; color:#222; background:#bcbdc1; line-height:1;}
  #order-summary h4 {font:bold 120% arial,sans-serif; padding:.8em 0 .7em 10px; color:#222; margin:0; line-height:1;}
  #order-summary p {margin:0;}
  #order-summary div {background:#e8e8e8; margin-bottom:3px; position:relative; overflow:hidden;}
  #customer-os h4 {padding:1.2em 0 1.1em 10px;}
  #customer-os p {margin:0 0 .7em 10px;}
  .normal-padding {padding:5px 0 5px 10px !important;}
  #review-order-os table {width:88%; margin-left:10px; margin-bottom:10px; border:none;}
  #review-order-os table td { border:none;}
</style>

<div id="order-summary">
  <h3>Order Summary</h3>

  <div id="customer-os">
    <h4>Customer</h4>
    <?php if ($CopiedhasLoggedUser) : ?>
      <script type="text/javascript" src="<?php Util::getServerHost() ?>js/jquery/jquery.js"></script>
      <script type="text/javascript">
        jQuery("#customer-os h4").css({"padding":"1em 0 .8em 10px"});
      </script>
      <p><?php echo $CopiedLoggedUserEmail ?></p>
    <?php endif; ?>
  </div>

  <div id="shipping-and-billing-os">
    <h4>Shipping Address</h4>
    <?php // echo ' $Copied_shipping_tax_data::<pre> '. print_r( $Copied_shipping_tax_data, true ) .'</pre><br>';
    if ($CopiedShippingAddress) : ?>

      <p style="margin:-.5em 0 0 1em;">
        <?php echo $CopiedCheckoutCopiedArray['name'] ?><br>
        <?php echo $CopiedCheckoutCopiedArray['address'] ?><br>
        <?php if (!empty($CopiedCheckoutCopiedArray['address_2'])) : ?>
          <?php echo $CopiedCheckoutCopiedArray['address_2'] ?><br>
        <?php endif; ?>
        <?php echo $CopiedCheckoutCopiedArray['city'] ?>,&nbsp;
        <?php echo $CopiedCheckoutCopiedArray['state'] ?>&nbsp;
        <?php echo $CopiedCheckoutCopiedArray['zip'] ?>
      <p>
      <?php endif; ?>

    <h4>Billing Address</h4>
    <?php if ($CopiedhasBillingAddress) : ?>
      <p style="margin:-.5em 0 1em 1em;">
        <?php echo $CopiedCheckoutCopiedArray['b_name'] ?><br>
        <?php echo $CopiedCheckoutCopiedArray['b_address'] ?><br>
        <?php if (!empty($CopiedCheckoutCopiedArray['b_address_2'])) : ?>
          <?php echo $CopiedCheckoutCopiedArray['b_address_2'] ?><br>
        <?php endif; ?>
        <?php echo $CopiedCheckoutCopiedArray['b_city'] ?>,&nbsp;
        <?php echo $CopiedCheckoutCopiedArray['b_state'] ?>&nbsp;
        <?php echo $CopiedCheckoutCopiedArray['b_zip'] ?><br>
        <?php echo $CopiedCheckoutCopiedArray['b_phone'] ?>
      </p>
    <?php endif; ?>
  </div>
  <div id="review-order-os">
    <h4>Review Order</h4>
    <?php if ($CopiedItemsCount > 0) : ?>
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <?php echo $CopiedItemsCount ?>&nbsp;Item(s)
          </td>
          <td style="text-align:right;">
            <?php echo Util::getDigitMoney($CopiedCommonSum, 'Money')  ?>
          </td>
        </tr>
        <tr>
          <td>
            Shipping
          </td>
          <td style="text-align:right;">
            <?php echo Util::getDigitMoney($lNewOrder->getShippingCost(), 'Money') ?>
          </td>
        </tr>
        <tr>
          <td>
            Tax
          </td>
          <td style="text-align:right;">
            <?php echo Util::getDigitMoney($lNewOrder->getTax(), 'Money') ?>
          </td>
        </tr>
        <tr>
          <td>
            Total
          </td>
          <td style="text-align:right;">
            <?php //echo (!empty($Copied_shipping_tax_data['estimated_total'])) ? Util::getDigitMoney($Copied_shipping_tax_data['estimated_total'], 'Money') : '' ?>
            <?php echo Util::getDigitMoney( $lNewOrder->getTotalPrice() + $lNewOrder->getTax() + $lNewOrder->getShippingCost(), 'Money') ?>
          </td>
        </tr>
      </table>
    <?php endif; ?>
  </div>
  <div id="payment-os">
    <h4>Payment</h4>

    <p style="margin:-.5em 0 1em 1em;">
      <?php echo OrderedPeer::getPaymentMethodAsText($CopiedPaymentData['payment_method']) ?><br>
      <?php echo $CopiedPaymentData['card_number'] ?><br>
    </p>

  </div>

</div>