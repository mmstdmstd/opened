<script type="text/javascript" language="JavaScript">
  <!--   

  function NewCustomerContinue() {
  	document.getElementById("new_customer_account_type").value= "New User";
    var theForm = document.getElementById("form_new_customer");
    theForm.submit();
  }
  
  function NewCustomerSaveAndContinue() {
  	document.getElementById("new_customer_account_type").value= "Existing User";
    var theForm = document.getElementById("form_new_customer");
    theForm.submit();
  } 
  
  
//-->
</script>  

<style type="text/css">
	#checkout-customer-page {overflow:hidden; clear:both;}
	#checkout-customer-page #customer-step {background-position:0 56px !important;}
	#checkout-customer-page #customer-step a {color:#fff; background-color:#6e6e70 !important;}
</style>

<div id="checkout-customer-page" class="checkout-section">
			<?php include_partial( 'check_out/top_menu', array(  ) ) ?>
      <?php include_partial( 'check_out/order_summary', array(  ) ) ?>
      <?php include_partial( 'check_out/customer_info', array( 'form'=>$form ) ) ?>
</div>