<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
$CheckoutArray= Cart::getShippingInfo();

$AdditionalFreightSum= Cart::getAdditionalFreightSum();
//Util::deb( $AdditionalFreightSum, '$AdditionalFreightSum::' );

?>

<style type="text/css">
	#order-summary {width:254px; overflow:hidden; float:left; margin:0 0 0 28px; background:#bcbdc1; padding:3px 3px 0 3px;}
	#order-summary h3 {width:auto; font:bold 120% arial,sans-serif; padding:.4em 0 .4em 10px; color:#222; background:#bcbdc1; line-height:1;}
	#order-summary h4 {font:bold 120% arial,sans-serif; padding:.8em 0 .7em 10px; color:#222; margin:0; line-height:1;}
	#order-summary p {margin:0;}
	#order-summary div {background:#e8e8e8; margin-bottom:3px; position:relative; overflow:hidden;}
	#customer-os h4 {padding:1.2em 0 1.1em 10px;}
	#customer-os p {margin:0 0 .7em 10px;}
	.normal-padding {padding:5px 0 5px 10px !important;}

	#review-order-os table {width:88%; margin-left:10px; margin-bottom:10px; border:none;}
	#review-order-os table td { border:none;}
</style>

<div id="order-summary">
	<h3>Order Summary</h3>

	<div id="customer-os">
		<h4>Customer</h4>
		<?php
		if( Cart::hasLoggedUser() ) : ?>
		<script type="text/javascript" src="<?php Util::getServerHost() ?>js/jquery/jquery.js"></script>
			<script type="text/javascript">
			jQuery("#customer-os h4").css({"padding":"1em 0 .8em 10px"});
			</script>
			<!--<input class="change-button" type="button"
			 onclick="javascrript:document.location= '<?php echo url_for( '@check_out_customer' ) ?>'" >-->
			<p><?php echo Cart::getLoggedUserEmail() ?></p>
		<?php endif; ?>
	</div>

	<div id="shipping-and-billing-os">
		<h4>Shipping Address</h4>
		<?php if( Cart::hasShippingAddress() ) : ?>
		<input class="change-button" style="top:7px !important;" type="button" onclick="javascrript:document.location= '<?php echo url_for( '@check_out_shipping?is_change=1' ) ?>'" >
		<p style="margin:-.5em 0 0 1em;">
		<?php echo $CheckoutArray['name'] ?><br>
		<?php echo $CheckoutArray['address'] ?><br>
		<?php if( !empty($CheckoutArray['address_2']) ) : ?>
		<?php echo $CheckoutArray['address_2'] ?><br>
		<?php endif; ?>
		<?php echo $CheckoutArray['city'] ?>,&nbsp;
		<?php echo $CheckoutArray['state'] ?>&nbsp;
		<?php echo $CheckoutArray['zip'] ?>
		<p>
		<?php endif; ?>

		<h4>Billing Address</h4>
		<?php if( Cart::hasBillingAddress() ) : ?>
		<p style="margin:-.5em 0 1em 1em;">
		<?php echo $CheckoutArray['b_name'] ?><br>
		<?php echo $CheckoutArray['b_address'] ?><br>
		<?php if( !empty($CheckoutArray['b_address_2']) ) : ?>
		<?php echo $CheckoutArray['b_address_2'] ?><br>
		<?php endif; ?>
		<?php echo $CheckoutArray['b_city'] ?>,&nbsp;
		<?php echo $CheckoutArray['b_state'] ?>&nbsp;
		<?php echo $CheckoutArray['b_zip'] ?><br>
		<?php echo $CheckoutArray['b_phone'] ?>
		</p>
		<?php endif; ?>
	</div>
	<div id="review-order-os">
		<h4>Review Order</h4>
		<?php if( Cart::getItemsCount() > 0 ) : ?>
        <?php
        $shipping_tax_data = Cart::getTaxShipData();
        //echo ' $shipping_tax_data::<pre> '. print_r( $shipping_tax_data, true ) .'</pre><br>';
        if ($CheckoutArray['b_is_department_purchase'] == 1) {
          $shipping_tax_data['est_tax'] = 0;
        }
        ?>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<?php echo Cart::getItemsCount() ?>&nbsp;Item(s)
				</td>
				<td style="text-align:right;">
					<span id="span_order_summary_est_common_sum"><?php echo Cart::getCommonSum(true) ?></span>
				</td>
			</tr>
			<tr>
				<td>
					Shipping
				</td>
				<td style="text-align:right;">
				
					<span id="span_order_summary_est_shipping"><?php echo (!empty($shipping_tax_data['est_shipping'])) ? Util::getDigitMoney($shipping_tax_data['est_shipping']+$AdditionalFreightSum,'Money' ) : Util::getDigitMoney( $AdditionalFreightSum,'Money' ) ?></span>
				</td>
			</tr>
			<tr>
				<td>
					Tax
				</td>
				<td style="text-align:right;">
					<span id="span_order_summary_est_tax"><?php echo (!empty($shipping_tax_data['est_tax'])) ?  Util::getDigitMoney($shipping_tax_data['est_tax'],'Money' )  : '$0.00' ?></span>
				</td>
			</tr>
			<tr>
				<td>
					Total
				</td>
				<td style="text-align:right;">
					<span id="span_order_summary_est_total"><?php echo (!empty($shipping_tax_data['estimated_total'])) ?  Util::getDigitMoney($shipping_tax_data['estimated_total'] + $AdditionalFreightSum,'Money' ) : Util::getDigitMoney( $AdditionalFreightSum,'Money' ) ?></span>
				</td>
			</tr>
		</table>
		<?php endif; ?>
	</div>
	<div id="payment-os">
		<h4>Payment</h4>
	</div>
<?php //echo link_to( "Review Order" , '@check_out_review_order' ) ?>
 <?php //echo link_to( "Payment" , '@check_out_payment' ) ?>

</div>
