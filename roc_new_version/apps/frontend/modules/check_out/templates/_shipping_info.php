<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
  <?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
  $UsersCartArray= Cart::getUsersCartArray();
  $UsersCartItemsSelectedCount= count($UsersCartArray);
  ?>

	<style type="text/css">
	#checkout-shipping-and-billing {}
	#checkout-shipping-and-billing {border:4px solid #666; float:left; padding-bottom:37px; margin:0 0 0 37px; width:590px; background:#f1f2f4;}
	#checkout-shipping-and-billing h5 {font:bold 120% arial,sans-serif; margin: 1.75em 0 .75em 2.2em; padding:0;}
	#checkout-shipping-and-billing p {margin:0; padding:0; position:relative;}
	#checkout-shipping-and-billing table {margin:0 0 0 2.65em; width:89%; border:none !important;}
	#checkout-shipping-and-billing table td {border:none !important;}
	#shipping-and-billing-os {background-color:#f1f2f4 !important;}
	#shipping_address, #shipping_name, #shipping_address_2, #shipping_b_name, #shipping_b_address, #shipping_b_address_2  {width:18em;}
	#shipping_city, #shipping_b_city {width:12em; margin-right:.35em;}
	#shipping_state, #shipping_country, #shipping_b_state, #shipping_b_country  {width:8em; padding:.15em .13em .15em 0; position:relative; top:0px;	padding:.05em\9 .08em\9 .05em\9 0em; margin-right:.35em;}
	#shipping_zip, #shipping_b_zip {width:5em;}
	#shipping_country, #shipping_b_country {width:10.5em !important;}
	hr {margin:2em 0 0 0;}
	p#same-as-shipping-address {position:absolute; top:2.1em; left:14.75em; margin:0; padding:0; font-size:90%; width:20em;}
	p#same-as-shipping-address input {position:relative; top:.1em;}
	#same-as-shipping-address legend {display:inline;}
	.error p {margin:.2em 0 .2em 7.2em !important; color:red;}
	#shipping_password, #shipping_password_2 {width:12em;}
	.error_list {list-style:none; margin:0 0 1em 0; padding:0; color:red; margin-left:2.75em;}

	/*
	 width:123px\0/; for ie8
	_width:123px; for ie6
	*width:123px; for ie7
	width:123px\9; for ie9
	*/

	</style>
<?php
    $shipping_tax_data = Cart::getTaxShipData();
?>
    <script type="text/javascript">
jQuery(document).ready(function() {


    var zip_code = jQuery('#shipping_zip').val();
    if (zip_code == '') {
      zip_code= '<?php echo (!empty($shipping_tax_data['postal_code'])) ? $shipping_tax_data['postal_code'] : '' ?>'
      // alert( zip_code )
      if ( zip_code.length == 5 ) {
        jQuery('#shipping_zip').attr('value', zip_code );
        getState(zip_code);
      }
    }
    jQuery('#shipping_zip').change(function() {
        zip_code = jQuery('#shipping_zip').val();
        getState(zip_code);
    });
    /*jQuery('#shipping_city').change(function() {
        var city = jQuery('#shipping_city').val();
        getState(city);
    }); */ 


    var b_zip_code = jQuery('#shipping_b_zip').val();
    if (b_zip_code == '') {
      /* zip_code= '<?php echo (!empty($shipping_tax_data['postal_code'])) ? $shipping_tax_data['postal_code'] : '' ?>'
      // alert( zip_code )
      if ( zip_code.length == 5 ) {
        jQuery('#shipping_b_zip').attr('value', zip_code );
        getState(zip_code);
      } */
    }
    jQuery('#shipping_b_zip').change(function() {
        b_zip_code = jQuery('#shipping_b_zip').val();
        getb_State(b_zip_code);
    });
    /* jQuery('#shipping_city').change(function() {
        var b_city = jQuery('#shipping_b_city').val();
        getb_State(b_city);
    }); */
    var MakeAutoSubmit= '<?php echo $MakeAutoSubmit ?>'

//alert( "END Ready MakeAutoSubmit::" + MakeAutoSubmit )
    /* if ( MakeAutoSubmit.toString() == "1" ) {
      SaveReviewOrder()
    } */

});

function onGetZipCode(data) {
  //alert("onGetZipCode data::"+var_dump(data))
  if ( data == null || typeof data == "undefined" ) return;
  jQuery('#shipping_city').val( data.city );
  jQuery('#shipping_state').val( data.state );
  jQuery('#shipping_country').val( data.country );
}
function getState(zipcode) {
  // alert( "getState zipcode::"+var_dump( zipcode ) )
  var HRef= 'http://zip.elevenbasetwo.com/v2/US/'+zipcode
    jQuery.getJSON(HRef,   {  },
    onGetZipCode,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
  );
  return false;
}

function onGetBZipCode(data) {
  // alert("onGetBZipCode data::"+var_dump(data))
  if ( data == null || typeof data == "undefined" ) return;

  jQuery('#shipping_b_city').val( data.city );
  jQuery('#shipping_b_state').val( data.state );
  jQuery('#shipping_b_country').val( data.country );
}
function getb_State(b_zipcode) {
  //alert( "getState zipcode::"+var_dump( b_zipcode ) )
  var HRef= 'http://zip.elevenbasetwo.com/v2/US/'+b_zipcode
    jQuery.getJSON(HRef,   {  },
    onGetBZipCode,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
  );
  return false;
}

</script>

	<div id="checkout-shipping-and-billing">


  <h5>Shipping Address</h5>
  <?php if ( $UsersCartItemsSelectedCount == 0 ) : ?><br>
    <strong style="margin-left:2.7em;">Your shopping cart is empty</strong>
    </div>
    <?php return; ?>
  <?php endif; ?>


  <form action="<?php echo url_for('@check_out_shipping') ?>" id="form_shipping" method="POST" enctype="multipart/form-data" >
  <?php echo $form['_csrf_token']->render()?>

    <?php echo $form->renderGlobalErrors() ?>

    <table class="Table_Editor_Conteiner" cellpadding="1" cellspacing="0" border="0">

      <tr>
        <td colspan="2">
         	<p style="margin-bottom:1em;">Enter the street address where you want your order<br> shipped (no post office boxes please):</p>
        </td>
      </tr>

      <tr>
        <td class="left" width="16.5%">
          <label for="shipping_name"><?php echo strip_tags( $form['name']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="left">
         	<?php echo $form['name']->render(); ?>
				</td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['name']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <label for="shipping_address"><?php echo strip_tags( $form['address']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right" >
         	<?php echo $form['address']->render(); ?>
				</td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['address']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <label for="shipping_address_2"><?php // echo strip_tags( $form['address_2']->renderLabel() ) ?>&nbsp;</label>
        </td>
        <td class="right" >
         	<?php echo $form['address_2']->render(); ?>
				</td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['address_2']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <label for="shipping_zip"><?php echo strip_tags( $form['zip']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right" >
         	<?php echo $form['zip']->render(); ?>
				</td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['zip']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <label for="shipping_city"><?php echo strip_tags( $form['city']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right" >
          <?php echo $form['city']->render(); ?>
  	      <label for="shipping_state"><?php echo strip_tags( $form['state']->renderLabel() ) ?>&nbsp;:</label>
          <?php echo $form['state']->render(); ?>
  	    </td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['city']->renderError() )  :"" ) ?>
  	      </p>
  	      <p>
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['state']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <label for="shipping_country"><?php echo strip_tags( $form['country']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right" >
         	<?php echo $form['country']->render(); ?>
        </td>
      </tr>

			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['country']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td colspan="2">
          <hr>
        </td>
      </tr>

    </table>


  <div style="position:relative; overflow:hidden;">
	<h5>Billing Address</h5>
    <p id="same-as-shipping-address"><?php echo $form['is_billing_address']->render(); ?>
    <legend for="shipping_is_billing_address"><?php echo strip_tags( $form['is_billing_address']->renderLabel() ) ?></legend></p>
  </div>
    <table class="Table_Editor_Conteiner" cellpadding="1" cellspacing="0" border="0">

      <tr>
        <td colspan="2">
         	<p style="margin-bottom:1em;">Enter the billing address:<br>
					<span style="font-size:90%;">(if paying by credit card, please make sure this address<br> mathes your credit card billing statement address)</span></p>
        </td>
      </tr>

      <tr>
        <td class="left" width="16.5%">
          <?php echo strip_tags( $form['b_name']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left">
         	<?php echo $form['b_name']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_name']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form['b_address']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right" >
         	<?php echo $form['b_address']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_address']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php //echo strip_tags( $form['b_address_2']->renderLabel() ) ?>&nbsp;
        </td>
        <td class="right" >
         	<?php echo $form['b_address_2']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_address_2']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form['b_zip']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right" >
         	<?php echo $form['b_zip']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_zip']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form['b_city']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right" >

          <?php echo $form['b_city']->render(); ?>

  	      <?php echo strip_tags( $form['b_state']->renderLabel() ) ?>&nbsp;:
          <?php echo $form['b_state']->render(); ?>


        </td>
			</tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_city']->renderError() )  :"" ) ?>
  	      </p>
					<p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_state']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form['b_country']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right" >
         	<?php echo $form['b_country']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_country']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

      <tr>
        <td class="left">
          <?php echo strip_tags( $form['b_phone']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="right" >
         	<?php echo $form['b_phone']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['b_phone']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          &nbsp;
        </td>
        <td class="right" >
         	<?php echo $form['b_is_department_purchase']->render(); ?>&nbsp;<?php echo strip_tags( $form['b_is_department_purchase']->renderLabel() ) ?>
        </td>
      </tr>


			<tr>
        <td colspan="2">
          <hr>
        </td>
      </tr>

		</table>

		<div style="position:relative; overflow:hidden;">
		 <h5>Customer Account</h5>
		 <span style="position:absolute; top:2.6em; left:16.7em; font-size:90%;">(optional)</span>
		</div>
		<table class="Table_Editor_Conteiner" cellpadding="1" cellspacing="0" border="0">
      <tr>
        <td colspan="2">
					<p style="margin-bottom:1.8em;">Save time on your next order by setting up an account to save your billing and shipping<br>
          information. It's easy! Just create a password bellow, and use it to log in for your next order.</p>
        </td>
      </tr>


      <tr>
        <td class="left" width="23%">
          email:
        </td>
        <td class="left">
         	<?php echo Cart::getLoggedUserEmail() ?>
        </td>
      </tr>


			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['password']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form['password']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left">
         	<?php echo $form['password']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['password']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php echo strip_tags( $form['password_2']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left">
         	<?php echo $form['password_2']->render(); ?>
        </td>
      </tr>
			<tr class="error">
				<td colspan="2">
  	      <p>
						<?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['password_2']->renderError() )  :"" ) ?>
  	      </p>
        </td>
      </tr>

			<tr>
        <td colspan="2">
          <hr style="margin:2em 0 2.25em 0;">
        </td>
      </tr>

      <tr>
        <td>&nbsp;</td>
        <td>
          <input id="btn_SaveReviewOrder" type="button" onclick="javascript:SaveReviewOrder()">
        </td>
      </tr>

    </table>

  </form>

	</div>
