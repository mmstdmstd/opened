<script type="text/javascript" language="JavaScript">
  <!--

  jQuery(document).ready(function() {
    onChangePaymentMethod('CC');
  });

  function onChangePaymentMethod( payment_method ) {
    if ( payment_method == 'CC' ) { // Credit Card
      document.getElementById("div_payment_method_message").style.display= "none"
      document.getElementById("tr_authorized_editor").style.display= GetShowTRMethod()
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_AuthorizeNet_DPM_HTML') ?>'
      jQuery.get(HRef,   {  },
        ongetAuthorizeNet_DPM,
        function(x,y,z) {   //Some sort of error
          alert(x.responseText);
        }
      );
      document.getElementById("hidden_payment_method").value= "CC"
    } //if ( payment_method == 'CC' ) { // Credit Card

    if ( payment_method == 'PP' ) { // PayPal
      document.getElementById("tr_authorized_editor").style.display= "none"
      document.getElementById("div_payment_method_message").style.display= "block"
      document.getElementById("span_payment_method_message").innerHTML= "Paypal"
      document.getElementById("hidden_payment_method").value= "PP"
    } // if ( payment_method == 'PP' ) { // PayPal

    if ( payment_method == 'GC' ) { // Google Checkout
      /*document.getElementById("tr_authorized_editor").style.display= "none"
      document.getElementById("div_payment_method_message").style.display= "block"
      document.getElementById("span_payment_method_message").innerHTML= "Google Checkout"
      document.getElementById("hidden_payment_method").value= "GC" */
      document.getElementById("div_payment_method_message").style.display= "none"
      document.getElementById("tr_authorized_editor").style.display= GetShowTRMethod()
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_GoogleCheck_Button_HTML') ?>'
      jQuery.get(HRef,   {  },
        ongetGoogleCheck_Button_HTML,
        function(x,y,z) {   //Some sort of error
          alert(x.responseText);
        }
      );
      document.getElementById("hidden_payment_method").value= "CC"

    } //if ( payment_method == 'GC' ) { // Google Checkout

    if ( payment_method == 'DP' ) { // Department Purchase
      document.getElementById("div_payment_method").innerHTML= "";
      document.getElementById("tr_authorized_editor").style.display= GetShowTRMethod()
      document.getElementById("div_payment_method").style.display= "block"
      document.getElementById("div_payment_method_message").style.display= "none"
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_Department_Purchase_HTML') ?>'
      jQuery.get(HRef,   {  },
        ongetDepartment_Purchase,
        function(x,y,z) {   //Some sort of error
          alert(x.responseText);
        }
      );
      document.getElementById("hidden_payment_method").value= "DP"
    } // if ( payment_method == 'DP' ) { // Department Purchase

  }

  function ongetGoogleCheck_Button_HTML(data) {
    document.getElementById("div_payment_method").innerHTML= data;
  }


  function ongetDepartment_Purchase(data) {
    document.getElementById("div_payment_method").innerHTML= data;
  }

  function ongetAuthorizeNet_DPM(data) {
    document.getElementById("div_payment_method").innerHTML= data;
  }

  function MakePayment() {
    var theForm = document.getElementById("form_payment");
    theForm.submit();
  }


  function onchangeDepartmentPurchaseAuthorized() {
    var cbx_authorized = document.getElementById("cbx_authorized").checked;
    if ( !cbx_authorized ) {
      document.getElementById("submit_order_btn_div").style.display = "none"
      document.getElementById("submit_order_btn_inactive_div").style.display = "block"
    } else {
      document.getElementById("submit_order_btn_div").style.display = "block"
      document.getElementById("submit_order_btn_inactive_div").style.display = "none"
    }
  }

  function onSubmitOrder() {
    var PaymentMethod= document.getElementById("hidden_payment_method").value
    var HRef= '<?php echo url_for('@check_out_payment?payment_method=') ?>'+PaymentMethod;
    document.location= HRef
  }

//-->
</script>

<style type="text/css">
#checkout-payment-page {overflow:hidden; clear:both;}
#checkout-payment-page #payment-step {background-position:0 56px !important;}
#checkout-payment-page #payment-step a {color:#fff; background-color:#6e6e70 !important;}

#checkout-payment {border:4px solid #666; float:left; padding-bottom:37px; margin:0 0 0 37px; width:590px; background:#f1f2f4; margin-bottom:8em;}
#checkout-payment h5 {font:bold 120% arial,sans-serif; margin: 1.75em 0 .75em 2.35em; padding:0;}
#checkout-payment p {margin:0; padding:0; position:relative;}
#checkout-payment table {margin:0 0 0 2.65em; width:89%; border:none;}
#payment-type {margin-left:2.9em; margin-bottom:.2em;}
#payment_method {margin-left:5.2em;}
#payment-os {background-color:#f1f2f4 !important;}
</style>

<div id="checkout-payment-page" class="checkout-section">
  <?php include_partial( 'check_out/top_menu', array() ) ?>
  <?php include_partial( 'check_out/order_summary', array() ) ?>

<div id="checkout-payment">

  <div id="page_PaymentEdit" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
    <div id="div_PaymentEdit"></div>
    <?php include_partial( 'check_out/PaymentEdit', array( 'form'=>$form ) )         ?>
  </div>


  <div id="div_payment_method_message" style=" padding-left: 20px; border:0px dotted green; display:none; ">
    You have selected <span id="span_payment_method_message"></span>.<br><br>
    Please click the "Make Payment" button below and you will be redirected to the site to complete your payment.<br><br>
    After your payment is accepted, you will be returned to the Ray O'Herron website.<br><br>
    <input type="button" id="btn_MakePayment" onclick="javascript:onSubmitOrder()" >

<!-- sz new code-->
<!--<div style="float:right; margin-right:283px;width:100px; height:100px;"> -->
    <div style="position:relative; left:28px; top:20px; width:100px; height:100px;border:solid 1px #bcbdc1; padding:8px;background-color:#ffffff;">
      <a target="_blank" href="https://www.paypal.com/us/verified/pal=rayoherron%40oherron%2ecom">
      <img border="0" alt="Official PayPal Seal" src="https://www.paypal.com/en_US/i/icon/verification_seal.gif">
      </a>
    </div>
<!-- end sz new code-->

  </div>
  <input id="hidden_payment_method" type="hidden" value="">

 </div>
</div>