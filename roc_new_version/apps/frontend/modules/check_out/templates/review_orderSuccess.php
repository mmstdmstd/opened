<style type="text/css">
#checkout-review-order-page {overflow:hidden; clear:both;}
#checkout-review-order-page #review-order-step {background-position:0 56px !important;}
#checkout-review-order-page #review-order-step a {color:#fff; background-color:#6e6e70 !important;}
</style>

<div id="checkout-review-order-page" class="checkout-section">
      <?php include_partial( 'check_out/top_menu', array() ) ?>
      <?php include_partial( 'check_out/order_summary', array(  ) ) ?>
      <?php include_partial( 'check_out/review_order', array( 'form'=>$form ) ) ?>
</div>

