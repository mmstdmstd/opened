<script type="text/javascript" language="JavaScript">
  <!--

  jQuery(document).ready(function() {
    onChangePaymentMethod('CC');
  });

  function onChangePaymentMethod( payment_method ) {
    // var payment_method = document.getElementById("payment_method");
    //alert(" onChangePaymentMethod() payment_method::"+payment_method)
    /*       CC : Credit Card
      PP : PayPal
      GC : Google Checkout
      DP : Department Purchase */

    if ( payment_method == 'CC' ) { // Credit Card
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_AuthorizeNet_DPM_HTML') ?>'
      //alert(HRef)
      jQuery.get(HRef,   {  },
        ongetAuthorizeNet_DPM,
        function(x,y,z) {   //Some sort of error
          alert(x.responseText);
        }
      );
    } //if ( payment_method == 'CC' ) { // Credit Card

    if ( payment_method == 'PP' ) { // PayPal
      var HRef= '<?php echo url_for('@check_out_payment?payment_method=PP') ?>';
      document.location= HRef
    } // if ( payment_method == 'PP' ) { // PayPal

    if ( payment_method == 'GC' ) { // Google Checkout
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_GoogleCheckout_HTML') ?>'
      //alert( HRef )
      document.location= HRef
    } //if ( payment_method == 'GC' ) { // Google Checkout

    if ( payment_method == 'DP' ) { // Department Purchase
      var HRef= '<?php echo AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_Department_Purchase_HTML') ?>'
      //alert(HRef)
      jQuery.get(HRef,   {  },
        ongetAuthorizeNet_DPM,
        function(x,y,z) {   //Some sort of error
          alert(x.responseText);
        }
      );
    } // if ( payment_method == 'DP' ) { // Department Purchase

  }
  function ongetAuthorizeNet_DPM(data) {
    //alert( "ongetChangePaymentMethod data::"+data )
    document.getElementById("div_payment_method").innerHTML= data;
  }

  function MakePayment() {
   // alert("MakePayment")
    var theForm = document.getElementById("form_payment");
    theForm.submit();
  }
//-->
</script>

<style type="text/css">
#checkout-payment-page {overflow:hidden; clear:both;}
#checkout-payment-page #payment-step {background-position:0 56px !important;}
#checkout-payment-page #payment-step a {color:#fff; background-color:#6e6e70 !important;}

#checkout-payment {border:4px solid #666; float:left; padding-bottom:37px; margin:0 0 0 37px; width:590px; background:#f1f2f4; margin-bottom:8em;}
#checkout-payment h5 {font:bold 120% arial,sans-serif; margin: 1.75em 0 .75em 2.35em; padding:0;}
#checkout-payment p {margin:0; padding:0; position:relative;}
#checkout-payment table {margin:0 0 0 2.65em; width:89%; border:none;}
#payment-type {margin-left:2.9em; margin-bottom:.2em;}
#payment_method {margin-left:5.2em;}
#payment-os {background-color:#f1f2f4 !important;}
</style>

<div id="checkout-payment-page" class="checkout-section">
  <?php include_partial( 'check_out/top_menu', array() ) ?>
  <?php include_partial( 'check_out/order_summary', array() ) ?>

<div id="checkout-payment">

  <div id="page_PaymentEdit" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
    <div id="div_PaymentEdit"></div>

    <?php // include_component( 'check_out', 'PaymentEdit', array( /*'recruit_id'=>$recruit_id*/ ) )
    // <?php include_component( 'admin', 'recruit_verbal_commitment', array( 'recruit_id'=>$recruit_id ) )
    // include_partial( 'check_out/payment_info', array( 'form'=>$form ) )
    include_partial( 'check_out/PaymentEdit', array( 'form'=>$form ) )         ?>
  </div>

 </div>
</div>