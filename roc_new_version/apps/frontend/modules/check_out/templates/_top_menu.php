<div id="checkout-steps-head">

  <!-- website message alert -->
  <?php
  if (trim(sfConfig::get('app_website_message_products_alert'))!='') {
  ?>
  <br />
  <div style="position:absolute;left:35px;width:885px">
  <p class="website_alert_message"><?php echo sfConfig::get('app_website_message_products_alert') ?></p>
  </div>
  <br />
  <?php
  }
  ?>

	<h1>Checkout</h1>
	<h2>Following these easy steps<br> to complete your order.</h2>
	<ol>
		<li id="customer-step"><a>Customer</a></li>
		<li id="shipping-billing-step"><a>Shipping/Billing</a></li>
    <li id="review-order-step"><a>Review Order</a></li>
		<li id="payment-step"><a>Payment</a></li>
	</ol>
</div>