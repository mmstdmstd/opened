<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
$UsersCartArray= Cart::getUsersCartArray();
$UsersCartItemsSelectedCount= count($UsersCartArray);

?>

  <h5>Payment</h5>

<?php //echo "HERE:" . AppUtils::getHostForJQuery( sfContext::getInstance()->getConfiguration() ).url_for('@get_AuthorizeNet_DPM_HTML'); //debug ?>

  <?php if ( $UsersCartItemsSelectedCount == 0 ) : ?><br>
    <strong style="margin-left:2.7em;">Your shopping cart is empty</strong>
    <?php return; ?>
  <?php endif; ?>

  <?php if( !Cart::hasLoggedUser() ) : ?>
    <strong style="margin-left:2.7em;">You are not logged in</strong>
    <?php return; ?>
  <?php endif; ?>

  <?php if( !Cart::hasShippingAddress() ) : ?>
    <strong style="margin-left:2.7em;">You did not fill in your shipping address</strong>
    <?php return; ?>
  <?php endif; ?>

  <form action="<?php echo url_for('@check_out_payment') ?>" name="form_payment" id="form_payment" method="POST"  enctype="multipart/form-data"  >
  <?php echo $form['_csrf_token']->render() ?>

    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <table class="Table_Editor_Conteiner" style="border:0px dotted" width="00%">
      <tr>
        <td class="left" style="padding-bottom:20px">
         	<?php echo $form['payment_method']->render(); ?>
  	      <span class="error">
  	        <?php echo $form['payment_method']->renderError() ?>
  	      </span>
        </td>
      </tr>
    </table>
  </form>
  <table class="Table_Editor_Conteiner" style="border:0px dotted" width="00%">
    <tr style="height:20px"><td style="border-top:1px solid #999"><br /></td></tr>
    <tr id="tr_authorized_editor" style="display:none">
      <td class="left">
        <div style="border:0px dotted red;" id="div_payment_method"></div>
      </td>
    </tr>
  </table>
<?php return; ?>



      <tr>
        <td class="left" width="25%">
          <?php // echo strip_tags( $form['card_number']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left" width="75%">
         	<?php // echo $form['card_number']->render(); ?>
  	      <span class="error">
  	        <?php //  echo $form['card_number']->renderError() ?>
  	      </span>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php // echo strip_tags( $form['expiration_date_year']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left" >
         	<?php // echo $form['expiration_date_year']->render(); ?>&nbsp;
          <?php // echo $form['expiration_date_month']->render(); ?>
  	      <span class="error">
  	        <?php //echo ( $sf_request->isMethod('post') ? strip_tags( $form['expiration_date_year']->renderError() )  :"" ) ?>&nbsp;
            <?php //echo ( $sf_request->isMethod('post') ? strip_tags( $form['expiration_date_month']->renderError() )  :"" ) ?>
  	      </span>
        </td>
      </tr>


      <tr>
        <td class="left">
          <?php // echo strip_tags( $form['card_security_code']->renderLabel() ) ?>&nbsp;:
        </td>
        <td class="left" >
         	<?php // echo $form['card_security_code']->render(); ?>
	        <?php // echo ( $sf_request->isMethod('post') ? strip_tags( $form['card_security_code']->renderError() )  :"" ) ?>
        </td>
      </tr>


      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>

      <tr>
        <td>&nbsp;</td>
        <td align="center" >
          <input type="button" id="btn_MakePayment" onclick="javascript:MakePayment()">
        </td>
      </tr>

    </table>


  </form>

