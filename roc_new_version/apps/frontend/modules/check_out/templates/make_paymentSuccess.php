<script type="text/javascript" language="JavaScript">
  <!--  
  
  function MakePrint() {
    document.getElementById("span_print_buttons").style.display="none";
    window.print()
    document.getElementById("span_print_buttons").style.display="block";
  }     
  //-->
  </script>


  <?php if ( $IsOrderSent ) : ?>
    <table border="0" width="720px" >

      <tr>
        <td  >
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Checkout</b>          
        </td>  
        <td align="right">
          <span id="span_print_buttons" >
            <a onclick="javascript:MakePrint()">Print this page</a> 
          </span>
        </td>  
      </tr>  

      <tr>
        <td  >
          &nbsp;
        </td>  
        <td>
          &nbsp;
        </td>  
      </tr>  
      
      <tr>
        <td  >
          <?php include_partial( 'check_out/order_summary_info', array( 'CopiedCheckoutCopiedArray'=> $CopiedCheckoutCopiedArray, 'CopiedhasLoggedUser'=> $CopiedhasLoggedUser, 
            'CopiedLoggedUserEmail'=> $CopiedLoggedUserEmail, 'CopiedShippingAddress'=> $CopiedShippingAddress, 'CopiedhasBillingAddress'=> $CopiedhasBillingAddress, 'CopiedItemsCount'=> $CopiedItemsCount,
            'Copied_shipping_tax_data'=> $Copied_shipping_tax_data, 'CopiedCommonSum'=> $CopiedCommonSum, 'CopiedPaymentData'=> $CopiedPaymentData  ) ) ?>
        </td>
        <td valign="top" >
          <div align="center" >
            <b>Thank you for your order !</b><br>
            Your order number is <b><?php echo $transaction_id ?></b><br>
            Please keep this number for your records.<br><br>
            A confirmation email will be sent shortly to your email address.<br><br>
            If you have any questions or changes your order , please call us at<br>
            1-800-223-2097.<br><br>
            <b>We look forward to your next visit! </b><br>
            <a href="http://www.facebook.com/rayoherron">Join us on Facebook</a>
          </div>
        </td>
      </tr>
      
    </table>

  <?php else: ?>
    <div style="width:400px;height:80px;border:1px solid #999;padding:50px;text-align:center;margin:60px auto 100px">
    <?php echo htmlspecialchars_decode( $PostDemoFormHTML ) ?><br>
    <a  href="<?php echo url_for('@check_out_payment?payment_method=-') ?>"><br />Return To Check Out Page</a>
    </div>
  <?php endif; ?>
