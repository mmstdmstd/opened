<?php
  $ErrorMsg= $sf_request->isMethod('post') ? strip_tags($form['email']->renderError()) : "";
  if ( empty($ErrorMsg) ) $ErrorMsg= $form->renderGlobalErrors();
?>

<form action="<?php echo url_for('@reset_password') ?>" id="form_firearm" method="POST" enctype="multipart/form-data" >
  <?php echo $form['_csrf_token']->render() ?>
<div id="policy_div">
  <p id="retrieve">Reset Password</p>
  <span id="retrieve-msg">Please enter the email address for this account and click the Reset Password button.</span>
  <span style="color:red; position:relative; left:300px;  bottom:20px; float:left;" ><?php echo strip_tags( $ErrorMsg ) ?></span>
  <div id="retrieve2-div">
    <?php echo $form['email']->render(); ?>
  
    <div id="retrieve-btn-div">
      <input type="image" src="<?php echo Util::getServerHost(sfContext::getInstance()->getConfiguration(), false) ?>images/reset-password-button.png" value="" id="retrieve-button">
    </div>
  </div>
</div>
</form>