<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ); ?>

<table border="0" style="border:0px solid green">
  <tr style="border:0">
    <td style="border:0">
      <input type="checkbox" onchange="javascript:onchangeDepartmentPurchaseAuthorized();" id="cbx_authorized" >&nbsp;I am authorized to place this order on behalf of the specified department.
    </td>
  </tr>
</table>
  <div id="submit_order_btn_div" style="display:none" >
      <img id="submit_order_btn"  src="<?php echo $HostForImage ?>images/submit-order.png" type="image" alt="Submit Order" Title="Submit Order" onclick="javascript:onSubmitOrder()" />
  </div>

  <div id="submit_order_btn_inactive_div" >
      <img id="submit_order_inactive_btn"  src="<?php echo $HostForImage ?>images/submit-order-inactive.png" type="image" alt="Submit Order" Title="Submit Order" />
  </div>
