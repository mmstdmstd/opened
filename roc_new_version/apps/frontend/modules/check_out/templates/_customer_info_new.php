<?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
$UsersCartArray= Cart::getUsersCartArray();
$UsersCartItemsSelectedCount= count($UsersCartArray);
?>

<style type="text/css">
	#checkout-customer-info {border:4px solid #666; float:left; padding-bottom:37px;  margin:0 0 0 37px; width:590px; background:#f1f2f4;}
	#checkout-customer-info h5 {font:bold 120% arial,sans-serif; margin: 1.75em 0 1.75em 0; padding:0;}
	#checkout-customer-info table {margin:0 0 0 2.65em; width:89%; vertical-align:top;}
	#checkout-customer-info table td {vertical-align:top !important;}
	#checkout-customer-info table p {margin:0; padding:0; position:relative; top:-.5em;}
	#checkout-customer-info table .error {color:red; margin:.75em 0 .4em 0;}
	#checkout-customer-info .continue-button {margin-top:.4em;}
	#checkout-customer-info .login-button {margin-top:.4em;}
	#checkout-customer-info hr {margin:6.5em 0 2em 0em;}
	#checkout-customer-info label {position:relative; top:.35em; line-height:1;}
	#checkout-customer-info #new_customer_email,
	#checkout-customer-info #new_customer_password,
	#checkout-customer-info	#new_customer_returning_email {width:14em;}
	#customer-os {background-color:#f1f2f4 !important;}
</style>

<div id="checkout-customer-info">

<?php if ( $UsersCartItemsSelectedCount == 0 ) : ?><br>
  <strong style="margin-left:2.7em;">Your shopping cart is empty</strong>
  </div>
  <?php return; ?>
<?php endif; ?>


<form action="<?php echo url_for('@check_out_customer') ?>" id="form_new_customer" method="POST" enctype="multipart/form-data" >
<?php echo $form['_csrf_token']->render()?>
<?php echo $form['account_type']->render()?>

    <span class="error" style="list-style: none;"><?php echo $form->renderGlobalErrors() ?></span>

    <p style="position:relative;top:20px;left:20px;color:#8B3F01;font-weight:bold;font-size:13px;background-color:#FFE9B0;border:1px solid #CCC;width:525px;padding:3px 10px 3px">Please note: All web orders are processed and shipped by our Danville, IL store.</p>
    <?php 
    $user_has_logged= sfContext::getInstance()->getRequest()->getCookie( 'user_has_logged' );
    //Util::deb($user_has_logged,'$user_has_logged::');
    $LoggedUserEmail= Cart::getLoggedUserEmail();
    //Util::deb( $LoggedUserEmail,'$LoggedUserEmail::');
    ?>
    <?php if ( !empty($user_has_logged) and empty($LoggedUserEmail) ) : ?>
      <p style="position:relative;top:22px;left:20px;color:#c20707;font-weight:bold;font-size:13px;background-color:#FFFFFF;border:1px solid #CCC;width:525px;padding:3px 10px 3px;font-size:arial 10px bold;">Your session has timed out. Please re-enter your customer information.</p>
    <?php endif; ?>

    <table class="Table_Editor_Conteiner" style="position:relative;margin:20px 30px" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td colspan="3">
					<h5>New Customer</h5>
				</td>
			</tr>
      <tr>
        <td class="left" width="24%">
          <label for="new_customer_email"><?php echo strip_tags( $form['email']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right">
         	<?php echo $form['email']->render(); ?>
					<input type="button" class="continue-button" id="btn_NewCustomerContinue" onclick="javascript:NewCustomerContinue()">
				</td>
				<td>
  	      <p class="error" style="font-size:12px; width:200px; position:absolute; left:126px;top:34px;">The email address is invalid
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['email']->renderError() )  :"" ) ?>
  	      </p>
					<p style="position:relative;top:-25px;width:160px;margin-right:20px">Please enter your email address (We will send your receipt and confirmation to this address)</p>
        </td>
      </tr>
			<tr>
				<td colspan="3">
					<hr style="margin:20px">
					<h5 style="margin-bottom:0">Returning Customer</h5>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="3">
					&nbsp;
				</td>
			</tr>

      <tr>
        <td class="left">
          <label for="new_customer_returning_email"><?php echo strip_tags( $form['returning_email']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="right" rowspan="1">
         	<?php echo $form['returning_email']->render(); ?>
  	      <p class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['returning_email']->renderError() )  :"" ) ?>
  	      </p>
        </td>

<!-- sz added td and in line style -->
        <td>
          <p class="error" style="font-size:12px; position:relative; bottom:100px;">Invalid email or password
          </p>
        </td>
<!-->



      </tr>

      <tr>
        <td class="left" width="24%" colspan="1">
          <label for="new_customer_password"><?php echo strip_tags( $form['password']->renderLabel() ) ?>&nbsp;:</label>
        </td>
        <td class="left">
         	<?php echo $form['password']->render(); ?>
        </td>
        <td class="left" rowspan="2">
					<p class="error">
  	        <?php echo ( $sf_request->isMethod('post') ? strip_tags( $form['password']->renderError() )  :"" ) ?>
          <a style="color:#000000;" href="">Forgot Password?</a>
  	      </p>
        </td>
      </tr>

			<tr>
				<td colspan="1">
					&nbsp;
				</td>
				<td colspan="3">
					<input type="button" class="login-button" id="btn_NewCustomerSaveAndContinue" onclick="javascript:NewCustomerSaveAndContinue()">
				</td>
			</tr>



    </table>

</form>

</div>
