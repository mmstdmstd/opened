<script type="text/javascript" language="JavaScript">
  <!--

  function SaveReviewOrder() {
    var theForm = document.getElementById("form_shipping");
    theForm.submit();
  }

  function isBillingAddressChanged() {
  	var is_billing_address= document.getElementById("shipping_is_billing_address").checked;
  	if ( is_billing_address ) {
  		document.getElementById("shipping_b_name").value= document.getElementById("shipping_name").value;
  		document.getElementById("shipping_b_address").value= document.getElementById("shipping_address").value;
  		document.getElementById("shipping_b_address_2").value= document.getElementById("shipping_address_2").value;
  		document.getElementById("shipping_b_city").value= document.getElementById("shipping_city").value;
  		document.getElementById("shipping_b_state").value= document.getElementById("shipping_state").value;
  		document.getElementById("shipping_b_zip").value= document.getElementById("shipping_zip").value;
  		document.getElementById("shipping_b_country").value= document.getElementById("shipping_country").value;
  	}
  }

//-->
</script>

<style type="text/css">
#checkout-shipping-billing-page {overflow:hidden; clear:both;}
#checkout-shipping-billing-page #shipping-billing-step {background-position:0 56px !important;}
#checkout-shipping-billing-page #shipping-billing-step a {color:#fff; background-color:#6e6e70 !important;}
</style>

<div id="checkout-shipping-billing-page" class="checkout-section">
      <?php include_partial( 'check_out/top_menu', array() ) ?>
      <?php include_partial( 'check_out/order_summary', array() ) ?>
      <?php include_partial( 'check_out/shipping_info', array( 'form'=>$form, 'MakeAutoSubmit'=>$MakeAutoSubmit ) ) ?>
</div>