<?php
// Util::deb( $price_type, ' $price_type::' );
$Item = InventoryItemPeer::getSimilarInventoryItem($sku);
//Util::deb( $Item, ' $Item::' );
if (empty($Item)) return;
//Util::deb($sku, '$sku::');
$badges_sku_list = sfConfig::get('app_application_badges_sku_list');
$is_badge_item= !empty($badges_sku_list[strtolower($sku)]);
//Util::deb($badges_sku_list, '$badges_sku_list::');
//Util::deb($is_badge_item, '$is_badge_item::');
if ($is_badge_item) {
	$GuestUserArray = Cart::getGuestUser();
	//Util::deb($GuestUserArray, '$GuestUserArray::');
	$lLoggedUser = sfGuardUserPeer::retrieveByUsername($GuestUserArray['LoggedUserEmail']);
	//Util::deb($lLoggedUser, '$lLoggedUser::');
	if (!empty($lLoggedUser)) {
		$LastUserBadgeData = UserBadgeDataPeer::getUserBadgeDataByUserId($lLoggedUser->getId());
		//Util::deb($LastUserBadgeData, '$LastUserBadgeData::');
		$UserBadgeDataArray = unserialize($LastUserBadgeData->getData());
		//Util::deb($UserBadgeDataArray, '$UserBadgeDataArray::');
	}
}


$sf_upload_dir = sfConfig::get('sf_upload_dir');
$InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');
$InventoryItems = sfConfig::get('app_application_InventoryItems');

$ProductPrice = $Item->getStdUnitPrice();
if (!empty($price_type) and $price_type == 'special') {
	$ProductPrice = $Item->getClearance_Or_SalePrice();
}
if (!empty($price_type) and $price_type == 'selected_price') {
	$ProductPrice = $selected_price;
}
if (!empty($price_type) and $price_type == 'badge_price') {
	$ProductPrice = $selected_price;
}

$Debug = false;
if ($sku == '0-8001-075') {
	//$Debug= true;
}
?>

<?php $Clearance = $Item->getClearance();
$ThumbnailImageFile = AppUtils::getThumbnailImageBySku($sku, false);
// Util::deb( $ThumbnailImageFile, ' $ThumbnailImageFile::' );
if (empty($ThumbnailImageFile)) {
	$ThumbnailImageFile = AppUtils::getResizedImageBySku($sku, false, false, true);
	// Util::deb( $ThumbnailImageFile, ' Resized $ThumbnailImageFile::' );
}
?>

<td width="68px" style="vertical-align: top;" >
	<?php $HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());

	$ThumbnailImageFullUrl = '';
	// Util::deb( $ThumbnailImageFile, ' 00$ThumbnailImageFile::' );
	if ($ThumbnailImageFile and !is_array($ThumbnailImageFile)) :
		$ThumbnailImageUrl = '/uploads' . $ThumbnailImageFile; // Util::deb( $ThumbnailImageUrl, ' $ThumbnailImageUrl::' );
		$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
		?>
		<a href="<?php echo url_for("@product_details?sku=" . urlencode($sku)) ?>">
			<?php echo image_tag($ThumbnailImageUrl, array('alt' => "", "height" => "52", "width" => "52")); ?>
		</a>
	<?php endif; ?>


	<?php if ($ThumbnailImageFile and is_array($ThumbnailImageFile)) :
		$ThumbnailImageUrl = 'uploads' . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName_encoded'];
		$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
		$ThumbnailImageFullFilePath = sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
		// Util::deb( $ThumbnailImageFullFilePath, ' $ThumbnailImageFullFilePath::' );
		if (empty($ThumbnailImageFile['ImageFileName']) or (!file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath))) {
			$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage150.png';
		}
		$ThumbnailImageFullUrl = str_replace("+", ' ', $ThumbnailImageFullUrl);
		$ImageHeight = 52;
		$ImageWidth = 52;
		if (!empty($ThumbnailImageFile['Height']) and !empty($ThumbnailImageFile['Width'])) {
			$ImageHeight = $ThumbnailImageFile['Height'];
			$ImageWidth = $ThumbnailImageFile['Width'];
		}
		?>
		<a href="<?php echo url_for("@product_details?sku=" . urlencode($sku)) ?>">
			<?php echo image_tag($ThumbnailImageFullUrl, array('alt' => "", "height" => "52" /*$ImageHeight*/, "width" => "52" /*$ImageWidth*/)); ?>
		</a>
	<?php endif; //Util::deb( $sku, ' !!!$sku::' ); ?>
</td>


<td width="40%" style="vertical-align: top;" >
<strong><?php echo htmlspecialchars_decode($Item->getTitleWithRSymbol()) ?></strong><br>
<?php echo $Item->getSku() ?>
<?php if ( $is_badge_item ) : ?>


	<?php if (!empty($UserBadgeDataArray['badge'])) : ?>
		<p>		<small>Badge</small>:&nbsp;<?php echo $UserBadgeDataArray['badge'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['quick_ship'])) : ?>
		<p>		<small>Quick Ship</small>:&nbsp;<?php echo $UserBadgeDataArray['quick_ship'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['finish'])) : ?>
		<p>		<small>Finish</small>:&nbsp;<?php echo $UserBadgeDataArray['finish'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['enamel_type'])) : ?>
		<p>		<small>Enamel Type</small>:&nbsp;<?php echo $UserBadgeDataArray['enamel_type'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['strike_solid'])) : ?>
		<p>		<small>Strike Solid</small>:&nbsp;<?php echo $UserBadgeDataArray['strike_solid'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['lettering_font'])) : ?>
		<p>		<small>Lettering Font</small>:&nbsp;<?php echo $UserBadgeDataArray['lettering_font'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['panel2'])) : ?>
		<p>		<small>Panel 2</small>:&nbsp;<?php echo $UserBadgeDataArray['panel2'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['panel3'])) : ?>
		<p>		<small>Panel 3</small>:&nbsp;<?php echo $UserBadgeDataArray['panel3'] ?>		</p>
	<?php endif; ?>

	<?php if (!empty($UserBadgeDataArray['panel5'])) : ?>
		<p>		<small>Panel 5</small>:&nbsp;<?php echo $UserBadgeDataArray['panel5'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['seal_style'])) : ?>
		<p>		<small>Seal Style</small>:&nbsp;<?php echo $UserBadgeDataArray['seal_style'] ?>		</p>
	<?php endif; ?>

	<?php if (!empty($UserBadgeDataArray['panel6'])) : ?>
		<p>		<small>Panel 6</small>:&nbsp;<?php echo $UserBadgeDataArray['panel6'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['badge_attachment'])) : ?>
		<p>		<small>Badge Attachment</small>:&nbsp;<?php echo $UserBadgeDataArray['badge_attachment'] ?>		</p>
	<?php endif; ?>

	<?php if (!empty($UserBadgeDataArray['text_separator'])) : ?>
		<p>		<small>Text Separator</small>:&nbsp;<?php echo $UserBadgeDataArray['text_separator'] ?>		</p>
	<?php endif; ?>
	<?php if (!empty($UserBadgeDataArray['selected_emboss'])) : ?>
		<p>		<small>Selected Emboss</small>:&nbsp;<?php echo $UserBadgeDataArray['selected_emboss'] ?>		</p>
	<?php endif; ?>

<?php endif; ?>
</td>


<td style="padding-top:10px; vertical-align: top; " >
	<!--span >Qty:</span-->
	<input name="<?php echo $sku ?>" id="cart_product_quantity_<?php echo $sku ?>" value="<?php echo $product_quantity ?>" size="2" maxlength="2"/>
	<input class="update-button" type="button" value=" " onclick="javascript:UpdateCartProductQuantity( '<?php echo $sku ?>' )">&nbsp;<br>
	<input style="position:relative; top:2px; left:33px;" value="" class="remove-button" onclick="javascript:DeleteCartProduct( '<?php echo $sku ?>' )">
</td>

<td style="text-align:right; padding-right:12px; vertical-align: top; ">

	<?php //if ( !$is_badge_item ) : ?>
	  <?php echo Util::getDigitMoney($ProductPrice, 'Money') ?>
	<?php //endif; ?>
</td>

<td style="text-align:right; padding-right:12px; vertical-align: top; ">
	<?php //if ( !$is_badge_item ) : ?>
  	<?php echo Util::getDigitMoney($ProductPrice * (int)$product_quantity, 'Money') ?>
	<?php //endif; ?>
</td>
