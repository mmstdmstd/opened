<?php
/**
 * check_out actions.
 *
 * @package    sf_sandbox
 * @subpackage check_out
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class check_outActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeCustomer(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
		try
		{
			$this->form = new NewCustomerForm();
			$response->setTitle("ROC Checkout");

			if ($request->isMethod('post'))
			{
        $new_customer_data= $request->getParameter('new_customer');
        $this->form->bind( $new_customer_data );
        if ( $this->form->isValid() )
        {
        	if ( $new_customer_data['account_type']== 'New User' and !empty($new_customer_data['email']) ) {
        	  Cart::setGuestUser($new_customer_data['email']);
 	          $this->redirect( '@check_out_shipping' );
        	}

        	if ( $new_customer_data['account_type']== 'Existing User' ) {
            $lsfGuardUser= sfGuardUserPeer::getByUsername( $new_customer_data['returning_email'] );
        	  Cart::setGuestUser( $new_customer_data['returning_email'] );
 	          $this->redirect( '@check_out_shipping?logged_user_id=' . ( !empty($lsfGuardUser)?$lsfGuardUser->getId():'' ) );
         	}
        }
			}
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}

  }


  public function executeShipping(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
    $con = Propel::getConnection(sfGuardGroupPeer::DATABASE_NAME,  Propel::CONNECTION_WRITE);

		try
		{
      $logged_user_id= $request->getParameter('logged_user_id');
      // Util::deb( $logged_user_id, '$logged_user_id::' );
      $lLoggedsfGuardUser= sfGuardUserPeer::retrieveByPK( $logged_user_id );
      //Util::deb( $lLoggedsfGuardUser, '$lLoggedsfGuardUser::' );
      if ( !empty($lLoggedsfGuardUser) ) {
        $this->lLastOrderOfUser= OrderedPeer::getLastOrderOfUser( $lLoggedsfGuardUser->getId() );
        //Util::deb( $this->lLastOrderOfUser, '$this->lLastOrderOfUser::' );
        if ( !empty($this->lLastOrderOfUser) ) {
          ShippingForm::$lLastOrderOfUser = $this->lLastOrderOfUser;
          $lLoggedsfGuardUser= sfGuardUserPeer::retrieveByPK( $this->lLastOrderOfUser->getUserId() );
          if ( !empty($lLoggedsfGuardUser) ) {
      	    Cart::setGuestUser( $lLoggedsfGuardUser->getUsername() );
          }
          //Util::deb( $lLoggedsfGuardUser, '$lLoggedsfGuardUser::' );
        }
      }

			$this->form = new ShippingForm();
			$response->setTitle("ROC Checkout");

			if ( $request->isMethod('post') )
			{
        $shipping_data= $request->getParameter('shipping');
        $this->form->bind( $shipping_data );
        if ( $this->form->isValid() )
        {
       	  Cart::setShippingInfo( $shipping_data );

          if ( !empty($shipping_data['password']) and !empty($shipping_data['password_2']) and !empty($shipping_data['password']) == !empty($shipping_data['password_2']) ) {

            $lNewSfUser= new sfGuardUser();
            $lNewSfUser->setName( $shipping_data['name'] );

        	  $GuestUserArray= Cart::getGuestUser();
            //Util::deb( $GuestUserArray, '$GuestUserArray::' );
            $lNewSfUser->setUsername( $GuestUserArray['LoggedUserEmail'] );
            $lNewSfUser->setIsActive( true );
            $lNewSfUser->setSalt( '' );
            $lNewSfUser->setPassword( $shipping_data['password'] );
            $lNewSfUser->setAlgorithm( 'md5' );
            $lNewSfUser->setIsSuperAdmin(false);
            $con->beginTransaction();
            $lNewSfUser->save();

            $lsfGuardUserGroup= new sfGuardUserGroup();
            $lsfGuardUserGroup->setUserId( $lNewSfUser->getId() );
            //Util::deb( sfGuardGroupPeer::getGroupIdByName('public'), 'sfGuardGroupPeer::getGroupIdByName(public)::' );
            $lsfGuardUserGroup->setGroupId( sfGuardGroupPeer::getGroupIdByName('public') );
            $lsfGuardUserGroup->save();
            $con->commit();
            //Util::deb( $lsfGuardUserGroup, '$lsfGuardUserGroup::' );

            $lBufUser= $lNewSfUser;
            //Util::deb( $lBufUser, '$lBufUser::' );
            //die("INSIDE");
            $this->getUser()->addCredential('public');
            $this->getUser()->signIn($lBufUser);


          }

          //die("TRE");
          $this->redirect( '@check_out_review_order' );
        }
			}
		}
		catch (Exception $lException)
		{
      $con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }


  public function executeReview_order(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
		try
		{
			$this->form = new ReviewOrderForm();
			$response->setTitle("ROC Checkout");

      $mode= $request->getParameter('mode');
      $sku= $request->getParameter('sku');
      $product_quantity= $request->getParameter('product_quantity');
      if ( $mode == 'update' ) {
        $Res= Cart::UpdateUsersCart( $sku, $product_quantity, '' );
      }

      if ( $mode == 'delete' ) {
        $Res= Cart::UsersCartDeleteItem( $sku );
      }

			if ( $request->isMethod('post') )
			{
        $review_order= $request->getParameter('review_order');
        $this->form->bind( $review_order );
        if ( $this->form->isValid() )
        {
       	  Cart::setReviewOrder( $review_order );
          $this->redirect( '@check_out_payment' );
        }
			}

		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }



  public function executePayment(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
		try
		{
      $payment_method= $request->getParameter('payment_method');
      if ( $payment_method == 'PP' ) {
        $this->redirect( '@get_PayPal_HTML' );
      }

      $this->form = new PaymentMethodCreditCardForm();
			$response->setTitle("ROC Checkout");

			if ( $request->isMethod('post') )
			{
        $payment= $request->getParameter('payment_type');
        $this->form->bind( $payment );
        // Util::deb( $this->form->isValid(), '$this->form->isValid()::' );
        if ( $this->form->isValid() )
        {
          Util::deb( $payment, '$payment::' );
       	  Cart::setPaymentData( $payment );
          $this->redirect( '@check_out_make_payment' );
        }
			}
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }


  public function executeMake_payment(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
		try
		{
      $con = Propel::getConnection( OrderedPeer::DATABASE_NAME,  Propel::CONNECTION_WRITE );
      require_once '../lib/anet_php_sdk/AuthorizeNet.php'; // The SDK
      $url = Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $this->response_code= $request->getParameter('response_code');
      $this->transaction_id= $request->getParameter('transaction_id');
      $this->IsOrderSent= false;
      if ( $this->response_code== '1' and !empty($this->transaction_id) ) {
        /* echo ' $_POST::<pre> '. print_r( $_POST, true ) .'</pre><br>';
        echo ' $_GET::<pre> '. print_r( $_GET, true ) .'</pre><br>';
        echo ' $_REQUEST::<pre> '. print_r( $_REQUEST, true ) .'</pre><br>'; */
        // die("GOOD");
        $this->CheckoutArray= Cart::getShippingInfo();
        $lNewOrder= new Ordered();

     	  $GuestUserArray= Cart::getGuestUser();
        $LoggedUserEmail= $GuestUserArray['LoggedUserEmail'];
        $lLoggedUser= sfGuardUserPeer::retrieveByUsername($LoggedUserEmail);
        if ( !empty($lLoggedUser) ) {
            $lNewOrder->setUserId( $lLoggedUser->getId() );
        } else {
         $lNewOrder->setUserId( 1 );   // Admin
        }
        $lNewOrder->setStatus( 'O' ); //

        $lNewOrder->setBCountry(  $this->CheckoutArray['b_country']  );
        $lNewOrder->setBStreet(  $this->CheckoutArray['b_address']  );
        $lNewOrder->setBStreet2(  $this->CheckoutArray['b_address_2']  );
        $lNewOrder->setBCity(  $this->CheckoutArray['b_city']  );
        $lNewOrder->setBState(  $this->CheckoutArray['b_state']  );
        $lNewOrder->setBZip(  $this->CheckoutArray['b_zip']  );

        $lNewOrder->setSCountry(  $this->CheckoutArray['country']  );
        $lNewOrder->setSStreet(  $this->CheckoutArray['address']  );
        $lNewOrder->setSStreet2(  $this->CheckoutArray['address_2']  );
        $lNewOrder->setSCity(  $this->CheckoutArray['city']  );
        $lNewOrder->setSState(  $this->CheckoutArray['state']  );
        $lNewOrder->setSZip(  $this->CheckoutArray['zip']  );

        $this->ReviewOrderArray= Cart::getReviewOrder();
        $lNewOrder->setNotes(  $this->ReviewOrderArray['notes']  );
        $lNewOrder->setShipping(  $this->ReviewOrderArray['shipping_service']  );

        $CommonSum= Cart::getCommonSum();
        // $this->shipping_tax_data_array = Cart::getTaxShipData();
        // echo ' $this->ReviewOrderArray:: '. print_r( $this->ReviewOrderArray, true ) .'<br>';
        //die("GGG");
        $lNewOrder->setTotalPrice(  $CommonSum  );

       //     $shipping = $this->getShippingRates($dst_zip);
        $shipping_array = ShippingTaxes::getShippingRates( $this->CheckoutArray['b_zip'], false );
        // echo ' $shipping_array:: '. print_r( $shipping_array, true ) .'<br>';

        $tax_sum= ShippingTaxes::Calculate_tax( $this->CheckoutArray['b_zip'], $this->CheckoutArray['b_state'], false );
        // echo ' $tax_sum:: '. print_r( $tax_sum, true ) .'<br>';


        $lNewOrder->setTax(  $tax_sum /*$this->shipping_tax_data_array['est_tax']*/  );
        // echo '-1:: $lNewOrder->getTax():: '. print_r( $lNewOrder->getTax(), true ) .'<br>';

        if ( !empty( $shipping_array[ $this->ReviewOrderArray['shipping_service'] ] ) ) {
          $lNewOrder->setShippingCost( $shipping_array[ $this->ReviewOrderArray['shipping_service'] ] /* $this->shipping_tax_data_array['est_shipping'] */ );
          // echo '-2:: $lNewOrder->getShippingCost():: '. print_r( $lNewOrder->getShippingCost(), true ) .'<br>';
        }

        $lNewOrder->setDiscount(  0  ); // TODO
        $lNewOrder->setTransactionId(  $this->transaction_id  );

        $this->PaymentDataArray= Cart::getPaymentData();
        //echo ' $this->PaymentDataArray::<pre> '. print_r( $this->PaymentDataArray, true ) .'</pre><br>';
        //die("-1");
        if ( empty($this->PaymentDataArray['payment_method']) ) {
          Cart::setPaymentData(   array( 'payment_method'=>'CC', 'card_number'=> '', 'expiration_date_year'=> '', 'expiration_date_month'=> '', 'card_security_code'=> '' )   );
          //$lNewOrder->setPaymentMethod( 'CC' );
        }

        $this->PaymentDataArray= Cart::getPaymentData();
        $lNewOrder->setPaymentMethod(  $this->PaymentDataArray['payment_method']  );



        $con->beginTransaction();
        $lNewOrder->save();
        $UsersCartArray= Cart::getUsersCartArray();
        $UsersCartItemsSelectedCount= count($UsersCartArray);

        for($Row=0; $Row< $UsersCartItemsSelectedCount; $Row++) {
           //echo ' $Row:: '. $Row ;
          if ( !empty($UsersCartArray[$Row]) ) {
 	          $sku = $UsersCartArray[$Row]['sku'];
            $product_quantity= $UsersCartArray[$Row]['product_quantity'];
            $price_type= $UsersCartArray[$Row]['price_type'];
            $lInventoryItem= InventoryItemPeer::getSimilarInventoryItem( $sku );

            if ( !empty($lInventoryItem) ) {
              $lNewOrderedItem= new OrderedItem();
              $lNewOrderedItem->setOrderId( $lNewOrder->getId() );
              $lNewOrderedItem->setSku( $sku );
              $lNewOrderedItem->setQty( $product_quantity );

              if ( empty($price_type) ) {
                $lNewOrderedItem->setUnitPrice( $lInventoryItem->getStdUnitPrice() );
              }
              if ( !empty($price_type) and $price_type == 'special' ) {
                $lNewOrderedItem->setUnitPrice( $lInventoryItem->getClearance_Or_SalePrice() );
              }
              $lNewOrderedItem->save();
              // echo '$lNewOrderedItem->getId() :: '. $lNewOrderedItem->getId() .'<br>' ;
            }
          }
        }
        $con->commit();

        $this->CopiedPaymentData= Cart::getPaymentData();

        $this->getUser()->setAttribute(  'CopiedCheckoutCopiedArray', $this->CheckoutArray  );
        $this->getUser()->setAttribute(  'CopiedhasLoggedUser', Cart::hasLoggedUser()  );
        $this->getUser()->setAttribute(  'CopiedLoggedUserEmail', Cart::getLoggedUserEmail()  );
        $this->getUser()->setAttribute(  'CopiedShippingAddress', Cart::hasShippingAddress()  );
        $this->getUser()->setAttribute(  'CopiedhasBillingAddress', Cart::hasBillingAddress()  );
        $this->getUser()->setAttribute(  'CopiedItemsCount', Cart::getItemsCount()  );
        $this->getUser()->setAttribute(  'Copied_shipping_tax_data', Cart::getTaxShipData()  );
        $this->getUser()->setAttribute(  'CopiedCommonSum', Cart::getCommonSum()  );
        $this->getUser()->setAttribute(  'CopiedPaymentData', Cart::getPaymentData()  );


        $Res= Cart::SendOrderEmailToAdmin( $lNewOrder->getId() );
        Cart::Clear();
        $this->IsOrderSent= false;
        $this->redirect( '@check_out_make_payment_report?transaction_id='.$this->transaction_id . '&new_order_id=' . $lNewOrder->getId() );

        return;
      } // if ( $this->response_code== '1' and !empty($this->transaction_id) ) {

      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum= Cart::getCommonSum();

      $amount = $CommonSum;
      $this->PostDemoFormHTML= AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting );

		}
		catch (Exception $lException)
		{
      $con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }



  public function executeGet_AuthorizeNet_DPM_HTML(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
		try
		{
      $url = Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum= Cart::getCommonSum();
      $amount = $CommonSum;
      echo AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting );
			return sfView::NONE;
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }



  public function executeGet_PayPal_HTML(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );

		try
		{
      $is_debug= false;
      $url = Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      if ( $is_debug ) Util::deb( $shipping_tax_data, '$shipping_tax_data::' );
      $CommonSum= Cart::getCommonSum();
      $ItemsCount= Cart::getItemsCount();
      $UsersCartArray= Cart::getUsersCartArray();

      $amount = $CommonSum;
      $this->ReviewOrderArray= Cart::getReviewOrder();
      if ( $is_debug ) echo ' $this->ReviewOrderArray:: '. print_r( $this->ReviewOrderArray, true ) .'<br>';
      $shipping_array = ShippingTaxes::getShippingRates( $this->CheckoutArray['b_zip'], false );
      // echo ' $shipping_array:: '. print_r( $shipping_array, true ) .'<br>';

      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79

//        $lNewOrder->setShippingCost( $shipping_array[ $this->ReviewOrderArray['shipping_service'] ] /* $this->shipping_tax_data_array['est_shipping'] */ );
      $shipping_cost= $shipping_tax_data['est_shipping'];
      if ( empty($shipping_cost) or intval($shipping_cost)<=0 ) $shipping_cost = 10;
      $tax_cost= $shipping_tax_data['est_tax'];

      if ( $is_debug ) {
        echo '-10:: $UsersCartArray:: <pre>'. print_r( $UsersCartArray, true ) .'</pre><br>';
        echo '-1:: $amount:: <pre>'. print_r( $amount, true ) .'</pre><br>';
        echo '-2:: $shipping_cost:: <pre>'. print_r( $shipping_cost, true ) .'</pre><br>';
        echo '-3:: $tax_cost:: <pre>'. print_r( $tax_cost, true ) .'</pre><br>';
        echo '-4:: $this->ReviewOrderArray:: <pre>'. print_r( $this->ReviewOrderArray, true ) .'</pre><br>';
      }

      include_once('../lib/paypal.php');

      $requestParams = array(
        'RETURNURL' => Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/paypal_payment_success",   //   'http://www.yourdomain.com/payment/success',
        'CANCELURL' => Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/paypal_payment_cancelled"  // 'http://www.yourdomain.com/payment/cancelled'
      );

      $orderParams = array(
        'PAYMENTREQUEST_0_AMT' => $amount  + $shipping_cost /*+ $tax_cost*/,     // Итоговая сумма для перевода. Указывать нужно два десятичных числа, разделенных точкой (.). По желанию можно использовать запятую (,) для разделения тысяч;
        'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping_cost,   // Стоимость доставки заказа
        'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD', // Этот параметр определяет валюту, в которой будут проводиться все операции. Указывать нужно трехзначный код. По-умолчанию установлено значение USD;
        'PAYMENTREQUEST_0_ITEMAMT' => $amount, // Итоговая стоимость, не включающая в себя комиссию, таксы, стоимость доставки и прочие доп. расходы. Если таковых не имеется, значение этого параметра должно соответствовать значению PAYMENTREQUEST_0_AMT.
      );

      $item = array(); $Index= 0; $Summa= 0;
      foreach( $UsersCartArray as $NextProduct ) {
        /*  [sku] => 06701
            [product_quantity] => 1
            [price_type] => special */
        $lProduct= InventoryItemPeer::getSimilarInventoryItem( $NextProduct['sku'] );
        $item[ 'L_PAYMENTREQUEST_0_NAME'.$Index ]= $lProduct->getTitle();
        $item[ 'L_PAYMENTREQUEST_0_DESC'.$Index ]= $lProduct->getDescriptionShort();

        if ( $NextProduct['price_type'] == 'special' ) {
          $item[ 'L_PAYMENTREQUEST_0_AMT'.$Index ]= $lProduct->getClearance_Or_SalePrice();
        }
        else {
          $item[ 'L_PAYMENTREQUEST_0_AMT'.$Index ]= $lProduct->getStdUnitPrice();
        }

        $item[ 'L_PAYMENTREQUEST_0_QTY'.$Index ]= $NextProduct['product_quantity'];
        $Summa+=  $item[ 'L_PAYMENTREQUEST_0_AMT'.$Index ] * $item[ 'L_PAYMENTREQUEST_0_QTY'.$Index ];

        $Index++;
      }
      if ( $is_debug ) echo '-42:: $Summa:: <pre>'. print_r( $Summa, true ) .'</pre><br>';

      /*
      $item = array(
        'L_PAYMENTREQUEST_0_NAME0' => 'Sales at oherron.com site.',
        'L_PAYMENTREQUEST_0_DESC0' => $ItemsCount . ' producta sold at oherron.com site.',
        'L_PAYMENTREQUEST_0_AMT0' => $amount,
        'L_PAYMENTREQUEST_0_QTY0' => 1, // $ItemsCount,

        'L_PAYMENTREQUEST_1_NAME1' => 'Sales at oherron.com site.',
        'L_PAYMENTREQUEST_1_DESC1' => $ItemsCount . ' producta sold at oherron.com site.',
        'L_PAYMENTREQUEST_1_AMT1' => $amount,
        'L_sPAYMENTREQUEST_1_QTY1' => 1 //$ItemsCount
      ); */

      $Testpaypal= new Testpaypal();
      $paypal_response= $Testpaypal->request( "SetExpressCheckout",$requestParams + $orderParams + $item, $is_debug );
      if ( $is_debug ) echo '-5:: $paypal_response:: <pre>'. print_r( $paypal_response, true ) .'</pre><br>';
      if(is_array($paypal_response) && $paypal_response['ACK'] == 'Success') { // Запрос был успешно принят
        //$Testpaypal->success();
        $token = $paypal_response['TOKEN'];
        if ( $is_debug ) echo '-6:: $token INSIDE:: <pre>'. print_r( $token, true ) .'</pre><br>';

        //$checkoutDetails = $Testpaypal->request('GetExpressCheckoutDetails', array('TOKEN' => $token) );
        //if ( $is_debug ) echo '-7:: $checkoutDetails:: <pre>'. print_r( $checkoutDetails, true ) .'</pre><br>';
        // $LocationUrl= 'Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token);
        $LocationUrl= 'Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token);

        if ( $is_debug ) {
          echo '-8:: $LocationUrl:: <pre>'. print_r( $LocationUrl, true ) .'</pre><br>';
          die("is_debug");
        }
        header( $LocationUrl );
        return;
      }

			return sfView::NONE;
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }



  public function executePaypal_payment_success(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
		try
		{
      die("paypal_payment_success");
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }

  public function executePaypal_payment_cancelled(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
		try
		{
      die("paypal_payment_cancelled");
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }



  public function executeGet_Google_Checkout_HTML(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
		try
		{
      $url = Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum= Cart::getCommonSum();
      $amount = $CommonSum;
      echo 'Google_Checkout';  //AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting );
			return sfView::NONE;
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }

  public function executeGet_Department_Purchase_HTML(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
		try
		{
      $url = Util::getServerHost( sfContext::getInstance()->getConfiguration() ) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
      $transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
      $md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum= Cart::getCommonSum();
      $amount = $CommonSum;
      echo 'Department_Purchase';  //AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting );
			return sfView::NONE;
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }

  public function executeMake_payment_report(sfWebRequest $request)
  {
    $response = $this->context->getResponse();
    $response->addJavascript( '/js/jquery/jquery.js', 'last' );
    $response->addJavascript( '/js/jquery/jquery-ui.js', 'last' );
    $this->transaction_id= $request->getParameter('transaction_id');
    $this->new_order_id= $request->getParameter('new_order_id');
    $this->lNewOrder= OrderedPeer::retrieveByPK( $this->new_order_id );
		try
		{
      $this->CopiedCheckoutCopiedArray = sfContext::getInstance()->getUser()->getAttribute( 'CopiedCheckoutCopiedArray' );
      $this->CopiedhasLoggedUser = sfContext::getInstance()->getUser()->getAttribute( 'CopiedhasLoggedUser' );
      $this->CopiedLoggedUserEmail = sfContext::getInstance()->getUser()->getAttribute( 'CopiedLoggedUserEmail' );
      $this->CopiedShippingAddress = sfContext::getInstance()->getUser()->getAttribute( 'CopiedShippingAddress' );
      $this->CopiedhasBillingAddress = sfContext::getInstance()->getUser()->getAttribute( 'CopiedhasBillingAddress' );
      $this->CopiedItemsCount = sfContext::getInstance()->getUser()->getAttribute( 'CopiedItemsCount' );
      $this->Copied_shipping_tax_data = sfContext::getInstance()->getUser()->getAttribute( 'Copied_shipping_tax_data' );
      $this->CopiedCommonSum = sfContext::getInstance()->getUser()->getAttribute( 'CopiedCommonSum' );
      $this->CopiedPaymentData = sfContext::getInstance()->getUser()->getAttribute( 'CopiedPaymentData' );

		}
		catch (Exception $lException)
		{
      $con->rollBack();
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
  }

}


/*



Ошибка при paypal-запросе

Всем привет,
кто работал с paypal-ом ?
Я делаю запрос paypal-а с "SetExpressCheckout" method, используя статью
http://habrahabr.ru/post/128198/.

Я посылаю следующие параметры:
Array
(
    [RETURNURL] => http://site/paypal_payment_success
    [CANCELURL] => http://site/paypal_payment_cancelled
    [PAYMENTREQUEST_0_AMT] => 45
    [PAYMENTREQUEST_0_SHIPPINGAMT] => 10
    [PAYMENTREQUEST_0_CURRENCYCODE] => USD
    [PAYMENTREQUEST_0_ITEMAMT] => 35.00
    [L_PAYMENTREQUEST_0_NAME0] => iPhone
    [L_PAYMENTREQUEST_0_DESC0] => White iPhone, 16GB
    [L_PAYMENTREQUEST_0_AMT0] => 35.00
    [L_PAYMENTREQUEST_0_QTY0] => 1
)

Со следующими настройками curl-а:
        $curlOptions = array (
            CURLOPT_URL => $this -> _endPoint,
            CURLOPT_VERBOSE => 1,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            //CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $request
        );

И я получаю следующий response:
Array
(
    [TOKEN] => EC-19B45407EE698120B
    [TIMESTAMP] => 2012-09-26T06:56:01Z
    [CORRELATIONID] => a92fb02c16e60
    [ACK] => Success
    [VERSION] => 74.0
    [BUILD] => 3719653
)
Но делаю редирект по urlу вида:
https://www.paypal.com/webscr?cmd=_express-checkout&token=EC-19B45407EE6...

Я получаю в браузере следующую ошибку:
This transaction is invalid. Please return to the recipient's website to complete your transaction using their regular checkout flow.
Return to merchant
At this time, we are unable to process your request. Please return to  and try another option.

Никто не знает, что это за ошибка и какой " another option" имеется в виду?

Если неправильно заполнен один из параметров     PAYMENTREQUEST_0_SHIPPINGAMT,  [L_PAYMENTREQUEST_0_NAME0, L_PAYMENTREQUEST_0_DESC0,  L_PAYMENTREQUEST_0_AMT0,  L_PAYMENTREQUEST_0_QTY0,
то в возврате [ACK] равен FAILURE и нормальное описание ошибки... Также выводится нормально описание ошибки если задать неправильные параметры логина.
несколько дней назад спросил на форуме paypal-а ( https://www.x.com/developers/paypal/forums/account-authentication )- молчат.

Может ли причиной быть то что запрос идет с украниского IP?
 */