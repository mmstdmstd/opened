<?php


/**
 * check_out actions.
 *
 * @package    sf_sandbox
 * @subpackage check_out
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class check_outActions extends sfActions {

  /* Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeCustomer(sfWebRequest $request) {
    $response = $this->context->getResponse();

    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');

    $response->addStylesheet('thickbox.css');
    $response->addJavascript('/js/jquery/thickbox.js', 'last');

    try {
      $this->form = new NewCustomerForm();
      $response->setTitle("ROC Checkout");

      if ($request->isMethod('post')) {
        $new_customer_data = $request->getParameter('new_customer');
        $this->form->bind($new_customer_data);
        if ($this->form->isValid()) {
          if ($new_customer_data['account_type'] == 'New User' and !empty($new_customer_data['email'])) {
            Cart::setGuestUser($new_customer_data['email']);
            // time()+60*60*24*30 will set the cookie to expire in 30 days
            $Hours= time()+60*60*12*1;// will set the cookie to expire in 1 ( 12 hours ) day
            $response->setCookie( 'user_has_logged', '1', $Hours, '/' );
            $this->redirect('@check_out_shipping');
          }

	  //Util::deb($new_customer_data, '$new_customer_data::');
          if ($new_customer_data['account_type'] == 'Existing User' and !empty($new_customer_data['returning_email'])) {
            $lsfGuardUser = sfGuardUserPeer::getByUsername($new_customer_data['returning_email']);
	  //Util::deb($lsfGuardUser, '$lsfGuardUser::');
	  //die("123::");
            Cart::setGuestUser($new_customer_data['returning_email']);
            $Hours= time()+60*60*12*1;// will set the cookie to expire in 1 ( 12 hours ) day
            //Util::deb( $Hours, '$Hours::' );
            //Util::deb( strftime('%Y-%m-%d %H:%M:%S',$Hours), '$Hours::' );
            $response->setCookie( 'user_has_logged', '1', $Hours, '/' );
            $this->redirect('@check_out_shipping?logged_user_id=' . ( ( !empty($lsfGuardUser) and !empty($new_customer_data['password']) ) ? $lsfGuardUser->getId() : '' ));
          }
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeShipping(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    $con = Propel::getConnection(sfGuardGroupPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
    $this->MakeAutoSubmit= false;
    try {
      $logged_user_id = $request->getParameter('logged_user_id') ;
      $is_change = $request->getParameter('is_change');
      //     Util::deb( $is_change, '$is_change::' );
      $lUserShippingInfo = UserShippingInfoPeer::getUserShippingInfoByUserId( $logged_user_id );
      if (!empty($lUserShippingInfo)) {
        ShippingForm::$lUserShippingInfo = $lUserShippingInfo;
        if ($is_change!= 1) {
          //           Util::deb( 'MAKE REDIRECT::' );
          $this->MakeAutoSubmit= true;
        }

      }

      /*
      $lLoggedsfGuardUser = sfGuardUserPeer::retrieveByPK($logged_user_id);
      // Util::deb( $lLoggedsfGuardUser, '$lLoggedsfGuardUser::' );
      if (!empty($lLoggedsfGuardUser)) {
      $this->lLastOrderOfUser = OrderedPeer::getLastOrderOfUser($lLoggedsfGuardUser->getId());
      // Util::deb( $this->lLastOrderOfUser, '$this->lLastOrderOfUser::' );
      if (!empty($this->lLastOrderOfUser)) {
      ShippingForm::$lLastOrderOfUser = $this->lLastOrderOfUser;

      $lLoggedsfGuardUser = sfGuardUserPeer::retrieveByPK($this->lLastOrderOfUser->getUserId());

      Util::deb( $lLoggedsfGuardUser, '$lLoggedsfGuardUser::' );

      if (!empty($lLoggedsfGuardUser)) {
      Cart::setGuestUser($lLoggedsfGuardUser->getUsername());
      if ($is_change!= 1) {
      // Util::deb( 'MAKE REDIRECT::' );
      $this->MakeAutoSubmit= true;
      }
      }
      //Util::deb( $lLoggedsfGuardUser, '$lLoggedsfGuardUser::' );
      }
      } */

      $this->form = new ShippingForm();
      $response->setTitle("ROC Checkout");

      if ($request->isMethod('post')) {
        $shipping_data = $request->getParameter('shipping');
        $this->form->bind($shipping_data);
        if ($this->form->isValid()) {
          Cart::setShippingInfo($shipping_data);
	  // Util::deb($shipping_data,'$shipping_data::');
	  $GuestUserArray = Cart::getGuestUser();
          // Util::deb( $GuestUserArray, '$GuestUserArray::' );
          $lLoggedUser = sfGuardUserPeer::retrieveByUsername($GuestUserArray['LoggedUserEmail']);
          // Util::deb( $lLoggedUser, '$lLoggedUser::' );
          if ( !empty($shipping_data['password']) and !empty($shipping_data['password_2']) and !empty($shipping_data['password']) == !empty($shipping_data['password_2']) and !empty($lLoggedUser) ) {

            $lLoggedUser->setSalt('');
            $lLoggedUser->setPassword($shipping_data['password']);
            $lLoggedUser->setAlgorithm('md5');
            $lLoggedUser->save();
	    //die("JJJ");

            /*$lNewSfUser = new sfGuardUser();
            $lNewSfUser->setName($shipping_data['name']);

            $GuestUserArray = Cart::getGuestUser();
            //Util::deb( $GuestUserArray, '$GuestUserArray::' );
            $lNewSfUser->setUsername($GuestUserArray['LoggedUserEmail']);
            $lNewSfUser->setIsActive(true);
            $lNewSfUser->setSalt('');
            $lNewSfUser->setPassword($shipping_data['password']);
            $lNewSfUser->setAlgorithm('md5');
            $lNewSfUser->setIsSuperAdmin(false);
            $con->beginTransaction();
            $lNewSfUser->save();

            $lsfGuardUserGroup = new sfGuardUserGroup();
            $lsfGuardUserGroup->setUserId($lNewSfUser->getId());
            //Util::deb( sfGuardGroupPeer::getGroupIdByName('public'), 'sfGuardGroupPeer::getGroupIdByName(public)::' );
            $lsfGuardUserGroup->setGroupId(sfGuardGroupPeer::getGroupIdByName('public'));
            $lsfGuardUserGroup->save();
            $con->commit();

            $lBufUser = $lNewSfUser;
            $this->getUser()->addCredential('public');
            $this->getUser()->signIn($lBufUser); */
          }  

          $this->redirect('@check_out_review_order');
        }
      }
    } catch (Exception $lException) {
      $con->rollBack();
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeReview_order(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      $response->setTitle("ROC Checkout");

      $mode = $request->getParameter('mode');
      $sku = $request->getParameter('sku');
      $product_quantity = $request->getParameter('product_quantity');
      if ($mode == 'update') {
        $Res = Cart::UpdateUsersCart($sku, $product_quantity, '');
      }

      if ($mode == 'delete') {
        $Res = Cart::UsersCartDeleteItem($sku);
      }

      $this->form = new ReviewOrderForm();
      if ($request->isMethod('post')) {
        $review_order = $request->getParameter('review_order');
        $this->form->bind($review_order);
        if ($this->form->isValid()) {
          Cart::setReviewOrder( $review_order, true );
          // echo ' $review_order:: '. print_r( $review_order, true ) .'<br>';

          $shipping_tax_data = Cart::getTaxShipData();
          // Util::deb( $shipping_tax_data, '$shipping_tax_data::' );
          //echo ' $shipping_tax_data:: '. print_r( $shipping_tax_data, true ) .'<br>';
          if ( empty($shipping_tax_data) ) {
            $this->CheckoutArray = Cart::getShippingInfo();
            // echo ' $this->CheckoutArray:: '. print_r( $this->CheckoutArray, true ) .'<br>';

            $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);
            // echo ' $shipping_array:: '. print_r( $shipping_array, true ) .'<br>';
            // $shipping_array:: Array ( [UPS-Ground] => 9.49 [UPS-3Day] => 14.54 [UPS-2Day] => 19.38 [UPS-NextDay] => 50.56 )
            //
            //$tax_sum = ShippingTaxes::Calculate_tax($this->CheckoutArray['b_zip'], $this->CheckoutArray['b_state'], false);
            $tax_percent= Cart::getTaxPercentByState( AppUtils::getStateNameByAbbr($this->CheckoutArray['state']) );
            // echo ' $tax_percent:: '. print_r( $tax_percent, true ) .'<br>';

            $common_sum = Cart::getCommonSum(false);
            // echo ' $common_sum:: '. print_r( $common_sum, true ) .'<br>';


            $tax_sum = round( ( $common_sum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
            // echo ' $tax_sum:: '. print_r( $tax_sum, true ) .'<br>';

            $SelectedShippingValue= '';
            if ( !empty(  $shipping_array[ $review_order['shipping_service'] ]  ) ) {
              $SelectedShippingValue= $shipping_array[ $review_order['shipping_service'] ];
            }
            // echo ' $SelectedShippingValue:: '. print_r( $SelectedShippingValue, true ) .'<br>';


            $out_str = $this->CheckoutArray['zip'] . ',' . ',' . $shipping_array['UPS-Ground'] . ',' . $shipping_array['UPS-3Day'] . ',' .
            $shipping_array['UPS-2Day'] . ',' . $shipping_array['UPS-NextDay'] . ',' . $tax_sum . ',' . $SelectedShippingValue . ',' . ( $common_sum + $SelectedShippingValue + $tax_sum );
            // echo ' $out_str:: '. print_r( $out_str, true ) .'<br>';

            Cart::setTaxShipData($out_str, 'all');
            $shipping_tax_data = Cart::getTaxShipData();
            // echo '+++ $shipping_tax_data:: '. print_r( $shipping_tax_data, true ) .'<br>';

          }
          $time= time();
          $NewOrdergetId = $this->MakeDataSaving("", $time, true );
          // Util::deb( $NewOrdergetId, '$NewOrdergetId::' );
          $lNewOrdergetId= OrderedPeer::retrieveByPK($NewOrdergetId);
          // Util::deb( $lNewOrdergetId, '$lNewOrdergetId::' );
          sfContext::getInstance()->getUser()->setAttribute( "last_order_id", $NewOrdergetId );
          // Util::deb( sfContext::getInstance()->getUser()->getAttribute( "last_order_id" ), 'sfContext::getInstance()->getUser()->getAttribute( last_order_id )::' );

          //$this->redirect('@check_out_payment');
          $this->redirect('@check_out_payment?payment_method=-');
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executePayment(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      $this->form = new PaymentMethodCreditCardForm();
      $response->setTitle("ROC Checkout");
      $payment_method = $request->getParameter('payment_method', 'DP');
      // Util::deb( $payment_method, '$payment_method::' );
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      // Util::deb( $last_order_id, '$last_order_id::' );
 //echo ' $payment_method:: '. print_r( $payment_method, true ) .'<br>';
 //die("-1");
      if ($payment_method == 'PP') {
        $this->redirect('@get_PayPal_HTML');
      } 


      if ($payment_method == 'GC') {
        /*$PaymentHTML= Cart::PrepareAndRedirectGoogleChechout();
        Util::deb( $PaymentHTML, '$PaymentHTML::' );
        echo $PaymentHTML; */
        //return sfView::NONE;
        //return;
        //$time= time();
        /* $NewOrdergetId = $this->MakeDataSaving("GC", $TransactionId, true );
        $lNewOrdergetId= OrderedPeer::retrieveByPK($NewOrdergetId);
        Util::deb( $lNewOrdergetId, '$lNewOrdergetId::' );
        $this->redirect('@check_out_make_payment_report?transaction_id=' . $lNewOrdergetId->getTransactionId() . '&new_order_id=' . $NewOrdergetId);  */

        //$this->redirect('@get_GoogleCheckout_HTML');
      }
      if ($payment_method == 'DP') {
        $time= time();
        // $NewOrdergetId = $this->MakeDataSaving("DP", $time, true );
        //$lNewOrdergetId= OrderedPeer::retrieveByPK($NewOrdergetId);
        $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
        $this->UpdateOrderStatus($last_order_id, "DP", "WO_" . $last_order_id );
        $this->redirect('@check_out_make_payment_report?transaction_id=' . "WO_" . $last_order_id . '&new_order_id=' . $last_order_id );
      }

      if ($request->isMethod('post')) {
        $payment = $request->getParameter('payment_type');
        $this->form->bind($payment);
        if ($this->form->isValid()) {
          Cart::setPaymentData($payment);
          $this->redirect('@check_out_make_payment');
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  // $this->UpdateOrderStatus($last_order_id, "DP", $time, true );
  private function UpdateOrderStatus($last_order_id, $payment_method, $transaction_id ) {
    $lOrder= OrderedPeer::retrieveByPK( $last_order_id );
    if ( !empty($lOrder) ) {
      $lOrder->setPaymentMethod($payment_method);
      $lOrder->setStatus("N");
      $lOrder->setTransactionId($transaction_id);
      if ($payment_method == 'DP') {
        $lOrder->setTax(0);
      }
      $lOrder->save();

      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];
      $lLoggedUser = sfGuardUserPeer::retrieveByUsername($LoggedUserEmail);
      if (empty($lLoggedUser) and $payment_method != 'CC') {
        $this->redirect('@check_out_customer');
      }

      $Res = Cart::SendOrderEmailToAdmin( $lOrder, $lLoggedUser, /*$lNewOrder->getId(),*/ true);
      Cart::Clear();
      return true;
    }
    return false;
  }

  public function executeMake_payment(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      // require_once 'lib/anet_php_sdk/AuthorizeNet.php'; // The SDK
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $this->response_code = $request->getParameter('response_code');
      $this->transaction_id = $request->getParameter('transaction_id');
      // $this->IsOrderSent= false;
      if ($this->response_code == '1' and !empty($this->transaction_id)) {
        //$NewOrdergetId = $this->MakeDataSaving("CC", $this->transaction_id, false );
        $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
        $this->UpdateOrderStatus($last_order_id, "CC",  "WO_" . $this->transaction_id );

        $this->redirect('@check_out_make_payment_report?transaction_id=' . $this->transaction_id . '&new_order_id=' . $NewOrdergetId);
        return;
      } // if ( $this->response_code== '1' and !empty($this->transaction_id) ) {
			//Util::deb( '-1::');

  		$GuestUserArray = Cart::getGuestUser();
			$LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];

			$PaymentTestingEmails= sfConfig::get('app_application_PaymentTestingEmails');
			//Util::deb($PaymentTestingEmails, '$PaymentTestingEmails::');
			$IsSandbox = false;
			if ( in_array($LoggedUserEmail, $PaymentTestingEmails)  ) {
				// Util::deb( '-1 TESTING::');
				$IsSandbox = true;
        $api_login_id = sfConfig::get('app_authorize_net_testing_api_login_id');           // 'YOUR_API_LOGIN_ID';
        $transaction_key = sfConfig::get('app_authorize_net_testing_transaction_key');     // 'YOUR_TRANSACTION_KEY';
        $md5_setting = sfConfig::get('app_authorize_net_testing_md5_setting');         // 'YOUR_API_LN_OGIID'; // Your MD5 Setting
			} else {
				$api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
				$transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
				$md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LN_OGIID'; // Your MD5 Setting

			}
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum = Cart::getCommonSum();

      $amount = $CommonSum;
			//Util::deb($IsSandbox, '-1 $IsSandbox::');
      $this->PostDemoFormHTML = AuthorizeNetDPM::directPostDemo($url, $api_login_id, $transaction_key, $amount, $md5_setting, $IsSandbox );
    } catch (Exception $lException) {
      //      $con->rollBack();
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  private function MakeDataSaving($PaymantType, $transaction_id, $ResaveTransaction= false, $VerifyEmptyUser= true ) {
    try {
      $con = Propel::getConnection(OrderedPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
      $this->CheckoutArray = Cart::getShippingInfo();
      $lNewOrder = new Ordered();

      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];
      // Util::deb( $LoggedUserEmail, '$LoggedUserEmail::' );
      $lLoggedUser = sfGuardUserPeer::retrieveByUsername($LoggedUserEmail);
      // Util::deb( $lLoggedUser, '$lLoggedUser::' );
      // Util::deb( $this->CheckoutArray, '$this->CheckoutArray::' );
      $con->beginTransaction();

      if (!empty($lLoggedUser)) {
        $lNewOrder->setUserId($lLoggedUser->getId());
      } else {
	      $lLoggedUser= $this->CreateUser( $this, $LoggedUserEmail, $this->CheckoutArray );
	      Cart::setGuestUser($LoggedUserEmail);
        $lNewOrder->setUserId($lLoggedUser->getId());
        //$this->redirect('@check_out_customer');
        // $lNewOrder->setUserId(1);   // Admin
      }
      $lNewOrder->setStatus('D'); // D-Draft, Order was submitted on Review Order page, but not submitted for payment with payment method
      // New - user created order and admin yet did not view it.
      // Opened - user created order and admin seen this  order and maybe sent emeil to user.
      // Payed - order is payed and closed.

      // Util::deb( $this->CheckoutArray['b_state'], '$this->CheckoutArray[b_state]::' );
      // Util::deb( AppUtils::getStateAbbrByName( $this->CheckoutArray['b_state']), 'AppUtils::getStateAbbrByName( $this->CheckoutArray[b_state])::' );

      $lNewOrder->setBCountry($this->CheckoutArray['b_country']);
      $lNewOrder->setBPhone($this->CheckoutArray['b_phone']);
      $lNewOrder->setBStreet($this->CheckoutArray['b_address']);
      $lNewOrder->setBStreet2($this->CheckoutArray['b_address_2']);
      $lNewOrder->setBCity($this->CheckoutArray['b_city']);
			//Util::deb( $this->CheckoutArray['b_state'], '$this->CheckoutArray[b_state]::');
			$b_state= AppUtils::getStateShortByFullName( $this->CheckoutArray['b_state']);
			//Util::deb( $b_state, '$b_state::');
			//die("III");
      $lNewOrder->setBState( $b_state );
      $lNewOrder->setBZip($this->CheckoutArray['b_zip']);
      $lNewOrder->setSCountry($this->CheckoutArray['country']);
      $lNewOrder->setSStreet($this->CheckoutArray['address']);
      $lNewOrder->setSStreet2($this->CheckoutArray['address_2']);
      $lNewOrder->setSCity($this->CheckoutArray['city']);
      $lNewOrder->setSState(AppUtils::getStateShortByFullName( $this->CheckoutArray['state']) );
      $lNewOrder->setSZip($this->CheckoutArray['zip']);

      $this->ReviewOrderArray = Cart::getReviewOrder();
      $lNewOrder->setNotes($this->ReviewOrderArray['notes']);
      $lNewOrder->setShipping($this->ReviewOrderArray['shipping_service']);

      $CommonSum = Cart::getCommonSum();
      // $this->shipping_tax_data_array = Cart::getTaxShipData();
      // echo ' $this->ReviewOrderArray:: '. print_r( $this->ReviewOrderArray, true ) .'<br>';
      $lNewOrder->setTotalPrice($CommonSum);

      //     $shipping = $this->getShippingRates($dst_zip);
      $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);
      // echo ' $shipping_array:: '. print_r( $shipping_array, true ) .'<br>';

      if ( $PaymantType == 'DP' ) {
        $tax_percent= 0;
        $tax_sum = 0;
        $lNewOrder->setTax( 0 );
        $TaxShipDataArray= Cart::getTaxShipData();
        // echo '-1:: $TaxShipDataArray:: '. print_r( $TaxShipDataArray, true ) .'<br>';

        $TaxShipDataArray['estimated_total']= $TaxShipDataArray['estimated_total'] - $TaxShipDataArray['est_tax'];

        $TaxShipDataArray['est_tax']= 0;
        //echo '-2:: $TaxShipDataArray:: '. print_r( $TaxShipDataArray, true ) .'<br>';
      } else {
        //$tax_sum = ShippingTaxes::Calculate_tax($this->CheckoutArray['b_zip'], $this->CheckoutArray['b_state'], false);
        $tax_percent= Cart::getTaxPercentByState( AppUtils::getStateNameByAbbr($this->CheckoutArray['state']) );
        $tax_sum = round( ( $CommonSum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
        $lNewOrder->setTax($tax_sum /* $this->shipping_tax_data_array['est_tax'] */);
        // echo '-1:: $lNewOrder->getTax():: '. print_r( $lNewOrder->getTax(), true ) .'<br>';
      }

      if (!empty($shipping_array[$this->ReviewOrderArray['shipping_service']])) {
        $AdditionalFreightSum= Cart::getAdditionalFreightSum();
        $lNewOrder->setShippingCost($shipping_array[$this->ReviewOrderArray['shipping_service']] + $AdditionalFreightSum /* $this->shipping_tax_data_array['est_shipping'] */);
        // echo '-2:: $lNewOrder->getShippingCost():: '. print_r( $lNewOrder->getShippingCost(), true ) .'<br>';
      }

      $lNewOrder->setDiscount(0); // TODO
      // $lNewOrder->setTransactionId('WO'.$transaction_id);

      $this->PaymentDataArray = Cart::getPaymentData();
      // echo ' $this->PaymentDataArray::<pre> '. print_r( $this->PaymentDataArray, true ) .'</pre><br>';
      if (empty($this->PaymentDataArray['payment_method'])) {
        $this->PaymentDataArray['payment_method'] = $PaymantType;
        Cart::setPaymentData(array('payment_method' => $PaymantType, 'card_number' => '', 'expiration_date_year' => '', 'expiration_date_month' => '', 'card_security_code' => ''));
        //$lNewOrder->setPaymentMethod( 'CC' );
      }

      $this->PaymentDataArray = Cart::getPaymentData();
      //$lNewOrder->setPaymentMethod($this->PaymentDataArray['payment_method']);



      $lNewOrder->save();
      if ( $ResaveTransaction ) {
        //$lNewOrder->setTransactionId( 'WO'.$PaymantType . '_' . $lNewOrder->getId() );
        $lNewOrder->save();
      }
      $UsersCartArray = Cart::getUsersCartArray();
      // echo ' $UsersCartArray::<pre> '. print_r( $UsersCartArray, true ) .'</pre><br>';
      $UsersCartItemsSelectedCount = count($UsersCartArray);

      for ($Row = 0; $Row < $UsersCartItemsSelectedCount; $Row++) {
        //echo ' $Row:: '. $Row ;
        if (!empty($UsersCartArray[$Row])) {
          $sku = $UsersCartArray[$Row]['sku'];
          $product_quantity = $UsersCartArray[$Row]['product_quantity'];
          $price_type = $UsersCartArray[$Row]['price_type'];
          $selected_price = (float)$UsersCartArray[$Row]['selected_price'];
          $lInventoryItem = InventoryItemPeer::getSimilarInventoryItem($sku);

          if (!empty($lInventoryItem)) {
            $lNewOrderedItem = new OrderedItem();
            $lNewOrderedItem->setOrderId($lNewOrder->getId());

						$lNewOrderedItem->setSku($sku);

            $lNewOrderedItem->setInventoryItemId( $lInventoryItem->getId() );
            $lNewOrderedItem->setQty($product_quantity);
            $lNewOrderedItem->setUnitPrice( $lInventoryItem->getStdUnitPrice() );
            
            
            
            $lNewOrderedItem->setTitle( $lInventoryItem->getTitle() );
            $lNewOrderedItem->setSize( $lInventoryItem->getSize() );
            $lNewOrderedItem->setColor( $lInventoryItem->getColor() );

            //$lNewOrderedItem->setOptions( $lInventoryItem->getOptions() );

            $lNewOrderedItem->setFinish( $lInventoryItem->getFinish() );
            $lNewOrderedItem->setHand( $lInventoryItem->getHand() );
            $lNewOrderedItem->setGender( $lInventoryItem->getGender() );
            $lNewOrderedItem->setUnitMeasureId( $lInventoryItem->getUnitMeasureId() );
            $lNewOrderedItem->setShipWeight( $lInventoryItem->getShipWeight() );
            
            if (!empty($price_type) and $price_type == 'special') {
              $lNewOrderedItem->setUnitPrice( $lInventoryItem->getClearance_Or_SalePrice() );
            }
            if (!empty($price_type) and $price_type == 'selected_price') {
              $lNewOrderedItem->setUnitPrice( $selected_price );
            }
						if (!empty($price_type) and $price_type == 'badge_price') {
							$lNewOrderedItem->setUnitPrice( $selected_price );
						}
            $lNewOrderedItem->save();
						$lOptionsItems= OptionsItemsPeer::getOptionsItems('',false,$lInventoryItem->getId() );
						//Util::deb( $lOptionsItems, '$lOptionsItems::');
						if( count($lOptionsItems) > 0 ) {
							foreach( $lOptionsItems as $lOptionsItem ) {
								$lOrderedItemOptionsItems= new OrderedItemOptionsItems();
								//Util::deb($lInventoryItem->getSku(), '$lInventoryItem->getSku()::');
								$lOrderedItemOptionsItems->setInventoryItemId( $lInventoryItem->getId() );
								$lOrderedItemOptionsItems->setOptionId( $lOptionsItem->getOptionId() );

								$lOrderedItemOptionsItems->setOrderedItemId( $lNewOrderedItem->getId() );
								$lOrderedItemOptionsItems->save();
							}
							//die("die -1:");
						}
          }
        }
      }
      $con->commit();

      $this->CopiedPaymentData = Cart::getPaymentData();

      $this->getUser()->setAttribute('CopiedCheckoutCopiedArray', $this->CheckoutArray);
      $this->getUser()->setAttribute('CopiedhasLoggedUser', Cart::hasLoggedUser());
      $this->getUser()->setAttribute('CopiedLoggedUserEmail', Cart::getLoggedUserEmail());
      $this->getUser()->setAttribute('CopiedShippingAddress', Cart::hasShippingAddress());
      $this->getUser()->setAttribute('CopiedhasBillingAddress', Cart::hasBillingAddress());
      $this->getUser()->setAttribute('CopiedItemsCount', Cart::getItemsCount());
      $this->getUser()->setAttribute('Copied_shipping_tax_data', $TaxShipDataArray /*Cart::getTaxShipData()*/);
      $this->getUser()->setAttribute('CopiedCommonSum', Cart::getCommonSum());
      $this->getUser()->setAttribute('CopiedPaymentData', Cart::getPaymentData());

      // SendOrderEmailToAdmin( $lOrder, $lsfGuardUser, $order_id, $is_send_touser = false) {
      //$Res = Cart::SendOrderEmailToAdmin( $lNewOrder, $lLoggedUser, /*$lNewOrder->getId(),*/ true);
      //Cart::Clear();
      return $lNewOrder->getId();
    } catch (Exception $lException) {
      $con->rollBack();
      return false;
    }
    // $this->IsOrderSent= false;
  }

  public function executeGet_AuthorizeNet_DPM_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
			// Util::deb( '-2::');
			$GuestUserArray = Cart::getGuestUser();
			$LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];
			$PaymentTestingEmails= sfConfig::get('app_application_PaymentTestingEmails');
			// Util::deb($PaymentTestingEmails, '$PaymentTestingEmails::');
			$IsSandbox = false;
			if ( in_array($LoggedUserEmail, $PaymentTestingEmails)  ) {
				$IsSandbox = true;
        $api_login_id = sfConfig::get('app_authorize_net_testing_api_login_id');           // 'YOUR_API_LOGIN_ID';
        $transaction_key = sfConfig::get('app_authorize_net_testing_transaction_key');     // 'YOUR_TRANSACTION_KEY';
        $md5_setting = sfConfig::get('app_authorize_net_testing_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
        //echo ' $md5_setting:: '. print_r( $md5_setting, true ) .'<br>';
                        
			} else {
				$api_login_id = sfConfig::get('app_authorize_net_api_login_id');           // 'YOUR_API_LOGIN_ID';
				$transaction_key = sfConfig::get('app_authorize_net_transaction_key');     // 'YOUR_TRANSACTION_KEY';
				$md5_setting = sfConfig::get('app_authorize_net_md5_setting');         // 'YOUR_API_LOGIN_ID'; // Your MD5 Setting
			}
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      //print_r($shipping_tax_data);  //debug
      $CommonSum = Cart::getCommonSum();
      //$amount = $CommonSum;
      $amount = $shipping_tax_data['estimated_total'];

			//Util::deb($IsSandbox, '-2 $IsSandbox::');
      echo AuthorizeNetDPM::directPostDemo( $url, $api_login_id, $transaction_key, $amount, $md5_setting, $IsSandbox );

      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGet_PayPal_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');

    try {
      $GuestUserArray = Cart::getGuestUser();
      $LoggedUserEmail = $GuestUserArray['LoggedUserEmail'];

      $DeduggingPaypal= false;
			$PaymentTestingEmails= sfConfig::get('app_application_PaymentTestingEmails');
			//Util::deb($PaymentTestingEmails, '$PaymentTestingEmails::');
      if ( in_array($LoggedUserEmail, $PaymentTestingEmails)  ) {
        $DeduggingPaypal= true;
      }
			//Util::deb($DeduggingPaypal, '$DeduggingPaypal::');
			//die("ttt");
      $is_debug = false;// $DeduggingPaypal;  //false;
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      if ($is_debug)
      Util::deb($shipping_tax_data, '$shipping_tax_data::');
      $CommonSum = Cart::getCommonSum();
      $ItemsCount = Cart::getItemsCount();
      $UsersCartArray = Cart::getUsersCartArray();

      $amount = $CommonSum;
      $this->ReviewOrderArray = Cart::getReviewOrder();
      /*if ($DeduggingPaypal) {
      echo ' $LoggedUserEmail:: ' . print_r($LoggedUserEmail, true) . '<br>';
      echo ' $DeduggingPaypal:: ' . print_r($DeduggingPaypal, true) . '<br>';
      echo ' $this->ReviewOrderArray:: ' . print_r($this->ReviewOrderArray, true) . '<br>';
      } */
      $shipping_array = ShippingTaxes::getShippingRates($this->CheckoutArray['b_zip'], false, false);
      // echo ' $shipping_array:: '. print_r( $shipping_array, true ) .'<br>';

      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      //        $lNewOrder->setShippingCost( $shipping_array[ $this->ReviewOrderArray['shipping_service'] ] /* $this->shipping_tax_data_array['est_shipping'] */ );
      $shipping_cost = $shipping_tax_data['est_shipping'] + Cart::getAdditionalFreightSum();
      //if (empty($shipping_cost) or intval($shipping_cost) <= 0)
      //  $shipping_cost = 10;
      $CheckoutArray = Cart::getShippingInfo();
      $tax_percent= Cart::getTaxPercentByState(AppUtils::getStateNameByAbbr($CheckoutArray['state']) );

      if ($CheckoutArray['b_is_department_purchase'] == 1) {
        $tax_cost = 0;
      } else {
        $tax_cost = round( ( $CommonSum / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
      }

      if ($is_debug) {
        echo '-10:: $UsersCartArray:: <pre>' . print_r($UsersCartArray, true) . '</pre><br>';
        echo '-1:: $amount:: <pre>' . print_r($amount, true) . '</pre><br>';
        echo '-2:: $shipping_cost:: <pre>' . print_r($shipping_cost, true) . '</pre><br>';
        echo '-3:: $tax_cost:: <pre>' . print_r($tax_cost, true) . '</pre><br>';
        echo '-4:: $this->ReviewOrderArray:: <pre>' . print_r($this->ReviewOrderArray, true) . '</pre><br>';
        echo '-41:: 0%2e00:: <pre>' . print_r( urldecode(0%2e00) , true) . '</pre><br>';
        echo '-41:: $CheckoutArray:: <pre>' . print_r( $CheckoutArray , true) . '</pre><br>';
        echo '-42:: $tax_percent:: <pre>' . print_r( $tax_percent , true) . '</pre><br>';
      }



      include_once('lib/paypal.php');

      $requestParams = array(
      'RETURNURL' => Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/paypal_payment_success", //   'http://www.yourdomain.com/payment/success',
      'CANCELURL' => Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/payment/payment_method/-"  // 'http://www.yourdomain.com/payment/cancelled'
      );

      $orderParams = array(
      'PAYMENTREQUEST_0_AMT' => $amount + $shipping_cost + $tax_cost, // Итоговая сумма для перевода. Указывать нужно два десятичных числа, разделенных точкой (.). По желанию можно использовать запятую (,) для разделения тысяч;
      'PAYMENTREQUEST_0_SHIPPINGAMT' => $shipping_cost, // Стоимость доставки заказа
      'PAYMENTREQUEST_0_TAXAMT' => $tax_cost,  //$tax_percent, // налоги
      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',  //$tax_percent, // налоги
      //&PAYMENTREQUEST_0_TAXAMT=0%2e00
      'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD', // Этот параметр определяет валюту, в которой будут проводиться все операции. Указывать нужно трехзначный код. По-умолчанию установлено значение USD;
      'PAYMENTREQUEST_0_ITEMAMT' => $amount, // Итоговая стоимость, не включающая в себя комиссию, таксы, стоимость доставки и прочие доп. расходы. Если таковых не имеется, значение этого параметра должно соответствовать значению PAYMENTREQUEST_0_AMT.
      );

      $item = array();
      $Index = 0;
      $Summa = 0;
      foreach ($UsersCartArray as $NextProduct) {
        /*  [sku] => 06701
        [product_quantity] => 1
        [price_type] => special */
        $lProduct = InventoryItemPeer::getSimilarInventoryItem($NextProduct['sku']);
        $item['L_PAYMENTREQUEST_0_NAME' . $Index] = $lProduct->getTitle();// WithRSymbol
        $item['L_PAYMENTREQUEST_0_DESC' . $Index] = $lProduct->getDescriptionShort();

        if ($NextProduct['price_type'] == 'special') {
          $item['L_PAYMENTREQUEST_0_AMT' . $Index] = $lProduct->getClearance_Or_SalePrice();
        } else {
          $item['L_PAYMENTREQUEST_0_AMT' . $Index] = $lProduct->getStdUnitPrice();
        }

        $item['L_PAYMENTREQUEST_0_QTY' . $Index] = $NextProduct['product_quantity'];
        $Summa+= $item['L_PAYMENTREQUEST_0_AMT' . $Index] * $item['L_PAYMENTREQUEST_0_QTY' . $Index];

        $Index++;
      }
      if ($is_debug)
      echo '-42:: $Summa:: <pre>' . print_r($Summa, true) . '</pre><br>';


      //Util::deb($DeduggingPaypal, '$DeduggingPaypal::');
      $PayPal = new PayPal( $DeduggingPaypal );


      $paypal_response = $PayPal->request("SetExpressCheckout", $requestParams + $orderParams + $item, $DeduggingPaypal );
      if ($is_debug)
      echo '-5:: $paypal_response:: <pre>' . print_r($paypal_response, true) . '</pre><br>';
      if (is_array($paypal_response) && $paypal_response['ACK'] == 'Success') { // Запрос был успешно принят
        //$PayPal->success();
        $token = $paypal_response['TOKEN'];

        if ($is_debug)
        echo '-6:: $token INSIDE:: <pre>' . print_r($token, true) . '</pre><br>';
        ###
        $checkoutDetails = $PayPal->request('GetExpressCheckoutDetails', array('TOKEN' => $token) );
        if ( $is_debug ) echo '-7:: $checkoutDetails:: <pre>'. print_r( $checkoutDetails, true ) .'</pre><br>';
        if ( $DeduggingPaypal ) {
          $LocationUrl = 'Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token);
        } else {
          $LocationUrl= 'Location: https://www.paypal.com/webscr?cmd=_express-checkout&token=' . urlencode($token);
        }
        //echo "L". $LocationURL;  //debug
        //$LocationUrl = 'Location: https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&token=' . urlencode($token);

        if ($is_debug) {
          echo '-8:: $LocationUrl:: <pre>' . print_r($LocationUrl, true) . '</pre><br>';
        }
        header($LocationUrl);
        return;
      }

      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGet_GoogleCheckout_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    try {
      //$token = $request->getParameter('token');
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executeGet_GoogleCheck_Button_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $PaymentHTML= Cart::PrepareAndRedirectGoogleChechout();
      echo $PaymentHTML;
      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeGoogle_checkout_payment_success(sfWebRequest $request) {
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {
    try {

      // echo '<pre> getenv("HTTP_REFERER") ::'.print_r( getenv("HTTP_REFERER"), true ).'</pre><br>';
      // echo '<pre> $_SERVER ::'.print_r( $_SERVER, true ).'</pre><br>';
      $time= time();
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      $this->UpdateOrderStatus($last_order_id, "GC",  "WO_" . $token );

      //$NewOrdergetId = $this->MakeDataSaving("GC", $time, true);
      //$lNewOrdergetId= OrderedPeer::retrieveByPK($NewOrdergetId);
      $this->redirect('@check_out_make_payment_report?transaction_id=' . $time . '&new_order_id=' . $last_order_id);
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executePaypal_payment_success(sfWebRequest $request) {
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {

    try {

      $token = $request->getParameter('token');
      $PayerID = $request->getParameter('PayerID');

      //$NewOrdergetId = $this->MakeDataSaving("PP", $token, false);
      $last_order_id= sfContext::getInstance()->getUser()->getAttribute( "last_order_id" );
      $this->UpdateOrderStatus($last_order_id, "PP",  "WO_" . $token );
      //    $this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $last_order_id );


      include_once('lib/paypal.php');
      $PayPal = new PayPal();

      //$amount = Cart::getCommonSum();
      //$this->ReviewOrderArray = Cart::getReviewOrder();
      //$shipping_tax_data = Cart::getTaxShipData();
      //$shipping_cost = $shipping_tax_data['est_shipping'];
      //$CheckoutArray = Cart::getShippingInfo();
      //$tax_percent= Cart::getTaxPercentByState( $CheckoutArray['state'] );
      //if ($CheckoutArray['b_is_department_purchase'] == 1) {
      //  $tax_cost = 0;
      //} else {
      //  $tax_cost = round( ( $amount / 100 * $tax_percent ), 2 ) ; // $shipping_tax_dat['est_tax'];
      //}

      $checkoutDetails = $PayPal->request('GetExpressCheckoutDetails', array('TOKEN' => $token) );
      //echo "<pre>"; //debug
      //print_r($checkoutDetails);  //debug
      //echo "</pre>"; //debug

      $orderParams = array(
      'PAYMENTREQUEST_0_AMT' => $checkoutDetails['ITEMAMT'] + $checkoutDetails['SHIPPINGAMT'] + $checkoutDetails['TAXAMT'],
      'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
      'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
      'PAYMENTREQUEST_0_SHIPPINGAMT' => $checkoutDetails['SHIPPINGAMT'],
      'PAYMENTREQUEST_0_TAXAMT' => $checkoutDetails['TAXAMT'],
      'PAYMENTREQUEST_0_ITEMAMT' => $checkoutDetails['ITEMAMT']
      );

      $additionalParams = array(
      'TOKEN' => $token,
      'PAYERID' => $PayerID
      );

      ###


      $paypal_response = $PayPal->request("DoExpressCheckoutPayment", $orderParams + $additionalParams, $is_debug);

/*      echo "<pre>"; //debug
      print_r($paypal_response); //debug
      echo "</pre>";  //debug
      exit; //debug
*/
      if(is_array($paypal_response) && $paypal_response['ACK'] == 'Success') {
        $this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $last_order_id);
      } else {
				$this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $last_order_id);
      }

    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executePaypal_payment_cancelled(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }


  public function executeGet_Department_Purchase_HTML(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $url = Util::getServerHost(sfContext::getInstance()->getConfiguration()) . "check_out/make_payment";     // http://YOUR_DOMAIN.com/direct_post.php";
			//Util::deb( '-3::');
      $shipping_tax_data = Cart::getTaxShipData();  //  [postal_code] => 55555 [usps] => 0 [ups_ground] => 9.36 [ups_3_day_selected] => 14.03 [ups_2_day_air] => 18.70 [ups_next_day_air_saver] => 48.79
      $CommonSum = Cart::getCommonSum();
      $amount = $CommonSum;
      echo 'Department Purchase';
      //      return sfView::NONE;
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeMake_Department_Purchase_Save(sfWebRequest $request) {
    Util::deb(  ' executeGet_Department_Purchase_HTML::' );
    $response = $this->context->getResponse(); // http://www.adenium.net/oherron.com/web/check_out/paypal_payment_success?token=EC-7S971534L8940971K&PayerID=JTAEEQBPFEEYW {
    try {
      $token = $request->getParameter('token');
      $PayerID = $request->getParameter('PayerID');

      $NewOrdergetId = $this->MakeDataSaving("PP", $token, false );
      $this->redirect('@check_out_make_payment_report?transaction_id=' . $token . '&new_order_id=' . $NewOrdergetId);

    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  public function executeMake_payment_report(sfWebRequest $request) {
    $response = $this->context->getResponse();
    $response->addJavascript('/js/jquery/jquery.js', 'last');
    $response->addJavascript('/js/jquery/jquery-ui.js', 'last');
    $this->transaction_id = $request->getParameter('transaction_id');
    $this->new_order_id = $request->getParameter('new_order_id');
    $this->lNewOrder = OrderedPeer::retrieveByPK($this->new_order_id);
    try {

      $this->CopiedCheckoutCopiedArray = sfContext::getInstance()->getUser()->getAttribute('CopiedCheckoutCopiedArray');
      $this->CopiedhasLoggedUser = sfContext::getInstance()->getUser()->getAttribute('CopiedhasLoggedUser');
      $this->CopiedLoggedUserEmail = sfContext::getInstance()->getUser()->getAttribute('CopiedLoggedUserEmail');
      $this->CopiedShippingAddress = sfContext::getInstance()->getUser()->getAttribute('CopiedShippingAddress');
      $this->CopiedhasBillingAddress = sfContext::getInstance()->getUser()->getAttribute('CopiedhasBillingAddress');
      $this->CopiedItemsCount = sfContext::getInstance()->getUser()->getAttribute('CopiedItemsCount');
      $this->Copied_shipping_tax_data = sfContext::getInstance()->getUser()->getAttribute('Copied_shipping_tax_data');
      $this->CopiedCommonSum = sfContext::getInstance()->getUser()->getAttribute('CopiedCommonSum');
      $this->CopiedPaymentData = sfContext::getInstance()->getUser()->getAttribute('CopiedPaymentData');
      $response->setCookie( 'user_has_logged', '0', 0, '/' );
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }

  }


  public function executeRetrievelink() {
  }

  public function executeThankyou() {
  }


 public function executeReset_password(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $response->setTitle("ROC Checkout");
      $this->form = new ResetPasswordForm();
      if ($request->isMethod('post')) {
        $data = $request->getParameter('reset_password');
        $this->form->bind($data);
        if ($this->form->isValid()) {
          $NewPassword= sfGuardUserPeer::GeneratePassword();
          $lsfGuardUser= sfGuardUserPeer::getByUsername( $data['email'] );
          if ( !empty($lsfGuardUser) ) {
            $lsfGuardUser->setSalt('');
            $lsfGuardUser->setPassword( $NewPassword );
            $lsfGuardUser->setAlgorithm('md5');
            $lsfGuardUser->save();
/* 3. The text in the body of the email should be exactly as specified in the task description above.
The From field in the email should be: "Ray O'Herron Co." < no-reply@oherron.com >
The email subject field should be: "Your account information"*/

            $MsgFromAddress = 'From: "Ray O\'Herron Co." < no-reply@oherron.com >';
            $recipient = $data['email'];
            $mail_body = "Ray O'Herron Co.
Suppliers of Public Safety Equipment Since 1964

Hello ".$lsfGuardUser->getName().",

We have processed your request to reset your password. Your new password is:

".$NewPassword."

Go to the Checkout page and complete your order. http://www.oherron.com/check_out/customer
Ray O'Herron Co.
1-800-223-2097";
//'Dear '.$lsfGuardUser->getName().', Your password at site http://www.oherron.com was changed. Your e-mail is '.$data['email'].' Your password is '. $NewPassword;
            $subject = 'Your account information';
            $header = $MsgFromAddress;//"From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields
            mail($recipient, $subject, $mail_body, $header);
            $this->redirect('@reset_password_done');
          }
        }
      }
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }



 public function executeReset_password_done(sfWebRequest $request) {
    $response = $this->context->getResponse();
    try {
      $response->setTitle("ROC Checkout");
    } catch (Exception $lException) {
      $this->logMessage($lException->getMessage(), 'err');
      return sfView::ERROR;
    }
  }

  private function CreateUser( $ControllerRef, $UserEmail, $CheckoutArray ) {

    $lNewSfUser = new sfGuardUser();
    $lNewSfUser->setName( $CheckoutArray['name'] );
    $lNewSfUser->setUsername( $UserEmail );
    $lNewSfUser->setIsActive(true);
    $lNewSfUser->setIsSuperAdmin(false);
    $lNewSfUser->save();
    $lsfGuardUserGroup = new sfGuardUserGroup();
    $lsfGuardUserGroup->setUserId($lNewSfUser->getId());
    $lsfGuardUserGroup->setGroupId(sfGuardGroupPeer::getGroupIdByName('public'));
    $lsfGuardUserGroup->save();
    $lBufUser = $lNewSfUser;
    $ControllerRef->getUser()->addCredential('public');
    $ControllerRef->getUser()->signIn($lBufUser);
    return $lNewSfUser;
  }
}