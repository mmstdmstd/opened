<?php

/**
 * cron actions.
 *
 * @package    sf_sandbox
 * @subpackage cron
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cronActions extends sfActions
{


	public function executeFill_udf_swc_colors_fields(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		try {
			$this->InventoryItemsListCount = InventoryItemPeer::getInventoryItems(1, false, true );
			$page_number= 1; $Limit= 100;
			$RowsChanged= 0;
			$RowsSkipped= 0;
			$Row= 0;
			while( $page_number * $Limit < $this->InventoryItemsListCount ) {
				$this->InventoryItemsList= InventoryItemPeer::getInventoryItemsListByLimits( $page_number, $Limit );

				foreach( $this->InventoryItemsList as $lInventoryItem ) {
					$Row++;
					$Color= $lInventoryItem->getColor();

					$lColorMapping= ColorMappingPeer::getSimilarColorMapping($Color);
					if ( !empty($Color) and !empty($lColorMapping) ) {
						$SwatchColor1= trim( $lColorMapping->getSwatchColor1() );
						$SwatchColor2= trim( $lColorMapping->getSwatchColor2() );
						$SwatchColor3= trim( $lColorMapping->getSwatchColor3() );
						if ( !empty($SwatchColor1) ) {
						  $lInventoryItem->setUdfItSwcBeige( strtoupper($SwatchColor1) == 'UDF_IT_SWC_BEIGE' );
					  	$lInventoryItem->setUdfItSwcBlack( strtoupper($SwatchColor1) == 'UDF_IT_SWC_BLACK' );
				  		$lInventoryItem->setUdfItSwcBlue( strtoupper($SwatchColor1) == 'UDF_IT_SWC_BLUE' );
			  			$lInventoryItem->setUdfItSwcBrown( strtoupper($SwatchColor1) == 'UDF_IT_SWC_BROWN' );
		  				$lInventoryItem->setUdfItSwcCamo( strtoupper($SwatchColor1) == 'UDF_IT_SWC_CAMO' );
	  					$lInventoryItem->setUdfItSwcGold( strtoupper($SwatchColor1) == 'UDF_IT_SWC_GOLD' );
   						$lInventoryItem->setUdfItSwcGray( strtoupper($SwatchColor1) == 'UDF_IT_SWC_GRAY' );
						  $lInventoryItem->setUdfItSwcGreen( strtoupper($SwatchColor1) == 'UDF_IT_SWC_GREEN' );
					  	$lInventoryItem->setUdfItSwcOrange( strtoupper($SwatchColor1) == 'UDF_IT_SWC_ORANGE' );
				  		$lInventoryItem->setUdfItSwcPink( strtoupper($SwatchColor1) == 'UDF_IT_SWC_PINK' );
			  			$lInventoryItem->setUdfItSwcPurple( strtoupper($SwatchColor1) == 'UDF_IT_SWC_PURPLE' );
		  				$lInventoryItem->setUdfItSwcRed( strtoupper($SwatchColor1) == 'UDF_IT_SWC_RED' );
	  					$lInventoryItem->setUdfItSwcSilver( strtoupper($SwatchColor1) == 'UDF_IT_SWC_SILVER' );
  						$lInventoryItem->setUdfItSwcWhite( strtoupper($SwatchColor1) == 'UDF_IT_SWC_WHITE' );
						  $lInventoryItem->setUdfItSwcYellow( strtoupper($SwatchColor1) == 'UDF_IT_SWC_YELLOW' );
						}
						if ( !empty($SwatchColor2) ) {
							//Util::deb( $Color, '$Color:::');
							//Util::deb( $SwatchColor1, '$SwatchColor1:::');
							//Util::deb( $SwatchColor2, '$SwatchColor2:::');
							//Util::deb($lInventoryItem->getSku(), '222::$lInventoryItem->getSku()::');
							//die("222:");
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_BEIGE' ) {
							  $lInventoryItem->setUdfItSwcBeige( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_BLACK' ) {
							  $lInventoryItem->setUdfItSwcBlack( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_BLUE' ) {
						  	$lInventoryItem->setUdfItSwcBlue( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_BROWN' ) {
							  $lInventoryItem->setUdfItSwcBrown( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_CAMO' ) {
							  $lInventoryItem->setUdfItSwcCamo( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_GOLD' ) {
							  $lInventoryItem->setUdfItSwcGold( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_GRAY' ) {
						  	$lInventoryItem->setUdfItSwcGray( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_GREEN' ) {
							  $lInventoryItem->setUdfItSwcGreen( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_ORANGE' ) {
							  $lInventoryItem->setUdfItSwcOrange( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_PINK' ) {
							  $lInventoryItem->setUdfItSwcPink( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_PURPLE' ) {
							  $lInventoryItem->setUdfItSwcPurple( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_RED' ) {
							  $lInventoryItem->setUdfItSwcRed( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_SILVER' ) {
							  $lInventoryItem->setUdfItSwcSilver( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_WHITE' ) {
							  $lInventoryItem->setUdfItSwcWhite( true );
							}
							if ( strtoupper($SwatchColor2) == 'UDF_IT_SWC_YELLOW' ) {
							  $lInventoryItem->setUdfItSwcYellow( true );
							}
						}


						if ( !empty($SwatchColor3) ) {
							//Util::deb( $Color, '$Color:::');
							//Util::deb( $SwatchColor1, '$SwatchColor1:::');
							//Util::deb( $SwatchColor3, '$SwatchColor3:::');
							//Util::deb($lInventoryItem->getSku(), '333::$lInventoryItem->getSku()::');
							// die("222:");
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_BEIGE' ) {
								$lInventoryItem->setUdfItSwcBeige( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_BLACK' ) {
								$lInventoryItem->setUdfItSwcBlack( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_BLUE' ) {
								$lInventoryItem->setUdfItSwcBlue( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_BROWN' ) {
								$lInventoryItem->setUdfItSwcBrown( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_CAMO' ) {
								$lInventoryItem->setUdfItSwcCamo( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_GOLD' ) {
								$lInventoryItem->setUdfItSwcGold( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_GRAY' ) {
								$lInventoryItem->setUdfItSwcGray( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_GREEN' ) {
								$lInventoryItem->setUdfItSwcGreen( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_ORANGE' ) {
								$lInventoryItem->setUdfItSwcOrange( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_PINK' ) {
								$lInventoryItem->setUdfItSwcPink( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_PURPLE' ) {
								$lInventoryItem->setUdfItSwcPurple( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_RED' ) {
								$lInventoryItem->setUdfItSwcRed( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_SILVER' ) {
								$lInventoryItem->setUdfItSwcSilver( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_WHITE' ) {
								$lInventoryItem->setUdfItSwcWhite( true );
							}
							if ( strtoupper($SwatchColor3) == 'UDF_IT_SWC_YELLOW' ) {
								$lInventoryItem->setUdfItSwcYellow( true );
							}
						}

						$lInventoryItem->save();
						$RowsChanged++;
					} else {
						$RowsSkipped++;
					}
				} // foreach( $this->InventoryItemsList as $lInventoryItem ) {
				$page_number++;
			}
			$this->InfoText= count($this->InventoryItemsList) . ' products, Rows Changed : ' . $RowsChanged . ',  RowsSkipped :' . $RowsSkipped;

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	/**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
	public function executeInventory_items_import(sfWebRequest $request)
	{
    $old_memory_limit= ini_set("memory_limit","1200M");
    $old_max_execution_time= ini_set("max_execution_time","3600");

    //echo '$old_memory_limit = ' . $old_memory_limit . "\n\r";
    //echo '$old_max_execution_time = ' . $old_max_execution_time . "\n\r";
    
    //echo 'memory_limit = ' . ini_get('memory_limit') . "\n\r";
    //echo 'max_execution_time = ' . ini_get('max_execution_time') . "\n\r";
    
		$manually= $request->getParameter('manually');
		$small_test= $request->getParameter('small_test');
		
		$this->InfoText= '';
		include_once('lib/InventoryItemsImport.php');
		try
		{
			$InventoryItemsImport= new InventoryItemsImport();
			
			$sf_root_dir= sfConfig::get('sf_root_dir');
			if ( !$small_test ) {
			  $InventoryItemsImportCVSFile= sfConfig::get('app_application_InventoryItemsImportCVSFile');
			} else {
			  $InventoryItemsImportCVSFile= sfConfig::get('app_application_InventoryItemsImportCVSFile_SmallTest');				
			}
			$InventoryItemsImport->setInventoryItemsImportCVSFile( $sf_root_dir . $InventoryItemsImportCVSFile );
						
			//$con = Propel::getConnection( InventoryItemPeer::DATABASE_NAME,  Propel::CONNECTION_WRITE );
			//$con->beginTransaction();
			$ImportRes= $InventoryItemsImport->Run();
			echo '$ImportRes::'.print_r( $ImportRes, true ).'<br>';
			if ( $ImportRes ) {
				//$con->commit();
			} else {
				//$con->rollBack();
			}
			$this->InfoText= $InventoryItemsImport->getInfoText();
      if ( $manually ) {       // http://www.local-lamp.com/frontend_dev.php/cron_calculating_score?manually=1
      } else {
      	return sfView::NONE;
      }
		}
		catch (Exception $lException)
		{
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}
	
	
}