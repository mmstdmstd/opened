<?php
$product_listings_columns= 2;
$A= array();
foreach($UsersCartArray as $UsersCartItem) {
	$sku= $UsersCartItem['sku'];
  $lProduct= InventoryItemPeer::getSimilarInventoryItem($sku);
	if ( !empty($lProduct) and is_object($lProduct)) {
		$A[]= array( 'sku'=>$sku, 'product_quantity'=> $UsersCartItem['product_quantity'], 'price_type'=> $UsersCartItem['price_type'], 'selected_price'=> $UsersCartItem['selected_price'] );
	}
}

$UsersCartArray= $A;
reset($UsersCartArray);
$UsersCartArrayLength= count($UsersCartArray);
$UsersCartItemsSelectedCount= Cart::getItemsCount();
if ( $UsersCartArrayLength==1 )  {
	$product_listings_columns=1;
}

$RowsCount= (int) ($UsersCartArrayLength/$product_listings_columns);
$HasDecimal= $UsersCartArrayLength%$product_listings_columns;
if ( $HasDecimal> 0 ) $RowsCount++;
$HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
?>

  <?php if ( $UsersCartArrayLength > 0 ) : ?>
      <table cellpadding="0" cellspacing="0" border="0" valign="top" width="636px">
      <?php $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
        <tr id="tr_product_listings_<?php echo $Row ?>" align="left">
        <?php for($Col=1; $Col<= $product_listings_columns; $Col++) : ?>
          <td >
            <?php if ( !empty($UsersCartArray[$I]) ) : ?>
     		      <?php include_partial( 'main/view_cart_item', array( 'Index'=>$I, 'Col'=> $Col, 'Row'=>$Row, 'Item'=>$UsersCartArray[$I] ) ) ?>
            <?php else: ?>
              &nbsp;
            <?php endif; ?>
          </td >
        <?php $I++; endfor; ?>

        </tr>
      <?php endfor; ?>
      </table>
  <?php endif; ?>