<div id="policy_div" >
  <h2>Company</h2>

  <div style="margin:0 auto; width:476px;">
    <img style="float:right; border:solid 1px #000; margin-left:50px;" src="<?php Util::getServerHost() ?>/images/Ray.jpg" width="195" height="250" />
    <h2 style="margin-top:20px; text-align:left; margin-left:14px;">The Source for All Your Quality Public Safety Equipment Needs</h2>
      <p class="right_column_text">Thank you for visiting the ALL NEW Ray O'Herron Web store. This business has come a long way since Ray Sr. started selling Radar out of the trunk of his car in 1964. We are a fourth generation family business who still operate on Ray's golden rule: SERVICE!!! When you buy from Ray O'Herron you are buying from a company that has been in business for almost 50 years. We have a very large inventory of quality products and can usually ship next day for items that are in stock. Our trained employees have the background to answer your questions knowledgeably and can offer help if you are uncertain about a particular product purchase.</p>
      <p class="right_column_text">It is our mission at Ray O'Herron to strive for excellence through our products and services. We have two store locations to serve you better, and we also have eight outside sales reps who can personalize your experience when shopping with us. You can go to our <a href="main/salesperson">Salesperson</a> tab to find out which sales rep services your area. If you are not sure, please call or email us so we can assist you.</p>
      <p class="right_column_text">We have developed a new, simple to use, product page to help you easily locate the products you are looking for within our massive inventory. In addition, to make it even easier to pin point the exact product you are looking for, we have added a versatile search bar to the product pages. With this search bar you can search for a product name, product number, brand, etc., within any product category or subcategory.</p>
			<p class="right_column_text">Ray always said that "if it were not for the customer we wouldn't have a job". This is the founding thought and belief of this company. I would like to invite you to shop with us today. We would also love to hear from you if you have questions or comments. Please also like us on <a href="https://www.facebook.com/RayOHerronCompany">Facebook</a>.</p>
			<p class="right_column_text">I would like to dedicate our new web store to the founder of our company who passed away in May 2011. Mr. Ray O'Herron Sr. We miss you dearly and hope we make you proud.</a>
			<p class="right_column_text" style="margin-bottom:27px !important;">
			Michael O'Herron<br />VP of Operations<br /><br /></p>
  </div>
</div><!-- close policy_div-->