<span style="display:none" <?php echo ( Util::isDeveloperComp() ) ? '' : ' style="display:none" ' ?> >
 <!-- <input type="checkbox" id="is_debug"  > -->
</span>
<?php
$Features = $Product->getFeatures();
//$InventoryCategory = $Product->getInventoryCategory();
$InventoryItemInventoryCategory= InventoryItemInventoryCategoryPeer::getInventoryItemInventoryCategorysByItemInventoryId( '', false, $Product->getId(), true );
//Util::deb($InventoryItemInventoryCategory[, '$InventoryItemInventoryCategory::');
$InventoryCategory= '';
if ( !empty($InventoryItemInventoryCategory[0]) ) {
	$lInventoryCategory= $InventoryItemInventoryCategory[0]->getInventoryCategory();
//	$lInventoryCategory= $InventoryItemInventoryCategory[0]->getInventoryCategory();
	//Util::deb($lInventoryCategory, '$lInventoryCategory::');
}
//$InventoryCategory= $A->

$Size = $Product->getSize();
$Color = $Product->getColor();
$Gender = $Product->getGender();
$DescriptionShort = $Product->getDescriptionShort();
//$SizingId = $Product->getSizingId();
$Sizing= SizingsPeer::retrieveByPK( $Product->getSizingId() );
//Util::deb( $Sizing, '$Sizing::');

//$Options = $Product->getOptions();
//$Paperwork = $Product->getPaperwork();
$UnitMeasure = MeasuresPeer::retrieveByPK( $Product->getMeasureId() );


$QtyOnHand = $Product->getQtyOnHand();
$StdUnitPrice = $Product->getStdUnitPrice();
$Msrp = $Product->getMsrp();
$SaleStartDate = $Product->getSaleStartDate();
$SaleEndDate = $Product->getSaleEndDate();
$TaxClass = $Product->getTaxClass();
$ShipWeight = $Product->getShipWeight();
$SalePromoDiscount = $Product->getSalePromoDiscount();
$Finish = $Product->getFinish();
$SetupCharge = $Product->getSetupCharge();
$SaleMethod = $Product->getSaleMethod();
$SalePrice = $Product->getSalePrice();
$SalePercent = $Product->getSalePercent();
// Util::deb( $Matrix, ' $Matrix::' );
//if ( $HasRootItem and $Matrix and !empty($RootItemObject) ) {
if (!empty($HasRootItem) and !empty($Matrix) and !empty($RootItemObject)) {
    $Features = $RootItemObject->getFeatures();
    //$InventoryCategory = $RootItemObject->getInventoryCategory();

	  $InventoryItemInventoryCategory= InventoryItemInventoryCategoryPeer::getInventoryItemInventoryCategorysByItemInventoryId( '', false, $RootItemObject->getId(), true );
	  // Util::deb($InventoryItemInventoryCategory[0], '$InventoryItemInventoryCategory::');
	  if ( !empty($InventoryItemInventoryCategory[0]) ) {
			$lInventoryCategory= $InventoryItemInventoryCategory[0]->getInventoryCategory();
		  $InventoryCategory= $InventoryItemInventoryCategory[0]->getInventoryCategory()->getProductLine();
	  	// Util::deb($InventoryCategory, '$InventoryCategory::');
	  }


	  $Size = $RootItemObject->getSize();
    $Color = $RootItemObject->getColor();
    $Gender = $RootItemObject->getGender();
    $DescriptionShort = $RootItemObject->getDescriptionShort();
    //$Sizing = $RootItemObject->getSizingId();
  	$Sizing= SizingsPeer::retrieveByPK( $RootItemObject->getSizingId() );

	  // $Options = $RootItemObject->getOptions();
    //$Paperwork = $RootItemObject->getPaperwork();
    // $UnitMeasure = $RootItemObject->getUnitMeasure();
	  $UnitMeasure = MeasuresPeer::retrieveByPK( $Product->getMeasureId() );

    $QtyOnHand = $RootItemObject->getQtyOnHand();
    $StdUnitPrice = $RootItemObject->getStdUnitPrice();
    $Msrp = $RootItemObject->getMsrp();
    $SaleStartDate = $RootItemObject->getSaleStartDate();
    $SaleEndDate = $RootItemObject->getSaleEndDate();
    $TaxClass = $RootItemObject->getTaxClass();
    $ShipWeight = $RootItemObject->getShipWeight();
    $SalePromoDiscount = $RootItemObject->getSalePromoDiscount();
    $Finish = $RootItemObject->getFinish();
    $SetupCharge = $RootItemObject->getSetupCharge();
    $SaleMethod = $RootItemObject->getSaleMethod();
    $SalePrice = $RootItemObject->getSalePrice();
    $SalePercent = $RootItemObject->getSalePercent();
}
//$lInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategory($InventoryCategory);
$InventoryCategoryText = '';
if (!empty($lInventoryCategory)) {
	//Util::deb( '-3::');
    $InventoryCategoryText = $lInventoryCategory->getCategory();
}
// Util::deb( $InventoryCategoryText, '-$InventoryCategoryText::');

	//die("die -1:");
$features_multi_array = array();
$result_colors_list = array();
$result_size_list = array();
$result_options_list = array();
$result_hands_list = array();
$result_finishes_list = array();
$FeaturesList = preg_split('/,/', $Features);
foreach ($FeaturesList as $key => $value) {
    $FeaturesList[$key] = strtolower($value);
}
// uasort( $FeaturesList, 'cmp' );
//Util::deb( $FeaturesList, '$FeaturesList::' );
$UsedFeaturesList = array();
/* if ($sf_request->getParameter('debug') == '9' ) {
  echo '<pre> $FeaturesList::'.print_r( $FeaturesList, true ).'</pre>';
  } */

$HasDifferentPrices = false;
// Util::deb( $Matrix, '$Matrix::' );
if ($Matrix) {
    $HasDifferentPrices = InventoryItemPeer::getHasDifferentPrices($RootItem);
}
// Util::deb( $HasDifferentPrices, '$HasDifferentPrices::' );
?>

<ul id="select_size">
    <?php
    foreach ($FeaturesList as $NextFeature) : $NextFeature = strtolower(trim($NextFeature));
        $UsedFeaturesList[] = $NextFeature;
        ?>
        <?php if ($NextFeature == 'inventory_category') : ?>
                <?php if (!empty($InventoryCategoryText)) : ?>
                <li>
                <?php echo $InventoryCategoryText ?>
                </li>
            <?php endif; ?>
        <?php endif; ?>
        <?php
        //Util::deb($NextFeature, '$NextFeature::');
        if ($NextFeature == 'size') {

            $SizesList = InventoryItemPeer::getSizesListByRootItem($RootItem);
            $SizesList = AppUtils::getSizeOrderedArray($SizesList);  // TODO!
            //Util::deb( $SizesList, '-1 $SizesList::' );
            //die("+++");
            /* if ($sf_request->getParameter('debug') == '9' ) {
              echo '<pre> $SizesList ::'.print_r( $SizesList, true ).'</pre>';
              } */

            $result_size_list = array();
            if (!empty($SizesList)) {
                foreach ($SizesList as $key => $value_array) {
                    // Util::deb( $value_array, ' -size : $value_array::' );
                    $IsFound = false;
                    for ($i = 0; $i < count($features_multi_array); $i++) {
                        if ($features_multi_array[$i]['sku'] == $key) {
                            $features_multi_array[$i]['size'] = $value_array['size'];
                            $features_multi_array[$i]['size_std_unit_price'] = $value_array['std_unit_price'];
                            $IsFound = true;
                            break;
                        }
                    }
                    if (!$IsFound) {
                        $img = get_image_by_sku($key);
                        $features_multi_array[] = array('sku' => $key, 'size' => $value_array['size'], 'size_std_unit_price' => $value_array['std_unit_price'], 'color' => '', 'option' => '',
                            'hand' => '', 'finish' => '', 'image' => $img);
                    }

                    $result_size_list[$key] = $value_array['size']; //. (  $HasDifferentPrices ? ' ( $' . $value_array['std_unit_price'] .' )' : '' );
                }  // foreach($SizesList as $key => $value_array) {
                echo '<li id="li_size">';
                echo '<b>Size:</b><br />' . Util::select_tag('select_size_', Util::SetArrayHeader(array('' => ' -select- '), /* $result_size_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'size\'); return false;')) . '&nbsp;&nbsp';
							//Util::deb($Sizing, '$Sizing::');
                if (!empty($Sizing)) {
                    $lSizingChart = SizingChartPeer::retrieveByPK($Sizing);
                    $SizingChartsDirectory = sfConfig::get('app_application_SizingChartsDirectory');
                    $SizingChartFilename = $SizingChartsDirectory . DIRECTORY_SEPARATOR . $lSizingChart->getFilename();
                    $tb_width = 850;
                    $tb_height = 800;
                    $FileArray = @getimagesize(str_replace("\\", "/", sfConfig::get('sf_root_dir') . Util::getSiteRootDir() . $SizingChartFilename));
                    if (!empty($FileArray[0])) {
                        $tb_width = $FileArray[0] + 20;
                    }
                    if (!empty($FileArray[1])) {
                        $tb_height = $FileArray[1] + 20;
                    }

                    echo '<span>';
                    $tb_height = 'ZZZZZ';
                    $Src = '<a id="a_tb_size_chart" class="thickbox" href="' . url_for("@product_size_chart?code=" . $Sizing) . '?width=' . $tb_width . '&height=' . $tb_height . '" >Size Chart</a> ';
                    echo $Src;
                    echo '</span>';
                }
                echo '</li><br />';
            } // if (!empty($SizesList)) {
        } //if ($NextFeature == 'size') {
//die("-2");


        if ($NextFeature == 'color') {
            $ColorsList = InventoryItemPeer::getColorsListByRootItem($RootItem);
            //Util::deb($ColorsList, '$ColorsList::');
            /* if ($sf_request->getParameter('debug') == '9' ) {
              echo '<pre> $ColorsList ::'.print_r( $ColorsList, true ).'</pre>';
              } */
            if (!empty($ColorsList)) {
                $result_colors_list = array();
                foreach ($ColorsList as $key => $value_array) {
                    $IsFound = false;
                    for ($i = 0; $i < count($features_multi_array); $i++) {
                        if ($features_multi_array[$i]['sku'] == $key) {
                            $features_multi_array[$i]['color'] = $value_array['size'];
                            $features_multi_array[$i]['color_std_unit_price'] = $value_array['std_unit_price'];
                            $IsFound = true;
                            break;
                        }
                    }
                    if (!$IsFound) {
                        $img = get_image_by_sku($key);
                        $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => $value_array['color'], 'color_std_unit_price' => $value_array['std_unit_price'], 'option' => '', 'hand' => '', 'finish' => '', 'image' => $img);
                    }
                    $result_colors_list[$key] = $value_array['color']; // . (  $HasDifferentPrices ? ' ( $' . $value_array['std_unit_price'] .' )' : '' );
                } // foreach ($ColorsList as $key => $value_array) {
                echo '<li id="li_color" >';
                echo '<b>Color:</b><br />' . Util::select_tag('select_color', Util::SetArrayHeader(array('' => ' -select- '), /* $result_colors_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'color\'); return false;'));
                echo '</li>';
            } // if (!empty($ColorsList)) {
        } //if ($NextFeature == 'color') {
        //Util::deb( $features_multi_array, '$features_multi_array::' );
        //Util::deb( $result_size_list, '-2 $result_size_list::' );

        if ($NextFeature == 'options') {
            $OptionsList = InventoryItemPeer::getOptionsListByRootItem($RootItem);
             //Util::deb($OptionsList, '$OptionsList::');
						//die("die -1:");
            /* if ($sf_request->getParameter('debug') == '9' ) {
              echo '<pre> $OptionsList ::'.print_r( $OptionsList, true ).'</pre>';
              } */
            if (!empty($OptionsList)) {
                $result_Options_list = array();
                foreach ($OptionsList as $key => $value_array) {
                    $IsFound = false;
                    for ($i = 0; $i < count($features_multi_array); $i++) {
                        if ($features_multi_array[$i]['sku'] == $key) {
                            $features_multi_array[$i]['option'] = $value_array['option'];
                            $features_multi_array[$i]['option_std_unit_price'] = $value_array['std_unit_price'];
                            $IsFound = true;
                            break;
                            //Util::deb( $features_multi_array, '????? $features_multi_array::' );
                        }
                    }
                    if (!$IsFound) {
                        $img = get_image_by_sku($key);
                        $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => '', 'option_std_unit_price' => $value_array['std_unit_price'], 'option' => $value_array['option'], 'hand' => '', 'finish' => '', 'image' => $img);
                    }
                    $result_options_list[$key] = $value_array['option']; // . (  $HasDifferentPrices ? ' ( $' . $value_array['std_unit_price'] .' )' : '' );
                } // foreach ($OptionsList as $key => $value_array) {
                echo '<li id="li_option" >';
                echo '<b>Option:</b><br />' . Util::select_tag('select_option', Util::SetArrayHeader(array('' => ' -select- '), /* $result_options_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'option\'); return false;'));
                echo '</li>';
            } // if (!empty($OptionsList)) {
        } //if ($NextFeature == 'options') {

        if ($NextFeature == 'hand') {
            // break;
            $HandsList = InventoryItemPeer::getHandsListByRootItem($RootItem);
            // Util::deb( $HandsList, '$HandsList::' );
            /* if ($sf_request->getParameter('debug') == '9' ) {
              echo '<pre> $HandsList::'.print_r( $HandsList, true ).'</pre>';
              } */
            if (!empty($HandsList)) {
                $result_hands_list = array();
                foreach ($HandsList as $key => $value_array) {
                    $IsFound = false;
                    for ($i = 0; $i < count($features_multi_array); $i++) {
                        if ($features_multi_array[$i]['sku'] == $key) {
                            $features_multi_array[$i]['hand'] = $value_array['hand'];
                            $features_multi_array[$i]['hand_std_unit_price'] = $value_array['std_unit_price'];
                            $IsFound = true;
                            break;
                            //Util::deb( $features_multi_array, '????? $features_multi_array::' );
                        }
                    }
                    if (!$IsFound) {
                        $img = get_image_by_sku($key);
                        $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => '', 'option' => '', 'hand' => $value_array['hand'], 'hand_std_unit_price' => $value_array['std_unit_price'], 'image' => $img);
                    }
                    $result_hands_list[$key] = $value_array['hand']; // . (  $HasDifferentPrices ? ' ( $' . $value_array['std_unit_price'] .' )' : '' );
                } // foreach ($HandsList as $key => $value_array) {
                echo '<li id="li_hand" >';
                echo '<b>Hand:</b><br />' . Util::select_tag('select_hand', Util::SetArrayHeader(array('' => ' -select- '), /* $result_hands_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'hand\'); return false;'));
                echo '</li>';
            } // if (!empty($HandsList)) {
        } //if ($NextFeature == 'hand') {

        if ($NextFeature == 'finish') {
            // break;
            $FinishesList = InventoryItemPeer::getFinishesListByRootItem($RootItem);
            // Util::deb( $FinishesList, '$FinishesList::' );
            /* if ($sf_request->getParameter('debug') == '9' ) {
              echo '<pre> $FinishesList::'.print_r( $FinishesList, true ).'</pre>';
              } */
            if (!empty($FinishesList)) {
                $result_finishes_list = array();
                foreach ($FinishesList as $key => $value_array) {
                    $IsFound = false;
                    for ($i = 0; $i < count($features_multi_array); $i++) {
                        if ($features_multi_array[$i]['sku'] == $key) {
                            $features_multi_array[$i]['finish'] = $value_array['finish'];
                            $features_multi_array[$i]['finish_std_unit_price'] = $value_array['std_unit_price'];
                            $IsFound = true;
                            break;
                            //Util::deb( $features_multi_array, '????? $features_multi_array::' );
                        }
                    }
                    if (!$IsFound) {
                        $img = get_image_by_sku($key);
                        $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => '', 'option' => '', 'hand' => '', 'finish' => $value_array['finish'], 'finish_std_unit_price' => $value_array['std_unit_price'], 'image' => $img);
                    }
                    $result_finishes_list[$key] = $value_array['finish']; // . (  $HasDifferentPrices ? ' ( $' . $value_array['std_unit_price'] .' )' : '' );
                } // foreach ($FinishesList as $key => $value_array) {
                echo '<li id="li_finish" >';
                echo '<b>Finish:</b><br />' . Util::select_tag('select_finish', Util::SetArrayHeader(array('' => ' -select- '), /* $result_finishes_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'finish\'); return false;'));
                echo '</li>';
            } // if (!empty($FinishesList)) {
        } //if ($NextFeature == 'finish') {


        if (!in_array('size', $FeaturesList)) {
            echo '<li>';
            if (!empty($Sizing)) {

                $tb_width = 850;
                $tb_height = 800;
                $lSizingChart = SizingChartPeer::retrieveByPK($Sizing);
                if (!empty($lSizingChart)) {
                    $SizingChartsDirectory = sfConfig::get('app_application_SizingChartsDirectory');
                    $SizingChartFilename = $SizingChartsDirectory . DIRECTORY_SEPARATOR . $lSizingChart->getFilename();
                    $FileArray = @getimagesize(str_replace("\\", "/", sfConfig::get('sf_root_dir') . Util::getSiteRootDir() . $SizingChartFilename));
                    if (!empty($FileArray[0])) {
                        $tb_width = $FileArray[0] + 20;
                    }
                    if (!empty($FileArray[1])) {
                        $tb_height = $FileArray[1] + 60;
                    }
                    echo '<span>';
                    $tb_height = 'ZZZZZ';
                    $Src = '<a id="a_tb_size_chart" class="thickbox" href="' . url_for("@product_size_chart?code=" . $Sizing) . '?width=' . $tb_width . '&height=' . $tb_height . '">Size Chart</a> ';
                    echo $Src;
                    echo '</span>';
                } // if ( !empty($lSizingChart) ) {
                //Util::deb( $FileArray, '$FileArray::' );
            }
            echo '</li><br />';
        }
        ?>

    <?php endforeach;   // Util::deb( $features_multi_array, 'LAST $features_multi_array::' );
    ?>
</ul>

<script type="text/javascript">
    var is_debug = false
    var IsSingleFeaturesList = <?php echo ( count($FeaturesList) > 1 ? "false" : "true" ) ?>;
    var CurrentStdUnitPrice = <?php echo (!empty($StdUnitPrice) ? $StdUnitPrice : 0 ) ?>

    var is_InitialLoading = true
    jQuery(function($) {
        repopulate_sizes('', '', '', '', '', '');
        repopulate_colors('', '', '', '', '', '');
        repopulate_options('', '', '', '', '', '');
        repopulate_hands('', '', '', '', '', '');
        repopulate_finishes('', '', '', '', '', ''); // repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )
        is_InitialLoading = false

        if (document.getElementById("a_tb_size_chart")) {
            var OldHRef = document.getElementById("a_tb_size_chart").href
            var WindowInnerHeight = window.innerHeight
            var NewWindowInnerHeight = parseInt(WindowInnerHeight * 0.9)
            var NewHRef = OldHRef.replace(/ZZZZZ/g, NewWindowInnerHeight);
            document.getElementById("a_tb_size_chart").href = NewHRef
        }
    });

    var featureses = jQuery.parseJSON(<?php /* Util::deb( $features_multi_array, '$features_multi_array::' );    die("!!!"); */ print json_encode(json_encode($features_multi_array)); ?>);
    var FeaturesList = jQuery.parseJSON(<?php print json_encode(json_encode($FeaturesList)); ?>);

    // if ( is_debug ) alert(  "FeaturesList::"+var_dump(FeaturesList)  )
    function SelectFeatures(type) {
        is_debug = false;  //document.getElementById("is_debug").checked
        // alert("SelectFeatures type::"+type+"  is_debug::"+is_debug)
        var sku = '';
        var color = '';
        var option = '';
        var hand = '';
        var finish = '';
        var image = '';
        if (type == 'size') {
            sku = jQuery('#select_size_ option:selected').val();
            var size = jQuery('#select_size_ option:selected').text();

            option = jQuery('#select_option option:selected').text();
            option = jQuery.trim(option);
            hand = jQuery('#select_hand option:selected').text();
            hand = jQuery.trim(hand);
            finish = jQuery('#select_finish option:selected').text();
            finish = jQuery.trim(finish);

            if (sku != '') {
                repopulate_colors(sku, size, option, hand, finish, 'size');  // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_options(sku, size, option, hand, finish, 'size'); // repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType )
                repopulate_hands(sku, size, color, option, finish, 'size');  // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType )
                repopulate_finishes(sku, size, color, option, hand, 'size'); // repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )

                for (i = 0; i < featureses.length; i++) {
                    if (featureses[i].sku == sku) {
                        color = featureses[i].color;
                        image = featureses[i].image;
                    }
                }
                jQuery('#sku').html('Item# ' + sku);
                if (image != '') {
                    var img_product = '<a href="' + image + '">';
                    img_product += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
                    img_product += '</a>';
                    jQuery('#product_photo_div').html(img_product);
                }
                for (I = 0; I < featureses.length; I++) {
                    if (sku == featureses[I]["sku"]) {
                        document.getElementById("span_CurrentStdUnitPrice").innerHTML = featureses[I]["size_std_unit_price"]
                        CurrentStdUnitPrice = featureses[I]["size_std_unit_price"]
                        break;
                    }
                }
                //alert( "IS SET SIZE CurrentStdUnitPrice::"+CurrentStdUnitPrice )
            }
            else {
                repopulate_colors('', '', '', '', '', 'size');
                repopulate_options('', '', '', '', '', 'size');
                repopulate_hands('', '', '', '', '', 'size');
                repopulate_finishes('', '', '', '', '', 'size'); //   repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )
            }
        } // if (type == 'size') {

        if (type == 'color') {
            color = jQuery('#select_color option:selected').text();
            sku = jQuery('#select_color option:selected').val();
            color = jQuery.trim(color);

            size = jQuery('#select_size_ option:selected').text();
            size = jQuery.trim(size);

            option = jQuery('#select_option option:selected').text();
            option = jQuery.trim(option);
            hand = jQuery('#select_hand option:selected').text();
            hand = jQuery.trim(hand);

            finish = jQuery('#select_finish option:selected').text();
            finish = jQuery.trim(finish);

            if (color != jQuery.trim('-select-')) {
                repopulate_sizes(sku, color, option, hand, finish, 'color'); // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_options(sku, size, option, hand, finish, 'color'); //  repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType )
                repopulate_hands(sku, size, color, option, finish, 'color'); // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType )
                repopulate_finishes(sku, size, color, option, hand, 'color'); // repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )

                for (i = 0; i < featureses.length; i++) {
                    // alert( "featureses[i].color::"+ featureses[i].color + "  color::"+color)
                    if (featureses[i].sku == sku) {
                        image = featureses[i].image;
                    }
                }
                // $("#select_size_ :nth-child(2)").attr("selected", "selected");
                // sku = $("#select_size_ :nth-child(2)").val();
                jQuery('#sku').html('Item# ' + sku);
                if (image != '') {
                    var img_product = '<a href="' + image + '">';
                    img_product += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
                    img_product += '</a>';
                    jQuery('#product_photo_div').html(img_product);
                }

                for (I = 0; I < featureses.length; I++) {
                    if (sku == featureses[I]["sku"]) {
                        document.getElementById("span_CurrentStdUnitPrice").innerHTML = featureses[I]["color_std_unit_price"]
                        CurrentStdUnitPrice = featureses[I]["color_std_unit_price"]
                        break;
                    }
                }
                // alert( "IS SET COLOR CurrentStdUnitPrice::"+CurrentStdUnitPrice )

            }
            else {
                // repopulate_colors();
                repopulate_sizes('', '', '', '', '', 'color');
                repopulate_options('', '', '', '', '', 'color');
                repopulate_hands('', '', '', '', '', 'color');
                repopulate_finishes('', '', '', '', '', 'color');
            }
        } // if (type == 'color') {


        if (type == 'option') {
            option = jQuery('#select_option option:selected').text();
            sku = jQuery('#select_option option:selected').val();
            option = jQuery.trim(option);

            size = jQuery('#select_size_ option:selected').text();
            size = jQuery.trim(size);

            option = jQuery('#select_option option:selected').text();
            option = jQuery.trim(option);
            hand = jQuery('#select_hand option:selected').text();
            hand = jQuery.trim(hand);
            finish = jQuery('#select_finish option:selected').text();
            finish = jQuery.trim(finish);

            if (option != jQuery.trim('-select-')) {
                repopulate_sizes(sku, color, option, hand, finish, 'option');  // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_colors(sku, size, option, hand, finish, 'option');  // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_hands(sku, size, color, option, finish, 'option');  // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType )
                repopulate_finishes(sku, size, color, option, hand, 'option');  // repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )

                /*Alexander Shkurko. 130729.*/
                for (i = 0; i < featureses.length; i++) {
                    if (featureses[i].size == size && featureses[i].hand == hand && featureses[i].finish == finish) {
                        image = featureses[i].image;
                        sku = featureses[i].sku;
                    }
                }
//                for (i = 0; i < featureses.length; i++) {
//                    if (featureses[i].sku == sku) {
//                        image = featureses[i].image;
//                    }
//                }
                jQuery('#sku').html('Item# ' + sku);
                if (image != '') {
                    var img_product = '<a href="' + image + '">';
                    img_product += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
                    img_product += '</a>';
                    jQuery('#product_photo_div').html(img_product);
                }

                for (I = 0; I < featureses.length; I++) {
                    if (sku == featureses[I]["sku"]) {
                        document.getElementById("span_CurrentStdUnitPrice").innerHTML = featureses[I]["option_std_unit_price"]
                        CurrentStdUnitPrice = featureses[I]["option_std_unit_price"]
                        break;
                    }
                }

            }
            else {
                repopulate_sizes('', '', '', '', '', 'option');
                repopulate_colors('', '', '', '', '', 'option');
                repopulate_hands('', '', '', '', '', 'option');
                repopulate_finishes('', '', '', '', '', 'option');
            }
        } // if (type == 'option') {

        if (type == 'hand') {
            hand = jQuery('#select_hand option:selected').text();
            sku = jQuery('#select_hand option:selected').val();
            hand = jQuery.trim(hand);

            size = jQuery('#select_size_ option:selected').text();
            size = jQuery.trim(size);

            option = jQuery('#select_option option:selected').text();
            option = jQuery.trim(option);
            color = jQuery('#select_color option:selected').text();
            color = jQuery.trim(color);

            finish = jQuery('#select_finish option:selected').text();
            finish = jQuery.trim(finish);
            if (hand != jQuery.trim('-select-')) {
                repopulate_sizes(sku, color, option, hand, finish, 'hand');  // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_colors(sku, size, option, hand, finish, 'hand');  // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_options(sku, size, color, hand, finish, 'hand'); //  repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType )
                repopulate_finishes(sku, size, color, option, hand, 'hand'); // repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )
                /*Alexander Shkurko. 130726.*/               
                for (i = 0; i < featureses.length; i++) {
                    if (featureses[i].size == size && featureses[i].hand == hand && featureses[i].finish == finish) {
                        image = featureses[i].image;
                        sku = featureses[i].sku;
                    }
                }
                /*Alexander Shkurko. 130726.*/
                //        for (i = 0; i < featureses.length; i++) {
                //          if (featureses[i].sku == sku) {
                //            image = featureses[i].image;
                //          }
                //        }
                        jQuery('#sku').html('Item# ' + sku);
                if (image != '') {
                    var img_product = '<a href="' + image + '">';
                    img_product += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
                    img_product += '</a>';
                    jQuery('#product_photo_div').html(img_product);
                }

                for (I = 0; I < featureses.length; I++) {
                    if (sku == featureses[I]["sku"]) {
                        document.getElementById("span_CurrentStdUnitPrice").innerHTML = featureses[I]["hand_std_unit_price"]
                        CurrentStdUnitPrice = featureses[I]["hand_std_unit_price"]
                        break;
                    }
                }

            }
            else {
                repopulate_sizes('', '', '', '', '', 'hand');
                repopulate_colors('', '', '', '', '', 'hand');
                repopulate_options('', '', '', '', '', 'hand');
                repopulate_finishes('', '', '', '', '', 'hand');
            }
        } // if (type == 'hand') {


        if (type == 'finish') {
            hand = jQuery('#select_hand option:selected').text();
            sku = jQuery('#select_finish option:selected').val();
            hand = jQuery.trim(hand);

            size = jQuery('#select_size_ option:selected').text();
            size = jQuery.trim(size);

            option = jQuery('#select_option option:selected').text();
            option = jQuery.trim(option);
            color = jQuery('#select_color option:selected').text();
            color = jQuery.trim(color);

            finish = jQuery('#select_finish option:selected').text();
            finish = jQuery.trim(finish);
            if (finish != jQuery.trim('-select-')) {
                repopulate_sizes(sku, color, option, hand, finish, 'finish');  // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_colors(sku, size, option, hand, finish, 'finish');  // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType )
                repopulate_options(sku, size, color, hand, finish, 'finish'); //  repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType )
                repopulate_hands(sku, size, color, option, finish, 'finish');  // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType )


               /*Alexander Shkurko. 130729.*/
                for (i = 0; i < featureses.length; i++) {
                    if (featureses[i].size == size && featureses[i].hand == hand && featureses[i].finish == finish) {
                        image = featureses[i].image;
                        sku = featureses[i].sku;
                    }
                }

//                for (i = 0; i < featureses.length; i++) {
//                    if (featureses[i].sku == sku) {
//                        image = featureses[i].image;
//                    }
//                }
                jQuery('#sku').html('Item# ' + sku);
                if (image != '') {
                    var img_product = '<a href="' + image + '">';
                    img_product += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
                    img_product += '</a>';
                    jQuery('#product_photo_div').html(img_product);
                }

                for (I = 0; I < featureses.length; I++) {
                    if (sku == featureses[I]["sku"]) {
                        document.getElementById("span_CurrentStdUnitPrice").innerHTML = featureses[I]["finish_std_unit_price"]
                        CurrentStdUnitPrice = featureses[I]["finish_std_unit_price"]
                        break;
                    }
                }

            }
            else {
                repopulate_sizes('', '', '', '', '', 'finish');
                repopulate_colors('', '', '', '', '', 'finish');
                repopulate_options('', '', '', '', '', 'finish');
                repopulate_hands('', '', '', '', '', 'finish');
            }
        } // if (type == 'finish') {

    } // function SelectFeatures(type) {

    function repopulate_colors(SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType)
    {
        var is_debug = false
        var temp_colors = jQuery.parseJSON(<?php print json_encode(json_encode($result_colors_list)); ?>);
        // alert("is_debug::"+is_debug)
        if (is_debug)
            alert(" repopulate_colors  temp_colors::" + var_dump(temp_colors) + "  SelectedSku::" + SelectedSku + "  SelectedSize::" + SelectedSize + "  SelectedOption::" + SelectedOption +
                    "  SelectedHand::" + SelectedHand + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
        var rep_colors_list = '<option value=""> -select-</option>';
        var filled_colors = Array();
        var CurrentColorValue = jQuery('#select_color option:selected').text();
        // alert( "CurrentColorValue::"+CurrentColorValue )
        RelatedSizesList = getRelatedSizesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_colors RelatedSizesList::" + var_dump(RelatedSizesList))
        RelatedHandsList = getRelatedHandsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_colors RelatedHandsList::" + var_dump(RelatedHandsList))
        RelatedOptionsList = getRelatedOptionsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_colors RelatedOptionsList::" + var_dump(RelatedOptionsList))
        RelatedFinishesList = getRelatedFinishesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_colors RelatedFinishesList::" + var_dump(RelatedFinishesList))

        jQuery.each(temp_colors, function(i, val) {
            if (is_debug)
                alert("++ i::" + i + "    val::" + val)
            var IsToAddSize = ((getIndexOf(FeaturesList, "size", false) > -1) || (getIndexOf(FeaturesList, "sizes", false) > -1));
            if (is_debug)
                alert("repopulate_colors IsToAddSize: " + IsToAddSize);
            if (/* (  SelectedSize!= "" || SelectionType == '' ) && */  IsToAddSize) {
                IsToAddSize = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == '61166-019-XL') {
                        if (is_debug)
                            alert(" -1 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedSize::" + SelectedSize + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedSizesList::" + var_dump(RelatedSizesList));
                    }
                    if (i == featureses[J].sku && ((getIndexOf(RelatedSizesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddSize = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_colors AFTER IsToAddSize: " + IsToAddSize);

            var IsToAddOption = ((getIndexOf(FeaturesList, "option", false) > -1) || (getIndexOf(FeaturesList, "options", false) > -1));
            if (is_debug)
                alert("repopulate_colors IsToAddOption: " + IsToAddOption);
            if (/* ( SelectedOption!= "" || SelectionType == '' ) &&  */ IsToAddOption) {
                IsToAddOption = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == '61166-019-XL') {
                        if (is_debug)
                            alert(" -11 Options ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedSize::" + SelectedSize + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedOptionsList::" + var_dump(RelatedOptionsList) + " getIndexOf(RelatedOptionsList,i,false )::" + getIndexOf(RelatedOptionsList, i, false));
                    }
                    if (i == featureses[J].sku && ((getIndexOf(RelatedOptionsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {

                        if (is_debug)
                            alert(" INSIDE -11 Options ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedSize::" + SelectedSize + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedOptionsList::" + var_dump(RelatedOptionsList));
                        IsToAddOption = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_colors AFTER IsToAddOption: " + IsToAddOption);

            var IsToAddHand = ((getIndexOf(FeaturesList, "hand", false) > -1) || (getIndexOf(FeaturesList, "hands", false) > -1));
            if (is_debug)
                alert("repopulate_colors IsToAddHand: " + IsToAddHand);
            if (/* ( SelectedHand!= "" || SelectionType == '' ) &&  */ IsToAddHand) {
                IsToAddHand = false
                for (J = 0; J < featureses.length; J++) {

                    if (is_debug)
                        alert(" -100 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedOption::" + SelectedOption + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + "  RelatedHandsList::" + var_dump(RelatedHandsList));

                    if (i == featureses[J].sku && ((getIndexOf(RelatedHandsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {

                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::" + SelectionType)

                        IsToAddHand = true;
                        break;
                    }

                }
            }
            if (is_debug)
                alert("repopulate_colors AFTER IsToAddHand: " + IsToAddHand);


            var IsToAddFinish = ((getIndexOf(FeaturesList, "finish", false) > -1) || (getIndexOf(FeaturesList, "finishes", false) > -1));
            if (is_debug)
                alert("repopulate_colors IsToAddFinish: " + IsToAddFinish);
            if (/* ( SelectedFinish!= "" || SelectionType == '' ) &&  */ IsToAddFinish) {
                IsToAddFinish = false
                for (J = 0; J < featureses.length; J++) {

                    if (is_debug)
                        alert(" -100 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedOption::" + SelectedOption + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + "  RelatedFinishesList::" + var_dump(RelatedFinishesList));

                    if (i == featureses[J].sku && ((getIndexOf(RelatedFinishesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {

                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].finish::" + featureses[J].finish +
                                    "  SelectedHand::" + SelectedHand + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)

                        IsToAddFinish = true;
                        break;
                    }

                }
            }
            if (is_debug)
                alert("repopulate_colors AFTER IsToAddFinish: " + IsToAddFinish);


            var IsFilled = false
            for (J = 0; J < filled_colors.length; J++) {
                if (filled_colors[J] == val) {
                    IsFilled = true;
                    break;
                }
            }
            //alert( "val::" + val + "   CurrentColorValue::" + CurrentColorValue )
            if (is_debug)
                alert("repopulate_colors i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedOption::" + SelectedOption + "  IsFilled::" + IsFilled + "  IsToAddSize::" + IsToAddSize
                        + "  IsToAddOption::" + IsToAddOption + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish + "   IsSingleFeaturesList::" + IsSingleFeaturesList)
            if (!IsFilled && (IsToAddSize || IsToAddOption || IsToAddHand || IsToAddFinish || IsSingleFeaturesList)) {
                if (is_debug)
                    alert("INSIDE repopulate_colors !!! i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedOption::" + SelectedOption + "  IsFilled::" + IsFilled +
                            "  IsToAddSize::" + IsToAddSize + "  IsToAddOption::" + IsToAddOption + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish)
                var SelectedTag = ""
                if (CurrentColorValue == val) {
                    SelectedTag = " selected "
                }
                rep_colors_list += '<option value="' + i + '" ' + SelectedTag + ' >' + val + '</option>';
                filled_colors[filled_colors.length] = val;
            }
            if (is_debug)
                alert(" repopulate_colors END i::" + i + "  rep_colors_list::" + rep_colors_list)
        });
        jQuery('#select_color').html(rep_colors_list);
    } // function repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectedFinish, SelectionType )

    function repopulate_sizes(SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType)
    {
        // var is_debug= true
        var temp_sizes = jQuery.parseJSON(<?php print json_encode(json_encode($result_size_list)); ?>);
        if (is_debug)
            alert("repopulate_sizes temp_sizes::" + var_dump(temp_sizes) + "  SelectedSku::" + SelectedSku + "  SelectedColor::" + SelectedColor + "  SelectedOption::" + SelectedOption + "  SelectedHand::" + SelectedHand +
                    "  SelectionType::" + SelectionType)
        var rep_sizes_list = '<option value=""> -select-</option>';
        var filled_sizes = Array();
        //alert(-1)
        var CurrentSizeValue = jQuery('#select_size_ option:selected').text();      // alert( "CurrentSizeValue::"+CurrentSizeValue )
        RelatedColorsList = getRelatedColorsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_sizes RelatedColorsList::" + var_dump(RelatedColorsList))
        RelatedHandsList = getRelatedHandsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_sizes RelatedHandsList::" + var_dump(RelatedHandsList))
        RelatedOptionsList = getRelatedOptionsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_sizes RelatedOptionsList::" + var_dump(RelatedOptionsList))
        RelatedFinishesList = getRelatedFinishesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_sizes RelatedFinishesList::" + var_dump(RelatedFinishesList))

        jQuery.each(temp_sizes, function(i, val) {
            if (is_debug)
                alert("++ i::" + i + "    val::" + val)
            var IsToAddColor = ((getIndexOf(FeaturesList, "color", false) > -1) || (getIndexOf(FeaturesList, "colors", false) > -1));
            if (is_debug)
                alert("repopulate_sizes IsToAddColor: " + IsToAddColor + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType);
            if (/*( SelectedColor!= "" || SelectionType == '' ) && */ IsToAddColor) {
                IsToAddColor = false
                if (is_debug)
                    alert(-1)
                for (J = 0; J < featureses.length; J++) {
                    if (is_debug)
                        alert(-2)
                    if (is_debug)
                        alert(" -13 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedColorsList::" + var_dump(RelatedColorsList));
                    if (i == featureses[J].sku && ((getIndexOf(RelatedColorsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert(" -13 INSIDE ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedColorsList::" + var_dump(RelatedColorsList));
                        IsToAddColor = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_sizes AFTER IsToAddColor: " + IsToAddColor);

            var IsToAddOption = ((getIndexOf(FeaturesList, "option", false) > -1) || (getIndexOf(FeaturesList, "options", false) > -1));
            if (is_debug)
                alert("repopulate_sizes IsToAddOption: " + IsToAddOption);
            if (/* ( SelectedOption!= "" || SelectionType == '' ) && */ IsToAddOption) {
                IsToAddOption = false
                for (J = 0; J < featureses.length; J++) {
                    // if ( i == '61166-019-XL' ) {
                    if (is_debug)
                        alert(" -12 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedOptionsList::" + var_dump(RelatedOptionsList));
                    // }
                    if (i == featureses[J].sku && ((getIndexOf(RelatedOptionsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert(" -12 INSIDE ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku + " RelatedOptionsList::" + var_dump(RelatedOptionsList));
                        IsToAddOption = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_sizes AFTER IsToAddOption: " + IsToAddOption);

            var IsToAddHand = ((getIndexOf(FeaturesList, "hand", false) > -1) || (getIndexOf(FeaturesList, "hands", false) > -1));
            if (is_debug)
                alert("repopulate_sizes IsToAddHand: " + IsToAddHand);
            if (/* ( SelectedHand!= "" || SelectionType == '' ) &&  */ IsToAddHand) {
                IsToAddHand = false
                for (J = 0; J < featureses.length; J++) {
                    // if ( is_debug ) alert( " -14 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::"+SelectionType + "  SelectedSku::"+SelectedSku +" RelatedHandsList::"+var_dump(RelatedHandsList) );
                    if (i == featureses[J].sku && ((getIndexOf(RelatedHandsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        // if ( is_debug ) alert( " -14 INSIDE ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::"+SelectionType + "  SelectedSku::"+SelectedSku +" RelatedHandsList::"+var_dump(RelatedHandsList) );
                        IsToAddHand = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_sizes AFTER IsToAddHand: " + IsToAddHand);


            var IsToAddFinish = ((getIndexOf(FeaturesList, "finish", false) > -1) || (getIndexOf(FeaturesList, "finishes", false) > -1));
            if (is_debug)
                alert("repopulate_sizes IsToAddFinish: " + IsToAddFinish);
            if (/* ( SelectedFinish!= "" || SelectionType == '' ) &&  */ IsToAddFinish) {
                IsToAddFinish = false
                for (J = 0; J < featureses.length; J++) {
                    // if ( is_debug ) alert( " -14 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::"+SelectionType + "  SelectedSku::"+SelectedSku + " RelatedHandsList::" + var_dump(RelatedHandsList + " RelatedFinishesList::" + var_dump(RelatedFinishesList ) );
                    if (i == featureses[J].sku && ((getIndexOf(RelatedFinishesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        // if ( is_debug ) alert( " -14 INSIDE ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedColor::" + SelectedColor + "  SelectionType::"+SelectionType + "  SelectedSku::"+SelectedSku +" RelatedFinishesList::"+var_dump(RelatedFinishesList) );
                        IsToAddFinish = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("repopulate_sizes AFTER IsToAddFinish: " + IsToAddFinish);


            var IsFilled = false
            for (J = 0; J < filled_sizes.length; J++) {
                if (filled_sizes[J] == val) {
                    IsFilled = true;
                    break;
                }
            }
            if (is_debug)
                alert("i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddColor::" + IsToAddColor
                        + "  IsToAddOption::" + IsToAddOption + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish + "   IsSingleFeaturesList::" + IsSingleFeaturesList)
            if (!IsFilled && (IsToAddColor || IsToAddOption || IsToAddHand || IsToAddFinish || IsSingleFeaturesList)) {
                if (is_debug)
                    alert("INSIDE !!! i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddColor::" + IsToAddColor
                            + "  IsToAddOption::" + IsToAddOption + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish)
                var SelectedTag = ""
                // alert( "val::" + val + "   CurrentSizeValue::" + CurrentSizeValue )
                if (CurrentSizeValue == val) {
                    SelectedTag = " selected "
                }
                rep_sizes_list += '<option value="' + i + '" ' + SelectedTag + ' >' + val + '</option>';
                filled_sizes[filled_sizes.length] = val;
            }
            if (is_debug)
                alert(" END rep_sizes_list::" + rep_sizes_list)
        });
        jQuery('#select_size_').html(rep_sizes_list);
    } // function repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectedFinish, SelectionType )

    function repopulate_options(SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType)
    {
        var is_debug = false
        var temp_options = jQuery.parseJSON(<?php print json_encode(json_encode($result_options_list)); ?>);
        if (is_debug)
            alert("repopulate_options temp_options::" + var_dump(temp_options) + "  SelectedSku::" + SelectedSku + "  SelectedSize::" + SelectedSize + "  SelectedColor::" + SelectedColor +
                    "  SelectedHand::" + SelectedHand + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
        var rep_options_list = '<option value=""> -select-</option>';
        var filled_options = Array();
        var CurrentOptionValue = jQuery('#select_option_ option:selected').text();
        if (is_debug)
            alert("CurrentOptionValue::" + CurrentOptionValue)

        RelatedSizesList = getRelatedSizesList(SelectedSku, SelectionType)        //  if ( is_debug ) alert( "repopulate_options RelatedSizesList::"+var_dump(RelatedSizesList) )
        RelatedColorsList = getRelatedColorsList(SelectedSku, SelectionType)      //  if ( is_debug ) alert( "repopulate_options RelatedColorsList::"+var_dump(RelatedColorsList) )
        RelatedHandsList = getRelatedHandsList(SelectedSku, SelectionType)        //  if ( is_debug ) alert( "repopulate_options RelatedHandsList::"+var_dump(RelatedHandsList) )
        RelatedFinishesList = getRelatedFinishesList(SelectedSku, SelectionType)  //  if ( is_debug ) alert( "repopulate_options RelatedFinishesList::"+var_dump(RelatedFinishesList) )
        jQuery.each(temp_options, function(i, val) {
            if (is_debug)
                alert("++ i::" + i + "    val::" + val)
            var IsToAddSize = ((getIndexOf(FeaturesList, "size", false) > -1) || (getIndexOf(FeaturesList, "sizes", false) > -1));
            if (is_debug)
                alert("IsToAddSize: " + IsToAddSize);
            if (/* (  SelectedSize!= "" ||  SelectionType == '' ) && */ IsToAddSize) {
                IsToAddSize = false
                for (J = 0; J < featureses.length; J++) {
                    if (is_debug)
                        alert(" -1 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedSize::" + SelectedSize + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku);
                    if (i == featureses[J].sku && ((getIndexOf(RelatedSizesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand +
                                    "  SelectedHand::" + SelectedHand + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
                        IsToAddSize = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("A IsToAddSize: " + IsToAddSize);

            var IsToAddColor = ((getIndexOf(FeaturesList, "color", false) > -1) || (getIndexOf(FeaturesList, "colors", false) > -1));
            if (is_debug)
                alert("IsToAddColor: " + IsToAddColor);
            if (/*(  SelectedColor!= "" ||  SelectionType == '' ) && */ IsToAddColor) {
                IsToAddColor = false
                for (J = 0; J < featureses.length; J++) {
                    if (is_debug)
                        alert("  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].color::" + featureses[J].color + "  SelectedColor::" + SelectedColor + "  SelectionType::" + SelectionType);
                    // if ( i== featureses[J].sku && featureses[J].color != SelectedColor  ) {
                    //if ( i== SelectedSku && SelectionType == 'color'  ) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedColorsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].color::" + featureses[J].color + "  SelectedHand::" + SelectedHand +
                                    "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
                        IsToAddColor = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("A IsToAddColor: " + IsToAddColor);

            var IsToAddHand = ((getIndexOf(FeaturesList, "hand", false) > -1) || (getIndexOf(FeaturesList, "hands", false) > -1));
            if (is_debug)
                alert("IsToAddHand: " + IsToAddHand);
            if (/* ( SelectedHand!= "" ||  SelectionType == '' ) && */ IsToAddHand) {
                IsToAddHand = false
                for (J = 0; J < featureses.length; J++) {
                    if (is_debug)
                        alert(" -2 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku);
                    if (i == featureses[J].sku && ((getIndexOf(RelatedHandsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::" + SelectionType)
                        IsToAddHand = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("A IsToAddHand: " + IsToAddHand);


            var IsToAddFinish = ((getIndexOf(FeaturesList, "finish", false) > -1) || (getIndexOf(FeaturesList, "finishes", false) > -1));
            if (is_debug)
                alert("IsToAddFinish: " + IsToAddFinish);
            if (/* ( SelectedFinish!= "" ||  SelectionType == '' ) && */ IsToAddFinish) {
                IsToAddFinish = false
                for (J = 0; J < featureses.length; J++) {
                    if (is_debug)
                        alert(" -2 ??? i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].finish::" + featureses[J].finish + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType + "  SelectedSku::" + SelectedSku);
                    if (i == featureses[J].sku && ((getIndexOf(RelatedFinishesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        if (is_debug)
                            alert('BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].finish::" + featureses[J].finish + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
                        IsToAddFinish = true;
                        break;
                    }
                }
            }
            if (is_debug)
                alert("A IsToAddFinish: " + IsToAddFinish);

            var IsFilled = false
            for (J = 0; J < filled_options.length; J++) {
                if (filled_options[J] == val) {
                    IsFilled = true;
                    break;
                }
            }

            if (is_debug)
                alert("i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddSize::" + IsToAddSize
                        + "  IsToAddColor::" + IsToAddColor + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish + "   IsSingleFeaturesList::" + IsSingleFeaturesList)
            if (!IsFilled && (IsToAddSize || IsToAddColor || IsToAddHand || IsToAddFinish || IsSingleFeaturesList)) {

                if (is_debug)
                    alert("INSIDE i::" + i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddSize::" + IsToAddSize
                            + "  IsToAddColor::" + IsToAddColor + "  IsToAddHand::" + IsToAddHand + "  IsToAddFinish::" + IsToAddFinish)

                var SelectedTag = ""
                if (is_debug)
                    alert("val::" + val + "   CurrentOptionValue::" + CurrentOptionValue)
                if (CurrentOptionValue == val) {
                    SelectedTag = " selected "
                }
                rep_options_list += '<option value="' + i + '" ' + SelectedTag + ' >' + val + '</option>';
                filled_options[filled_options.length] = val;
                if (is_debug)
                    alert(" END! rep_options_list::" + rep_options_list + "  filled_options::" + var_dump(filled_options))
            }
        });
        if (is_debug)
            alert(" rep_options_list::" + rep_options_list)
        jQuery('#select_option').html(rep_options_list);
    } // function repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectedFinish, SelectionType )

    function repopulate_hands(SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType)
    {
        //var is_debug= false
        var temp_hands = jQuery.parseJSON(<?php print json_encode(json_encode($result_hands_list)); ?>);
        if (is_debug)
            alert("repopulate_hands  temp_hands::" + var_dump(temp_hands) + "  SelectedSku::" + SelectedSku + "  SelectedSize::" + SelectedSize + "  SelectedColor::" + SelectedColor
                    + "  SelectedOption::" + SelectedOption + "  SelectedFinish::" + SelectedFinish + "  SelectionType::" + SelectionType)
        var rep_hands_list = '<option value=""> -select-</option>';
        var filled_hands = Array();
        RelatedSizesList = getRelatedSizesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_hands RelatedSizesList::" + var_dump(RelatedSizesList))
        RelatedColorsList = getRelatedColorsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_hands RelatedColorsList::" + var_dump(RelatedColorsList))
        RelatedOptionsList = getRelatedOptionsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_hands RelatedOptionsList::" + var_dump(RelatedOptionsList))
        RelatedFinishesList = getRelatedFinishesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_hands RelatedFinishesList::" + var_dump(RelatedFinishesList))

        var CurrentHandValue = jQuery('#select_hand option:selected').text();
        if (is_debug)
            alert("CurrentHandValue::" + CurrentHandValue)

        jQuery.each(temp_hands, function(i, val) {

            var IsToAddSize = ((getIndexOf(FeaturesList, "size", false) > -1) || (getIndexOf(FeaturesList, "sizes", false) > -1));
            if (/* ( SelectedSize!= "" || SelectionType == '' ) && */ IsToAddSize) {
                IsToAddSize = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedSizesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddSize = true;
                        break;
                    }
                }
            }

            var IsToAddColor = ((getIndexOf(FeaturesList, "color", false) > -1) || (getIndexOf(FeaturesList, "colors", false) > -1));
            if (/* ( SelectedColor!= "" || SelectionType == '' ) && */ IsToAddColor) {
                IsToAddColor = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedColorsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddColor = true;
                        break;
                    }
                }
            }

            var IsToAddOption = ((getIndexOf(FeaturesList, "option", false) > -1) || (getIndexOf(FeaturesList, "options", false) > -1))
            if (/* ( SelectedOption!= "" ||  SelectionType == '' ) && */ IsToAddOption) {
                IsToAddOption = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedOptionsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddOption = true;
                        break;
                    }
                }
            }

            var IsToAddFinish = ((getIndexOf(FeaturesList, "finish", false) > -1) || (getIndexOf(FeaturesList, "finishes", false) > -1));
            if (/* ( SelectedFinish!= "" || SelectionType == '' ) && */ IsToAddFinish) {
                IsToAddFinish = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedFinishesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddFinish = true;
                        break;
                    }
                }
            }

            var IsFilled = false;
            for (J = 0; J < filled_hands.length; J++) {
                if (filled_hands[J] == val) {
                    IsFilled = true;
                    break;
                }
            }
            if (is_debug)
                alert("repopulate_hands  IsFilled::" + IsFilled + "   IsSingleFeaturesList::" + IsSingleFeaturesList)

            if (!IsFilled && (IsToAddSize || IsToAddColor || IsToAddOption || IsToAddFinish || IsSingleFeaturesList)) {
                var SelectedTag = ""
                if (is_debug)
                    alert("val::" + val + "   CurrentOptionValue::" + CurrentOptionValue)
                if (CurrentHandValue == val) {
                    SelectedTag = " selected "
                }

                rep_hands_list += '<option value="' + i + '" ' + SelectedTag + ' >' + val + '</option>';
                filled_hands[filled_hands.length] = val;
            }
        });
        jQuery('#select_hand').html(rep_hands_list);
    }  // function repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedFinish, SelectionType )


    function repopulate_finishes(SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType)
    {
        // var is_debug= true
        var temp_finishes = jQuery.parseJSON(<?php print json_encode(json_encode($result_finishes_list)); ?>);
        if (is_debug)
            alert("repopulate_finishes  temp_finishes::" + var_dump(temp_finishes) + "  SelectedSku::" + SelectedSku + "  SelectedSize::" + SelectedSize + "  SelectedColor::" + SelectedColor
                    + "  SelectedOption::" + SelectedOption + "  SelectedHand::" + SelectedHand + "  SelectionType::" + SelectionType)
        var rep_finishes_list = '<option value=""> -select-</option>';
        var filled_finishes = Array();
        RelatedSizesList = getRelatedSizesList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_finishes RelatedSizesList::" + var_dump(RelatedSizesList))
        RelatedColorsList = getRelatedColorsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_finishes RelatedColorsList::" + var_dump(RelatedColorsList))
        RelatedOptionsList = getRelatedOptionsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_finishes RelatedOptionsList::" + var_dump(RelatedOptionsList))
        RelatedHandsList = getRelatedHandsList(SelectedSku, SelectionType);
        if (is_debug)
            alert("repopulate_finishes RelatedHandsList::" + var_dump(RelatedHandsList))

        var CurrentFinishValue = jQuery('#select_finish option:selected').text();
        if (is_debug)
            alert("CurrentFinishValue::" + CurrentFinishValue)

        jQuery.each(temp_finishes, function(i, val) {

            var IsToAddSize = ((getIndexOf(FeaturesList, "size", false) > -1) || (getIndexOf(FeaturesList, "sizes", false) > -1));
            if (/* ( SelectedSize!= "" || SelectionType == '' ) && */ IsToAddSize) {
                IsToAddSize = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedSizesList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddSize = true;
                        break;
                    }
                }
            }

            var IsToAddColor = ((getIndexOf(FeaturesList, "color", false) > -1) || (getIndexOf(FeaturesList, "colors", false) > -1));
            if (/* ( SelectedColor!= "" || SelectionType == '' ) && */ IsToAddColor) {
                IsToAddColor = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedColorsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddColor = true;
                        break;
                    }
                }
            }

            var IsToAddOption = ((getIndexOf(FeaturesList, "option", false) > -1) || (getIndexOf(FeaturesList, "options", false) > -1))
            if (/* ( SelectedOption!= "" ||  SelectionType == '' ) && */ IsToAddOption) {
                IsToAddOption = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedOptionsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddOption = true;
                        break;
                    }
                }
            }

            var IsToAddHand = ((getIndexOf(FeaturesList, "hand", false) > -1) || (getIndexOf(FeaturesList, "hands", false) > -1));
            if (/* ( SelectedHand!= "" || SelectionType == '' ) && */ IsToAddHand) {
                IsToAddHand = false
                for (J = 0; J < featureses.length; J++) {
                    if (i == featureses[J].sku && ((getIndexOf(RelatedHandsList, i, false) > -1 || SelectedSku == '') || is_InitialLoading)) {
                        IsToAddHand = true;
                        break;
                    }
                }
            }

            var IsFilled = false;
            for (J = 0; J < filled_finishes.length; J++) {
                if (filled_finishes[J] == val) {
                    IsFilled = true;
                    break;
                }
            }
            if (is_debug)
                alert("repopulate_finishes  IsFilled::" + IsFilled + "   IsSingleFeaturesList::" + IsSingleFeaturesList)

            if (!IsFilled && (IsToAddSize || IsToAddColor || IsToAddOption || IsToAddHand || IsSingleFeaturesList)) {
                var SelectedTag = ""
                if (is_debug)
                    alert("val::" + val + "   CurrentFinishValue::" + CurrentFinishValue)
                if (CurrentFinishValue == val) {
                    SelectedTag = " selected "
                }

                rep_finishes_list += '<option value="' + i + '" ' + SelectedTag + ' >' + val + '</option>';
                filled_finishes[filled_finishes.length] = val;
            }
        });
        jQuery('#select_finish').html(rep_finishes_list);
    }  // function repopulate_finishes( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectedHand, SelectionType )

    function getRelatedSizesList(SelectedSku, SelectionType) {
        if (SelectionType != 'size')
            return Array(SelectedSku);
        var temp_sizes = jQuery.parseJSON(<?php print json_encode(json_encode($result_size_list)); ?>);
        var ResArray = Array();
        var SelectedSize = ''
        jQuery.each(temp_sizes, function(i, val) {
            if (i == SelectedSku) {
                SelectedSize = val;
            }
        });
        jQuery.each(temp_sizes, function(i, val) {
            //alert( "++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "  SelectedSize::" + SelectedSize )
            if (val == SelectedSize) {
                // alert( "INSIDE++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku )
                ResArray[ResArray.length] = i;
            }
        });
        return ResArray;
    }

    function getRelatedColorsList(SelectedSku, SelectionType) {
        if (SelectionType != 'color')
            return Array(SelectedSku);
        var temp_colors = jQuery.parseJSON(<?php print json_encode(json_encode($result_colors_list)); ?>);
        var ResArray = Array();
        var SelectedColor = ''
        jQuery.each(temp_colors, function(i, val) {
            if (i == SelectedSku) {
                SelectedColor = val;
            }
        });
        jQuery.each(temp_colors, function(i, val) {
            // alert( "++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "  SelectedColor::" + SelectedColor )
            if (val == SelectedColor) {
                ResArray[ResArray.length] = i;
            }
        });
        return ResArray;
    }

    function getRelatedHandsList(SelectedSku, SelectionType) {
        if (SelectionType != 'hand')
            return Array(SelectedSku);
        var temp_hands = jQuery.parseJSON(<?php print json_encode(json_encode($result_hands_list)); ?>);
        var ResArray = Array();
        var SelectedHand = ''
        jQuery.each(temp_hands, function(i, val) {
            if (i == SelectedSku) {
                SelectedHand = val;
            }
        });
        jQuery.each(temp_hands, function(i, val) {
            // alert( "++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "  SelectedHand::" + SelectedHand )
            if (val == SelectedHand) {
                ResArray[ResArray.length] = i;
            }
        });
        return ResArray;
    }

    function getRelatedOptionsList(SelectedSku, SelectionType) {
        if (SelectionType != 'option')
            return Array(SelectedSku);
        var temp_options = jQuery.parseJSON(<?php print json_encode(json_encode($result_options_list)); ?>);
        var ResArray = Array();
        var SelectedOption = ''
        jQuery.each(temp_options, function(i, val) {
            if (i == SelectedSku) {
                SelectedOption = val;
            }
        });
        jQuery.each(temp_options, function(i, val) {
            // alert( "getRelatedOptionsList  ++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "  SelectedOption::" + SelectedOption )
            if (val == SelectedOption) {
                ResArray[ResArray.length] = i;
            }
        });
        return ResArray;
    }

    function getRelatedFinishesList(SelectedSku, SelectionType) {
        if (SelectionType != 'finish')
            return Array(SelectedSku);
        var temp_finishes = jQuery.parseJSON(<?php print json_encode(json_encode($result_finishes_list)); ?>);
        var ResArray = Array();
        var SelectedFinish = ''
        jQuery.each(temp_finishes, function(i, val) {
            if (i == SelectedSku) {
                SelectedFinish = val;
            }
        });
        jQuery.each(temp_finishes, function(i, val) {
            // alert( "++ i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "  SelectedFinish::" + SelectedFinish )
            if (val == SelectedFinish) {
                ResArray[ResArray.length] = i;
            }
        });
        return ResArray;
    }


</script>

<?php

function get_image_by_sku($sku) {
    $temp_file_name = '';
    $check = '';

    $thumbnail_image_file = AppUtils::getThumbnailImageBySku($sku, false);
    if (empty($thumbnail_image_file)) {
        $thumbnail_image_file = AppUtils::getResizedImageBySku($sku, false, false, true);
        $check = $thumbnail_image_file['ImageFileName'];
        if (is_array($thumbnail_image_file)) {
            $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file['ImageFileName'];
            $temp_file_name = $thumbnail_image_file;
        } else {
            $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file;
            $temp_file_name = $thumbnail_image_file;
        }
        $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $thumbnail_image_file;
        if (!file_exists($temp_file_name)) {
            $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
        }
    }
    if ($check == '') {
        $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
    }
    return $thumbnail_image_file;
}
?>



