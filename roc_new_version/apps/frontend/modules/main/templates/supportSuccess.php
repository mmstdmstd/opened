<style type="text/css">
	#support {width:885px; overflow:hidden; margin:0 auto; font:normal 135% arial, sans-serif; color:#222;}
	#support h1 {margin:40px 0 10px 0; font-size:110%; width:676px;}
	#support h2 {margin:15px 0 0 0; height:22px; padding:4px 0 0 13px; font-size:95%; width:656px; background:url("<?php Util::getServerHost() ?>images/h2-grey-bg.png") repeat-x;}
	#videos-section table {margin:0 0 70px 0; padding:0; list-style:none; line-height:1.3; width:676px; overflow:hidden;}
	#videos-section table td {width:326px; overflow:hidden; float:left; margin-top:20px;}
	#videos-section table td a.video-thumb {display:block; width:130px; height:97px; float:left;}
	#videos-section table td.even {margin-left:14px;}
	#videos-section table td p span {display:inline-block; width:45px;}
	#videos-section table td p {margin:0 0 4px 130px; padding-left:10px; font-size:80%; line-height:1.2; color:#aaa;}
	#videos-section table td p a {text-decoration:none; font-weight:bold; color:#222; display:inline-block; width:90%;}
	.video-thumb img {border:none;}

	
 	#testing-evaluation-program table {margin:0 0 70px 0; padding:0; list-style:none; line-height:1.3; width:676px; overflow:hidden;}
	#testing-evaluation-program table td {width:326px; overflow:hidden; float:left; margin-top:20px;}
	#testing-evaluation-program table td a.video-thumb {display:block; width:130px; height:97px; float:left;}
	#testing-evaluation-program table td.even {margin-left:14px;}
	#testing-evaluation-program table td p span {display:inline-block; width:45px;}
	#testing-evaluation-program table td p {margin:0 0 4px 130px; padding-left:10px; font-size:80%; line-height:1.2; color:#aaa;}
	#testing-evaluation-program table td p a {text-decoration:none; font-weight:bold; color:#222; display:inline-block; width:90%;}

	
	
	#display-resources-for {background-color:#f8d37a; overflow:hidden;}
	#send-us-an-email {background-color:#ffe4ad; overflow:hidden; margin-top:15px;}
	#size-charts-section ol, #documents-section ol {list-style:none; margin:0 0 70px 0; padding:0; width:676px; overflow:hidden;
		font-size:80%;}
	#size-charts-section ol li, #documents-section ol li {float:left; width:24%; text-align:center; margin-top:20px}
	#size-charts-section ol li a, #documents-section ol li a {color:#222; width:82%; display:inline-block;}
	#size-charts-section img, #documents-section img {border:1px solid #ccc; display:block; width:79px;
		margin:0 auto 10px auto;}
	#support-sidebar {width:180px; overflow:hidden; float:right; margin-top:1px;}
	#support-sidebar h4 {margin:9px 0 6px 10px;; padding:0; font-size:86%; color:#333;}
	#support-sidebar input, #support-sidebar select { line-height:1; font:inherit; font-size:80%; white-space:normal;}
	#support-sidebar select {width:100% !important; padding:2px 4px 2px 4px; margin:0; font-size: 13px;}
	#support-sidebar p {margin:0 0 9px 10px; padding:0; width:160px; font-size: 13px;}
	#s-search, #q-telephone, #q-email, #q-name {width:148px; border:1px solid #c2c2c2; padding:3px 4px 1px 6px; padding-bottom:2px\9; margin:0; line-height:1; font-size:13px;}
	#support-sidebar p input {font-size:13px;}
	#support-sidebar .display-checkbox {margin-bottom:5px;}
	#support-sidebar .display-checkbox label {position:relative; top:-1px;}
	#support-sidebar .display-checkbox input {margin:0 1px 0 0; padding:0;}
	#display-resources-for #display-update {display:block; width:60px; height:20px; margin:13px 0 12px 10px;
		background:url("<?php Util::getServerHost() ?>images/update-aligned-2.png") no-repeat;}
	#display-resources-for a#display-update:hover {background-position:0 -20px;}
	#display-resources-for #display-update span, #send-us-an-email #q-submit span {display:none;}
	#support-sidebar #q-message {resize: none; width:154px; font:normal 100% arial, sans-serif;}
	#send-us-an-email #q-submit {display:block; width:60px; height:20px; margin:13px 0 12px 10px;
		background:url("<?php Util::getServerHost() ?>images/submit-aligned-2.png") no-repeat;}
	#send-us-an-email a#q-submit:hover {background-position:0 -20px;}
	
	#size-charts-section ol, #paperworks-section ol {list-style:none; margin:0 0 70px 0; padding:0; width:676px; overflow:hidden;
		font-size:80%;}
	#size-charts-section ol li {float:left; width:24%; text-align:center; margin-top:20px;}
	#paperworks-section ol li {float:left; width:24%; text-align:center; margin-top:30px;}
	#size-charts-section ol li a, #paperworks-section ol li a {color:#222; width:82%; display:inline-block;}
	#size-charts-section img, #paperworks-section img {border:1px solid #ccc; display:block; width:79px;
		margin:0 auto 10px auto;}	
	
</style>




<script type="text/javascript" language="JavaScript">
<!--

function SendSupportEmail() {
  var q_name = Trim(document.getElementById("q-name").value)
  if ( q_name== "" ) {
    alert("Please fill in your name before submitting.")
    document.getElementById("q-name").focus()
    return
  }

  var q_email = Trim(document.getElementById("q-email").value)
  if ( q_email== "" ) {
    alert("Please fill in your email address before submitting.")
    document.getElementById("q-email").focus()
    return
  }

  if ( !CheckEmail(q_email) ) {
    alert("Please check your email address -- an invalid email address has been entered.")
    document.getElementById("q-email").focus()
    return
  }

  var q_subject= document.getElementById('q-subject').selectedIndex
  if ( q_subject== 0 ) {
    alert("Please select a subject for your email before submitting.")
    document.getElementById("q-subject").focus()
    return
  }
  q_subject= document.getElementById('q-subject').options[document.getElementById('q-subject').selectedIndex].value
  var q_telephone = Trim(document.getElementById("q-telephone").value)

  var q_message = Trim(document.getElementById("q-message").value)
  if ( q_message== "" ) {
    alert("Please write something in the message area before submitting.")
    document.getElementById("q-message").focus()
    return
  }
  document.getElementById("span_loadingAnimation").style.display="inline"

  var HRef= '<?php echo url_for('@send_support_email') ?>'

  //alert(HRef)
  var DataArray= {
  "name" : q_name,
  "email" :q_email,
  "message" : q_message,
  "subject" : q_subject,
  "telephone" : q_telephone
  };

  jQuery.ajax({
    url: HRef,
    type: "POST",
    data: DataArray,
    success: onSendSupportEmail,
    dataType: "json"
  });



}

function onSendSupportEmail(data) {
  document.getElementById("q-name").value= ""
  document.getElementById("q-email").value= ""
  document.getElementById('q-subject').selectedIndex=0
  document.getElementById("q-telephone").value=""
  document.getElementById("q-message").value=""
  document.getElementById("span_loadingAnimation").style.display="none"
  alert("Thank you, your message has been sent.")
}
//-->
</script>

<div id="support">
	<h1 style="">Support</h1>
	<p style="font-size:80%; width:875px; margin-top:3px;">We have added this support page to ensure that our customers have access to product documents from manufacturers and from us. These documents can help with certain benefits and features that can sometimes be left out from regular advertisements.  We also have added sizing charts below to give a general idea of what size to order for a variety of apparel and garments. You can also find Body Armor Sizing Sheets from our various manufacturers.  The videos are instructional and will constantly be changing and updated.  We are always curious on how we can make your shopping experience better. Please call or email us today if you need anything or have questions or comments.</p>
	<div id="support-sidebar">
		<div id="display-resources-for" style="display:none">
			<h4>Display Resources for:</h4>
			<p>
			<select>
				<option>All Categories</option>
				<option>Option</option>
				<option>Option</option>
			</select>
			</p>
			<p>
			<select>
				<option>All Subcategories</option>
				<option>Option</option>
				<option>Option</option>
			</select>
			</p>
			<p><input id="s-search" type="text" value="Type Search Here"
				onfocus="if(this.value==this.defaultValue)this.value='';"
				onblur="if(this.value=='')this.value=this.defaultValue;"/></p>
			<p class="display-checkbox">
				<input id="display-videos" type="checkbox" /> <label for="display-videos">Videos</label>
			</p>
			<p class="display-checkbox">
				<input id="display-documents" type="checkbox" /> <label for="display-documents">Documents</label>
			</p>
			<p class="display-checkbox">
				<input id="display-charts" type="checkbox" /> <label for="display-charts">Sizing Charts</label>
			</p>
			<a href="#" id="display-update"><span>Update</span></a>
		</div>
		<div id="send-us-an-email" style="position:relative">
			<img style="margin:7px 0 0 9px;" src="<?php Util::getServerHost() ?>images/emailbox-header-trans.png" />
			<p><label for="q-name">Name</label>: * <input id="q-name" type="text" /></p>
			<p><label for="q-email">Email Address</label>: * <input id="q-email" type="text" /></p>
			<p><label for="q-telephone">Telephone Number</label>: <input id="q-telephone" type="text" /></p>
      <?php $support_subject_items= sfConfig::get( 'app_application_support_subject_items' ); ?>
			<p><label>Subject</label>: *
				<select id="q-subject" style="width:112px;">
					<option>Select</option>
          <?php foreach($support_subject_items as $support_subject_item) : ?>
  					<option value="<?php echo $support_subject_item ?>" ><?php echo $support_subject_item ?></option>
          <?php endforeach; ?>
				</select>
			</p>
			<p><label for="q-message">Message</label>: *
				<textarea id="q-message" name="address" rows="12"></textarea>
			</p>
			<a  style="cursor:pointer;" onclick="javascript:SendSupportEmail(); return false;" id="q-submit"><span>Submit</span></a>
      <span style="display:none;position:absolute;left:80px;bottom:14px" id="span_loadingAnimation"> <?php echo image_tag('loadingAnimation.gif', array('style'=>"padding-top:0px;"))?></span>
		</div> <!-- / send-us-an-email -->
		<img style="margin-top:25px;" src="<?php Util::getServerHost() ?>images/natlnumber-side.png" />
	</div> <!-- / support-sidebar -->
	<div id="videos-section">
		<h2>Videos</h2>
		<table>
		<?php
		$product_support_videos_text= sfConfig::get('app_application_product_support_videos_text' );
		for ( $I= 0; $I< $product_support_video_count; $I++ ) :
		if ( empty($YoutubeVideosList[$I]) or !is_object($YoutubeVideosList[$I]) ) continue;
		$Sku='-';
		$Url= $YoutubeVideosList[$I]->getUrl();
		$VideoKey= $YoutubeVideosList[$I]->getKey();
		$YuotubeKey= AppUtils::getYuotubeKey($Url);
    if ( strpos( $Url, 'vimeo.com' ) === false ) {
      $ThumbnailUrl= 'http://i.ytimg.com/vi/'.$YuotubeKey.'/1.jpg';
      $ThumbnailSize= '';
    } else {
      $A= preg_split('/\/video\//', $Url );
      if ( !empty($A[1]) ) { // http://stackoverflow.com/questions/1361149/get-img-thumbnails-from-vimeo
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".$A[1].".php"));
        $ThumbnailUrl= $hash[0]['thumbnail_small'];
        // 120px × 90px
        $ThumbnailSize= ' width="120px" height="90px" ';
      }
    }
		$VideoTitle= $YoutubeVideosList[$I]->getTitle();
		$VideoDescription= $YoutubeVideosList[$I]->getDescription();
      ?>
      <!-- product support video <?php echo $I+1 ?> -->
      <div class="video_1" style="display:none">
        <iframe width="276" height="217" src="<?php echo $Url ?>" frameborder="0" allowfullscreen>
        </iframe>
      </div>

      <?php if ($I%2==0) { ?>
      <tr>
      <?php } ?>
			<td <?php if ($I%2==1) { ?>class="even"<?php } ?>>
        <?php  $Src= '<a class="thickbox video-thumb" href="'.url_for( "@product_video_view?sku=" . urlencode($Sku) . '&video_key=' . $VideoKey ).'/width/1000/height/800' . '"><img src="'.$ThumbnailUrl.'" '.$ThumbnailSize.' alt="'.$VideoTitle.'" /></a> ';
          echo $Src;  ?>
        <p>
        <?php  $Src= '<a class="thickbox" href="'.url_for( "@product_video_view?sku=" . urlencode($Sku) . '&video_key=' . $VideoKey ).'/width/1000/height/800' . '">'.$VideoTitle.'</a> ';
          echo $Src;  ?>
        </p>
        <p>
				<?php
				echo substr($VideoDescription,0,100);
				if (strlen($VideoDescription) > 100) {
				  echo "...";
				}
        ?></p>
			</td>
      <?php if ($I%2==1) { ?>
      </tr>
      <?php } ?>

    <?php endfor;  ?>
    </table>
	</div> <!-- / videos-section -->


  <div id="testing-evaluation-program">
		<h2>Testing & Evaluation Program</h2>
		<table style="border;2px dotted gray">

      <!-- <div class="video_1" style="display:none">
        <img width="276" height="217" src="<?php echo Util::getServerHost() ?>images/firearm-support-btn.png" >
      </div> -->

      <tr>

  			<td>
          <div class="firearm-btn-div">
            <a class="" href="<?php echo url_for('@firearm') ?>" ><img src="<?php echo Util::getServerHost() ?>/images/firearm-support-btn.png" alt="Firearms" /></a>
          </div>
          <p>
            <a class="" href="<?php echo url_for( "@firearm" ) ?>" >
              <b>Firearm Testing & Evaluation</b><br />
            </a>
          </p>
          <p>
            <a style="color:#aaaaaa; font-weight:normal;" class="" href="<?php echo url_for( "@firearm" ) ?>" >
				      Test and Evaluate popular<br>and new firearms in person<br>to find what is right for your<br>organization.
            </a>
          </p>
	  		</td>

  			<td>
          <div class="firearm-btn-div">
            <a class="video-thumb" href="<?php echo url_for( "@taser" ) ?>" ><img src="<?php echo Util::getServerHost() ?>/images/taser-support-btn.png" alt="Taser" /></a>
          </div>
            <p>
              <a class="" href="<?php echo url_for( "@taser" ) ?>" >
                <b>Taser Testing & Evaluation</b><br />
              </a>
            </p>
          <p>
            <a style="color:#aaaaaa; font-weight:normal;" class="" href="<?php echo url_for( "@taser" ) ?>" >
				      Test and Evaluate popular<br>and new tasers in person<br>to find what is right for your<br>organization.
            </a>
          </p>
	  		</td>

      </tr>

    </table>
	</div> <!-- / testing-evaluation-program -->
	
	<div id="paperworks-section">
		<h2>Paperwork</h2>
		<ol>
      <?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
      $PaperworksThumbnailsDirectory= sfConfig::get('app_application_PaperworksThumbnailsDirectory' );
      //Util::deb( $PaperworksThumbnailsDirectory, '$PaperworksThumbnailsDirectory::' );
      
      
      $PaperworksDirectory= sfConfig::get('app_application_PaperworksDirectory' );
      //Util::deb( $PaperworksDirectory, '$PaperworksDirectory::' );
      
      $support_paperworks_number= sfConfig::get('app_application_support_paperworks_number' );
      //Util::deb( $support_paperworks_number, '$support_paperworks_number::' );
      
      $PaperworksList= PaperworkPeer::getPaperworks( 1, false, $support_paperworks_number );
      //Util::deb( $PaperworksList, '$PaperworksList::' );
      ?>
      <?php foreach ( $PaperworksList as $Paperwork ) : ?>
  			<li style="width:200px">
          <a href="<?php echo $HostForImage . $PaperworksDirectory . DIRECTORY_SEPARATOR . $Paperwork->getFilename() ?>">
            <img src="<?php echo $HostForImage . $PaperworksThumbnailsDirectory . DIRECTORY_SEPARATOR . $Paperwork->getThumbnail() ?>" />
            <?php echo $Paperwork->getTitle() ?>
          </a>
        </li>
      <?php endforeach; ?>
		</ol>
	</div> <!-- / Paperworks-section -->
	
	
	<div id="documents-section">
		<h2>Documents</h2>
		<ol>
      <?php $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
      $DocumentsThumbnailsDirectory= sfConfig::get('app_application_DocumentsThumbnailsDirectory' );
      $DocumentsDirectory= sfConfig::get('app_application_DocumentsDirectory' );
      $support_documents_number= sfConfig::get('app_application_support_documents_number' );
      $DocumentsList= DocumentPeer::getDocuments( 1, false, $support_documents_number );
      ?>
      <?php foreach ( $DocumentsList as $Document ) : ?>
  			<li>
          <a href="<?php echo $HostForImage . $DocumentsDirectory . DIRECTORY_SEPARATOR . $Document->getFilename() ?>">
            <img src="<?php echo $HostForImage . $DocumentsThumbnailsDirectory . DIRECTORY_SEPARATOR . $Document->getThumbnail() ?>" /><?php echo $Document->getTitle() ?>
          </a>
        </li>
      <?php endforeach; ?>
		</ol>
	</div> <!-- / documents-section -->

	
	
	<div id="size-charts-section">
		<h2>Size Charts</h2>
		<ol>
      <?php  $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
      $SizingChartsThumbnailsDirectory= sfConfig::get('app_application_SizingChartsThumbnailsDirectory' );
      $SizingChartsDirectory= sfConfig::get('app_application_SizingChartsDirectory' );
      $support_sizingcharts_number= (int)sfConfig::get('app_application_support_sizingcharts_number' );
      $SizingChartsList= SizingChartPeer::getSizingCharts( 1, false, $support_sizingcharts_number );
      ?>
      <?php foreach ( $SizingChartsList as $SizingChart ) : ?>
        <li>
          <a href="<?php echo $HostForImage . $SizingChartsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getFilename() ?>">
            <img src="<?php echo $HostForImage . $SizingChartsThumbnailsDirectory . DIRECTORY_SEPARATOR . $SizingChart->getThumbnail() ?>" /><?php echo $SizingChart->getTitle() ?>
          </a>
        </li>
      <?php endforeach; ?>
		</ol>
	</div> <!-- / size-charts-section -->


</div>