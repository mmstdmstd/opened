<?php
$ImagesBySkuList = AppUtils::getImagesBySku($Sku, /* $ReturnFullPath= */ false, /* $ReturnLowestIndex= */ false, true, true);
$product_more_views_columns = (int) sfConfig::get('app_application_product_more_views_columns');
$sf_upload_dir = sfConfig::get('sf_upload_dir');

$ImagesBySkuCount = count($ImagesBySkuList);

$RowsCount = (int) ($ImagesBySkuCount / $product_more_views_columns);

$HasDecimal = $ImagesBySkuCount % $product_more_views_columns;
if ($HasDecimal > 0)
  $RowsCount++;
?>


<p style="float:left; margin:4px 0 0 0; padding:0;" id="sub_heading_1"><?php echo __("More Views") ?>:</p>

<div id="more_views_div" style="position:relative; top:-2px;" >
  <?php $I = 0;
  for ($Row = 1; $Row <= $RowsCount; $Row++) : ?>

    <?php
    $VideoForRow = '';
    $ThumbnailImageFile = '';


    for ($Col = 1; $Col <= $product_more_views_columns; $Col++) :
      ?>
      <?php
      if (!empty($ImagesBySkuList[$I])) :
        $ImagePath = $ImagesBySkuList[$I];
      ?>


        <?php
        $ThumbnailImageFullUrl = '';
        if (!empty($ImagePath)) :
          $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'uploads/' . $ImagePath;
          //  echo '-2 $ThumbnailImageFullUrl::'.print_r( $ThumbnailImageFullUrl,true ).'<br>';
          //Util::deb( $ThumbnailImageFullUrl, ' $ThumbnailImageFullUrl::' );
          $ImageFullPath = $sf_upload_dir . DIRECTORY_SEPARATOR . $ImagePath;
          //Util::deb( $ImageFullPath, ' $ImageFullPath::' );
          // echo '-3 $ImageFullPath::'.print_r( $ImageFullPath,true ).'<br>';
          if (!file_exists($ImageFullPath) and !is_dir($ImageFullPath)) {
            //Util::deb( ' -99::' );
            $ImagePath = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage48.png';
          }
          ?>
          <img src="<?php echo $ThumbnailImageFullUrl ?>" alt="" height="48" width="48" onclick="javascript:ShowMainImage('<?php echo $ThumbnailImageFullUrl ?>', '<?php echo $ImagePath ?>', false )" style="cursor:pointer;" >
        <?php endif; //die("DIE"); ?>

        <?php
        if (is_array($ThumbnailImageFile) and !empty($ThumbnailImageFile/* ['ImageFileName'] */)) :
          //Util::deb( $ThumbnailImageFile, ' -2 $ThumbnailImageFile::' );
          $ThumbnailImageUrl = 'uploads' . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
          $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
          //Util::deb( $ThumbnailImageUrl, ' -2 $ThumbnailImageUrl::' );
          //Util::deb( $ThumbnailImageFullUrl, ' -3 $ThumbnailImageFullUrl::' );
          $ThumbnailImageFullFilePath = sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
          //Util::deb( $ThumbnailImageFullFilePath, ' $ThumbnailImageFullFilePath::' );
          if (!file_exists($ThumbnailImageFullFilePath) or is_dir($ThumbnailImageFullFilePath)) {
            //Util::deb( -4 );
            $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage48.png';
          }
          //Util::deb( $ThumbnailImageFullUrl, ' -33334445555555 $ThumbnailImageFullUrl::' );
          ?>

          <a href="<?php echo url_for("@product_details?sku=" . urlencode($Sku) ) ?>" >
            <img src="<?php echo $ThumbnailImageFullUrl ?>" alt="" height="48" width="48" style="cursor:pointer;" >
          </a>
        <?php endif; // if ( $Sku=='0-8001-090' ) die("DIE");  ?>



    <?php else: ?>
        &nbsp;
    <?php endif; ?>

    <?php $I++;
  endfor; ?>

<?php endfor; ?>
</div>
