
    <div id="container">

  		<div id="slides">
  			<div class="slides_container" style="width:640px !important; height:378px !important;">
        
  
<!-- slide 1 -->					
   				<div class="slide">
            <a href="main/product_listings/page/1/sorting/-/rows_in_pager/50?select_category=&input_search=PMAG%20MOE">
              <?php echo image_tag( "slideshow/magpul-magazines.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 1") )?>
            </a>
  				</div>

<!-- slide 2 --> 
  				<div class="slide">
            <a href="main/product_details/sku/AR1297">   
  				    <?php echo image_tag( "slideshow/AR1927-slideshow.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 2") )?>
            </a>
  				</div> 

<!-- slide 3 --> 

  				<div class="slide">
            <a href=" http://www.oherron.com/main/product_listings/page/1/sorting/-/rows_in_pager?select_brand_id=139">   
  				    <?php echo image_tag( "slideshow/ROC-Turtleskin-banner_v02.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 3") )?>
            </a>
  				</div> 


<!-- slide 4 --> 
  				<div class="slide">
            <a href="main/product_listings/page/1/sorting/-/rows_in_pager?select_brand_id=42">

             <?php echo image_tag( "slideshow/Winchester-banner_v03.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 4") )?>

            </a>
  				</div>

<!-- slide 5 -->

  				<div class="slide">
            <a href="http://www.oherron.com/main/product_listings/page/1/sorting/-/rows_in_pager?select_brand_id=23">
  						<?php echo image_tag( "slideshow/ROC-Streamline-banner-v03.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 5") )?>
            </a>
  				</div>


<!-- slide 6 -->
  				<div class="slide">
            <a href="main/product_details/sku/FW028WP-080R?input_search=&select_category=Footwear&select_subcategory=Boots&select_brand_id=&rows_in_pager=100&sorting=-&page=1"> 
              <?php echo image_tag( "slideshow/blauer-boots.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 6") )?>
            </a>
  				</div> 
  				
<!-- slide 7 -->
  				<div class="slide">
            <a href="http://www.oherron.com/main/product_listings/page/1/sorting/-/rows_in_pager/50?select_category=&input_search=breakout"> 
              <?php echo image_tag( "slideshow/Breakout-banner_v04.jpg", array("width"=>"640", "height"=>"378", "alt"=>"Slide 7") )?>
            </a>
  				</div> 
  				
				
  			</div> <!-- close slides_container -->
  		</div> <!-- close slides -->

  	</div> <!-- close container -->

<!-- slideshow ends -->
