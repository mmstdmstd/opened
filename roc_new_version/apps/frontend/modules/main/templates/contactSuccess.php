<style type="text/css">
#contact {width:885px; overflow:hidden; margin:0 auto; font:normal 135% arial, sans-serif;}
#contact div a {text-decoration:none;}
#contact a {color:#222;}
#contact a:hover {text-decoration:underline;}
#contact h1 {margin:40px 0 10px 0; font-size:110%; width:676px;}
#contact h2 {margin:15px 0 0 0; height:22px; padding:4px 0 0 9px; padding-top:6px\9; font-size:90%; width:656px; background:url("<?php Util::getServerHost() ?>images/h2-gold-bg.png") repeat-x;}
#contact h3 {margin:10px 0 5px 0; padding:0; font-size:80%; color:#000;}
#contact ol {margin:0 0 50px 0; padding:0; list-style:none; line-height:1.3; width:676px; overflow:hidden;}
#contact ol li {width:325px; overflow:hidden; float:left; margin-top:20px;}
#contact ol li img {margin-bottom:20px;}
#contact ol li strong {color:#000;}
#contact ol li.even {padding-left:22px; border-left:2px solid #eed18f;}
#contact ol li p {margin:0 0 16px 0px; padding-left:0; font-size:75%; line-height:1.2; color:#222;}
#contact-your-representative {overlow:hidden; position:relative;}
#contact-your-representative ol {width:900px !important; padding-top:327px; height:300px; }
#contact-your-representative ol li p {font-size:70%; padding:0; margin:20px 0 0 0; display:block; width:52%; float:left; line-height:1.3;}
#contact-your-representative ol li p:first-child {width:48%; font-weight:bold; font-size:75%; color:#000; margin-top:0;}
#contact-your-representative ol li p img {float:right; padding-left:10px; margin-top:-20px;}
#contact-your-representative ol li p strong {font-weight:bold; font-size:120%; margin-bottom:5px; display:inline-block;}
#contact-your-representative ol li p span {display:inline-block; width:42px;}
#contact-your-representative ol li {width:426px; border-top:2px solid #eed18f; padding-top:8px; height:96px; margin-top:0;}
#contact-your-representative ol li.even {border-left:none; margin-left:33px; padding-left:0; position:relative; top:-212px;}
#contact .get-derections {margin:18px 0 0 0;}
#contact .get-derections input {width:250px; font:normal 100% arial, sans-serif; margin-top:4px; padding:3px 0;}
#contact .get-derections a {display:block; width:90px; height:20px; margin:13px 0 12px 0;
	background:url("<?php Util::getServerHost() ?>images/getdirections-aligned.png") no-repeat;}
#contact .get-derections a span {display:none;}
#contact .get-derections a:hover {background-position:0 -20px;}

#display-resources-for {background-color:#f8d37a; overflow:hidden;}
#send-us-an-email {background-color:#ffe4ad; overflow:hidden; margin-top:1px;}
#size-charts-section ol, #documents-section ol {list-style:none; margin:15px 0 70px 0; padding:0; width:676px; overflow:hidden;
	font-size:80%;}
#size-charts-section ol li, #documents-section ol li {float:left; width:24%; text-align:center;}
#size-charts-section ol li a, #documents-section ol li a {color:#222; width:82%; display:inline-block;}
#size-charts-section img, #documents-section img {border:1px solid #ccc; display:block; width:79px;
	margin:0 auto 10px auto;}
#support-sidebar {width:180px; overflow:hidden; float:right; margin-top:1px;}
#support-sidebar h4 {margin:9px 0 6px 10px;; padding:0; font-size:86%; color:#333;}
#support-sidebar input, #support-sidebar select { line-height:1; font:inherit; font-size:80%; white-space:normal;}
#support-sidebar select {width:100% !important; padding:2px 4px 2px 4px; margin:0; font-size: 13px;}
#support-sidebar p {margin:0 0 9px 10px; padding:0; width:160px; font-size: 13px;}
#s-search, #q-telephone, #q-email, #q-name {width:148px; border:1px solid #c2c2c2; padding:3px 4px 1px 6px; padding-bottom:2px\9; margin:0; line-height:1; font-size:13px;}
#support-sidebar p input {font-size:13px;}
#support-sidebar .display-checkbox {margin-bottom:5px;}
#support-sidebar .display-checkbox label {position:relative; top:-1px;}
#support-sidebar .display-checkbox input {margin:0 1px 0 0; padding:0;}
#display-resources-for #display-update {display:block; width:60px; height:20px; margin:13px 0 12px 10px;
	background:url("update-aligned-2.png") no-repeat;}
#display-resources-for a#display-update:hover {background-position:0 -20px;}
#display-resources-for #display-update span, #send-us-an-email #q-submit span {display:none;}
#support-sidebar #q-message {resize: none; width:154px; font:normal 100% arial, sans-serif;}
#send-us-an-email #q-submit {display:block; width:60px; height:20px; margin:13px 0 12px 10px;
	background:url("<?php Util::getServerHost() ?>images/submit-aligned-2.png") no-repeat;}
#send-us-an-email a#q-submit:hover {background-position:0 -20px;}
</style>


<script type="text/javascript" language="JavaScript">
  <!--

  function GetDirections(Index) {
    var SAddr= Trim(document.getElementById('input_get_directions_'+Index).value)
    if ( SAddr== "" ) {
      alert("Enter search criteria.")
      document.getElementById("input_get_directions_"+Index).focus()
      return
    }

    var H= screen.availHeight - 80;
    var W= screen.availWidth - 60;
    var naProps = window.open( "<?php echo url_for("@map_search_addr?addr=") ?>"+encodeURIComponent(SAddr)+'/index/'+Index,
    "showzip","status=no,modal=yes,scrollbars=1,width="+W+",height="+H+",left="+GetCenteredLeft(W)+", top="+GetCenteredTop(H) );

  }

  function SendContactEmail() {
  	var q_name = Trim(document.getElementById("q-name").value)
    if ( q_name== "" ) {
      alert("Please fill in your name before submitting.")
      document.getElementById("q-name").focus()
      return
    }

  	var q_email = Trim(document.getElementById("q-email").value)
    if ( q_email== "" ) {
      alert("Please fill in your email address before submitting.")
      document.getElementById("q-email").focus()
      return
    }

    if ( !CheckEmail(q_email) ) {
      alert("Please check your email address -- invalid address submitted.")
      document.getElementById("q-email").focus()
      return
    }

    var q_subject= document.getElementById('q-subject').selectedIndex
    if ( q_subject== 0 ) {
      alert("Please select a subject for your message before submitting.")
      document.getElementById("q-subject").focus()
      return
    }
    q_subject= document.getElementById('q-subject').options[document.getElementById('q-subject').selectedIndex].value
  	var q_telephone = Trim(document.getElementById("q-telephone").value)

  	var q_message = Trim(document.getElementById("q-message").value)
    if ( q_message== "" ) {
      alert("Please write something in the message field before submitting.")
      document.getElementById("q-message").focus()
      return
    }
    document.getElementById("span_loadingAnimation").style.display="inline"
    var HRef= '<?php echo url_for('@send_contact_email') ?>'

    //alert(HRef)
    var DataArray= {
			"name" : q_name,
			"email" :q_email,
		  "message" : q_message,
		  "subject" : q_subject,
		  "telephone" : q_telephone
	  };

  	jQuery.ajax({
      url: HRef,
      type: "POST",
      data: DataArray,
      success: onSendContactEmail,
      dataType: "json"
    });
  }

  function onSendContactEmail(data) {
  	document.getElementById("q-name").value= ""
  	document.getElementById("q-email").value= ""
    document.getElementById('q-subject').selectedIndex=0
  	document.getElementById("q-telephone").value=""
    document.getElementById("q-message").value=""
    document.getElementById("span_loadingAnimation").style.display="none"
    alert("Thank you, your message has been sent.")
  }
  //-->
</script>
<?php $contact_subject_items= sfConfig::get( 'app_application_contact_subject_items' ); ?>

<div id="contact">
	<h1 style="">Contact</h1>
	<p style="font-size:80%; width:680px; margin-top:3px;margin-bottom:0">If one of our salesmen handles your area, be sure to give them a call. They will be more than happy to stop in and speak with you in person and discuss any of the products and values we offer.</p>
  <p style="font-size:80%; width:680px; margin-top:10px;margin-bottom:25px;">If a salesman does not represent your area, stop by one of our stores in Lombard or Danville Illinois. You can also call us at 1-800-223-2097 and we will be glad to take your order!</p>
	<div id="support-sidebar">
		<div id="send-us-an-email" style="position:relative">
			<img style="margin:7px 0 0 9px;" src="<?php Util::getServerHost() ?>images/emailbox-header-trans.png" />
			<p><label for="q-name">Name</label>: * <input id="q-name" type="text" /></p>
			<p><label for="q-email">Email Address</label>: * <input id="q-email" type="text" /></p>
			<p><label for="q-telephone">Telephone Number</label>: <input id="q-telephone" type="text" /></p>
			<p><label>Subject</label>: *
				<select id="q-subject" style="width:112px;">
					<option>Select</option>
          <?php foreach($contact_subject_items as $contact_subject_item) : ?>
  					<option value="<?php echo $contact_subject_item ?>" ><?php echo $contact_subject_item ?></option>
          <?php endforeach; ?>
				</select>
			</p>
			<p><label for="q-message">Message</label>: *
				<textarea id="q-message" name="address" rows="12"></textarea>
			</p>
			<a  style="cursor:pointer;" onclick="javascript:SendContactEmail(); return false;" id="q-submit"><span>Submit</span></a>
      <span style="display:none;position:absolute;left:80px;bottom:14px" id="span_loadingAnimation"> <?php echo image_tag('loadingAnimation.gif', array('style'=>"padding-top:0px;"))?></span>
		</div> <!-- / send-us-an-email -->
	</div> <!-- / support-sidebar -->
	<div>
		<h2>Visit Our Store</h2>
		<ol>
			<li>
				<h3>Danville, Illinois</h3>
				<img src="<?php Util::getServerHost() ?>images/Danville.jpg" />
        <p>
          <strong>Address</strong><br />
          3549 N. Vermilion St.<br />
          Danville, IL 61834-1070<br />
        </p>
				<p>
					<strong>Store Hours:</strong><br />
					8:00 A.M. to 4:30 P.M. Mon.-Fri.<br />
					9:00 A.M. to 12:00 P.M. Sat.<br />
					Closed Sunday
				</p>
				<p>
					<strong>Contact:</strong><br />
					Local: 217-442-0860<br />
					Toll Free: 1-800-223-2097<br />
					Fax: 1-888-223-3235<br />
					Email: <a href="mailto:rayoherron@oherron.com">rayoherron@oherron.com</a>
				</p>
				<h3 style="margin-top:25px;">Get Directions</h3>
				<iframe width="250" height="180" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Ray+O'Herron+Co+Inc&amp;aq=&amp;sll=40.18307,-87.6297&amp;sspn=0.058885,0.094757&amp;ie=UTF8&amp;hq=Ray+O'Herron+Co+Inc&amp;hnear=&amp;ll=40.18307,-87.6297&amp;spn=0.058885,0.094757&amp;t=m&amp;output=embed"></iframe>
        <?php
          $Src= '<a class="thickbox" href="'.url_for( "@google_large_map"  ).'?width=850&height=800' . '"><div style="display:block;border:1px solid #000;background-color:#FFF;font-size:11px;width:100px;text-align:center;margin-left:75px">&nbsp;view larger map&nbsp;</div></a> ';
          echo $Src; ?>


				<p class="get-derections">
					Enter Address below for driving directions:
					<input type="text" id="input_get_directions_1" />
          <?php
          $Src= '<a onclick="javascript:GetDirections(1)" ></a> ';
          echo $Src; ?>
				</p>
			</li>
			<li class="even">
				<h3>Chicagoland, Illinois</h3>
				<img src="<?php Util::getServerHost() ?>images/Chicagoland.jpg" />
        <p>
          <strong>Address</strong><br />
          523 E. Roosevelt Rd.<br />
          Lombard, IL 60148</p>
				<p>
					<strong>Store Hours:</strong><br />
					9:00 A.M. to 5:00 P.M. Mon., Wed.-Fri.<br />
					9:00 A.M. to 7:00 P.M. Tue.<br />
					8:00 A.M. to 12:00 P.M. Sat.
				</p>
				<p>
					<strong>Contact:</strong><br />
					Local: 1-630-629-COPS (2677)<br />
					Toll Free: 1-800-782-8674<br />
					Fax: 1-630-629-2682<br />
					Email: <a href="mailto:chris@oherron.com">chris@oherron.com</a>
				</p>
				<h3 style="margin-top:25px;">Get Directions</h3>
				<div style="position:relative">
  				<iframe width="250" height="180" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Ray+O'Herron+Co+Inc,+Chicagoland,+Illinois&amp;aq=&amp;sll=40.185168,-87.630386&amp;sspn=0.235532,0.379028&amp;ie=UTF8&amp;hq=Ray+O'Herron+Co+Inc,+Chicagoland,+Illinois&amp;hnear=&amp;radius=15000&amp;ll=41.858749,-88.003888&amp;spn=0.071946,0.071946&amp;t=m&amp;output=embed"></iframe>
          <!-- <a class="thickbox" href="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Ray+O'Herron+Co+Inc,+Chicagoland,+Illinois&amp;aq=&amp;sll=40.185168,-87.630386&amp;sspn=0.235532,0.379028&amp;ie=UTF8&amp;hq=Ray+O'Herron+Co+Inc,+Chicagoland,+Illinois&amp;hnear=&amp;radius=15000&amp;ll=41.858749,-88.003888&amp;spn=0.071946,0.071946&amp;t=m&amp;output=embed" style="display:block;border:1px solid #000;background-color:#FFF;font-size:11px;width:100px;text-align:center;margin-left:75px">&nbsp;view larger map&nbsp;</a>  -->
        <?php
          $Src= '<a class="thickbox" href="'.url_for( "@google_large_map_2"  ).'?width=850&height=800' . '"><div style="display:block;border:1px solid #000;background-color:#FFF;font-size:11px;width:100px;text-align:center;margin-left:75px">&nbsp;view larger map&nbsp;</div></a> ';
          echo $Src; ?>

        </div>
				<p class="get-derections">
					Enter Address below for driving directions:
					<input type="text" id="input_get_directions_2" />
        <?php
          $Src= '<a onclick="javascript:GetDirections(2)" ></a>';
          echo $Src; ?>
				</p>
			</li>
		</ol>
	</div> <!-- / videos-section -->
	<h2 style="width:99.5%;">Contact Your Representative</h2>
	<p style="font-size:80%; margin-top:30px;">Check the map below to find the representative to serve you.</p>
	<div id="contact-your-representative">
		<img src="<?php Util::getServerHost() ?>images/supportmap-large.png" style="position:absolute; top:0; left:0;" />
		<img src="<?php Util::getServerHost() ?>images/support-natlnumber.png" style="position:absolute; right:0; top:0;" />
		<ol>
			<li>
				<p>
					<strong>Bob Komarek</strong><br />
					<span>cell:</span> 630-235-2098<br />
					<span>fax:</span> 630-513-7264<br />
					<span>email:</span> <a href="mailto:rrk1@earthlink.net">rrk1@earthlink.net</a>
				</p>
				<p>
					<img src="<?php Util::getServerHost() ?>images/state-02.png" width="38px" />
					Represents the northern counties of Illinois, and southern Wisconsin counties south of Dade County
				</p>
			</li>
			<li class="even">
				<p>
					<strong>Craig Villanova</strong><br />
					<span>cell:</span> 773-841-9967<br />
					<span>office:</span> 630-629-COPS<br />
					<span>fax:</span> 816-478-7074<br />
					<span>email:</span> <a href="mailto:rocin4le@me.com">rocin4le@me.com</a>
				</p>
				<p>
					<img src="<?php Util::getServerHost() ?>images/state-07.png"/>
					Represents Northern DuPage and Cook County
				</p>
			</li>
			<li>
				<p>
					<strong>Dan Yara</strong><br />
					<span>cell:</span> 708-710-3396<br />
					<span>fax:</span> 708-532-5714<br />
					<span>email:</span> <a href="mailto:dpyara@comcast.net">dpyara@comcast.net</a>
				</p>
				<p>
					<img src="<?php Util::getServerHost() ?>images/state-01.png" width="56px" />
					Represents Southern Cook Country, Central Illinois and Lake County Indiana
				</p>
			</li>
			<li class="even">
				<p>
					<strong>TJ Lindmeier</strong><br />
					<span>cell:</span> 715-675-2899<br />
					<span>fax:</span> 715-675-3932<br />
					<span>email:</span> <a href="mailto:tjconj@charter.net">tjconj@charter.net</a>
				</p>
				<p style="margin-top:8px;">
					<img src="<?php Util::getServerHost() ?>images/state-04.png" width="75px" style="margin-top:0;" />
					Represents all of Minnesota, North Dakota, West, Central, and Northern Wisconsin
				</p>
			</li>
			<li>
				<p>
					<strong>Larry Fredericks</strong><br />
					<span>cell:</span>217-474-3826<br />
					<span>email:</span> <a href="mailto:ljfred5@comcast.net">ljfred5@comcast.net</a>
				</p>
				<p style="margin-top:12px">
					<img src="<?php Util::getServerHost() ?>images/state-08.png" width="58px" style="margin-top:0"/>
					Represents Western and Southern Illinois, Central and Eastern Missouri and McCracken County, KY</p>
				</p>
			</li>
			<li class="even">
				<p>
					<strong>Ken Fortman</strong><br />
					<span>cell:</span> 612-227-3872<br />
					<span>email:</span> <a href="mailto:kenfortman@gmail.com">kenfortman@gmail.com</a>
				</p>
				<p style="padding-left:15%; width:32%; margin-top:30px;">
					<img style="margin-top:0px;" src="<?php Util::getServerHost() ?>images/state-05.png"/>
					Represents Iowa and all of South Dakota
				</p>
			</li>
			<li></li>
			<li class="even">
				<p>
					<strong>Gary Critzer</strong><br />
					<span>cell:</span> 812-350-7390<br />
					<span>fax:</span> 812-799-0823<br />
					<span>email:</span> <a href="mailto:gecritzer@aol.com">gecritzer@aol.com</a>
				</p>
				<p style="padding-left:20%; width:32%;">
					<img src="<?php Util::getServerHost() ?>images/state-03.png"/>
					Represents<br /> Indiana
				</p>
			</li>
			<li></li>
			<li class="even">
				<p>
					<strong>Mike Hatfield</strong><br />
					<span>cell:</span> 816-352-4378<br />
					<span>fax:</span> 816-478-7074<br />
					<span>email:</span> <a href="mailto:mbh1222@gmail.com">mbh1222@gmail.com</a>
				</p>
				<p style="padding-left:10%; width:42%; margin-top:30px">
					<img style="margin-top:0px;" src="<?php Util::getServerHost() ?>images/state-06.png"/>
					Represents Kansas<br />and the Western Counties of Missouri
				</p>
			</li>
		</ol>
	</div>
</div>