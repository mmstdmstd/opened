<?php
$Sku = $Item->getSku();
$BrandId = $Item->getBrandId();
$PhoneOrder = $Item->getPhoneOrder();
$Brand = $Item->getBrand();
$sf_upload_dir = sfConfig::get('sf_upload_dir');
$InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');
$InventoryItems = sfConfig::get('app_application_InventoryItems');
$badges_sku_list = sfConfig::get('app_application_badges_sku_list');

$LoggedUserEmail= Cart::getLoggedUserEmail();
//Util::deb($LoggedUserEmail, '$LoggedUserEmail::');


$DeduggingAccount= false;
$PaymentTestingEmails= sfConfig::get('app_application_PaymentTestingEmails');
//Util::deb($PaymentTestingEmails, '$PaymentTestingEmails::');
if ( in_array($LoggedUserEmail, $PaymentTestingEmails)  ) {
	$DeduggingAccount= true;
}
//Util::deb($DeduggingAccount, '$DeduggingAccount::');

$is_badge_product= false;
if ( $DeduggingAccount ) {
  $is_badge_product = in_array(strtolower($Sku), $badges_sku_list);
}
//Util::deb($is_badge_product, '$is_badge_product::');

$Debug = false;
if (empty($checked_filters)) {
	$checked_filters = '';
}
?>


<?php $Clearance = $Item->getClearance();
//if ($Debug) Util::deb( $Sku, ' $Sku::' );
$ThumbnailImageFile = '';
$UseServerThumbnails = true;
?>

<div class="product_div">
	<?php $HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());

	$ThumbnailImageFullUrl = '';
	if ($ThumbnailImageFile and !is_array($ThumbnailImageFile) and !$UseServerThumbnails) :
		$ThumbnailImageUrl = '/uploads' . $ThumbnailImageFile; // Util::deb( $ThumbnailImageUrl, ' $ThumbnailImageUrl::' );
		$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
		?>
		<div class="product_image_div">
			<a href="<?php echo url_for("@product_details?sku=" . urlencode($Item->getSku()) . '&input_search=' . $input_search .
				'&select_category=' . $select_category .
				'&select_subcategory=' . $select_subcategory .
				'&select_brand_id=' . $select_brand_id .
				'&rows_in_pager=' . $rows_in_pager .
				'&select_brand_id=' . $select_brand_id .
				'&sorting=' . $sorting .
				'&page=' . $page . (!empty($checked_filters) ? '&checked_filters=' . $checked_filters : "")
			) ?>">
				<?php echo image_tag($ThumbnailImageUrl, array('alt' => "", "height" => "150", "width" => "150")); ?>
			</a>
		</div>
	<?php /* echo ( $ImagesBySkuListLenth > 0 ? '<h2>'.$ImagesBySkuListLenth.'</h2>' : '' ); */
	endif; ?>


	<?php if ($ThumbnailImageFile and is_array($ThumbnailImageFile) and !$UseServerThumbnails) :
		$ThumbnailImageUrl = 'uploads' . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
		//if ( $Debug ) Util::deb( $ThumbnailImageUrl, '-11 $ThumbnailImageUrl::' );

		$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
		if ($Debug) Util::deb($ThumbnailImageFullUrl, ' -2 $ThumbnailImageFullUrl::');
		$ThumbnailImageFullFilePath = sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
		if ($Debug) Util::deb($ThumbnailImageFullFilePath, ' $ThumbnailImageFullFilePath::');
		if (empty($ThumbnailImageFile['ImageFileName']) or (!file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath))) {
			$ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage150.png';
		}
		if ($Debug) Util::deb($ThumbnailImageFullUrl, ' -3 $ThumbnailImageFullUrl::');
		$ImageHeight = 150;
		$ImageWidth = 150;
		if (!empty($ThumbnailImageFile['Height']) and !empty($ThumbnailImageFile['Width'])) {
			$ImageHeight = $ThumbnailImageFile['Height'];
			$ImageWidth = $ThumbnailImageFile['Width'];
		}
		$sku = $Item->getSku();
		$sku = preg_replace("/\+/", '__plus', $sku);
		$ProductUrl = url_for("@product_details?sku=" . urlencode($sku) . '&input_search=' . $input_search .
			'&select_category=' . $select_category .
			'&select_subcategory=' . $select_subcategory .
			'&select_brand_id=' . $select_brand_id .
			'&rows_in_pager=' . $rows_in_pager .
			'&select_brand_id=' . $select_brand_id .
			'&page=' . $page . (!empty($checked_filters) ? '&checked_filters=' . $checked_filters : "")
		);
		?>
		<div class="product_image_div">
			<a href="<?php echo $ProductUrl ?>">
				<?php echo image_tag($ThumbnailImageFullUrl, array('alt' => "", "height" => $ImageHeight, "width" => $ImageWidth)); ?>
			</a>
		</div>
	<?php /* echo ( $ImagesBySkuListLenth > 0 ? '<h2>'.$ImagesBySkuListLenth.'</h2>' : '' );*/
	endif; ?>


	<?php if ($UseServerThumbnails) : ?>
		<?php
		$InventoryItemsImage_MaxImages = (int)sfConfig::get('app_application_InventoryItemsImage_MaxImages');
		$sf_upload_dir = sfConfig::get('sf_upload_dir');
		$InventoryItemsThumbnails = sfConfig::get('app_application_InventoryItemsThumbnails');
//        $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
		$ThumbnailFromServer = AppUtils::getThumbnailFromServer($Sku, $InventoryItemsImage_MaxImages, $InventoryItemsThumbnails, $sf_upload_dir, $HostForImage);
		if ($Debug) Util::deb($ThumbnailFromServer, ' -2123 $ThumbnailFromServer::');
		// die("UYTRE");
		$sku_label = preg_replace("/\+/", '__plus', $Sku);
		$ProductUrl = url_for("@product_details?sku=" . urlencode($sku_label) . '&input_search=' . $input_search .
			'&select_category=' . $select_category .
			'&select_subcategory=' . $select_subcategory .
			'&select_brand_id=' . $select_brand_id .
			'&rows_in_pager=' . $rows_in_pager .
			'&select_brand_id=' . $select_brand_id .
			'&page=' . $page . (!empty($checked_filters) ? '&checked_filters=' . $checked_filters : "")
		);
		?>
		<div class="product_image_div">
			<!-- <u><?php // echo $Sku  ?></u> -->
			<a href="<?php echo $ProductUrl ?>">
				<?php           $ThumbnailFromServer = str_replace("+", ' ', $ThumbnailFromServer);
				//Util::deb( $ThumbnailFromServer, '$ThumbnailFromServer::' );
				echo image_tag($ThumbnailFromServer, array()); ?>
			</a>
		</div>
	<?php endif; /* if ( !$UseServerThumbnails ) */ ?>


	<div class="product_title">
		<a href="<?php echo url_for("@product_details?sku=" . urlencode($Item->getSku()) . '&input_search=' . $input_search .
			'&select_category=' . $select_category .
			'&select_subcategory=' . $select_subcategory .
			'&select_brand_id=' . $select_brand_id .
			'&rows_in_pager=' . $rows_in_pager .
			'&select_brand_id=' . $select_brand_id .
			'&sorting=' . $sorting .
			'&page=' . $page . (!empty($checked_filters) ? '&checked_filters=' . $checked_filters : "")
		) ?>"><?php echo htmlspecialchars_decode($Item->getTitleWithRSymbol()) ?></a>
	</div>

	<?php if (!empty($BrandId)) : ?>
		<p class="product_brand">
			<?php echo link_to($Brand, '@product_listings?page=1&select_brand_id=' . $BrandId); ?>
		</p>
	<?php endif; ?>


	<?php if ( $is_badge_product ) {
    echo '<p class="product_price"> Price Varies</p>';
	}  ?>


  <?php if ( !$is_badge_product ) :  ?>

 	<?php $Clearance = $Item->getClearance();
	$SalePrice = $Item->getSalePrice();
	if ($Item->getCurrentlyOnSale() && ($Clearance > 0 || $SalePrice > 0)) {
		$StdUnitPrice = $Item->getStdUnitPrice();
		?>
		<p class="special_product_price">
			<span style="align:left;position:absolute;left:8px;width:80px;">Special</span>
      <span
				style="text-align:center;width:80px;left:65px;position:absolute;font-size:14px;border:0px solid black"><?php // If both clearance and sale_price have values, use the lowest of the two. If sale_start_date or sale_end_date is not NULL then the sale is only valid between those dates (including the start and end date). Also, always check to make sure the price displayed on the page is not $0.00. If it's $0.00, display an mdash in its place.
				$ShownPrice = $Item->getClearance_Or_SalePrice();
				if ($ShownPrice > 0) {
					echo Util::getDigitMoney($ShownPrice, 'Money');
				} else {
					echo '&mdash;';
				}
				?></span>

			<?php if ($StdUnitPrice > 0) : ?>
				<span
					style="text-align:right;text-decoration:line-through;width:70px;right:5px;font-weight:normal;position:absolute;border:0px solid black"><?php echo Util::getDigitMoney($StdUnitPrice, 'Money') ?></span>
			<?php endif; ?>

		</p>
	<?php } else { ?>
		<p class="product_price">
			<span><?php echo Util::getDigitMoney($Item->getStdUnitPrice(), 'Money') ?></span>
		</p>
	<?php } ?>

	<?php endif; /*if ( !$is_badge_product ) :*/ ?>


	<div class="product_details_div">
		<?php
		$sku = $Item->getSku();
		$sku = preg_replace("/\+/", '__plus', $sku);
		?>
		<div class="details_btn_div">
			<a href="<?php echo url_for("@product_details?sku=" . urlencode($sku) . '&input_search=' . $input_search .
				'&select_category=' . $select_category .
				'&select_subcategory=' . $select_subcategory .
				'&select_brand_id=' . $select_brand_id .
				'&rows_in_pager=' . $rows_in_pager .
				'&select_brand_id=' . $select_brand_id .
				'&sorting=' . $sorting .
				'&page=' . $page . (!empty($checked_filters) ? '&checked_filters=' . $checked_filters : "")
			) ?>">

			<?php if ( !$is_badge_product ) :  ?>
				<img class="details_btn" src="<?php echo $HostForImage ?>images/details-btn.jpg" type="image"/>
			<?php endif; /*if ( !$is_badge_product ) :*/ ?>

			</a>
		</div>

		<?php if ( !$is_badge_product ) :  ?>
  		<?php if (!$PhoneOrder) : ?>
	  		<span class="qty_label">Qty:</span>
		  	<input class="qty_in_cart" name="qty_in_cart_<?php echo $Sku ?>" id="qty_in_cart_<?php echo $Sku ?>"
						 value="<?php echo Cart::getValueBySku($Sku, 1, $UsersCartArray) ?>"/>
		  <?php else: ?>
			  <img style="position:absolute; left:70px; top:5px;" class="cart_btn" src="<?php echo $HostForImage ?>images/phone-orders.png" type="image"/>
		  <?php endif; ?>


  		<div class="cart_btn_div">
	  		<?php if (!$PhoneOrder) : ?>
		  		<a onclick="javascript:AddToCart('<?php echo $Sku ?>', event );" style="cursor:pointer;">
			  		<img class="cart_btn" src="<?php echo $HostForImage ?>images/cart-btn.jpg" type="image"/>
				  </a>
			  <?php endif; ?>
		  </div>
		<?php endif; /*if ( !$is_badge_product ) :*/ ?>


    <?php if ( $is_badge_product ) :  ?>
  		<div style="  position:absolute; left:35px; top:5px;  width:143px; height:21px; overflow:hidden;" >
			<a onclick="javascript:RunBadgeProduct('<?php echo $Sku ?>', event );" style="cursor:pointer;">
				<img src="<?php echo $HostForImage ?>images/create-badge-btn.png" type="image"/ >
			</a>
			</div>
		<?php endif; /*if ( $is_badge_product ) :*/ ?>

	</div>
</div><!-- close product div -->

<?php //if ( $Debug )  die('DIE');  ?>