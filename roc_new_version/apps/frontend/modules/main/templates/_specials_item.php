<?php
  $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
  $Sku= $Item->getSku();
  $BrandId= $Item->getBrandId();
  $Brand= $Item->getBrand();
  $sf_upload_dir= sfConfig::get('sf_upload_dir');
  $InventoryItemsThumbnails= sfConfig::get('app_application_InventoryItemsThumbnails');
  $InventoryItems= sfConfig::get('app_application_InventoryItems');
  $Debug= false;
  if ( $Sku== '0-8001-075' ) {
  	$Debug= true;
  }
  //$ImagesBySkuList= AppUtils::getImagesBySku( $Sku, /*$ReturnFullPath=*/ false, /*$ReturnLowestIndex=*/ false, true, true );
  // Util::deb( $ImagesBySkuList, ' $ImagesBySkuList::' );
  //$ImagesBySkuListLenth= count($ImagesBySkuList);
  // Util::deb( $ImagesBySkuListLenth, ' $ImagesBySkuListLenth::' );
?>

<?php $Clearance= $Item->getClearance();
  $SalePrice= $Item->getSalePrice();
  $Msrp= $Item->getMsrp();
  $StdUnitPrice= $Item->getStdUnitPrice();
  //Util::deb( $Clearance, '$Clearance::' );
  //Util::deb( $SalePrice, ' $SalePrice::' );
  $ThumbnailImageFile= '';
  $UseServerThumbnails= true;

  /* $ThumbnailImageFile=  AppUtils::getThumbnailImageBySku($Sku, false);
  // Util::deb( $ThumbnailImageFile, ' $ThumbnailImageFile::' );
  if ( empty($ThumbnailImageFile) ) {
  	$ThumbnailImageFile=  AppUtils::getResizedImageBySku($Sku, false, false, true);
    // Util::deb( $ThumbnailImageFile, ' Resized $ThumbnailImageFile::' );
  }  */
  ?>

    <div class="product_div">
    <?php //if ( $Clearance> 0 ) : ?> <!--
      <div class="clearance_div">
        <span>Clearance</span>
      </div>
    <?php //endif; ?> -->

    <?php $ThumbnailImageFullUrl= '';
    // Util::deb( $ThumbnailImageFile, ' 00$ThumbnailImageFile::' );
    if ( $ThumbnailImageFile and !is_array($ThumbnailImageFile) and !$UseServerThumbnails ) :
      $ThumbnailImageUrl= '/uploads' . $ThumbnailImageFile; // Util::deb( $ThumbnailImageUrl, ' $ThumbnailImageUrl::' );
      $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;
      // Util::deb( $ThumbnailImageFullUrl, ' -1 $ThumbnailImageFullUrl::' );
      ?>
        <div class="product_image_div">
          <a href="<?php echo url_for(  "@product_details?sku=".urlencode( $Item->getSku() ).'&input_search='.$input_search.
      '&select_category=' . $select_category .
      '&select_subcategory=' . $select_subcategory .
      '&select_brand_id='. $select_brand_id .
      '&rows_in_pager='. $rows_in_pager .
      '&select_brand_id='. $select_brand_id .
      '&sorting='. $sorting .
      '&page='. $page
       ) ?>">
            <?php echo image_tag( $ThumbnailImageUrl,array('alt'=>"", "height"=>"150", "width"=>"150") );  ?>
          </a>
        </div>
    <?php /* echo ( $ImagesBySkuListLenth > 0 ? '<h2>'.$ImagesBySkuListLenth.'</h2>' : '' ); */ endif; ?>


    <?php if ( $ThumbnailImageFile and is_array($ThumbnailImageFile) and !$UseServerThumbnails ) :
      $ThumbnailImageUrl= 'uploads'.DIRECTORY_SEPARATOR.$ThumbnailImageFile['ImageFileName_encoded'];
      $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;
      // Util::deb( $ThumbnailImageFullUrl, ' -2 $ThumbnailImageFullUrl::' );
      $ThumbnailImageFullFilePath= sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
      // Util::deb( $ThumbnailImageFullFilePath, ' $ThumbnailImageFullFilePath::' );
      if ( empty($ThumbnailImageFile['ImageFileName']) or ( !file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath) ) ) {
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage150.png' ;
      }
      // Util::deb( $ThumbnailImageFullUrl, ' -3 $ThumbnailImageFullUrl::' );
      $ImageHeight= 150;
      $ImageWidth= 150;
      if ( !empty($ThumbnailImageFile['Height']) and !empty($ThumbnailImageFile['Width']) ) {
        $ImageHeight= $ThumbnailImageFile['Height'];
        $ImageWidth= $ThumbnailImageFile['Width'];
      }
      $sku = $Item->getSku();
      $sku = preg_replace("/\+/", '__plus', $sku);
      $ProductUrl= url_for( "@product_details?sku=". urlencode( $sku ).'&input_search='.$input_search.
      '&select_category=' . $select_category .
      '&select_subcategory=' . $select_subcategory .
      '&select_brand_id='.$select_brand_id .
      '&rows_in_pager='.$rows_in_pager .
      '&select_brand_id='.$select_brand_id .
      '&page='.$page
       );
      ?>
        <div class="product_image_div"  >
          <a href="<?php echo $ProductUrl ?>"  >
          <?php echo image_tag( $ThumbnailImageFullUrl,array('alt'=>"", "height"=>$ImageHeight, "width"=>$ImageWidth  ) ); ?>
          </a>
        </div>
    <?php /* echo ( $ImagesBySkuListLenth > 0 ? '<h2>'.$ImagesBySkuListLenth.'</h2>' : '' );*/ endif;   ?>

    <?php if ( $UseServerThumbnails ) :  ?>
      <?php
        $InventoryItemsImage_MaxImages= (int)sfConfig::get('app_application_InventoryItemsImage_MaxImages' );
        $sf_upload_dir= sfConfig::get('sf_upload_dir');
        $InventoryItemsThumbnails= sfConfig::get('app_application_InventoryItemsThumbnails');
        $ThumbnailFromServer= AppUtils::getThumbnailFromServer( $Sku, $InventoryItemsImage_MaxImages, $InventoryItemsThumbnails, $sf_upload_dir, $HostForImage );

        // Util::deb( $ThumbnailFromServer, ' -2 $ThumbnailFromServer::' );
        $sku_label = preg_replace("/\+/", '__plus', $Sku);
        $ProductUrl= url_for( "@product_details?sku=". urlencode($sku_label) .'&input_search='.$input_search.
        '&select_category=' . $select_category .
        '&select_subcategory=' . $select_subcategory .
        '&select_brand_id='.$select_brand_id .
        '&rows_in_pager='.$rows_in_pager .
        '&select_brand_id='.$select_brand_id .
        '&page='.$page
       );
      ?>
      <div class="product_image_div"  >
        <a href="<?php echo $ProductUrl ?>"  >
        <?php echo image_tag( $ThumbnailFromServer, array() ); ?>
        </a>
      </div>
    <?php endif; /* if ( !$UseServerThumbnails ) */  ?>



    <div class="product_title">
      <a href="<?php echo url_for(  "@product_details?sku=".urlencode($Item->getSku()) .'&input_search='.$input_search.
      '&select_category=' . $select_category .
      '&select_subcategory=' . $select_subcategory .
      '&select_brand_id='.$select_brand_id .
      '&rows_in_pager='.$rows_in_pager .
      '&select_brand_id='.$select_brand_id .
      '&sorting='.$sorting .
      '&page='.$page
       ) ?>"><?php echo htmlspecialchars_decode( $Item->getTitleWithRSymbol() ) ?></a>
    </div>

    <?php if( !empty($BrandId) ) : ?>
    <p class="product_brand">
      <?php echo link_to( $Brand, '@specials?page=1&select_brand_id=' . $BrandId ); ?>
    </p>
    <?php endif; ?>

    <p class="special_product_price">
			<span style="align:left;position:absolute;left:8px;width:80px;">Special</span>
      <span style="text-align:center;width:80px;left:65px;position:absolute;font-size:14px;border:0px solid black"><?php // If both clearance and sale_price have values, use the lowest of the two. If sale_start_date or sale_end_date is not NULL then the sale is only valid between those dates (including the start and end date). Also, always check to make sure the price displayed on the page is not $0.00. If it's $0.00, display an mdash in its place.

      $ShownPrice= $Item->getClearance_Or_SalePrice();
      if ( $ShownPrice> 0 ) {
        echo Util::getDigitMoney( $ShownPrice,'Money' );
      } else {
        echo '&mdash;';
      }
      ?></span>
      <?php if ( $StdUnitPrice> 0 ) :  ?>
        <span style="text-align:right;text-decoration:line-through;width:70px;right:5px;font-weight:normal;position:absolute;border:0px solid black" ><?php echo Util::getDigitMoney($StdUnitPrice,'Money') ?></span>
      <?php endif; ?>
      <?php //$Clearance= $Item->getClearance();
      // if ( $Clearance > 0 ) : ?>
        <!-- ,&nbsp;$<span class="product_price_clearance" style="text-decoration :line-through"><?php //echo Util::getDigitMoney($Clearance,'Money') ?></span> -->
      <?php // endif ?>
    </p>

    <div class="product_details_div">
      <?php
          $sku = $Item->getSku();
          $sku = preg_replace("/\+/", '__plus', $sku);
      ?>
      <div class="details_btn_div">
        <a href="<?php echo url_for(  "@product_details?sku=". urlencode($sku) .'&input_search='.$input_search.
      '&select_category=' . $select_category .
      '&select_subcategory=' . $select_subcategory .
      '&select_brand_id='.$select_brand_id .
      '&rows_in_pager='.$rows_in_pager .
      '&select_brand_id='.$select_brand_id .
      '&sorting='.$sorting .
      '&page='.$page
       ) ?>">
          <img class="details_btn" src="<?php echo $HostForImage ?>images/details-btn.jpg" type="image"  />
        </a>
      </div>

      <span class="qty_label">Qty:</span>
      <input class="qty_in_cart" name="qty_in_cart_<?php echo $Sku ?>" id="qty_in_cart_<?php echo $Sku ?>" value="<?php echo Cart::getValueBySku( $Sku, 1, $UsersCartArray )?>" />
      <div class="cart_btn_div">
        <a onclick="javascript:AddToCart('<?php echo $Sku ?>', event );" style="cursor:pointer;" >
          <img class="cart_btn"  src="<?php echo $HostForImage ?>images/cart-btn.jpg" type="image"  />
        </a>
      </div>
    </div>
  </div><!-- close product div -->

