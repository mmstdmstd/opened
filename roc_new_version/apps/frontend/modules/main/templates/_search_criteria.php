<?php
if ( empty($page_type) ) $page_type= 'product_listings';

$ModuleName= $sf_context->getModuleName();
$ActionName= $sf_context->getActionName();
$FirstSearch= '';
$SecondSearch= '';
$ThirdSearch= '';
$TopBannerDivClass= "search-panel-on-cart-page";
$search_flag = 1;

if (empty($select_category) and empty($select_subcategory) and empty($input_search) ) {
    $search_flag = 0;
}
if ( $page_type== 'specials' ) {
  $search_flag = 1;
  $TopBannerDivClass= "when-search-applied";
}
// Util::deb( $search_flag, '$search_flag::' );
$select_category_code= $select_category;
$select_subcategory_code= $select_subcategory;

if ( !empty($lInventoryCategory) and empty($select_category) and empty($select_subcategory) ) {
    $select_category    = $lInventoryCategory->getCategory();
    $select_subcategory = $lInventoryCategory->getSubcategory();
}

if ( !empty($select_category) ) {
  $FirstSearch= link_to(__($select_category), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_category=' . urlencode($select_category) );
  $TopBannerDivClass= "when-search-applied";
}
if ( !empty($select_subcategory) ) {
  $SecondSearch= link_to(__($select_subcategory), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_subcategory=' . urlencode($select_subcategory ) );
  $TopBannerDivClass= "when-search-applied";
}
if ( !empty($input_search) ) {
  $ThirdSearch= link_to(__($input_search), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&input_search=' . urlencode($input_search ) );
  $TopBannerDivClass= "when-search-applied";
}
if (!empty($select_brand_id)) {
   $TopBannerDivClass= "when-search-applied";
}

?>

<?php if ( $page_type== 'specials' ) : ?>
<style type="text/css">
	#top_banner_div {background-image:url(<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/top-banner-2-specials.jpg) !important;}
	.product-sucess {background-color:#ed1b24 !important; color:#fff !important;}
	.special {display:inline-block; font-zie:80%; font-weight:normal; color:#fff;}
</style>
<?php endif ?>

  <div id="page_body_prod_detail_div">
    <div id="top_banner_div" class="<?php echo $TopBannerDivClass ?>" ><!-- top horizontal banner -->
    <!-- <textarea name="textarea_sizes" id="textarea_sizes">!!</textarea>  -->
      <span id="search_label">SEARCH</span>
			<!--<a id="advanced_search" href="#">Advanced Search</a>-->
			<?php if ( $ModuleName== 'main' and $ActionName== 'product_details' ) : ?>
            <?php if ($search_flag == 1) { ?>
			<a id="back-to-search-results" href="<?php echo url_for( '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_brand_id=' . urlencode($select_brand_id)
			.'&input_search=' . urlencode($input_search)
			.'&select_category=' . urlencode($select_category)
			.'&select_subcategory=' . urlencode($select_subcategory)
			.'&sorting=' . urlencode($sorting))  ?>">< Back to Search Results</a>
            <?php } ?>
			<?php endif ?>

      <?php  if ( !empty($select_category) or !empty($select_subcategory) or !empty($select_brand_id) or $page_type== 'specials' ) : ?>
			<p id="search_breadcrumb">
        <?php if ( $page_type== 'specials' ) :
          echo link_to( 'Specials', '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager );
        endif; ?>
        <?php if ( $page_type== 'specials' and ( !empty($select_category) or !empty($select_subcategory) or !empty($input_search) )   ) : ?>
          >
        <?php endif ?>

        <?php if ( !empty($select_category) ) : ?>
          <?php $FirstSearch= link_to(__($select_category), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_category=' . urlencode($select_category) ) ?>
  				<?php echo link_to(__($select_category), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_category=' . urlencode($select_category) ) ?>
        <?php endif; ?>

        <?php if (  !empty($select_category) and !empty($select_subcategory)  ) : ?>
          >
        <?php endif ?>

        <?php if ( !empty($select_subcategory) ) : ?>
          <?php $SecondSearch= link_to(__($select_subcategory), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_subcategory=' . urlencode($select_subcategory ) ) ?>
				  <?php echo link_to(__($select_subcategory), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_subcategory=' . urlencode($select_subcategory ) ) ?>
        <?php endif; ?>

        <?php if (  ( !empty($select_category) or !empty($select_subcategory) ) and !empty($select_brand_id) ) : ?>
          >
        <?php endif; ?>

        <?php if ( !empty($select_brand_id) ) : ?>
          <?php $lSelectedBrand= BrandPeer::retrieveByPK($select_brand_id);
          if ( !empty($lSelectedBrand) ) {
            echo link_to( __($lSelectedBrand->getTitle() ), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&select_brand_id=' . urlencode($select_brand_id ) );
          }  ?>
        <?php endif; ?>

        <?php  if (  ( !empty($select_category) or !empty($select_subcategory) or !empty($select_brand_id) ) and !empty($input_search) ) : ?>
          >
        <?php endif; ?>

        <?php  if ( !empty($input_search) ) : ?>
          <?php  $ThirdSearch= link_to(__($input_search), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&input_search=' . urlencode($input_search ) ) ?>
          <?php  echo link_to(__($input_search), '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&input_search=' . urlencode($input_search ) )  ?>
        <?php  endif ?>

        <?php if ( ( $ModuleName == "main" and $ActionName== "specials") or $page_type== 'specials' ) : ?>
          <span style=" text-align:right; position:absolute; left:685px;width:185px; border:0px dotted"><?php echo link_to( "View All Products", '@product_listings') ?></span>
        <?php  endif ?>

			</p>
      <?php endif; ?>

			<?php if ( $ModuleName== 'main' and $ActionName== ''.$page_type.'' ) : ?>

      <?php
          if (!empty($FirstSearch) or !empty($SecondSearch) or !empty($ThirdSearch)  ) {
              $search_content = '';
              $search_content = '<p id="you-searched-for">You Searched';
              if (!empty($ThirdSearch)) {
                  $search_content  = '<p id="you-searched-for">You searched for';
                  $search_content .= '&nbsp;<strong id="">' . $ThirdSearch . '</strong>';
                  if (!empty($FirstSearch)) {
                      $search_content .= '&nbsp;in&nbsp;<strong id="searched-category">' . $FirstSearch . '</strong>';
                  }
                  if (!empty($SecondSearch)) {
                      $search_content .= '/<strong id="current-search-phrase">' . $SecondSearch . '</strong>';
                  }
                  $search_content .= '</p>';
              }
              else if (empty($ThirdSearch)) {
                  if (!empty($FirstSearch)) {
                      $search_content .= '<strong id="searched-category">&nbsp;' . $FirstSearch . '</strong>';
                  }
                  if (!empty($SecondSearch)) {
                      $search_content .= '/<strong id="current-search-phrase">' . $SecondSearch . '</strong>';
                  }
                  $search_content .= '</p>';
              }
              echo $search_content;
          }
      ?>

      <?php endif ?>

			<!-- <a id="back-to-search-results" href="#">< Back to Search Results</a>                      -->

        <?php $CategoriesList= Util::SetArrayHeader( array( ''=>'  -All Categories-  '), InventoryCategoryPeer::getCategoriesList( true ) ) ;
        echo Util::select_tag('select_category', $CategoriesList, $select_category_code, array( 'onchange'=>'SelectCategoryonChange(this.value)', 'style'=>'z-index:98' ) ); ?>

        <?php $SubcategoriesList= Util::SetArrayHeader( array( ''=>'  -All Subcategories-  '), InventoryCategoryPeer::getSubcategoriesList($select_category, true) ) ;
        //$SubcategoriesList = Util::SetArrayHeader(array( '' => '  -All Subcategories-  '));
        echo Util::select_tag('select_subcategory', $SubcategoriesList, $select_subcategory_code, array( 'style'=>'z-index:98' ) ); ?>&nbsp;



      <div id="go_div">
        <input name="input_search" maxlength="100" size="40" id="input_search" value="<?php echo $input_search ?>" />
        <div id="go_btn2_div">
         <input id="go_btn2" type="image" value="" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/go_btn.jpg" onclick="javascript:onSubmit(); return false;" />
        </div>
      </div>

    </div><!-- close top horizontal banner -->

   </div> <!-- close div = page_body_prod_detail_div  -->
   <div style="clear:both;"></div>

 <script type="text/javascript">
jQuery(document).ready(function() {
    var selected_sub_cat = '<?php echo $select_subcategory ?>';
    setTimeout(function() {
        jQuery('#select_subcategory [value="<?php echo $select_subcategory ?>"]').attr("selected", "selected");
    }, 1000);
});
</script>
