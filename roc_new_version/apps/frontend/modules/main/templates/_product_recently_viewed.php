<p id="sub_heading_2"> RECENTLY VIEWED</p>
<?php foreach( $RecentlyViewedList as $RecentlyViewedSku  ) : ?>

  <?php
  $RecentlyViewedProduct= InventoryItemPeer::getSimilarInventoryItem( $RecentlyViewedSku, true );
  if ( empty($RecentlyViewedProduct) )continue;
  $Title= trim( $RecentlyViewedProduct->getTitleWithRSymbol() );
  $ThumbnailImageFile= '';

  //if (empty($Title)) continue;
  $sf_upload_dir= sfConfig::get('sf_upload_dir');
  $InventoryItemsThumbnails= sfConfig::get('app_application_InventoryItemsThumbnails');
  $InventoryItems= sfConfig::get('app_application_InventoryItems');
  $ThumbnailImageFile=  AppUtils::getThumbnailImageBySku($RecentlyViewedSku, false);
  if ( empty($ThumbnailImageFile) ) {
  	$ThumbnailImageFile=  AppUtils::getResizedImageBySku($RecentlyViewedSku, false, false, false);
    if ( is_array($ThumbnailImageFile) and empty($ThumbnailImageFile['ImageFileName']) ) {
    	$ThumbnailImageFile= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage79.png' ;
    }
  }
  ?>

    <?php
    $ThumbnailImageFullUrl= '';
    if ( !empty($ThumbnailImageFile) and !is_array($ThumbnailImageFile) ) :
      $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'uploads' . $ThumbnailImageFile;
      $ThumbnailImageFullFilePath= sfConfig::get('sf_upload_dir') . $ThumbnailImageFile;
      if ( !file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath)) {
       	$ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage79.png' ;
      }

      ?>
      <a href="<?php echo url_for(  "@product_details?sku=".urlencode($RecentlyViewedSku)  ) ?>">
        <img height="78" width="79" alt="" src="<?php echo $ThumbnailImageFullUrl ?>" /><br />
      </a>
    <?php endif; ?>

    <?php if ( $ThumbnailImageFile and is_array($ThumbnailImageFile) and !empty($ThumbnailImageFile['ImageFileName']) ) :
      $ThumbnailImageUrl= 'uploads'.DIRECTORY_SEPARATOR.$ThumbnailImageFile['ImageFileName'];
      $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) .'/uploads/'. $ThumbnailImageFile['ImageFileName'];
      $ThumbnailImageFullFilePath= sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];

      if ( !file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath)) {
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage79.png' ;
      }
      ?>

      <a href="<?php echo url_for(  "@product_details?sku=".urlencode($RecentlyViewedSku)  ) ?>">
        <img height="79" width="79" alt="" src="<?php echo $ThumbnailImageFullUrl ?>" /><br />
      </a>

    <?php endif;  ?>
    <a href="<?php echo url_for(  "@product_details?sku=".urlencode($RecentlyViewedSku)  ) ?>">
      <p class="recently_viewed_description"><?php echo htmlspecialchars_decode( $Title ) ?></p>
    </a>

<?php endforeach ?>