      <div id="menu_div">
        <ul id="menu_ul">
          <li id="inline1" style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-home-press.jpg');" >
            <a href="<?php echo url_for('@homepage') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-home.jpg" width="135" height="27" alt="Home" />
            </a>

          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-company-press.jpg');" >
            <a href="<?php echo url_for('@company') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-company.jpg" width="135" height="27" alt="Home" />
            </a>
          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-salesperson-press.jpg');" >
            <a href="<?php echo url_for('@salesperson') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-salesperson.jpg" width="135" height="27" alt="Home" />

            </a>
          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-support-press.jpg');" >
            <a href="<?php echo url_for('@support') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-support.jpg" width="135" height="27" alt="Home" />
            </a>
          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-contact-press.jpg');" >
            <a href="<?php echo url_for('@contact') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-contact.jpg" width="135" height="27" alt="Home" />
            </a>
          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-products-press.jpg');" >
            <a href="<?php echo url_for('@products') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-products.jpg" width="135" height="27" alt="Home" />
            </a>
          </li>
          <li style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-specials-press.jpg');" >
            <a href="<?php echo url_for('@specials') ?>" style="display:block">
              <img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/menu-btn-specials.jpg" width="135" height="27" alt="Home" />
            </a>
          </li>
        </ul>
      </div><!-- Close menu div -->
