<div id="policy_div" >
  <p id="taser-thank">Thank you!</p>
  <p id="taser-thank1">Your T&E information has been sent.<br />We will be in contact with you.</p>
      <!-- <div id="firearm-note2"><span style="color:#f00">Important:&nbsp;</span><br /> Required paperwork needed before T&E can be processed. Please download, fill out, and submit the DCS paperwork form. You can download the form here:<br /><a href="<?php echo Util::getServerHost(sfContext::getInstance()->getConfiguration(), false) ?>uploads/PaperWorkDirectory/DCS.pdf"><span style="color:#000000">Department Certifying Statement</span></a>&nbsp;<span style="font-weight:normal">[PDF 623KB]</div>  -->
  <p id="taser-thank2">If you have any questions please call us at<br />1-800-223-2097.</p>
  <div id="taser-thank3">
    <a href="<?php echo Util::getServerHost(sfContext::getInstance()->getConfiguration(), false) ?>">Proceed to Home Page</a>
  </div>
</div><!-- close policy_div-->