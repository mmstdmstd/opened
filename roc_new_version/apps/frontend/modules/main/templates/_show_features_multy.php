<?php 
  $Features= $Product->getFeatures();
  $InventoryCategory= $Product->getInventoryCategory();
  $Size= $Product->getSize();  
  $Color= $Product->getColor();
  $Gender= $Product->getGender();
  $DescriptionShort= $Product->getDescriptionShort();
  $Sizing= $Product->getSizing();
  $Options= $Product->getOptions();  
  $Paperwork= $Product->getPaperwork();  
  $UnitMeasure= $Product->getUnitMeasure();
  $QtyOnHand= $Product->getQtyOnHand();
  $StdUnitPrice= $Product->getStdUnitPrice();
  $Msrp= $Product->getMsrp();  
  $SaleStartDate= $Product->getSaleStartDate();
  $SaleEndDate= $Product->getSaleEndDate();
  $TaxClass= $Product->getTaxClass();
  $ShipWeight= $Product->getShipWeight();
  $SalePromoDiscount= $Product->getSalePromoDiscount();  
  $Finish= $Product->getFinish();
  $SetupCharge= $Product->getSetupCharge();    
  $SaleMethod= $Product->getSaleMethod();  
  $SalePrice= $Product->getSalePrice();
  $SalePercent= $Product->getSalePercent();  
// Util::deb( $Matrix, ' $Matrix::' );
  //if ( $HasRootItem and $Matrix and !empty($RootItemObject) ) {
      if (!empty($HasRootItem) and !empty($Matrix) and !empty($RootItemObject) ) {
    $Features= $RootItemObject->getFeatures();
    $InventoryCategory= $RootItemObject->getInventoryCategory();
    $Size= $RootItemObject->getSize();  
    $Color= $RootItemObject->getColor();
    $Gender= $RootItemObject->getGender();
    $DescriptionShort= $RootItemObject->getDescriptionShort();
    $Sizing= $RootItemObject->getSizing();
    $Options= $RootItemObject->getOptions();  
    $Paperwork= $RootItemObject->getPaperwork();  
    $UnitMeasure= $RootItemObject->getUnitMeasure();
    $QtyOnHand= $RootItemObject->getQtyOnHand();
    $StdUnitPrice= $RootItemObject->getStdUnitPrice();
    $Msrp= $RootItemObject->getMsrp();  
    $SaleStartDate= $RootItemObject->getSaleStartDate();
    $SaleEndDate= $RootItemObject->getSaleEndDate();
    $TaxClass= $RootItemObject->getTaxClass();
    $ShipWeight= $RootItemObject->getShipWeight();
    $SalePromoDiscount= $RootItemObject->getSalePromoDiscount();  
    $Finish= $RootItemObject->getFinish();
    $SetupCharge= $RootItemObject->getSetupCharge(); 
    $SaleMethod= $RootItemObject->getSaleMethod();  
    $SalePrice= $RootItemObject->getSalePrice();
    $SalePercent= $RootItemObject->getSalePercent();  
  }
  $lInventoryCategory= InventoryCategoryPeer::getSimilarInventoryCategory( $InventoryCategory );  
  $InventoryCategoryText= '';
  if ( !empty($lInventoryCategory) ) {
  	$InventoryCategoryText= $lInventoryCategory->getCategory();
  }
        
  $features_multi_array = array();
  $result_colors_list = array();
  $result_size_list = array(); 
  $result_options_list = array();
  $result_hands_list = array();
  $FeaturesList= preg_split( '/,/', $Features);
  // uasort( $FeaturesList, 'cmp' );
  // Util::deb( $FeaturesList, '$FeaturesList::' );
  $UsedFeaturesList= array();
  // Util::deb( $UsedFeaturesList, ' $UsedFeaturesList::' );
?>

  <ul id="select_size">
  <?php foreach( $FeaturesList as $NextFeature ) : $NextFeature= strtolower(trim($NextFeature)); $UsedFeaturesList[]= $NextFeature;   ?>
    <?php if ( $NextFeature == 'inventory_category' ) : ?>
      <?php if ( !empty($InventoryCategoryText) ) : ?>
        <li>
          <?php echo $InventoryCategoryText ?>
        </li> 
      <?php endif; ?>        
    <?php endif; ?>
      
    <?php
      
      //Util::deb($NextFeature, '$NextFeature::');
      if ($NextFeature == 'size') {          

        $SizesList= InventoryItemPeer::getSizesListByRootItem($RootItem);
        $SizesList= AppUtils::getSizeOrderedArray( $SizesList );                 
         
         Util::deb( $SizesList, '$SizesList::' );
        
        $result_size_list = array();
        if (!empty($SizesList)) {
          foreach($SizesList as $key => $value) {
            $IsFound= false;
            for ($i = 0; $i < count($features_multi_array); $i++) {
              if ($features_multi_array[$i]['sku'] == $key) {
                $features_multi_array[$i]['size'] = $value;
                $IsFound= true;
                break;
              }
            }
            if ( !$IsFound ) {
              $img = get_image_by_sku($key);
              $features_multi_array[]= array('sku' => $key, 'size' => $value, 'color' => '', 'option' => '', 'hand' => '', 'image' => $img);              
            }
                                   
            $result_size_list[$key] = $value;
          }  // foreach($SizesList as $key => $value) {
          echo '<li>';
          echo '<b>Size:</b><br />' . Util::select_tag('select_size_', Util::SetArrayHeader(array(''=>' -select- '), /*$result_size_list*/ array() ), '', array('onchange'=>'javascript:SelectFeatures(\'size\'); return false;')) . '&nbsp;&nbsp'; 
          if (!empty($Sizing)) {
            echo '<span>';  
            $Src= '<a class="thickbox" href="'.url_for( "@product_size_chart?code=".$Sizing ) . '">Size Chart</a> ';
            echo $Src;
            echo '</span>';
          }
          echo '</li><br />';
        } // if (!empty($SizesList)) {             
      } //if ($NextFeature == 'size') {
          
      
      if ($NextFeature == 'color') {          
        $ColorsList= InventoryItemPeer::getColorsListByRootItem($RootItem);
         Util::deb( $ColorsList, '$ColorsList::' );
        if (!empty($ColorsList)) {
          $result_colors_list = array();
          foreach ($ColorsList as $key => $value) {
            $IsFound= false;
            for ($i = 0; $i < count($features_multi_array); $i++) {
              if ($features_multi_array[$i]['sku'] == $key) {
                $features_multi_array[$i]['color'] = $value;
                $IsFound= true;
                break;
              }
            }
            if ( !$IsFound ) {
              $img = get_image_by_sku($key);
              $features_multi_array[]= array('sku' => $key, 'size' => '', 'color' => $value, 'option' => '', 'hand' => '', 'image' => $img);              
            }
            $result_colors_list[$key] = $value;
          } // foreach ($ColorsList as $key => $value) {
          echo '<li id="li_color" >';
          echo '<b>Color:</b><br />' . Util::select_tag('select_color', Util::SetArrayHeader( array( ''=>' -select- ' ), /*$result_colors_list*/ array() ), '', array('onchange'=>'javascript:SelectFeatures(\'color\'); return false;'));
          echo '</li>';
        } // if (!empty($ColorsList)) {
      } //if ($NextFeature == 'color') {
        
      if ($NextFeature == 'options') {          
        $OptionsList= InventoryItemPeer::getOptionsListByRootItem($RootItem);
        Util::deb( $OptionsList, '$OptionsList::' );
        if (!empty($OptionsList)) {
          $result_Options_list = array();
          foreach ($OptionsList as $key => $value) {
            $IsFound= false;
            for ($i = 0; $i < count($features_multi_array); $i++) {
              if ($features_multi_array[$i]['sku'] == $key) {
                $features_multi_array[$i]['option'] = $value;
                $IsFound= true;
                break;
                //Util::deb( $features_multi_array, '????? $features_multi_array::' );
              }
            }
            if ( !$IsFound ) {
              $img = get_image_by_sku($key);
              $features_multi_array[]= array('sku' => $key, 'size' => '', 'color' => '', 'option' => $value, 'hand' => '', 'image' => $img);              
            }
            $result_options_list[$key] = $value;
          } // foreach ($OptionsList as $key => $value) {
          echo '<li id="li_option" >';
          echo '<b>Option:</b><br />' . Util::select_tag('select_option', Util::SetArrayHeader( array( ''=>' -select- ' ), /*$result_options_list*/ array() ), '', array('onchange'=>'javascript:SelectFeatures(\'option\'); return false;'));
          echo '</li>';
        } // if (!empty($OptionsList)) {
      } //if ($NextFeature == 'options') {

      if ($NextFeature == 'hand') {          
        // break;
        $HandsList= InventoryItemPeer::getHandsListByRootItem($RootItem);
        if (!empty($HandsList)) {
          $result_hands_list = array();
          foreach ($HandsList as $key => $value) {
            $IsFound= false;
            for ($i = 0; $i < count($features_multi_array); $i++) {
              if ($features_multi_array[$i]['sku'] == $key) {
                $features_multi_array[$i]['hand'] = $value;
                $IsFound= true;
                break;
                      //Util::deb( $features_multi_array, '????? $features_multi_array::' );
              }
            }
            if ( !$IsFound ) {
              $img = get_image_by_sku($key);
              $features_multi_array[]= array('sku' => $key, 'size' => '', 'color' => '', 'option' => '', 'hand' => $value, 'image' => $img);
            }
            $result_hands_list[$key] = $value;
          } // foreach ($HandsList as $key => $value) {
          echo '<li id="li_hand" >';
          echo '<b>Hand:</b><br />' . Util::select_tag('select_hand', Util::SetArrayHeader( array( ''=>' -select- ' ), /*$result_hands_list*/ array() ), '', array('onchange'=>'javascript:SelectFeatures(\'hand\'); return false;'));
          echo '</li>';
        } // if (!empty($HandsList)) {
      } //if ($NextFeature == 'hand') {

      
      if ($NextFeature == 'color' and !in_array('size', $FeaturesList)) {
        break;
            //die("INSIDE 1");
              $ColorsList = InventoryItemPeer::getColorsListByRootItem($RootItem);
              $ColorsList = array_unique($ColorsList);
              if (!empty($ColorsList)) {
                  echo '<li id="li_color" >';
                  echo '<b>Color:</b><br />' . Util::select_tag('select_color', Util::SetArrayHeader( array( ''=>' -select- ' ), $ColorsList), '', array('onchange'=>'javascript:SelectFeatures(\'color\'); return false;'));
                  echo '</li>';
              }
          }
          if (!in_array('size', $FeaturesList)) {
              echo '<li>';
              if (!empty($Sizing)) {
                  echo '<span>';  
                  $Src= '<a class="thickbox" href="'.url_for( "@product_size_chart?code=".$Sizing ) . '">Size Chart</a> ';
                  echo $Src;
                  echo '</span>';
              }
              echo '</li><br />';
      }      
      ?>                      
                  
      
    <?php endforeach;  // Util::deb( $features_multi_array, 'LAST $features_multi_array::' );  ?>
    </ul>
    
<script type="text/javascript">
  
  jQuery(function($) {  
    repopulate_sizes( '', '', '', '', '' );
    repopulate_colors( '', '', '', '', '' );
    repopulate_options( '', '', '', '', '' );
    repopulate_hands( '', '', '', '', '' );
    // alert("AFTER!!")
  });
  
  var featureses = jQuery.parseJSON(<?php /*Util::deb( $features_multi_array, '$features_multi_array::' );    die("!!!"); */ print json_encode(json_encode($features_multi_array)); ?>);
  var FeaturesList = jQuery.parseJSON(<?php print json_encode(json_encode( $FeaturesList )); ?>);
    alert(  "FeaturesList::"+var_dump(FeaturesList)  )
  function SelectFeatures(type) {
    // alert("SelectFeatures type::"+type)    
    var sku   = '';
    var color = '';
    var option   = '';
    var hand = '';
    var image = ''; 
    if (type == 'size') {
      sku = jQuery('#select_size_ option:selected').val();
      var size = jQuery('#select_size_ option:selected').text();
      
      option = jQuery('#select_option option:selected').text();
      option = jQuery.trim(option);
      hand = jQuery('#select_hand option:selected').text();
      hand = jQuery.trim(hand);
      
      if (sku !='') {
        repopulate_colors( sku, size, option, hand, 'size' ); // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand )
        repopulate_options( sku, size, option, hand, 'size' ); // repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand )
        repopulate_hands( sku, size, color, option, 'size' ); // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption )
        for (i = 0; i < featureses.length; i++) {
          if ( featureses[i].sku == sku ) {
            color = featureses[i].color;
            image = featureses[i].image;
          }
        }
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_colors( '', '', '', '', 'size' );
        repopulate_options( '', '', '', '', 'size' );
        repopulate_hands( '', '', '', '', 'size' );
      }
    } // if (type == 'size') {
        
    if (type == 'color') {
      color = jQuery('#select_color option:selected').text();
      sku   = jQuery('#select_color option:selected').val();
      color = jQuery.trim(color);

      size = jQuery('#select_size_ option:selected').text();
      size = jQuery.trim(size);

      option = jQuery('#select_option option:selected').text();
      option = jQuery.trim(option);
      hand = jQuery('#select_hand option:selected').text();
      hand = jQuery.trim(hand);

      if (color != jQuery.trim('-select-')) {
        repopulate_sizes( sku, color, option, hand, 'color' ); // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand )
        repopulate_options( sku, size, option, hand, 'color' ); //  repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand )
        repopulate_hands( sku, size, color, option, 'color' ); // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption )
        for (i = 0; i < featureses.length; i++) {
          // alert( "featureses[i].color::"+ featureses[i].color + "  color::"+color)
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        // repopulate_colors();
        repopulate_sizes( '', '', '', '', 'color' );
        repopulate_options( '', '', '', '', 'color' );
        repopulate_hands( '', '', '', '', 'color' );
      }
    } // if (type == 'color') {


    if (type == 'option') {
      option = jQuery('#select_option option:selected').text();
      sku   = jQuery('#select_option option:selected').val();
      option = jQuery.trim(option);
      
      size = jQuery('#select_size_ option:selected').text();
      size = jQuery.trim(size);

      option = jQuery('#select_option option:selected').text();
      option = jQuery.trim(option);
      hand = jQuery('#select_hand option:selected').text();
      hand = jQuery.trim(hand);
      
      if (option != jQuery.trim('-select-')) {
        repopulate_sizes( sku, color, option, hand, 'option' );  // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand )
        repopulate_colors( sku, size, option, hand, 'option' );  // repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand )
        repopulate_hands( sku, size, color, option, 'option' );  // repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption )
        for (i = 0; i < featureses.length; i++) {
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_sizes( '', '', '', '', 'option' );
        repopulate_colors( '', '', '', '', 'option' );
        repopulate_hands( '', '', '', '', 'option' );
      }
    } // if (type == 'option') {

    if (type == 'hand') {
      hand = jQuery('#select_hand option:selected').text();
      sku   = jQuery('#select_hand option:selected').val();
      hand = jQuery.trim(hand);

      size = jQuery('#select_size_ option:selected').text();
      size = jQuery.trim(size);

      option = jQuery('#select_option option:selected').text();
      option = jQuery.trim(option);
      hand = jQuery('#select_hand option:selected').text();
      hand = jQuery.trim(hand);      
      if (hand != jQuery.trim('-select-')) {
        repopulate_sizes( sku, color, option, hand, 'hand' );  // repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand )
        repopulate_colors( sku, size, option, hand, 'hand' );  //  repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand )
        repopulate_options( sku, size, option, hand, 'hand' ); //  repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand )
        for (i = 0; i < featureses.length; i++) {
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_hands( '', '', '', '', 'hand' );
        repopulate_colors( '', '', '', '', 'hand' );
        repopulate_options( '', '', '', '', 'hand' );
      }
    } // if (type == 'hand') {

  } // function SelectFeatures(type) {
    
    function repopulate_colors( SelectedSku, SelectedSize, SelectedOption, SelectedHand, SelectionType )
    {
      var temp_colors     = jQuery.parseJSON(<?php print json_encode(json_encode($result_colors_list)); ?>);        
       alert(  " repopulate_colors  temp_colors::"+var_dump(temp_colors)  + "  SelectedSku::"+SelectedSku+"  SelectedSize::"+SelectedSize+"  SelectedOption::"+SelectedOption+"  SelectedHand::"+SelectedHand + 
       "  SelectionType::"+SelectionType )
      var rep_colors_list = '<option value=""> -select-</option>';         
      var filled_colors= Array();        
      var CurrentColorValue = jQuery('#select_color option:selected').text();
      // alert( "CurrentColorValue::"+CurrentColorValue )
      
      jQuery.each(temp_colors, function(i, val) {
        IsSkip= false;        
        if ( SelectedSize!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].size != SelectedSize  ) {
              IsSkip= true;
              break;
            }
          }
        }

        IsSkipOption= false;        
        if ( SelectedOption!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].option != SelectedOption  ) {
              IsSkipOption= true;
              break;
            }
          }
        }

        IsSkipHand= false;        
        if ( SelectedHand!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].hand != SelectedHand  ) {
              IsSkipHand= true;
              break;
            }
          }
        }

        var IsFilled= false
        for( J= 0; J< filled_colors.length;J++  ) {            
          if ( filled_colors[J] == val ) {
            IsFilled= true;
            break;
          }
        }
        //alert( "val::" + val + "   CurrentColorValue::" + CurrentColorValue )
        if ( !IsFilled && ( !IsSkip || !IsSkipOption || !IsSkipHand ) ) {
          var SelectedTag= ""
                              
          if ( CurrentColorValue == val ) {
            SelectedTag= " selected "
          }
          rep_colors_list += '<option value="'+ i +'" '+SelectedTag+' >'+ val +'</option>';
          filled_colors[filled_colors.length]= val;
        }
      });   // alert(" rep_colors_list::"+rep_colors_list )
      jQuery('#select_color').html(rep_colors_list);
    }

    function repopulate_sizes( SelectedSku, SelectedColor, SelectedOption, SelectedHand, SelectionType )
    {
      var temp_sizes     = jQuery.parseJSON(<?php print json_encode(json_encode($result_size_list)); ?>);
       alert("repopulate_sizes temp_sizes::"+var_dump(temp_sizes) + "  SelectedSku::"+SelectedSku+"  SelectedColor::"+SelectedColor+"  SelectedOption::"+SelectedOption+"  SelectedHand::"+SelectedHand + 
       "  SelectionType::"+SelectionType )
      var rep_sizes_list = '<option value=""> -select-</option>';
      var filled_sizes= Array();        
      //alert(-1)
      var CurrentSizeValue = jQuery('#select_size_ option:selected').text();      
      // alert( "CurrentSizeValue::"+CurrentSizeValue )
       
      jQuery.each(temp_sizes, function(i, val) {
        // alert( "++ i::"+i + "    val::" + val )
        var IsSkipColor= Array.indexOf(FeaturesList, "color" ) == -1 ;
        //alert("IsSkipColor: " + IsSkipColor);
        if ( SelectedColor!= "" && !IsSkipColor ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].color != SelectedColor  ) {
              IsSkipColor= true;
              break;
            }
          }
        }

        var IsSkipOption= Array.indexOf(FeaturesList, "options" ) == -1 ;
        // alert("IsSkipOption: " + IsSkipOption);
        if ( SelectedOption!= "" && !IsSkipOption ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].option != SelectedOption  ) {
              IsSkipOption= true;
              break;
            }
          }
        }

        var IsSkipHand= Array.indexOf(FeaturesList, "hand" ) == -1 ;
        //alert("IsSkipHand: " + IsSkipHand);
        if ( SelectedHand!= "" && !IsSkipHand ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].hand != SelectedHand  ) {
              IsSkipHand= true;
              break;
            }
          }
        }

        var IsFilled= false
        for( J= 0; J< filled_sizes.length; J++  ) {
          if ( filled_sizes[J] == val ) {
            IsFilled= true;
            break;
          }
        }
        
         //alert( "i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsSkipColor::" + IsSkipColor 
         // + "  IsSkipOption::" + IsSkipOption  + "  IsSkipHand::" + IsSkipHand )

        if ( !IsFilled && ( !IsSkipColor || !IsSkipOption || !IsSkipHand ) ) {

          //alert( "INSIDE !!! i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsSkipColor::" + IsSkipColor 
          //+ "  IsSkipOption::" + IsSkipOption  + "  IsSkipHand::" + IsSkipHand )

          var SelectedTag= ""
          // alert( "val::" + val + "   CurrentSizeValue::" + CurrentSizeValue )
          if ( CurrentSizeValue == val ) {
            SelectedTag= " selected "
          }
          rep_sizes_list += '<option value="'+ i +'" '+SelectedTag+' >'+ val +'</option>';
          filled_sizes[filled_sizes.length]= val;
        }
      });
      // alert(" rep_sizes_list::"+rep_sizes_list )
      jQuery('#select_size_').html(rep_sizes_list);
    }
    
    function repopulate_options( SelectedSku, SelectedSize, SelectedColor, SelectedHand, SelectionType )
    {

      var temp_options     = jQuery.parseJSON(<?php print json_encode(json_encode($result_options_list)); ?>);
       alert("repopulate_options temp_options::"+var_dump(temp_options) + "  SelectedSku::"+SelectedSku+"  SelectedSize::"+SelectedSize    +"  SelectedColor::"+SelectedColor  +"  SelectedHand::"+SelectedHand + 
       "  SelectionType::"+SelectionType  )
      var rep_options_list = '<option value=""> -select-</option>';
      var filled_options= Array();        
      //alert(-1)
      var CurrentOptionValue = jQuery('#select_option_ option:selected').text();      
      alert( "CurrentOptionValue::"+CurrentOptionValue )
       
      jQuery.each(temp_options, function(i, val) {
         alert( "++ i::"+i + "    val::" + val )
        var IsToAddSize= Array.indexOf(FeaturesList, "size" ) > -1 ;
        alert("IsToAddSize: " + IsToAddSize);
        if ( SelectedSize!= "" && IsToAddSize ) {
          IsToAddSize= false
          for (J = 0; J < featureses.length; J++) {
            alert( "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].size::" + featureses[J].size + "  SelectedSize::" + SelectedSize + "  SelectionType::"+SelectionType );
            if ( i== featureses[J].sku && featureses[J].sku == SelectedSku /* SelectionType == 'size'*/  /* && featureses[J].size != SelectedSize */ ) {
              alert( 'BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::"+SelectionType )
              IsToAddSize= true;
              break;
            }
          }
        }
        alert("A IsToAddSize: " + IsToAddSize);

        var IsToAddColor= Array.indexOf(FeaturesList, "color" ) > -1 ;
        alert("IsToAddColor: " + IsToAddColor);
        if ( SelectedColor!= "" && IsToAddColor ) {
          IsToAddColor= false
          for (J = 0; J < featureses.length; J++) {
            alert( "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].color::" + featureses[J].color + "  SelectedColor::" + SelectedColor + "  SelectionType::"+SelectionType  );
            // if ( i== featureses[J].sku && featureses[J].color != SelectedColor  ) {
            //if ( i== SelectedSku && SelectionType == 'color'  ) {
             if ( i == featureses[J].sku &&  featureses[J].sku == SelectedSku /* SelectionType == 'color'*/  /* && featureses[J].color != SelectedColor */ ) {
              alert( 'BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].color::" + featureses[J].color + "  SelectedHand::" + SelectedHand + "  SelectionType::"+SelectionType )
              IsToAddColor= true;
              break;
            }
          }
        }
        alert("A IsToAddColor: " + IsToAddColor);

        var IsToAddHand= Array.indexOf(FeaturesList, "hand" ) > -1 ;
        alert("IsToAddHand: " + IsToAddHand);
        if ( SelectedHand!= "" && IsToAddHand ) {
          IsToAddHand= false
          for (J = 0; J < featureses.length; J++) {
            alert( "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::"+SelectionType  );
            if ( i== featureses[J].sku && featureses[J].sku == SelectedSku /* featureses[J].hand != SelectedHand */ ) {
              alert( 'BAD INSIDE!!!' + "  i: " + i + "  featureses[J].sku::" + featureses[J].sku + "  featureses[J].hand::" + featureses[J].hand + "  SelectedHand::" + SelectedHand + "  SelectionType::"+SelectionType )
              IsToAddHand= true;
              break;
            }
          }
        }
        alert("A IsToAddHand: " + IsToAddHand);

        var IsFilled= false
        for( J= 0; J< filled_options.length; J++  ) {
          if ( filled_options[J] == val ) {
            IsFilled= true;
            break;
          }
        }        
         alert( "i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddSize::" + IsToAddSize 
        + "  IsToAddColor::" + IsToAddColor  + "  IsToAddHand::" + IsToAddHand )
        if ( !IsFilled && ( IsToAddSize || IsToAddColor || IsToAddHand ) ) {

         alert( "INSIDE i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsToAddSize::" + IsToAddSize 
        + "  IsToAddColor::" + IsToAddColor  + "  IsToAddHand::" + IsToAddHand )

          var SelectedTag= ""
           alert( "val::" + val + "   CurrentOptionValue::" + CurrentOptionValue )
          if ( CurrentOptionValue == val ) {
            SelectedTag= " selected "
          }
          rep_options_list += '<option value="'+ i +'" '+SelectedTag+' >'+ val +'</option>';
          filled_options[filled_options.length]= val;
          alert(" END! rep_options_list::"+rep_options_list + "  filled_options::"+var_dump(filled_options) )
        }
      });
       alert(" rep_options_list::"+rep_options_list )
      jQuery('#select_option').html(rep_options_list);
    }

    function repopulate_hands( SelectedSku, SelectedSize, SelectedColor, SelectedOption, SelectionType )
    {
      var temp_hands     = jQuery.parseJSON(<?php print json_encode(json_encode($result_hands_list)); ?>);
      alert( "repopulate_hands  temp_hands::"+var_dump(temp_hands) + "  SelectedSku::"+SelectedSku+"  SelectedSize::"+SelectedSize    +"  SelectedColor::"+SelectedColor  +"  SelectedOption::"+SelectedOption + 
       "  SelectionType::"+SelectionType )
      var rep_hands_list = '<option value=""> -select-</option>'; 
      var filled_hands= Array();        

      var CurrentHandValue = jQuery('#select_hand option:selected').text();      
      alert( "CurrentHandValue::"+CurrentHandValue )
      
      jQuery.each(temp_hands, function(i, val) {
        
        IsSkipSize= false;        
        if ( SelectedSize!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].size != SelectedSize  ) {
              IsSkipSize= true;
              break;
            }
          }
        }

        IsSkipColor= false;        
        if ( SelectedColor!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].color != SelectedColor  ) {
              IsSkipColor= true;
              break;
            }
          }
        }

        IsSkipOption= false;        
        if ( SelectedOption!= "" ) {
          for (J = 0; J < featureses.length; J++) {
            if ( i== featureses[J].sku && featureses[J].option != SelectedOption  ) {
              IsSkipOption= true;
              break;
            }
          }
        }
        
        var IsFilled= false;
        for( J= 0; J< filled_hands.length;J++  ) {            
          if ( filled_hands[J] == val ) {
            IsFilled= true;
            break;
          }
        }
        alert("repopulate_hands  IsFilled::"+IsFilled )
        
        if ( !IsFilled && ( !IsSkipSize || !IsSkipColor || !IsSkipOption ) ) {
           rep_hands_list += '<option value="'+ i +'">'+ val +'</option>';
          filled_hands[filled_hands.length]= val;
        }
      });
      jQuery('#select_hand').html(rep_hands_list);
    }

</script>  

<?php 
  function get_image_by_sku($sku) {
    $temp_file_name = '';
    $check = '';
                 
    $thumbnail_image_file = AppUtils::getThumbnailImageBySku($sku, false);
    if (empty($thumbnail_image_file)) {
      $thumbnail_image_file = AppUtils::getResizedImageBySku($sku, false, false, true);
      $check = $thumbnail_image_file['ImageFileName'];
      if (is_array($thumbnail_image_file)) {
        $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file['ImageFileName'];
        $temp_file_name       = $thumbnail_image_file;
      }
      else {
        $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file;
        $temp_file_name       = $thumbnail_image_file;
      }
      $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $thumbnail_image_file;
      if (!file_exists($temp_file_name)) {
        $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
      }
    }
    if ($check == '') {
      $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
    }
    return $thumbnail_image_file;
  }
  
?>