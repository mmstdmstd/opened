
<style type="text/css">
	#top_banner_div {background-image:url(<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/top-banner-2-specials.jpg) !important;}
	.product-sucess {background-color:#ed1b24 !important; color:#fff !important;}
	.special {display:inline-block; font-zie:80%; font-weight:normal; color:#fff;}
</style>


<?php
$product_listings_columns= (int)sfConfig::get('app_application_product_listings_columns' );
//Util::deb( $product_listings_columns, ' $product_listings_columns::' );
$InventoryItemsArray= array();
foreach( $InventoryItemsPager->getResults() as $InventoryItem ) {
	$InventoryItemsArray[]= $InventoryItem;
}
$InventoryItemsCount= count($InventoryItemsArray);
//Util::deb( $InventoryItemsCount, ' $InventoryItemsCount::' );
//if ( $InventoryItemsCount< 4 ) $InventoryItemsCount= 4;
//Util::deb( $InventoryItemsCount, ' ++$InventoryItemsCount::' );

$RowsCount= (int) ($InventoryItemsCount/$product_listings_columns);
$HasDecimal= $InventoryItemsCount%$product_listings_columns;
if ( $HasDecimal> 0 ) $RowsCount++;
//Util::deb( $RowsCount, ' $RowsCount::' );
?>
  <script type="text/javascript" language="JavaScript">
  <!--

  jQuery('body').click(function(){
    jQuery('#div_cart_info').hide();
  });

  function onChangeSelectSortBy(value) {
  	var Url= '<?php echo url_for( '@specials?page=1&rows_in_pager='.$rows_in_pager.'&sorting=ZZZZZ'.'&select_category=' . urlencode($select_category)  .'&select_subcategory=' . urlencode($select_subcategory) . '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search)  ) ?>'
    Url= Url.replace(/ZZZZZ/g, value);
  	document.location= Url
  }

  function onChangeSelectItemsPerPage(value) {
  	var Url= '<?php echo url_for( '@specials?page=1&rows_in_pager=ZZZZZ'.'&sorting='.$sorting.'&select_category=' . urlencode($select_category)  .'&select_subcategory=' . urlencode($select_subcategory) . '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search)  ) ?>'
    Url= Url.replace(/ZZZZZ/g, value);
  	document.location= Url
  }

  function AddToCart( Sku, eventObject ) {
    var HRef= '<?php echo url_for('@cart_product_update?sku=ZZZZZ&product_quantity=XXXXX&price_type=special') ?>'
    HRef= HRef.replace( /ZZZZZ/g, encodeURIComponent(Sku) );
    HRef= HRef.replace( /XXXXX/g, document.getElementById("qty_in_cart_"+Sku).value );
    //alert( HRef )

    jQuery.getJSON(HRef,   {  },
    onAddedToCart,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );

  }

  function onAddedToCart(data) {
    var ErrorCode= data.ErrorCode
    var ErrorMessage= data.ErrorMessage
    var product_quantity= data.product_quantity
    var items_count= data.items_count
    var common_sum= data.common_sum
    if ( ErrorCode == 1 ) {
      alert( ErrorMessage )
      return;
    }
    document.getElementById("span_header_number_items").innerHTML= items_count
    document.getElementById("span_header_total_cost").innerHTML= '$' +AddDecimalsDigit(common_sum)

    jQuery('#div_cart_info').show();
    if ((items_count * 1) > 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' items  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    else if ((items_count * 1) == 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' item  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    if ((product_quantity * 1) > 1) {
        jQuery('#span_action').html(product_quantity + ' items added to your cart');
    }
    else if ((product_quantity * 1) == 1) {
        jQuery('#span_action').html(product_quantity + ' item added to your cart');
    }
  }



  function onFilterSubmit() {
    var theFilterForm = document.getElementById("form_filter");
    theFilterForm.submit();
  }

  jQuery(function($) {
    SelectCategoryonChange('<?php echo $select_subcategory ?>')
  });

  function SelectCategoryonChange(selected_select_subcategory) {
  	// alert( " SelectCategoryonChange  selected_category_id selected_category_id::" + selected_select_category )
    var select_category= document.getElementById('select_category').value;
    if ( select_category=="" ) {
      ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
      return
    }
    var HRef= '<?php echo url_for('@main_getsubcategories?category=') ?>' + encodeURIComponent(select_category)+'/selected_subcategory/' + encodeURIComponent(selected_select_subcategory)
    // alert(HRef)
    jQuery.getJSON(HRef,
    {
    },
    FillSubcategories,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function FillSubcategories(data) {
  	// alert( "FillSubcategories(data)::"+var_dump(data) )
    var ArrayLength= parseInt(data['length'])
    var selected_subcategory= data['selected_subcategory']
    ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
    var DataArray= data['data']
    for( i=0; i< ArrayLength; i++ ) {
      if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
        var Id= DataArray[i]['id']
        var Name= DataArray[i]['name']
        AddDDLBItem( 'select_subcategory', Id, Name ); //Add all options to input selection
      }
    }
    // alert("selected_subcategory::"+selected_subcategory)
    SetDDLBActiveItem( 'select_subcategory', selected_subcategory )
  }

  function FieldOnFocus(FieldName) {
    FieldValue= "<?php echo __("enter email address") ?>";
    var S = document.getElementById(FieldName).value;
    if ( Trim(S)==FieldValue ) {
      document.getElementById(FieldName).value= "";
    }
  }

  function FieldOnBlur(FieldName) {
    FieldValue= "<?php echo __("enter email address") ?>";
    var S = document.getElementById(FieldName).value;
    if ( Trim(S)=="" ) {
      document.getElementById(FieldName).value= FieldValue;
    }
  }

  function OnSelectoKeyUp(e) {
    if ( e.keyCode == 13 || e.keyCode == 9 ) {
      return true;
    }
    document.getElementById( "predict_filter_recruit_id" ).value= "";
  }

  function cart_window_remove()
  {
      jQuery('#div_cart_info').hide();
  }

  //-->
  </script>



<form action="<?php echo url_for('@specials?page=1&rows_in_pager='.$rows_in_pager  ) ?>" id="form_filter" method="POST">
  <?php include_partial( 'product_listings_header', array( 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  
      'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle,
      'checked_filters'=> $checked_filters, 'UsersCartArray'=>$UsersCartArray, 'sorting'=>$sorting, 'page_type'=>'specials' ) )?>
</form>

<input type="hidden" id="return_page" name="return_page" value="view_cart" >
<input type="hidden" id="sku" name="sku" value="" >
<input type="hidden" id="product_quantity" name="product_quantity" value="" >
<table width="80%" align="center" style="margin:0 auto;">

    <td colspan="2" align="<?php echo( $InventoryItemsCount>3?"center":"left") ?>" valign="top" >
      <table width="90%" cellpadding="0" cellspacing="0" border="0" valign="top">
      <?php $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
        <tr id="tr_specials_<?php echo $Row ?>" align="left">
      <?php $count = 0; ?>
        <?php for($Col=1; $Col<= $product_listings_columns; $Col++) : ?>
            <?php if ( $I < $rows_in_pager and !empty($InventoryItemsArray[$I]) ) : $count++; ?>
            <td>
          		<?php include_partial( 'main/specials_item', array( 'sorting'=>$sorting, 'Index'=>$I, 'Col'=> $Col, 'Row'=>$Row, 'Item'=>$InventoryItemsArray[$I], 'page'=> $page, 'rows_in_pager'=> $rows_in_pager, 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search, 'UsersCartArray'=>$UsersCartArray ) ) ?>
            <?php else: ?>
             <!--&nbsp;-->
             </td>
            <?php endif; ?>
        <?php $I++; endfor; ?>
        <?php
            if ($count < 4) {
                for ($count; $count < 4; $count++ ) {
                    echo '<td><div class="product_div"></div></td>';
                }
            }
        ?>
        </tr>
      <?php endfor; ?>
      </table>
     </td>

</table>


  <?php $page_sorting= '-'; if ( !empty($sorting) ) $page_sorting= $sorting;
  include_partial( 'main/filters_and_paginator', array( 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle, 'sorting'=>$sorting, 'page_sorting'=> $page_sorting, 'page_type'=>'specials' ) ) ?>


<!--
<div id="div_cart_info" style="display:none;" >
	<img id="modal-window-close-button" onclick="javascript:HideCartInfo()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png " />
	<p id="span_action"></p>
	<p id="span_cart_info"></p>

	<p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>

	<p id="click-anywhere">Click anywhere on this page<br> to continue shopping</p>
	<a class="check-out-now-button" href="<?php echo url_for( '@check_out_customer' ) ?>">Check Out Now ►</a>
</div>
-->

<div id="div_cart_info" style="display:none;">
	<img id="modal-window-close-button" onclick="javascript:cart_window_remove()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png" />
    <?php
	if (empty($items_count)) {
		$items_count = Cart::getItemsCount();
	}
	if (empty($common_sum)) {
		$common_sum = Cart::getCommonSum(true);
	}
	if ($message_type == 'delete') { ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) deleted from your cart</p>
    <?php
    }
    else {
    ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) added to your cart</p>
    <?php } ?>
	<p id="span_cart_info">Cart: <?php echo $items_count ?> items   <?php echo $common_sum ?></p>
    <?php if (empty($hide_view_message)) { ?>
	    <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>
	    <p id="click-anywhere">Click anywhere on this page<br /> to continue shopping</p>
    <?php } ?>
	<a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>



















<?php return ?>

<form action="<?php echo url_for('@specials?page=1&rows_in_pager='.$rows_in_pager  ) ?>" id="form_specials" method="POST">
    <?php include_partial( 'main/search_criteria', array( 'select_category'=>$select_category,  'sorting'=>$sorting, 'page'=>$page, 'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle/*, 'RootItem'=> $RootItem, 'HasRootItem'=> $HasRootItem, 'RootItemObject'=>$RootItemObject, 'Matrix'=> $Matrix*/ ) ) ?>
</form>



