    <?php 
    if ( empty($page_type) ) $page_type= 'product_listings';
    $StartRow= ( $page- 1) * $rows_in_pager+1;
      $EndRow= ( $page) * $rows_in_pager;
      $HasRows= $InventoryItemsPager->getNbResults() > 0;
      if ( $EndRow > $InventoryItemsPager->getNbResults() ) $EndRow= $InventoryItemsPager->getNbResults();
    ?>
   
    <?php if ( $HasRows ) : ?>
       
    
		<div class="filters-and-paginator">
			<div class="current-items-range-and-total">Items <?php echo $StartRow ?>-<?php echo $EndRow ?> of <?php echo $InventoryItemsPager->getNbResults() ?></div>
			<div class="sort-by">Sort by:
				<select id="select_sort_by" onchange="javascript:onChangeSelectSortBy(this.value)">
					<option value="" >Relevance</option>
					<option value="STD_UNIT_PRICE_LOW_TO_HIGN" <?php echo ( $sorting == "STD_UNIT_PRICE_LOW_TO_HIGN" ? "selected" : "" ) ?> >Price Low-to-High</option>
					<option value="STD_UNIT_PRICE_HIGN_TO_LOW" <?php echo ( $sorting == "STD_UNIT_PRICE_HIGN_TO_LOW" ? "selected" : "" ) ?> >Price High-to-Low</option>
					<option value="BRAND" <?php echo ( $sorting == "BRAND" ? "selected" : "" ) ?> >Brand</option>
				</select>
				
<!-- 2- The "Sort By" field needs to be changed to include only the following items: Relevance, Price Low-to-High, Price High-to-Low, Brand.  The "Relevance" item will be the same as no sort for now. We will improve it later.

    -->
			</div>
			<div class="show-items-per-page">Show:
				<select id="select_items_per_page" onchange="javascript:onChangeSelectItemsPerPage(this.value)" >
        <?php $AllItemsPaginationItemsArray= sfConfig::get('app_application_AllItemsPaginationItems');
        foreach( $AllItemsPaginationItemsArray as $AllItemsPaginationItem ) : ?>
					<option value="<?php echo $AllItemsPaginationItem ?>" <?php echo ($AllItemsPaginationItem == $rows_in_pager ? "selected" : "") ?>><?php echo $AllItemsPaginationItem?> items per page</option>
        <?php endforeach; ?>
				</select>
			</div>

      <?php if ($InventoryItemsPager->haveToPaginate()): ?>
			<div class="paginator">Page:
				<a class="previous" href="<?php echo url_for( '@'.$page_type.'?page=1&rows_in_pager='.$rows_in_pager.'&sorting='.$page_sorting .'&select_category=' . urlencode($select_category)  .'&select_subcategory=' .
            urlencode($select_subcategory) . '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search) . ( !empty($checked_filters)?'&checked_filters='.$checked_filters:'' ) ) ?>">&lt;</a>
				
        <?php $links = $InventoryItemsPager->getLinks(5, 3);  
        foreach ($links as $link_page=>$link_Title): 
          if ( (int)$link_page<= 0 ) continue;
          $SelectedClass= ( $InventoryItemsPager->getPage() == $link_page ? 'class="current-page"' : "" )  ?>
          <a href="<?php echo url_for( '@'.$page_type.'?page='.$link_page.'&rows_in_pager='.$rows_in_pager.'&sorting='.$page_sorting . '&select_category=' . urlencode($select_category) . '&select_subcategory=' .
            urlencode($select_subcategory) . '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search) . ( !empty($checked_filters)?'&checked_filters='.$checked_filters:'' )  ) ?>" <?php echo $SelectedClass ?>><?php echo $link_Title ?></a>
        <?php endforeach ?>
				
				<a class="next" href="<?php echo url_for( '@'.$page_type.'?page='.$link_Title.'&rows_in_pager='.$rows_in_pager.'&sorting='.$page_sorting . '&select_category=' . urlencode($select_category) .
            '&select_subcategory=' . urlencode($select_subcategory)  . '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search) . ( !empty($checked_filters)?'&checked_filters='.$checked_filters:'' ) ) ?>">></a>
			</div>
      <?php endif; ?>          			
		</div>
		<div style="clear: both;"></div>
  <?php else: ?> 
    <span class="items_not_found">No&nbsp;Items&nbsp;Found</span>
  <?php endif; ?> 
