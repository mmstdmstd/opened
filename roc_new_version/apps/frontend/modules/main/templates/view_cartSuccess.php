<?php
$SelectedRadioId= '';
$shipping_tax_data = Cart::getTaxShipData();
$HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
if ( !empty($shipping_tax_data) ) {
  if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_ground'] ) $SelectedRadioId= 'ups_ground';
  if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_3_day_selected'] ) $SelectedRadioId= 'ups_3_day_selected';
  if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_2_day_air'] ) $SelectedRadioId= 'ups_2_day_air';
  if ( $shipping_tax_data['est_shipping'] == $shipping_tax_data['ups_next_day_air_saver'] ) $SelectedRadioId= 'ups_next_day_air_saver';
}
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php Util::getServerHost() ?>js/jquery/jquery.js"></script>
<script type="text/javascript" language="JavaScript">
  <!--

  var SelectedRadioId= ''
  jQuery(document).ready(function() {
    LoadCartItemsList()
    SelectedRadioId= '<?php echo $SelectedRadioId ?>'
    if ( document.getElementById(SelectedRadioId) ) {
      //alert( "INSIDE SelectedRadioId::"+SelectedRadioId )
      document.getElementById(SelectedRadioId).checked= true
    }
    <?php
    if ( !empty($shipping_tax_data['postal_code']) ) { ?>
      document.getElementById("zip_code").value= '<?php echo $shipping_tax_data['postal_code'] ?>';
      calculateWithTax();
    <?php } ?>
  });

  jQuery('body').click(function(){
    jQuery('#div_cart_info').hide();
  });

  function LoadCartItemsList() {
    var HRef= '<?php echo url_for('@load_cart_items_list') ?>'
    jQuery.get(HRef,   {  },
    onLoadedCartItemsList,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }


  function onLoadedCartItemsList(data) {
  	document.getElementById("div_view_cart_items_list").innerHTML= data
    var HRef= '<?php echo url_for('@get_cart_info') ?>'
    jQuery.getJSON(HRef,   {  },
    onGetCartInfo,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function onGetCartInfo(data) {
    var items_count= data.items_count
    var common_sum= data.common_sum
    setItemsInfo( items_count, common_sum )
  }



  function UpdateCartProductQuantity( Sku ) {
	  var product_quantity= document.getElementById("cart_product_quantity_"+Sku).value
    var HRef= '<?php echo url_for('@cart_product_update?sku=ZZZZZ&product_quantity=XXXXX') ?>'
    HRef= HRef.replace( /ZZZZZ/g, encodeURIComponent(Sku) );
    HRef= HRef.replace( /XXXXX/g, encodeURIComponent(product_quantity) );
    // alert( HRef )
    jQuery.getJSON(HRef,   {  },
    onAddedToCart,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function onAddedToCart(data) {
    var ErrorCode= data.ErrorCode
    var ErrorMessage= data.ErrorMessage
    var product_quantity= data.product_quantity
    var items_count= data.items_count
    var common_sum= data.common_sum
    if ( ErrorCode == 1 ) {
      alert( ErrorMessage )
      return;
    }
    document.getElementById("span_header_number_items").innerHTML= items_count
    document.getElementById("span_header_total_cost").innerHTML= common_sum
    //setItemsInfo( items_count, common_sum );
    jQuery('#div_cart_info').show();
     if ((items_count * 1) > 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' items  &nbsp;&nbsp;' + '$' + AddDecimalsDigit(common_sum) );
    }
    else if ((items_count * 1) == 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' item  &nbsp;&nbsp;' + '$' + AddDecimalsDigit(common_sum) );
    }
    if ((product_quantity * 1) > 1) {
        jQuery('#span_action').html(product_quantity + ' items added to your cart');
    }
    else if ((product_quantity * 1) == 1) {
        jQuery('#span_action').html(product_quantity + ' item added to your cart');
    }
  }

  function setItemsInfo( items_count, common_sum ) {
    document.getElementById("span_header_number_items").innerHTML= items_count
    document.getElementById("span_header_total_cost").innerHTML=  '$' + AddDecimalsDigit(common_sum)
    if ( items_count > 0 ) {
      document.getElementById("span_UsersCartItemsSelectedCount").innerHTML= "You have "+items_count+" item(s) in your cart."
    } else {
      document.getElementById("span_UsersCartItemsSelectedCount").innerHTML= "Your shopping cart is empty"
    }
    document.getElementById("span_subtotal").innerHTML= '<b>$' + AddDecimalsDigit(common_sum) + '</b>'
    // document.getElementById("span_estimated_total").innerHTML= '<b>' + AddDecimalsDigit(common_sum) + '</b>'
  }


  function DeleteCartProduct( Sku ) {
    var HRef= '<?php echo url_for('@cart_product_delete?sku=ZZZZZ') ?>'
    HRef= HRef.replace( /ZZZZZ/g, encodeURIComponent(Sku) );
    //alert( HRef )
    jQuery.getJSON(HRef,   {  },
    onDeletedCartProduct,
      function(x,y,z) {   //Some sort of error
        alert(x.responseText);
      }
    );
  }

  function onDeletedCartProduct(data) {
  	var product_quantity= data.product_quantity
    var ErrorCode= data.ErrorCode
    var ErrorMessage= data.ErrorMessage
    var product_quantity= data.product_quantity
    var items_count= data.items_count
    var common_sum= data.common_sum
    LoadCartItemsList()

    jQuery('#div_cart_info').show();
    if ( items_count == 0 ) {
      jQuery('#span_cart_info').html('Cart is empty' );
    }
    if ((items_count * 1) > 1) {
      jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' items  &nbsp;&nbsp;' +  '$' + AddDecimalsDigit(common_sum) );
    }
    else if ((items_count * 1) == 1) {
      jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' item  &nbsp;&nbsp;' +  '$' + AddDecimalsDigit(common_sum) );
    }
    if ((product_quantity * 1) > 1) {
      jQuery('#span_action').html(product_quantity + ' items deleted from your cart');
    }
    else if ((product_quantity * 1) == 1) {
      jQuery('#span_action').html(product_quantity + ' item deleted from your cart');
    }
     // jQuery('#span_action').html(product_quantity + ' item(s) deleted from your cart');
      jQuery('#click-anywhere').html('Click anywhere on this page<br /> to continue view cart');
  }
  function cart_window_remove()
  {
      jQuery('#div_cart_info').hide();
  }

  function calculateWithTax()
  {
      var if_zip_exists = jQuery('#zip_code').val();
      //var url = '<?php //echo url_for('main/calculate_tax') ?>'; //PH
      //var url   = Util::getServerHost() . "main/calculate_tax";
      var url = 'http://www.oherron.com/main/calculate_tax';
      if (if_zip_exists != '') {
          jQuery('#est_shiping').html('--');
          document.getElementById("span_loadingAnimation").style.display="inline"
          var geocoder = new google.maps.Geocoder();
          var zipcode  = jQuery('#zip_code').val();
        //  alert( "calculateWithTax() zipcode::"+zipcode );
          geocoder.geocode( { 'address': zipcode}, function (result, status) {
              var state = "N/A";
              for (var component in result[0]['address_components']) {
                  for (var i in result[0]['address_components'][component]['types']) {
                      if (result[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
                          state = result[0]['address_components'][component]['short_name'];
                          //url   = Util::getServerHost() . "main/calculate_tax";
                          var json_data = {
                              'state'  : state,
                              'zip'    : zipcode
                          };
                          //alert(url); //debug
                          jQuery.post(url, json_data, function(data) {
                              var tax_rates = JSON.parse(data);
                              // alert( "tax_rates::"+var_dump(tax_rates) )
                              var tax                    = tax_rates.tax;
                              var usps                   = tax_rates.USPS;
                              var ups_ground             = tax_rates.UPSGround;
                              var ups_3_day_selected     = tax_rates.UPS3DaySelected;
                              var ups_2_day_air          = tax_rates.UPS2DayAir;
                              var ups_next_day_air_saver = tax_rates.UPSNextDayAirSaver;


                              if (  tax == 'null'  ||  (typeof tax== "undefined")  ||  (typeof tax== "object") ) tax= '0';
                              if (  usps == 'null'  ||  (typeof usps== "undefined")  ||  (typeof usps== "object")  ) usps= '0';
                              if (  ups_ground == 'null' || (typeof ups_ground== "undefined")  ||  (typeof ups_ground== "object")  ) ups_ground= '0';
                              if (  ups_3_day_selected == 'null' || (typeof ups_3_day_selected== "undefined"  ||  (typeof ups_3_day_selected== "object")  ) ) ups_3_day_selected= '0';
                              if (  ups_2_day_air == 'null' || (typeof ups_2_day_air== "undefined")   ||  (typeof ups_2_day_air== "object")  ) ups_2_day_air= '0';
                              if (  ups_next_day_air_saver == 'null' || (typeof ups_next_day_air_saver== "undefined")  ||  (typeof ups_next_day_air_saver== "object")  ) ups_next_day_air_saver= '0';


                              var AdditionalFreight = parseFloat('<?php echo Cart::getAdditionalFreightSum(); ?>');
                              // alert( "AdditionalFreight::"+AdditionalFreight )
                              if (tax != '' && tax != 0 && tax != '0'  ) {
                                  var est_total = parseFloat('<?php echo Cart::getCommonSum(); ?>');
                                  var est_tax   = est_total / 100 * tax;
                                  est_tax = Math.round(est_tax * 100) / 100;
                                  if (est_tax > 0) {
                                      jQuery('#est_tax').html('<b>$' + AddDecimalsDigit(est_tax) + '</b>');
                                  }
                                  else {
                                      jQuery('#est_tax').html('--');
                                  }
                                  // alert( "-1 est_tax::"+est_tax )
                                  est_total = est_total + parseFloat(est_tax);
                                  // alert( "-2 est_total::"+est_total )
                                  est_total  = Math.round(est_total * 100) / 100;
                                  // alert( "-3 est_total::"+est_total )
                                  jQuery('#span_estimated_total').html('<b>$' + AddDecimalsDigit(est_total /* + AdditionalFreight */ ) + '</b>');
                              }
                              else {
                                  jQuery('#est_tax').html('--');
                                  // alert( "-Z1 est_total::"+ '<?php echo Cart::getCommonSum(); ?>' )
                                  jQuery('#span_estimated_total').html('<b>$' + AddDecimalsDigit( parseFloat('<?php echo Cart::getCommonSum(); ?>') /* + AdditionalFreight */ ) + '</b>');

                              }
                              if (usps != '') {
                                  jQuery('#usps').html('&nbsp;-&nbsp;$' + AddDecimalsDigit( parseFloat(usps) + AdditionalFreight ) );
                              }
                              else {
                                  jQuery('#usps').html('');
                              }
                              if (ups_ground != '') {
                                  jQuery('#ups_ground').html('&nbsp;-&nbsp;$' + AddDecimalsDigit( parseFloat(ups_ground) + AdditionalFreight ) );
                              }
                              else {
                                  jQuery('#ups_ground').html('');
                              }
                              if (ups_3_day_selected != '') {
                                  jQuery('#ups_3_day_selected').html('&nbsp;-&nbsp;$' + AddDecimalsDigit( parseFloat(ups_3_day_selected) + AdditionalFreight ) ) ;
                              }
                              else {
                                  jQuery('#ups_3_day_selected').html('');
                              }
                              if (ups_2_day_air != '') {
                                  jQuery('#ups_2_day_air').html('&nbsp;-&nbsp;$' + AddDecimalsDigit( parseFloat(ups_2_day_air) + AdditionalFreight ) );
                              }
                              else {  // UPS-NextDay
                                  jQuery('#ups_2_day_air').html('');
                              }
                              if (ups_next_day_air_saver != '') {
                                  jQuery('#ups_next_day_air_saver').html('&nbsp;-&nbsp;$' + AddDecimalsDigit( parseFloat(ups_next_day_air_saver) + AdditionalFreight ) );
                              }
                              else {
                                  jQuery('#ups_next_day_air_saver').html('');
                              }
                              sendShipTaxInfo(zipcode, usps, ups_ground, ups_3_day_selected, ups_2_day_air, ups_next_day_air_saver, est_tax, '--', est_total);
                          });
                      }
                  }
              }
            /*alert( "END SelectedRadioId::"+SelectedRadioId )
            if ( SelectedRadioId!= "" ) {
              if ( document.getElementById(SelectedRadioId) ) {
                document.getElementById(SelectedRadioId).checked= true
                alert( "NOW:: SelectedRadioId::"+SelectedRadioId )
                setShippingMethod(SelectedRadioId)
                SelectedRadioId= ""
              }
            } */

          });
          //alert("Clear!!")
          $("input:radio").attr("checked", false);
      }
      else {
        alert('Please enter your zip code');
        document.getElementById("zip_code").focus();
      }
  }

  function setShippingMethod(shipping_method)
  {

      var if_zip_exists = jQuery('#zip_code').val();
      if (if_zip_exists != '') {
          var shipping_cost = jQuery('#' + shipping_method).text();
          re = /\d+\.\d+/;
          var found = shipping_cost.match(re);
          if (found == '') {
              re = /\d+/;
              found = shipping_cost.match(re);
          }
          //alert( "found::" + found  )
          if (found != '') {
              var temp = jQuery('#span_estimated_total').text();
              re    = /\d+\.\d+/;
              temp= temp.replace( /,/g, "" )
              temp= temp.replace( /$/g, "" )
              found_ = temp.match(re);
              //alert( "-Z2 temp::" + temp + "  found_::" + found_  )
              if (found_ == '') {
                  re = /\d+/;
                  found_ = temp.match(re);
              }
              if (found_ > 0) {
                  var shipping = jQuery('#est_shiping').text();
                  //alert("shipping::"+shipping)
                  shipping= shipping.replace( /,/g, "" )
                  shipping= shipping.replace( /$/g, "" )
                  //alert("-1 shipping::"+shipping)
                  if (shipping != '--') {
                      var re = /\d+\.\d+/;
                      var t_ship = shipping.match(re);
                      if (t_ship == '') {
                          re = /\d+/;
                          t_ship = shipping.match(re);
                      }
                      found_ = found_ * 1 - t_ship * 1;
                  }
                  //alert("-2 shipping::"+shipping)
                  var total_ = found_ * 1 + found * 1;
                  //alert( "-3 total_::" + total_ )
                  total_ = Math.round(total_ * 100) / 100;
                  // alert( "-4 total_::" + total_ )

                  var AdditionalFreight = parseFloat('<?php echo Cart::getAdditionalFreightSum(); ?>');
                  // alert( "::"+AdditionalFreight )
                  jQuery('#span_estimated_total').html('<b>$' + AddDecimalsDigit( total_  /* + AdditionalFreight */ ) + '</b>');
                  jQuery('#est_shiping').html('<b>$' + AddDecimalsDigit( parseFloat(found) /* + AdditionalFreight */ ) + '</b>');
                  //alert( "-5 found::"+found+"  total_::"+total_  )
                  found[0] = parseFloat(found[0]) - AdditionalFreight
                  setEstimateTotalaShipping( found, total_ - AdditionalFreight );
              }
          }
          else {
              jQuery('#est_shiping').html('--');
          }
      }
      else {
          alert('Please enter your zip code');
      }
  }

  function sendShipTaxInfo(zipcode, usps, ups_ground, ups_3_day_selected, ups_2_day_air, ups_next_day_air_saver, est_tax, est_shipping, estimated_total)
  {
      //var url = '<?php //echo url_for('main/review_order') ?>';  //PH
      //var url   = Util::getServerHost() . "main/review_order";
      var url = 'http://www.oherron.com/main/review_order';
      var json_data = {
                          'zipcode'                : zipcode,
                          'usps'                   : usps,
                          'ups_ground'             : ups_ground,
                          'ups_3_day_selected'     : ups_3_day_selected,
                          'ups_2_day_air'          : ups_2_day_air,
                          'ups_next_day_air_saver' : ups_next_day_air_saver,
                          'est_tax'                : est_tax,
                          'est_shipping'           : est_shipping,
                          'estimated_total'        : estimated_total
                      };
      jQuery.post(url, json_data, function(data) {
          document.getElementById("span_loadingAnimation").style.display="none"
          return true;
      });

  }

  function setEstimateTotalaShipping(est_shipping, est_total)
  {
      //var url = '<?php //echo url_for('main/set_estimates') ?>'; //PH
      //var url   = Util::getServerHost() . "main/set_estimates";
      var url = 'http://www.oherron.com/main/set_estimates';
      var json_data = {
                          'est_shipping' : est_shipping[0],
                          'est_total'    : est_total
                      }
      jQuery.post(url, json_data, function(data) {
          return true;
      });
  }

  //-->
</script>


<form action="<?php echo url_for('@product_listings?'  ) ?>" id="form_product_listings" method="POST">
    <?php include_partial( 'main/search_criteria', array( 'select_category'=>'',  'sorting'=>'', 'page'=>1, 'rows_in_pager'=>'','select_subcategory'=> '',  'select_brand_id'=> '', 'input_search'=> '', 'HeaderTitle'=> '', 'RootItem'=> '', 'HasRootItem'=> '', 'RootItemObject'=>'', 'Matrix'=> '' ) ) ?>
</form>

<!-- website message alert -->
<?php
if (trim(sfConfig::get('app_website_message_products_alert'))!='') {
?>
<br />
<div style="position:absolute;left:35px;width:885px">
<p class="website_alert_message"><?php echo sfConfig::get('app_website_message_products_alert') ?></p>
</div>
<br />
<?php
}
?>

<div style="position:relative">
<h3>Shopping Cart
  <small>
    <a id="continue-shopping" href="<?php echo url_for( '@product_listings' ) ?>">Continue Shopping</a>
    <span id="span_check_out_customer_2">
      <a class="check-out-now-button" href="<?php echo url_for( '@check_out_customer' ) ?>">Check Out Now ►</a>
    </span>
  </small>
</h3>
</div>


<p id="total-items-in-cart"><span id="span_UsersCartItemsSelectedCount" ></span></p>
 <table id="cart-table" cellpadding="0" cellspacing="0" border="0" valign="top">

  <tr>
    <td width="70%" valign="top">
      <div id="div_view_cart_items_list"></div>
    </td>
    <td width="30%" valign="top" >
      <div id="price-summary-and-estimates">
        <h3>Price Summary</h3>
        <p id="subtotal">Subtotal: <span id="span_subtotal"><?php echo Cart::getCommonSum(true) ?></span></p>
        <div id="estimates">
          <h3>Estimates</h3>
          <p>To estimate shipping and tax,<br>enter your ZIP code <br /></p>
          <div style="margin:8px 0 0 5px !important; padding:0 !important;height:20px;border:0px solid blue;position:relative">
            <input type="text" value="" id="zip_code" name="zip_code" size="5" maxlength="5" style="margin-top:0px;width:60px">
            <div style="width:60px; position:relative; bottom:40px; !important;left:70px !important;">
              <input class="submit-button" style="width:50px" src="<?php echo $HostForImage ?>images/submit-aligned.png" type="button" onclick="calculateWithTax(); return false;" />
            </div>
            <span style="border:0px dotted green;position:absolute;top:3px;left:140px;display:none" id="span_loadingAnimation"><?php echo image_tag('loadingAnimation.gif', array('style'=>"padding-top:0px;"))?></span>
          </div>


          <div>
            <p><strong>Select desired shipping:</strong></p>
            <form name="f">
              <!--
              <?php // $UPSServicesArray= sfConfig::get('app_application_UPSServices');  ?>
              <?php // foreach( $UPSServicesArray as $UPSKey => $UPSService ) :?>
                <p><input type="radio" name="shipping" id="<?php // echo $UPSKey ?>" onClick="setShippingMethod('<?php // echo $UPSKey ?>');" value="1" />
                  <label for="<?php // echo $UPSKey ?>"><?php // echo $UPSService ?></label>
                  <span id="<?php // echo $UPSKey ?>"></span>
                </p>
              <?php // endforeach; ?>  -->

              <!-- <p><input type="radio" name="shipping" id="US-Postal" onClick="setShippingMethod('usps');" value="1" /><label for="US-Postal">US Postal Service</label><span id="usps"></span></p> -->
              <p><input type="radio" name="shipping" id="UPS-Ground" onClick="setShippingMethod('ups_ground');" value="2" /><label for="UPS-Ground">UPS Ground</label><span id="ups_ground"></span></p>
              <!--<p><input type="radio" name="shipping" id="UPS-3Day" onClick="setShippingMethod('ups_3_day_selected');" value="3" /><label for="UPS-3Day">UPS 3 Day Select</label><span id="ups_3_day_selected"></span></p> -->
              <p><input type="radio" name="shipping" id="UPS-2Day" onClick="setShippingMethod('ups_2_day_air');" value="4" /><label for="UPS-2Day">UPS 2 Day Air</label><span id="ups_2_day_air"></span></p>
              <p><input type="radio" name="shipping" id="UPS-NextDay" onClick="setShippingMethod('ups_next_day_air_saver');" value="5" /><label for="UPS-NextDay">UPS Next Day Air Saver</label><span id="ups_next_day_air_saver"></span>
              <b><?php //echo 'est_shipping:'.$shipping_tax_data['est_shipping']; //die("TREW"); ?></b>
              </p>
            </form>
            <!--TODO-->
          </div>
        </div>

				<p id="estimated-shipping">Estimated shipping: <span id="est_shiping">--</span></p>
				<p id="estimated-tax">Estimated tax: <span id="est_tax">--</span></p>
				<p id="estimated-total">Estimated total: <span id="span_estimated_total"><?php echo Util::getDigitMoney( Cart::getCommonSum(false) + Cart::getAdditionalFreightSum(), 'Money' )  ?></span>
          <!-- Ship&nbsp;Weight: <span id="span_estimated_total"><?php // echo Cart::getTotalShipWeight() ?></span>  -->
        </p>
      </div>
       <span id="span_check_out_customer_1">
  			<a class="check-out-now-button check-out-now-button-second-one" href="<?php echo url_for( '@check_out_customer' ) ?>">Check Out Now ►</a>
  		</span>
      </div>
    </td>
  </tr>

</table>

<div id="customers-also-bought">
	<h4>TODO Customers also bought:</h4>
	<p>
		<a href="#"><img src="view_cart_files/shoe.jpg" />Men's 6" US Navy DuraShock Steel Toe Boot</a>
	</p>
	<p>
		<a href="#"><img src="view_cart_files/shoe.jpg" />Men's 6" US Navy DuraShock Steel Toe Boot</a>
	</p>
	<p>
		<a href="#"><img src="view_cart_files/shoe.jpg" />Men's 6" US Navy DuraShock Steel Toe Boot</a>
	</p>
	<p>
		<a href="#"><img src="view_cart_files/shoe.jpg" />Men's 6" US Navy DuraShock Steel Toe Boot</a>
	</p>
</div>

<!--
<div id="div_cart_infodiv_cart_info" style="display:none;" >
	<img id="modal-window-close-button" onclick="javascript:HideCartInfo()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png " />
	<p id="span_action"></p>
	<p id="span_cart_info"></p>
	<a class="check-out-now-button" href="<?php echo url_for( '@check_out_customer' ) ?>">Check Out Now ►</a>
</div>
-->
<div id="div_cart_info" style="display:none;">
	<img id="modal-window-close-button" onclick="javascript:cart_window_remove()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png" />
    <?php
	if (empty($items_count)) {
		$items_count = Cart::getItemsCount();
	}
	if (empty($common_sum)) {
		$common_sum = Cart::getCommonSum(true);
	}
	if ($message_type == 'delete') { ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) deleted from your cart</p>
    <?php
    }
    else {
    ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) added to your cart</p>
    <?php } ?>
	<p id="span_cart_info">Cart: <?php echo $items_count ?> items   $<?php echo $common_sum ?></p>
    <?php if (empty($hide_view_message)) { ?>
	    <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>
	    <p id="click-anywhere">Click anywhere on this page<br /> to continue shopping</p>
    <?php } ?>
	<a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>



