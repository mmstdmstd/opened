<script type="text/javascript" language="JavaScript">
<!--

function SendForm() {
  var theForm = document.getElementById("form_firearm")
  theForm.submit()
}

//-->
</script>
<form action="<?php echo url_for('@firearm') ?>" id="form_firearm" method="POST" enctype="multipart/form-data" >
  <?php echo $form['_csrf_token']->render() ?>

<div id="policy_div">
  <h2>Testing &amp; Evaluation (T&amp;E)&nbsp;&mdash;&nbsp;Firearms</h2>
    <p>Thank you for your interest in testing and evaluating our selection of firearms.<br>Please fill out the form below and we will contact you shortly.</p>
    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Contact<br>Information<br>
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="width:342px; float:right;position:relative;">
        <label class="te-form-label">Email:&nbsp;
        <span style="color:#ff0000;">*</span></label>
        <?php echo $form['email']->render(); ?>        
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['email']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Phone:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['phone']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['phone']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Address:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['address']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['address']->renderError()) : "" ) ?></span>

        <label class="te-form-label">City:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['city']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['city']->renderError()) : "" ) ?></span>

        <label class="te-form-label4">State:&nbsp;<span style="color:#ff0000;">*</span></label>
        <label style="display:block; margin-top:16px; position:absolute; width:70px; left:166px; top:232px;">Zip Code:&nbsp;<span style="color:#ff0000;">*</span></label>

        <?php echo $form['state']->render(); ?>
        <span style="  position:relative;right:115px;bottom:21px; color:#ff0000!important; width:82px;  float:right; right:187px;"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['state']->renderError()) : "" ) ?></span>
        <?php echo $form['zip']->render(); ?>
        <span style="  position:relative;right:11px;bottom:45px;color:#ff0000!important;width:82px;float:right;"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['zip']->renderError()) : "" ) ?></span>
      </div>

      <div style="width:351px; float:right;">
          <label class="te-form-label">First Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['first_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['first_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Last Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['last_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['last_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Department Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['department_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['department_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Contact Person for Firearms in Department:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['department_person']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['department_person']->renderError()) : "" ) ?></span>
      </div>
      <div style="clear:both;"></div>
    </div>


    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Evaluation<br>Information<br>
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="float:left;">
        <label id="firearm_firearms_model" style="position:relative;left:70px;top:5px;important;">What make/model of firearm(s) are you interested in?&nbsp;<span style="color:#ff0000;">*</span>
        </label>
        <?php echo $form['firearms_model']->render(); ?>
        <span class="te-error4"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['firearms_model']->renderError()) : "" ) ?></span>

      </div>
      <div style="clear:both;"></div>



      <div style="width:347px; float:left;position:relative;left:149px;top:8px;">


        <label class="te-form-label">Additional Comments:</label>
        <?php echo $form['additional_comments']->render(); ?>

        <div id="firearm-note"><span style="color:#f00">Important:&nbsp;</span> Required paperwork needed before T&amp;E can be processed. Please download, fill out, and submit the DCS paperwork form. You can download the form here:&nbsp;<a href="uploads/PaperWorkDirectory/DCS.pdf"><span style="color:#000000">Department Certifying Statement</span></a>&nbsp;<span style="font-weight:normal">[PDF 623KB]</span></div>
        <div id="te-submit-btn-div2">
          <input type="image" value="" src="<?php echo Util::getServerHost(sfContext::getInstance()->getConfiguration(), false) ?>images/te-submit-button.png" onclick="javascript:SendForm(); return false;" tabindex="19" id="te-submit-button">
        </div>
      </div>




      <div style="clear:both;"></div>




    </div>
</div>

 </form>