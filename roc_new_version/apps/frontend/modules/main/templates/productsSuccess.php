<script type="text/javascript" src="<?php Util::getServerHost() ?>js/jquery/jquery.js"></script>
<script type="text/javascript">
function SelectCategoryonChange(selected_select_subcategory) {
	// alert( " SelectCategoryonChange  selected_category_id selected_category_id::" + selected_select_category )
	var select_category= document.getElementById('select_category').value;
	if ( select_category=="" ) {
		ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
		return
	}
	var HRef= '<?php echo url_for('@main_getsubcategories?category=') ?>' + encodeURIComponent(select_category)+'/selected_subcategory/' + encodeURIComponent(selected_select_subcategory)
	// alert(HRef)
	jQuery.getJSON(HRef,
	{
	},
	FillSubcategories,
	function(x,y,z) {   //Some sort of error
		alert(x.responseText);
	}
	);
}

function FillSubcategories(data) {
	// alert( "FillSubcategories(data)::"+var_dump(data) )
	var ArrayLength= parseInt(data['length'])
	var selected_subcategory= data['selected_subcategory']
	ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
	var DataArray= data['data']
	for( i=0; i< ArrayLength; i++ ) {
		if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
			var Id= DataArray[i]['id']
			var Name= DataArray[i]['name']
			AddDDLBItem( 'select_subcategory', Id, Name ); //Add all options to input selection
		}
	}
	// alert("selected_subcategory::"+selected_subcategory)
	SetDDLBActiveItem( 'select_subcategory', selected_subcategory )
}
</script>

<form action="<?php echo url_for('@product_listings?page=1&rows_in_pager='.$rows_in_pager  ) ?>" id="form_product_listings" method="POST">
    <?php include_partial( 'main/search_criteria', array( 'select_category'=>$select_category,  'sorting'=>$sorting, 'page'=>$page, 'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle, 'RootItem'=> $RootItem, 'HasRootItem'=> $HasRootItem, 'RootItemObject'=>$RootItemObject, 'Matrix'=> $Matrix ) ) ?>
</form>

<style type="text/css">
#products {width:885px; overflow:hidden; margin:0 auto; font-size:130%;}
#products div.p-section {float:left; overflow:hidden; width:278px; margin:0px 25px 20px 0;
background:url("<?php Util::getServerHost() ?>images/p-section-bottom.png") #e8e8ea left bottom no-repeat;}
#products div.p-section div {overflow:hidden; margin:0 12px 18px 12px; padding:0 0 18px 0; border-bottom:1px solid #aaa;}
#products a h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-additional-sprite.jpg") no-repeat;}

#products div.no-border {border-bottom:none !important;}
#products a {color:#222;}
#products h1 {margin:40px 0 10px 0; font-size:110%;}
#products h2 {margin:0 0 22px 0; width:278px; height:133px; padding:0; font-size:90%; background:url("<?php Util::getServerHost() ?>images/products-h2-sprite.jpg") 0 0 no-repeat;}
#products h2 a {display:inline-block; width:100%; height:100%;}
#products h3 {margin:4px 0 0 0 !important; padding:0 !important; background:red; width:84px !important; height:84px !important; float:left;}
#products h3 span, #products h2 span {display:none;}
#products h3:hover {}
#products ul {margin:0 0 0 12px; padding:0; float:left; list-style-position:inside; font-size:60%; color:#555;}
#products ul.no-h3 {margin-left:97px;}
#products ul a {font-size:150%; color:#555; position:relative; top:1px; left:-3px;}
#products ul a:hover {text-decoration:none;}

h2#p-tactical-firearms {background-position:0 0;}
h2#p-tactical-firearms:hover {background-position:0 -133px;}
h2#p-equipment-gear {background-position:-280px 0;}
h2#p-equipment-gear:hover {background-position:-280px -133px;}
h2#p-clothing-uniforms {background-position:-560px 0;}
h2#p-clothing-uniforms:hover {background-position:-560px -133px;}
h2#p-vehicle-bikepatrol {background-position:-840px 0;}
h2#p-vehicle-bikepatrol:hover {background-position:-840px -133px;}
h2#p-generalsupplies-equipment {background-position:-1120px 0;}
h2#p-generalsupplies-equipment:hover {background-position:-1120px -133px;}
h2#p-emergency-response {background-position:-1400px 0;}
h2#p-emergency-response:hover {background-position:-1400px -133px;}
h2#p-training-resources {background-position:-1680px 0;}
h2#p-training-resources:hover {background-position:-1680px -133px;}
h2#p-forensics {background-position:-1960px 0;}
h2#p-forensics:hover {background-position:-1960px -133px;}


#products a#b-riot-gears {}
#products a#b-riot-gears:hover h3 {background-position:0 -84px;}
#products a#b-tactical h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -1061px -1px; no-repeat;}
#products a#b-tactical:hover h3 {background-position:-1061px -90px;}
#products a#b-body-armor h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -579px -1px; no-repeat;}
#products a#b-body-armor:hover h3{background-position:-579px -90px;}
#products a#b-ammunition h3 {background-position:-89px 0;}
#products a#b-ammunition:hover h3 {background-position:-89px -84px;}
#products a#b-guns h3 {background-position:-178px 0;}
#products a#b-guns:hover h3 {background-position:-178px -84px;}
#products a#b-restraints h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -772px -1px; no-repeat;}
#products a#b-restraints:hover h3{background-position:-772px -90px;}
#products a#b-stun-guns h3 {background-position:-267px 0;}
#products a#b-stun-guns:hover h3 {background-position:-267px -84px;}
#products a#b-duty-gear h3 {background-position:-356px 0;}
#products a#b-duty-gear:hover h3 {background-position:-356px -84px;}
#products a#b-metal-detectors h3 {background-position:-445px 0;}
#products a#b-metal-detectors:hover h3 {background-position:-445px -84px;}
#products a#b-breath-testers h3 {background-position:-534px 0;}
#products a#b-breath-testers:hover h3 {background-position:-534px -84px;}
#products a#b-pepper-spray h3 {background-position:-623px 0;}
#products a#b-pepper-spray:hover h3 {background-position:-623px -84px;}
#products a#b-clothing h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -482px -1px; no-repeat;}
#products a#b-clothing:hover h3{background-position:-482px -90px;}
#products a#b-footwear h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -386px -1px; no-repeat;}
#products a#b-footwear:hover h3{background-position:-386px -90px;}
#products a#b-outwear h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -289px -1px; no-repeat;}
#products a#b-outwear:hover h3{background-position:-289px -90px;}
#products a#b-badges h3 {background-position:-712px 0;}
#products a#b-badges:hover h3 {background-position:-712px -84px;}
#products a#b-vehicle h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") 0 -1px; no-repeat;}
#products a#b-vehicle:hover h3{background-position:0 -90px;}
#products a#b-bicycles h3 {background-position:-801px 0;}
#products a#b-bicycles:hover h3 {background-position:-801px -84px;}
#products a#b-road-safety h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -96px -1px; no-repeat;}
#products a#b-road-safety:hover h3{background-position:-96px -90px;}
#products a#b-gift h3 {background-position:-890px 0;}
#products a#b-gift:hover h3 {background-position:-890px -84px;}
#products a#b-k9 h3 {background-position:-979px 0;}
#products a#b-k9:hover h3 {background-position:-979px -84px;}
#products a#b-emt h3 {background-position:-1068px 0;}
#products a#b-emt:hover h3 {background-position:-1068px -84px;}
#products a#b-emergency-riot-gear:hover h3 {background-position:0 -84px;}
#products a#b-flashlights h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -675px -1px; no-repeat;}
#products a#b-flashlights:hover h3{background-position:-675px -90px;}
#products a#b-books h3 {background-position:-1157px 0;}
#products a#b-books:hover h3 {background-position:-1157px -84px;}
#products a#b-training h3 {background-position:-1246px 0;}
#products a#b-training:hover h3 {background-position:-1246px -84px;}
#products a#b-forensics h3 {background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") -868px -1px; no-repeat;}
#products a#b-forensics:hover h3{background-position:-868px -90px;}

</style>

<div id="products">
	<h1 style="">Products</h1>
		
	<div class="p-section" style="min-height:68em;">
		<h2 id="p-tactical-firearms"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical"><span>Tactical & Firearms</span></a></h2>
		<div>
			<a id="b-riot-gears" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear"><h3><span>Riot Gear</span></h3></a>
			<ul>
				<li><a href="<?php echo Util::getServerHost( sfContext::getInstance()->getConfiguration(), true, false ) ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Megaphones">Megaphones</a></li>
				<li><a href="<?php echo Util::getServerHost( sfContext::getInstance()->getConfiguration(), true, false ) ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Riot+Shields">Riot Shields</a></li>
				<li><a href="<?php echo Util::getServerHost( sfContext::getInstance()->getConfiguration(), true, false ) ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Teargas">Teargas</a></li>
			</ul>
		</div>
		<div>
			<a id="b-tactical" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical"><h3><span>Tactical</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Binoculars">Binoculars</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Clothing">Clothing</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Duty+Gear">Duty Gear</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Gas+Mask">Gas Mask</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Guns">Guns</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Hearing+Protectors">Hearing Protectors</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Helmets">Helmets</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Hydration+Systems">Hydration Systems</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Jumpsuits">Jumpsuits</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Knives">Knives</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Monoculars">Monoculars</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Nightvision">Nightvision</a></li>
			</ul>
		</div>
		<div>
			<a id="b-body-armor" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Body Armor"><h3><span>Body Armor</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Body Armor&select_subcategory=Vests">Vests</a></li>
			</ul>
		</div>
		<div>
			<a id="b-ammunition" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Ammunition"><h3><span>Ammunition</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Ammunition&select_subcategory=Guns">Guns</a></li>
			</ul>
		</div>
		<div>
			<a id="b-guns" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories"><h3><span>Guns</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Guns+(new)">Guns (new)</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Gun+Accessories">Gun Accessories</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Gun+Cleaning">Gun Cleaning</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Gun+Locks">Gun Locks</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Sights+and+Scopes">Sights and Scopes</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Tools">Tools</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Vehicle">Vehicle</a></li>
			</ul>
		</div>
		<div class="no-border">
			<ul class="no-h3">
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Batteries&select_subcategory=Batteries">Batteries</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Targets&select_subcategory=Targets">Targets</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="min-height:68em;">
		<h2 id="p-equipment-gear"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Duty+Gear"><span>Equipment & Gear</span></a></h2>
		<div>
			<a id="b-restraints" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Restraints"><h3><span>Restraints</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Restraints&select_subcategory=Handcuffs">Handcuffs</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Restraints&select_subcategory=Restraints">Restraints</a></li>
			</ul>
		</div>
		<div>
			<a id="b-stun-guns" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Stun+Guns"><h3><span>Stun Guns</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Stun+Guns">Taser</a></li>
			</ul>
		</div>
		<div>
			<a id="b-duty-gear" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical&select_subcategory=Duty+Gear"><h3><span>Duty Gear</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Barrier+Tape">Barrier Tape</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Duty+Gear&select_subcategory=Batons">Batons</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Guns+and+Accessories&select_subcategory=Guns+(new)">Guns</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Duty+Gear&select_subcategory=Holsters">Holsters</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Duty+Gear&select_subcategory=Leather">Leather</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Duty+Gear&select_subcategory=Nylon">Nylon</a></li>
			</ul>
		</div>
		<div>
			<a id="b-metal-detectors" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Metal+Detectors"><h3><span>Metal Detectors</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Metal+Detectors&select_subcategory=Metal+Detectors">Metal Detectors</a></li>
			</ul>
		</div>
		<div>
			<a id="b-breath-testers" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Breath+Testers"><h3><span>Breath Testers</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Breath+Testers&select_subcategory=Breath+Testers">Breath Testers</a></li>
			</ul>
		</div>
		<div>
			<a id="b-pepper-spray" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Pepper+Spray"><h3><span>Pepper+Spray</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Pepper+Spray">Pepper Spray</a></li>
			</ul>
		</div>
		<div class="no-border">
			<ul class="no-h3">
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Batteries">Batteries</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Jail+Supplies">Jail Supplies</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=K9">K9</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="min-height:68em; margin-right:0;">
		<h2 id="p-clothing-uniforms"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing"><span>Clothing & Uniforms</span></a></h2>
		<div>
			<a id="b-clothing" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing"><h3><span>Clothing</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Belts">Belts</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Hats">Hats</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Pants">Pants</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Patches">Patches</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Shirts">Shirts</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Shorts">Shorts</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Sweaters">Sweaters</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing&select_subcategory=Ties">Ties</a></li>
			</ul>
		</div>
		<div>
			<a id="b-footwear" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Footwear"><h3><span>Footwear</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Footwear&select_subcategory=Boots">Boots</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Footwear&select_subcategory=Shoes">Shoes</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Footwear&select_subcategory=Socks">Socks</a></li>
			</ul>
		</div>
		<div>
			<a id="b-outwear" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear"><h3><span>Outerwear</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear&select_subcategory=Coats">Coats</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear&select_subcategory=Eyewear">Eyewear</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear&select_subcategory=Protective+Clothing">Protective Clothing</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear&select_subcategory=Rainwear">Rainwear</a></li>
			</ul>
		</div>
		<div class="no-border">
			<a id="b-badges" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Badges"><h3><span>Badges</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Badges&selected_subcategory=Badges">Badges</a></li>
			</ul>
		</div>
	</div>
		
	<div class="p-section" style="height:65em;">
		<h2 id="p-vehicle-bikepatrol"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle"><span>Vehicle & Bike Patrol</span></a></h2>
		<div>
			<a id="b-vehicle" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle"><h3><span>Vehicle</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Barrier+Tape">Barrier Tape</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Cameras">Cameras</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Car+Emblems">Car Emblems</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Car+Opening+Tools">Car Opening Tools</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Car+Shields">Car Shields</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Consoles">Consoles</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Fire">Fire</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=First+Aid+Kits">First Aid Kits</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Flashlights">Flashlights</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Fusees">Fusees</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Lightbars">Lightbars</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Lights">Lights</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Measure+Devices">Measure Devices</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Push+Bumpers">Push Bumpers</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Radars">Radars</a></li>
<!--
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Radio">Radio</a></li>
-->
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Seat+Organizers">Seat Organizers</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Sirens">Sirens</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Speakers">Speakers</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Tape+Measure">Tape Measure</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Trunk+Organizers">Trunk Organizers</a></li>
			</ul>
		</div>
<!--
		<div>
			<a id="b-bicycles" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Bike+Patrol"><h3><span>Bicycle</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Bike+Patrol&select_subcategory=Bicycles">Bicycles</a></li>
			</ul>
		</div>
-->
		<div class="no-border">
			<a id="b-road-safety" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety"><h3><span>Road Safety</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Chalk">Chalk</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Lightsticks">Lightsticks</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Signs">Signs</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Traffic">Tickets & Holders</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Traffic">Traffic</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety&select_subcategory=Whistles">Whistles</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="height:37em;">
		<h2 id="p-generalsupplies-equipment"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?input_search=equipment"><span>General Supplies & Equipment</span></a></h2>
		<div>
			<a id="b-gift" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Gift"><h3><span>Gift</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Gift">Gift</a></li>
			</ul>
		</div>
		<div>
			<a id="b-k9" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=K9"><h3><span>K9</span></h3></a>
			<ul style="width:150px">
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=K9&select_subcategory=Dog+Handling+Equipment">Dog Handling <span style="position:relative; left:11px;">Equipment</span></a></li>
			</ul>
		</div>
		<div class="no-border">
			<ul class="no-h3">
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Batteries">Batteries</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Breath+Testers">Breath Testers</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Jail+Supplies">Jail Supplies</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Metal+Detectors">Metal Detectors</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Pepper+Spray">Pepper Spray</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="height:37em; margin-right:0;">
		<h2 id="p-emergency-response"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=EMT"><span>Emergency Response</span></a></h2>
		<div>
			<a id="b-emt" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=EMT"><h3><span>EMT</span></h3></a>
			<ul>
<!--
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=EMT&select_subcategory=CPR+Accessories">CPR Accessories</a></li>
-->
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=EMT&select_subcategory=EMS">EMS</a></li>
			</ul>
		</div>
		<div>
			<a id="b-emergency-riot-gear" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear"><h3><span>Riot Gear</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Megaphones">Megaphones</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Riot+Shields">Riot Shields</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Riot+Gear&select_subcategory=Teargas">Teargas</a></li>
			</ul>
		</div>
		<div>
			<a id="b-flashlights" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Flashlights"><h3><span>Flashlights</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Batteries&select_subcategory=Batteries">Batteries</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle&select_subcategory=Flashlights">Flashlights</a></li>
			</ul>
		</div>
		<div class="no-border">
			<ul class="no-h3">
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety">Road Safety</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="height:26.75em;">
		<h2 id="p-training-resources"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Training"><span>Training Resources</span></a></h2>
		<div>
			<a id="b-books" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Books"><h3><span>Books</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Books&select_subcategory=Audio+/+Video">Audio / Video</a></li>
			</ul>
		</div>
		<div class="no-border">
			<a id="b-training" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Training"><h3><span>Training</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Training&select_subcategory=Training+Gear">Training Gear</a></li>
			</ul>
		</div>
	</div>
	
	<div class="p-section" style="height:26.75em; margin-right:0;">
		<h2 id="p-forensics"><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics"><span>Forensics</span></a></h2>
		<div class="no-border">
			<a id="b-forensics" href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics"><h3><span>Forensics</span></h3></a>
			<ul>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics&select_subcategory=Biohazard+Supplies">Biohazard Supplies</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics&select_subcategory=Drug+Test+Kits">Drug Test Kits</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics&select_subcategory=Evidence">Evidence</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics&select_subcategory=Fingerprint">Fingerprint</a></li>
				<li><a href="<?php Util::getServerHost() ?>main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics&select_subcategory=Mirrors">Mirrors</a></li>
			</ul>
		</div>
	</div>

</div>
