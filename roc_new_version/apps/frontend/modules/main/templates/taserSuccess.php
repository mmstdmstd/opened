<script type="text/javascript" language="JavaScript">
<!--

function SendForm() {
  var theForm = document.getElementById("form_taser")
  theForm.submit()
}

//-->
</script>


<form action="<?php echo url_for('@taser') ?>" id="form_taser" method="POST" enctype="multipart/form-data" >
  <?php echo $form['_csrf_token']->render() ?>

<div id="policy_div">
  <h2>Testing &amp; Evaluation (T&amp;E)&nbsp;&mdash;&nbsp;Taser</h2>
    <p>Thank you for your interest in testing and evaluating our selection of ECW products.<br>Please fill out the form below and we will contact you shortly.</p>
    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Contact<br>Information<br>
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="width:342px; float:right;position:relative;">
        <label class="te-form-label">Email:&nbsp;<span style="color:#ff0000;">*</span></label>        
        <?php echo $form['email']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['email']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Phone:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['phone']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['phone']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Address:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['address']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['address']->renderError()) : "" ) ?></span>

        <label class="te-form-label">City:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['city']->render(); ?>
        <span class="te-error2"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['city']->renderError()) : "" ) ?></span>

        <label class="te-form-label4">State:&nbsp;<span style="color:#ff0000;">*</span></label>
        <label class="te-form-label5">Zip Code:&nbsp;<span style="color:#ff0000;">*</span></label>

        <?php echo $form['state']->render(); ?>
        <span style="position:relative; left:73px;  bottom:18px;  color:#ff0000!important; width:82px; float:left;" >
          <?php echo ( $sf_request->isMethod('post') ? strip_tags($form['state']->renderError()) : "" ) ?>
        </span>
        
        
        <?php echo $form['zip']->render(); ?>
        <span style="position:absolute; left:246px; top:263px; color:#ff0000!important; width:82px; float:left;">
          <?php echo ( $sf_request->isMethod('post') ? strip_tags($form['zip']->renderError()) : "" ) ?>
        </span>
      </div>



      


      <div style="width:351px; float:right;">
          <label class="te-form-label">First Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['first_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['first_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Last Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['last_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['last_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Department Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['department_name']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['department_name']->renderError()) : "" ) ?></span>

          <label class="te-form-label">Contact Person for Tasers in Department:&nbsp;<span style="color:#ff0000;">*</span></label>
          <?php echo $form['department_person']->render(); ?>
          <span class="te-error"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['department_person']->renderError()) : "" ) ?></span>
      </div>
      <div style="clear:both;"></div>
    </div>




    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Evaluation<br>Information<br>
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="float:left;">
        <label id="te-form-label3">What make/model of ECWs are you interested in?&nbsp;<span style="color:#ff0000;">*</span>
        </label>
        <?php echo $form['ecws_model']->render(); ?>
        <span class="te-error4"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['ecws_model']->renderError()) : "" ) ?></span>

      </div>
      <div style="clear:both;"></div>

      <div style="width:342px; float:right;position:relative;">
        <label class="te-form-label">Is your agency considering video camera systems?:&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['is_your_agency']->render(); ?>        
        <span class="te-error6"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['is_your_agency']->renderError()) : "" ) ?></span>

        <label class="te-form-label">When do you plan to acquire your camera system?&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['plan_to_acquire_camera']->render(); ?>        
        <span class="te-error6"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['plan_to_acquire_camera']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Do you currently have budget allocated for camera systems?&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['have_budget_allocated_for_camera']->render(); ?>
        <span style=" position:relative;  right:115px;  bottom:-5px;  color:#ff0000!important;  width:82px;  float:right;">
          <?php echo ( $sf_request->isMethod('post') ? strip_tags($form['have_budget_allocated_for_camera']->renderError()) : "" ) ?>
        </span>
      </div>

      <div style="width:347px; float:right;position:relative;">
        <label class="te-form-label">How old are your current ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['current_ecws']->render(); ?>
        <span class="te-error5"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['current_ecws']->renderError()) : "" ) ?></span>


        <label class="te-form-label">When do you plan to acquire or replace your ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['plan_to_replace_ecws']->render(); ?>        
        <span class="te-error5"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['plan_to_replace_ecws']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Do you have budget allocated in your planned<br>acquire/replace timeframe for new ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <?php echo $form['have_budget_ecws']->render(); ?>
        <span class="te-error5"><?php echo ( $sf_request->isMethod('post') ? strip_tags($form['have_budget_ecws']->renderError()) : "" ) ?></span>

        <label class="te-form-label">Additional Notes:</label>
        <?php echo $form['additional_notes']->render(); ?>        

        <div id="te-submit-btn-div">
          ZZ<input type="image" value="" src="<?php echo Util::getServerHost(sfContext::getInstance()->getConfiguration(), false) ?>images/te-submit-button.png" onclick="javascript:SendForm(); return false;" tabindex="19" id="te-submit-button">SS
        </div>

      </div>

      <div style="clear:both;"></div>




    </div>
</div>
 </form>

