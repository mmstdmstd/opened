<?php
$Features = $Product->getFeatures();
$InventoryCategory = $Product->getInventoryCategory();
$Size = $Product->getSize();
$Color = $Product->getColor();
$Gender = $Product->getGender();
$DescriptionShort = $Product->getDescriptionShort();
$Sizing = $Product->getSizing();
$Options = $Product->getOptions();
$Paperwork = $Product->getPaperwork();
$UnitMeasure = $Product->getUnitMeasure();
$QtyOnHand = $Product->getQtyOnHand();
$StdUnitPrice = $Product->getStdUnitPrice();
$Msrp = $Product->getMsrp();
$SaleStartDate = $Product->getSaleStartDate();
$SaleEndDate = $Product->getSaleEndDate();
$TaxClass = $Product->getTaxClass();
$ShipWeight = $Product->getShipWeight();
$SalePromoDiscount = $Product->getSalePromoDiscount();
$Finish = $Product->getFinish();
$SetupCharge = $Product->getSetupCharge();
$SaleMethod = $Product->getSaleMethod();
$SalePrice = $Product->getSalePrice();
$SalePercent = $Product->getSalePercent();
// Util::deb( $Matrix, ' $Matrix::' );
//if ( $HasRootItem and $Matrix and !empty($RootItemObject) ) {
if (!empty($HasRootItem) and !empty($Matrix) and !empty($RootItemObject)) {
  $Features = $RootItemObject->getFeatures();
  $InventoryCategory = $RootItemObject->getInventoryCategory();
  $Size = $RootItemObject->getSize();
  $Color = $RootItemObject->getColor();
  $Gender = $RootItemObject->getGender();
  $DescriptionShort = $RootItemObject->getDescriptionShort();
  $Sizing = $RootItemObject->getSizing();
  $Options = $RootItemObject->getOptions();
  $Paperwork = $RootItemObject->getPaperwork();
  $UnitMeasure = $RootItemObject->getUnitMeasure();
  $QtyOnHand = $RootItemObject->getQtyOnHand();
  $StdUnitPrice = $RootItemObject->getStdUnitPrice();
  $Msrp = $RootItemObject->getMsrp();
  $SaleStartDate = $RootItemObject->getSaleStartDate();
  $SaleEndDate = $RootItemObject->getSaleEndDate();
  $TaxClass = $RootItemObject->getTaxClass();
  $ShipWeight = $RootItemObject->getShipWeight();
  $SalePromoDiscount = $RootItemObject->getSalePromoDiscount();
  $Finish = $RootItemObject->getFinish();
  $SetupCharge = $RootItemObject->getSetupCharge();
  $SaleMethod = $RootItemObject->getSaleMethod();
  $SalePrice = $RootItemObject->getSalePrice();
  $SalePercent = $RootItemObject->getSalePercent();
}
$lInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategory($InventoryCategory);
$InventoryCategoryText = '';
if (!empty($lInventoryCategory)) {
  $InventoryCategoryText = $lInventoryCategory->getCategory();
}

$features_multi_array = array();
$result_colors_list = array();
$result_size_list = array();
$result_options_list = array();
$result_hands_list = array();
$FeaturesList = preg_split('/,/', $Features);
// uasort( $FeaturesList, 'cmp' );
// Util::deb( $FeaturesList, '$FeaturesList::' );
$UsedFeaturesList = array();
// Util::deb( $UsedFeaturesList, ' $UsedFeaturesList::' );
?>

<ul id="select_size">
  <?php foreach ($FeaturesList as $NextFeature) : $NextFeature = strtolower(trim($NextFeature));
    $UsedFeaturesList[] = $NextFeature; ?>
    <?php if ($NextFeature == 'inventory_category') : ?>
        <?php if (!empty($InventoryCategoryText)) : ?>
        <li>
        <?php echo $InventoryCategoryText ?>
        </li>
      <?php endif; ?>
    <?php endif; ?>

    <?php
    //Util::deb($NextFeature, '$NextFeature::');
    if ($NextFeature == 'size') {

      $SizesList = InventoryItemPeer::getSizesListByRootItem($RootItem);
      $SizesList = AppUtils::getSizeOrderedArray($SizesList);

      Util::deb($SizesList, '$SizesList::');

      $result_size_list = array();
      if (!empty($SizesList)) {
        foreach ($SizesList as $key => $value) {
          $IsFound = false;
          for ($i = 0; $i < count($features_multi_array); $i++) {
            if ($features_multi_array[$i]['sku'] == $key) {
              $features_multi_array[$i]['size'] = $value;
              $IsFound = true;
              break;
            }
          }
          if (!$IsFound) {
            $img = get_image_by_sku($key);
            $features_multi_array[] = array('sku' => $key, 'size' => $value, 'color' => '', 'option' => '', 'hand' => '', 'image' => $img);
          }

          $result_size_list[$key] = $value;
        }  // foreach($SizesList as $key => $value) {
        echo '<li>';
        echo '<b>Size:</b><br />' . Util::select_tag('select_size_', Util::SetArrayHeader(array('' => ' -select- '), /* $result_size_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'size\'); return false;')) . '&nbsp;&nbsp';
        if (!empty($Sizing)) {
          echo '<span>';
          $Src = '<a class="thickbox" href="' . url_for("@product_size_chart?code=" . $Sizing) . '">Size Chart</a> ';
          echo $Src;
          echo '</span>';
        }
        echo '</li><br />';
      } // if (!empty($SizesList)) {
    } //if ($NextFeature == 'size') {


    if ($NextFeature == 'color') {
      $ColorsList = InventoryItemPeer::getColorsListByRootItem($RootItem);
      Util::deb($ColorsList, '$ColorsList::');
      if (!empty($ColorsList)) {
        $result_colors_list = array();
        foreach ($ColorsList as $key => $value) {
          $IsFound = false;
          for ($i = 0; $i < count($features_multi_array); $i++) {
            if ($features_multi_array[$i]['sku'] == $key) {
              $features_multi_array[$i]['color'] = $value;
              $IsFound = true;
              break;
            }
          }
          if (!$IsFound) {
            $img = get_image_by_sku($key);
            $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => $value, 'option' => '', 'hand' => '', 'image' => $img);
          }
          $result_colors_list[$key] = $value;
        } // foreach ($ColorsList as $key => $value) {
        echo '<li id="li_color" >';
        echo '<b>Color:</b><br />' . Util::select_tag('select_color', Util::SetArrayHeader(array('' => ' -select- '), /* $result_colors_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'color\'); return false;'));
        echo '</li>';
      } // if (!empty($ColorsList)) {
    } //if ($NextFeature == 'color') {

    if ($NextFeature == 'options') {
      $OptionsList = InventoryItemPeer::getOptionsListByRootItem($RootItem);
      Util::deb($OptionsList, '$OptionsList::');
      if (!empty($OptionsList)) {
        $result_Options_list = array();
        foreach ($OptionsList as $key => $value) {
          $IsFound = false;
          for ($i = 0; $i < count($features_multi_array); $i++) {
            if ($features_multi_array[$i]['sku'] == $key) {
              $features_multi_array[$i]['option'] = $value;
              $IsFound = true;
              break;
              //Util::deb( $features_multi_array, '????? $features_multi_array::' );
            }
          }
          if (!$IsFound) {
            $img = get_image_by_sku($key);
            $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => '', 'option' => $value, 'hand' => '', 'image' => $img);
          }
          $result_options_list[$key] = $value;
        } // foreach ($OptionsList as $key => $value) {
        echo '<li id="li_option" >';
        echo '<b>Option:</b><br />' . Util::select_tag('select_option', Util::SetArrayHeader(array('' => ' -select- '), /* $result_options_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'option\'); return false;'));
        echo '</li>';
      } // if (!empty($OptionsList)) {
    } //if ($NextFeature == 'options') {

    if ($NextFeature == 'hand') {
      break;
      $HandsList = InventoryItemPeer::getHandsListByRootItem($RootItem);
      if (!empty($HandsList)) {
        $result_hands_list = array();
        foreach ($HandsList as $key => $value) {
          $IsFound = false;
          for ($i = 0; $i < count($features_multi_array); $i++) {
            if ($features_multi_array[$i]['sku'] == $key) {
              $features_multi_array[$i]['hand'] = $value;
              $IsFound = true;
              break;
              //Util::deb( $features_multi_array, '????? $features_multi_array::' );
            }
          }
          if (!$IsFound) {
            $img = get_image_by_sku($key);
            $features_multi_array[] = array('sku' => $key, 'size' => '', 'color' => '', 'option' => '', 'hand' => $value, 'image' => $img);
          }
          $result_hands_list[$key] = $value;
        } // foreach ($HandsList as $key => $value) {
        echo '<li id="li_hand" >';
        echo '<b>Hand:</b><br />' . Util::select_tag('select_hand', Util::SetArrayHeader(array('' => ' -select- '), /* $result_hands_list */ array()), '', array('onchange' => 'javascript:SelectFeatures(\'hand\'); return false;'));
        echo '</li>';
      } // if (!empty($HandsList)) {
    } //if ($NextFeature == 'hand') {


    if ($NextFeature == 'color' and !in_array('size', $FeaturesList)) {
      //die("INSIDE 1");
      $ColorsList = InventoryItemPeer::getColorsListByRootItem($RootItem);
      $ColorsList = array_unique($ColorsList);
      if (!empty($ColorsList)) {
        echo '<li id="li_color" >';
        echo '<b>Color:</b><br />' . Util::select_tag('select_color', Util::SetArrayHeader(array('' => ' -select- '), $ColorsList), '', array('onchange' => 'javascript:SelectFeatures(\'color\'); return false;'));
        echo '</li>';
      }
    }
    if (!in_array('size', $FeaturesList)) {
      echo '<li>';
      if (!empty($Sizing)) {
        echo '<span>';
        $Src = '<a class="thickbox" href="' . url_for("@product_size_chart?code=" . $Sizing) . '">Size Chart</a> ';
        echo $Src;
        echo '</span>';
      }
      echo '</li><br />';
    }
    ?>


  <?php endforeach;  // Util::deb( $features_multi_array, 'LAST $features_multi_array::' );  ?>
</ul>

<script type="text/javascript">

  jQuery(function($) {
    repopulate_sizes( '', '' );
    repopulate_colors( '', '' );
    repopulate_options();
    repopulate_hands();
    // alert("AFTER!!")
  });

  var featureses = jQuery.parseJSON(<?php /* Util::deb( $features_multi_array, '$features_multi_array::' );    die("!!!"); */ print json_encode(json_encode($features_multi_array)); ?>);
  //alert(  "featureses::"+var_dump(featureses)  )
  function SelectFeatures(type)
  {
    // alert("SelectFeatures type::"+type)
    var sku   = '';
    var color = '';
    var image = '';
    if (type == 'size') {
      sku = jQuery('#select_size_ option:selected').val();
      var size = jQuery('#select_size_ option:selected').text();

      if (sku !='') {
        repopulate_colors( sku, size );
        repopulate_options();
        repopulate_hands();
        for (i = 0; i < featureses.length; i++) {
          if ( featureses[i].sku == sku ) {
            color = featureses[i].color;
            image = featureses[i].image;
          }
        }
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_colors( '', '' );
      }
    } // if (type == 'size') {

    if (type == 'color') {
      color = jQuery('#select_color option:selected').text();
      sku   = jQuery('#select_color option:selected').val();
      color = jQuery.trim(color);
      if (color != jQuery.trim('-select-')) {
        repopulate_sizes( sku, color );
        repopulate_options();
        repopulate_hands();
        for (i = 0; i < featureses.length; i++) {
          // alert( "featureses[i].color::"+ featureses[i].color + "  color::"+color)
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        // repopulate_colors();
        repopulate_sizes( '', '' );
      }
    } // if (type == 'color') {


    if (type == 'option') {
      option = jQuery('#select_option option:selected').text();
      sku   = jQuery('#select_option option:selected').val();
      option = jQuery.trim(option);
      if (option != jQuery.trim('-select-')) {
        repopulate_sizes( sku, '' );
        repopulate_colors( sku, '' );
        repopulate_hands();
        for (i = 0; i < featureses.length; i++) {
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_options();
      }
    } // if (type == 'option') {

    if (type == 'hand') {
      hand = jQuery('#select_hand option:selected').text();
      sku   = jQuery('#select_hand option:selected').val();
      hand = jQuery.trim(hand);
      if (hand != jQuery.trim('-select-')) {
        repopulate_sizes( sku, '' );
        repopulate_colors( sku, '' );
        repopulate_options();
        for (i = 0; i < featureses.length; i++) {
          if (featureses[i].sku == sku) {
            image = featureses[i].image;
          }
        }
        // $("#select_size_ :nth-child(2)").attr("selected", "selected");
        // sku = $("#select_size_ :nth-child(2)").val();
        jQuery('#sku').html('Item# ' + sku);
        if (image != '') {
          var img_product = '<a href="'+ image +'">';
          img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + image + '" style="cursor:pointer;" /><br />';
          img_product    += '</a>';
          jQuery('#product_photo_div').html(img_product);
        }
      }
      else {
        repopulate_hands();
      }
    } // if (type == 'hand') {

  }

  function repopulate_colors( SelectedSku, SelectedSize )
  {
    var temp_colors     = jQuery.parseJSON(<?php print json_encode(json_encode($result_colors_list)); ?>);
    // alert(  " repopulate_colors  temp_colors::"+var_dump(temp_colors)  + "  SelectedSku::"+SelectedSku+"  SelectedSize::"+SelectedSize )
    var rep_colors_list = '<option value=""> -select-</option>';
    var filled_colors= Array();
    var CurrentColorValue = jQuery('#select_color option:selected').text();
    // alert( "CurrentColorValue::"+CurrentColorValue )

    jQuery.each(temp_colors, function(i, val) {
      IsSkip= false;
      if ( SelectedSize!= "" ) {
        for (J = 0; J < featureses.length; J++) {
          if ( i== featureses[J].sku && featureses[J].size != SelectedSize  ) {
            IsSkip= true;
            break;
          }
        }
      }

      var IsFilled= false
      for( J= 0; J< filled_colors.length;J++  ) {
        if ( filled_colors[J] == val ) {
          IsFilled= true;
          break;
        }
      }
      if ( !IsFilled && !IsSkip ) {
        var SelectedTag= ""

        //alert( "val::" + val + "   CurrentColorValue::" + CurrentColorValue )

        if ( CurrentColorValue == val ) {
          SelectedTag= " selected "
        }
        rep_colors_list += '<option value="'+ i +'" '+SelectedTag+' >'+ val +'</option>';
        filled_colors[filled_colors.length]= val;
      }
    });   // alert(" rep_colors_list::"+rep_colors_list )
    jQuery('#select_color').html(rep_colors_list);
  }

  function repopulate_sizes( SelectedSku, SelectedColor )
  {
    var temp_sizes     = jQuery.parseJSON(<?php print json_encode(json_encode($result_size_list)); ?>);
    // alert("repopulate_sizes temp_sizes::"+var_dump(temp_sizes) + "  SelectedSku::"+SelectedSku+"  SelectedColor::"+SelectedColor )
    var rep_sizes_list = '<option value=""> -select-</option>';
    var filled_sizes= Array();
    //alert(-1)
    var CurrentSizeValue = jQuery('#select_size_ option:selected').text();
    // alert( "CurrentSizeValue::"+CurrentSizeValue )

    jQuery.each(temp_sizes, function(i, val) {
      // alert( "++ i::"+i + "    val::" + val )
      IsSkip= false;
      if ( SelectedColor!= "" ) {
        for (J = 0; J < featureses.length; J++) {
          if ( i== featureses[J].sku && featureses[J].color != SelectedColor  ) {
            IsSkip= true;
            break;
          }
        }
      }

      var IsFilled= false
      for( J= 0; J< filled_sizes.length; J++  ) {
        if ( filled_sizes[J] == val ) {
          IsFilled= true;
          break;
        }
      }

      // alert( "i::"+i + "    val::" + val + "  SelectedSku::" + SelectedSku + "    SelectedColor::" + SelectedColor + "  IsFilled::" + IsFilled + "  IsSkip::" + IsSkip )

      if ( !IsFilled && !IsSkip ) {
        var SelectedTag= ""
        // alert( "val::" + val + "   CurrentSizeValue::" + CurrentSizeValue )
        if ( CurrentSizeValue == val ) {
          SelectedTag= " selected "
        }
        rep_sizes_list += '<option value="'+ i +'" '+SelectedTag+' >'+ val +'</option>';
        filled_sizes[filled_sizes.length]= val;
      }
    });
    // alert(" rep_sizes_list::"+rep_sizes_list )
    jQuery('#select_size_').html(rep_sizes_list);
  }

  function repopulate_options()
  {
    var temp_options     = jQuery.parseJSON(<?php print json_encode(json_encode($result_options_list)); ?>);
    var select_option_value = jQuery('#select_color option:selected').val(); // select_option
    // alert( "repopulate_options  temp_options::"+var_dump(temp_options) )
    var rep_options_list = '<option value=""> -select-</option>';
    var filled_options= Array();
    jQuery.each(temp_options, function(i, val) {
      var IsFilled= false
      for( J= 0; J< filled_options.length;J++  ) {
        if ( filled_options[J] == val ) {
          IsFilled= true;
          break;
        }
      }
      if ( !IsFilled ) {
        rep_options_list += '<option value="'+ i +'">'+ val +'</option>';
        filled_options[filled_options.length]= val;
      }
    });
    jQuery('#select_option').html(rep_options_list);
  }

  function repopulate_hands()
  {
    var temp_hands     = jQuery.parseJSON(<?php print json_encode(json_encode($result_hands_list)); ?>);
    // alert( "repopulate_hands  temp_hands::"+var_dump(temp_hands) )
    var rep_hands_list = '<option value=""> -select-</option>';
    var filled_hands= Array();
    jQuery.each(temp_hands, function(i, val) {
      var IsFilled= false
      for( J= 0; J< filled_hands.length;J++  ) {
        if ( filled_hands[J] == val ) {
          IsFilled= true;
          break;
        }
      }
      if ( !IsFilled ) {
        rep_hands_list += '<option value="'+ i +'">'+ val +'</option>';
        filled_hands[filled_hands.length]= val;
      }
    });
    jQuery('#select_hand').html(rep_hands_list);
  }

</script>

<?php

function get_image_by_sku($sku) {
  $temp_file_name = '';
  $check = '';

  $thumbnail_image_file = AppUtils::getThumbnailImageBySku($sku, false);
  if (empty($thumbnail_image_file)) {
    $thumbnail_image_file = AppUtils::getResizedImageBySku($sku, false, false, true);
    $check = $thumbnail_image_file['ImageFileName'];
    if (is_array($thumbnail_image_file)) {
      $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file['ImageFileName'];
      $temp_file_name = $thumbnail_image_file;
    } else {
      $thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file;
      $temp_file_name = $thumbnail_image_file;
    }
    $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $thumbnail_image_file;
    if (!file_exists($temp_file_name)) {
      $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
    }
  }
  if ($check == '') {
    $thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
  }
  return $thumbnail_image_file;
}
?>