<?php 
  $Features= $Product->getFeatures();
  $InventoryCategory= $Product->getInventoryCategory();
  $Size= $Product->getSize();  
  $Color= $Product->getColor();
  $Gender= $Product->getGender();
  $DescriptionShort= $Product->getDescriptionShort();
  $Sizing= $Product->getSizing();
  $Options= $Product->getOptions();  
  $Paperwork= $Product->getPaperwork();  
  $UnitMeasure= $Product->getUnitMeasure();
  $QtyOnHand= $Product->getQtyOnHand();
  $StdUnitPrice= $Product->getStdUnitPrice();
  $Msrp= $Product->getMsrp();  
  $SaleStartDate= $Product->getSaleStartDate();
  $SaleEndDate= $Product->getSaleEndDate();
  $TaxClass= $Product->getTaxClass();
  $ShipWeight= $Product->getShipWeight();
  $SalePromoDiscount= $Product->getSalePromoDiscount();  
  $Finish= $Product->getFinish();
  $SetupCharge= $Product->getSetupCharge();    
  $SaleMethod= $Product->getSaleMethod();  
  $SalePrice= $Product->getSalePrice();
  $SalePercent= $Product->getSalePercent();  
//Util::deb( $Matrix, ' $Matrix::' );
  if ( $HasRootItem and $Matrix and !empty($RootItemObject) ) {
    $Features= $RootItemObject->getFeatures();
    $InventoryCategory= $RootItemObject->getInventoryCategory();
    $Size= $RootItemObject->getSize();  
    $Color= $RootItemObject->getColor();
    $Gender= $RootItemObject->getGender();
    $DescriptionShort= $RootItemObject->getDescriptionShort();
    $Sizing= $RootItemObject->getSizing();
    $Options= $RootItemObject->getOptions();  
    $Paperwork= $RootItemObject->getPaperwork();  
    $UnitMeasure= $RootItemObject->getUnitMeasure();
    $QtyOnHand= $RootItemObject->getQtyOnHand();
    $StdUnitPrice= $RootItemObject->getStdUnitPrice();
    $Msrp= $RootItemObject->getMsrp();  
    $SaleStartDate= $RootItemObject->getSaleStartDate();
    $SaleEndDate= $RootItemObject->getSaleEndDate();
    $TaxClass= $RootItemObject->getTaxClass();
    $ShipWeight= $RootItemObject->getShipWeight();
    $SalePromoDiscount= $RootItemObject->getSalePromoDiscount();  
    $Finish= $RootItemObject->getFinish();
    $SetupCharge= $RootItemObject->getSetupCharge(); 
    $SaleMethod= $RootItemObject->getSaleMethod();  
    $SalePrice= $RootItemObject->getSalePrice();
    $SalePercent= $RootItemObject->getSalePercent();  
  }
  $FeaturesList= preg_split( '/,/', $Features);
  Util::deb( $FeaturesList, ' $FeaturesList::' );
  $UsedFeaturesList= array();
  //Util::deb( $UsedFeaturesList, ' $UsedFeaturesList::' );
  $NextFeature= '!';
?>

    <ul id="select_size">
    <?php // foreach( $FeaturesList as $NextFeature ) : $NextFeature= strtolower(trim($NextFeature)); $UsedFeaturesList[]= $NextFeature; ?>

      <?php if ( $NextFeature == 'inventory_category' ) : ?>
        <?php if ( !empty($InventoryCategory) ) : ?>
          <li>
            <?php echo $InventoryCategory ?>
          </li> 
        <?php endif; ?>        
      <?php endif; ?>

      <?php if ( $NextFeature == 'size' ) : ?>            
        <?php $SizesList= InventoryItemPeer::getSizesListByRootItem( $RootItem );
          if( !empty($SizesList) ) : ?> 
          <li>
            <?php echo Util::select_tag('select_size', Util::SetArrayHeader( array( ''=>' Select Size ' ), $SizesList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') ) ?> &nbsp;&nbsp; 
            <?php if ( !empty($Sizing) ) : ?>
              <span><?php $Src= '<a class="thickbox" href="'.url_for( "@product_size_chart?code=".$Sizing ) . '">Size Chart</a> ';
              echo $Src;
            ?></span>
            <?php endif ?>
          </li>
        <?php endif; ?>
      <?php endif; ?>
                      
      <?php $ColorsList= InventoryItemPeer::getColorsListByRootItem( $RootItem );
      //Util::deb( $ColorsList, ' $ColorsList::' );
      $ShowColor= ( in_array('color',$FeaturesList ) and !empty($ColorsList) );
      Util::deb( $ShowColor, ' $ShowColor::' ); ?>
      
          <li id="li_color" <?php echo ($ShowColor)?"":'style="display:none"' ?> >
            <?php echo Util::select_tag('select_color', Util::SetArrayHeader( array( ''=>' Select Color ' ), $ColorsList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li>
                            
      
      <?php 
      if ( $NextFeature == 'options' ) : ?>
        <?php $OptionsList= InventoryItemPeer::getOptionsListByRootItem( $RootItem );
          if ( !empty($OptionsList) ) : ?>
          <li>
            <?php echo Util::select_tag('select_options', Util::SetArrayHeader( array( ''=>' Select Options ' ), $OptionsList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      
      <?php if ( $NextFeature == 'gender' ) : ?>            
        <?php $GendersList= InventoryItemPeer::getGendersListByRootItem( $RootItem );
          if ( !empty($GendersList) ) : ?>
          <li>
            <?php echo Util::select_tag('select_genders', Util::SetArrayHeader( array( ''=>' Select Gender ' ), $GendersList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'description_short' ) : ?>
        <?php $DescriptionShortsList= InventoryItemPeer::getDescriptionShortsListByRootItem( $RootItem );
          if ( !empty($DescriptionShortsList) ) : ?>
          <li>
            <?php echo Util::select_tag('select_description_shorts', Util::SetArrayHeader( array( ''=>' Select Description Short ' ), $DescriptionShortsList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      
      <?php if ( $NextFeature == 'paperwork' ) : ?>
        <?php $PaperworksList= InventoryItemPeer::getPaperworksListByRootItem( $RootItem );
          if ( !empty($PaperworksList) ) : ?>
          <li>
            <?php echo Util::select_tag('select_paperwork', Util::SetArrayHeader( array( ''=>' Select Paperwork ' ), $PaperworksList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'unit_measure' ) : ?>
        <?php $UnitMeasuresList= InventoryItemPeer::getUnitMeasuresListByRootItem( $RootItem );
          if ( !empty($UnitMeasuresList) ) : ?>
          <li>
            <?php echo Util::select_tag('select_unit_measures', Util::SetArrayHeader( array( ''=>' Select Unit Measures ' ), $UnitMeasuresList), $Product->getSku(), array('onchange'=>'javascript:SelectInventotyItemBySku( this.value )') )?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      <?php if ( $NextFeature == 'qty_on_hand' ) : ?>
        <?php if ( !empty($QtyOnHand) ) : ?>
          <li>
            <small>Qty On Hand:</small>&nbsp;<?php echo $QtyOnHand ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'std_unit_price' ) : ?>
        <?php if ( !empty($StdUnitPrice) ) : ?>
          <li>
            <small>Std Unit Price:</small>&nbsp;$<?php echo $StdUnitPrice ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      <?php if ( $NextFeature == 'msrp' ) : ?>
        <?php if ( !empty($Msrp) ) : ?>
          <li>
            <small>Msrp:</small>&nbsp;$<?php echo $Msrp ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'sale_start_date' ) : ?>
        <?php if ( !empty($SaleStartDate) ) : ?>
          <li>
            <small>Sale Start Date:</small>&nbsp;<?php echo strftime( sfConfig::get('app_application_date_format'), strtotime($SaleStartDate) ) ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      <?php if ( $NextFeature == 'sale_end_date' ) : ?>
        <?php if ( !empty($SaleEndDate) ) : ?>
          <li>
            <small>Sale End Date:</small>&nbsp;<?php echo strftime( sfConfig::get('app_application_date_format'), strtotime($SaleEndDate) ) ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'tax_class' ) : ?>
        <?php if ( !empty($TaxClass) ) : ?>
          <li>
            <small>Tax Class:</small>&nbsp;<?php echo $TaxClass ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      <?php if ( $NextFeature == 'ship_weight' ) : ?>
        <?php if ( !empty($ShipWeight) ) : ?>
          <li>
            <small>Ship Weight:</small>&nbsp;<?php echo $ShipWeight ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      
      <?php if ( $NextFeature == 'sale_promo_discount' ) : ?>
        <?php if ( !empty($SalePromoDiscount) ) : ?>
          <li>
            <small>Sale Promo Discount:</small>&nbsp;<?php echo $SalePromoDiscount ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      <?php if ( $NextFeature == 'finish' ) : ?>
        <?php if ( !empty($Finish) ) : ?>
          <li>
            <small>Finish:</small>&nbsp;<?php echo $Finish ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'setup_charge' ) : ?>
        <?php if ( !empty($SetupCharge) ) : ?>
          <li>
            <small>Setup Charge:</small>&nbsp;<?php echo $SetupCharge ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      <?php if ( $NextFeature == 'sale_method' ) : ?>
        <?php if ( !empty($SaleMethod) ) : ?>
          <li>
            <small>Sale Method:</small>&nbsp;<?php echo $SaleMethod ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>            
      
      <?php if ( $NextFeature == 'sale_price' ) : ?>
        <?php if ( !empty($SalePrice) ) : ?>
          <li>
            <small>Sale Price:</small>&nbsp;<?php echo $SalePrice ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>
      
      
      
      <?php if ( $NextFeature == 'sale_percent' ) : ?>
        <?php if ( !empty($SalePercent) ) : ?>
          <li>
            <small>Sale Percent:</small>&nbsp;<?php echo $SalePercent ?>
          </li> 
        <?php endif; ?>
      <?php endif; ?>

      
      
    <?php //endforeach; ?>
    
    
    <?php if ( !in_array( 'inventory_category', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($InventoryCategory) ) : ?>
        <li>
          <small>Inventory Category:</small>&nbsp;<?php echo $InventoryCategory ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'size', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Size) ) : ?>
        <li>
          <small>Size:</small>&nbsp;<?php echo $Size ?>
            <?php if ( !empty($Sizing) ) : ?>
              <span><?php $Src= '<a class="thickbox" href="'.url_for( "@product_size_chart?code=".$Sizing ) . '">Size Chart</a> ';
              echo $Src;
            ?></span>
            <?php endif ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'color', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Color) ) : ?>
        <li>
          <small>Color:</small>&nbsp;<?php echo $Color ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'options', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Options) ) : ?>
        <li>
          <small>Options:</small>&nbsp;<?php echo $Options ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    
    
    <?php if ( !in_array( 'gender', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Gender) ) : ?>
        <li>
          <small>Gender:</small>&nbsp;<?php echo $Gender ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    
    <?php if ( !in_array( 'description_short', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($DescriptionShort) ) : ?>
        <li>
          <small>Description Short:</small>&nbsp;<?php echo $DescriptionShort ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'paperwork', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Paperwork) ) : ?>
        <li>
          <small>Paperwork:</small>&nbsp;<?php echo $Paperwork ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'unit_measure', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($UnitMeasure) ) : ?>
        <li>
          <small>Unit Measure:</small>&nbsp;<?php echo $UnitMeasure ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'qty_on_hand', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($QtyOnHand) ) : ?>
        <li>
          <small>Qty On Hand:</small>&nbsp;<?php echo $QtyOnHand ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'msrp', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Msrp) ) : ?>
        <li>
          <small>Msrp:</small>&nbsp;<?php echo $Msrp ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'sale_start_date', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SaleStartDate) ) : ?>
        <li>
          <small>Sale Start Date:</small>&nbsp;<?php echo $SaleStartDate ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'sale_end_date', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SaleEndDate) ) : ?>
        <li>
          <small>Sale End Date:</small>&nbsp;<?php echo $SaleEndDate ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'tax_class', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($TaxClass) ) : ?>
        <li>
          <small>Tax Class:</small>&nbsp;<?php echo $TaxClass ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'ship_weight', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($ShipWeight) ) : ?>
        <li>
          <small>Ship Weight:</small>&nbsp;<?php echo $ShipWeight ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'sale_promo_discount', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SalePromoDiscount) ) : ?>
        <li>
          <small>Sale Promo Discount:</small>&nbsp;<?php echo $SalePromoDiscount ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    <?php if ( !in_array( 'finish', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($Finish) ) : ?>
        <li>
          <small>Finish:</small>&nbsp;<?php echo $Finish ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'setup_charge', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SetupCharge) ) : ?>
        <li>
          <small>Setup Charge:</small>&nbsp;<?php echo $SetupCharge ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'sale_method', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SaleMethod) ) : ?>
        <li>
          <small>Sale Method:</small>&nbsp;<?php echo $SaleMethod ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
           
    <?php if ( !in_array( 'sale_price', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SalePrice) ) : ?>
        <li>
          <small>Sale Price:</small>&nbsp;<?php echo $SalePrice ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>

    <?php if ( !in_array( 'sale_percent', $UsedFeaturesList ) ) : ?>
      <?php if ( !empty($SalePercent) ) : ?>
        <li>
          <small>Sale Percent:</small>&nbsp;<?php echo $SalePercent ?>
        </li> 
      <?php endif; ?>    
    <?php endif; ?>
    
    </ul>
    
