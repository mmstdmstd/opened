<div id="div_cart_info">
	<img id="modal-window-close-button" onclick="javascript:tb_remove()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png" />
    <?php
	if (empty($items_count)) {
		$items_count = Cart::getItemsCount();
	}
	if (empty($common_sum)) {
		$common_sum = Cart::getCommonSum(true);
	}
	if ($message_type == 'delete') { ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) deleted from your cart</p>
    <?php
    }
    else {
    ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) added to your cart</p>
    <?php } ?>
	<p id="span_cart_info">Cart: <?php echo $items_count ?> items   $<?php echo $common_sum ?></p>
    <?php if (empty($hide_view_message)) { ?>
	    <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>
	    <p id="click-anywhere">Click anywhere on this page<br /> to continue shopping</p>
    <?php } ?>
	<a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>
