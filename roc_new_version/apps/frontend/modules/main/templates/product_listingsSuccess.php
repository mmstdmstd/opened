<?php
$product_quantity='';
$PageUrl= 'product_listings';
$product_listings_columns= 3;//(int)sfConfig::get('app_application_product_listings_columns' );
$InventoryItemsArray= array();
foreach( $InventoryItemsPager->getResults() as $InventoryItem ) {
	$InventoryItemsArray[]= $InventoryItem;
}
$InventoryItemsCount= count($InventoryItemsArray);
if ( $InventoryItemsCount< 4 ) $InventoryItemsCount= 4;

$RowsCount= (int) ($InventoryItemsCount/$product_listings_columns);
$HasDecimal= $InventoryItemsCount%$product_listings_columns;
if ( $HasDecimal> 0 ) $RowsCount++;

$checked_filters= '';
$original_SelectedBrandsArray = $sf_data->getRaw('SelectedBrandsArray');
$selected_brand_as_string= '';
foreach( $original_SelectedBrandsArray as $original_SelectedBrand ) {
  if ( empty($original_SelectedBrand) ) continue;
  $selected_brand_as_string.= $original_SelectedBrand.ElemSeparator;
}
if ( !empty($selected_brand_as_string) ) {
  $checked_filters.= 'SelectedBrands='.$selected_brand_as_string.'|';
}


$original_SelectedSizesArray = $sf_data->getRaw('SelectedSizesArray');
$selected_size_as_string= '';
foreach( $original_SelectedSizesArray as $original_SelectedSize ) {
  if ( empty($original_SelectedSize) ) continue;
  $selected_size_as_string.= $original_SelectedSize.ElemSeparator;
}
if ( !empty($selected_size_as_string) ) {
  $checked_filters.= 'SelectedSizes='.$selected_size_as_string.'|';
}


$original_SelectedColorsArray = $sf_data->getRaw('SelectedColorsArray');
$selected_color_as_string= '';
foreach( $original_SelectedColorsArray as $original_SelectedColor ) {
  if ( empty($original_SelectedColor) ) continue;
  $selected_color_as_string.= $original_SelectedColor.ElemSeparator;
}
if ( !empty($selected_color_as_string) ) {
  $checked_filters.= 'SelectedColors='.$selected_color_as_string.'|';
}


$original_SelectedGendersArray = $sf_data->getRaw('SelectedGendersArray');
$selected_gender_as_string= '';
foreach( $original_SelectedGendersArray as $original_SelectedGender ) {
    if ( empty($original_SelectedGender) ) continue;
    $selected_gender_as_string.= $original_SelectedGender.ElemSeparator;
}
if ( !empty($selected_gender_as_string) ) {
    $checked_filters.= 'SelectedGenders='.$selected_gender_as_string.'|';
}

$original_SelectedPricesArray = $sf_data->getRaw('SelectedPricesArray');
$selected_price_as_string= '';
foreach( $original_SelectedPricesArray as $original_SelectedPrice ) {
    if ( empty($original_SelectedPrice) ) continue;
    $selected_price_as_string.= $original_SelectedPrice.ElemSeparator;
}
if ( !empty($selected_price_as_string) ) {
    $checked_filters.= 'SelectedPrices='.$selected_price_as_string.'|';
}


$original_SelectedSale_PricesArray = $sf_data->getRaw('SelectedSale_PricesArray');
$selected_sale_price_as_string= '';
foreach( $original_SelectedSale_PricesArray as $original_SelectedSale_Price ) {
    if ( empty($original_SelectedSale_Price) ) continue;
    $selected_sale_price_as_string.= $original_SelectedSale_Price.ElemSeparator;
}
if ( !empty($selected_sale_price_as_string) ) {
    $checked_filters.= 'SelectedSale_Prices='.$selected_sale_price_as_string.'|';
}



$original_SelectedFinishsArray = $sf_data->getRaw('SelectedFinishsArray');
$selected_finish_as_string= '';
foreach( $original_SelectedFinishsArray as $original_SelectedFinish ) {
	if ( empty($original_SelectedFinish) ) continue;
	$selected_finish_as_string.= $original_SelectedFinish.ElemSeparator;
}
if ( !empty($selected_finish_as_string) ) {
	$checked_filters.= 'SelectedFinishs='.$selected_finish_as_string.'|';
}
$badge_added= $sf_request->getParameter('badge_added');
if ( $badge_added== '1' ) {
	$product_quantity= 1;
};
$badge_sku= $sf_request->getParameter('badge_sku');


?>
  <script type="text/javascript" language="JavaScript">
  <!--

  function onSubmit(  ) {
    document.getElementById("checked_filters").value=""
    var theForm = document.getElementById("form_filter");
    theForm.submit();
  }

  jQuery('body').click(function(){
    jQuery('#div_cart_info').hide();
  });

  function onChangeSelectSortBy(value) {
  	var Url= '<?php echo url_for( '@'.$PageUrl.'?page=1&rows_in_pager='.$rows_in_pager.'&sorting=ZZZZZ'.'&select_category=' . urlencode($select_category)  .'&select_subcategory=' . urlencode($select_subcategory) .
            '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search) . '&page_type=' . 'product_listings' . '&checked_filters=' . urlencode($checked_filters) ) ?>'
    Url= Url.replace(/ZZZZZ/g, value);
  	document.location= Url
  }

  function onChangeSelectItemsPerPage(value) {
		//alert("$checked_filters::"+$checked_filters)
  	var Url= '<?php echo url_for( '@'.$PageUrl.'?page=1&rows_in_pager=ZZZZZ'.'&sorting='.$sorting.'&select_category=' . urlencode($select_category)  .'&select_subcategory=' . urlencode($select_subcategory) .
            '&select_brand_id=' . urlencode($select_brand_id) . '&input_search=' . urlencode($input_search) . '&page_type=' . 'product_listings' . '&checked_filters=' . urlencode($checked_filters)  ) ?>'
    Url= Url.replace(/ZZZZZ/g, value);
		//alert(Url)
  	document.location= Url
  }

	function RunBadgeProduct( Sku ) {
		var Url= '<?php echo url_for( '@run_badge_product?sku=' ) ?>'+Sku
		document.location= Url
	}


	function AddToCart( Sku, eventObject ) {
    var HRef= '<?php echo url_for('@cart_product_update?sku=ZZZZZ&product_quantity=XXXXX') ?>'
    HRef= HRef.replace( /ZZZZZ/g, encodeURIComponent(Sku) );
    HRef= HRef.replace( /XXXXX/g, document.getElementById("qty_in_cart_"+Sku).value );

    jQuery.getJSON(HRef,   {  },
    onAddedToCart,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );

  }

  function onAddedToCart(data) {
    var ErrorCode= data.ErrorCode
    var ErrorMessage= data.ErrorMessage
    var product_quantity= data.product_quantity
    var items_count= data.items_count
    var common_sum= data.common_sum
    if ( ErrorCode == 1 ) {
      alert( ErrorMessage )
      return;
    }
    document.getElementById("span_header_number_items").innerHTML= items_count
    document.getElementById("span_header_total_cost").innerHTML= '$' +AddDecimalsDigit(common_sum)

    jQuery('#div_cart_info').show();
    if ((items_count * 1) > 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' items  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    else if ((items_count * 1) == 1) {
        jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' item  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    if ((product_quantity * 1) > 1) {
        jQuery('#span_action').html(product_quantity + ' items added to your cart');
    }
    else if ((product_quantity * 1) == 1) {
        jQuery('#span_action').html(product_quantity + ' item added to your cart');
    }
  }


  function onFilterSubmit(checked_action) {
		document.getElementById("checked_action").value= checked_action
		// alert("checked_action::"+checked_action)
    var theFilterForm = document.getElementById("form_filter");
    theFilterForm.submit();
  }

  jQuery(function($) {
    SelectCategoryonChange('<?php echo $select_subcategory ?>')

		if ( '<?php echo $badge_added ?>' == '1' &&	'<?php echo $badge_sku ?>' !='' ) {
			ShowBadgeAdded('<?php echo $badge_sku ?>')
		}
  });

	function ShowBadgeAdded( $badge_sku ) {
		jQuery('#div_cart_info').show();
		jQuery('#span_cart_info').html('You added badge to your cart'  );

	}

  function SelectCategoryonChange(selected_select_subcategory) {
    var select_category= document.getElementById('select_category').value;
    if ( select_category=="" ) {
      ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
      return
    }
    var HRef= '<?php echo url_for('@main_getsubcategories?category=') ?>' + encodeURIComponent(select_category)+'/selected_subcategory/' + encodeURIComponent(selected_select_subcategory)
    // alert(HRef)
    jQuery.getJSON(HRef,
    {
    },
    FillSubcategories,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
    );
  }

  function FillSubcategories(data) {
    var ArrayLength= parseInt(data['length'])
    var selected_subcategory= data['selected_subcategory']
    ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
    var DataArray= data['data']
    for( i=0; i< ArrayLength; i++ ) {
      if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
        var Id= DataArray[i]['id']
        var Name= DataArray[i]['name']
        AddDDLBItem( 'select_subcategory', Id, Name ); //Add all options to input selection
      }
    }
    SetDDLBActiveItem( 'select_subcategory', selected_subcategory )
  }

  function FieldOnFocus(FieldName) {
    FieldValue= "<?php echo __("enter email address") ?>";
    var S = document.getElementById(FieldName).value;
    if ( Trim(S)==FieldValue ) {
      document.getElementById(FieldName).value= "";
    }
  }

  function FieldOnBlur(FieldName) {
    FieldValue= "<?php echo __("enter email address") ?>";
    var S = document.getElementById(FieldName).value;
    if ( Trim(S)=="" ) {
      document.getElementById(FieldName).value= FieldValue;
    }
  }

  function OnSelectoKeyUp(e) {
    if ( e.keyCode == 13 || e.keyCode == 9 ) {
      return true;
    }
    document.getElementById( "predict_filter_recruit_id" ).value= "";
  }

  function cart_window_remove()
  {
      jQuery('#div_cart_info').hide();
  }

  //-->
  </script>



<form action="<?php echo url_for('@'.$PageUrl.'?page=1&rows_in_pager='.$rows_in_pager  ) ?>" id="form_filter" method="POST">
  <?php include_partial( 'product_listings_header', array( 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,
      'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle, 'UsersCartArray'=>$UsersCartArray, 'sorting'=>$sorting,
      'page_type'=>'product_listings', 'checked_filters'=> $checked_filters ) )?>

<input type="hidden" id="return_page" name="return_page" value="view_cart" >
<input type="hidden" id="sku" name="sku" value="" >
<input type="hidden" id="product_quantity" name="product_quantity" value="" >
<input type="hidden" id="checked_filters" name="checked_filters"  value="<?php echo $checked_filters ?>" size="200">

<input type="hidden" id="checked_action" name="checked_action"  value="" >

<table width="93%" align="center" style="margin:0 auto;border: 0px dotted;" cellpadding="0" cellspacing="0"  valign="top" >
  <tr valign="top" >
  
    <td style="width:192px;" valign="top" rowspan="50">
      <?php include_partial( 'main/product_listing_filters_sidebar', array( 'sorting'=>$sorting, 'page'=> $page, 'rows_in_pager'=> $rows_in_pager, 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,
          'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id, 'input_search'=> $input_search, 'SelectedBrandsArray'=>$SelectedBrandsArray, 'SelectedSizesArray'=>$SelectedSizesArray, 'SelectedColorsArray'=>$SelectedColorsArray, 'SelectedGendersArray'=>$SelectedGendersArray, 'SelectedPricesArray'=>$SelectedPricesArray, 'SelectedSale_PricesArray'=>$SelectedSale_PricesArray, 'SelectedFinishsArray'=>$SelectedFinishsArray, 'SelectedHandsArray'=>$SelectedHandsArray, 'checked_filters'=> $checked_filters, 'extended_parameters'=>$extended_parameters, 'checked_action'=> $checked_action) ) ?>
    </td>
    <td align="<?php echo( $InventoryItemsCount>3?"center":"left") ?>" valign="top" >
      <table width="100%" align="right" cellpadding="0" cellspacing="0"  style="none;padding-left: -0px;" valign="top">
      <?php $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
        <tr id="tr_product_listings_<?php echo $Row ?>" align="left"  valign="top" >
      <?php $count = 0; ?>
        <?php for($Col=1; $Col<= $product_listings_columns; $Col++) : ?>
            <?php if ( $I < $rows_in_pager and !empty($InventoryItemsArray[$I]) ) : $count++; ?>
            <td>
          		<?php include_partial( 'main/product_listing_item', array( 'sorting'=>$sorting, 'Index'=>$I, 'Col'=> $Col, 'Row'=>$Row, 'Item'=>$InventoryItemsArray[$I], 'page'=> $page, 'rows_in_pager'=> $rows_in_pager, 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search, 'UsersCartArray'=>$UsersCartArray, 'checked_filters'=> $checked_filters ) ) ?>
            <?php else: ?>
             <!--&nbsp;-->
             </td>
            <?php endif; ?>
        <?php $I++; endfor; ?>
        <?php
            if ($count < 3) {
                for ($count; $count < 3; $count++ ) {
                    echo '<td><div class="product_div"></div></td>';
                }
            }
        ?>
        </tr>
      <?php endfor; ?>
      </table>
     </td>

  </tr>
</table>
</form>


  <?php $page_sorting= '-'; if ( !empty($sorting) ) $page_sorting= $sorting;
  include_partial( 'main/filters_and_paginator', array( 'select_category'=>$select_category, 'page'=>$page,'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,
    'page_type'=>'product_listings',  'select_brand_id'=> $select_brand_id,  'InventoryItemsPager'=>$InventoryItemsPager, 'input_search'=> $input_search,
      'HeaderTitle'=> $HeaderTitle, 'sorting'=>$sorting, 'page_sorting'=> $page_sorting, 'checked_filters'=> $checked_filters ) ) ?>


<div id="div_cart_info" style="display:none;">
	<img id="modal-window-close-button" onclick="javascript:cart_window_remove()" src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/close.png" />
    <?php
	if (empty($items_count)) {
		$items_count = Cart::getItemsCount();
	}
	if (empty($common_sum)) {
		$common_sum = Cart::getCommonSum(true);
	}
	if ($message_type == 'delete') { ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) deleted from your cart</p>
    <?php
    }
    else {
    ?>
        <p id="span_action"><?php echo $product_quantity ?> item(s) added to your cart</p>
    <?php } ?>
	<p id="span_cart_info">Cart: <?php echo $items_count ?> items   <?php echo $common_sum ?></p>
    <?php if (empty($hide_view_message)) { ?>
	    <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>
	    <p id="click-anywhere">Click anywhere on this page<br /> to continue shopping</p>
    <?php } ?>
	<a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>

