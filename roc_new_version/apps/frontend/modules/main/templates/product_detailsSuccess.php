<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<?php
if ( empty( $checked_filters) ) {
	$checked_filters= '';
}
//Util::deb($checked_filters, '+++$checked_filters::');

$IsSpecialRedToolBar = false;
if (!empty($_SERVER['HTTP_REFERER'])) {
  if (strpos($_SERVER['HTTP_REFERER'], 'main/specials')) {
    $IsSpecialRedToolBar = true;
  }
}

$Title = $Product->getTitle();
$PhoneOrder= $Product->getPhoneOrder();
// Util::deb( $PhoneOrder, '  $PhoneOrder::' );

$Sku = $Product->getSku();
$Gender = trim($Product->getGender());
$Brand = trim($Product->getBrand());
$BrandId = $Product->getBrandId();
$StdUnitPrice = $Product->getStdUnitPrice();
$SaleMethod = $Product->getSaleMethod();
if ($SaleMethod == "D" or $SaleMethod == "P") {
  $StdUnitPrice = $Product->getClearance_Or_SalePrice();
}

$Sizing = trim($Product->getSizingId());
$Documents = trim($Product->getDocuments());
$InventoryItemPaperWorks = InventoryItemPaperworkPeer::getInventoryItemPaperworks( '', false, $Product->getId() )   ;//trim($Product->getPaperwork());
$DescriptionLong = trim($Product->getDescriptionLong());
$DescriptionShort = trim($Product->getDescriptionShort());

// $InventoryCategoryCode = $Product->getInventoryCategory();
// $lInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategory($InventoryCategoryCode);
$lInventoryCategory= null;
$lInventoryItemInventoryCategory= InventoryItemInventoryCategoryPeer::getInventoryItemInventoryCategorysByItemInventoryId( '', false, $Product->getId(), '', true );
if ( !empty($lInventoryItemInventoryCategory) ) {
  $lInventoryCategory= $lInventoryItemInventoryCategory->getInventoryCategory();
}
// Util::deb( $lInventoryCategory, ' $lInventoryCategory::' );
//if ( $HasRootItem and !empty($RootItemObject) ) {
if ($HasRootItem) {
  //Util::deb( $Product, ' -1 $Product::' );
  //$Product= $RootItemObject;
  //Util::deb( ' CHANGE::' );
  $Title = $RootItemObject->getTitle();
  $Sku = $RootItemObject->getSku();
  $Gender = trim($RootItemObject->getGender());
  $Brand = trim($RootItemObject->getBrand());
  $BrandId = $RootItemObject->getBrandId();
  $StdUnitPrice = $RootItemObject->getStdUnitPrice();
  $SaleMethod = $RootItemObject->getSaleMethod();
  if ($SaleMethod == "D" or $SaleMethod == "P") {
    $StdUnitPrice = $RootItemObject->getClearance_Or_SalePrice();
  }
  $Sizing = trim($RootItemObject->getSizingId());
  $Documents = trim($RootItemObject->getDocuments());
  $DescriptionLong = trim($RootItemObject->getDescriptionLong());
  $DescriptionShort = trim($Product->getDescriptionShort());
}

$Title = preg_replace("/\(R\)/", '&reg;', $Title);
$Title = preg_replace("/\(TM\)/", '&trade;', $Title);

$final_description = '';
if ($DescriptionLong != '') {
  $final_description = $DescriptionLong;
} else {
  $final_description = 'no description available';
}
if ($final_description != '') {
  $final_description = preg_replace("/\(R\)/", '&reg;', $final_description);
  $final_description = preg_replace("/\(TM\)/", '&trade;', $final_description);
  $final_description = preg_replace("/\*/", '&bull;', $final_description);
  $final_description = nl2br($final_description);
}


$ThumbnailImageFile = AppUtils::getThumbnailImageBySku($Sku, false);
if (empty($ThumbnailImageFile)) {
  $ThumbnailImageFile = AppUtils::getResizedImageBySku($Sku, false, false, true);
}
$ImageHeight = 298;
$ImageWidth = 298;
$HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());
//        echo '--0 $HostForImage::'.print_r($HostForImage,true).'<br>';
?>
<script type="text/javascript" language="JavaScript">
  <!--
  jQuery('body').click(function(){
    jQuery('#div_cart_info').hide();
  });
  function ShowMainImage( ImageFullUrl, image_path, IsToShowSku ) {
    var Dat= new Date()
    var NewUrl= ImageFullUrl+"?t="+encodeURIComponent(Dat)
    //alert( "NewUrl::" + NewUrl )
    document.getElementById("img_main_image").src= NewUrl
    if ( IsToShowSku ) {
      var temp = image_path.split('/');
      if ( temp.length > 1 ) {
        var sku  = temp[ temp.length-1 ].split('.');
        jQuery('#sku').html('Item#&nbsp;' + sku[0]);
      }
    }
  }

  function AddToCart( Sku ) {
    var sel_item = '';
    if(jQuery("#select_size_").length > 0) {
      sel_item = jQuery('#select_size_ option:selected').val();
      if (sel_item != '') {
        Sku = sel_item;
      }
    }
    else if (jQuery("#select_color").length > 0) {
      sel_item = jQuery('#select_color option:selected').val();
      if (sel_item != '') {
        Sku = sel_item;
      }
    }
    else if (jQuery("#select_option").length > 0) {
      sel_item = jQuery('#select_option option:selected').val();
      if (sel_item != '') {
        Sku = sel_item;
      }
    }
    else if (jQuery("#select_finish").length > 0) {
      sel_item = jQuery('#select_finish option:selected').val();
      if (sel_item != '') {
        Sku = sel_item;
      }
    }
    else if (jQuery("#select_hand").length > 0) {
      sel_item = jQuery('#select_hand option:selected').val();
      if (sel_item != '') {
        Sku = sel_item;
      }
    }
    var CurrentStdUnitPrice= document.getElementById('span_CurrentStdUnitPrice').value
    var price_type= '-'
    if ( CurrentStdUnitPrice> 0 ) {
      price_type= 'selected_price'
    }
    var HRef= '<?php echo url_for('@cart_product_update?sku=ZZZZZ&product_quantity=XXXXX&price_type=VVVVV&CurrentStdUnitPrice=WWWWW') ?>'

    HRef= HRef.replace( /ZZZZZ/g, encodeURIComponent(Sku) );
    HRef= HRef.replace( /XXXXX/g, encodeURIComponent(document.getElementById( "product_quantity" ).value) );
    HRef= HRef.replace( /WWWWW/g, encodeURIComponent(CurrentStdUnitPrice) );
    HRef= HRef.replace( /VVVVV/g, encodeURIComponent(price_type) );
// alert(HRef)
    jQuery.getJSON(HRef,   {  },
    onAddedToCart,
    function(x,y,z) {   //Some sort of error
      alert(x.responseText);
    }
  );

  }

  function onAddedToCart(data) {
    //	alert( "onAddedToCart::"+var_dump(data) )
    var ErrorCode= data.ErrorCode
    var ErrorMessage= data.ErrorMessage
    var product_quantity= data.product_quantity
    var items_count= data.items_count
    var common_sum= data.common_sum

    if ( ErrorCode == 1 ) {
      alert( ErrorMessage )
      return;
    }

    document.getElementById("span_header_number_items").innerHTML= items_count
    document.getElementById("span_header_total_cost").innerHTML= '$' +AddDecimalsDigit(common_sum)
    jQuery('#div_cart_info').show();
    if ((items_count * 1) > 1) {
      jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' items  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    else if ((items_count * 1) == 1) {
      jQuery('#span_cart_info').html('Cart:&nbsp;&nbsp;&nbsp;' + items_count + ' item  &nbsp;&nbsp;' + '$' +AddDecimalsDigit(common_sum) );
    }
    if ((product_quantity * 1) > 1) {
      jQuery('#span_action').html(product_quantity + ' items added to your cart');
    }
    else if ((product_quantity * 1) == 1) {
      jQuery('#span_action').html(product_quantity + ' item added to your cart');
    }
  }
    function SelectInventotyItemBySku(Sku, type) {
      var sizes  = '';
      var colors = '';
      var url = "/oherron.com/main/my_test";
      var json_data = {
        'sku'  : Sku,
        'type' : type
      };
      jQuery.post(url, json_data, function(data) {
        data = jQuery.parseJSON(data);
        sizes  = data.sizes;
        colors = data.colors;
        if (sizes != '' && colors != '') {
          if (type == 'color' && Sku != '') {
            jQuery('#select_size_').html(sizes);
            var img_product = '<a href="' + data.img + '">';
            img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + data.img + '" style="cursor:pointer;" /><br />';
            img_product    += '</a>';

            jQuery('#product_photo_div').html(img_product);
            jQuery('#sku').html('Item#&nbsp;' + Sku);
          }
          else if (type == 'color' && Sku == '') {
            jQuery('#select_color').html(colors);
          }
          if (type == 'size' && Sku != '') {
            jQuery('#select_color').html(colors);
            var img_product = '<a href="' + data.img + '">';
            img_product    += '<img height="298" width="298" alt=""  id="img_main_image" src="' + data.img + '" style="cursor:pointer;" /><br />';
            img_product    += '</a>';
            jQuery('#product_photo_div').html(img_product);
            jQuery('#sku').html('Item#&nbsp;' + Sku);
          }
          else if (type == 'size' && Sku == '') {
            jQuery('#select_size_').html(sizes);
          }
        }
      });
    }

    function onSubmit() {
      var theForm = document.getElementById("form_product_listings");
      theForm.submit();
    }

    jQuery(function($) {
      SelectCategoryonChange('<?php echo $select_subcategory ?>')
    });

    function SelectCategoryonChange(selected_select_subcategory) {
      // alert( " SelectCategoryonChange  selected_category_id selected_category_id::" + selected_select_category )
      var select_category= document.getElementById('select_category').value;
      if ( select_category=="" ) {
        ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
        return
      }
      var HRef= '<?php echo url_for('@main_getsubcategories?category=') ?>' + encodeURIComponent(select_category)+'/selected_subcategory/' + encodeURIComponent(selected_select_subcategory)
      // alert(HRef)
      jQuery.getJSON(HRef,
      {
      },
      FillSubcategories,
      function(x,y,z) {   //Some sort of error
        alert(x.responseText);
      }
    );
    }

    function FillSubcategories(data) {
      // alert( "FillSubcategories(data)::"+var_dump(data) )
      var ArrayLength= parseInt(data['length'])
      var selected_subcategory= data['selected_subcategory']
      ClearDDLBItems( 'select_subcategory', false ) //clear all items but first("-Select-")
      var DataArray= data['data']
      for( i=0; i< ArrayLength; i++ ) {
        if ( DataArray[i]['id'] && DataArray[i]['name'] ) {
          var Id= DataArray[i]['id']
          var Name= DataArray[i]['name']
          AddDDLBItem( 'select_subcategory', Id, Name ); //Add all options to input selection
        }
      }
      // alert("selected_subcategory::"+selected_subcategory)
      SetDDLBActiveItem( 'select_subcategory', selected_subcategory )
    }
    /*
function FieldOnFocus(FieldName) {
FieldValue= "<?php echo __("enter email address") ?>";
var S = document.getElementById(FieldName).value;
if ( Trim(S)==FieldValue ) {
document.getElementById(FieldName).value= "";
}
}

function FieldOnBlur(FieldName) {
FieldValue= "<?php echo __("enter email address") ?>";
var S = document.getElementById(FieldName).value;
if ( Trim(S)=="" ) {
document.getElementById(FieldName).value= FieldValue;
}
}

function OnSelectoKeyUp(e) {
if ( e.keyCode == 13 || e.keyCode == 9 ) {
return true;
}
document.getElementById( "predict_filter_recruit_id" ).value= "";
}
     */
    //-->

    function cart_window_remove()
    {
      jQuery('#div_cart_info').hide();
    }
</script>

<form action="<?php echo url_for('@' . (!$IsSpecialRedToolBar ? 'product_listings' : 'specials' ) . '?page=1&rows_in_pager=' . $rows_in_pager  . ( !empty($checked_filters) ? '&checked_filters=' . $checked_filters : "" ) ) ?>" id="form_product_listings" method="POST">
<?php include_partial('main/search_criteria', array('lInventoryCategory' => $lInventoryCategory, 'select_category' => $select_category, 'sorting' => $sorting, 'page' => $page, 'rows_in_pager' => $rows_in_pager, 'select_subcategory' => $select_subcategory, 'select_brand_id' => $select_brand_id, 'input_search' => $input_search, 'HeaderTitle' => $HeaderTitle, 'RootItem' => $RootItem, 'HasRootItem' => $HasRootItem, 'RootItemObject' => $RootItemObject, 'Matrix' => $Matrix, 'page_type' => ($IsSpecialRedToolBar ? "specials" : "") , 'checked_filters'=>$checked_filters) ) ?>
</form>

<!-- website message alert -->
<?php
if (trim(sfConfig::get('app_website_message_products_alert'))!='') {
?>
<p class="website_alert_message"><?php echo sfConfig::get('app_website_message_products_alert') ?></p>
<?php
}
?>

<!-- start content-->
<div id="content_div">

  <div id="content_right_div">

<?php if (!empty($RecentlyViewedList)) : ?>
      <?php include_partial('main/product_recently_viewed', array('Product' => $Product, 'RecentlyViewedList' => $RecentlyViewedList, 'HostForImage' => $HostForImage, 'RootItem' => $RootItem, 'HasRootItem' => $HasRootItem, 'RootItemObject' => $RootItemObject, 'Matrix' => $Matrix)) ?>
    <?php endif; ?>


  </div>
  <div id="content_mid_div" style="position:relative; left:5px;">
    <div id="product_info_div">
      <p style="font-size:22px; margin:-2px 0 7px 0;"><?php echo $Title ?></p>


<?php if (!empty($Brand)) : ?>
        <span><?php echo link_to($Brand, '@product_listings?page=1&select_brand_id=' . $BrandId . ( !empty($checked_filters) ? '&checked_filters=' . $checked_filters : "" ) ); ?></span
      <?php endif; ?>

      <?php
      $papers_content = '<table cellpadding="0" cellspacing="0" style="float:right;">';
      if (!empty($InventoryItemPaperWorks)) {
        $papers_content .= '<tr><td colspan="2"><br><br>Required Paperwork:<br></td></tr>';
        foreach ( $InventoryItemPaperWorks as $lInventoryItemPaperWork ) {
					$lInventoryItemPaperWork= $lInventoryItemPaperWork->getPaperWork();
          if (!empty($lInventoryItemPaperWork)) {
            $papers_content .= '<tr><td>' . link_to($lInventoryItemPaperWork->getTitle(), "@product_paperwork_view?code=" . $lInventoryItemPaperWork->getCode()) . '<br></td></tr>';
          }
        }
        $papers_content .= '</table>';
        echo $papers_content;
      }
      ?>


<?php include_partial('main/show_features', array('Product' => $Product, 'HostForImage' => $HostForImage, 'RootItem' => $RootItem, 'HasRootItem' => $HasRootItem, 'RootItemObject' => $RootItemObject, 'Matrix' => $Matrix, 'Sku' => $sku, 'Type' => $type, 'StdUnitPrice'=> $StdUnitPrice)) ?>


<?php if (!empty($StdUnitPrice)) : ?>
        <p style="margin-bottom:7px !important;"><span id="span_CurrentStdUnitPrice"><?php echo Util::getDigitMoney($StdUnitPrice, 'Money') ?></span></p>
      <?php endif; ?>

      <!-- add to cart -->
      <div id="add_to_cart_div">
        <?php if (!$PhoneOrder) : ?>
        <span>Qty:</span>
        <input type="text" id="product_quantity" name="product_quantity" value="<?php echo Cart::getValueBySku($Sku, 1, $UsersCartArray) ?>" maxlength="2" size="2"/>
        <div id="add_to_cart_btn_div">
          <input id="add_to_cart_btn" type="image" value="" src="<?php echo AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) ?>images/add-to-cart.jpg" onclick="javascript:AddToCart('<?php echo $Sku ?>')" />
        </div>
        <?php else: ?>
          <img style="position:absolute; left:7px; top:5px;"  class="cart_btn"  src="<?php echo $HostForImage ?>images/phone-orders.png" type="image"  />

        <?php endif; ?>
      </div>
      <small style="color:#949494"><span id="sku">Item#&nbsp;
<?php echo $Sku ?></span></small>
      <div style="clear:both;"></div>

    </div><!-- close product info div -->
  </div>


  <div id="content_left_div">
    <div id="product_photo_div">

<?php
$ThumbnailImageFullUrl = '';

//Util::deb( $ThumbnailImageFile, '-1 $ThumbnailImageFile::' );
// echo '$ThumbnailImageFile::'.print_r($ThumbnailImageFile,true).'<br>';
if (!empty($ThumbnailImageFile) and !is_array($ThumbnailImageFile)) :
  $ThumbnailImageUrl = '/uploads' . $ThumbnailImageFile;
  //Util::deb( $ThumbnailImageUrl, '-99 $ThumbnailImageUrl::' );
  $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
  //echo '-2 $ThumbnailImageFullUrl::'.print_r($ThumbnailImageFullUrl,true).'<br>';
  // Util::deb( $ThumbnailImageFullUrl, '-2 $ThumbnailImageFullUrl::' );
  ?>


        <a href="<?php echo $ThumbnailImageFullUrl ?>">
          <img width="298" height="298" src="<?php echo $ThumbnailImageFullUrl ?>" alt="" id="img_main_image" style="cursor:pointer;" >2222222
        </a>
      <?php endif; ?>

      <?php
      if ($ThumbnailImageFile and is_array($ThumbnailImageFile) /* and !empty($ThumbnailImageFile['ImageFileName']) */) :
        //Util::deb(  ' -33::' );
        //Util::deb( $ThumbnailImageFile, '-2299 $ThumbnailImageFile::' );
        // echo '-3 $ThumbnailImageFile::'.print_r($ThumbnailImageFile,true).'<br>';
        if (!empty($ThumbnailImageFile['Width']) and !!empty($ThumbnailImageFile['Height'])) {
          $ImageWidth = $ThumbnailImageFile['Width'];
          $ImageHeight = $ThumbnailImageFile['Height'];
        }
        $ThumbnailImageUrl = 'uploads' . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName_encoded'];
        //Util::deb( $ThumbnailImageUrl, '$ThumbnailImageUrl::' );
        $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $ThumbnailImageUrl;
        $ThumbnailImageFullFilePath = sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
        if (empty($ThumbnailImageFile['ImageFileName']) or (!file_exists(urldecode($ThumbnailImageFullFilePath)) and !is_dir(urldecode($ThumbnailImageFullFilePath)) )) {
          $ThumbnailImageFullUrl = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
        }
        $ThumbnailImageFullUrl = str_replace("+", ' ', $ThumbnailImageFullUrl);
        //Util::deb( $ThumbnailImageFullUrl, '$ThumbnailImageFullUrl::' );
        ?>


        <a href="<?php echo $ThumbnailImageFullUrl ?>" >
          <img height="<?php echo $ImageWidth ?>" width="<?php echo $ImageHeight ?>" alt=""  id="img_main_image" src="<?php echo $ThumbnailImageFullUrl ?>" style="cursor:pointer;" /><br />
        </a>
      <?php endif; ?>

    </div>

    <?php if (count($MoreViewInventoryItems) > 0) : ?>
      <?php include_partial('main/product_more_views', array('Sku' => $Sku, 'Product' => $Product, 'MoreViewInventoryItems' => $MoreViewInventoryItems, 'HostForImage' => $HostForImage, 'RootItem' => $RootItem, 'HasRootItem' => $HasRootItem, 'RootItemObject' => $RootItemObject, 'Matrix' => $Matrix)) ?>
    <?php endif ?>


  </div> <!-- closes content_left_div> -->



  <div id="tab_div">
    <div id="description_tab">
      <p class="tab_text">Description
      </p>
    </div>

    <div class="tab_spacer_div">
    </div>
    <div id="reviews_tab">
      <ul>
        <li><a class="tab_text" href="#">Reviews 7</a></li>
        <li>&nbsp;&nbsp;<span style="font-weight:bold;">|</span>&nbsp;&nbsp;</li>
        <li><a class="tab_text2" href="#">Write a Review</a></li>
      </ul>
    </div>
    <div class="tab_spacer_div">
    </div>
    <div id="product_tab">
      <ul>

        <li><a class="tab_text" href="#">Product Q&amp;A</a></li>
        <li>&nbsp;&nbsp;<span style="font-weight:bold;">|</span>&nbsp;&nbsp;</li>
        <li><a class="tab_text2" href="#">Ask a Question</a></li>
      </ul>
    </div>
    <div id="tab_spacer_wide_div">
    </div>

    <?php //if ( !empty($DescriptionLong) ) : ?>
    <div id="description_content_div">
      <p><?php echo $final_description ?></p>



      <?php if (!empty($Documents)) : ?>
        <table>
          <?php $DocumentsArray = preg_split('/,/', $Documents); ?>
          <tr>
            <td>
              Product Documents:
            </td>
          </tr>
          <?php
          $I = 1;
          foreach ($DocumentsArray as $Code) :
            $Document = DocumentPeer::retrieveByPK($Code);

            if (!empty($Document)) :
              ?>
              <tr>
                <td>
              <?php echo link_to($Document->getTitle() /* "Link Document # ".$I */, "@product_document_view?code=" . $Code) ?>
                </td>
              </tr>
            <?php endif; ?>

          <?php $I++;
        endforeach; ?>

        </table>
<?php endif; ?>




    </div>
<?php // endif;  ?>

  </div><!--close tab_div-->
  <div style="clear: both;"></div>

</div> <!-- closes content div -->

<!-- end content-->

<!--
<div id="div_cart_info" style="display:none;" >
  <img id="modal-window-close-button" onclick="javascript:HideCartInfo()" src="<?php echo AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) ?>images/close.png " />
  <p id="span_action"></p>
  <p id="span_cart_info"></p>

  <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>

  <p id="click-anywhere">Click anywhere on this page<br> to continue shopping</p>
  <a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>
-->
<div id="div_cart_info" style="display:none;">
  <img id="modal-window-close-button" onclick="javascript:cart_window_remove()" src="<?php echo AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) ?>images/close.png" />
  <?php
  if (empty($items_count)) {
    $items_count = Cart::getItemsCount();
  }
  if (empty($common_sum)) {
    $common_sum = Cart::getCommonSum(true);
  }
  if ($message_type == 'delete') {
    ?>
    <p id="span_action"><?php echo $product_quantity ?> item(s) deleted from your cart</p>
    <?php
  } else {
    ?>
    <p id="span_action"><?php echo $product_quantity ?> item(s) added to your cart</p>
<?php } ?>
  <p id="span_cart_info">Cart: <?php echo $items_count ?> items   <?php echo $common_sum ?></p>
  <?php if (empty($hide_view_message)) { ?>
    <p id="view-cart-link"><a href="<?php echo url_for("@view_cart") ?>">View Cart</a></p>
    <p id="click-anywhere">Click anywhere on this page<br /> to continue shopping</p>
<?php } ?>
  <a class="check-out-now-button" href="<?php echo url_for('@check_out_customer') ?>">Check Out Now ►</a>
</div>

