<?php /*
$MostPopularInventoryCategoriesList= sfConfig::get('app_application_MostPopularInventoryCategories');
$inventory_category_listings_columns= 3;
$InventoryCategoriesCount= 12 ;
$RowsCount= (int) ($InventoryCategoriesCount / $inventory_category_listings_columns);
$RowsCount= 4; */
?>
    <div id="left_column_products_div">
			<style type="text/css">
/*sz was 10 now 31 */
				#category-buttons {margin:38px 0 0 13px; padding:0; list-style:none;}
				#category-buttons li {margin:0; padding:3px 0 3px 0; width:33.3%; float:left;}
				#category-buttons a {display:block; width:85px; height:85px; text-decoration:none;
				background:url("<?php Util::getServerHost() ?>images/category-buttons-sprite.png") no-repeat;}
				#category-buttons li a span {display:none !important;}
				a#Vehicle {background-position:0 0;}
				a#Vehicle:hover {background-position:0 -89px;}
				a#Road-Safety {background-position:-96px 0;}
				a#Road-Safety:hover {background-position:-96px -89px;}
				a#EMS {background-position:-193px 0;}
				a#EMS:hover {background-position:-193px -89px;}
				a#Outerwear {background-position:-289px 0;}
				a#Outerwear:hover {background-position:-289px -89px;}
				a#Footwear {background-position:-386px 0;}
				a#Footwear:hover {background-position:-386px -89px;}
				a#Clothing {background-position:-482px 0;}
				a#Clothing:hover {background-position:-482px -89px;}
				a#Body-Armor {background-position:-579px 0;}
				a#Body-Armor:hover {background-position:-579px -89px;}
				a#Flashlights {background-position:-675px 0;}
				a#Flashlights:hover {background-position:-675px -89px;}
				a#Restraints {background-position:-772px 0;}
				a#Restraints:hover {background-position:-772px -89px;}
				a#Forensics {background-position:-868px 0;}
				a#Forensics:hover {background-position:-868px -89px;}
				a#Knives {background-position:-965px 0;}
				a#Knives:hover {background-position:-965px -89px;}
				a#Tactical {background-position:-1061px 0;}
				a#Tactical:hover {background-position:-1061px -89px;}
				#download_catalog_btn a {background:url("<?php Util::getServerHost() ?>images/download-catalog.jpg") no-repeat 0 0; display:block;
				 width:278px; height:62px; position:relative; left:0px;}
				#download_catalog_btn a:hover {background-position:0 -62px;}

/* sz 1-17 added product button style */
        #products_page_btn {position:relative;left:11px;top:10px;}
				#products_page_btn a {background:url("images/roc-products-page-btn.png") no-repeat 0 0; display:block;
				 width:275px; height:75px; position:relative; left:0px;}
				#products_page_btn a:hover {background-position:0 -75px;}
			</style>



      <div id="products_page_btn" style="width:278px; height:62px;" >
        <a href="/main/products">
          <span style="display:none;">Download Our Catalog</span>
        </a>
      </div>

      <span style="position:relative;left:11px;top:34px;font-weight:bold;font-size:15px;">OR USE THESE QUICKLINKS</span>

			<ul id="category-buttons">
				<li><a id="Vehicle" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Vehicle"><span>Vehicle</span></a></li>
				<li><a id="Road-Safety" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Road+Safety"><span>Road Safety</span></a></li>
				<li><a id="Forensics" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Forensics"><span>Forensics</span></a></li>

				<li><a id="Outerwear" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Outerwear"><span>Outerwear</span></a></li>
				<li><a id="Footwear" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Footwear"><span>Footwear</span></a></li>
				<li><a id="Tactical" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Tactical"><span>Tactical</span></a></li>


				<li><a id="EMS" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=EMT"><span>EMS</span></a></li>
				<li><a id="Body-Armor" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Body+Armor"><span>Body Armor</span></a></li>
				<li><a id="Clothing" href="main/product_listings/page/1/sorting/-/rows_in_pager?select_category=Clothing"><span>Clothing</span></a></li>
				
			</ul>

       <?php /* $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
        <!-- row <?php echo $Row; ?> -->
        <ul class="product_buttons_ul">
              
        <?php for($Col=1; $Col<= $inventory_category_listings_columns; $Col++) :
          if ( empty($MostPopularInventoryCategoriesList[$I]) ) { $I++; continue; }?>
            <?php $InventoryCategoryImgSrc= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . AppUtils::getInventoryCategoryByProductLine( $MostPopularInventoryCategoriesList[$I],false);
            ?>
             <li class="product_btn" style="background-image:url('<?php echo $InventoryCategoryImgSrc ?>');" >
               <a href="<?php echo url_for( '@product_listings?page=1&select_category=' . urlencode( $MostPopularInventoryCategoriesList[$I] ) ) ?>" style="display:block">
                 <img src="<?php echo $InventoryCategoryImgSrc ?>" width="85" height="85" alt="<?php echo $MostPopularInventoryCategoriesList[$I] ?>" />
               </a>
             </li>                     
        <?php $I++; endfor; ?>

        </ul>
      <?php endfor; */ ?>

      <div id="download_catalog_btn" style="width:278px; height:62px;" >
        <a target="_blank" href="<?php Util::getServerHost() ?>uploads/Catalog/2011_Catalog.pdf" title="2011_Catalog.pdf / 21.3 Mb" style="display:block">
          <span style="display:none;">Download Our Catalog</span>
        </a>
      </div>

<?php 
$product_support_video_count= (int)sfConfig::get('app_application_product_support_video_count' );
$product_support_videos_text= sfConfig::get('app_application_product_support_videos_text' );
$YoutubeVideosList= YoutubeVideoPeer::getYoutubeVideos( '', false, 'ORDERING', $product_support_video_count  );

?>      
<!-- product support videos text --> 
      <div id="product-support-videos" style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/product-support-gradient.jpg');" >
          <h2><?php echo $product_support_videos_text ?></h2>

      </div>
    
      <?php for ( $I= 0; $I< 5; $I++ ) :
      if ( empty($YoutubeVideosList[$I]) or !is_object($YoutubeVideosList[$I]) ) continue;
      $Url= $YoutubeVideosList[$I]->getUrl();
        ?>
<!-- product support video <?php echo $I+1 ?> --> 
      <div class="video_1">
        <iframe width="276" height="217" src="<?php echo $Url ?>" frameborder="0" allowfullscreen>
        </iframe>
      </div>
      <?php endfor;  ?>

      
<!-- click here for more videos text --> 
      <div id="click-here-for-more-videos" style="background-image:url('<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/click-here-gradient.jpg');" >
        <a href="<?php Util::getServerHost() ?>main/support" style="display:block">
          <span>Click here for more videos...</span>
        </a>
      </div>

    </div><!-- Close left_column_products_div -->

  <div style="clear:both;"></div>