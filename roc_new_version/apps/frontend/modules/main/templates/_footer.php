
  <div id="footer_div">
    <div class="page_bottom">
      <ul style="width:315px;margin-top:15px;font-weight:bold;text-align:center;">
        <li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/shipping_info">Shipping Info&nbsp;&bull;&nbsp;</a></li>
        <li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/returns">Returns&nbsp;&bull;&nbsp;</a></li>
        <!--li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/product_documents">Product Documents&nbsp;&bull;&nbsp;</a></li-->
        <!--li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/press">Press&nbsp;&bull;&nbsp;</a></li-->
        <li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/privacy_policy">Privacy Policy&nbsp;&bull;&nbsp;</a></li>
        <li><a href="<?php Util::getServerHost(sfContext::getInstance()->getConfiguration()) ?>main/term_of_use">Terms of Use</a></li>
      </ul>
      <br />
      <p style="font-size:12px;margin:20px 0px 0px;padding:0px;font-weight:bold;">Accepted payment methods for online orders</p>
      <ul style="position:relative; margin:7px auto 0;">
        <li><img src="http://www.oherron.com/images/american-express-straight-32px.png" width="51" height="32" alt="American Express" /></li>
        <li style="margin-left:10px;"><img src="http://www.oherron.com/images/discover-straight-32px.png" width="51" height="32" alt="Discover" /></li>
        <li style="margin-left:10px;"><img src="http://www.oherron.com/images/mastercard-straight-32px.png" width="51" height="32" alt="Master Card" /></li>
        <li style="margin-left:10px;"><img src="http://www.oherron.com/images/paypal-straight-32px.png" width="51" height="32" alt="Paypal" /></li>
        <li style="margin-left:10px;"><img src="http://www.oherron.com/images/visa-straight-32px.png" width="51" height="32" alt="Paypal" /></li>
      </ul>
      <div style="clear:both;"></div>
      <div style="margin:20px auto;width:234px;">
        <script type="text/javascript" data-pp-pubid="880e4984aa" data-pp-placementtype="234x60"> (function (d, t) { "use strict"; var s = d.getElementsByTagName(t)[0], n = d.createElement(t); n.src = "//paypal.adtag.where.com/merchant.js";
        s.parentNode.insertBefore(n, s);
        }(document, "script"));
        </script>
      </div> 
      <p>&copy;
      <?php $curr_year = date('Y'); ?>
        2011-<?php echo $curr_year ?>
        Ray O'Herron Co., Inc. All Rights Reserved.<br />
      </p>
      <a id="wgc" href="http://www.magimpact.com" title="who are we?"><img src="<?php echo AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) ?>images/powered-by.gif" width="148" height="18" alt="Powered by Magnetic Impact" />
      </a>
    </div>
	</div> <!-- hey -->
