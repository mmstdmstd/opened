<div id="policy_div" >
  <h2>Testing & Evaluation (T&amp;E)&nbsp;&mdash;&nbsp;Taser</h2>
    <p>Thank you for your interest in testing and evaluating our selection of ECW products.<br />Please fill out the form below and we will contact you shortly.</p>
    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Contact<br />Information<br />
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="width:342px; float:right;position:relative;">
        <label class="te-form-label">Email:&nbsp;
        <span style="color:#ff0000;">*</span></label>
        <input class="te-form-input" type="text" tabindex="5"  />
        <span class="te-error2">&lt;&nbsp;required field</span>
          
        <label class="te-form-label">Phone:&nbsp;<span style="color:#ff0000;">*</span></label>
        <input class="te-form-input" type="text" tabindex="6"  />
        <span class="te-error2">&lt;&nbsp;required field</span>
        
        <label class="te-form-label">Address:&nbsp;<span style="color:#ff0000;">*</span></label>
        <input class="te-form-input" type="text" tabindex="7"  />
        <span class="te-error2">&lt;&nbsp;required field</span>

        <label class="te-form-label">City:&nbsp;<span style="color:#ff0000;">*</span></label>
        <input class="te-form-input" type="text" tabindex="8"  />
        <span class="te-error2">&lt;&nbsp;required field</span>

        <label class="te-form-label4">State:&nbsp;<span style="color:#ff0000;">*</span></label>
        <label class="te-form-label5">Zip Code:&nbsp;<span style="color:#ff0000;">*</span></label>


<select class="te-form-select" name="te-state" size="1" tabindex="5" style="width: 50px">
            <option  value="NONE" selected="selected">-Select-</option>
            <option  value="AK">AK</option>
          	<option  value="AL">AL</option>
          	<option  value="AR">AR</option>
          	<option  value="AZ">AZ</option>

          	<option  value="CA">CA</option>
          	<option  value="CO">CO</option>
          	<option  value="CT">CT</option>
          	<option  value="DC">DC</option>
          	<option  value="DE">DE</option>
          	<option  value="FL">FL</option>

          	<option  value="GA">GA</option>
          	<option  value="HI">HI</option>
          	<option  value="IA">IA</option>
          	<option  value="ID">ID</option>
          	<option  value="IL">IL</option>
          	<option  value="IN">IN</option>

          	<option  value="KS">KS</option>
          	<option  value="KY">KY</option>
          	<option  value="LA">LA</option>
          	<option  value="MA">MA</option>
          	<option  value="MD">MD</option>
          	<option  value="ME">ME</option>

          	<option  value="MI">MI</option>
          	<option  value="MN">MN</option>
          	<option  value="MO">MO</option>
          	<option  value="MS">MS</option>
          	<option  value="MT">MT</option>
          	<option  value="NC">NC</option>

          	<option  value="ND">ND</option>
          	<option  value="NE">NE</option>
          	<option  value="NH">NH</option>
          	<option  value="NJ">NJ</option>
          	<option  value="NM">NM</option>
          	<option  value="NV">NV</option>

          	<option  value="NY">NY</option>
          	<option  value="OH">OH</option>
          	<option  value="OK">OK</option>
          	<option  value="OR">OR</option>
          	<option  value="PA">PA</option>
          	<option  value="RI">RI</option>

          	<option  value="SC">SC</option>
          	<option  value="SD">SD</option>
          	<option  value="TN">TN</option>
          	<option  value="TX">TX</option>
          	<option  value="UT">UT</option>
          	<option  value="VA">VA</option>

          	<option  value="VT">VT</option>
          	<option  value="WA">WA</option>
          	<option  value="WI">WI</option>
          	<option  value="WV">WV</option>
          	<option  value="WY">WY</option>
        </select>
        <span class="te-error2">&lt;&nbsp;required field</span>
        <input id="te-form-input2" type="text" tabindex="10"  />
        <span class="te-error3">&lt;&nbsp;required field</span>
      </div>

      <div style="width:351px; float:right;">
          <label class="te-form-label">First Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <input class="te-form-input" type="text" tabindex="1" />
          <span class="te-error">&lt;&nbsp;required field</span>

          <label class="te-form-label">Last Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <input class="te-form-input" type="text" tabindex="2"  />
          <span class="te-error">&lt;&nbsp;required field</span>

          <label class="te-form-label">Department Name:&nbsp;<span style="color:#ff0000;">*</span></label>
          <input class="te-form-input" type="text" tabindex="3"  />
          <span class="te-error">&lt;&nbsp;required field</span>

          <label class="te-form-label">Contact Person for Tasers in Department:&nbsp;<span style="color:#ff0000;">*</span></label>
          <input class="te-form-input" type="text" tabindex="4"  />
          <span class="te-error">&lt;&nbsp;required field</span>
      </div>
      <div style="clear:both;"></div>
    </div>


    <div style="margin:30px auto 0; width:838px; background-color:#e8e8ea; position:relative; padding:23px;">
      <div style="float:left;font-size:16px;font-weight:bold;">Evaluation<br />Information<br />
        <span style="color:#ff0000;font-size:10px;">*&nbsp;</span>
        <span style="font-size:10px;">Required field</span>
      </div>
      <div style="float:left;">
        <label id=te-form-label3>What make/model of ECWs are you interested in?&nbsp;<span style="color:#ff0000;">*</span>
        </label>
        <input id="te-form-input3" type="text" tabindex="11"  />
        <span class="te-error4">&lt;&nbsp;required field</span>

      </div>
      <div style="clear:both;"></div>

      <div style="width:342px; float:right;position:relative;">
        <label class="te-form-label">Is your agency considering video camera systems?:&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="13" >
          <option value="option1">-Select-</option>
          <option value="option2">Yes</option>
          <option value="option2">No</option>
        </select>
        <span class="te-error6">&lt;&nbsp;required field</span>

        <label class="te-form-label">When do you plan to acquire your camera system?&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="15">
          <option value="option1">-Select-</option>
          <option value="option2">1-3 months</option>
          <option value="option2">3-6 months</option>
          <option value="option2">6 or more months</option>
          <option value="option2">Not yet determined</option>
          <option value="option2">Not applicable</option>
        </select>
        <span class="te-error6">&lt;&nbsp;required field</span>

        <label class="te-form-label">Do you currently have budget allocated for camera systems?&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="17">
          <option value="option1">-Select-</option>
          <option value="option2">Yes</option>
          <option value="option2">No</option>
        </select>
        <span class="te-error6">&lt;&nbsp;required field</span>
      </div>

      <div style="width:347px; float:right;position:relative;">
        <label class="te-form-label">How old are your current ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="12">
          <option value="option1">-Select-</option>
          <option value="option2">1-2 years</option>
          <option value="option2">2-3 years</option>
          <option value="option2">3-4 years</option>
          <option value="option2">4 and older</option>
          <option value="option2">Not applicable</option>
        </select>
        <span class="te-error5">&lt;&nbsp;required field</span>

        <label class="te-form-label">When do you plan to acquire or replace your ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="14">
          <option value="option1">-Select-</option>
          <option value="option2">1-3 months</option>
          <option value="option2">3-6 months</option>
          <option value="option2">6 or more months</option>
          <option value="option2">Not yet determined</option>
          <option value="option2">Not applicable</option>
        </select>
        <span class="te-error5">&lt;&nbsp;required field</span>

        <label class="te-form-label">Do you have budget allocated in your planned<br />acquire/replace timeframe for new ECWs?&nbsp;<span style="color:#ff0000;">*</span></label>
        <select class="te-form-select2" tabindex="16">
          <option value="option1">-Select-</option>
          <option value="option2">Yes</option>
          <option value="option2">No</option>
        </select>
        <span class="te-error5">&lt;&nbsp;required field</span>

        <label class="te-form-label">Additional Notes:</label>
        <textarea id="te-form-note" tabindex="18"></textarea>

        <div id="te-submit-btn-div">
          <input id="te-submit-button" type="image" tabindex="19" onclick="window.location='main/taserthankyou'" src="images/te-submit-button.png" value="">
        </div>

      </div>
                  
      <div style="clear:both;"></div>




    </div>
</div><!-- close policy_div-->