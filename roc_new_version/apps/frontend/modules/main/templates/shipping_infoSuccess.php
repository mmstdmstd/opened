<div id="policy_div" >
  <h2>Shipping Information</h2>
    <p>Orders of in-stock merchandise received Monday through Friday by 3:00 p.m. CT are usually shipped the same day for receipt within 3-5 business days.</p>
    <p>You should expect an extended delivery date when:</p>
    <ul>
      <li>Merchandise requires personalization</li>
      <li>Merchandise is shipped direct from the manufacturer</li>
      <li>Merchandise is of an irregular size or weight that doesn't allow for air delivery</li>
      <li>Merchandise is a full-size lightbar</li>
      <li>Merchandise is on back order from the manufacturer</li>
      <li>Shipment is destined for a US Territory or Possession, a Foreign Nation, Canadian Province or contains a Military Shipping Code (APO/FPO, etc.)</li>
      <li>The transaction is for a restricted item and or restricted destination requiring a US Government authorized export license for shipment</li>
    </ul>
    <p>If you need your order sooner than our fast Standard Best Way Delivery, you can specify 2-Day, Next Day Air Saver, or 3 Day Select. Orders received on Saturdays, Sundays and holidays are processed on the next business day. Fast delivery requires you to supply us with a street address for shipping; using a P.O. Box address may delay your order.</p>
    <p>Important note regarding delivery of hazardous materials: The shipping of hazardous materials is heavily regulated. If your order contains these items (including pepper spray, flares, etc;), they will be shipped Standard Best Way Delivery regardless of the delivery method selected at Checkout.</p>
    <p>2-Day, Next Day, and 3 Day Select Delivery charges are in addition to the Standard Best Way Delivery rates.</p>
    <p>Deliveries to APO/FPO and PO Boxes will be shipped via United States Postal Service at the Standard rates. Select Standard Best Way Delivery at checkout. Please allow for extended delivery times.</p>
    <p>Important note regarding APO/FPO shipments: Due to recent changes in export restrictions, some items may require an export license which could delay your shipment. We apologize for any inconvenience this may cause.</p>
    <p>Not all services available to all locations&mdash;please contact us for details.</p>
    <p>Heavy or bulky items require separate shipping due to size and weight. For lightbars (38" and up), a separate $24.95 shipping charge will be added to the final shipping total for each lightbar ordered. Additional shipping and handling charges also apply to walk-through metal detectors, safes, vehicle partitions, push bumpers, and traffic cones. </p>
    <p>Guns must be shipped Next Day Air&nbsp;&#64;&nbsp;$40.00 to Illinois / Indiana and $50.00 to Michigan, Minnesota, Missouri, Ohio and Iowa per gun. This is in addition to the standard freight charges.</p>
    <p>Hazardous materials (fuses, etc.) require a Hazmat fee of $35.00 per box plus standard freight charges. Important note regarding delivery of hazardous materials: The shipping of hazardous materials is heavily regulated. If your order contains these items (including pepper spray, flares, etc;), they will be shipped Standard Best Way Delivery regardless of the delivery method selected at Checkout.</p>    
    <p>Updated 11/5/12. Subject to change without notice.</p>
</div>