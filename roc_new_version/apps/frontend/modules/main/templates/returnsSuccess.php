<div id="policy_div" >
  <h2>Returns, Repairs, and Replacements</h2>

  <div style="margin:0 auto; width:884px;">
      <p>If you aren't satisfied with a newly purchased item, simply return it to us so we can repair it, replace it, or refund your money.</p>
      <p>We will refund or credit the value of the item and any taxes you were charged (except where an item is repaired and returned to you). Original shipping charges are non-refundable unless the return is related to our error.</p>
      <p>Special order items or items that have been altered, decorated, engraved, customized, or otherwise embellished at the request of a customer cannot be returned unless the return is related to our error.</p>
			<p>All clearance items are sold "as is" and cannot be returned.</p>
			<p>The original packing receipt should accompany all returns or the return must include the original order number associated with the return.</p>
      <p>Please call us at (800) 223-2097 and ask for the Returns Department or send return to:</p>
			<p style="margin-bottom:27px !important;">
			Ray O'Herron Co. Inc.<br />3549 N. Vermilion St.<br />P.O. Box 1070<br />Danville, IL 61834</p>
  </div>
</div><!-- close policy_div-->
