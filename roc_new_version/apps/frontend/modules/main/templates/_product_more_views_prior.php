<?php 
$product_more_views_columns= (int)sfConfig::get('app_application_product_more_views_columns' );
$InventoryItemsCount= count($MoreViewInventoryItems);
if ( $InventoryItemsCount< 5 ) $InventoryItemsCount= 5;

$RowsCount= (int) ($InventoryItemsCount/$product_more_views_columns);

$HasDecimal= $InventoryItemsCount%$product_more_views_columns;
if ( $HasDecimal> 0 ) $RowsCount++;
?>


      <p id="sub_heading_1"><?php echo __("More Views") ?>:</p>
      
      <div id="more_views_div" >
      <?php $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
      
        <?php $VideoForRow= ''; $ThumbnailImageFile= '';
          for($Col=1; $Col<= $product_more_views_columns; $Col++) : ?>
            <?php if ( /*$I < $rows_in_pager and*/ !empty($MoreViewInventoryItems[$I]) ) :
              $lNextProduct= $MoreViewInventoryItems[$I];
              
              $NextSku= $lNextProduct->getSku();
              //Util::deb( $NextSku, ' $NextSku::' );
              $NextVideo= trim($lNextProduct->getVideo());
              if ( !empty($NextVideo) and empty($VideoForRow) ) {
                $A= preg_split( '/,/', $NextVideo );
                if ( !empty($A[0]) ) {
                	$VideoForRow= $A[0];
                }
              }
              
              $ThumbnailImageFile= AppUtils::getThumbnailImageBySku($NextSku, false); 
    //Util::deb( $ThumbnailImageFile, ' -000 $ThumbnailImageFile::' );
      //        Util::deb( $NextSku, ' $NextSku::' );
              if ( empty($ThumbnailImageFile) ) {
  	            $ThumbnailImageFile=  AppUtils::getResizedImageBySku($NextSku, false, false, false); 
    //Util::deb( $ThumbnailImageFile, ' -1 $ThumbnailImageFile::' );
              }            
            ?> 
              

    <?php $ThumbnailImageFullUrl= '';
      if ( !empty($ThumbnailImageFile) and !is_array($ThumbnailImageFile) ) : 
        $ThumbnailImageUrl= '/uploads'.$ThumbnailImageFile; 
        //Util::deb( $ThumbnailImageUrl, ' $ThumbnailImageUrl::' );
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;  
        //Util::deb( $ThumbnailImageFullUrl, ' $ThumbnailImageFullUrl::' );
        
        if ( !file_exists($ThumbnailImageFullUrl) and !is_dir($ThumbnailImageFullUrl)) {
        	//Util::deb( ' -99::' );
        	$ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage48.png' ;
        }
        
        ?> 
          <a href="<?php echo url_for(  "@product_details?sku=" . $NextSku  ) ?>">
            <?php echo image_tag( $ThumbnailImageUrl,array('alt'=>"", "height"=>"48", "width"=>"48") ); ?>
          </a>          
      <?php endif; ?>
      
      <?php if ( is_array($ThumbnailImageFile) and !empty($ThumbnailImageFile/*['ImageFileName']*/)  ) : 
        //Util::deb( $ThumbnailImageFile, ' -2 $ThumbnailImageFile::' ); 
        $ThumbnailImageUrl= 'uploads'.DIRECTORY_SEPARATOR.$ThumbnailImageFile['ImageFileName'];
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;
        //Util::deb( $ThumbnailImageUrl, ' -2 $ThumbnailImageUrl::' );
        //Util::deb( $ThumbnailImageFullUrl, ' -3 $ThumbnailImageFullUrl::' );
        $ThumbnailImageFullFilePath= sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
        //Util::deb( $ThumbnailImageFullFilePath, ' $ThumbnailImageFullFilePath::' );
        if ( !file_exists($ThumbnailImageFullFilePath) or is_dir($ThumbnailImageFullFilePath)) {
        	//Util::deb( -4 );
        	$ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage48.png' ;
        }
        //Util::deb( $ThumbnailImageFullUrl, ' -33334445555555 $ThumbnailImageFullUrl::' );
        ?> 
        
          <a href="<?php echo url_for(  "@product_details?sku=" . $NextSku  ) ?>" >
            <?php echo image_tag( $ThumbnailImageFullUrl,array('alt'=>"", "height"=>/*$ThumbnailImageFile['Height']*/"48", "width"=>/*$ThumbnailImageFile['Width']*/"48") ); ?>
          </a>          
      <?php endif; // if ( $NextSku=='0-8001-090' ) die("DIE"); ?>
              
              
              
            <?php else: ?>
             &nbsp;
            <?php endif; ?>
          
        <?php $I++; endfor; ?>
            <?php  if ( !empty($VideoForRow) ) {
            	$lYoutubeVideo= YoutubeVideoPeer::getSimilarYoutubeVideo( $VideoForRow );            	
            	if ( !empty($lYoutubeVideo) ) {
                $ImageTag= '<img height="48" width="48" alt="" src="'.AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ).'images/video-image_1.jpg" />';

                $Src= '<a class="thickbox" href="'.url_for( "@product_video_view?sku=" . $NextSku . '&video_key=' . $VideoForRow ).'/width/1000/height/800' . '">'.$ImageTag.'</a> ';
                echo $Src;
            	}
            }
            if ( empty($VideoForRow) ) {
              $ImageTag= '<img height="48" width="48" alt="" src="'.AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ).'images/novideo48.png" />';
              echo $ImageTag;            	
            }
            
?>            

      <?php endfor; ?>
      </div>
      
<?php return ?>

<p id="sub_heading_1">More Views:</p>
             <div id="more_views_div" >
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-green.jpg" />

              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-blue.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/video-image_1.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-green.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-blue.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/video-image_2.jpg" />

               <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-green.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-blue.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/shoe-brown.jpg" />
              <img height="48" width="48" alt="" src="http://www.local-roc_samples.com//images/video-image_1.jpg" />
            </div>        
            
            

<?php return;
$product_more_views_columns= (int)sfConfig::get('app_application_product_more_views_columns' );
$InventoryItemsCount= count($MoreViewInventoryItems);
if ( $InventoryItemsCount< 5 ) $InventoryItemsCount= 5;

$RowsCount= (int) ($InventoryItemsCount/$product_more_views_columns);

$HasDecimal= $InventoryItemsCount%$product_more_views_columns;
if ( $HasDecimal> 0 ) $RowsCount++;
?>


  <tr>
    <td colspan="2">
      <b><?php echo __("More Views") ?>:</b>
      
      <div style="max-height:120px; overflow:auto;">
      <table style="border:0px dotted; " valign="top">
      <?php $I= 0; for($Row=1; $Row<= $RowsCount; $Row++) : ?>
        <tr id="tr_product_listings_<?php echo $Row ?>" align="left">
      
        <?php $VideoForRow= '';
          for($Col=1; $Col<= $product_more_views_columns; $Col++) : ?>
          <td >
            <?php if ( /*$I < $rows_in_pager and*/ !empty($MoreViewInventoryItems[$I]) ) :
              $lNextProduct= $MoreViewInventoryItems[$I];
              
              $NextSku= $lNextProduct->getSku();
               
              $NextVideo= trim($lNextProduct->getVideo());
              if ( !empty($NextVideo) and empty($VideoForRow) ) {
                $A= preg_split( '/,/', $NextVideo );
                if ( !empty($A[0]) ) {
                	$VideoForRow= $A[0];
                }
              }
              
              $ThumbsnailImageFile=  AppUtils::getThumbnailImageBySku($NextSku, false); 
              if ( empty($ThumbnailImageFile) ) {
  	            $ThumbnailImageFile=  AppUtils::getResizedImageBySku($NextSku, false, false, false); 
              }            
            ?> 
              

    <?php $ThumbnailImageFullUrl= '';
      if ( $ThumbnailImageFile and !is_array($ThumbnailImageFile) ) : 
        $ThumbnailImageUrl= '/uploads'.$ThumbnailImageFile; 
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl?> 
        <div >
          <a href="<?php echo $ThumbnailImageFullUrl ?>">
            <?php echo image_tag( $ThumbnailImageUrl,array('alt'=>"", "height"=>"150", "width"=>"150") ); ?>
          </a>          
        </div>
      <?php endif; ?>
      
      <?php if ( $ThumbnailImageFile and is_array($ThumbnailImageFile) ) : 
        $ThumbnailImageUrl= 'uploads'.DIRECTORY_SEPARATOR.$ThumbnailImageFile['ImageFileName'];
        $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl?> 
        <div >
          <a href="<?php echo url_for(  "@product_details?sku=" . $NextSku  ) ?>" >
            <?php echo image_tag( $ThumbnailImageFullUrl,array('alt'=>"", "height"=>$ThumbnailImageFile['Height'], "width"=>$ThumbnailImageFile['Width']) ); ?>
          </a>          
        </div>
      <?php endif; ?>
              
              
              
            <?php else: ?>
             &nbsp;
            <?php endif; ?>
          </td >
          
        <?php $I++; endfor; ?>
          <td >
            <?php  if ( !empty($VideoForRow) ) {
            	$lYoutubeVideo= YoutubeVideoPeer::getSimilarYoutubeVideo( $VideoForRow );            	
            	if ( !empty($lYoutubeVideo) ) {
                $ImageTag= "Video";
                $Src= '<a class="thickbox" href="'.url_for( "@product_video_view?sku=" . $NextSku . '&video_key=' . $VideoForRow ).'/width/1000/height/800' . '">'.$ImageTag.'</a> ';
                echo $Src;
            	}
            }
?>            
          </td >

        </tr> 
      <?php endfor; ?>
      </table>
      </div>
      
    </td>
  </tr>