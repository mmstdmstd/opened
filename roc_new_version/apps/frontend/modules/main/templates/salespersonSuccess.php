<form action="<?php echo url_for('@product_listings?page=1&rows_in_pager='.$rows_in_pager  ) ?>" id="form_product_listings" method="POST">
    <?php include_partial( 'main/search_criteria', array( 'select_category'=>$select_category,  'sorting'=>$sorting, 'page'=>$page, 'rows_in_pager'=>$rows_in_pager,'select_subcategory'=> $select_subcategory,  'select_brand_id'=> $select_brand_id, 'input_search'=> $input_search, 'HeaderTitle'=> $HeaderTitle, 'RootItem'=> $RootItem, 'HasRootItem'=> $HasRootItem, 'RootItemObject'=>$RootItemObject, 'Matrix'=> $Matrix ) ) ?>
</form>
<style type="text/css">
#salespersons {width:885px; overflow:hidden; margin:0 auto; font-size:130%;}
#salespersons a {color:#222;text-decoration:none;}
#salespersons a:hover {text-decoration:underline;}
#salespersons h1 {margin:40px 0 0 0; font-size:115%;}
#salespersons h2 {margin:15px 0 0 0; font-size:150%;}
#salespersons ol {margin:0; padding:0; list-style:none; line-height:1.3;}
#salespersons ol li {width:428px; height:204px; border:2px solid #777; overflow:hidden; float:left; margin-top:20px; background:url(<?php Util::getServerHost() ?>images/card-bg.png) repeat-x;}
#salespersons ol li.even {margin-left:21px;}
#salespersons ol li p span {display:inline-block; width:45px;}
#salespersons ol li p {margin:0;}
#salespersons ol li p strong {font-size:90%;}
</style>

<div id="salespersons">
	<img style="float:right; padding:20px 0 0 32px; margin:8px 0 14px 0;" src="<?php Util::getServerHost() ?>images/salesperson-map.png" />
	<h1 style="margin-left:1px; position:absolute; top:75px; left:34px;">Salespersons&mdash;Departments Only</h1>
	<p style="font-size:80%; line-height:1.28; color:#222; margin:64px 0 0 2px;">If one of our salesmen handle your area, be sure to give them a call. They will be more than happy to stop in and speak with you in person and discuss any of the products and values we offer.</p>
	<p style="font-size:80%; line-height:1.28; color:#222; margin:14px 0 0 2px">If a salesman does not represent your area, stop by one of our stores in <a href="<?php Util::getServerHost() ?>main/contact">Lombard</a> or <a href="<?php Util::getServerHost() ?>main/contact">Danville Illinois</a>. You can also call us at 1-800-223-2097 and we will be glad to take your order!</p>
	<ol>
		<li>
			<img src="<?php Util::getServerHost() ?>images/sales-dan-yara.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Dan Yara</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2000</p>
			<p><strong><span>cell:</span> 708-710-3396</strong></p>
			<p><strong><span>fax:</span> 708-532-5714</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:dpyara@comcast.net">dpyara@comcast.net</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-01.png" style="float:right; margin:-2px 26px 0 0;" />
			<p style="font-size:70%; margin:0px 90px 0 200px; position:relative; top:16px;">Represents Southern Cook Country, Central Illinois and Lake County Indiana</p>
		</li>
		<li class="even">
			<img src="<?php Util::getServerHost() ?>images/sales-larry-fredericks.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Larry Fredericks</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 1987</p>
			<p><strong><span>cell:</span> 217-474-3826</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:ljfred5@comcast.net">ljfred5@comcast.net</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-08.png" style="float:right; margin:0px 26px 0 0;" />
			<p style="font-size:70%; margin:5px 90px 0 120px; position:relative; top:16px;">Represents Western and Southern Illinois, Central and Eastern Missouri and McCracken County, KY</p>
		</li>
		<li>
			<img src="<?php Util::getServerHost() ?>images/sales-gary-critzer.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Gary Critzer</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2009</p>
			<p><strong><span>cell:</span> 812-350-7390</strong></p>
<!--			<p><strong><span>fax:</span> 812-799-0823</strong></p>  -->
			<p><strong><span>email:</span>&nbsp;<a href="mailto:gecritzer@aol.com">gecritzer@aol.com</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-03.png" style="float:right; margin:-2px 36px 0 0;" />
			<p style="font-size:70%; margin:2px 90px 0 290px; position:relative; top:16px;">Represents<br /> Indiana</p>
		</li>
		<li class="even">
			<img src="<?php Util::getServerHost() ?>images/sales-bob-komarek.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Bob Komarek</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2007</p>
			<img src="<?php Util::getServerHost() ?>images/state-02.png" style="float:right; margin:2px 16px 0 0; position:relative; left:-20px;" />
			<p><strong><span>cell:</span> 630-235-2098</strong></p>
			<p><strong><span>fax:</span> 630-513-7264</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:rrk1@earthlink.net">rrk1@earthlink.net</a></strong></p>
			<p style="font-size:70%; margin:0px 85px 0 170px; position:relative; top:16px;">Represents the northern counties of Illinois, and southern Wisconsin counties south of Dade County</p>
		</li>
		<li>
			<img src="<?php Util::getServerHost() ?>images/sales-ken-fortman.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Ken Fortman</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2010</p>
			<p><strong><span>cell:</span> 612-227-3872</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:kenfortman@gmail.com">kenfortman@gmail.com</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-05.png" style="float:right; margin:32px 26px 0 0;" />
			<p style="font-size:70%; margin:17px 90px 0 230px; position:relative; top:16px;">Represents Iowa and all of South Dakota</p>
		</li>
		<li class="even">
			<img src="<?php Util::getServerHost() ?>images/sales-tj-lindmeier.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>TJ Lindmeier</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2009</p>
			<p><strong><span>cell:</span> 715-675-2899</strong></p>
			<p><strong><span>fax:</span> 715-675-3932</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:tjconj@charter.net">tjconj@charter.net</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-04.png" style="float:right; margin:9px 18px 0 0;" />
			<p style="font-size:70%; margin:0px 110px 0 150px; position:relative; top:16px;">Represents all of Minnesota, North Dakota, West, Central, and Northern Wisconsin</p>
		</li>
		<li>
			<img src="<?php Util::getServerHost() ?>images/sales-craig-villanova.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Craig Villanova</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 1990</p>
			<p><strong><span>cell:</span> 773-841-9967</strong></p>
			<p><strong><span>office:</span> 630-629-COPS</strong></p>
			<p><strong><span>fax:</span> 630-629-2682</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:rocin4le@me.com">rocin4le@me.com</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-07.png" style="float:right; margin:-12px 26px 0 0;" />
			<p style="font-size:70%; line-height:1.2; margin:-7px 50px 0 270px; position:relative; top:16px;">Represents Northern DuPage and Cook County</p>
		</li>
		<li class="even">
			<img src="<?php Util::getServerHost() ?>images/sales-mike-hatfield.jpg" style="float:left; margin:20px 18px 60px 20px; " />
			<h2>Mike Hatfield</h2>
			<p style="color:#e5ac39; margin-bottom:10px; font-size:75%;">proudly serving since 2010</p>
			<p><strong><span>cell:</span> 816-352-4378</strong></p>
			<p><strong><span>fax:</span> 816-478-7074</strong></p>
			<p><strong><span>email:</span>&nbsp;<a href="mailto:mbh1222@gmail.com">mbh1222@gmail.com</a></strong></p>
			<img src="<?php Util::getServerHost() ?>images/state-06.png" style="float:right; margin:12px 26px 0 0;" />
			<p style="font-size:70%; margin:-2px 90px 0 200px; position:relative; top:16px;">Represents Kansas and the Western Counties of Missouri</p>
		</li>
	</ol>
</div>
