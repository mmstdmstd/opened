<?php
// define("ElemSeparator","^");
$HostForImage = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration());

function ReplaceSpecialChars( $Item ) {
	$Item= preg_replace("/,/", "-",$Item);
	$Item= preg_replace("/\//", "^",$Item); // GLK 19, 23 GLK 19/23
	$Item= preg_replace("/\"/", "|",$Item);
	$Item= preg_replace("/[^a-zA-Z0-9-=0]+/", "_",$Item);
	return $Item;
}

function ShowPriceItem( $PricesCountItem ) {
  /* echo   ($PricesCountListing[$I]['prior_key'] ) ?>.00-$<?php echo  ( $PricesCountListing[$I]['key'] == 20000 ? "..." : ($PricesCountListing[$I]['key'] - 0.01 ) )  */
  if ( $PricesCountItem['key'] == 25 ) {
		return 'Up to $'.  (  $PricesCountItem['key'] - 0.01 ) ;
	}
	if ( $PricesCountItem['key'] == 20000 ) {
		return '$10,000+' ;
	}

	return "$".$PricesCountItem['prior_key'] . ' to $' . ( $PricesCountItem['key'] - 0.01 ) ;

}

function SortSizeBySizeOrder( $SizesCountListing ) {
	$ResArray= array();
	// getSizeOrders( $page=1, $ReturnPager=true, $filter_size= '',  $Sorting='ID', $Limit= '' ) {
	$SizeOrderList= SizeOrderPeer::getSizeOrders('', false /*, '02'*/ );
	foreach( $SizeOrderList as $lSizeOrder ) {
		//Util::deb( $lSizeOrder->getSize(), '$lSizeOrder->getSize()::');
	  foreach( $SizesCountListing as $SizeKey=> $SizesCount) {
			//Util::deb($SizesCount['size'], '$SizesCount[size]::');
      if ( strtolower($lSizeOrder->getSize()) == strtolower($SizesCount['size']) ) {
				$ResArray[]= $SizesCount;
				$SizesCountListing[ $SizeKey ]['is_used']= 1 ;
				break;
			}
		}
	}
	//Util::deb($ResArray, '$ResArray::');
	//die("-1");
  /*reset($SizesCountListing);
	foreach( $SizesCountListing as $SizeKey=> $SizesCount) {
		if ( empty($SizesCount['is_used']) ) {
			$ResArray[]= $SizesCount;
			//$ResArray[ count($ResArray)-1 ]['is_used']= 1;
		}
	} */
	return $ResArray;
}

?>
<style type="text/css">
	/* sz commented out
	html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
			border: 0 none;
			font-family: inherit;
			font-size: 100%;
			font-style: inherit;
			font-weight: inherit;
			list-style: none outside none;
			margin: 0;
			padding: 0;
			vertical-align: baseline;
	}
	*/
h3 a {
	font-size: 18px;
}

#wrapper-div {
	font-family: "arial", sans-serif;
}

.visually-hidden, .ada-hidden {
	border: 0 none;
	clip: rect(0px, 0px, 0px, 0px);
	height: 1px;
	margin: -1px;
	overflow: hidden;
	padding: 0;
	position: absolute;
	width: 1px;
}

#dimensions {
	float: left;
	font-family: arial, sans-serif;
	line-height: 1;
	margin: 0 5px 0 0; /* was 30px sz */
	width: 211px;/* was 186px sz */
}

.dimension-group h3 {
	background: none repeat scroll 0 0 #e6e7e9;
	font-size: 13px;
	font-weight: normal;
	position: relative;
	display: block;
	width: 211px; /* was 186px sz */
	margin: 0;
	padding: 0;
	/* sz    text-transform: uppercase; */
}

.dimension-group h3 a {
	color: #252525;
	display: block;
	padding: 8px 30px 8px 8px;
	text-decoration: none;
}

.dimension-group .toggle-button {
	background: url("<?php echo $HostForImage; ?>images/plusminus-btn.png") no-repeat scroll 0 0 transparent;
	cursor: pointer;
	display: block;
	height: 16px;
	position: absolute;
	right: 7px;
	top: 10px;
	width: 16px;
	overflow: none;
}

.dimension-group .expand .toggle-button {
	background-position: 0px -14px;
}

.dimension-group .collapse .toggle-button {
	background-position: 0px 0px;
}

.dimension-group {
	margin: 0 0 6px;
}

.dimension-group .hidden {
	display: none;
}

.dimension-list {
	font-size: 12px;
/*	padding: 9px; */
  padding: 0px;
	position: relative;
}

.dimension-list:after {

  background: -moz-linear-gradient(top,  rgba(255,255,255,0) 0%, rgba(255,255,255,1) 91%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0)), color-stop(100%,rgba(255,255,255,1))); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 91%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 91%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 91%); /* IE10+ */
  background: linear-gradient(to bottom,  rgba(255,255,255,0) 0%,rgba(255,255,255,1) 91%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */

	bottom: 0px;
	content: "";
	display: block;
	height: 15px;
	position: absolute;
	width: 91%;/* was 100% sz */
	z-index: 1;
}

.dimension-list p {
	color: #B9B8B8 !important;
	margin: 0 0 9px !important;
}

.dimension-list p a {
	color: #4791C5;
}

.dimension-list p a:hover {
	color: #0066AE;
}

.dimension-list li {
/*	margin: 7px 0 !important;*/
  margin: 2px 0 !important;
	padding: 1px 0 2px 0px !important; /* sz */
	position: relative !important;
}

.mobile-device .dimension-list li {
	transform: translateZ(0px);
}

.dimension-list .first {
	margin-top: 0;

}

.dimension-list .last {
	margin-bottom: 0;
	padding-bottom: 15px !important;

}

.dimension-list li a {
	color: #666666;
	display: inline-block;
	text-decoration: none;

}

.dimension-list .unavailable {
	color: #E0DEDA;
	padding: 1px 0 2px;
}

.dimension-list .number-products {
	color: #BBB7B1;
	white-space: nowrap;
}

.dimension-list .checkbox {
	/*    background: url("
<?php echo $HostForImage; ?> images/checkmark-4state.png") no-repeat scroll 0px 0px transparent;   */
	border-radius: 2px 2px 2px 2px;
	display: block;
	height: 15px;
	left: 1px;
	position: absolute;
	top: 0px;
	width: 15px;
}

.dimension-list .checkbox {
	cursor: pointer;
}

	/* sz
	.dimension-list .checkbox {
			background-position: 0px -45px;
	}

	.dimension-list a:focus .checkbox, .dimension-list a:hover .checkbox {
			background-position: 0px -15px;
	}
	.dimension-list .checked .checkbox, .dimension-list .checked:focus .checkbox, .dimension-list .checked:hover .checkbox, .dimension-list a:active .checkbox {
			background-position: 0px -30px;
			border-color: #7E766C;
	}
	.dimension-list .unavailable .checkbox {
			background-position: -63px -163px;
			border-color: #FFFFFF;
	}
	*/
.dimension-category .dimension-list .checkbox {
	border: medium none;
	height: 0;
	width: 0;
}

.dimension-category .dimension-list .checked .checkbox {
	background-position: -82px -162px;
	border-color: #FFFFFF;
	height: 13px;
	width: 13px;
}

.dimension-colors li {
	padding-left: 50px !important; /* sz */
}

.dimension-colors .swatch {
	border-radius: 2px 2px 2px 2px;
	display: block;
	height: 15px;
	left: 20px;
	position: absolute;
	top: 0;
	width: 30px;
}

.dimension-colors a .swatch {
	cursor: pointer;
}

.dimension-colors .unavailable .swatch {
	opacity: 0.2;
}

.ie .dimension-colors .unavailable .swatch {
}

.dimension-list .unavailable .number-products {
	color: #E0DEDA;
}

	/* sz
	.swatch-beige {
			background: none repeat scroll 0 0 #D7C79E;
	}
	*/
.swatch-beige {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px 0px;
}

.swatch-blue {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -15px;
}

.swatch-black {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -30px;
}

.swatch-brown {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -45px;
}

.swatch-camo {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -60px;
}

.swatch-gray {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -75px;
}

.swatch-green {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -90px;
}

.swatch-gold {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -105px;
}

.swatch-orange {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -120px;
}

.swatch-pink {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -135px;
}

.swatch-purple {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -150px;
}

.swatch-red {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -165px;
}

.swatch-silver {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -180px;
}

.swatch-white {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -195px;
}

.swatch-yellow {
	background: url("/images/colors-list.png") no-repeat scroll 0 0;
	background-position: 0px -210px;
}

.dimension-list .expose {
	max-height: 184px;
	min-height: 30px;
	overflow-x: hidden;
	overflow-y: auto;
	padding: 0 5px 0 0 !important;
	position: relative;
}

	/* sz added expose ul */
.expose ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	width: 200px; /* sz */
}
.expose input {
  margin:0px;
  padding:0px;
}
.mobile-device .dimension-list .expose {
	transform: translateZ(0px);
}

.swatch-container {
	height: 20px;
	margin: 0 0 11px -1px;
	overflow: hidden;
	position: relative;
}

.swatch-container li {
	border: 1px solid #FFFFFF;
	border-radius: 3px 3px 3px 3px;
	float: left;
}

.swatch-container li:hover {
	border-color: #C7C3BF;
}

.swatch-container a {
	border: 1px solid #FFFFFF;
	border-radius: 2px 2px 2px 2px;
	display: block;
}

.swatch-container .swatch {
	background: none repeat scroll 0 0 transparent;
	border-radius: 2px 2px 2px 2px;
	display: block;
	height: 26px;
	width: 57px;
	overflow: hidden;
}

.swatch-container .toggle {
	position: absolute;
	right: 1px;
	top: 0;
}

.swatch-container .toggle-button {
	background: url("/images/product-matrix-sprite.png") no-repeat scroll 0 0 transparent;
	height: 16px;
	width: 16px;
}

.swatch-container .toggle-expand {
	background-position: -142px -142px;
}

.swatch-container .toggle-collapse {
	background-position: -162px -142px;
}

.swatch-container .even-more {
	border: medium none;
	line-height: 17px;
	padding: 0 0 0 5px;
	text-transform: lowercase;
}

/* sz test */
.check_box {
    opacity:0;
     filter: alpha(opacity=0);
}
.check_box_img_div {
/*    background:url('images/checkmark-4state.png') no-repeat;    */
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 0;
  height: 15px;
  width:15px;
  position:relative;
  display:inline-block;
  padding: 0 0 0 0px;
}
/*
.check_box_img_div:hover {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 -15px;
}
*/
/*.check_box_img_div:active {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 -30px;
}
*/
.check_box_img_div_checked {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 -30px;
}
.check_box_img_div_checked_hover {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 -45px;
}
.check_box_img_div_unchecked {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 0px;
}
.check_box_img_div_unchecked_hover {
	background: url("/images/checkmark-4state.png") no-repeat scroll 0 -15px;
}
/* end sz test */
</style>


<?php
function BlockPrices($PricesArray)
{
	$ResArray = array();

	foreach ($PricesArray as $Price) {

		if ($Price['std_unit_price'] >= 0 and $Price['std_unit_price'] < 25) {
			if (empty($ResArray[25])) {
				$ResArray[25] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[25] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 25 and $Price['std_unit_price'] < 50) {
			if (empty($ResArray[50])) {
				$ResArray[50] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[50] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 50 and $Price['std_unit_price'] < 75) {
			if (empty($ResArray[75])) {
				$ResArray[75] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[75] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 75 and $Price['std_unit_price'] < 100) {
			if (empty($ResArray[100])) {
				$ResArray[100] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[100] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 100 and $Price['std_unit_price'] < 150) {
			if (empty($ResArray[150])) {
				$ResArray[150] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[150] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 150 and $Price['std_unit_price'] < 200) {
			if (empty($ResArray[200])) {
				$ResArray[200] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[200] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 200 and $Price['std_unit_price'] < 250) {
			if (empty($ResArray[250])) {
				$ResArray[250] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[250] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 250 and $Price['std_unit_price'] < 500) {
			if (empty($ResArray[500])) {
				$ResArray[500] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[500] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 500 and $Price['std_unit_price'] < 750) {
			if (empty($ResArray[750])) {
				$ResArray[750] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[750] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 750 and $Price['std_unit_price'] < 1000) {
			if (empty($ResArray[1000])) {
				$ResArray[1000] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[1000] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 1000 and $Price['std_unit_price'] < 2500) {
			if (empty($ResArray[2500])) {
				$ResArray[2500] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[2500] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 2500 and $Price['std_unit_price'] < 5000) {
			if (empty($ResArray[5000])) {
				$ResArray[5000] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[5000] += $Price['std_unit_prices_count'];
			}
		}

		if ($Price['std_unit_price'] >= 5000 and $Price['std_unit_price'] < 10000) {
			if (empty($ResArray[10000])) {
				$ResArray[10000] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[10000] += $Price['std_unit_prices_count'];
			}
		}


		if ($Price['std_unit_price'] >= 10000 /* and $Price['std_unit_price'] <= 500*/) {
			if (empty($ResArray[20000])) {
				$ResArray[20000] = $Price['std_unit_prices_count'];
			} else {
				$ResArray[20000] += $Price['std_unit_prices_count'];
			}
		}

	}

  $PriorPricesArray= array(25=>0, 50=>25, 75=>50, 100=>75, 150=>100, 200=>150, 250=>200, 500=>250, 750=>500, 1000=>750, 2500=>1000, 5000=>2500, 10000=>5000, 20000=>10000);
	$A = array();
	foreach ($ResArray as $key => $value) {
		$A[] = array('key' => $key, 'count' => $value , 'prior_key'=>$PriorPricesArray[$key] );
	}
	uasort($A, 'SortByCount');
	//Util::deb( $A, '$A::' );
	$ResArray = array();
	foreach ($A as $key => $value) {
		$ResArray[] = $value;
	}
	return $ResArray;
}

function SortCheckedAtTop($SelectedItemsArray, $CountListing, $keyname) {
	$CheckedArray= array();
	$UnCheckedArray= array();
  foreach( $CountListing as $CountItem ) {
		if ( empty($CountItem) ) continue;
		if ( in_array( $CountItem[$keyname], $SelectedItemsArray,( $keyname =='key' ? false : true ) ) ) {
			$CheckedArray[]= $CountItem;
		} else {
			$UnCheckedArray[]= $CountItem;
		}
	}
	foreach($UnCheckedArray as $UnCheckedItem) {
		if ( empty($UnCheckedItem) ) continue;
	  array_push(	$CheckedArray , $UnCheckedItem );
	}
	return $CheckedArray;
}

function SortColorsByCount($a, $b) {
	if ($a['colors_count'] == $b['colors_count']) {
		return 0;
	}
	return ($a['colors_count'] > $b['colors_count']) ? -1 : 1;
}



function SortByCount($a, $b)
{
	if ($a['key'] == $b['key']) {
		return 0;
	}
	return ($a['key'] > $b['key']) ? 1 : -1;
}

function  GetGenderlabel( $Gender ) {
	if ( $Gender == "Womens" ) return "Female";
	if ( $Gender == "Mens" ) return "Male";
	// Mens" ? "Male"
	// if ( empty($Gender) ) return "Unisex";
	return $Gender;
}


//Util::deb($checked_filters,'$checked_filters::');
$extended_parameters= $sf_data->getRaw('extended_parameters');
// Util::deb($extended_parameters,'$extended_parameters::');

$original_SelectedBrandsArray = $sf_data->getRaw('SelectedBrandsArray');
//Util::deb($original_SelectedBrandsArray, '$original_SelectedBrandsArray::');

$Time_1= time();
$A= array();

//$ATemp= InventoryItemPeer::getColorsCountListingAllGroups( $select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_black',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_gray',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_white',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_gold',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_silver',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_beige',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_brown',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_camo',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_pink',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_red',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_orange',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_yellow',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_green',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_blue',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

$ATemp= InventoryItemPeer::getColorsCountListing('udf_it_swc_purple',$select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
if ( !empty($ATemp) ) $A[]= $ATemp;

//Util::deb($A, '$A::');
// Util::deb($ATemp, '$ATemp::');
//die("kkk");

/*
Всем привет,
Есть таблица inventory_item в которых есть 15 булевых полей цветов udf_it_swc_black, udf_it_swc_gray, udf_it_swc_white...

надо посчитать количество цветов с пометкой 1
Я делал 15 запросов вида:
SELECT count(udf_it_swc_black) AS colors_count, 'udf_it_swc_black' AS color FROM `inventory_item` WHERE inventory_item.UDF_IT_SWC_BLACK=1
SELECT count(udf_it_swc_gray) AS colors_count, 'udf_it_swc_gray' AS color FROM `inventory_item` WHERE inventory_item.UDF_IT_SWC_GRAY=1
SELECT count(udf_it_swc_white) AS colors_count, 'udf_it_swc_white' AS color FROM `inventory_item` WHERE inventory_item.UDF_IT_SWC_WHITE=1
 ...
Потом их обьединял на клиенте - все бы хорошо но медленно
Но можно ли сделать один запрос посчитать данные 15 запросов выше ?
MySql 5.1.67.
*/
//uasort( $A, 'SortColorsByCount' );
$ColorsCountListing= array();
$original_SelectedColorsArray = $sf_data->getRaw('SelectedColorsArray');
foreach( $original_SelectedColorsArray as $key=>$original_SelectedColor ) {
	if( empty($original_SelectedColor) ) unset( $original_SelectedColorsArray[$key] );
}
foreach($A as $Key=>$Color) {
	if ( !empty($Color['color']) ) {
		$ASepred= preg_split('/udf_it_swc_/', $Color['color'] );
		$Color['color_name']= '';
		if ( !empty($ASepred[1]) ) {
			$Color['color_name']= ucwords($ASepred[1]);
		}
		$ColorsCountListing[]= $Color;
	}
}
//Util::deb($original_SelectedColorsArray, '$original_SelectedColorsArray::');
$ColorsCountListing= SortCheckedAtTop($original_SelectedColorsArray, $ColorsCountListing,  'color_name' );
//Util::deb($ColorsCountListing, '$ColorsCountListing::');
//die("LLL");


$Sale_PricesCountListing = InventoryItemPeer::getSale_PricesCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
//Util::deb($Sale_PricesCountListing, '$Sale_PricesCountListing::');
$original_SelectedSale_PricesArray = $sf_data->getRaw('SelectedSale_PricesArray');

// Util::deb($original_SelectedSale_PricesArray, '$original_SelectedSale_PricesArray::');
//die("UUu");
/*$Time_2= time();
Util::deb( $Time_1, '$Time_1::');
Util::deb( $Time_2, '$Time_2::');
$diff_Time= $Time_2 - $Time_1 ;
Util::deb( $diff_Time, '$diff_Time::');
Util::deb(strftime($diff_Time), '$diff_Time::');
*/

$A = InventoryItemPeer::getBrandsCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action );
//Util::deb($A, '$A::');
//die("IIIII");
$BrandsCountListing= array();
foreach($A as $Key=>$Brand) {
	if ( !empty($Brand['brand_id']) ) {
		$BrandsCountListing[]= $Brand;
	}
}
$BrandsCountListing= SortCheckedAtTop($original_SelectedBrandsArray, $BrandsCountListing,  'brand_id' );
//Util::deb($BrandsCountListing, '$BrandsCountListing::');


$original_SelectedSizesArray = $sf_data->getRaw('SelectedSizesArray');
foreach($original_SelectedSizesArray as $key=>$value) {
	$original_SelectedSizesArray[$key] = htmlspecialchars_decode($value);
}
// Util::deb($original_SelectedSizesArray, '+++$original_SelectedSizesArray::');

$A = InventoryItemPeer::getSizesCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
$SizesCountListing= array();
foreach($A as $Key=>$Size) {
	if ( !empty($Size['size']) ) {
		$SizesCountListing[]= $Size;
	}
}
//Util::deb($SizesCountListing, '-0 $SizesCountListing::');
// Util::deb($SizesCountListing, '$SizesCountListing::');
$SizesCountListing= SortSizeBySizeOrder( $SizesCountListing );  // ERROR !
// Util::deb($SizesCountListing, '=$SizesCountListing::');
//die("-III");

$SizesCountListing= SortCheckedAtTop($original_SelectedSizesArray, $SizesCountListing,  'size' );
// Util::deb($SizesCountListing, '++$SizesCountListing::');
// die("III");




$GendersCountListing= array();
$A= InventoryItemPeer::getGendersCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
foreach( $A as $key=> $GendersCount ) {
	//Util::deb($GendersCount, '$GendersCount::');
	if ( empty($GendersCount['gender']) ) {
		//$GendersCountListing[$key]['gender']= 'Unisex';
		continue;
	}
	$GendersCountListing[]= $GendersCount;
}

$original_SelectedGendersArray = $sf_data->getRaw('SelectedGendersArray');
$GendersCountListing= SortCheckedAtTop($original_SelectedGendersArray, $GendersCountListing,  'gender' );
// Util::deb($GendersCountListing, '$GendersCountListing::');


// Util::deb($GendersCountListing, '++$GendersCountListing::');
$PricesCountListing = InventoryItemPeer::getPricesCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
$original_SelectedPricesArray = $sf_data->getRaw('SelectedPricesArray');
//Util::deb($original_SelectedPricesArray, '$original_SelectedPricesArray::');
$PricesCountListing = BlockPrices($PricesCountListing);
//Util::deb($PricesCountListing, '$PricesCountListing::');
//die("YY");
$PricesCountListing= SortCheckedAtTop($original_SelectedPricesArray, $PricesCountListing, 'key');
//Util::deb($PricesCountListing, '++++$PricesCountListing::');



$original_SelectedFinishsArray = $sf_data->getRaw('SelectedFinishsArray');
foreach($original_SelectedFinishsArray as $key=>$value) {
	if ( empty($value) ) {
		unset( $original_SelectedFinishsArray[$key] );
		continue;
	}
	$original_SelectedFinishsArray[$key] = htmlspecialchars_decode($value);
}

$A = InventoryItemPeer::getFinishsCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
$FinishsCountListing= array();
foreach($A as $Key=>$Finish) {
	if ( !empty($Finish['finish']) ) {
		$FinishsCountListing[]= $Finish;
	}
}
$FinishsCountListing= SortCheckedAtTop($original_SelectedFinishsArray, $FinishsCountListing,  'finish' );
//Util::deb($FinishsCountListing, '$FinishsCountListing::');
//Util::deb($original_SelectedFinishsArray, '$original_SelectedFinishsArray::');
//die("HHTRRR");

$original_SelectedHandsArray = $sf_data->getRaw('SelectedHandsArray');
foreach($original_SelectedHandsArray as $key=>$value) {
	if ( empty($value) ) {
		unset( $original_SelectedHandsArray[$key] );
		continue;
	}
	$original_SelectedHandsArray[$key] = htmlspecialchars_decode($value);
}

$A = InventoryItemPeer::getHandsCountListing($select_category, $select_subcategory, $input_search, $extended_parameters, $checked_action);
$HandsCountListing= array();
foreach($A as $Key=>$Hand) {
	if ( !empty($Hand['hand']) ) {
		$HandsCountListing[]= $Hand;
	}
}
$RightValue= 0;
$LeftValue= 0;
$RValue= 0;
$LValue= 0;
foreach( $HandsCountListing as $key=>$HandsCount ) {
	if ( strtoupper($HandsCount['hand']) == strtoupper('Right') ) {
		$RightValue= $HandsCount['hands_count'];
	}
	if ( strtoupper($HandsCount['hand']) == strtoupper('Left') ) {
		$LeftValue= $HandsCount['hands_count'];
	}
	if ( strtoupper($HandsCount['hand']) == strtoupper('R') ) {
		$RValue= $HandsCount['hands_count'];
	}
	if ( strtoupper($HandsCount['hand']) == strtoupper('L') ) {
		$LValue= $HandsCount['hands_count'];
	}
}

$HandsCountListing= array();
if ( $RightValue+$RValue > 0 ) {
  $HandsCountListing[]= array( 'hand'=> 'Right', 'hands_count'=>($RightValue+$RValue) );
}
if ($LeftValue+$LValue > 0) {
	$HandsCountListing[]= array( 'hand'=> 'Left', 'hands_count'=>($LeftValue+$LValue) );
}
$HandsCountListing= SortCheckedAtTop($original_SelectedHandsArray, $HandsCountListing,  'hand' );
//Util::deb($HandsCountListing, '$HandsCountListing::');
//Util::deb($HandsCountListing, '++$HandsCountListing::');

//die("GGG");
?>

<script type="text/javascript">
	function toggleColorContent() {
		// Get the DOM reference
		var contentId = document.getElementById("color-list");
		var contentToggle = document.getElementById("color-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function toggleBrandContent() {
		// Get the DOM reference
		var contentId = document.getElementById("brand-list");
		var contentToggle = document.getElementById("brand-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function toggleSizeContent() {
		// Get the DOM reference
		var contentId = document.getElementById("size-list");
		var contentToggle = document.getElementById("size-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function toggleGenderContent() {
		// Get the DOM reference
		var contentId = document.getElementById("gender-list");
		var contentToggle = document.getElementById("gender-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function togglePriceContent() {
		// Get the DOM reference
		var contentId = document.getElementById("price-list");
		var contentToggle = document.getElementById("price-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function toggleSale_PriceContent() {
		// Get the DOM reference
		var contentId = document.getElementById("sale_price-list");
		var contentToggle = document.getElementById("sale_price-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

	function toggleFinishContent() {
		// Get the DOM reference
		var contentId = document.getElementById("finish-list");
		var contentToggle = document.getElementById("finish-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}
	function toggleHandContent() {
		// Get the DOM reference
		var contentId = document.getElementById("hand-list");
		var contentToggle = document.getElementById("hand-toggle-btn");
		// Toggle
		contentId.style.display == "block" ? contentId.style.display = "none" :
			contentId.style.display = "block";
		if (contentId.style.display == "none") contentToggle.className = "expand";
		if (contentId.style.display == "block") contentToggle.className = "collapse";
	}

</script>

<script type="text/javascript" language="JavaScript">
<!--


function BrandRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedBrands = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('brand_filter_');
		if (K > -1) {
			var BrandId = ElemId.substring(K + 'brand_filter_'.length, ElemId.length);
			if (document.getElementById('brand_filter_' + BrandId).checked) {
				SelectedBrands = SelectedBrands + BrandId + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		SizeRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedBrands=' + htmlspecialchars(SelectedBrands) + '|';
	if (run_other_filters) {
		onFilterSubmit('brand')
	}
}
function ClearBrandFilters() {
	document.getElementById('checked_filters').value = ''
	SizeRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}


function SizeRunFilter(run_other_filters) {
	/* var S="GLK 19, 23"
	alert(S.replace(/[^a-zA-Z0-9]+/g, "_" ))
	return; */
	var theForm = document.getElementById("form_filter");
	var SelectedSizes = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('size_filter_');
		if (K > -1) {
	//		var SizeId = ElemId.substring(K + 'size_filter_'.length, ElemId.length).replace(/[^a-zA-Z0-9]+/, "_" );
		  var SizeId = ElemId.substring(K + 'size_filter_'.length, ElemId.length);

			if (document.getElementById('size_filter_' + SizeId).checked) {
				//alert( "Checked:"+SizeId )
			  SelectedSizes = SelectedSizes + document.getElementById('size_filter_' + SizeId).alt + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
		HandRunFilter(false)
	}

	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedSizes=' +  htmlspecialchars(SelectedSizes) + '|';
	// alert(htmlspecialchars(SelectedSizes) )

	if (run_other_filters) {
		onFilterSubmit('size')
	}
}
function ClearSizeFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}


function ColorRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedColors = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('color_filter_');
		if (K > -1) {
			var ColorId = ElemId.substring(K + 'color_filter_'.length, ElemId.length);
			if (document.getElementById('color_filter_' + ColorId).checked) {
				SelectedColors = SelectedColors + document.getElementById('color_filter_' + ColorId).alt + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedColors=' + htmlspecialchars(SelectedColors) + '|';
	if (run_other_filters) {
		onFilterSubmit('color')
	}
}
function ClearColorFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}


function GenderRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedGenders = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('gender_filter_');
		if (K > -1) {
			var GenderId = ElemId.substring(K + 'gender_filter_'.length, ElemId.length);
			//alert("ElemId::"+ElemId+"  K::"+K+"  GenderId::"+GenderId)
			if (document.getElementById('gender_filter_' + GenderId).checked) {
				SelectedGenders = SelectedGenders + document.getElementById('gender_filter_' + GenderId).alt + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		ColorRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedGenders=' + htmlspecialchars(SelectedGenders) + '|';
	if (run_other_filters) {
		onFilterSubmit('gender')
	}
}
function ClearGenderFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	ColorRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}


function PriceRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedPrices = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('price_filter_');
		if (K > -1) {
			var PriceId = ElemId.substring(K + 'price_filter_'.length, ElemId.length);
			//alert("ElemId::"+ElemId+"  K::"+K+"  PriceId::"+PriceId)
			if (document.getElementById('price_filter_' + PriceId).checked) {
				SelectedPrices = SelectedPrices + PriceId + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedPrices=' + htmlspecialchars(SelectedPrices) + '|';
	if (run_other_filters) {
		onFilterSubmit('price')
	}
}
function ClearPriceFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}


function SalePriceRunFilterSpecials(run_other_filters) {
	// alert("SalePriceRunFilterSpecials!!!")
	var theForm = document.getElementById("form_filter");
	var SelectedSale_Prices = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('sale_price_specials_filter_1');
		if (K > -1) {
			var Sale_PriceId = ElemId.substring(K + 'sale_price_specials_filter_1'.length, ElemId.length);
			//alert("ElemId::"+ElemId+"  K::"+K+"  Sale_PriceId::"+Sale_PriceId)
			// alert( '-1::' + document.getElementById('sale_price_specials_filter_' ))

			if (document.getElementById('sale_price_specials_filter_1' ).checked) {
				SelectedSale_Prices = SelectedSale_Prices + /* Sale_PriceId +*/ "1,"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		FinishRunFilter(false)
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedSale_Prices=' + htmlspecialchars(SelectedSale_Prices) + '|';

	if (run_other_filters) {
		onFilterSubmit('sale_price')
	}
}
function ClearSale_PriceFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	FinishRunFilter(false)
	HandRunFilter(false)
	onFilterSubmit('')
}

//////////////////////////////////////
function FinishRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedFinishs = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('finish_filter_');
		if (K > -1) {
			var FinishId = ElemId.substring(K + 'finish_filter_'.length, ElemId.length);
			if (document.getElementById('finish_filter_' + FinishId).checked) {
  			document.getElementById('finish_filter_div_' + FinishId).className += ' check_box_img_div_checked';
				SelectedFinishs = SelectedFinishs + document.getElementById('finish_filter_' + FinishId).alt + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		HandRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedFinishs=' + htmlspecialchars(SelectedFinishs) + '|';

	if (run_other_filters) {
		onFilterSubmit('finish')
	}
}
function ClearFinishFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	HandRunFilter(false)
	onFilterSubmit('')
}


//////////////////////////////////////
function HandRunFilter(run_other_filters) {
	var theForm = document.getElementById("form_filter");
	var SelectedHands = ""
	for (i = 0; i < theForm.elements.length; i++) {
		var ElemId = theForm.elements[i].id
		var K = ElemId.indexOf('hand_filter_');
		if (K > -1) {
			var HandId = ElemId.substring(K + 'hand_filter_'.length, ElemId.length);
			if (document.getElementById('hand_filter_' + HandId).checked) {
				SelectedHands = SelectedHands + HandId + "<?php echo ElemSeparator ?>"
			}
		}
	}
	if (run_other_filters) {
		document.getElementById('checked_filters').value = ''
		BrandRunFilter(false)
		SizeRunFilter(false)
		ColorRunFilter(false)
		GenderRunFilter(false)
		PriceRunFilter(false)
		SalePriceRunFilterSpecials(false);
		FinishRunFilter(false)
	}
	document.getElementById('checked_filters').value = document.getElementById('checked_filters').value + 'SelectedHands=' + htmlspecialchars(SelectedHands) + '|';

	if (run_other_filters) {
		onFilterSubmit('hand')
	}
}
function ClearHandFilters() {
	document.getElementById('checked_filters').value = ''
	BrandRunFilter(false)
	SizeRunFilter(false)
	ColorRunFilter(false)
	GenderRunFilter(false)
	PriceRunFilter(false)
	SalePriceRunFilterSpecials(false);
	FinishRunFilter(false)
	onFilterSubmit('')
}
////////////////

function ClearAllFilters() {
	document.getElementById('checked_filters').value = '';
	onFilterSubmit('')
}


//-->
</script>

<div
	style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; margin-bottom: 10px; margin-left: 0px;  margin-top: 5px;">
	Refine Your Search
</div>
<div
	style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; margin-bottom: 10px; margin-left: 0px;  margin-top: 5px;  text-decoration: underline; ">
	<a onclick="javascript:ClearAllFilters();" style="cursor:pointer;">Clear All Filters</a>
</div>
<div style="clear:both;"></div><!-- sz -->

<!-- Brand Section -->
<?php if ( count($BrandsCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="brand-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleBrandContent();">
				Brand
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-brands">
			<div id="brand-list" class="dimension-list">

      	<?php if ( count($original_SelectedBrandsArray)> 0 ) : ?>
				<div
					style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 0px;">
					<a onclick="javascript:ClearBrandFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">

					<ul>
						<?php $L = count($BrandsCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
                <div id="brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id']?>" class="check_box_img_div">
									<input class="check_box" type="checkbox" id="brand_filter_<?php echo $BrandsCountListing[$I]['brand_id'] ?>"
												 onclick="javascript:BrandRunFilter(true);" <?php echo (in_array($BrandsCountListing[$I]['brand_id'], $original_SelectedBrandsArray, true) ? 'checked' : ''); ?> >
                </div>
									&nbsp;<?php echo $BrandsCountListing[$I]['brand_name'] ?>
									<span class="number-products">(<?php echo $BrandsCountListing[$I]['brands_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>

              <script type="text/javascript" language="JavaScript">
                    if(document.getElementById("brand_filter_<?php echo $BrandsCountListing[$I]['brand_id'] ?>" ).checked==true)
                      {
                        document.getElementById("brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").className += ' check_box_img_div_checked';
                      }
                    jQuery("#brand_filter_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").hover(function(){
                      if (document.getElementById("brand_filter_<?php echo $BrandsCountListing[$I]['brand_id'] ?>" ).checked==true) {
                        document.getElementById("brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").style.backgroundPosition = '0 -45px';
                      } else {
                        document.getElementById("brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").style.backgroundPosition = '0 -15px';
                      }
                    },function(){
                      if (document.getElementById("brand_filter_<?php echo $BrandsCountListing[$I]['brand_id'] ?>" ).checked==true) {
                        document.getElementById("brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").style.backgroundPosition = '0 -30px';
                      } else {
                        document.getElementById("brand_filter_div_<?php echo $BrandsCountListing[$I]['brand_id'] ?>").style.backgroundPosition = '0 0';
                      }
                    });
              </script>

							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Size Section -->
<?php if ( count($SizesCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="size-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleSizeContent();">
				Size
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-sizes">
			<div id="size-list" class="dimension-list">

      	<?php if ( count($original_SelectedSizesArray)> 0 ) : ?>
				<div style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 3px;">
					<a onclick="javascript:ClearSizeFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
				<?php endif; ?>

				<div class="expose js-scroll">
					<ul>

						<?php $L = count($SizesCountListing);for ($I = 0; $I < $L; $I++) : ?>
						<?php if ( !empty($SizesCountListing[$I]['is_used']) and $SizesCountListing[$I]['is_used']== 1 ) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
                <div id="size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>" class="check_box_img_div">
									<input class="check_box" type="checkbox" id="size_filter_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>"
												 onclick="javascript:SizeRunFilter( true );" alt="<?php echo htmlspecialchars($SizesCountListing[$I]['size']) ?>" <?php echo (in_array( $SizesCountListing[$I]['size'], $original_SelectedSizesArray, true) ? 'checked' : ''); ?> >
                </div>
									&nbsp;<?php echo $SizesCountListing[$I]['size'] ?>
									<span class="number-products">(<?php echo $SizesCountListing[$I]['sizes_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>
<script type="text/javascript" language="JavaScript">
      if(document.getElementById("size_filter_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>" ).checked==true)
        {
          document.getElementById("size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#size_filter_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").hover(function(){
        if (document.getElementById("size_filter_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>" ).checked==true) {
          document.getElementById("size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("size_filter_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>" ).checked==true) {
          document.getElementById("size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("size_filter_div_<?php echo ReplaceSpecialChars( html_entity_decode($SizesCountListing[$I]['size'], ENT_QUOTES) ) ?>").style.backgroundPosition = '0 0';
        }
      });
</script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Color Section -->
<?php if ( count($ColorsCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="color-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleColorContent();">
				Color
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-colors">
			<div id="color-list" class="dimension-list">

      	<?php if ( count($original_SelectedColorsArray)> 0 ) : ?>
				<div
					style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 3px;">
					<a onclick="javascript:ClearColorFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">

					<ul>
						<?php $L = count($ColorsCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
<span class="checkbox ir">
                <div id="color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>" class="check_box_img_div">
                  <input class="check_box" type="checkbox" id="color_filter_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>" onclick="javascript:ColorRunFilter(true);"  alt="<?php echo $ColorsCountListing[$I]['color_name'] ?>"  <?php echo (in_array(str_replace(" ","_",$ColorsCountListing[$I]['color_name']), $original_SelectedColorsArray, true) ? 'checked' : ''); ?>>
                </div>
</span>
									<!--  &nbsp;<?php echo $ColorsCountListing[$I]['color_name'] ?>  -->
									<span
										class="swatch swatch-<?php echo strtolower($ColorsCountListing[$I]['color_name']) ?>"></span>&nbsp;&nbsp;<?php echo $ColorsCountListing[$I]['color_name'] ?>&nbsp;
									<span class="number-products">(<?php echo $ColorsCountListing[$I]['colors_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>
<script type="text/javascript" language="JavaScript">
      if(document.getElementById("color_filter_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>" ).checked==true)
        {
          document.getElementById("color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#color_filter_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").hover(function(){
        if (document.getElementById("color_filter_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>" ).checked==true) {
          document.getElementById("color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("color_filter_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>" ).checked==true) {
          document.getElementById("color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("color_filter_div_<?php echo str_replace(" ","_",$ColorsCountListing[$I]['color_name']) ?>").style.backgroundPosition = '0 0';
        }
      });
</script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Gender Section -->
<?php if ( count($GendersCountListing) > 0 ) : ?>
	<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="gender-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleGenderContent();">
				Gender
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-genders">
			<div id="gender-list" class="dimension-list">

      	<?php if ( count($original_SelectedGendersArray)> 0 ) : ?>
				<div
					style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 3px;">
					<a onclick="javascript:ClearGenderFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">

					<ul>

						<?php $L = count($GendersCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<?php if (empty($GendersCountListing[$I]['gender'])) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
                <div id="gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>" class="check_box_img_div">
									<input class="check_box" type="checkbox" id="gender_filter_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>"
												 onclick="javascript:GenderRunFilter(true);"  alt="<?php echo htmlspecialchars($GendersCountListing[$I]['gender']) ?>" <?php echo (in_array($GendersCountListing[$I]['gender'], $original_SelectedGendersArray, true ) ? 'checked' : ''); ?> >
                </div>
									&nbsp;<?php echo GetGenderlabel($GendersCountListing[$I]['gender'] ) ?>
									<span class="number-products">(<?php echo $GendersCountListing[$I]['genders_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>

<script type="text/javascript" language="JavaScript">
      if(document.getElementById("gender_filter_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender']) ?>" ).checked==true)
        {
          document.getElementById("gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#gender_filter_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").hover(function(){
        if (document.getElementById("gender_filter_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>" ).checked==true) {
          document.getElementById("gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("gender_filter_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>" ).checked==true) {
          document.getElementById("gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("gender_filter_div_<?php echo ReplaceSpecialChars( $GendersCountListing[$I]['gender'] ) ?>").style.backgroundPosition = '0 0';
        }
      });
</script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Price Section -->
<?php if ( count($PricesCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="price-toggle-btn" class="collapse" style="cursor:pointer;" onclick="togglePriceContent();">
				Price
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-prices">
			<div id="price-list" class="dimension-list">

      	<?php if ( count($original_SelectedPricesArray)> 0 ) : ?>
				<div
					style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; margin-left: 10px;  margin-top: 5px;  text-decoration: underline;">
					<a onclick="javascript:ClearPriceFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">

					<ul>

						<?php $L = count($PricesCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<?php if (empty($PricesCountListing[$I]['key'])) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
                  <div id="prices_filter_div_<?php echo $PricesCountListing[$I]['key']?>" class="check_box_img_div">
									<input class="check_box" type="checkbox" id="price_filter_<?php echo $PricesCountListing[$I]['key'] ?>"
												 onclick="javascript:PriceRunFilter(true);" <?php echo (in_array($PricesCountListing[$I]['key'], $original_SelectedPricesArray, false) ? 'checked' : ''); ?> >
                </div>
									&nbsp;<?php echo ShowPriceItem($PricesCountListing[$I]);/* echo   ($PricesCountListing[$I]['prior_key'] ) ?>.00-$<?php echo  ( $PricesCountListing[$I]['key'] == 20000 ? "..." : ($PricesCountListing[$I]['key'] - 0.01 ) )*/   ?>
									<span class="number-products">(<?php echo $PricesCountListing[$I]['count'] ?><span
											class="visually-hidden"> products</span>)</span>
								</a>
<script type="text/javascript" language="JavaScript">
      if(document.getElementById("price_filter_<?php echo $PricesCountListing[$I]['key'] ?>" ).checked==true)
        {
          document.getElementById("prices_filter_div_<?php echo $PricesCountListing[$I]['key'] ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#price_filter_<?php echo $PricesCountListing[$I]['key'] ?>").hover(function(){
        if (document.getElementById("price_filter_<?php echo $PricesCountListing[$I]['key'] ?>" ).checked==true) {
          document.getElementById("prices_filter_div_<?php echo $PricesCountListing[$I]['key'] ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("prices_filter_div_<?php echo $PricesCountListing[$I]['key'] ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("price_filter_<?php echo $PricesCountListing[$I]['key'] ?>" ).checked==true) {
          document.getElementById("prices_filter_div_<?php echo $PricesCountListing[$I]['key'] ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("prices_filter_div_<?php echo $PricesCountListing[$I]['key'] ?>").style.backgroundPosition = '0 0';
        }
      });
</script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Specials Section -->
<?php if ( count($Sale_PricesCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="sale_price-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleSale_PriceContent();">
				Specials
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-sale_prices">
			<div id="sale_price-list" class="dimension-list">

      	<?php if ( count($original_SelectedSale_PricesArray)> 0 ) : ?>
				<div style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; margin-left: 10px;  margin-top: 5px;  text-decoration: underline;">
					<a onclick="javascript:ClearSale_PriceFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">

					<ul>

						<?php $L = count($Sale_PricesCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<?php if (floatval($Sale_PricesCountListing[$I]['sale_price']) == 0) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>

                  <div id="specials_filter_div_1" class="check_box_img_div">
									<input class="check_box" type="checkbox" id="sale_price_specials_filter_1"
												 onclick="javascript:SalePriceRunFilterSpecials(true);" <?php echo (  in_array( /*$Sale_PricesCountListing[$I]['sale_price']*/ 1, $original_SelectedSale_PricesArray, false) ? 'checked' : ''); ?> >
                  </div>
									&nbsp;Specials&nbsp;
									<span class="number-products">(<?php echo $Sale_PricesCountListing[$I]['sale_prices_count'] ?><span
											class="visually-hidden"> products</span>)</span>

								</a>
                <script type="text/javascript" language="JavaScript">
                      if(document.getElementById("sale_price_specials_filter_1" ).checked==true)
                        {
                          document.getElementById("specials_filter_div_1").className += ' check_box_img_div_checked';
                        }
                      jQuery("#sale_price_specials_filter_1").hover(function(){
                        if (document.getElementById("sale_price_specials_filter_1" ).checked==true) {
                          document.getElementById("specials_filter_div_1").style.backgroundPosition = '0 -45px';
                        } else {
                          document.getElementById("specials_filter_div_1").style.backgroundPosition = '0 -15px';
                        }
                      },function(){
                        if (document.getElementById("sale_price_specials_filter_1" ).checked==true) {
                          document.getElementById("specials_filter_div_1").style.backgroundPosition = '0 -30px';
                        } else {
                          document.getElementById("specials_filter_div_1").style.backgroundPosition = '0 0';
                        }
                      });
                </script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Finish Section -->
<?php if ( count($FinishsCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="finish-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleFinishContent();">
				Finish
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-finishs">
			<div id="finish-list" class="dimension-list">

      	<?php if ( count($original_SelectedFinishsArray)> 0 ) : ?>
				<div style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 3px;">
					<a onclick="javascript:ClearFinishFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">
					<ul>

						<?php $L = count($FinishsCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<?php if ( empty($FinishsCountListing[$I]['finish']) ) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
<!-- sz test add div around input-->
                <div id="finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] )?>" class="check_box_img_div"><!-- sz test div for new checkbox -->
									<input class="check_box" type="checkbox" id="finish_filter_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>"
												 onclick="javascript:FinishRunFilter(true);" alt="<?php echo $FinishsCountListing[$I]['finish'] ?>"
										<?php echo (in_array( $FinishsCountListing[$I]['finish'], $original_SelectedFinishsArray, true) ? 'checked' : ''); ?> >
                </div>
									&nbsp;<?php echo str_replace(" ","_",$FinishsCountListing[$I]['finish']) ?>
									<span class="number-products">(<?php echo $FinishsCountListing[$I]['finishs_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>
<script type="text/javascript" language="JavaScript">
		if(document.getElementById("finish_filter_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>" ).checked==true)
        {
          document.getElementById("finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#finish_filter_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").hover(function(){
        if (document.getElementById("finish_filter_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>" ).checked==true) {
          document.getElementById("finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("finish_filter_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>" ).checked==true) {
          document.getElementById("finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("finish_filter_div_<?php echo ReplaceSpecialChars( $FinishsCountListing[$I]['finish'] ) ?>").style.backgroundPosition = '0 0';
        }
      });

</script>
							</li>
						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>

<!-- Hand Section -->
<?php if ( count($HandsCountListing) > 0 ) : ?>
<div id="dimensions">
	<div class="dimension-group">
		<h3>
			<a id="hand-toggle-btn" class="collapse" style="cursor:pointer;" onclick="toggleHandContent();">
				Hand
				<span class="toggle-button ir"></span>
			</a>
		</h3>

		<div class="dimension-list-container dimension-hands">
			<div id="hand-list" class="dimension-list">

      	<?php if ( count($original_SelectedHandsArray)> 0 ) : ?>
				<div style="background: none repeat scroll 0 0 white; font-size: 13px; font-weight: normal; position: relative; display:block; width:186px; color: blue; text-decoration: underline; margin:5px 0px 3px 3px;">
					<a onclick="javascript:ClearHandFilters();" style="cursor:pointer;">Clear Filter</a>
				</div>
      	<?php endif; ?>

				<div class="expose js-scroll">
					<ul>

						<?php $L = count($HandsCountListing);for ($I = 0; $I < $L; $I++) : ?>
							<?php if ( empty($HandsCountListing[$I]['hand']) ) continue; ?>
							<li class="<?php echo ($I == 0 ? 'first' : (($I == $L - 1) ? "last" : "")) ?>">
								<a>
<!-- sz test add div around input-->
                <div id="hand_filter_div_<?php echo $HandsCountListing[$I]['hand']?>" class="check_box_img_div"><!-- sz test div for new checkbox -->
									<input class="check_box" type="checkbox" id="hand_filter_<?php echo $HandsCountListing[$I]['hand'] ?>"
												 onclick="javascript:HandRunFilter(true);" <?php echo (in_array($HandsCountListing[$I]['hand'], $original_SelectedHandsArray, true) ? 'checked' : ''); ?> >
                </div>

									&nbsp;<?php echo $HandsCountListing[$I]['hand'] ?>
									<span class="number-products">(<?php echo $HandsCountListing[$I]['hands_count'] ?><span class="visually-hidden"> products</span>)</span>
								</a>

<script type="text/javascript" language="JavaScript">
      if(document.getElementById("hand_filter_<?php echo $HandsCountListing[$I]['hand'] ?>" ).checked==true)
        {
          document.getElementById("hand_filter_div_<?php echo $HandsCountListing[$I]['hand'] ?>").className += ' check_box_img_div_checked';
        }
      jQuery("#hand_filter_<?php echo $HandsCountListing[$I]['hand'] ?>").hover(function(){
        if (document.getElementById("hand_filter_<?php echo $HandsCountListing[$I]['hand'] ?>" ).checked==true) {
          document.getElementById("hand_filter_div_<?php echo $HandsCountListing[$I]['hand'] ?>").style.backgroundPosition = '0 -45px';
        } else {
          document.getElementById("hand_filter_div_<?php echo $HandsCountListing[$I]['hand'] ?>").style.backgroundPosition = '0 -15px';
        }
      },function(){
        if (document.getElementById("hand_filter_<?php echo $HandsCountListing[$I]['hand'] ?>" ).checked==true) {
          document.getElementById("hand_filter_div_<?php echo $HandsCountListing[$I]['hand'] ?>").style.backgroundPosition = '0 -30px';
        } else {
          document.getElementById("hand_filter_div_<?php echo $HandsCountListing[$I]['hand'] ?>").style.backgroundPosition = '0 0';
        }
      });
</script>
							</li>

						<?php endfor; ?>
					</ul>

				</div>
				<!-- .expose -->

			</div>
			<!-- .dimension-list -->
		</div>
		<!-- .dimension-list-container -->
	</div>
	<!-- close dimension group sz -->
</div> <!-- close dimensions sz-->
<?php endif; ?>