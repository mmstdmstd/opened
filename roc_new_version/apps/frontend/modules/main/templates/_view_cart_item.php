<?php
  if ( empty($Item['sku']) ) return '';
  $sku= $Item['sku'];
  $badges_sku_list = sfConfig::get('app_application_badges_sku_list');
  $is_badge_item= !empty($badges_sku_list[strtolower($sku)]);
//Util::deb( $sku, '$sku::' );
//Util::deb( $badges_sku_list, '$badges_sku_list::' );
  //Util::deb( $is_badge_item, '$is_badge_item::' );
  if ( $is_badge_item ) {
		$GuestUserArray = Cart::getGuestUser();
		//Util::deb($GuestUserArray, '$GuestUserArray::');
		$lLoggedUser = sfGuardUserPeer::retrieveByUsername($GuestUserArray['LoggedUserEmail']);
		//Util::deb($lLoggedUser, '$lLoggedUser::');
		if ( !empty($lLoggedUser) ) {
		  $LastUserBadgeData= UserBadgeDataPeer::getUserBadgeDataByUserId($lLoggedUser->getId());
			//Util::deb($LastUserBadgeData, '$LastUserBadgeData::');
			$UserBadgeDataArray= unserialize($LastUserBadgeData->getData());
			//Util::deb($UserBadgeDataArray, '$UserBadgeDataArray::');
		}

	}
  $product_quantity= 0;
  if( !empty($Item['product_quantity']) ) {
    $product_quantity= $Item['product_quantity'];
  }
  $Product= InventoryItemPeer::getSimilarInventoryItem($sku);
  if ( empty($Product) ) return '';
  $ShipWeight= $Product->getShipWeight();
  if ( empty($ShipWeight) ) {
    if ( $Product->getRootitem() != $sku ) {
      $lParentProduct= InventoryItemPeer::getSimilarInventoryItem( $Product->getRootitem() );
      if( !empty($lParentProduct) ) {
        $ShipWeight= $lParentProduct->getShipWeight();
      }

    }
  }
  $Title= $Product->getTitleWithRSymbol();

  $ProductPrice= Util::getDigitMoney( $Product->getStdUnitPrice(),'Money');

  if ( !empty($Item['price_type']) and $Item['price_type'] == 'special' ) {
    $ProductPrice= Util::getDigitMoney( $Product->getClearance_Or_SalePrice(),'Money');
  }

  if ( !empty($Item['price_type']) and $Item['price_type'] == 'selected_price' ) {
    $ProductPrice= Util::getDigitMoney( $Item['selected_price'],'Money' );
  }

  if ( !empty($Item['price_type']) and $Item['price_type'] == 'badge_price' ) {
	  $ProductPrice= Util::getDigitMoney( $Item['selected_price'],'Money' );
  }

  $Color= $Product->getColor();
  $Size= $Product->getSize();

  $ThumbnailImageFile=  AppUtils::getThumbnailImageBySku( $sku, false );
  if ( empty($ThumbnailImageFile) ) {
  	$ThumbnailImageFile=  AppUtils::getResizedImageBySku( $sku, false, false, true );
  }
  $ImageHeight= 75;
  $ImageWidth= 75;
  $HostForImage= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() );
?>



      <div id="content_left_div">
        <div id="product_photo_div">

          <?php $ThumbnailImageFullUrl= '';
          if ( !empty($ThumbnailImageFile) and !is_array($ThumbnailImageFile) ) :
            $ThumbnailImageUrl= 'uploads'.$ThumbnailImageFile;
            $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;
            if (file_exists($ThumbnailImageFullUrl)) { ?>
            <a href="<?php echo $ThumbnailImageFullUrl ?>">
              <?php echo image_tag( $ThumbnailImageUrl,array('alt'=>"", "height"=>"75", "width"=>"75") ); ?>
            </a>
            <?php } ?>
          <?php endif; ?>

          <?php //if ( $ThumbnailImageFile and is_array($ThumbnailImageFile) /* and !empty($ThumbnailImageFile['ImageFileName'])*/ ) :
          //Util::deb(  ' -33::' );
          if ($ThumbnailImageFile and is_array($ThumbnailImageFile)) {
            if ( !empty($ThumbnailImageFile['Width']) and !empty($ThumbnailImageFile['Height']) ) {
            	$ImageWidth= $ThumbnailImageFile['Width'];
                $ImageHeight= $ThumbnailImageFile['Height'];
            }
            $ThumbnailImageUrl= 'uploads'.DIRECTORY_SEPARATOR.$ThumbnailImageFile['ImageFileName_encoded'];
            $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . $ThumbnailImageUrl;
            $ThumbnailImageFullFilePath= sfConfig::get('sf_upload_dir') . DIRECTORY_SEPARATOR . $ThumbnailImageFile['ImageFileName'];
            if ( empty($ThumbnailImageFile['ImageFileName']) or ( !file_exists($ThumbnailImageFullFilePath) and !is_dir($ThumbnailImageFullFilePath) ) ) {
       	      $ThumbnailImageFullUrl= AppUtils::getHostForImage( sfContext::getInstance()->getConfiguration() ) . 'images/noimage79.png' ;
            }
          }
           $ThumbnailImageFullUrl=  str_replace( "+", ' ', $ThumbnailImageFullUrl);
           ?>

            <table cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
                  <a href="<?php echo url_for(  "@product_details?sku=" . urlencode($sku) ) ?>" >
                    <!--<img height="<?php //echo $ImageWidth ?>" width="<?php //echo $ImageHeight ?>" alt="tt" src="<?php //echo $ThumbnailImageFullUrl ?> " /><br />-->
                    <img height="75" width="75" alt="tt" src="<?php echo $ThumbnailImageFullUrl ?> " /><br />
                  </a>
                </td>
                <td>
                  <b style="text-transform:capitalize;"><?php echo htmlspecialchars_decode($Title) ?></b>
                  <p style="color:gray"><small>Item#</small>:&nbsp;<?php echo $sku ?></p>

                  <?php if ( !empty($Color) ) : ?>
                    <p><small>Color</small>:&nbsp;<?php echo $Color ?></p>
                  <?php endif; ?>

                  <?php if ( !empty($Size) ) : ?>
                    <p><small>Size</small>:&nbsp;<?php echo $Size ?></p>
                  <?php endif; ?>


									<?php if ( $is_badge_item ) : ?>
										<?php if (!empty($UserBadgeDataArray['badge'])) : ?>
  										<p>		<small>Badge</small>:&nbsp;<?php echo $UserBadgeDataArray['badge'] ?>		</p>
									  <?php endif; ?>
									  <?php if (!empty($UserBadgeDataArray['quick_ship'])) : ?>
										<p>		<small>Quick Ship</small>:&nbsp;<?php echo $UserBadgeDataArray['quick_ship'] ?>		</p>
									  <?php endif; ?>
									  <?php if (!empty($UserBadgeDataArray['finish'])) : ?>
									    <p>		<small>Finish</small>:&nbsp;<?php echo $UserBadgeDataArray['finish'] ?>		</p>
									  <?php endif; ?>
									  <?php if (!empty($UserBadgeDataArray['enamel_type'])) : ?>
									    <p>		<small>Enamel Type</small>:&nbsp;<?php echo $UserBadgeDataArray['enamel_type'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['strike_solid'])) : ?>
											<p>		<small>Strike Solid</small>:&nbsp;<?php echo $UserBadgeDataArray['strike_solid'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['lettering_font'])) : ?>
											<p>		<small>Lettering Font</small>:&nbsp;<?php echo $UserBadgeDataArray['lettering_font'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['panel2'])) : ?>
											<p>		<small>Panel 2</small>:&nbsp;<?php echo $UserBadgeDataArray['panel2'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['panel3'])) : ?>
											<p>		<small>Panel 3</small>:&nbsp;<?php echo $UserBadgeDataArray['panel3'] ?>		</p>
										<?php endif; ?>

										<?php if (!empty($UserBadgeDataArray['panel5'])) : ?>
											<p>		<small>Panel 5</small>:&nbsp;<?php echo $UserBadgeDataArray['panel5'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['seal_style'])) : ?>
											<p>		<small>Seal Style</small>:&nbsp;<?php echo $UserBadgeDataArray['seal_style'] ?>		</p>
										<?php endif; ?>

										<?php if (!empty($UserBadgeDataArray['panel6'])) : ?>
											<p>		<small>Panel 6</small>:&nbsp;<?php echo $UserBadgeDataArray['panel6'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['badge_attachment'])) : ?>
											<p>		<small>Badge Attachment</small>:&nbsp;<?php echo $UserBadgeDataArray['badge_attachment'] ?>		</p>
										<?php endif; ?>

										<?php if (!empty($UserBadgeDataArray['text_separator'])) : ?>
											<p>		<small>Text Separator</small>:&nbsp;<?php echo $UserBadgeDataArray['text_separator'] ?>		</p>
										<?php endif; ?>
										<?php if (!empty($UserBadgeDataArray['selected_emboss'])) : ?>
											<p>		<small>Selected Emboss</small>:&nbsp;<?php echo $UserBadgeDataArray['selected_emboss'] ?>		</p>
										<?php endif; ?>

									<?php endif; ?>

                  <?php // if ( !empty($ProductPrice) and !$is_badge_item ) : ?>
                    <p>
                      <b><small>Price</small>:&nbsp;<?php echo $ProductPrice ?></b>
                      <!-- <small>Ship Weight:&nbsp;</small><?php //echo (empty($ShipWeight)?'0':$ShipWeight) ?>
                      <?php //echo ($product_quantity > 1 ? ' -> '.($product_quantity*$ShipWeight) : '') ?>  -->
                    </p>
                  <?php //endif; ?>

                  <span>Qty:</span>
                  <input type="input" id="cart_product_quantity_<?php echo $sku ?>" name="cart_product_quantity_<?php echo $sku ?>" value="<?php echo $product_quantity ?>" maxlength="2" size="2"/>&nbsp;

                  <input class="update-button" value=" " onclick="javascript:UpdateCartProductQuantity( '<?php echo $sku ?>' )" type="button">&nbsp;
                  <input type="button" class="remove-button" alt="Remove" value=" " onclick="javascript:DeleteCartProduct( '<?php echo $sku ?>' )" >

                </td>
              </tr>

            </table>

          <?php //endif; ?>

        </div>
      </div>

