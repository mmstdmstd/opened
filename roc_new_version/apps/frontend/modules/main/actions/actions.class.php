<?php

include_once('lib/cart.php');

/**
 * main actions.
 *
 * @package    sf_sandbox
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
if (!function_exists("deb")) {

	function deb($S, $Mark = '', $ToFile = "/_SymfonyProjects/MagneticImpact/log/__deb.txt")
	{
		$fd = fopen($ToFile, "a+");
		if (!empty($Mark)) {
			fwrite($fd, $Mark . " : ");
		}
		fwrite($fd, print_r($S, true) . "\r\r");
		fclose($fd);
	}

}

class mainActions extends sfActions
{

	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */


	public function executeRun_badge_product(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->addJavascript('/js/jquery/jquery.js', 'last');
		try {
			$this->sku = $request->getParameter('sku');
			$this->qty = $request->getParameter('qty');
			$this->done = $request->getParameter('done');
			//Util::deb($this->qty, "this->qty::");
			//Util::deb($this->done, '$this->done::');
			//die("+00");
			$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
			if ((string)$this->done == 1 and (int)$this->qty > 0) {
				// UpdateUsersCart($sku, $product_quantity, $price_type = '', $selected_price = '') {
				$price_type = 'badge_price';
				$selected_price = $request->getParameter('BASE_PRICE');
				$Res = Cart::UpdateUsersCart($this->sku, $this->qty, $price_type, $selected_price);
				//die("+1");

				$GuestUserArray = Cart::getGuestUser();
				$lLoggedUser = sfGuardUserPeer::retrieveByUsername($GuestUserArray['LoggedUserEmail']);
				if (!empty($lLoggedUser)) {
					$lUserBadgeData = new UserBadgeData();
					$lUserBadgeData->setUserId($lLoggedUser->getid());
					$lUserBadgeData->setData(serialize($_POST));
					$lUserBadgeData->save();
					$this->redirect('@product_listings?rows_in_pager=20&badge_sku=' . $this->sku . '&badge_added=1');
					// .'/badge_sku/'.$sku.'/badge_added/1'
					// http://local-roc.com//frontend_dev.php/main/product_listings/page/1/sorting/-/rows_in_pager/20/badge_sku/VHBDAB13/badge_added/1
				}
			}
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


//////////////// BEGIN /////////////////////
	// product_listings_sidebar_test
	public function executeProduct_listings(sfWebRequest $request)
	{
		try {
			$this->input_search = '';
			$this->select_category = '';
			$this->select_subcategory = '';
			$this->select_brand_id = '';

			$this->filter_pager_count = '';
			$this->rows_in_pager = '';
			$this->HeaderTitle = '';
			$this->message_type = $request->getParameter('message_type');
			$this->checked_action = $request->getParameter('checked_action');
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Product Listings");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');
			define("ElemSeparator", "^");
//			  Util::deb($_POST ,'$_POST ::');
			// Util::deb($_REQUEST ,'$_REQUEST ::');

			$this->SelectedBrandsArray = array();
			$this->checked_filters = '';
			if (!empty($_REQUEST['checked_filters'])) { // SelectedBrands=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedBrands') {
						$this->SelectedBrandsArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			$this->extended_parameters = array('selected_brands' => array(), 'selected_sizes' => array());
			foreach ($this->SelectedBrandsArray as $selected_brand) {
				if (empty($selected_brand)) continue;
				$this->extended_parameters['selected_brands'][] = $selected_brand;
			}
			//Util::deb($this->extended_parameters['selected_brands'], '$this->extended_parameters[selected_brands]::');


			$this->SelectedSizesArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedSizes=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedSizes') {
						$this->SelectedSizesArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
						//Util::deb($this->SelectedSizesArray, '$this->SelectedSizesArray::');
					}
				}
			}
			foreach ($this->SelectedSizesArray as $selected_size) {
				if (empty($selected_size)) continue;
				$selected_size = htmlspecialchars_decode($selected_size);
				$this->extended_parameters['selected_sizes'][] = $selected_size;
			}
			// Util::deb($this->extended_parameters['selected_sizes'], '$this->extended_parameters[selected_sizes]::');


			$this->SelectedColorsArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedColors=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				//Util::deb($A, '$A::');
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					//Util::deb($A_1, '$A_1::');
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedColors') {
						$this->SelectedColorsArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			//Util::deb( $this->SelectedColorsArray, '$this->SelectedColorsArray::');
			foreach ($this->SelectedColorsArray as $selected_color) {
				if (empty($selected_color)) continue;
				$selected_color = htmlspecialchars_decode($selected_color);
				$this->extended_parameters['selected_colors'][] = $selected_color;
			}
			//Util::deb($this->extended_parameters['selected_colors'], '$this->extended_parameters[selected_colors]::');


			$this->SelectedGendersArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedGenders=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedGenders') {
						$this->SelectedGendersArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			foreach ($this->SelectedGendersArray as $selected_gender) {
				if (empty($selected_gender)) continue;
				$this->extended_parameters['selected_genders'][] = $selected_gender;
			}


			$this->SelectedPricesArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedPrices=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedPrices') {
						$this->SelectedPricesArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			foreach ($this->SelectedPricesArray as $selected_price) {
				if (empty($selected_price)) continue;
				$this->extended_parameters['selected_prices'][] = $selected_price;
			}


			$this->SelectedSale_PricesArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedSale_Prices=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedSale_Prices') {
						$this->SelectedSale_PricesArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			foreach ($this->SelectedSale_PricesArray as $selected_sale_price) {
				if (empty($selected_sale_price)) continue;
				$this->extended_parameters['selected_sale_prices'][] = $selected_sale_price;
			}


			$this->SelectedFinishsArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedFinishs=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				//Util::deb($A ,'$A ::');
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					//Util::deb($A_1 ,'$A_1 ::');
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedFinishs') {
						$this->SelectedFinishsArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			//Util::deb($this->SelectedFinishsArray, '$this->SelectedFinishsArray ::');
			foreach ($this->SelectedFinishsArray as $selected_finish) {
				if (empty($selected_finish)) continue;
				$selected_finish = htmlspecialchars_decode($selected_finish);
				$this->extended_parameters['selected_finishs'][] = $selected_finish;
			}


			$this->SelectedHandsArray = array();
			if (!empty($_REQUEST['checked_filters'])) { // SelectedHands=31,;
				$this->checked_filters = $_REQUEST['checked_filters'];
				$checked_filters = $_REQUEST['checked_filters'];
				$A = preg_split('/\|/', $checked_filters);
				//Util::deb($A ,'$A ::');
				foreach ($A as $key => $array_value) {
					if (empty($array_value)) continue;
					$A_1 = preg_split('/=/', $array_value);
					//Util::deb($A_1 ,'$A_1 ::');
					if (!empty($A_1[1]) and $A_1[0] == 'SelectedHands') {
						$this->SelectedHandsArray = preg_split('/\\' . ElemSeparator . '/', $A_1[1]);
					}
				}
			}
			//Util::deb($this->SelectedHandsArray, '$this->SelectedHandsArray ::');
			foreach ($this->SelectedHandsArray as $selected_hand) {
				if (empty($selected_hand)) continue;
				$selected_hand = htmlspecialchars_decode($selected_hand);
				$this->extended_parameters['selected_hands'][] = $selected_hand;
			}


			if ($request->isMethod('post')) {
				if (!empty($_REQUEST['input_search'])) {
					$this->input_search = $_REQUEST['input_search'];
					$this->input_search = trim($this->input_search);
					if ($this->input_search == '-')
						$this->input_search = '';
				}
				if (!empty($_REQUEST['select_category'])) {
					$this->select_category = $_REQUEST['select_category'];
					if ($this->select_category == '-')
						$this->select_category = '';
				}

				if (!empty($_REQUEST['select_subcategory'])) {
					$this->select_subcategory = $_REQUEST['select_subcategory'];
					if ($this->select_subcategory == '-')
						$this->select_subcategory = '';
				}


				if (!empty($_REQUEST['select_brand_id'])) {
					$this->select_brand_id = $_REQUEST['select_brand_id'];
					if ($this->select_brand_id == '-')
						$this->select_brand_id = '';
				}

				if (!empty($_REQUEST['filter_pager_count'])) {
					if (strlen($_REQUEST['filter_pager_count']) > 0) {
						$this->filter_pager_count = $_REQUEST['filter_pager_count'];
						if ($this->filter_pager_count == '-')
							$this->filter_pager_count = '';
					}
				}
				if (!empty($_REQUEST['rows_in_pager'])) {
					if (strlen($_REQUEST['rows_in_pager']) > 0) {
						$this->rows_in_pager = $_REQUEST['rows_in_pager'];
						if ($this->rows_in_pager == '-')
							$this->rows_in_pager = '';
					}
				}
			} else {
				$this->input_search = $request->getParameter('input_search');
				$this->input_search = trim($this->input_search);
				if ($this->input_search == '-')
					$this->input_search = '';

				$this->select_category = $request->getParameter('select_category');
				if ($this->select_category == '-')
					$this->select_category = '';
				$this->select_subcategory = $request->getParameter('select_subcategory');
				if ($this->select_subcategory == '-')
					$this->select_subcategory = '';

				$this->select_brand_id = $request->getParameter('select_brand_id');
				if ($this->select_brand_id == '-')
					$this->select_brand_id = '';

				$this->filter_pager_count = $request->getParameter('filter_pager_count');
				if ($this->filter_pager_count == '-')
					$this->filter_pager_count = '';
				$this->rows_in_pager = $request->getParameter('rows_in_pager');
				if ($this->rows_in_pager == '-')
					$this->rows_in_pager = '';
			}

			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			if (empty($this->sorting))
				$this->sorting = '-';

			if (empty($this->rows_in_pager) and !is_integer($this->rows_in_pager)) {
				$this->rows_in_pager = (int)sfConfig::get('app_application_product_listings_rows_in_pager');
			}

			if (empty($this->select_category) and !empty($this->select_subcategory)) {
				$lSimilarInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategoryBySubcategory($this->select_subcategory);
				if (!empty($lSimilarInventoryCategory)) {
					$this->select_category = $lSimilarInventoryCategory->getCategory();
					//$this->subcategory = $lSimilarInventoryCategory->getSubcategory();
				}
			}
			// Util::deb($this->select_subcategory, '$this->select_subcategory::');
			// '001BA74Z0'
			/*  public static function getInventoryItems( $page=1, $ReturnPager=true, $ReturnCount= false, $rows_in_pager='', $filter_matrix='', $filter_rootitem='',
				$filter_title='', $ExtendedSearch= false, $filter_sku='', $filter_category='', $filter_subcategory='', $filter_brand_id='', $filter_qty_on_hand_min='',  $filter_qty_on_hand_max='',  $filter_std_unit_price_min='' , $filter_std_unit_price_max='', $Sorting= '', '' ) { */
			$filter_matrix = 1;
			$filter_rootitem = '';
			// public static function getInventoryItems( $page=1, $ReturnPager=true, $ReturnCount= false, $rows_in_pager='', $filter_matrix='', $filter_rootitem='',
			// $filter_title='', $ExtendedSearch= false, $filter_sku='', $filter_category='', $filter_subcategory='', $filter_brand_id='', $filter_qty_on_hand_min='',
			//    $filter_qty_on_hand_max='',  $filter_std_unit_price_min='' , $filter_std_unit_price_max='', $Sorting= '', $filter_sale_method='' )
			// Util::deb( $this->extended_parameters, '$this->extended_parameters::');
			$HaveParameters= false;
			if ( (  !empty($this->extended_parameters['selected_brands']) and count($this->extended_parameters['selected_brands']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_sizes']) and count($this->extended_parameters['selected_sizes']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_colors']) and count($this->extended_parameters['selected_colors']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_genders']) and count($this->extended_parameters['selected_genders']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_prices']) and count($this->extended_parameters['selected_prices']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_finishs']) and count($this->extended_parameters['selected_finishs']) > 0 ) OR
				(  !empty($this->extended_parameters['selected_hands']) and count($this->extended_parameters['selected_hands']) > 0 )
			) {
				$HaveParameters= true;
			}
			//Util::deb($HaveParameters, '$HaveParameters::');
			if ( $HaveParameters /* $this->extended_parameters  )*/ ) { //There are checked parameters
				//Util::deb($_REQUEST['checked_filters'], 'NOT EMPTY $_REQUEST[checked_filters]::');
				$unique_session_id = session_id() . '_' . time();
				// $UniqueSkusList = array();
				$InventoryItemsWithRootList = InventoryItemPeer::getInventory_Items_Left_sidebar_test( 1, true, false, 100000 /*$this->rows_in_pager*/, /*$filter_matrix*/false, $filter_rootitem, $this->input_search, true, '', $this->select_category,
					$this->select_subcategory, $this->select_brand_id, '', '', '', '', $this->sorting, '', $this->extended_parameters );
				//Util::deb(count($InventoryItemsWithRootList), 'count($InventoryItemsList)::');
				//die("END+1 ");
				$ParentItemsArray= array();
				foreach($InventoryItemsWithRootList as $lInventoryItemsWithRoot) {
					$Rootitem= $lInventoryItemsWithRoot->getRootitem();
					if ( !empty($Rootitem) and !in_array( $Rootitem, $ParentItemsArray ) ) {
						$ParentItemsArray[]= $Rootitem;
					}
				}
				// Util::deb($ParentItemsArray, '$ParentItemsArray::');
				$this->extended_parameters['parent_items_array']= $ParentItemsArray;
		//die("AFTER");

				$InventoryItemsList = InventoryItemPeer::getInventory_Items_Left_sidebar_test(1, true, false, 100000 /*$this->rows_in_pager*/, $filter_matrix, $filter_rootitem, $this->input_search, true, '', $this->select_category,
					$this->select_subcategory, $this->select_brand_id, '', '', '', '', $this->sorting, '', $this->extended_parameters);
				// Util::deb(count($InventoryItemsList), 'count($InventoryItemsList)::');
				// die("UUU");
				foreach ($InventoryItemsList as $lInventoryItem) {
					$lUniquesSkuList = new UniquesSkuList();
					$lUniquesSkuList->setUniqueSessionId($unique_session_id);
					if (trim($lInventoryItem->getRootItem()) != '') {
						$lUniquesSkuList->setSku($lInventoryItem->getRootItem());
					} else {
						$lUniquesSkuList->setSku($lInventoryItem->getSku());
					}
					$lUniquesSkuList->save();
				}
				$this->InventoryItemsPager = InventoryItemPeer::getInventory_Items_Left_sidebar_ByUniquesSkuList($this->page, true, false, $unique_session_id, $this->rows_in_pager, $this->sorting);
				/*$UniquesSkuList= UniquesSkuListPeer::getUniquesSkuList( '',false,false, $unique_session_id ) ;
				foreach( $UniquesSkuList as $lUniquesSku ) {
					$lUniquesSku->delete();
				} */

			} else { //no checked parameters
				//Util::deb($_REQUEST, '$Have NO Parameters:: EMPTY $_REQUEST::');
				//Util::deb($this->extended_parameters, '$this->extended_parameters::');
				$this->extended_parameters['select_only_rootitems'] = 1;
				$this->InventoryItemsPager = InventoryItemPeer::getInventory_Items_Left_sidebar_test($this->page, true, false, $this->rows_in_pager, $filter_matrix, $filter_rootitem, $this->input_search, true, '', $this->select_category,
					$this->select_subcategory, $this->select_brand_id, '', '', '', '', $this->sorting, '', $this->extended_parameters);

				UniquesSkuListPeer::DeleteSimilarUniquesBySeconds();
			}
			/*echo '<pre>:: ';
			print_r($this->InventoryItemsPager->getNbResults());
			echo ' ;;</pre>';  */
			//	exit;
			$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	/////////////////////// END //////////////////////////
	public function executeIndex(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->setTitle("Ray O'Herron Co.");
		$response->addJavascript('/js/mootools-1.js', 'last');
		$response->addJavascript('/js/jquery/jquery_slides.js', 'last');
		$response->addJavascript('/js/slides.js', 'last');
	}

	public function executeIndex_new(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->setTitle("Ray O'Herron Co.");
		$response->addJavascript('/js/mootools-1.js', 'last');
		$response->addJavascript('/js/jquery/jquery_slides.js', 'last');
		$response->addJavascript('/js/slides.js', 'last');
	}

	public function executeIndex_new2(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->setTitle("Ray O'Herron Co.");
		$response->addJavascript('/js/mootools-1.js', 'last');
		$response->addJavascript('/js/jquery/jquery_slides.js', 'last');
		$response->addJavascript('/js/slides.js', 'last');
	}

	public function executeProduct_listings_old_version_without_sidebar(sfWebRequest $request)
	{
		try {
			$this->input_search = '';
			$this->select_category = '';
			$this->select_subcategory = '';
			$this->select_brand_id = '';

			$this->filter_pager_count = '';
			$this->rows_in_pager = '';
			$this->HeaderTitle = '';
			$this->checked_filters = '';

			/*    Util::deb( $_POST, '$_POST::' );
				if ( !empty( $_POST['textarea_sizes'] ) ) {
				$A= preg_split("/\r/",$_POST['textarea_sizes']);
				$ResStr='';
				foreach( $A as $Val ) {
				if ( trim($Val)== '' ) continue;
				$ResStr.="'" . addslashes( trim($Val) ) . "', ";
				}
				}
				print_r($ResStr);     die(-1); */
			$this->message_type = $request->getParameter('message_type');
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Product Listings");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');

			if ($request->isMethod('post')) {
				if (!empty($_REQUEST['input_search'])) {
					$this->input_search = $_REQUEST['input_search'];
					$this->input_search = trim($this->input_search);
					if ($this->input_search == '-')
						$this->input_search = '';
				}
				if (!empty($_REQUEST['select_category'])) {
					$this->select_category = $_REQUEST['select_category'];
					if ($this->select_category == '-')
						$this->select_category = '';
				}

				if (!empty($_REQUEST['select_subcategory'])) {
					$this->select_subcategory = $_REQUEST['select_subcategory'];
					if ($this->select_subcategory == '-')
						$this->select_subcategory = '';
				}


				if (!empty($_REQUEST['select_brand_id'])) {
					$this->select_brand_id = $_REQUEST['select_brand_id'];
					if ($this->select_brand_id == '-')
						$this->select_brand_id = '';
				}

				if (!empty($_REQUEST['filter_pager_count'])) {
					if (strlen($_REQUEST['filter_pager_count']) > 0) {
						$this->filter_pager_count = $_REQUEST['filter_pager_count'];
						if ($this->filter_pager_count == '-')
							$this->filter_pager_count = '';
					}
				}
				if (!empty($_REQUEST['rows_in_pager'])) {
					if (strlen($_REQUEST['rows_in_pager']) > 0) {
						$this->rows_in_pager = $_REQUEST['rows_in_pager'];
						if ($this->rows_in_pager == '-')
							$this->rows_in_pager = '';
					}
				}
			} else {
				$this->input_search = $request->getParameter('input_search');
				$this->input_search = trim($this->input_search);
				if ($this->input_search == '-')
					$this->input_search = '';

				$this->select_category = $request->getParameter('select_category');
				if ($this->select_category == '-')
					$this->select_category = '';
				$this->select_subcategory = $request->getParameter('select_subcategory');
				if ($this->select_subcategory == '-')
					$this->select_subcategory = '';

				$this->select_brand_id = $request->getParameter('select_brand_id');
				if ($this->select_brand_id == '-')
					$this->select_brand_id = '';

				$this->filter_pager_count = $request->getParameter('filter_pager_count');
				if ($this->filter_pager_count == '-')
					$this->filter_pager_count = '';
				$this->rows_in_pager = $request->getParameter('rows_in_pager');
				if ($this->rows_in_pager == '-')
					$this->rows_in_pager = '';
			}

			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			if (empty($this->sorting))
				$this->sorting = '-';

			if (empty($this->rows_in_pager) and !is_integer($this->rows_in_pager)) {
				$this->rows_in_pager = (int)sfConfig::get('app_application_product_listings_rows_in_pager');
			}

			if (empty($this->select_category) and !empty($this->select_subcategory)) {
				$lSimilarInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategoryBySubcategory($this->select_subcategory);
				if (!empty($lSimilarInventoryCategory)) {
					$this->select_category = $lSimilarInventoryCategory->getCategory();
					//$this->subcategory = $lSimilarInventoryCategory->getSubcategory();
				}
			}
			// '001BA74Z0'
			/*  public static function getInventoryItems( $page=1, $ReturnPager=true, $ReturnCount= false, $rows_in_pager='', $filter_matrix='', $filter_rootitem='',
				$filter_title='', $ExtendedSearch= false, $filter_sku='', $filter_category='', $filter_subcategory='', $filter_brand_id='', $filter_qty_on_hand_min='',  $filter_qty_on_hand_max='',  $filter_std_unit_price_min='' , $filter_std_unit_price_max='', $Sorting= '', '' ) { */
			$filter_matrix = 1;
			$filter_rootitem = '';
			$this->InventoryItemsPager = InventoryItemPeer::getInventoryItems($this->page, true, false, $this->rows_in_pager, $filter_matrix, $filter_rootitem, $this->input_search, true, '', $this->select_category, $this->select_subcategory, $this->select_brand_id, '', '', '', '', $this->sorting, '');
			/* echo '<pre>';
				print_r($this->InventoryItemsPager);
				echo '</pre>';
				exit; */
			$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeProduct_details(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Product View");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');

			$this->sku = $request->getParameter('sku');
			$this->sku = preg_replace("/__plus/", '+', $this->sku);

			$badges_sku_list = sfConfig::get('app_application_badges_sku_list');
			$is_badge_item = !empty($badges_sku_list[strtolower($this->sku)]);
			//Util::deb($is_badge_item, '$is_badge_item::');
			/*			if( $is_badge_item ) {
							$this->redirect('@run_badge_product?sku='.$this->sku);
							return;
						} */


			$this->Product = InventoryItemPeer::getSimilarInventoryItem($this->sku, true);
			$this->message_type = $request->getParameter('message_type');
			$this->checked_filters = $request->getParameter('checked_filters');
			/* echo '<pre>';
				print_r($this->Product);
				echo '</pre>';
				exit; */
			if (empty($this->Product))
				$this->redirect('@homepage');
			$this->HeaderTitle = 'Product: ' . $this->Product->getTitle();
			$this->Matrix = $this->Product->getMatrix();
			/* echo '<pre>';
				print_r($this->Matrix);
				echo '</pre>';
				exit; */
			$this->RootItem = $this->Product->getRootItem();
			// Util::deb($this->RootItem, '$this->RootItem::');

			$this->ItemInventoryId= $this->Product->getId();
			// Util::deb( $this->ItemInventoryId, '$this->ItemInventoryId::' );

			/* echo '<pre>';
				print_r($this->RootItem);
				echo '</pre>';exit; */
			$this->RootItemObject = null;

			//$this->HasRootItem= ( ( $this->RootItem != $this->sku ) and !empty($this->RootItem) );
			$this->HasRootItem = $this->RootItem;
			/* Util::deb( $this->sku, ' $this->sku::' );
				Util::deb( $this->Matrix, ' $this->Matrix::' );
				Util::deb( $this->RootItem, ' $this->RootItem::' );
				Util::deb( $this->HasRootItem, ' $this->HasRootItem::' ); */

			if ($this->HasRootItem) {
				$this->RootItemObject = InventoryItemPeer::getSimilarInventoryItem($this->RootItem, true);
				if (!empty($this->RootItemObject)) {
					$this->HeaderTitle = 'Product: ' . $this->RootItemObject->getTitle();
				}
			}
			// Util::deb( $this->RootItemObject, ' $this->RootItemObject::' );

			$this->type = $request->getParameter('type');
			$this->input_search = $request->getParameter('input_search');
			$this->input_search = trim($this->input_search);
			$this->select_category = $request->getParameter('select_category');
			$this->select_subcategory = $request->getParameter('select_subcategory');
			$this->select_brand_id = $request->getParameter('select_brand_id');
			$this->filter_pager_count = $request->getParameter('filter_pager_count');
			$this->rows_in_pager = $request->getParameter('rows_in_pager');
			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			if (empty($this->sorting))
				$this->sorting = '-';

			/* $this->Product->setRecentlyViewed(time());
				$this->Product->save(); */

			//$this->InventoryCategoryProductLine = $this->Product->getInventoryCategory();
			$this->MoreViewInventoryItems = array();

			//$A = InventoryItemPeer::getInventoryItemsByInventoryCategory(1, false, false, $this->InventoryCategoryProductLine, 1);
			$A= InventoryItemInventoryCategoryPeer::getInventoryItemInventoryCategorysByItemInventoryId( '', false, $this->ItemInventoryId );
			//Util::deb($A, '$A::');
			for ($I = 0; $I < count($A) - 1; $I++) {
				if (trim($A[$I]->getSku()) != $this->sku) {
					$this->MoreViewInventoryItems[] = $A[$I];
				}
			}
//      Util::deb( $this->MoreViewInventoryItems, '$this->MoreViewInventoryItems::' );

			AppUtils::PushProductRecentView($this->sku);
			$this->RecentlyViewedList = AppUtils::getProductRecentViewsList($this->sku); //InventoryItemPeer::getRecentlyViewedList( $this->sku );
			//echo '<pre> $this->RecentlyViewedList ::'.print_r( $this->RecentlyViewedList, true ).'</pre><br>';

			$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeView_cart(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');
			$response->setTitle("Ray O'Herron Co. - View Cart");
			$this->message_type = $request->getParameter('message_type');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeLoad_cart_items_list($request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeGet_cart_info($request)
	{
		try {
			$response = $this->context->getResponse();
			echo json_encode(array('items_count' => Cart::getItemsCount(), 'common_sum' => Cart::getCommonSum(false)));
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeCart_product_update($request)
	{
		try {
			$response = $this->context->getResponse();
			$sku = $request->getParameter('sku');
			$product_quantity = $request->getParameter('product_quantity');
			$price_type = $request->getParameter('price_type');
			//$CurrentStdUnitPrice = $request->getParameter('CurrentStdUnitPrice');
			$CurrentStdUnitPrice = Cart::getUnitPriceBySku($sku);

			//$UsersCartArray = Cart::getUsersCartArray();
			//print_r($UsersCartArray); //debug


			$Res = Cart::UpdateUsersCart($sku, $product_quantity, $price_type, $CurrentStdUnitPrice);
			if ($Res) {
				echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'product_quantity' => $product_quantity, 'sku' => $sku, 'items_count' => Cart::getItemsCount(),
					'common_sum' => Cart::getCommonSum(false)));
			} else {
				echo json_encode(array('ErrorMessage' => 'Undefined Error', 'ErrorCode' => 1, 'product_quantity' => $product_quantity, 'sku' => $sku,
					'items_count' => Cart::getItemsCount(), 'common_sum' => Cart::getCommonSum(false)));
			}
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeCart_product_delete($request)
	{
		try {
			$response = $this->context->getResponse();
			$sku = $request->getParameter('sku');
			$product_quantity = Cart::getValueBySku($sku, 1);
			//Util::deb( $product_quantity, ' $product_quantity::' );
			// die("DIE");
			$Res = Cart::UsersCartDeleteItem($sku);
			if ($Res) {
				echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'sku' => $sku, 'product_quantity' => $product_quantity, 'items_count' => Cart::getItemsCount(),
					'common_sum' => Cart::getCommonSum(false)));
			} else {
				echo json_encode(array('ErrorMessage' => 'Undefined Error', 'ErrorCode' => 1, 'sku' => $sku,
					'items_count' => Cart::getItemsCount(), 'common_sum' => Cart::getCommonSum(false)));
			}


			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeShow_cart_info(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$this->items_count = $request->getParameter('items_count');
			$this->common_sum = $request->getParameter('common_sum');
			$this->product_quantity = $request->getParameter('product_quantity');
			$this->price_type = $request->getParameter('price_type');
			$this->hide_view_message = $request->getParameter('hide_view_message');
			$this->message_type = $request->getParameter('message_type');
			//Util::deb( $this->message_type, ' $this->message_type::' );
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeProduct_video_view(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$this->sku = $request->getParameter('sku');
			// Util::deb( $this->sku, ' $this->sku::' );
			$this->video_key = $request->getParameter('video_key');
			// Util::deb( $this->video_key, ' $this->video_key::' );
			$this->lYoutubeVideo = YoutubeVideoPeer::getSimilarYoutubeVideo($this->video_key);
			// Util::deb( $this->lYoutubeVideo, ' $this->lYoutubeVideo::' );
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeProduct_size_chart(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$this->code = $request->getParameter('code');
			$A = preg_split('/&random=/', $this->code);
			if (count($A) == 2) {
				$this->code = $A[0];
			}
			$this->lSizingChart = SizingChartPeer::retrieveByPK($this->code);
			$SizingChartsDirectory = sfConfig::get('app_application_SizingChartsDirectory');
			$this->Filename = $SizingChartsDirectory . DIRECTORY_SEPARATOR . $this->lSizingChart->getFilename();
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeProduct_document_view(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$this->code = $request->getParameter('code');

			$Document = DocumentPeer::retrieveByPK($this->code);
			if (empty($Document)) return sfView::NONE;

			$Filename = $Document->getFilename();

			$FullFileName = sfConfig::get('sf_web_dir') . sfConfig::get('app_application_DocumentsDirectory') . DIRECTORY_SEPARATOR . $Filename;
			if (!file_exists($FullFileName))
				return sfView::NONE;

			$this->setLayout(false);
			$response->setHttpHeader('Content-Disposition', 'attachment; filename="' . $Filename . '"');
			$response->setContentType('application/pdf');
			$response->setContent(file_get_contents($FullFileName));
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeAdd_email($request)
	{
		try {
			$response = $this->context->getResponse();
			$email = $request->getParameter('email');
			$lSimilarEmailSubmission = EmailSubmissionPeer::getSimilarEmailSubmission($email);
			if (!empty($lSimilarEmailSubmission)) {
				echo json_encode(array('ErrorMessage' => 'This Email was already submitted !', 'ErrorCode' => 1, 'id' => $lSimilarEmailSubmission->getId()));
				return sfView::NONE;
			}

			$lNewEmailSubmission = new EmailSubmission();
			$lNewEmailSubmission->setEmail($email);
			$lNewEmailSubmission->save();

			echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'email_id' => $lNewEmailSubmission->getId()));
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	/**
	 * Show list of getsubcategories in Administration Zone
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeGetsubcategories($request)
	{
		try {
			$response = $this->context->getResponse();
			$this->category = $request->getParameter('category');
			$this->selected_subcategory = $request->getParameter('selected_subcategory');
			$this->Subcategories = InventoryCategoryPeer::getSubcategoriesList($this->category, true);
			$ResArray = array();
			foreach ($this->Subcategories as $InventoryCategory) {
				$ResArray[] = array('id' => $InventoryCategory, 'name' => $InventoryCategory);
			}
			echo json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'data' => $ResArray, 'length' => count($ResArray), 'selected_subcategory' => $this->selected_subcategory));
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeMake_credit($request)
	{
		try {
			$response = $this->context->getResponse();
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	/*
	 * Function executeMy_test
	 */

	public function executeMy_test()
	{
		$sizes = '';
		$colors = '';
		$out_content = array();
		$sku = $_POST['sku'];
		$type = $_POST['type'];
		if ($sku != '') {
			$_SESSION['sku_'] = $sku;
			$product = InventoryItemPeer::getSimilarInventoryItem($sku, true);
			/* echo '<pre>';
				print_r($product);
				echo '</pre>';
				exit; */
			$root_item = $product->getRootItem();

			// Image of product
			$temp_file_name = '';
			$check = '';
			$thumbnail_image_file = AppUtils::getThumbnailImageBySku($sku, false);
			if (empty($thumbnail_image_file)) {
				$thumbnail_image_file = AppUtils::getResizedImageBySku($sku, false, false, true);
				$check = $thumbnail_image_file['ImageFileName'];
				if (is_array($thumbnail_image_file)) {
					$thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file['ImageFileName'];
					$temp_file_name = $thumbnail_image_file;
				} else {
					$thumbnail_image_file = 'uploads' . DIRECTORY_SEPARATOR . $thumbnail_image_file;
					$temp_file_name = $thumbnail_image_file;
				}
				$thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . $thumbnail_image_file;
				if (!file_exists($temp_file_name)) {
					$thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
				}
			}
			if ($check == '') {
				$thumbnail_image_file = AppUtils::getHostForImage(sfContext::getInstance()->getConfiguration()) . 'images/noimage298.png';
			}
			/* echo '<pre>';
				print_r($thumbnail_image_file);
				echo '</pre>';
				exit; // DEBUG */


			if ($type == 'size') {
				$size_list = InventoryItemPeer::getSizesListByRootItem($root_item);
				if (!empty($size_list)) {
					$sizes .= '<option value=""> -select- </option>';
					$result_size_list = array();
					foreach ($size_list as $key => $value) {
						$flag = 0;
						$temp1 = explode('-', $key);
						foreach ($result_size_list as $k => $v) {
							$temp2 = explode('-', $k);
							if ($temp1[2] == $temp2[2]) {
								$flag = 1;
							}
						}
						if ($flag == 0) {
							$result_size_list[$key] = $value;
						}
					}
					foreach ($result_size_list as $key => $value) {
						if ($key != $sku) {
							$sizes .= '<option value=' . $key . '>' . $value . '</option>';
						} else {
							$sizes .= '<option selected value=' . $key . '>' . $value . '</option>';
						}
					}
				}
				$color_list = InventoryItemPeer::getColorsListByRootItem($root_item);
				if (!empty($color_list)) {
					$colors .= '<option value=""> -select- </option>';
					$temp = explode('-', $sku);
					$size = $temp[2];
					foreach ($color_list as $key => $value) {
						$temp1 = explode('-', $key);
						if ($temp1[2] == $size) {
							if ($temp1[1] == $temp[1]) {
								$colors .= '<option selected value="' . $key . '">' . $value . '</option>';
							} else {
								$colors .= '<option value="' . $key . '">' . $value . '</option>';
							}
						}
					}
				}
			} else if ($type == 'color') {
				$sizes = '';
				$size_list = InventoryItemPeer::getSizesListByRootItem($root_item);
				if (!empty($size_list)) {
					$sizes .= '<option value=""> -select- </option>';
					$temp = explode('-', $sku);
					$color = $temp[1];
					foreach ($size_list as $key => $value) {
						$temp1 = explode('-', $key);
						if ($temp1[1] == $color) {
							if ($temp[2] == $temp1[2]) {
								$sizes .= '<option selected value="' . $key . '">' . $value . '</option>';
							} else {
								$sizes .= '<option value="' . $key . '">' . $value . '</option>';
							}
						}
					}
				}
				$color_list = InventoryItemPeer::getColorsListByRootItem($root_item);
				if (!empty($color_list)) {
					$colors .= '<option value=""> -select- </option>';
					$result_colors_list = array();
					foreach ($color_list as $key => $value) {
						$flag = 0;
						$temp1 = explode('-', $key);
						foreach ($result_colors_list as $k => $v) {
							$temp2 = explode('-', $k);
							if ($temp1[1] == $temp2[1]) {
								$flag = 1;
							}
						}
						if ($flag == 0) {
							$result_colors_list[$key] = $value;
						}
					}
				}
				$current_color = explode('-', $sku);
				foreach ($result_colors_list as $key => $value) {
					$color_in_sku = explode('-', $key);
					if ($color_in_sku[1] != $current_color[1]) {
						$colors .= '<option value="' . $key . '">' . $value . '</option>';
					} else {
						$colors .= '<option selected value="' . $key . '">' . $value . '</option>';
					}
				}
			}
			$out_content['sizes'] = $sizes;
			$out_content['colors'] = $colors;
		} else {
			$sku = $_SESSION['sku_'];
			$product = InventoryItemPeer::getSimilarInventoryItem($sku, true);
			$root_item = $product->getRootItem();
			$size_list = InventoryItemPeer::getSizesListByRootItem($root_item);
			$color_list = InventoryItemPeer::getColorsListByRootItem($root_item);
			$sizes .= '<option selected value=""> -select- </option>';
			$colors .= '<option selected value=""> -select- </option>';
			$result_colors_list = array();
			$result_size_list = array();

			// Fill colors
			foreach ($color_list as $key => $value) {
				$flag = 0;
				$temp1 = explode('-', $key);
				foreach ($result_colors_list as $k => $v) {
					$temp2 = explode('-', $k);
					if ($temp1[1] == $temp2[1]) {
						$flag = 1;
					}
				}
				if ($flag == 0) {
					$result_colors_list[$key] = $value;
				}
			}
			foreach ($result_colors_list as $key => $value) {
				$colors .= '<option value="' . $key . '">' . $value . '</option>';
			}

			// Fill sizes
			foreach ($size_list as $key => $value) {
				$sizes .= '<option value="' . $key . '">' . $value . '</option>';
			}

			$out_content['sizes'] = $sizes;
			$out_content['colors'] = $colors;
		}


		/** DEBUG SECTION * */
		/* echo '<pre>';
			print_r($product);
			echo '</pre>';
			exit; */ // DEBUG

		/* echo $root_item;
			exit; // DEBUG */

		/* echo '<pre>';
			print_r($size_list);
			echo '</pre>';
			exit; // DEBUG */

		/* echo $sizes; exit; // DEBUG */

		/* echo $colors; exit; // DEBUG */

		/* echo '<pre>';
			print_r($out_content);
			echo '</pre>';
			exit; // DEBUG */

		/** END OF DEBUG SECTION * */
		$out_content['img'] = $thumbnail_image_file;
		$out_content = json_encode($out_content);
		echo $out_content;
		exit;
	}

	public function executeCalculate_tax()
	{
		$tax = 0;
		$out_tax_rates = array();
		// This is test array of taxes
		$taxes = array(
			'MI' => 6,
			'IN' => 7,
			'WI' => 5,
			'MN' => 6.875,
			'IL' => 8.75
		);

		if (!empty($_POST['state'])) {
			$state = $_POST['state'];
		} else {
			$state = 'IL';
		}
		if (!empty($_POST['zip'])) {
			$dst_zip = $_POST['zip'];
		} else {
			$dst_zip = '61820';
		}
		$shipping = $this->getShippingRates($dst_zip);

		foreach ($taxes as $key => $value) {
			if ($key == $state) {
				$tax = $value;
			}
		}
		$out_tax_rates['tax'] = "$tax";
		$out_tax_rates['USPS'] = $shipping['USPS'];
		$out_tax_rates['UPSGround'] = $shipping['UPSGround'];
		$out_tax_rates['UPS3DaySelected'] = $shipping['UPS3DaySelected'];
		$out_tax_rates['UPS2DayAir'] = $shipping['UPS2DayAir'];
		$out_tax_rates['UPSNextDayAirSaver'] = $shipping['UPSNextDayAirSaver'];
		$out_tax_rates = json_encode($out_tax_rates);
		echo $out_tax_rates;
		exit;
	}

	/*
	 * Function etShippingRates
	 * @params $dst_zip
	 * @return $rates
	 */

	private function getShippingRates($dst_zip)
	{
		$rates = array();
		$is_debug = false;
		/*     * ************ USPS *********** */
		$TotalShipWeight = Cart::getTotalShipWeight();
		//echo '-0:: $dst_zip:: <pre>' . print_r($dst_zip, true) . '</pre><br>';
		if ($is_debug) echo '-1:: $TotalShipWeight:: <pre>' . print_r($TotalShipWeight, true) . '</pre><br>';
		if (empty($TotalShipWeight)) $TotalShipWeight = 2;
		if ($is_debug) echo '-2:: $TotalShipWeight:: <pre>' . print_r($TotalShipWeight, true) . '</pre><br>';

		$postal_code = sfConfig::get('app_UPS_postal_code'); // 20770
		if ($is_debug) echo '-0:: $postal_code:: <pre>' . print_r($postal_code, true) . '</pre><br>';
		$RSSRM = "
      <RateV3Request USERID=\"397PETWO5514\">
          <Package ID=\"1ST\">
              <Service>ALL</Service>
              <ZipOrigination>$postal_code</ZipOrigination>
              <ZipDestination>$dst_zip</ZipDestination>
              <Pounds>" . $TotalShipWeight . "</Pounds>
              <Ounces>0</Ounces>
              <Container/>
              <Size>REGULAR</Size>
              <Machinable>true</Machinable>
          </Package>
      </RateV3Request>";
		if ($is_debug) echo '-11:: $RSSRM:: <pre>' . print_r(htmlspecialchars($RSSRM), true) . '</pre><br>';

		$ch = curl_init();
		$URL = "http://production.shippingapis.com/ShippingAPI.dll?API=RateV3&XML=" . urlencode($RSSRM);
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$data = strstr($result, '<?');
		if ($is_debug) {
			echo '<pre>';
			print_r($result);
			echo '</pre>';
		}
		$parser = xml_parser_create();
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, $data, $values, $tags);
		xml_parser_free($parser);

		if ($is_debug) {
			echo '<pre>';
			print_r($values);
			echo '</pre>';
		}
		$counter = 0;
		$c = 0;
		foreach ($values as $index) {
			foreach ($index as $key => $value) {
				/* echo '<pre>';
					print_r($key);
					echo '</pre>'; */
				if ($key == 'value' and preg_match("/Flat Rate Boxes$/", $value)) {
					$c = $counter + 1;
				}
			}
			$counter++;
		}
		if ($c != 0) {
			$rates['USPS'] = $values[$c]['value'];
		}
		curl_close($ch);

		/*     * ************ UPS *********** */
		$rates['UPSGround'] = $this->ups($dst_zip, '03', $TotalShipWeight /*'1'*/, '1', '1', '1');
		$rates['UPS3DaySelected'] = $this->ups($dst_zip, '12', $TotalShipWeight /*'1'*/, '1', '1', '1');
		$rates['UPS2DayAir'] = $this->ups($dst_zip, '02', $TotalShipWeight /*'1'*/, '1', '1', '1');
		$rates['UPSNextDayAirSaver'] = $this->ups($dst_zip, '01', $TotalShipWeight /*'1'*/, '1', '1', '1');

		if ($is_debug) {
			echo '<pre>';
			print_r($rates);
			echo '</pre>';
			exit;
		}

		return $rates;
	}

	private function ups($dest_zip, $service, $weight, $length, $width, $height)
	{
		/* $access_license_number = 'EC4B8375440DAE00';
		$user_id = 'petworldstore';
		$password = 'pet37world';
		$postal_code = '10001';
		$shipper_number = '2937X9'; */
		$access_license_number = sfConfig::get('app_UPS_access_license_number');
		$user_id = sfConfig::get('app_UPS_user_id');
		$password = sfConfig::get('app_UPS_password');
		$postal_code = sfConfig::get('app_UPS_postal_code');
		$shipper_number = sfConfig::get('app_UPS_shipper_number');


		$data = "<?xml version=\"1.0\"?>
      <AccessRequest xml:lang=\"en-US\">
    		<AccessLicenseNumber>$access_license_number</AccessLicenseNumber>
    		<UserId>$user_id</UserId>
    		<Password>$password</Password>
    	</AccessRequest>
    	<?xml version=\"1.0\"?>
    	<RatingServiceSelectionRequest xml:lang=\"en-US\">
    		<Request>
    			<TransactionReference>
    				<CustomerContext>Bare Bones Rate Request</CustomerContext>
    				<XpciVersion>1.0001</XpciVersion>
    			</TransactionReference>
    			<RequestAction>Rate</RequestAction>
    			<RequestOption>Rate</RequestOption>
    		</Request>
    	<PickupType>
    		<Code>01</Code>
    	</PickupType>
    	<Shipment>
    		<Shipper>
    			<Address>
    				<PostalCode>$postal_code</PostalCode>
    				<CountryCode>US</CountryCode>
    			</Address>
			<ShipperNumber>$shipper_number</ShipperNumber>
    		</Shipper>
    		<ShipTo>
    			<Address>
    				<PostalCode>$dest_zip</PostalCode>
    				<CountryCode>US</CountryCode>
				<ResidentialAddressIndicator/>
    			</Address>
    		</ShipTo>
    		<ShipFrom>
    			<Address>
    				<PostalCode>$postal_code</PostalCode>
    				<CountryCode>US</CountryCode>
    			</Address>
    		</ShipFrom>
    		<Service>
    			<Code>$service</Code>
    		</Service>
    		<Package>
    			<PackagingType>
    				<Code>02</Code>
    			</PackagingType>
    			<Dimensions>
    				<UnitOfMeasurement>
    					<Code>IN</Code>
    				</UnitOfMeasurement>
    				<Length>$length</Length>
    				<Width>$width</Width>
    				<Height>$height</Height>
    			</Dimensions>
    			<PackageWeight>
    				<UnitOfMeasurement>
    					<Code>LBS</Code>
    				</UnitOfMeasurement>
    				<Weight>$weight</Weight>
    			</PackageWeight>
    		</Package>
    	</Shipment>
    	</RatingServiceSelectionRequest>";
		$ch = curl_init("https://www.ups.com/ups.app/xml/Rate");
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($ch);
		$data = strstr($result, '<?');
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $data, $vals, $index);
		xml_parser_free($xml_parser);
		$params = array();
		$level = array();
		foreach ($vals as $xml_elem) {
			if ($xml_elem['type'] == 'open') {
				if (array_key_exists('attributes', $xml_elem)) {
					list($level[$xml_elem['level']], $extra) = array_values($xml_elem['attributes']);
				} else {
					$level[$xml_elem['level']] = $xml_elem['tag'];
				}
			}
			if ($xml_elem['type'] == 'complete') {
				$start_level = 1;
				$php_stmt = '$params';
				while ($start_level < $xml_elem['level']) {
					$php_stmt .= '[$level[' . $start_level . ']]';
					$start_level++;
				}
				$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
				eval($php_stmt);
			}
		}
		curl_close($ch);
		return $params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'];
	}

	public function executeReview_order()
	{
		$out_str = $_POST['zipcode'] . ',' . $_POST['usps'] . ',' . $_POST['ups_ground'] . ',' . $_POST['ups_3_day_selected'] . ',' .
			$_POST['ups_2_day_air'] . ',' . $_POST['ups_next_day_air_saver'] . ',' . $_POST['est_tax'] . ',' . $_POST['est_shipping'] . ',' . $_POST['estimated_total'];

		Cart::setTaxShipData($out_str, 'all');
	}

	public function executeSet_estimates()
	{
		$out_str = $_POST['est_shipping'] . ',' . $_POST['est_total'];
		Cart::setTaxShipData($out_str, 'estimates');
	}

	public function executeProduct_paperwork_view(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$this->code = $request->getParameter('code');

			$PaperWork = PaperworkPeer::getPaperworkByCode($this->code);

			if (empty($PaperWork)) {
				return sfView::NONE;
			}
			$Filename = $PaperWork->getFilename();

			$FullFileName = sfConfig::get('sf_web_dir') . sfConfig::get('app_application_PaperworksDirectory') . DIRECTORY_SEPARATOR . $Filename;
			if (!file_exists($FullFileName)) {
				echo 'dsadasdas - ' . $FullFileName;
				exit;
				return sfView::NONE;
			}

			$this->setLayout(false);
			$response->setHttpHeader('Content-Disposition', 'attachment; filename="' . $Filename . '"');
			$response->setContentType('application/pdf');
			$response->setContent(file_get_contents($FullFileName));
			return sfView::NONE;
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeContact()
	{
		try {
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Contact");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeSalesperson()
	{

	}

	public function executeSupport()
	{

		try {

			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Support");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');
			$this->product_support_video_count = (int)sfConfig::get('app_application_product_support_video_count');
			$this->YoutubeVideosList = YoutubeVideoPeer::getYoutubeVideos('', false, 'ORDERING', $this->product_support_video_count);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeSupport_new()
	{

		try {

			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Support");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');
			$this->product_support_video_count = (int)sfConfig::get('app_application_product_support_video_count');
			$this->YoutubeVideosList = YoutubeVideoPeer::getYoutubeVideos('', false, 'ORDERING', $this->product_support_video_count);
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeTaser(sfWebRequest $request)
	{

		try {

			$response = $this->context->getResponse();
			$response->setTitle("Testing & Evaluation (T&E) - Taser");

			$this->form = new TaserForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('taser');
				$this->form->bind($data);
				if ($this->form->isValid()) {
					//$data['email']= 'nilov@softreactor.com';
					//Util::deb($data, '$data::');
					//echo '$data::'.print_r($data,true);

					$noanswer_email = sfConfig::get('app_notification_noanswer_email');
					//$contact_email = sfConfig::get('app_notification_contact_email');
					//$contact_email = 'projectexaminer@magimpact.com'; //debug
					$contact_email = sfConfig::get('app_notification_contact_email');
					/*if ( $data['email']== 'nilov@softreactor.com' ) { // TO HIDE THIS BLOCK
						echo '<br>$data::'.print_r($data,true);
						$contact_email = 'nilov@softreactor.com';
					}  */ // TO HIDE THIS BLOCK


					//$Msg = "Request for Taser Testing & Evaluation";//  "The following message has been submitted via the Testing & Evaluation (T&E) - Taser page on the Ray O'Herron website:\n\r";
					$Msg = "-------------------------------\r\nContact Information\r\n-------------------------------\r\n\r\n";
					$Msg .= "Name: \r\n" . $data['first_name'] . ' ' . $data['last_name'] . "\r\n\r\n";
					$Msg .= "Contact Person for Tasers in Department: \r\n" . $data['department_person'] . "\r\n\r\n";
					$Msg .= "Department Name: \r\n" . $data['department_name'] . "\r\n\r\n";
					$Msg .= "Address: \r\n" . $data['address'] . "\r\n";
					$Msg .= $data['city'] . ", " . $data['state'] . ' ' . $data['zip'] . "\r\n\r\n";
					$Msg .= "Email: \r\n" . $data['email'] . "\r\n\r\n";
					$Msg .= "Phone: \r\n" . $data['phone'] . "\r\n\r\n";

					$Msg .= "-------------------------------\r\nEvaluation Information\r\n-------------------------------\r\n\r\n";

					//$Msg.= "City: " . $data['city'] . "\r\n\r\n";
					//$Msg.= "State: " . $data['state'] . "\r\n\r\n";
					//$Msg.= "Zip: " . $data['zip'] . "\r\n\r\n";
					$Msg .= "What make/model of ECWs are you interested in? \r\n" . $data['ecws_model'] . "\r\n\r\n";
					$Msg .= "How old are your current ECWs? \r\n" . $data['current_ecws'] . "\r\n\r\n";
					$Msg .= "When do you plan to acquire or replace your ECWs? \r\n" . $data['plan_to_replace_ecws'] . "\r\n\r\n";
					$Msg .= "Do you have budget allocated in your planned acquire/replace timeframe for new ECWs? \r\n" . $data['have_budget_ecws'] . "\r\n\r\n";
					$Msg .= "Is your agency considering video camera systems? \r\n" . $data['is_your_agency'] . "\r\n\r\n";
					$Msg .= "When do you plan to acquire your camera system? \r\n" . $data['plan_to_acquire_camera'] . "\r\n\r\n";
					$Msg .= "Do you currently have budget allocated for camera systems? \r\n" . $data['have_budget_allocated_for_camera'] . "\r\n\r\n";
					$Msg .= !empty($data['additional_notes']) ? "Additional Notes: \r\n" . $data['additional_notes'] . "\r\n\r\n" : "";


					$MsgSubject = "Request for Taser Testing & Evaluation"; // 'Message From Ray O\'Herron Testing & Evaluation (T&E) - Taser Webpage';
					$MsgFromAddress = '"Ray O\'Herron Co. Website" <no-reply@oherron.com>'; // 'projectexaminer@magimpact.com';//Ray O\'Herron Co. Website" < projectexaminer@magimpact.com >';
					$recipient = $contact_email; //"nilov@softreactor.com"; //recipient
					$mail_body = $Msg; //"The text for the mail..."; //mail body
					$subject = $MsgSubject; //"Subject for reviever"; //subject

					// -In the email, the From address should be: "Ray O'Herron Co. Website" <no-reply@oherron.com>
					$header = 'From: ' . $MsgFromAddress; //"From: ". $Name . " <" . $email . ">\r\n"; //optional headerfields

					//send copy to client
					mail($recipient, $subject, $mail_body, $header);

					//send copy to MagImpact for quality control
					mail('peter@magimpact.com', $subject, $mail_body, $header);

					/*if ( $data['email'] == 'nilov@softreactor.com' ) { // TO HIDE THIS BLOCK
						//mail('peter@magimpact.co', '$Title1111', '$Subject2222', $headers);  //debug

						//echo '<br>$SendEmailResult::'.print_r($SendEmailResult,true);
						echo '<br>$contact_email::'.print_r($contact_email,true);
						echo '<br>$MsgFromAddress::'.print_r($MsgFromAddress,true);
						echo '<br>$MsgSubject::'.print_r($MsgSubject,true);
						echo '<br>$Msg::'.print_r($Msg,true);


						die("!!TTYT");
					} // TO HIDE THIS BLOCK   */

					$this->redirect('@taserthankyou');
				}
			}

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeTaserthankyou(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->setTitle("Testing & Evaluation (T&E) - Taser");
	}


	public function executeFirearm(sfWebRequest $request)
	{

		try {

			$response = $this->context->getResponse();
			$response->setTitle("Testing & Evaluation (T&E) - Firearm");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');

			$this->form = new FirearmForm();
			if ($request->isMethod('post')) {
				$data = $request->getParameter('firearm');
				$this->form->bind($data);
				if ($this->form->isValid()) {

					$noanswer_email = sfConfig::get('app_notification_noanswer_email');
					//$contact_email = 'nilov@softreactor.com';// 'projectexaminer@magimpact.com';
					//$contact_email = 'projectexaminer@magimpact.com';
					$contact_email = sfConfig::get('app_notification_contact_email');

					//$Msg = "The following message has been submitted via the Testing & Evaluation (T&E) - Firearm page on the Ray O'Herron website:\n\r";
					$Msg = "-------------------------------\r\nContact Information\r\n-------------------------------\r\n\r\n";
					$Msg .= "Name: \r\n" . $data['first_name'] . ' ' . $data['last_name'] . "\r\n\r\n";
					$Msg .= "Contact Person for Firearms in Department: \r\n" . $data['department_person'] . "\r\n\r\n";
					$Msg .= "Department Name: \r\n" . $data['department_name'] . "\r\n\r\n";
					$Msg .= "Address: \r\n" . $data['address'] . "\r\n";
					$Msg .= $data['city'] . ", " . $data['state'] . ' ' . $data['zip'] . "\r\n\r\n";
					$Msg .= "Email: \r\n" . $data['email'] . "\r\n\r\n";
					$Msg .= "Phone: \r\n" . $data['phone'] . "\r\n\r\n\r\n";

					$Msg .= "-------------------------------\r\nEvaluation Information\r\n-------------------------------\r\n\r\n";

					$Msg .= "What make/model of of firearm(s) are you interested in? \r\n" . $data['firearms_model'] . "\r\n\r\n";
					$Msg .= !empty($data['additional_comments']) ? "Additional Comments: \r\n" . $data['additional_comments'] . "\r\n\r\n" : "";
					$MsgSubject = 'Request for Firearms Testing & Evaluation'; // 'Message From Ray O\'Herron Testing & Evaluation (T&E) - Firearm Webpage';
					$MsgFromAddress = '"Ray O\'Herron Co. Website" <no-reply@oherron.com>'; // 'From: projectexaminer@magimpact.com';// 'Ray O\'Herron Co. Website" < projectexaminer@magimpact.com >';
					/*-In the email, the From address should be: "Ray O'Herron Co. Website" <no-reply@oherron.com> */
					$header = 'From: ' . $MsgFromAddress;

					//send copy to client
					mail($contact_email, $MsgSubject, $Msg, $header);

					//send copy to MagImpact for quality control
					mail('peter@magimpact.com', $MsgSubject, $Msg, $header);

					$this->redirect('@firearmthankyou');
				}
			}

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeFirearmthankyou(sfWebRequest $request)
	{
		$response = $this->context->getResponse();
		$response->setTitle("Testing & Evaluation (T&E) - Firearm");
	}

	public function executeCompany()
	{

	}

  public function executeReturns()
	{

	}
	
	public function executeTandeform()
	{

	}

	public function executeGunform()
	{

	}


	public function executeGunthankyou()
	{

	}


	public function executeSpecials(sfWebRequest $request)
	{
		{
			try {
				$this->input_search = '';
				$this->select_category = '';
				$this->select_subcategory = '';
				$this->select_brand_id = '';

				$this->filter_pager_count = '';
				$this->rows_in_pager = '';
				$this->HeaderTitle = '';
				$this->checked_filters = '';

				$response = $this->context->getResponse();
				$response->setTitle("Ray O'Herron Co. - Product Listings");
				$response->addStylesheet('thickbox.css');
				$response->addJavascript('/js/jquery/jquery.js', 'last');
				$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
				$response->addJavascript('/js/jquery/thickbox.js', 'last');
				$this->message_type = $request->getParameter('message_type');

				if ($request->isMethod('post')) {
					if (!empty($_REQUEST['input_search'])) {
						$this->input_search = $_REQUEST['input_search'];
						$this->input_search = trim($this->input_search);
						if ($this->input_search == '-')
							$this->input_search = '';
					}
					if (!empty($_REQUEST['select_category'])) {
						$this->select_category = $_REQUEST['select_category'];
						if ($this->select_category == '-')
							$this->select_category = '';
					}

					if (!empty($_REQUEST['select_subcategory'])) {
						$this->select_subcategory = $_REQUEST['select_subcategory'];
						if ($this->select_subcategory == '-')
							$this->select_subcategory = '';
					}


					if (!empty($_REQUEST['select_brand_id'])) {
						$this->select_brand_id = $_REQUEST['select_brand_id'];
						if ($this->select_brand_id == '-')
							$this->select_brand_id = '';
					}

					if (!empty($_REQUEST['filter_pager_count'])) {
						if (strlen($_REQUEST['filter_pager_count']) > 0) {
							$this->filter_pager_count = $_REQUEST['filter_pager_count'];
							if ($this->filter_pager_count == '-')
								$this->filter_pager_count = '';
						}
					}
					if (!empty($_REQUEST['rows_in_pager'])) {
						if (strlen($_REQUEST['rows_in_pager']) > 0) {
							$this->rows_in_pager = $_REQUEST['rows_in_pager'];
							if ($this->rows_in_pager == '-')
								$this->rows_in_pager = '';
						}
					}
				} else {
					$this->input_search = $request->getParameter('input_search');
					$this->input_search = trim($this->input_search);
					if ($this->input_search == '-')
						$this->input_search = '';

					$this->select_category = $request->getParameter('select_category');
					if ($this->select_category == '-')
						$this->select_category = '';
					$this->select_subcategory = $request->getParameter('select_subcategory');
					if ($this->select_subcategory == '-')
						$this->select_subcategory = '';

					$this->select_brand_id = $request->getParameter('select_brand_id');
					if ($this->select_brand_id == '-')
						$this->select_brand_id = '';

					$this->filter_pager_count = $request->getParameter('filter_pager_count');
					if ($this->filter_pager_count == '-')
						$this->filter_pager_count = '';
					$this->rows_in_pager = $request->getParameter('rows_in_pager');
					if ($this->rows_in_pager == '-')
						$this->rows_in_pager = '';
				}

				$this->page = $request->getParameter('page', 1);
				$this->sorting = $request->getParameter('sorting');
				if (empty($this->sorting))
					$this->sorting = '-';

				if (empty($this->rows_in_pager) and !is_integer($this->rows_in_pager)) {
					$this->rows_in_pager = (int)sfConfig::get('app_application_product_listings_rows_in_pager');
				}

				if (empty($this->select_category) and !empty($this->select_subcategory)) {
					$lSimilarInventoryCategory = InventoryCategoryPeer::getSimilarInventoryCategoryBySubcategory($this->select_subcategory);
					if (!empty($lSimilarInventoryCategory)) {
						$this->select_category = $lSimilarInventoryCategory->getCategory();
						//$this->subcategory = $lSimilarInventoryCategory->getSubcategory();
					}
				}
				// '001BA74Z0'
				/*  public static function getInventoryItems( $page=1, $ReturnPager=true, $ReturnCount= false, $rows_in_pager='', $filter_matrix='', $filter_rootitem='',
					$filter_title='', $ExtendedSearch= false, $filter_sku='', $filter_category='', $filter_subcategory='', $filter_brand_id='', $filter_qty_on_hand_min='',  $filter_qty_on_hand_max='',  $filter_std_unit_price_min='' , $filter_std_unit_price_max='', $Sorting= '', '' ) { */
				$filter_matrix = 1;
				$filter_rootitem = '';
				$this->InventoryItemsPager = InventoryItemPeer::getInventoryItems($this->page, true, false, $this->rows_in_pager, $filter_matrix, $filter_rootitem, $this->input_search, true, '', $this->select_category, $this->select_subcategory, $this->select_brand_id, '', '', '', '', $this->sorting, array("D", "P"));
				// If sale_method == "D" or "P"
				/* echo '<pre>';
					print_r($this->InventoryItemsPager);
					echo '</pre>';
					exit; */
				$this->UsersCartArray = $this->getUser()->getAttribute('UsersCart');
			} catch (Exception $lException) {
				$this->logMessage($lException->getMessage(), 'err');
				return sfView::ERROR;
			}
		}
	}

	public function executeProducts(sfWebRequest $request)
	{
		//Util::deb(  'executeProducts()::' );


		try {
			$this->input_search = '';
			$this->select_category = '';
			$this->select_subcategory = '';
			$this->select_brand_id = '';

			$this->filter_pager_count = '';
			$this->rows_in_pager = '';
			$this->HeaderTitle = '';

			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Product Listings");
			$response->addStylesheet('thickbox.css');
			$response->addJavascript('/js/jquery/jquery.js', 'last');
			$response->addJavascript('/js/jquery/jquery-ui.js', 'last');
			$response->addJavascript('/js/jquery/thickbox.js', 'last');


			$this->input_search = $request->getParameter('input_search');
			$this->select_category = $request->getParameter('select_category');
			$this->select_subcategory = $request->getParameter('select_subcategory');
			$this->select_brand_id = $request->getParameter('select_brand_id');
			$this->filter_pager_count = $request->getParameter('filter_pager_count');
			$this->rows_in_pager = $request->getParameter('rows_in_pager');

			$this->RootItem = '';
			$this->HasRootItem = '';
			$this->RootItemObject = '';
			$this->Matrix = '';
			$this->lInventoryCategory = '';
			$this->RootItemObject = '';


			$this->page = $request->getParameter('page', 1);
			$this->sorting = $request->getParameter('sorting');
			if (empty($this->sorting))
				$this->sorting = '-';

			if (empty($this->rows_in_pager) and !is_integer($this->rows_in_pager)) {
				$this->rows_in_pager = (int)sfConfig::get('app_application_product_listings_rows_in_pager');
			}
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeSend_contact_email(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$this->name = $request->getParameter('name');
			$this->email = $request->getParameter('email');
			$this->message = htmlspecialchars($request->getParameter('message'));
			$this->subject = $request->getParameter('subject');
			$this->telephone = $request->getParameter('telephone');

			$noanswer_email = sfConfig::get('app_notification_noanswer_email');
			$contact_email = sfConfig::get('app_notification_contact_email');

			$Msg = "The following message has been submitted via the Contact page on the Ray O'Herron website:\n\r<br /><br />";
			$Msg .= "Name: " . $this->name . "\r\n<br />";
			$Msg .= "Email: " . $this->email . "\r\n<br />";
			$Msg .= !empty($this->telephone) ? "Telephone: " . $this->telephone . "\r\n<br />" : "";
			$Msg .= '<br />Subject: ' . $this->subject . "\r\n<br />";
			$Msg .= "<br />Message:<br />" . str_replace("\n", "<br>", $this->message) . "\r\n<br />";

			$MsgSubject = 'Message From Ray O\'Herron Contact Webpage';
			$MsgFromAddress = $this->name . ' <' . $this->email . '>';

			if (trim($this->name) != '' && trim($this->email) != '') {
				$SendEmailResult = Util::SendEmail($contact_email, $MsgFromAddress, $MsgSubject, $Msg);
			}

			//$Msg = $this->name . ' ( Email: '.$this->email . ( !empty($this->telephone) ? ", telephone: ".$this->telephone: "" ) . ' ) wrote to contact of Ray O\'Herron Co. site.'."\r<br>";
			//$Msg.= 'Subject: ' . $this->subject.".\r<br>";
			//$Msg.= 'Message: ' . str_replace("\n","<br>",$this->message) . ".\r<br>";
			//$SendEmailResult= Util::SendEmail( $contact_email, $noanswer_email, $this->name . ' wrote to contact of Ray O\'Herron Co. site', $Msg);

			echo json_encode(array('send_email_result' => $SendEmailResult));
			return sfView::NONE;

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeSend_support_email(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$this->name = $request->getParameter('name');
			$this->email = $request->getParameter('email');
			$this->message = htmlspecialchars($request->getParameter('message'));
			$this->subject = $request->getParameter('subject');
			$this->telephone = $request->getParameter('telephone');

			$noanswer_email = sfConfig::get('app_notification_noanswer_email');
			$support_email = sfConfig::get('app_notification_support_email');

			$Msg = "The following message has been submitted via the Support page on the Ray O'Herron website:\n\r<br /><br />";
			$Msg .= "Name: " . $this->name . "\r\n<br />";
			$Msg .= "Email: " . $this->email . "\r\n<br />";
			$Msg .= !empty($this->telephone) ? "Telephone: " . $this->telephone . "\r\n<br />" : "";
			$Msg .= '<br />Subject: ' . $this->subject . "\r\n<br />";
			$Msg .= "<br />Message:<br />" . str_replace("\n", "<br>", $this->message) . "\r\n<br />";

			$MsgSubject = 'Message From Ray O\'Herron Support Webpage';
			//$MsgFromAddress = $noanswer_email;
			$MsgFromAddress = $this->name . ' <' . $this->email . '>';

			if (trim($this->name) != '' && trim($this->email) != '') {
				$SendEmailResult = Util::SendEmail($support_email, $MsgFromAddress, $MsgSubject, $Msg);
			}
			echo json_encode(array('send_email_result' => $SendEmailResult));
			return sfView::NONE;

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeShipping_info()
	{

	}

	public function executeProduct_documents()
	{

	}

	public function executePress()
	{

	}

	public function executePrivacy_policy()
	{

	}

	public function executeTerm_of_use()
	{

	}


	public function executeGoogle_large_map()
	{
		try {
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Contact");
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}

	public function executeGoogle_large_map_2()
	{
		try {
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Contact");
		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;
		}
	}


	public function executeMap_search_addr(sfWebRequest $request)
	{
		try {
			$response = $this->context->getResponse();
			$response->setTitle("Ray O'Herron Co. - Contact");
			$this->index = $request->getParameter('index');
			$this->addr = $request->getParameter('addr');

			if ((string)$this->index == '1') {
				$daddr = '3549 N. Vermilion St. Danville, IL 61834-1070';
				$this->redirect('https://maps.google.com/maps?daddr=' . $daddr . '&saddr=' . $this->addr); // 212+S.+Clinton+Street,+Iowa+City,+IA+52240+USA
			}
			if ((string)$this->index == '2') {
				$daddr = '523 E. Roosevelt Rd. Lombard, IL 60148';
				$this->redirect('https://maps.google.com/maps?daddr=' . $daddr . '&saddr=' . $this->addr);
			}

		} catch (Exception $lException) {
			$this->logMessage($lException->getMessage(), 'err');
			return sfView::ERROR;

		}
	}

}