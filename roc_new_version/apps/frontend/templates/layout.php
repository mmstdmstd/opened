<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<?php include_http_metas() ?>
	<?php include_metas() ?>
	<?php include_title() ?>
	<?php include_stylesheets() ?>
	<?php include_javascripts() ?>

  <base href="<?php echo Util::getServerHost( sfContext::getInstance()->getConfiguration(), true, false ) ?>" />
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-37212791-1']);
    _gaq.push(['_trackPageview']);
    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>

  <script type="text/javascript"> jQuery.noConflict(); </script>

  <?php include_partial('main/header'); ?>
  <?php include_partial('main/top_menu'); ?>
  <div id="page_body_div">

    <?php echo $sf_content ?>

    <!--
		<div id="footer">
			<?php // if ( !( $sf_context->getUser()->isAuthenticated() ) ) : ?>
			<small> <?php // echo link_to("Login",'@sf_guard_signin' ); ?>&nbsp; </small>
			<?php  // else:  ?>
			<small> <?php  // echo link_to("Logout",'@sf_guard_signout' );  ?>&nbsp;
			<?php //Util::deb($sf_context->getUser()->isAuthenticated());
			//echo 'Logged as <b>' . $sf_context->getUser()->getGuardUser()->getUsername().'</b>&nbsp;('.
			// $sf_context->getUser()->getAccessAsText(). ' )' ?>

			</small>
			<?php // endif; ?>
			<small>
            <p>
			<b><?php // echo sfConfig::get('app_application_name') .'</b> '.sfConfig::get('app_application_version')?>
            </p>
            <p>	(c) 2012 App Name. </p>
			</small>
		</div>  -->

  </div>  <!-- close page_body_div -->

  <?php include_partial('main/footer'); ?>



</div> <!-- / wrapper -->
</body>


</html>