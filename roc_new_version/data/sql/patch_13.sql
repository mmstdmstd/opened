alter TABLE `ordered`
  add column `transaction_id` VARCHAR(20) after `discount`;
	
alter TABLE `ordered`
  add KEY `ind_ordered_transaction_id`(`transaction_id`);
