SET FOREIGN_KEY_CHECKS = 0;
alter table  `inventory_category`
  drop  key ind_inventory_category_product_line;

alter table  `inventory_category`
  add column `id` INTEGER  NOT NULL AUTO_INCREMENT after  `product_line`;

alter table  `inventory_category`
  add PRIMARY KEY (`id`);


alter table  `inventory_category`
  add UNIQUE KEY `ind_inventory_category` (`product_line`, `category`, `subcategory`);



SET FOREIGN_KEY_CHECKS = 1;

alter TABLE `inventory_item`
  change column `brand_id` `brand_id` INTEGER;


