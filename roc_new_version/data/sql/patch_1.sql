INSERT INTO `sf_guard_group` (`id`, `name`, `description`) VALUES
(1, 'public', 'public access...'),
(2, 'editor', 'editor...'),
(3, 'admin', 'admin group...');


#drop table `brand`;
drop table `sale_method`;


Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 1', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 2', '2011-11-11 12:10:12', '2011-11-11 12:10:12' );

Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 3', '2011-10-01 11:01:12', '2011-10-01 11:01:12' );


Insert into `inventory_category` ( `product_line`, `category`, `subcategory`, `created_at` , `updated_at` )
  values( 'S001',   'Footwear', 'Boots',   '2011-12-21 15:11:22', '2011-12-21 15:11:22' );


INSERT INTO `inventory_category` (`product_line`, `description`, `category`, `subcategory`, `created_at`, `updated_at`) VALUES
('A006', NULL, 'Category A006', 'Subcategory A006', '2012-01-18 08:05:40', '2012-01-18 08:05:40'),
('D003', NULL, 'Category D003', 'Subcategory D003', '2012-01-09 07:52:33', '2012-01-09 07:52:33'),
('T005', NULL, 'Category T005', 'Subcategory T005', '2012-01-18 08:07:07', '2012-01-18 08:07:07');



Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`,  `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 1', 'Sku_1', 3, 'A006', 'Inventory Item # 1 description short', '5.11', 'red', 'Sale Method # 1', 5, 23.21,  156, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 2', 'Sku_2', 3, 'A006', 'Inventory Item # 2 description short', '5.1', 'black', 'Sale Method # 1', 15, 45.09, 412, 2, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`,  `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 3', 'Sku_3', 2, 'A006', 'Inventory Item # 3 description short', '5.11', 'white', 'Sale Method # 1', 25, 876.45, 128, 0, 'tax', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 4', 'Sku_4', 1, 'A006', 'Inventory Item # 4 description short', '5.1', 'gray', 'Sale Method # 1', 35, 32.10, 31, 2, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 5', 'Sku_5', 2, 'D003', 'Inventory Item # 5 description short', '5.12', 'red', 'Sale Method # 1', 45, 52.41, 51, 0.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 6', 'Sku_6', 3, 'A006', 'Inventory Item # 6 description short', '5.1', 'gray', 'Sale Method # 1', 55, 312.14, 37, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 7', 'Sku_7', 1, 'D003', 'Inventory Item # 7 description short', '5.2', 'blue', 'Sale Method # 1', 58, 25.10, 21, 2.2, '', '2011-12-01 11:10:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 8', 'Sku_8', 2, 'A006', 'Inventory Item # 8 description short', '5.11', 'red', 'Sale Method # 1', 75, 3.24, 17, .5, '', '2011-12-11 15:11:02', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 9', 'Sku_9', 3, 'T005', 'Inventory Item # 9 description short', '5.6', 'white', 'Sale Method # 1', 15, 24.01, 44, 0, '', '2011-11-01 11:10:02', '2011-11-01 11:10:02', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 10', 'Sku_10', 2, 'D003', 'Inventory Item # 10 description short', '5.1', 'gray', 'Sale Method # 1', 55, 312.14, 37, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 11', 'Sku_11', 3, 'A006', 'Inventory Item # 12 description short', '5.10', 'navy', 'Sale Method # 1', 5, 5.40, 21, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 12', 'Sku_12', 1, 'T005', 'Inventory Item # 12 description short', '5.9', 'white', 'Sale Method # 1', 65, 30.9, 32, .5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );



Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 13', 'Sku_13', 2, 'D003', 'Inventory Item # 13 description short', '5.11', 'navy', 'Sale Method # 1', 52, 35.4, 25, .5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 14', 'Sku_14', 2, 'A006', 'Inventory Item # 14 description short', '5.10', 'white', 'Sale Method # 1', 61, 32.4, 12, 3.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );






Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 15', 'Sku_15', 1, 'A006', 'Inventory Item # 15 description short', '5.10', 'navy', 'Sale Method # 1', 12, 325.1, 15, .2, '', '2011-02-19 15:01:12', '2011-02-19 15:01:12', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 16', 'Sku_16', 3, 'T005', 'Inventory Item # 16 description short', '5.12', 'red', 'Sale Method # 1', 42, 22.1, 8, 1, '', '2011-09-23 11:01:15', '2011-09-23 11:01:15', '2011-12-21 15:11:22' );


Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 17', 'Sku_17', 2, 'D003', 'Inventory Item # 17 description short', '5.09', 'gray', 'Sale Method # 1', 22, 32.14, 11, 1.5, '', '2011-07-25 03:02:12', '2011-07-25 03:02:12', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at`, `date_updated` )
values( 'Inventory Item 18', 'Sku_18', 3, 'A006', 'Inventory Item # 18 description short', '5.11', 'white', 'Sale Method # 1', 11, 32, 12, .5, '', '2011-04-03 18:01:04', '2011-04-03 18:01:04', '2011-12-21 15:11:22' );











