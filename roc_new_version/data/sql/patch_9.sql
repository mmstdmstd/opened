DROP TABLE IF EXISTS `sizing_chart_mapping`;

CREATE TABLE `sizing_chart_mapping`
(
	`sizing` VARCHAR(10)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	PRIMARY KEY (`sizing`)
)Engine=InnoDB;




insert into youtube_video( `key`, url, ordering, description, created_at) 
  values ( 'erg', 'http://www.youtube.com/embed/C0DPdy98e4c', '1', '', '2012-01-01' );  
insert into youtube_video( `key`, url, ordering, description, created_at) 
  values ( 'htr', 'http://www.youtube.com/embed/MOTA1rUwYVI', '2', '', '2012-01-01' );
insert into youtube_video( `key`, url, ordering, description, created_at) 
  values ( 'yte', 'http://www.youtube.com/embed/2aSytXt4BBQ', '3', '', '2012-01-01' );
insert into youtube_video( `key`, url, ordering, description, created_at) 
  values ( 'gde', 'http://www.youtube.com/embed/Z5DDPe8-6fQ', '4', '', '2012-01-01' );    
insert into youtube_video( `key`, url, ordering, description, created_at) 
  values ( 'ret', 'http://www.youtube.com/embed/YzE8r7-iW7k', '5', '', '2012-01-01' );



DROP TABLE IF EXISTS `pdf_document`;
CREATE TABLE `pdf_document`
(
	`code` VARCHAR(10)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	PRIMARY KEY (`code`)
)Engine=InnoDB;


insert into `pdf_document`(`code`, `filename`)
  values('swe','Light Fixtures.pdf');
insert into `pdf_document`(`code`, `filename`)
  values('der','Basement masterbath option.pdf');

insert into `pdf_document`(`code`, `filename`)
  values('hre','reference_calendar.pdf');

insert into `pdf_document`(`code`, `filename`)
  values('fre','imagerotator.pdf');
