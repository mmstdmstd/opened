alter TABLE `ordered`
  add column `notes` TEXT after `transaction_id`;
  
alter TABLE `ordered`
  add column `payment_method` VARCHAR(2)  NOT NULL after `notes`;
    
alter TABLE `ordered`
  add column `shipping` VARCHAR(20)  NOT NULL after `payment_method`;  