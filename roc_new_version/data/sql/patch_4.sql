CREATE TABLE `email_submission`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_email_submission` (`email`)
)Engine=InnoDB;
