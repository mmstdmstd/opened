DROP TABLE IF EXISTS `document`;
CREATE TABLE `document`
(
	`key` VARCHAR(3)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Engine=InnoDB;

DROP TABLE IF EXISTS `sizing_chart`;


CREATE TABLE `sizing_chart`
(
	`key` VARCHAR(3)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Engine=InnoDB;


INSERT INTO document (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('abc', 'Logisitcs Supply Fire Extinguisher Manual', 'Logistics-Supply.doc', 'Logistics-Supply.jpg', 1, '2012-09-05', '2012-09-05');
INSERT INTO document (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('gre', 'Bushnell Binoculars 10x50 PermaFocus Manual', 'Bushnell-Binoculars.doc', 'Bushnell-Binoculars.jpg', 2, '2012-04-09', '2012-07-02');
INSERT INTO document (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('kyt', 'Decatur Electronics Hand-Held Radar Manual', 'Logistics-Supply.doc', 'Logistics-Supply.jpg', 3, '2012-02-15', '2012-06-21');  
INSERT INTO document (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('hes', 'Blackhawk Products Group Omega Gas Mask Manual', 'Bushnell-Binoculars.doc', 'Bushnell-Binoculars.jpg', 4, '2012-02-22', '2012-02-22');
  
  
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('511', '5.11 Size Chart', '511sizechart.doc', '511sizechart.png', 1, '2012-09-24', '2012-09-24');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('AET', 'ArmorExpress Tactical Size Chart', 'ArmorExpressTacticalSizeChart.doc', 'ArmorExpressTacticalSizeChart.png', 2, '2012-08-16', '2012-08-16');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('AF', 'ArmorExpress Female Size Chart', 'ArmorExpressFemaleSizeChart.doc', 'ArmorExpressFemaleSizeChart.png', 3, '2012-07-13', '2012-07-13');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('AM', 'ArmorExpress Male Size Chart', 'ArmorExpressMaleSizeChart.doc', 'ArmorExpressMaleSizeChart.png', 4, '2012-03-12', '2012-03-12');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('DSC', 'Damascus Size Chart', 'DamascusSizingChart.doc', 'DamascusSizingChart.png', 5, '2012-01-14', '2012-01-14');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('TSM', 'Tru-Spec Mens Size Chart', 'TRU-SPECchart_mens.doc', 'TRU-SPECchart_mens.png', 6, '2012-07-09', '2012-07-09');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('TSL', 'Tru-Spec Ladies Size Chart', 'TRU-SPECchart_ladies.doc', 'TRU-SPECchart_ladies.png', 7, '2012-07-22', '2012-07-22');   
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('VSC', 'Veridian Size Chart', 'VeridianSizeChart.doc', 'VeridianSizeChart.png', 8, '2012-05-23', '2012-05-23');
   

INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('rea', 'Veridian Size Chart', 'VeridianSizeChart.doc', 'VeridianSizeChart.png', 2, '2012-07-13', '2012-07-13');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('oit', 'TRU-SPEC (Mens)', 'TRU-SPECchart_mens.doc', 'TRU-SPECchart_mens.png', 3, '2012-06-04', '2012-06-04');
INSERT INTO sizing_chart (`key`, title, filename, thumbnail, ordering, created_at, updated_at) 
  VALUES ('bde', 'TRU-SPEC (Ladies)', 'TRU-SPECchart_ladies.doc', 'TRU-SPECchart_ladies.png', 4, '2012-08-16', '2012-08-16');





