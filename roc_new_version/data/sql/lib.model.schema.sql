
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- sf_guard_user_profile
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sf_guard_user_profile`;


CREATE TABLE `sf_guard_user_profile`
(
	`user_id` INTEGER  NOT NULL,
	`first_name` VARCHAR(50),
	`last_name` VARCHAR(50),
	`b_country` VARCHAR(2)  NOT NULL,
	`b_street` VARCHAR(50)  NOT NULL,
	`b_street_2` VARCHAR(50)  NOT NULL,
	`b_city` VARCHAR(50)  NOT NULL,
	`b_state` VARCHAR(2)  NOT NULL,
	`b_zip` VARCHAR(10)  NOT NULL,
	`phone` VARCHAR(50)  NOT NULL,
	`fax` VARCHAR(50)  NOT NULL,
	`url` VARCHAR(50)  NOT NULL,
	`s_country` VARCHAR(2)  NOT NULL,
	`s_street` VARCHAR(50)  NOT NULL,
	`s_street_2` VARCHAR(50)  NOT NULL,
	`s_city` VARCHAR(50)  NOT NULL,
	`s_state` VARCHAR(2)  NOT NULL,
	`s_zip` VARCHAR(10)  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_sf_guard_user_profile` (`user_id`),
	CONSTRAINT `sf_guard_user_profile_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- brand
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `brand`;


CREATE TABLE `brand`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(50)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_brand` (`title`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- sizings
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sizings`;


CREATE TABLE `sizings`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(32)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_sizings` (`title`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- measures
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `measures`;


CREATE TABLE `measures`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(32)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_measures` (`name`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- inventory_category
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_category`;


CREATE TABLE `inventory_category`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`product_line` VARCHAR(10)  NOT NULL,
	`description` VARCHAR(255),
	`category` VARCHAR(100)  NOT NULL,
	`subcategory` VARCHAR(100)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_inventory_category` (`product_line`, `category`, `subcategory`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- inventory_item
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_item`;


CREATE TABLE `inventory_item`
(
	`id` INTEGER  NOT NULL,
	`sku` VARCHAR(30)  NOT NULL,
	`title` VARCHAR(100)  NOT NULL,
	`brand_id` INTEGER,
	`description_short` VARCHAR(255)  NOT NULL,
	`description_long` TEXT,
	`size` VARCHAR(10),
	`color` VARCHAR(7),
	`gender` VARCHAR(10),
	`sizing_id` INTEGER,
	`customizable` TINYINT default 0,
	`video` VARCHAR(100),
	`measure_id` INTEGER,
	`qty_on_hand` INTEGER(4),
	`std_unit_price` FLOAT(9,2),
	`msrp` FLOAT(9,2),
	`clearance` FLOAT(9,2),
	`sale_start_date` DATE,
	`sale_end_date` DATE,
	`sale_method` VARCHAR(30)  NOT NULL,
	`sale_price` FLOAT(9,2),
	`sale_percent` FLOAT(3,1),
	`tax_class` VARCHAR(10),
	`ship_weight` VARCHAR(10),
	`sale_promo_discount` FLOAT(3,1),
	`finish` VARCHAR(20),
	`documents` VARCHAR(50),
	`hand` VARCHAR(50),
	`matrix` TINYINT default 0,
	`rootitem` VARCHAR(30),
	`features` VARCHAR(255),
	`setup_charge` TINYINT default 0,
	`date_updated` DATETIME  NOT NULL,
	`time_updated` FLOAT(9,5) default 0 NOT NULL,
	`recently_viewed` DATETIME,
	`additional_freight` FLOAT(9,2),
	`free_shipping` TINYINT default 0,
	`phone_order` TINYINT default 0,
	`udf_it_swc_beige` TINYINT default 0,
	`udf_it_swc_black` TINYINT default 0,
	`udf_it_swc_blue` TINYINT default 0,
	`udf_it_swc_brown` TINYINT default 0,
	`udf_it_swc_camo` TINYINT default 0,
	`udf_it_swc_gold` TINYINT default 0,
	`udf_it_swc_gray` TINYINT default 0,
	`udf_it_swc_green` TINYINT default 0,
	`udf_it_swc_orange` TINYINT default 0,
	`udf_it_swc_pink` TINYINT default 0,
	`udf_it_swc_purple` TINYINT default 0,
	`udf_it_swc_red` TINYINT default 0,
	`udf_it_swc_silver` TINYINT default 0,
	`udf_it_swc_white` TINYINT default 0,
	`udf_it_swc_yellow` TINYINT default 0,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`sku`),
	UNIQUE KEY `ind_inventory_item_sku` (`sku`),
	KEY `ind_mgmtsys_inventory_matrix_rootitem`(`matrix`, `rootitem`),
	INDEX `I_referenced_ordered_item_options_items_FK_1_1` (`id`),
	INDEX `inventory_item_FI_1` (`brand_id`),
	CONSTRAINT `inventory_item_FK_1`
		FOREIGN KEY (`brand_id`)
		REFERENCES `brand` (`id`)
		ON DELETE RESTRICT,
	INDEX `inventory_item_FI_2` (`sizing_id`),
	CONSTRAINT `inventory_item_FK_2`
		FOREIGN KEY (`sizing_id`)
		REFERENCES `sizings` (`id`)
		ON DELETE RESTRICT,
	INDEX `inventory_item_FI_3` (`measure_id`),
	CONSTRAINT `inventory_item_FK_3`
		FOREIGN KEY (`measure_id`)
		REFERENCES `measures` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- ordered
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ordered`;


CREATE TABLE `ordered`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_id` INTEGER  NOT NULL,
	`status` VARCHAR(1) default 'O' NOT NULL,
	`b_country` VARCHAR(2)  NOT NULL,
	`b_street` VARCHAR(50)  NOT NULL,
	`b_street_2` VARCHAR(50)  NOT NULL,
	`b_city` VARCHAR(50)  NOT NULL,
	`b_state` VARCHAR(2)  NOT NULL,
	`b_zip` VARCHAR(10)  NOT NULL,
	`b_phone` VARCHAR(50)  NOT NULL,
	`s_country` VARCHAR(2)  NOT NULL,
	`s_street` VARCHAR(50)  NOT NULL,
	`s_street_2` VARCHAR(50)  NOT NULL,
	`s_city` VARCHAR(50)  NOT NULL,
	`s_state` VARCHAR(2)  NOT NULL,
	`s_zip` VARCHAR(10)  NOT NULL,
	`total_price` FLOAT(9,2)  NOT NULL,
	`tax` FLOAT(9,2)  NOT NULL,
	`shipping_cost` FLOAT(9,2)  NOT NULL,
	`discount` FLOAT(9,2)  NOT NULL,
	`transaction_id` VARCHAR(20),
	`notes` TEXT,
	`payment_method` VARCHAR(2)  NOT NULL,
	`shipping` VARCHAR(20)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `ind_ordered_transaction_id`(`transaction_id`),
	INDEX `ordered_FI_1` (`user_id`),
	CONSTRAINT `ordered_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- ordered_item
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ordered_item`;


CREATE TABLE `ordered_item`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`order_id` INTEGER  NOT NULL,
	`inventory_item_id` INTEGER  NOT NULL,
	`sku` VARCHAR(30),
	`qty` INTEGER(4)  NOT NULL,
	`unit_price` FLOAT(9,2)  NOT NULL,
	`title` VARCHAR(100)  NOT NULL,
	`size` VARCHAR(10),
	`color` VARCHAR(7),
	`options` TEXT,
	`finish` VARCHAR(20),
	`hand` VARCHAR(50),
	`gender` VARCHAR(10),
	`measure_id` INTEGER,
	`ship_weight` VARCHAR(10),
	`backorder` TINYINT default 0,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `ind_ordered_item_sku`(`sku`),
	KEY `ind_ordered_item_inventory_item_id`(`inventory_item_id`),
	INDEX `ordered_item_FI_1` (`order_id`),
	CONSTRAINT `ordered_item_FK_1`
		FOREIGN KEY (`order_id`)
		REFERENCES `ordered` (`id`)
		ON DELETE RESTRICT,
	INDEX `ordered_item_FI_2` (`measure_id`),
	CONSTRAINT `ordered_item_FK_2`
		FOREIGN KEY (`measure_id`)
		REFERENCES `measures` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- ordered_item_options_items
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `ordered_item_options_items`;


CREATE TABLE `ordered_item_options_items`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`option_id` INTEGER  NOT NULL,
	`ordered_item_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_ordered_item_options_items` (`inventory_item_id`, `option_id`, `ordered_item_id`),
	CONSTRAINT `ordered_item_options_items_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	INDEX `ordered_item_options_items_FI_2` (`option_id`),
	CONSTRAINT `ordered_item_options_items_FK_2`
		FOREIGN KEY (`option_id`)
		REFERENCES `options` (`id`)
		ON DELETE RESTRICT,
	INDEX `ordered_item_options_items_FI_3` (`ordered_item_id`),
	CONSTRAINT `ordered_item_options_items_FK_3`
		FOREIGN KEY (`ordered_item_id`)
		REFERENCES `ordered_item` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- email_submission
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `email_submission`;


CREATE TABLE `email_submission`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(50)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_email_submission` (`email`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- youtube_video
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `youtube_video`;


CREATE TABLE `youtube_video`
(
	`key` VARCHAR(3)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`url` VARCHAR(100)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`description` TEXT,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- paperwork
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `paperwork`;


CREATE TABLE `paperwork`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`code` VARCHAR(16)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(256)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_paperwork` (`code`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- inventory_item_paperwork
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_item_paperwork`;


CREATE TABLE `inventory_item_paperwork`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`paperwork_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_inventory_item_paperwork` (`paperwork_id`, `inventory_item_id`),
	INDEX `inventory_item_paperwork_FI_1` (`inventory_item_id`),
	CONSTRAINT `inventory_item_paperwork_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	CONSTRAINT `inventory_item_paperwork_FK_2`
		FOREIGN KEY (`paperwork_id`)
		REFERENCES `paperwork` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- inventory_item_inventory_category
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_item_inventory_category`;


CREATE TABLE `inventory_item_inventory_category`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`inventory_category_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_inventory_item_inventory_category` (`inventory_category_id`, `inventory_item_id`),
	INDEX `inventory_item_inventory_category_FI_1` (`inventory_item_id`),
	CONSTRAINT `inventory_item_inventory_category_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	CONSTRAINT `inventory_item_inventory_category_FK_2`
		FOREIGN KEY (`inventory_category_id`)
		REFERENCES `inventory_category` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- document
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `document`;


CREATE TABLE `document`
(
	`key` VARCHAR(3)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- sizing_chart
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `sizing_chart`;


CREATE TABLE `sizing_chart`
(
	`key` VARCHAR(3)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(100)  NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- user_shipping_info
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `user_shipping_info`;


CREATE TABLE `user_shipping_info`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_id` INTEGER  NOT NULL,
	`b_name` VARCHAR(100)  NOT NULL,
	`b_country` VARCHAR(2)  NOT NULL,
	`b_phone` VARCHAR(50)  NOT NULL,
	`b_street` VARCHAR(50)  NOT NULL,
	`b_street_2` VARCHAR(50),
	`b_city` VARCHAR(50)  NOT NULL,
	`b_state` VARCHAR(2)  NOT NULL,
	`b_zip` VARCHAR(10)  NOT NULL,
	`s_name` VARCHAR(100)  NOT NULL,
	`s_country` VARCHAR(2)  NOT NULL,
	`s_street` VARCHAR(50)  NOT NULL,
	`s_street_2` VARCHAR(50)  NOT NULL,
	`s_city` VARCHAR(50)  NOT NULL,
	`s_state` VARCHAR(2)  NOT NULL,
	`s_zip` VARCHAR(10)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_user_shipping_info_user_id` (`user_id`),
	CONSTRAINT `user_shipping_info_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- color_mapping
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `color_mapping`;


CREATE TABLE `color_mapping`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`color` VARCHAR(50)  NOT NULL,
	`swatch_color_1` VARCHAR(50)  NOT NULL,
	`swatch_color_2` VARCHAR(50),
	`swatch_color_3` VARCHAR(50),
	PRIMARY KEY (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- user_badge_data
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `user_badge_data`;


CREATE TABLE `user_badge_data`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_id` INTEGER  NOT NULL,
	`data` TEXT  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `user_badge_data_FI_1` (`user_id`),
	CONSTRAINT `user_badge_data_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- size_order
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `size_order`;


CREATE TABLE `size_order`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`size` VARCHAR(50)  NOT NULL,
	PRIMARY KEY (`id`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- import_rules
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `import_rules`;


CREATE TABLE `import_rules`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`key_name` VARCHAR(50)  NOT NULL,
	`key_value` VARCHAR(255)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_import_rules_key_name` (`key_name`)
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- options
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `options`;


CREATE TABLE `options`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_options_title` ()
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- options_items
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `options_items`;


CREATE TABLE `options_items`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`option_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_options_items` (`inventory_item_id`, `option_id`),
	CONSTRAINT `options_items_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	INDEX `options_items_FI_2` (`option_id`),
	CONSTRAINT `options_items_FK_2`
		FOREIGN KEY (`option_id`)
		REFERENCES `options` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- options_value
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `options_value`;


CREATE TABLE `options_value`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`option_id` INTEGER  NOT NULL,
	`value` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_options_value` (`inventory_item_id`, `option_id`),
	CONSTRAINT `options_value_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	INDEX `options_value_FI_2` (`option_id`),
	CONSTRAINT `options_value_FK_2`
		FOREIGN KEY (`option_id`)
		REFERENCES `options` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

#-----------------------------------------------------------------------------
#-- uniques_sku_list
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `uniques_sku_list`;


CREATE TABLE `uniques_sku_list`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`unique_session_id` VARCHAR(50)  NOT NULL,
	`sku` VARCHAR(30)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `uniques_sku_list`(`unique_session_id`, `sku`),
	KEY `uniques_sku_list_created_at`(`created_at`)
)Type=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
