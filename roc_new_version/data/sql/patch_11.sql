alter TABLE `inventory_item`
  add column `matrix` TINYINT default 0 after `documents`;

alter TABLE `inventory_item`
  add column `rootitem` VARCHAR(30) after `matrix`;

	
alter TABLE `inventory_item`
  add INDEX `inventory_item_FI_3` (`rootitem`);

alter TABLE `inventory_item`
  add KEY `ind_mgmtsys_inventory_matrix_rootitem`(`matrix`, `rootitem`);



delete from ordered;
delete from ordered_item;

delete from inventory_item;
delete from inventory_category;
delete from brand;

