alter TABLE `inventory_item`
  add column `msrp` FLOAT(9,2) after `std_unit_price`;

alter TABLE `inventory_item`
  add column `sale_promo_discount` FLOAT(3,1) after `ship_weight`;

alter TABLE `inventory_item`
  add column `finish` VARCHAR(20) after `sale_promo_discount`;

alter TABLE `inventory_item`
  add column `documents` VARCHAR(255) after `finish`;

alter TABLE `inventory_item`
  add column `setup_charge` VARCHAR(10) after `documents`;

alter TABLE `inventory_item`
  add column `date_updated` DATETIME after `setup_charge`;


