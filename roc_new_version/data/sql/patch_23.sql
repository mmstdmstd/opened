

alter TABLE `inventory_item`
  add KEY `inventory_item_FI_size` (`size`);
  
alter TABLE `inventory_item`
  add KEY `inventory_item_FI_color` (`color`);

  
alter TABLE `inventory_item`
  add KEY `inventory_item_FI_gender` (`gender`);

alter TABLE `inventory_item`
  add KEY `inventory_item_FI_finish` (`finish`);
  
alter TABLE `inventory_item`
  add KEY `inventory_item_FI_hand` (`hand`);

alter TABLE `inventory_item`
  add KEY `inventory_item_FI_std_unit_price` (`std_unit_price`);

alter TABLE `inventory_item`
  add KEY `inventory_item_FI_clearance` (`clearance`);