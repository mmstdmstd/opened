DROP TABLE IF EXISTS `options`;

CREATE TABLE `options`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`key_name` VARCHAR(50)  NOT NULL,
	`key_value` VARCHAR(255)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_options_key_name` (`key_name`)
)Type=InnoDB;
