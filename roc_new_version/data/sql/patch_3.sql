DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(50)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_brand` (`title`)
)Engine=InnoDB;


DROP TABLE IF EXISTS `inventory_category`;
CREATE TABLE `inventory_category`
(
	`product_line` VARCHAR(10)  NOT NULL,
	`category` VARCHAR(100)  NOT NULL,
	`subcategory` VARCHAR(100)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`product_line`)
)Engine=InnoDB;





Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 1', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 2', '2011-11-11 12:10:12', '2011-11-11 12:10:12' );

Insert into `brand` (`title`, `created_at` , `updated_at` )
  values( 'Brand # 3', '2011-10-01 11:01:12', '2011-10-01 11:01:12' );


Insert into `inventory_category` ( `product_line`, `category`, `subcategory`, `created_at` , `updated_at` )
  values( 'S001',   'Footwear', 'Boots',   '2011-12-21 15:11:22', '2011-12-21 15:11:22' );


alter TABLE `inventory_item`
  drop column `brand`;


alter TABLE `inventory_item`
  add column `brand_id` INTEGER  NOT NULL after `title`;


alter TABLE `inventory_item`
  add column `inventory_category` VARCHAR(10)  NOT NULL after `brand_id`;

alter TABLE `inventory_item`
  add INDEX `inventory_item_FI_2` (`inventory_category`);


alter TABLE `inventory_item`
  add CONSTRAINT `inventory_item_FK_2`
    FOREIGN KEY (`inventory_category`)
    REFERENCES `inventory_category` (`product_line`)
    ON DELETE RESTRICT;



alter TABLE `inventory_item`
  change column `gender` `gender` VARCHAR(10);



DROP TABLE IF EXISTS `inventory_item`;
CREATE TABLE `inventory_item`
(
	`sku` VARCHAR(30)  NOT NULL,
	`title` VARCHAR(100)  NOT NULL,
	`brand_id` INTEGER  NOT NULL,
	`inventory_category` VARCHAR(10)  NOT NULL,
	`description_short` VARCHAR(255)  NOT NULL,
	`description_long` TEXT,
	`size` VARCHAR(10),
	`color` VARCHAR(7),
	`options` TEXT,
	`gender` VARCHAR(10),
	`paperwork` VARCHAR(2),
	`sizing` VARCHAR(10),
	`customizable` TINYINT default 0,
	`video` VARCHAR(100),
	`unit_measure` VARCHAR(10),
	`qty_on_hand` INTEGER(4),
	`std_unit_price` FLOAT(9,2),
	`msrp` FLOAT(9,2),
	`clearance` FLOAT(9,2),
	`sale_start_date` DATE,
	`sale_end_date` DATE,
	`sale_method` VARCHAR(30)  NOT NULL,
	`sale_price` FLOAT(9,2),
	`sale_percent` FLOAT(3,1),
	`tax_class` VARCHAR(10),
	`ship_weight` VARCHAR(10),
	`sale_promo_discount` FLOAT(3,1),
	`finish` VARCHAR(20),
	`documents` VARCHAR(255),
	`setup_charge` VARCHAR(10),
	`date_updated` DATETIME  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`sku`),
	UNIQUE KEY `ind_inventory_item_sku` (`sku`),
	INDEX `inventory_item_FI_1` (`brand_id`),
	CONSTRAINT `inventory_item_FK_1`
		FOREIGN KEY (`brand_id`)
		REFERENCES `brand` (`id`)
		ON DELETE RESTRICT,
	INDEX `inventory_item_FI_2` (`inventory_category`),
	CONSTRAINT `inventory_item_FK_2`
		FOREIGN KEY (`inventory_category`)
		REFERENCES `inventory_category` (`product_line`)
		ON DELETE RESTRICT
)Type=InnoDB;



Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 1', 'Sku_1', 1, 'S001', 'Inventory Item # 1 description short', '5.11', 'red', 'Sale Method # 1', 5, 23.21, 0, 156, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 2', 'Sku_2', 2, 'S001', 'Inventory Item # 2 description short', '5.1', 'black', 'Sale Method # 1', 15, 45.09, 1.24, 412, 2, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`,  `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 3', 'Sku_3', 3, 'S001', 'Inventory Item # 3 description short', '5.11', 'white', 'Sale Method # 1', 25, 876.45, 11.23, 128, 0, 'tax', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 4', 'Sku_4', 1, 'S001', 'Inventory Item # 4 description short', '5.1', 'gray', 'Sale Method # 1', 35, 32.10, 10.21, 31, 2, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 5', 'Sku_5', 1, 'S001', 'Inventory Item # 5 description short', '5.12', 'red', 'Sale Method # 1', 45, 52.41, 0, 51, 0.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 6', 'Sku_6', 2, 'S001', 'Inventory Item # 6 description short', '5.1', 'gray', 'Sale Method # 1', 55, 312.14, 4.5, 37, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 7', 'Sku_7', 3, 'S001', 'Inventory Item # 7 description short', '5.2', 'blue', 'Sale Method # 1', 58, 25.10, 0, 21, 2.2, '', '2011-12-01 11:10:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 8', 'Sku_8', 1, 'S001', 'Inventory Item # 8 description short', '5.11', 'red', 'Sale Method # 1', 75, 3.24, 1.5, 17, .5, '', '2011-12-11 15:11:02', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 9', 'Sku_9', 2, 'S001', 'Inventory Item # 9 description short', '5.6', 'white', 'Sale Method # 1', 15, 24.01, 0, 44, 0, '', '2011-11-01 11:10:02', '2011-11-01 11:10:02' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 10', 'Sku_10', 3, 'S001', 'Inventory Item # 10 description short', '5.1', 'gray', 'Sale Method # 1', 55, 312.14, 8.7, 37, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 11', 'Sku_11', 2, 'S001', 'Inventory Item # 12 description short', '5.10', 'navy', 'Sale Method # 1', 5, 5.40, 0, 21, 1.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 12', 'Sku_12', 1, 'S001', 'Inventory Item # 12 description short', '5.9', 'white', 'Sale Method # 1', 65, 30.9, 1.22, 32, .5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );



Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 13', 'Sku_13', 1, 'S001', 'Inventory Item # 13 description short', '5.11', 'navy', 'Sale Method # 1', 52, 35.4, 0, 25, .5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 14', 'Sku_14', 2, 'S001', 'Inventory Item # 14 description short', '5.10', 'white', 'Sale Method # 1', 61, 32.4, 2.0, 12, 3.5, '', '2011-12-21 15:11:22', '2011-12-21 15:11:22' );






Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 15', 'Sku_15', 2, 'S001', 'Inventory Item # 15 description short', '5.10', 'navy', 'Sale Method # 1', 12, 325.1, 0, 15, .2, '', '2011-02-19 15:01:12', '2011-02-19 15:01:12' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 16', 'Sku_16', 3, 'S001', 'Inventory Item # 16 description short', '5.12', 'red', 'Sale Method # 1', 42, 22.1, 0, 8, 1, '', '2011-09-23 11:01:15', '2011-09-23 11:01:15' );


Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 17', 'Sku_17', 1, 'S001', 'Inventory Item # 17 description short', '5.09', 'gray', 'Sale Method # 1', 22, 32.14, 1.15, 11, 1.5, '', '2011-07-25 03:02:12', '2011-07-25 03:02:12' );

Insert into  `inventory_item` (`title` ,`sku`, `brand_id`, `inventory_category`, `description_short`, `size`, `color`, `sale_method`, `qty_on_hand`, `std_unit_price`, `clearance`, `sale_price`, `sale_percent`,
`tax_class`, `created_at` , `updated_at` )
values( 'Inventory Item 18', 'Sku_18', 1, 'S001', 'Inventory Item # 18 description short', '5.11', 'white', 'Sale Method # 1', 11, 32, 5.4, 12, .5, '', '2011-04-03 18:01:04', '2011-04-03 18:01:04' );











