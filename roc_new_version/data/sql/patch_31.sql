DROP TABLE IF EXISTS `uniques_sku_list`;



CREATE TABLE `uniques_sku_list`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`unique_session_id` VARCHAR(50)  NOT NULL,
	`sku` VARCHAR(30)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	KEY `uniques_sku_list`(`unique_session_id`, `sku`)
)Type=InnoDB;

alter TABLE `uniques_sku_list`
 add KEY `uniques_sku_list_created_at`(`created_at`);
