DROP TABLE IF EXISTS `color_mapping`;


CREATE TABLE `color_mapping`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`color` VARCHAR(50)  NOT NULL,
	`swatch_color_1` VARCHAR(50)  NOT NULL,
	`swatch_color_2` VARCHAR(50),
	`swatch_color_3` VARCHAR(50),
	PRIMARY KEY (`id`)
)Type=InnoDB;


alter TABLE `inventory_item`
  add column `udf_it_swc_beige` TINYINT default 0 after  `phone_order`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_black` TINYINT default 0 after `udf_it_swc_beige`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_blue` TINYINT default 0 after `udf_it_swc_black`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_brown` TINYINT default 0 after `udf_it_swc_blue`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_camo` TINYINT default 0 after `udf_it_swc_brown`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_gold` TINYINT default 0 after `udf_it_swc_camo`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_gray` TINYINT default 0 after `udf_it_swc_gold`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_green` TINYINT default 0 after `udf_it_swc_gray`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_orange` TINYINT default 0 after `udf_it_swc_green`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_pink` TINYINT default 0 after `udf_it_swc_orange`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_purple` TINYINT default 0 after `udf_it_swc_pink`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_red` TINYINT default 0 after `udf_it_swc_purple`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_silver` TINYINT default 0 after `udf_it_swc_red`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_white` TINYINT default 0 after `udf_it_swc_silver`;
  
alter TABLE `inventory_item`
  add column `udf_it_swc_yellow` TINYINT default 0 after `udf_it_swc_white`;
