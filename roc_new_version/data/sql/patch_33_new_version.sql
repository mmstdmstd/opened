DROP TABLE IF EXISTS `import_rules`;
CREATE TABLE `import_rules`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`key_name` VARCHAR(50)  NOT NULL,
	`key_value` VARCHAR(255)  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	`updated_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_import_rules_key_name` (`key_name`)
)Type=InnoDB;





DROP TABLE IF EXISTS `sizings`;
CREATE TABLE `sizings`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(32)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_sizings` (`title`)
)Type=InnoDB;


alter TABLE `inventory_item`
  drop column `sizing`;
  
alter TABLE `inventory_item`
  add column `sizing_id` INTEGER after `paperwork`;
  
alter TABLE `inventory_item`
  add INDEX `inventory_item_FI_4` (`sizing_id`);
  
alter TABLE `inventory_item`
  add CONSTRAINT `inventory_item_FK_5`
    FOREIGN KEY (`sizing_id`)
    REFERENCES `sizings` (`id`)
    ON DELETE RESTRICT;
    
Insert into sizings(title) value('511');
Update `inventory_item` set `sizing_id`= 1 where sku = '12001-019-040M';






alter TABLE inventory_item
  drop column unit_measure;
  
DROP TABLE IF EXISTS `measures`;
CREATE TABLE `measures`
(
  `id` INTEGER  NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32)  NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_measures` (`name`)
)Type=InnoDB;
    
alter TABLE `inventory_item`
  add column `measure_id` INTEGER after `video`;  
  
alter TABLE `inventory_item`
  add INDEX `inventory_item_FI_6` (`measure_id`);
  
alter TABLE `inventory_item`
  add CONSTRAINT `inventory_item_FK_6`
    FOREIGN KEY (`measure_id`)
    REFERENCES `measures` (`id`)
    ON DELETE RESTRICT;
    
Insert into `measures`(name) value('EACH');
Update `inventory_item` set `measure_id`= 1 where sku in ( '0-8001-060', '0-8001-085' );
    

    
    
    
ALTER TABLE paperwork DROP PRIMARY KEY;    
  
alter TABLE `paperwork`
  add column `id` INTEGER  NOT NULL AUTO_INCREMENT before `code`;

alter TABLE `paperwork`
  add PRIMARY KEY (`id`);
  
  
DROP TABLE IF EXISTS `paperwork`;
CREATE TABLE `paperwork`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`code` VARCHAR(16)  NOT NULL,
	`title` VARCHAR(50)  NOT NULL,
	`filename` VARCHAR(256)  NOT NULL,
	`ordering` INTEGER(2) default 0 NOT NULL,
	`thumbnail` VARCHAR(100)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_paperwork` (`code`)
)Type=InnoDB;

INSERT INTO `paperwork` (`code`, `title`, `filename`, `ordering`, `thumbnail`) VALUES
('CTS', 'CTS FET Exemption Certificate [PDF]', 'CTS.pdf', 1, 'CTS-tn.png'),
('DCS', 'Department Certifying Statement [PDF]', 'DCS.pdf', 2, 'DCS-tn.png'),
('FET', 'Federal Excise Tax Exemption Certificate [PDF]', 'FET.pdf', 3, 'FET-tn.png'),
('ICS', 'Individual Certifying Statement [PDF]', 'ICS.pdf', 4, 'ICS-tn.png'),
('SET', 'Sage Exemption Certificate [PDF]', 'SET.pdf', 5, 'SET-tn.png');

alter TABLE inventory_item
  drop column paperwork;
  
DROP TABLE IF EXISTS `inventory_item_paperwork`;
CREATE TABLE `inventory_item_paperwork`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`paperwork_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_inventory_item_paperwork` (`paperwork_id`, `inventory_item_id`),
	INDEX `inventory_item_paperwork_FI_1` (`inventory_item_id`),
	CONSTRAINT `inventory_item_paperwork_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	CONSTRAINT `inventory_item_paperwork_FK_2`
		FOREIGN KEY (`paperwork_id`)
		REFERENCES `paperwork` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

insert into `inventory_item_paperwork`(	`inventory_item_id`, `paperwork_id` ) values( '710', 1 );
insert into `inventory_item_paperwork`(	`inventory_item_id`, `paperwork_id` ) values( '710', 2 );
insert into `inventory_item_paperwork`(	`inventory_item_id`, `paperwork_id` ) values( '234', 3 );
insert into `inventory_item_paperwork`(	`inventory_item_id`, `paperwork_id` ) values( '234', 4 );
insert into `inventory_item_paperwork`(	`inventory_item_id`, `paperwork_id` ) values( '238', 5 );


alter TABLE inventory_item
  drop column inventory_category;
  
DROP TABLE IF EXISTS `inventory_item_inventory_category`;
CREATE TABLE `inventory_item_inventory_category`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`inventory_category_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_inventory_item_inventory_category` (`inventory_category_id`, `inventory_item_id`),
	INDEX `inventory_item_inventory_category_FI_1` (`inventory_item_id`),
	CONSTRAINT `inventory_item_inventory_category_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	CONSTRAINT `inventory_item_inventory_category_FK_2`
		FOREIGN KEY (`inventory_category_id`)
		REFERENCES `inventory_category` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;
  
insert into `inventory_item_inventory_category`	( `inventory_item_id`, `inventory_category_id` ) values( '913', 1313 );
insert into `inventory_item_inventory_category`	( `inventory_item_id`, `inventory_category_id` ) values( '913', 1313 );
insert into `inventory_item_inventory_category`	( `inventory_item_id`, `inventory_category_id` ) values( '238', 1419 );
insert into `inventory_item_inventory_category`	( `inventory_item_id`, `inventory_category_id` ) values( '238', 1419 );

# 200-383-161 -=> 913    103810=>238

# 103810,  200-383-161,  070-383-161


DROP TABLE IF EXISTS inventory_item_inventory_category;


alter TABLE `inventory_item`
 add column `id` INTEGER  NOT NULL AUTO_INCREMENT;
 
ALTER TABLE inventory_item DROP INDEX inventory_item_inventory_category_FI_1;

ALTER TABLE inventory_item DROP INDEX inventory_item_inventory_category_FK_1;

ALTER TABLE inventory_item DROP PRIMARY KEY;
 
 
alter TABLE `inventory_item`
 add PRIMARY KEY (`id`);

	


DROP TABLE IF EXISTS `options`;
CREATE TABLE `options`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_options_title` ()
)Type=InnoDB;


DROP TABLE IF EXISTS `options_items`;
CREATE TABLE `options_items`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`option_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `options_items_FI_1` (`inventory_item_id`),
	CONSTRAINT `options_items_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	INDEX `options_items_FI_2` (`option_id`),
	CONSTRAINT `options_items_FK_2`
		FOREIGN KEY (`option_id`)
		REFERENCES `options` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;

alter TABLE `options_items`
  add UNIQUE KEY `ind_options_items` (`inventory_item_id`, `option_id`);


insert into `options` (	`id`, `title` ) values( 1, '62025SH' );
insert into `options` (	`id`, `title` ) values( 2, 'SEE MEMO' );
insert into `options` (	`id`, `title` ) values( 3, 'PAE' );
insert into `options` (	`id`, `title` ) values( 4, 'CASE' );



insert into `options_items` (  `inventory_item_id`, `option_id` ) values ( 46, 1 );
insert into `options_items` (  `inventory_item_id`, `option_id` ) values ( 83, 2 );
insert into `options_items` (  `inventory_item_id`, `option_id` ) values ( 151, 3 );
insert into `options_items` (  `inventory_item_id`, `option_id` ) values ( 64, 4 );
insert into `options_items` (  `inventory_item_id`, `option_id` ) values ( 54, 4 );
# 07301 => 151    0-8003-115W => 46   06501 => 83   001042K => 64 - for testing
	
238


alter TABLE `ordered_item`
  add column `inventory_item_id` INTEGER  NOT NULL after order_id;
  
alter TABLE `ordered_item`
  add KEY `ind_ordered_item_sku`(`sku`);
  
alter TABLE `ordered_item`
  add KEY `ind_ordered_item_inventory_item_id`(`inventory_item_id`);

DROP TABLE IF EXISTS `ordered_item_options_items`;
CREATE TABLE `ordered_item_options_items`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`inventory_item_id` INTEGER  NOT NULL,
	`option_id` INTEGER  NOT NULL,
	`ordered_item_id` INTEGER  NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE KEY `ind_ordered_item_options_items` (`inventory_item_id`, `option_id`, `ordered_item_id`),
	CONSTRAINT `ordered_item_options_items_FK_1`
		FOREIGN KEY (`inventory_item_id`)
		REFERENCES `inventory_item` (`id`)
		ON DELETE RESTRICT,
	INDEX `ordered_item_options_items_FI_2` (`option_id`),
	CONSTRAINT `ordered_item_options_items_FK_2`
		FOREIGN KEY (`option_id`)
		REFERENCES `options` (`id`)
		ON DELETE RESTRICT,
	INDEX `ordered_item_options_items_FI_3` (`ordered_item_id`),
	CONSTRAINT `ordered_item_options_items_FK_3`
		FOREIGN KEY (`ordered_item_id`)
		REFERENCES `ordered_item` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;


alter table ordered_item
  drop column unit_measure;
    
alter TABLE `ordered_item`
  add column `measure_id` INTEGER;
  
alter TABLE `ordered_item`
  add INDEX `ordered_item_FI_2` (`measure_id`);
  
alter TABLE `ordered_item`
  add CONSTRAINT `ordered_item_FK_2`
  FOREIGN KEY (`measure_id`)
  REFERENCES `measures` (`id`)
  ON DELETE RESTRICT;
  