alter TABLE `inventory_item`
  add column `additional_freight` FLOAT(9,2) after `recently_viewed`;
  
  
alter TABLE `inventory_item`
  add column `free_shipping` TINYINT default 0 after `additional_freight`;
  
  
ALTER TABLE ordered_item DROP INDEX ordered_item_FI_2;


alter TABLE `ordered_item`	
  add column `title` VARCHAR(100)  NOT NULL after `unit_price`;
  
  
alter TABLE `ordered_item`	
  add column `size` VARCHAR(10) after `title`;
  
  
alter TABLE `ordered_item`	
  add column `color` VARCHAR(7) after `size`;
  
  
alter TABLE `ordered_item`	
  add column `options` TEXT after `color`;
  
  
alter TABLE `ordered_item`	
  add column `finish` VARCHAR(20) after `options`;
  
alter TABLE `ordered_item`	
  add column `hand` VARCHAR(50) after `finish`;
  
alter TABLE `ordered_item`	
  add column `gender` VARCHAR(10) after `hand`;
  
  
alter TABLE `ordered_item`	
  add column `unit_measure` VARCHAR(10) after `gender`;
  
alter TABLE `ordered_item`	
  add column `ship_weight` VARCHAR(10) after `unit_measure`;
  

	
	

	