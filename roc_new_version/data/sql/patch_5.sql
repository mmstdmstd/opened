DROP TABLE IF EXISTS `youtube_video`;


CREATE TABLE `youtube_video`
(
	`key` VARCHAR(3)  NOT NULL,
	`url` VARCHAR(100)  NOT NULL,
	`description` VARCHAR(255),
	`ordering` INTEGER(2) default 0 NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`key`)
)Engine=InnoDB;


alter TABLE `inventory_item`
 change `documents` `documents` VARCHAR(50);

update `inventory_item` SET `setup_charge` = 0;

alter TABLE `inventory_item`
 change `setup_charge` `setup_charge` TINYINT default 0;
