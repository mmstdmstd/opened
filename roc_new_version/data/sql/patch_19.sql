
alter TABLE `ordered`
  add  column `b_phone` VARCHAR(50)  NOT NULL after `b_country`;

alter TABLE `user_shipping_info`
  add column `b_phone` VARCHAR(50)  NOT NULL after `b_country`;
  