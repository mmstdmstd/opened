DROP TABLE IF EXISTS `user_badge_data`;


CREATE TABLE `user_badge_data`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`user_id` INTEGER  NOT NULL,
	`data` TEXT  NOT NULL,
	`created_at` DATETIME  NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `user_badge_data_FI_1` (`user_id`),
	CONSTRAINT `user_badge_data_FK_1`
		FOREIGN KEY (`user_id`)
		REFERENCES `sf_guard_user` (`id`)
		ON DELETE RESTRICT
)Type=InnoDB;
