<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    //$this->enablePlugins('sfDoctrinePlugin');
    $this->enablePlugins('sfPropelPlugin');
    $this->enablePlugins('sfGuardPlugin');
    $this->enablePlugins('sfGuardExtraPlugin');
    $this->enablePlugins('sfCsvPlugin');
    $this->enablePlugins('sfFormExtraPlugin');
    $this->enablePlugins('sfJQueryUIPlugin');
    $this->enablePlugins('sfExcelReaderPlugin');    

    sfConfig::add(array(
      'sf_web_dir'    => sfConfig::get('sf_root_dir'),
      'sf_upload_dir' => sfConfig::get('sf_root_dir').DIRECTORY_SEPARATOR.'uploads',
    ));
    
  }
}
